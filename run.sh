#!/bin/bash

function vagrant_halt()
{
	while [ -n "$(ps aux | grep 'vagrant up' | grep ruby)" ]
	do
		sleep 2
	done
	vagrant halt
}

vagrant up | zenity --progress --title='Starting Vagrant' --percentage=0 --auto-close --pulsate &

# get zenity brother process which is parent of all running tasks
PID_ZENITY=$(ps -C zenity h -o pid,command | grep "Starting Vagrant" | awk '{print $1}')
PID_PARENT=$(ps -p ${PID_ZENITY} -o ppid=)
PID_CHILDREN=$(pgrep -d ',' -P ${PID_PARENT})

# loop to check that progress dialog has not been cancelled
while [ "$PID_ZENITY" != "" ]
do
  # get PID of running processes for the children
  PID_GRANDCHILDREN=$(pgrep -d ' ' -P "${PID_CHILDREN}")

  # check if zenity PID is still there (dialog box still open)
  PID_ZENITY=$(ps h -o pid -p ${PID_ZENITY})

  # sleep for 2 second
  sleep 2
done

# if some running tasks are still there, kill them
if [ "${PID_GRANDCHILDREN}" != "" ]
then
	vagrant_halt
	kill -9 ${PID_GRANDCHILDREN}
	exit 2
fi

if [ -n "$(vagrant status | grep running)" ]
then
	eclipse/eclipse
	vagrant halt
else
	zenity --error --text="Vagrant isn't running."
fi
