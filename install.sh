#!/bin/bash

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   echo "Try running: sudo $0" 1>&2
   exit 1
fi

#get the absolute path of where the script is
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd )"

#check internet connection
wget -q --tries=10 --timeout=20 --spider http://google.com > /dev/null
if [[ $? -eq 0 ]]; then
        echo "Internet works!" >> "$ABSOLUTE_PATH"/installation_log.txt
else
        echo "Internet doesn't work." >> "$ABSOLUTE_PATH"/installation_log.txt
        echo "This script needs internet connection to install some stuff." 1>&2
        exit 1
fi

#check whether running on ubuntu or fedora and install vagrant accordingly
if [ -n "$(python -mplatform | grep -i fedora)" ]
then
	dnf install -y vagrant VirtualBox
elif [ -n "$(python -mplatform | grep -i ubuntu)" ]
then
	apt-get update
	apt-get -y install vagrant VirtualBox
fi
