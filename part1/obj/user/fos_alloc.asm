
obj/user/fos_alloc:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 b8 01 00 00       	call   8001ee <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:

#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	53                   	push   %ebx
  80003c:	83 ec 34             	sub    $0x34,%esp
	int size = 10 ;
  80003f:	c7 45 f0 0a 00 00 00 	movl   $0xa,-0x10(%ebp)
	int *x = malloc(sizeof(int)*size) ;
  800046:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800049:	c1 e0 02             	shl    $0x2,%eax
  80004c:	89 04 24             	mov    %eax,(%esp)
  80004f:	e8 6d 0f 00 00       	call   800fc1 <malloc>
  800054:	89 45 ec             	mov    %eax,-0x14(%ebp)
	int *y = malloc(sizeof(int)*size) ;
  800057:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80005a:	c1 e0 02             	shl    $0x2,%eax
  80005d:	89 04 24             	mov    %eax,(%esp)
  800060:	e8 5c 0f 00 00       	call   800fc1 <malloc>
  800065:	89 45 e8             	mov    %eax,-0x18(%ebp)
	int *z = malloc(sizeof(int)*size) ;
  800068:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80006b:	c1 e0 02             	shl    $0x2,%eax
  80006e:	89 04 24             	mov    %eax,(%esp)
  800071:	e8 4b 0f 00 00       	call   800fc1 <malloc>
  800076:	89 45 e4             	mov    %eax,-0x1c(%ebp)

	int i ;
	for (i = 0 ; i < size ; i++)
  800079:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  800080:	eb 63                	jmp    8000e5 <_main+0xad>
	{
		x[i] = i ;
  800082:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800085:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80008c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80008f:	01 c2                	add    %eax,%edx
  800091:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800094:	89 02                	mov    %eax,(%edx)
		y[i] = 10 ;
  800096:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800099:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000a0:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8000a3:	01 d0                	add    %edx,%eax
  8000a5:	c7 00 0a 00 00 00    	movl   $0xa,(%eax)
		z[i] = (int)x[i]  * y[i]  ;
  8000ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000ae:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000b5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8000b8:	01 c2                	add    %eax,%edx
  8000ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000bd:	8d 0c 85 00 00 00 00 	lea    0x0(,%eax,4),%ecx
  8000c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8000c7:	01 c8                	add    %ecx,%eax
  8000c9:	8b 08                	mov    (%eax),%ecx
  8000cb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000ce:	8d 1c 85 00 00 00 00 	lea    0x0(,%eax,4),%ebx
  8000d5:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8000d8:	01 d8                	add    %ebx,%eax
  8000da:	8b 00                	mov    (%eax),%eax
  8000dc:	0f af c1             	imul   %ecx,%eax
  8000df:	89 02                	mov    %eax,(%edx)
	int *x = malloc(sizeof(int)*size) ;
	int *y = malloc(sizeof(int)*size) ;
	int *z = malloc(sizeof(int)*size) ;

	int i ;
	for (i = 0 ; i < size ; i++)
  8000e1:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8000e5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000e8:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8000eb:	7c 95                	jl     800082 <_main+0x4a>
		x[i] = i ;
		y[i] = 10 ;
		z[i] = (int)x[i]  * y[i]  ;
	}
	
	for (i = 0 ; i < size ; i++)
  8000ed:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  8000f4:	eb 4f                	jmp    800145 <_main+0x10d>
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
  8000f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000f9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800100:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  800103:	01 d0                	add    %edx,%eax
  800105:	8b 08                	mov    (%eax),%ecx
  800107:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80010a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800111:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800114:	01 d0                	add    %edx,%eax
  800116:	8b 10                	mov    (%eax),%edx
  800118:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80011b:	8d 1c 85 00 00 00 00 	lea    0x0(,%eax,4),%ebx
  800122:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800125:	01 d8                	add    %ebx,%eax
  800127:	8b 00                	mov    (%eax),%eax
  800129:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80012d:	89 54 24 08          	mov    %edx,0x8(%esp)
  800131:	89 44 24 04          	mov    %eax,0x4(%esp)
  800135:	c7 04 24 00 16 80 00 	movl   $0x801600,(%esp)
  80013c:	e8 c7 01 00 00       	call   800308 <cprintf>
		x[i] = i ;
		y[i] = 10 ;
		z[i] = (int)x[i]  * y[i]  ;
	}
	
	for (i = 0 ; i < size ; i++)
  800141:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  800145:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800148:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  80014b:	7c a9                	jl     8000f6 <_main+0xbe>
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
	
	freeHeap();
  80014d:	e8 91 0e 00 00       	call   800fe3 <freeHeap>
	cprintf("the heap is freed successfully\n");
  800152:	c7 04 24 10 16 80 00 	movl   $0x801610,(%esp)
  800159:	e8 aa 01 00 00       	call   800308 <cprintf>
	z = malloc(sizeof(int)*size) ;
  80015e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800161:	c1 e0 02             	shl    $0x2,%eax
  800164:	89 04 24             	mov    %eax,(%esp)
  800167:	e8 55 0e 00 00       	call   800fc1 <malloc>
  80016c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	for (i = 0 ; i < size ; i++)
  80016f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  800176:	eb 67                	jmp    8001df <_main+0x1a7>
	{
		cprintf("x[i] = %d\t",x[i]);
  800178:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80017b:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800182:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800185:	01 d0                	add    %edx,%eax
  800187:	8b 00                	mov    (%eax),%eax
  800189:	89 44 24 04          	mov    %eax,0x4(%esp)
  80018d:	c7 04 24 30 16 80 00 	movl   $0x801630,(%esp)
  800194:	e8 6f 01 00 00       	call   800308 <cprintf>
		cprintf("y[i] = %d\t",y[i]);
  800199:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80019c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001a3:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8001a6:	01 d0                	add    %edx,%eax
  8001a8:	8b 00                	mov    (%eax),%eax
  8001aa:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001ae:	c7 04 24 3b 16 80 00 	movl   $0x80163b,(%esp)
  8001b5:	e8 4e 01 00 00       	call   800308 <cprintf>
		cprintf("z[i] = %d\n",z[i]);
  8001ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8001bd:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001c4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8001c7:	01 d0                	add    %edx,%eax
  8001c9:	8b 00                	mov    (%eax),%eax
  8001cb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001cf:	c7 04 24 46 16 80 00 	movl   $0x801646,(%esp)
  8001d6:	e8 2d 01 00 00       	call   800308 <cprintf>
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
	
	freeHeap();
	cprintf("the heap is freed successfully\n");
	z = malloc(sizeof(int)*size) ;
	for (i = 0 ; i < size ; i++)
  8001db:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8001df:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8001e2:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8001e5:	7c 91                	jl     800178 <_main+0x140>
		cprintf("y[i] = %d\t",y[i]);
		cprintf("z[i] = %d\n",z[i]);
	
	}

	return;	
  8001e7:	90                   	nop
}
  8001e8:	83 c4 34             	add    $0x34,%esp
  8001eb:	5b                   	pop    %ebx
  8001ec:	5d                   	pop    %ebp
  8001ed:	c3                   	ret    

008001ee <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8001ee:	55                   	push   %ebp
  8001ef:	89 e5                	mov    %esp,%ebp
  8001f1:	83 ec 18             	sub    $0x18,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8001f4:	c7 05 08 20 80 00 00 	movl   $0xeec00000,0x802008
  8001fb:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8001fe:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800202:	7e 0a                	jle    80020e <libmain+0x20>
		binaryname = argv[0];
  800204:	8b 45 0c             	mov    0xc(%ebp),%eax
  800207:	8b 00                	mov    (%eax),%eax
  800209:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  80020e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800211:	89 44 24 04          	mov    %eax,0x4(%esp)
  800215:	8b 45 08             	mov    0x8(%ebp),%eax
  800218:	89 04 24             	mov    %eax,(%esp)
  80021b:	e8 18 fe ff ff       	call   800038 <_main>

	// exit gracefully
	//exit();
	sleep();
  800220:	e8 16 00 00 00       	call   80023b <sleep>
}
  800225:	c9                   	leave  
  800226:	c3                   	ret    

00800227 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800227:	55                   	push   %ebp
  800228:	89 e5                	mov    %esp,%ebp
  80022a:	83 ec 18             	sub    $0x18,%esp
	sys_env_destroy(0);	
  80022d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800234:	e8 6d 0e 00 00       	call   8010a6 <sys_env_destroy>
}
  800239:	c9                   	leave  
  80023a:	c3                   	ret    

0080023b <sleep>:

void
sleep(void)
{	
  80023b:	55                   	push   %ebp
  80023c:	89 e5                	mov    %esp,%ebp
  80023e:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  800241:	e8 d7 0e 00 00       	call   80111d <sys_env_sleep>
}
  800246:	c9                   	leave  
  800247:	c3                   	ret    

00800248 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800248:	55                   	push   %ebp
  800249:	89 e5                	mov    %esp,%ebp
  80024b:	83 ec 18             	sub    $0x18,%esp
	b->buf[b->idx++] = ch;
  80024e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800251:	8b 00                	mov    (%eax),%eax
  800253:	8d 48 01             	lea    0x1(%eax),%ecx
  800256:	8b 55 0c             	mov    0xc(%ebp),%edx
  800259:	89 0a                	mov    %ecx,(%edx)
  80025b:	8b 55 08             	mov    0x8(%ebp),%edx
  80025e:	89 d1                	mov    %edx,%ecx
  800260:	8b 55 0c             	mov    0xc(%ebp),%edx
  800263:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800267:	8b 45 0c             	mov    0xc(%ebp),%eax
  80026a:	8b 00                	mov    (%eax),%eax
  80026c:	3d ff 00 00 00       	cmp    $0xff,%eax
  800271:	75 20                	jne    800293 <putch+0x4b>
		sys_cputs(b->buf, b->idx);
  800273:	8b 45 0c             	mov    0xc(%ebp),%eax
  800276:	8b 00                	mov    (%eax),%eax
  800278:	8b 55 0c             	mov    0xc(%ebp),%edx
  80027b:	83 c2 08             	add    $0x8,%edx
  80027e:	89 44 24 04          	mov    %eax,0x4(%esp)
  800282:	89 14 24             	mov    %edx,(%esp)
  800285:	e8 a6 0d 00 00       	call   801030 <sys_cputs>
		b->idx = 0;
  80028a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80028d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  800293:	8b 45 0c             	mov    0xc(%ebp),%eax
  800296:	8b 40 04             	mov    0x4(%eax),%eax
  800299:	8d 50 01             	lea    0x1(%eax),%edx
  80029c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80029f:	89 50 04             	mov    %edx,0x4(%eax)
}
  8002a2:	c9                   	leave  
  8002a3:	c3                   	ret    

008002a4 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002a4:	55                   	push   %ebp
  8002a5:	89 e5                	mov    %esp,%ebp
  8002a7:	81 ec 28 01 00 00    	sub    $0x128,%esp
	struct printbuf b;

	b.idx = 0;
  8002ad:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8002b4:	00 00 00 
	b.cnt = 0;
  8002b7:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8002be:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8002c1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8002c4:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8002c8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002cb:	89 44 24 08          	mov    %eax,0x8(%esp)
  8002cf:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002d5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8002d9:	c7 04 24 48 02 80 00 	movl   $0x800248,(%esp)
  8002e0:	e8 ed 01 00 00       	call   8004d2 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8002e5:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8002eb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8002ef:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002f5:	83 c0 08             	add    $0x8,%eax
  8002f8:	89 04 24             	mov    %eax,(%esp)
  8002fb:	e8 30 0d 00 00       	call   801030 <sys_cputs>

	return b.cnt;
  800300:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  800306:	c9                   	leave  
  800307:	c3                   	ret    

00800308 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800308:	55                   	push   %ebp
  800309:	89 e5                	mov    %esp,%ebp
  80030b:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80030e:	8d 45 0c             	lea    0xc(%ebp),%eax
  800311:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  800314:	8b 45 08             	mov    0x8(%ebp),%eax
  800317:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80031a:	89 54 24 04          	mov    %edx,0x4(%esp)
  80031e:	89 04 24             	mov    %eax,(%esp)
  800321:	e8 7e ff ff ff       	call   8002a4 <vcprintf>
  800326:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  800329:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80032c:	c9                   	leave  
  80032d:	c3                   	ret    

0080032e <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80032e:	55                   	push   %ebp
  80032f:	89 e5                	mov    %esp,%ebp
  800331:	53                   	push   %ebx
  800332:	83 ec 34             	sub    $0x34,%esp
  800335:	8b 45 10             	mov    0x10(%ebp),%eax
  800338:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80033b:	8b 45 14             	mov    0x14(%ebp),%eax
  80033e:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800341:	8b 45 18             	mov    0x18(%ebp),%eax
  800344:	ba 00 00 00 00       	mov    $0x0,%edx
  800349:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80034c:	77 72                	ja     8003c0 <printnum+0x92>
  80034e:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800351:	72 05                	jb     800358 <printnum+0x2a>
  800353:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800356:	77 68                	ja     8003c0 <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800358:	8b 45 1c             	mov    0x1c(%ebp),%eax
  80035b:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80035e:	8b 45 18             	mov    0x18(%ebp),%eax
  800361:	ba 00 00 00 00       	mov    $0x0,%edx
  800366:	89 44 24 08          	mov    %eax,0x8(%esp)
  80036a:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80036e:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800371:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800374:	89 04 24             	mov    %eax,(%esp)
  800377:	89 54 24 04          	mov    %edx,0x4(%esp)
  80037b:	e8 f0 0f 00 00       	call   801370 <__udivdi3>
  800380:	8b 4d 20             	mov    0x20(%ebp),%ecx
  800383:	89 4c 24 18          	mov    %ecx,0x18(%esp)
  800387:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  80038b:	8b 4d 18             	mov    0x18(%ebp),%ecx
  80038e:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  800392:	89 44 24 08          	mov    %eax,0x8(%esp)
  800396:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80039a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80039d:	89 44 24 04          	mov    %eax,0x4(%esp)
  8003a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8003a4:	89 04 24             	mov    %eax,(%esp)
  8003a7:	e8 82 ff ff ff       	call   80032e <printnum>
  8003ac:	eb 1c                	jmp    8003ca <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8003ae:	8b 45 0c             	mov    0xc(%ebp),%eax
  8003b1:	89 44 24 04          	mov    %eax,0x4(%esp)
  8003b5:	8b 45 20             	mov    0x20(%ebp),%eax
  8003b8:	89 04 24             	mov    %eax,(%esp)
  8003bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8003be:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8003c0:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  8003c4:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  8003c8:	7f e4                	jg     8003ae <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8003ca:	8b 4d 18             	mov    0x18(%ebp),%ecx
  8003cd:	bb 00 00 00 00       	mov    $0x0,%ebx
  8003d2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8003d5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8003d8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8003dc:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  8003e0:	89 04 24             	mov    %eax,(%esp)
  8003e3:	89 54 24 04          	mov    %edx,0x4(%esp)
  8003e7:	e8 b4 10 00 00       	call   8014a0 <__umoddi3>
  8003ec:	05 20 17 80 00       	add    $0x801720,%eax
  8003f1:	0f b6 00             	movzbl (%eax),%eax
  8003f4:	0f be c0             	movsbl %al,%eax
  8003f7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8003fa:	89 54 24 04          	mov    %edx,0x4(%esp)
  8003fe:	89 04 24             	mov    %eax,(%esp)
  800401:	8b 45 08             	mov    0x8(%ebp),%eax
  800404:	ff d0                	call   *%eax
}
  800406:	83 c4 34             	add    $0x34,%esp
  800409:	5b                   	pop    %ebx
  80040a:	5d                   	pop    %ebp
  80040b:	c3                   	ret    

0080040c <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80040c:	55                   	push   %ebp
  80040d:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80040f:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800413:	7e 1c                	jle    800431 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  800415:	8b 45 08             	mov    0x8(%ebp),%eax
  800418:	8b 00                	mov    (%eax),%eax
  80041a:	8d 50 08             	lea    0x8(%eax),%edx
  80041d:	8b 45 08             	mov    0x8(%ebp),%eax
  800420:	89 10                	mov    %edx,(%eax)
  800422:	8b 45 08             	mov    0x8(%ebp),%eax
  800425:	8b 00                	mov    (%eax),%eax
  800427:	83 e8 08             	sub    $0x8,%eax
  80042a:	8b 50 04             	mov    0x4(%eax),%edx
  80042d:	8b 00                	mov    (%eax),%eax
  80042f:	eb 40                	jmp    800471 <getuint+0x65>
	else if (lflag)
  800431:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800435:	74 1e                	je     800455 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  800437:	8b 45 08             	mov    0x8(%ebp),%eax
  80043a:	8b 00                	mov    (%eax),%eax
  80043c:	8d 50 04             	lea    0x4(%eax),%edx
  80043f:	8b 45 08             	mov    0x8(%ebp),%eax
  800442:	89 10                	mov    %edx,(%eax)
  800444:	8b 45 08             	mov    0x8(%ebp),%eax
  800447:	8b 00                	mov    (%eax),%eax
  800449:	83 e8 04             	sub    $0x4,%eax
  80044c:	8b 00                	mov    (%eax),%eax
  80044e:	ba 00 00 00 00       	mov    $0x0,%edx
  800453:	eb 1c                	jmp    800471 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  800455:	8b 45 08             	mov    0x8(%ebp),%eax
  800458:	8b 00                	mov    (%eax),%eax
  80045a:	8d 50 04             	lea    0x4(%eax),%edx
  80045d:	8b 45 08             	mov    0x8(%ebp),%eax
  800460:	89 10                	mov    %edx,(%eax)
  800462:	8b 45 08             	mov    0x8(%ebp),%eax
  800465:	8b 00                	mov    (%eax),%eax
  800467:	83 e8 04             	sub    $0x4,%eax
  80046a:	8b 00                	mov    (%eax),%eax
  80046c:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800471:	5d                   	pop    %ebp
  800472:	c3                   	ret    

00800473 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  800473:	55                   	push   %ebp
  800474:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800476:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  80047a:	7e 1c                	jle    800498 <getint+0x25>
		return va_arg(*ap, long long);
  80047c:	8b 45 08             	mov    0x8(%ebp),%eax
  80047f:	8b 00                	mov    (%eax),%eax
  800481:	8d 50 08             	lea    0x8(%eax),%edx
  800484:	8b 45 08             	mov    0x8(%ebp),%eax
  800487:	89 10                	mov    %edx,(%eax)
  800489:	8b 45 08             	mov    0x8(%ebp),%eax
  80048c:	8b 00                	mov    (%eax),%eax
  80048e:	83 e8 08             	sub    $0x8,%eax
  800491:	8b 50 04             	mov    0x4(%eax),%edx
  800494:	8b 00                	mov    (%eax),%eax
  800496:	eb 38                	jmp    8004d0 <getint+0x5d>
	else if (lflag)
  800498:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80049c:	74 1a                	je     8004b8 <getint+0x45>
		return va_arg(*ap, long);
  80049e:	8b 45 08             	mov    0x8(%ebp),%eax
  8004a1:	8b 00                	mov    (%eax),%eax
  8004a3:	8d 50 04             	lea    0x4(%eax),%edx
  8004a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8004a9:	89 10                	mov    %edx,(%eax)
  8004ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8004ae:	8b 00                	mov    (%eax),%eax
  8004b0:	83 e8 04             	sub    $0x4,%eax
  8004b3:	8b 00                	mov    (%eax),%eax
  8004b5:	99                   	cltd   
  8004b6:	eb 18                	jmp    8004d0 <getint+0x5d>
	else
		return va_arg(*ap, int);
  8004b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8004bb:	8b 00                	mov    (%eax),%eax
  8004bd:	8d 50 04             	lea    0x4(%eax),%edx
  8004c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8004c3:	89 10                	mov    %edx,(%eax)
  8004c5:	8b 45 08             	mov    0x8(%ebp),%eax
  8004c8:	8b 00                	mov    (%eax),%eax
  8004ca:	83 e8 04             	sub    $0x4,%eax
  8004cd:	8b 00                	mov    (%eax),%eax
  8004cf:	99                   	cltd   
}
  8004d0:	5d                   	pop    %ebp
  8004d1:	c3                   	ret    

008004d2 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8004d2:	55                   	push   %ebp
  8004d3:	89 e5                	mov    %esp,%ebp
  8004d5:	56                   	push   %esi
  8004d6:	53                   	push   %ebx
  8004d7:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004da:	eb 18                	jmp    8004f4 <vprintfmt+0x22>
			if (ch == '\0')
  8004dc:	85 db                	test   %ebx,%ebx
  8004de:	75 05                	jne    8004e5 <vprintfmt+0x13>
				return;
  8004e0:	e9 07 04 00 00       	jmp    8008ec <vprintfmt+0x41a>
			putch(ch, putdat);
  8004e5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8004e8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8004ec:	89 1c 24             	mov    %ebx,(%esp)
  8004ef:	8b 45 08             	mov    0x8(%ebp),%eax
  8004f2:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004f4:	8b 45 10             	mov    0x10(%ebp),%eax
  8004f7:	8d 50 01             	lea    0x1(%eax),%edx
  8004fa:	89 55 10             	mov    %edx,0x10(%ebp)
  8004fd:	0f b6 00             	movzbl (%eax),%eax
  800500:	0f b6 d8             	movzbl %al,%ebx
  800503:	83 fb 25             	cmp    $0x25,%ebx
  800506:	75 d4                	jne    8004dc <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  800508:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  80050c:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  800513:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  80051a:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  800521:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800528:	8b 45 10             	mov    0x10(%ebp),%eax
  80052b:	8d 50 01             	lea    0x1(%eax),%edx
  80052e:	89 55 10             	mov    %edx,0x10(%ebp)
  800531:	0f b6 00             	movzbl (%eax),%eax
  800534:	0f b6 d8             	movzbl %al,%ebx
  800537:	8d 43 dd             	lea    -0x23(%ebx),%eax
  80053a:	83 f8 55             	cmp    $0x55,%eax
  80053d:	0f 87 78 03 00 00    	ja     8008bb <vprintfmt+0x3e9>
  800543:	8b 04 85 44 17 80 00 	mov    0x801744(,%eax,4),%eax
  80054a:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  80054c:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  800550:	eb d6                	jmp    800528 <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800552:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  800556:	eb d0                	jmp    800528 <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800558:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  80055f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800562:	89 d0                	mov    %edx,%eax
  800564:	c1 e0 02             	shl    $0x2,%eax
  800567:	01 d0                	add    %edx,%eax
  800569:	01 c0                	add    %eax,%eax
  80056b:	01 d8                	add    %ebx,%eax
  80056d:	83 e8 30             	sub    $0x30,%eax
  800570:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  800573:	8b 45 10             	mov    0x10(%ebp),%eax
  800576:	0f b6 00             	movzbl (%eax),%eax
  800579:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  80057c:	83 fb 2f             	cmp    $0x2f,%ebx
  80057f:	7e 0b                	jle    80058c <vprintfmt+0xba>
  800581:	83 fb 39             	cmp    $0x39,%ebx
  800584:	7f 06                	jg     80058c <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800586:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80058a:	eb d3                	jmp    80055f <vprintfmt+0x8d>
			goto process_precision;
  80058c:	eb 39                	jmp    8005c7 <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
  80058e:	8b 45 14             	mov    0x14(%ebp),%eax
  800591:	83 c0 04             	add    $0x4,%eax
  800594:	89 45 14             	mov    %eax,0x14(%ebp)
  800597:	8b 45 14             	mov    0x14(%ebp),%eax
  80059a:	83 e8 04             	sub    $0x4,%eax
  80059d:	8b 00                	mov    (%eax),%eax
  80059f:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  8005a2:	eb 23                	jmp    8005c7 <vprintfmt+0xf5>

		case '.':
			if (width < 0)
  8005a4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005a8:	79 0c                	jns    8005b6 <vprintfmt+0xe4>
				width = 0;
  8005aa:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  8005b1:	e9 72 ff ff ff       	jmp    800528 <vprintfmt+0x56>
  8005b6:	e9 6d ff ff ff       	jmp    800528 <vprintfmt+0x56>

		case '#':
			altflag = 1;
  8005bb:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  8005c2:	e9 61 ff ff ff       	jmp    800528 <vprintfmt+0x56>

		process_precision:
			if (width < 0)
  8005c7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005cb:	79 12                	jns    8005df <vprintfmt+0x10d>
				width = precision, precision = -1;
  8005cd:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8005d0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005d3:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  8005da:	e9 49 ff ff ff       	jmp    800528 <vprintfmt+0x56>
  8005df:	e9 44 ff ff ff       	jmp    800528 <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8005e4:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  8005e8:	e9 3b ff ff ff       	jmp    800528 <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8005ed:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f0:	83 c0 04             	add    $0x4,%eax
  8005f3:	89 45 14             	mov    %eax,0x14(%ebp)
  8005f6:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f9:	83 e8 04             	sub    $0x4,%eax
  8005fc:	8b 00                	mov    (%eax),%eax
  8005fe:	8b 55 0c             	mov    0xc(%ebp),%edx
  800601:	89 54 24 04          	mov    %edx,0x4(%esp)
  800605:	89 04 24             	mov    %eax,(%esp)
  800608:	8b 45 08             	mov    0x8(%ebp),%eax
  80060b:	ff d0                	call   *%eax
			break;
  80060d:	e9 d4 02 00 00       	jmp    8008e6 <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800612:	8b 45 14             	mov    0x14(%ebp),%eax
  800615:	83 c0 04             	add    $0x4,%eax
  800618:	89 45 14             	mov    %eax,0x14(%ebp)
  80061b:	8b 45 14             	mov    0x14(%ebp),%eax
  80061e:	83 e8 04             	sub    $0x4,%eax
  800621:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  800623:	85 db                	test   %ebx,%ebx
  800625:	79 02                	jns    800629 <vprintfmt+0x157>
				err = -err;
  800627:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  800629:	83 fb 07             	cmp    $0x7,%ebx
  80062c:	7f 0b                	jg     800639 <vprintfmt+0x167>
  80062e:	8b 34 9d 00 17 80 00 	mov    0x801700(,%ebx,4),%esi
  800635:	85 f6                	test   %esi,%esi
  800637:	75 23                	jne    80065c <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
  800639:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  80063d:	c7 44 24 08 31 17 80 	movl   $0x801731,0x8(%esp)
  800644:	00 
  800645:	8b 45 0c             	mov    0xc(%ebp),%eax
  800648:	89 44 24 04          	mov    %eax,0x4(%esp)
  80064c:	8b 45 08             	mov    0x8(%ebp),%eax
  80064f:	89 04 24             	mov    %eax,(%esp)
  800652:	e8 9c 02 00 00       	call   8008f3 <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
  800657:	e9 8a 02 00 00       	jmp    8008e6 <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  80065c:	89 74 24 0c          	mov    %esi,0xc(%esp)
  800660:	c7 44 24 08 3a 17 80 	movl   $0x80173a,0x8(%esp)
  800667:	00 
  800668:	8b 45 0c             	mov    0xc(%ebp),%eax
  80066b:	89 44 24 04          	mov    %eax,0x4(%esp)
  80066f:	8b 45 08             	mov    0x8(%ebp),%eax
  800672:	89 04 24             	mov    %eax,(%esp)
  800675:	e8 79 02 00 00       	call   8008f3 <printfmt>
			break;
  80067a:	e9 67 02 00 00       	jmp    8008e6 <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80067f:	8b 45 14             	mov    0x14(%ebp),%eax
  800682:	83 c0 04             	add    $0x4,%eax
  800685:	89 45 14             	mov    %eax,0x14(%ebp)
  800688:	8b 45 14             	mov    0x14(%ebp),%eax
  80068b:	83 e8 04             	sub    $0x4,%eax
  80068e:	8b 30                	mov    (%eax),%esi
  800690:	85 f6                	test   %esi,%esi
  800692:	75 05                	jne    800699 <vprintfmt+0x1c7>
				p = "(null)";
  800694:	be 3d 17 80 00       	mov    $0x80173d,%esi
			if (width > 0 && padc != '-')
  800699:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80069d:	7e 37                	jle    8006d6 <vprintfmt+0x204>
  80069f:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  8006a3:	74 31                	je     8006d6 <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
  8006a5:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8006a8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006ac:	89 34 24             	mov    %esi,(%esp)
  8006af:	e8 62 03 00 00       	call   800a16 <strnlen>
  8006b4:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  8006b7:	eb 17                	jmp    8006d0 <vprintfmt+0x1fe>
					putch(padc, putdat);
  8006b9:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  8006bd:	8b 55 0c             	mov    0xc(%ebp),%edx
  8006c0:	89 54 24 04          	mov    %edx,0x4(%esp)
  8006c4:	89 04 24             	mov    %eax,(%esp)
  8006c7:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ca:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8006cc:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8006d0:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006d4:	7f e3                	jg     8006b9 <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8006d6:	eb 38                	jmp    800710 <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
  8006d8:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006dc:	74 1f                	je     8006fd <vprintfmt+0x22b>
  8006de:	83 fb 1f             	cmp    $0x1f,%ebx
  8006e1:	7e 05                	jle    8006e8 <vprintfmt+0x216>
  8006e3:	83 fb 7e             	cmp    $0x7e,%ebx
  8006e6:	7e 15                	jle    8006fd <vprintfmt+0x22b>
					putch('?', putdat);
  8006e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006eb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006ef:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  8006f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006f9:	ff d0                	call   *%eax
  8006fb:	eb 0f                	jmp    80070c <vprintfmt+0x23a>
				else
					putch(ch, putdat);
  8006fd:	8b 45 0c             	mov    0xc(%ebp),%eax
  800700:	89 44 24 04          	mov    %eax,0x4(%esp)
  800704:	89 1c 24             	mov    %ebx,(%esp)
  800707:	8b 45 08             	mov    0x8(%ebp),%eax
  80070a:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80070c:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800710:	89 f0                	mov    %esi,%eax
  800712:	8d 70 01             	lea    0x1(%eax),%esi
  800715:	0f b6 00             	movzbl (%eax),%eax
  800718:	0f be d8             	movsbl %al,%ebx
  80071b:	85 db                	test   %ebx,%ebx
  80071d:	74 10                	je     80072f <vprintfmt+0x25d>
  80071f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800723:	78 b3                	js     8006d8 <vprintfmt+0x206>
  800725:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  800729:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  80072d:	79 a9                	jns    8006d8 <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80072f:	eb 17                	jmp    800748 <vprintfmt+0x276>
				putch(' ', putdat);
  800731:	8b 45 0c             	mov    0xc(%ebp),%eax
  800734:	89 44 24 04          	mov    %eax,0x4(%esp)
  800738:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  80073f:	8b 45 08             	mov    0x8(%ebp),%eax
  800742:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800744:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800748:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80074c:	7f e3                	jg     800731 <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
  80074e:	e9 93 01 00 00       	jmp    8008e6 <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800753:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800756:	89 44 24 04          	mov    %eax,0x4(%esp)
  80075a:	8d 45 14             	lea    0x14(%ebp),%eax
  80075d:	89 04 24             	mov    %eax,(%esp)
  800760:	e8 0e fd ff ff       	call   800473 <getint>
  800765:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800768:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  80076b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80076e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800771:	85 d2                	test   %edx,%edx
  800773:	79 26                	jns    80079b <vprintfmt+0x2c9>
				putch('-', putdat);
  800775:	8b 45 0c             	mov    0xc(%ebp),%eax
  800778:	89 44 24 04          	mov    %eax,0x4(%esp)
  80077c:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  800783:	8b 45 08             	mov    0x8(%ebp),%eax
  800786:	ff d0                	call   *%eax
				num = -(long long) num;
  800788:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80078b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80078e:	f7 d8                	neg    %eax
  800790:	83 d2 00             	adc    $0x0,%edx
  800793:	f7 da                	neg    %edx
  800795:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800798:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  80079b:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8007a2:	e9 cb 00 00 00       	jmp    800872 <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8007a7:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8007aa:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007ae:	8d 45 14             	lea    0x14(%ebp),%eax
  8007b1:	89 04 24             	mov    %eax,(%esp)
  8007b4:	e8 53 fc ff ff       	call   80040c <getuint>
  8007b9:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007bc:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  8007bf:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8007c6:	e9 a7 00 00 00       	jmp    800872 <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  8007cb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ce:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007d2:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8007d9:	8b 45 08             	mov    0x8(%ebp),%eax
  8007dc:	ff d0                	call   *%eax
			putch('X', putdat);
  8007de:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007e1:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007e5:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8007ec:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ef:	ff d0                	call   *%eax
			putch('X', putdat);
  8007f1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007f4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007f8:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8007ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800802:	ff d0                	call   *%eax
			break;
  800804:	e9 dd 00 00 00       	jmp    8008e6 <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
  800809:	8b 45 0c             	mov    0xc(%ebp),%eax
  80080c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800810:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  800817:	8b 45 08             	mov    0x8(%ebp),%eax
  80081a:	ff d0                	call   *%eax
			putch('x', putdat);
  80081c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80081f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800823:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  80082a:	8b 45 08             	mov    0x8(%ebp),%eax
  80082d:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  80082f:	8b 45 14             	mov    0x14(%ebp),%eax
  800832:	83 c0 04             	add    $0x4,%eax
  800835:	89 45 14             	mov    %eax,0x14(%ebp)
  800838:	8b 45 14             	mov    0x14(%ebp),%eax
  80083b:	83 e8 04             	sub    $0x4,%eax
  80083e:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800840:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800843:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  80084a:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  800851:	eb 1f                	jmp    800872 <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800853:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800856:	89 44 24 04          	mov    %eax,0x4(%esp)
  80085a:	8d 45 14             	lea    0x14(%ebp),%eax
  80085d:	89 04 24             	mov    %eax,(%esp)
  800860:	e8 a7 fb ff ff       	call   80040c <getuint>
  800865:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800868:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  80086b:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  800872:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  800876:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800879:	89 54 24 18          	mov    %edx,0x18(%esp)
  80087d:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  800880:	89 54 24 14          	mov    %edx,0x14(%esp)
  800884:	89 44 24 10          	mov    %eax,0x10(%esp)
  800888:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80088b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80088e:	89 44 24 08          	mov    %eax,0x8(%esp)
  800892:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800896:	8b 45 0c             	mov    0xc(%ebp),%eax
  800899:	89 44 24 04          	mov    %eax,0x4(%esp)
  80089d:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a0:	89 04 24             	mov    %eax,(%esp)
  8008a3:	e8 86 fa ff ff       	call   80032e <printnum>
			break;
  8008a8:	eb 3c                	jmp    8008e6 <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8008aa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008ad:	89 44 24 04          	mov    %eax,0x4(%esp)
  8008b1:	89 1c 24             	mov    %ebx,(%esp)
  8008b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b7:	ff d0                	call   *%eax
			break;
  8008b9:	eb 2b                	jmp    8008e6 <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8008bb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008be:	89 44 24 04          	mov    %eax,0x4(%esp)
  8008c2:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  8008c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8008cc:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
  8008ce:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8008d2:	eb 04                	jmp    8008d8 <vprintfmt+0x406>
  8008d4:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8008d8:	8b 45 10             	mov    0x10(%ebp),%eax
  8008db:	83 e8 01             	sub    $0x1,%eax
  8008de:	0f b6 00             	movzbl (%eax),%eax
  8008e1:	3c 25                	cmp    $0x25,%al
  8008e3:	75 ef                	jne    8008d4 <vprintfmt+0x402>
				/* do nothing */;
			break;
  8008e5:	90                   	nop
		}
	}
  8008e6:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8008e7:	e9 08 fc ff ff       	jmp    8004f4 <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  8008ec:	83 c4 40             	add    $0x40,%esp
  8008ef:	5b                   	pop    %ebx
  8008f0:	5e                   	pop    %esi
  8008f1:	5d                   	pop    %ebp
  8008f2:	c3                   	ret    

008008f3 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8008f3:	55                   	push   %ebp
  8008f4:	89 e5                	mov    %esp,%ebp
  8008f6:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  8008f9:	8d 45 10             	lea    0x10(%ebp),%eax
  8008fc:	83 c0 04             	add    $0x4,%eax
  8008ff:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  800902:	8b 45 10             	mov    0x10(%ebp),%eax
  800905:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800908:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80090c:	89 44 24 08          	mov    %eax,0x8(%esp)
  800910:	8b 45 0c             	mov    0xc(%ebp),%eax
  800913:	89 44 24 04          	mov    %eax,0x4(%esp)
  800917:	8b 45 08             	mov    0x8(%ebp),%eax
  80091a:	89 04 24             	mov    %eax,(%esp)
  80091d:	e8 b0 fb ff ff       	call   8004d2 <vprintfmt>
	va_end(ap);
}
  800922:	c9                   	leave  
  800923:	c3                   	ret    

00800924 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800924:	55                   	push   %ebp
  800925:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800927:	8b 45 0c             	mov    0xc(%ebp),%eax
  80092a:	8b 40 08             	mov    0x8(%eax),%eax
  80092d:	8d 50 01             	lea    0x1(%eax),%edx
  800930:	8b 45 0c             	mov    0xc(%ebp),%eax
  800933:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800936:	8b 45 0c             	mov    0xc(%ebp),%eax
  800939:	8b 10                	mov    (%eax),%edx
  80093b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80093e:	8b 40 04             	mov    0x4(%eax),%eax
  800941:	39 c2                	cmp    %eax,%edx
  800943:	73 12                	jae    800957 <sprintputch+0x33>
		*b->buf++ = ch;
  800945:	8b 45 0c             	mov    0xc(%ebp),%eax
  800948:	8b 00                	mov    (%eax),%eax
  80094a:	8d 48 01             	lea    0x1(%eax),%ecx
  80094d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800950:	89 0a                	mov    %ecx,(%edx)
  800952:	8b 55 08             	mov    0x8(%ebp),%edx
  800955:	88 10                	mov    %dl,(%eax)
}
  800957:	5d                   	pop    %ebp
  800958:	c3                   	ret    

00800959 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800959:	55                   	push   %ebp
  80095a:	89 e5                	mov    %esp,%ebp
  80095c:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  80095f:	8b 45 08             	mov    0x8(%ebp),%eax
  800962:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800965:	8b 45 0c             	mov    0xc(%ebp),%eax
  800968:	8d 50 ff             	lea    -0x1(%eax),%edx
  80096b:	8b 45 08             	mov    0x8(%ebp),%eax
  80096e:	01 d0                	add    %edx,%eax
  800970:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800973:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80097a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  80097e:	74 06                	je     800986 <vsnprintf+0x2d>
  800980:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800984:	7f 07                	jg     80098d <vsnprintf+0x34>
		return -E_INVAL;
  800986:	b8 03 00 00 00       	mov    $0x3,%eax
  80098b:	eb 2a                	jmp    8009b7 <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80098d:	8b 45 14             	mov    0x14(%ebp),%eax
  800990:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800994:	8b 45 10             	mov    0x10(%ebp),%eax
  800997:	89 44 24 08          	mov    %eax,0x8(%esp)
  80099b:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80099e:	89 44 24 04          	mov    %eax,0x4(%esp)
  8009a2:	c7 04 24 24 09 80 00 	movl   $0x800924,(%esp)
  8009a9:	e8 24 fb ff ff       	call   8004d2 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  8009ae:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8009b1:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8009b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  8009b7:	c9                   	leave  
  8009b8:	c3                   	ret    

008009b9 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8009b9:	55                   	push   %ebp
  8009ba:	89 e5                	mov    %esp,%ebp
  8009bc:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8009bf:	8d 45 10             	lea    0x10(%ebp),%eax
  8009c2:	83 c0 04             	add    $0x4,%eax
  8009c5:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  8009c8:	8b 45 10             	mov    0x10(%ebp),%eax
  8009cb:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8009ce:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8009d2:	89 44 24 08          	mov    %eax,0x8(%esp)
  8009d6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009d9:	89 44 24 04          	mov    %eax,0x4(%esp)
  8009dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e0:	89 04 24             	mov    %eax,(%esp)
  8009e3:	e8 71 ff ff ff       	call   800959 <vsnprintf>
  8009e8:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  8009eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8009ee:	c9                   	leave  
  8009ef:	c3                   	ret    

008009f0 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  8009f0:	55                   	push   %ebp
  8009f1:	89 e5                	mov    %esp,%ebp
  8009f3:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  8009f6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8009fd:	eb 08                	jmp    800a07 <strlen+0x17>
		n++;
  8009ff:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800a03:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a07:	8b 45 08             	mov    0x8(%ebp),%eax
  800a0a:	0f b6 00             	movzbl (%eax),%eax
  800a0d:	84 c0                	test   %al,%al
  800a0f:	75 ee                	jne    8009ff <strlen+0xf>
		n++;
	return n;
  800a11:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800a14:	c9                   	leave  
  800a15:	c3                   	ret    

00800a16 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  800a16:	55                   	push   %ebp
  800a17:	89 e5                	mov    %esp,%ebp
  800a19:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800a1c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800a23:	eb 0c                	jmp    800a31 <strnlen+0x1b>
		n++;
  800a25:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800a29:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a2d:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  800a31:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a35:	74 0a                	je     800a41 <strnlen+0x2b>
  800a37:	8b 45 08             	mov    0x8(%ebp),%eax
  800a3a:	0f b6 00             	movzbl (%eax),%eax
  800a3d:	84 c0                	test   %al,%al
  800a3f:	75 e4                	jne    800a25 <strnlen+0xf>
		n++;
	return n;
  800a41:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800a44:	c9                   	leave  
  800a45:	c3                   	ret    

00800a46 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800a46:	55                   	push   %ebp
  800a47:	89 e5                	mov    %esp,%ebp
  800a49:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800a4c:	8b 45 08             	mov    0x8(%ebp),%eax
  800a4f:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800a52:	90                   	nop
  800a53:	8b 45 08             	mov    0x8(%ebp),%eax
  800a56:	8d 50 01             	lea    0x1(%eax),%edx
  800a59:	89 55 08             	mov    %edx,0x8(%ebp)
  800a5c:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a5f:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a62:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800a65:	0f b6 12             	movzbl (%edx),%edx
  800a68:	88 10                	mov    %dl,(%eax)
  800a6a:	0f b6 00             	movzbl (%eax),%eax
  800a6d:	84 c0                	test   %al,%al
  800a6f:	75 e2                	jne    800a53 <strcpy+0xd>
		/* do nothing */;
	return ret;
  800a71:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800a74:	c9                   	leave  
  800a75:	c3                   	ret    

00800a76 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  800a76:	55                   	push   %ebp
  800a77:	89 e5                	mov    %esp,%ebp
  800a79:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  800a7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7f:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800a82:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800a89:	eb 23                	jmp    800aae <strncpy+0x38>
		*dst++ = *src;
  800a8b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a8e:	8d 50 01             	lea    0x1(%eax),%edx
  800a91:	89 55 08             	mov    %edx,0x8(%ebp)
  800a94:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a97:	0f b6 12             	movzbl (%edx),%edx
  800a9a:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  800a9c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a9f:	0f b6 00             	movzbl (%eax),%eax
  800aa2:	84 c0                	test   %al,%al
  800aa4:	74 04                	je     800aaa <strncpy+0x34>
			src++;
  800aa6:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800aaa:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800aae:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ab1:	3b 45 10             	cmp    0x10(%ebp),%eax
  800ab4:	72 d5                	jb     800a8b <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800ab6:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800ab9:	c9                   	leave  
  800aba:	c3                   	ret    

00800abb <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800abb:	55                   	push   %ebp
  800abc:	89 e5                	mov    %esp,%ebp
  800abe:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800ac1:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac4:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800ac7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800acb:	74 33                	je     800b00 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800acd:	eb 17                	jmp    800ae6 <strlcpy+0x2b>
			*dst++ = *src++;
  800acf:	8b 45 08             	mov    0x8(%ebp),%eax
  800ad2:	8d 50 01             	lea    0x1(%eax),%edx
  800ad5:	89 55 08             	mov    %edx,0x8(%ebp)
  800ad8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800adb:	8d 4a 01             	lea    0x1(%edx),%ecx
  800ade:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800ae1:	0f b6 12             	movzbl (%edx),%edx
  800ae4:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800ae6:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800aea:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800aee:	74 0a                	je     800afa <strlcpy+0x3f>
  800af0:	8b 45 0c             	mov    0xc(%ebp),%eax
  800af3:	0f b6 00             	movzbl (%eax),%eax
  800af6:	84 c0                	test   %al,%al
  800af8:	75 d5                	jne    800acf <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800afa:	8b 45 08             	mov    0x8(%ebp),%eax
  800afd:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  800b00:	8b 55 08             	mov    0x8(%ebp),%edx
  800b03:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b06:	29 c2                	sub    %eax,%edx
  800b08:	89 d0                	mov    %edx,%eax
}
  800b0a:	c9                   	leave  
  800b0b:	c3                   	ret    

00800b0c <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800b0c:	55                   	push   %ebp
  800b0d:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800b0f:	eb 08                	jmp    800b19 <strcmp+0xd>
		p++, q++;
  800b11:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b15:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800b19:	8b 45 08             	mov    0x8(%ebp),%eax
  800b1c:	0f b6 00             	movzbl (%eax),%eax
  800b1f:	84 c0                	test   %al,%al
  800b21:	74 10                	je     800b33 <strcmp+0x27>
  800b23:	8b 45 08             	mov    0x8(%ebp),%eax
  800b26:	0f b6 10             	movzbl (%eax),%edx
  800b29:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b2c:	0f b6 00             	movzbl (%eax),%eax
  800b2f:	38 c2                	cmp    %al,%dl
  800b31:	74 de                	je     800b11 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800b33:	8b 45 08             	mov    0x8(%ebp),%eax
  800b36:	0f b6 00             	movzbl (%eax),%eax
  800b39:	0f b6 d0             	movzbl %al,%edx
  800b3c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b3f:	0f b6 00             	movzbl (%eax),%eax
  800b42:	0f b6 c0             	movzbl %al,%eax
  800b45:	29 c2                	sub    %eax,%edx
  800b47:	89 d0                	mov    %edx,%eax
}
  800b49:	5d                   	pop    %ebp
  800b4a:	c3                   	ret    

00800b4b <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800b4b:	55                   	push   %ebp
  800b4c:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800b4e:	eb 0c                	jmp    800b5c <strncmp+0x11>
		n--, p++, q++;
  800b50:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800b54:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b58:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800b5c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b60:	74 1a                	je     800b7c <strncmp+0x31>
  800b62:	8b 45 08             	mov    0x8(%ebp),%eax
  800b65:	0f b6 00             	movzbl (%eax),%eax
  800b68:	84 c0                	test   %al,%al
  800b6a:	74 10                	je     800b7c <strncmp+0x31>
  800b6c:	8b 45 08             	mov    0x8(%ebp),%eax
  800b6f:	0f b6 10             	movzbl (%eax),%edx
  800b72:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b75:	0f b6 00             	movzbl (%eax),%eax
  800b78:	38 c2                	cmp    %al,%dl
  800b7a:	74 d4                	je     800b50 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800b7c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b80:	75 07                	jne    800b89 <strncmp+0x3e>
		return 0;
  800b82:	b8 00 00 00 00       	mov    $0x0,%eax
  800b87:	eb 16                	jmp    800b9f <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800b89:	8b 45 08             	mov    0x8(%ebp),%eax
  800b8c:	0f b6 00             	movzbl (%eax),%eax
  800b8f:	0f b6 d0             	movzbl %al,%edx
  800b92:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b95:	0f b6 00             	movzbl (%eax),%eax
  800b98:	0f b6 c0             	movzbl %al,%eax
  800b9b:	29 c2                	sub    %eax,%edx
  800b9d:	89 d0                	mov    %edx,%eax
}
  800b9f:	5d                   	pop    %ebp
  800ba0:	c3                   	ret    

00800ba1 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800ba1:	55                   	push   %ebp
  800ba2:	89 e5                	mov    %esp,%ebp
  800ba4:	83 ec 04             	sub    $0x4,%esp
  800ba7:	8b 45 0c             	mov    0xc(%ebp),%eax
  800baa:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800bad:	eb 14                	jmp    800bc3 <strchr+0x22>
		if (*s == c)
  800baf:	8b 45 08             	mov    0x8(%ebp),%eax
  800bb2:	0f b6 00             	movzbl (%eax),%eax
  800bb5:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800bb8:	75 05                	jne    800bbf <strchr+0x1e>
			return (char *) s;
  800bba:	8b 45 08             	mov    0x8(%ebp),%eax
  800bbd:	eb 13                	jmp    800bd2 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800bbf:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bc3:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc6:	0f b6 00             	movzbl (%eax),%eax
  800bc9:	84 c0                	test   %al,%al
  800bcb:	75 e2                	jne    800baf <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800bcd:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bd2:	c9                   	leave  
  800bd3:	c3                   	ret    

00800bd4 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800bd4:	55                   	push   %ebp
  800bd5:	89 e5                	mov    %esp,%ebp
  800bd7:	83 ec 04             	sub    $0x4,%esp
  800bda:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bdd:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800be0:	eb 11                	jmp    800bf3 <strfind+0x1f>
		if (*s == c)
  800be2:	8b 45 08             	mov    0x8(%ebp),%eax
  800be5:	0f b6 00             	movzbl (%eax),%eax
  800be8:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800beb:	75 02                	jne    800bef <strfind+0x1b>
			break;
  800bed:	eb 0e                	jmp    800bfd <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800bef:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bf3:	8b 45 08             	mov    0x8(%ebp),%eax
  800bf6:	0f b6 00             	movzbl (%eax),%eax
  800bf9:	84 c0                	test   %al,%al
  800bfb:	75 e5                	jne    800be2 <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
  800bfd:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c00:	c9                   	leave  
  800c01:	c3                   	ret    

00800c02 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800c02:	55                   	push   %ebp
  800c03:	89 e5                	mov    %esp,%ebp
  800c05:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800c08:	8b 45 08             	mov    0x8(%ebp),%eax
  800c0b:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800c0e:	8b 45 10             	mov    0x10(%ebp),%eax
  800c11:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800c14:	eb 0e                	jmp    800c24 <memset+0x22>
		*p++ = c;
  800c16:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c19:	8d 50 01             	lea    0x1(%eax),%edx
  800c1c:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800c1f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800c22:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800c24:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800c28:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800c2c:	79 e8                	jns    800c16 <memset+0x14>
		*p++ = c;

	return v;
  800c2e:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c31:	c9                   	leave  
  800c32:	c3                   	ret    

00800c33 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800c33:	55                   	push   %ebp
  800c34:	89 e5                	mov    %esp,%ebp
  800c36:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800c39:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c3c:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800c3f:	8b 45 08             	mov    0x8(%ebp),%eax
  800c42:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800c45:	eb 17                	jmp    800c5e <memcpy+0x2b>
		*d++ = *s++;
  800c47:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c4a:	8d 50 01             	lea    0x1(%eax),%edx
  800c4d:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800c50:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c53:	8d 4a 01             	lea    0x1(%edx),%ecx
  800c56:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800c59:	0f b6 12             	movzbl (%edx),%edx
  800c5c:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800c5e:	8b 45 10             	mov    0x10(%ebp),%eax
  800c61:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c64:	89 55 10             	mov    %edx,0x10(%ebp)
  800c67:	85 c0                	test   %eax,%eax
  800c69:	75 dc                	jne    800c47 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800c6b:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c6e:	c9                   	leave  
  800c6f:	c3                   	ret    

00800c70 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800c70:	55                   	push   %ebp
  800c71:	89 e5                	mov    %esp,%ebp
  800c73:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800c76:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c79:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800c7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7f:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800c82:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c85:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c88:	73 3d                	jae    800cc7 <memmove+0x57>
  800c8a:	8b 45 10             	mov    0x10(%ebp),%eax
  800c8d:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c90:	01 d0                	add    %edx,%eax
  800c92:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c95:	76 30                	jbe    800cc7 <memmove+0x57>
		s += n;
  800c97:	8b 45 10             	mov    0x10(%ebp),%eax
  800c9a:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800c9d:	8b 45 10             	mov    0x10(%ebp),%eax
  800ca0:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800ca3:	eb 13                	jmp    800cb8 <memmove+0x48>
			*--d = *--s;
  800ca5:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800ca9:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800cad:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800cb0:	0f b6 10             	movzbl (%eax),%edx
  800cb3:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cb6:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800cb8:	8b 45 10             	mov    0x10(%ebp),%eax
  800cbb:	8d 50 ff             	lea    -0x1(%eax),%edx
  800cbe:	89 55 10             	mov    %edx,0x10(%ebp)
  800cc1:	85 c0                	test   %eax,%eax
  800cc3:	75 e0                	jne    800ca5 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800cc5:	eb 26                	jmp    800ced <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800cc7:	eb 17                	jmp    800ce0 <memmove+0x70>
			*d++ = *s++;
  800cc9:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ccc:	8d 50 01             	lea    0x1(%eax),%edx
  800ccf:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800cd2:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800cd5:	8d 4a 01             	lea    0x1(%edx),%ecx
  800cd8:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800cdb:	0f b6 12             	movzbl (%edx),%edx
  800cde:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800ce0:	8b 45 10             	mov    0x10(%ebp),%eax
  800ce3:	8d 50 ff             	lea    -0x1(%eax),%edx
  800ce6:	89 55 10             	mov    %edx,0x10(%ebp)
  800ce9:	85 c0                	test   %eax,%eax
  800ceb:	75 dc                	jne    800cc9 <memmove+0x59>
			*d++ = *s++;

	return dst;
  800ced:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800cf0:	c9                   	leave  
  800cf1:	c3                   	ret    

00800cf2 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800cf2:	55                   	push   %ebp
  800cf3:	89 e5                	mov    %esp,%ebp
  800cf5:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800cf8:	8b 45 08             	mov    0x8(%ebp),%eax
  800cfb:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800cfe:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d01:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800d04:	eb 30                	jmp    800d36 <memcmp+0x44>
		if (*s1 != *s2)
  800d06:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800d09:	0f b6 10             	movzbl (%eax),%edx
  800d0c:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d0f:	0f b6 00             	movzbl (%eax),%eax
  800d12:	38 c2                	cmp    %al,%dl
  800d14:	74 18                	je     800d2e <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800d16:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800d19:	0f b6 00             	movzbl (%eax),%eax
  800d1c:	0f b6 d0             	movzbl %al,%edx
  800d1f:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d22:	0f b6 00             	movzbl (%eax),%eax
  800d25:	0f b6 c0             	movzbl %al,%eax
  800d28:	29 c2                	sub    %eax,%edx
  800d2a:	89 d0                	mov    %edx,%eax
  800d2c:	eb 1a                	jmp    800d48 <memcmp+0x56>
		s1++, s2++;
  800d2e:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800d32:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800d36:	8b 45 10             	mov    0x10(%ebp),%eax
  800d39:	8d 50 ff             	lea    -0x1(%eax),%edx
  800d3c:	89 55 10             	mov    %edx,0x10(%ebp)
  800d3f:	85 c0                	test   %eax,%eax
  800d41:	75 c3                	jne    800d06 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800d43:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800d48:	c9                   	leave  
  800d49:	c3                   	ret    

00800d4a <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800d4a:	55                   	push   %ebp
  800d4b:	89 e5                	mov    %esp,%ebp
  800d4d:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800d50:	8b 45 10             	mov    0x10(%ebp),%eax
  800d53:	8b 55 08             	mov    0x8(%ebp),%edx
  800d56:	01 d0                	add    %edx,%eax
  800d58:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800d5b:	eb 13                	jmp    800d70 <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
  800d5d:	8b 45 08             	mov    0x8(%ebp),%eax
  800d60:	0f b6 10             	movzbl (%eax),%edx
  800d63:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d66:	38 c2                	cmp    %al,%dl
  800d68:	75 02                	jne    800d6c <memfind+0x22>
			break;
  800d6a:	eb 0c                	jmp    800d78 <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d6c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d70:	8b 45 08             	mov    0x8(%ebp),%eax
  800d73:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800d76:	72 e5                	jb     800d5d <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
  800d78:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800d7b:	c9                   	leave  
  800d7c:	c3                   	ret    

00800d7d <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800d7d:	55                   	push   %ebp
  800d7e:	89 e5                	mov    %esp,%ebp
  800d80:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800d83:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800d8a:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d91:	eb 04                	jmp    800d97 <strtol+0x1a>
		s++;
  800d93:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d97:	8b 45 08             	mov    0x8(%ebp),%eax
  800d9a:	0f b6 00             	movzbl (%eax),%eax
  800d9d:	3c 20                	cmp    $0x20,%al
  800d9f:	74 f2                	je     800d93 <strtol+0x16>
  800da1:	8b 45 08             	mov    0x8(%ebp),%eax
  800da4:	0f b6 00             	movzbl (%eax),%eax
  800da7:	3c 09                	cmp    $0x9,%al
  800da9:	74 e8                	je     800d93 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800dab:	8b 45 08             	mov    0x8(%ebp),%eax
  800dae:	0f b6 00             	movzbl (%eax),%eax
  800db1:	3c 2b                	cmp    $0x2b,%al
  800db3:	75 06                	jne    800dbb <strtol+0x3e>
		s++;
  800db5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800db9:	eb 15                	jmp    800dd0 <strtol+0x53>
	else if (*s == '-')
  800dbb:	8b 45 08             	mov    0x8(%ebp),%eax
  800dbe:	0f b6 00             	movzbl (%eax),%eax
  800dc1:	3c 2d                	cmp    $0x2d,%al
  800dc3:	75 0b                	jne    800dd0 <strtol+0x53>
		s++, neg = 1;
  800dc5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800dc9:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800dd0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800dd4:	74 06                	je     800ddc <strtol+0x5f>
  800dd6:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800dda:	75 24                	jne    800e00 <strtol+0x83>
  800ddc:	8b 45 08             	mov    0x8(%ebp),%eax
  800ddf:	0f b6 00             	movzbl (%eax),%eax
  800de2:	3c 30                	cmp    $0x30,%al
  800de4:	75 1a                	jne    800e00 <strtol+0x83>
  800de6:	8b 45 08             	mov    0x8(%ebp),%eax
  800de9:	83 c0 01             	add    $0x1,%eax
  800dec:	0f b6 00             	movzbl (%eax),%eax
  800def:	3c 78                	cmp    $0x78,%al
  800df1:	75 0d                	jne    800e00 <strtol+0x83>
		s += 2, base = 16;
  800df3:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800df7:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800dfe:	eb 2a                	jmp    800e2a <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800e00:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800e04:	75 17                	jne    800e1d <strtol+0xa0>
  800e06:	8b 45 08             	mov    0x8(%ebp),%eax
  800e09:	0f b6 00             	movzbl (%eax),%eax
  800e0c:	3c 30                	cmp    $0x30,%al
  800e0e:	75 0d                	jne    800e1d <strtol+0xa0>
		s++, base = 8;
  800e10:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800e14:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800e1b:	eb 0d                	jmp    800e2a <strtol+0xad>
	else if (base == 0)
  800e1d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800e21:	75 07                	jne    800e2a <strtol+0xad>
		base = 10;
  800e23:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800e2a:	8b 45 08             	mov    0x8(%ebp),%eax
  800e2d:	0f b6 00             	movzbl (%eax),%eax
  800e30:	3c 2f                	cmp    $0x2f,%al
  800e32:	7e 1b                	jle    800e4f <strtol+0xd2>
  800e34:	8b 45 08             	mov    0x8(%ebp),%eax
  800e37:	0f b6 00             	movzbl (%eax),%eax
  800e3a:	3c 39                	cmp    $0x39,%al
  800e3c:	7f 11                	jg     800e4f <strtol+0xd2>
			dig = *s - '0';
  800e3e:	8b 45 08             	mov    0x8(%ebp),%eax
  800e41:	0f b6 00             	movzbl (%eax),%eax
  800e44:	0f be c0             	movsbl %al,%eax
  800e47:	83 e8 30             	sub    $0x30,%eax
  800e4a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800e4d:	eb 48                	jmp    800e97 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800e4f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e52:	0f b6 00             	movzbl (%eax),%eax
  800e55:	3c 60                	cmp    $0x60,%al
  800e57:	7e 1b                	jle    800e74 <strtol+0xf7>
  800e59:	8b 45 08             	mov    0x8(%ebp),%eax
  800e5c:	0f b6 00             	movzbl (%eax),%eax
  800e5f:	3c 7a                	cmp    $0x7a,%al
  800e61:	7f 11                	jg     800e74 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800e63:	8b 45 08             	mov    0x8(%ebp),%eax
  800e66:	0f b6 00             	movzbl (%eax),%eax
  800e69:	0f be c0             	movsbl %al,%eax
  800e6c:	83 e8 57             	sub    $0x57,%eax
  800e6f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800e72:	eb 23                	jmp    800e97 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800e74:	8b 45 08             	mov    0x8(%ebp),%eax
  800e77:	0f b6 00             	movzbl (%eax),%eax
  800e7a:	3c 40                	cmp    $0x40,%al
  800e7c:	7e 3d                	jle    800ebb <strtol+0x13e>
  800e7e:	8b 45 08             	mov    0x8(%ebp),%eax
  800e81:	0f b6 00             	movzbl (%eax),%eax
  800e84:	3c 5a                	cmp    $0x5a,%al
  800e86:	7f 33                	jg     800ebb <strtol+0x13e>
			dig = *s - 'A' + 10;
  800e88:	8b 45 08             	mov    0x8(%ebp),%eax
  800e8b:	0f b6 00             	movzbl (%eax),%eax
  800e8e:	0f be c0             	movsbl %al,%eax
  800e91:	83 e8 37             	sub    $0x37,%eax
  800e94:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800e97:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e9a:	3b 45 10             	cmp    0x10(%ebp),%eax
  800e9d:	7c 02                	jl     800ea1 <strtol+0x124>
			break;
  800e9f:	eb 1a                	jmp    800ebb <strtol+0x13e>
		s++, val = (val * base) + dig;
  800ea1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ea5:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ea8:	0f af 45 10          	imul   0x10(%ebp),%eax
  800eac:	89 c2                	mov    %eax,%edx
  800eae:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800eb1:	01 d0                	add    %edx,%eax
  800eb3:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800eb6:	e9 6f ff ff ff       	jmp    800e2a <strtol+0xad>

	if (endptr)
  800ebb:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800ebf:	74 08                	je     800ec9 <strtol+0x14c>
		*endptr = (char *) s;
  800ec1:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ec4:	8b 55 08             	mov    0x8(%ebp),%edx
  800ec7:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800ec9:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800ecd:	74 07                	je     800ed6 <strtol+0x159>
  800ecf:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ed2:	f7 d8                	neg    %eax
  800ed4:	eb 03                	jmp    800ed9 <strtol+0x15c>
  800ed6:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800ed9:	c9                   	leave  
  800eda:	c3                   	ret    

00800edb <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800edb:	55                   	push   %ebp
  800edc:	89 e5                	mov    %esp,%ebp
  800ede:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800ee1:	8b 45 14             	mov    0x14(%ebp),%eax
  800ee4:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800eea:	8b 45 14             	mov    0x14(%ebp),%eax
  800eed:	8b 00                	mov    (%eax),%eax
  800eef:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ef6:	8b 45 10             	mov    0x10(%ebp),%eax
  800ef9:	01 d0                	add    %edx,%eax
  800efb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800f01:	eb 0c                	jmp    800f0f <strsplit+0x34>
			*string++ = 0;
  800f03:	8b 45 08             	mov    0x8(%ebp),%eax
  800f06:	8d 50 01             	lea    0x1(%eax),%edx
  800f09:	89 55 08             	mov    %edx,0x8(%ebp)
  800f0c:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800f0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800f12:	0f b6 00             	movzbl (%eax),%eax
  800f15:	84 c0                	test   %al,%al
  800f17:	74 1c                	je     800f35 <strsplit+0x5a>
  800f19:	8b 45 08             	mov    0x8(%ebp),%eax
  800f1c:	0f b6 00             	movzbl (%eax),%eax
  800f1f:	0f be c0             	movsbl %al,%eax
  800f22:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f26:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f29:	89 04 24             	mov    %eax,(%esp)
  800f2c:	e8 70 fc ff ff       	call   800ba1 <strchr>
  800f31:	85 c0                	test   %eax,%eax
  800f33:	75 ce                	jne    800f03 <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800f35:	8b 45 08             	mov    0x8(%ebp),%eax
  800f38:	0f b6 00             	movzbl (%eax),%eax
  800f3b:	84 c0                	test   %al,%al
  800f3d:	75 1f                	jne    800f5e <strsplit+0x83>
			break;
  800f3f:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800f40:	8b 45 14             	mov    0x14(%ebp),%eax
  800f43:	8b 00                	mov    (%eax),%eax
  800f45:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f4c:	8b 45 10             	mov    0x10(%ebp),%eax
  800f4f:	01 d0                	add    %edx,%eax
  800f51:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800f57:	b8 01 00 00 00       	mov    $0x1,%eax
  800f5c:	eb 61                	jmp    800fbf <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800f5e:	8b 45 14             	mov    0x14(%ebp),%eax
  800f61:	8b 00                	mov    (%eax),%eax
  800f63:	83 f8 0f             	cmp    $0xf,%eax
  800f66:	75 07                	jne    800f6f <strsplit+0x94>
		{
			return 0;
  800f68:	b8 00 00 00 00       	mov    $0x0,%eax
  800f6d:	eb 50                	jmp    800fbf <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800f6f:	8b 45 14             	mov    0x14(%ebp),%eax
  800f72:	8b 00                	mov    (%eax),%eax
  800f74:	8d 48 01             	lea    0x1(%eax),%ecx
  800f77:	8b 55 14             	mov    0x14(%ebp),%edx
  800f7a:	89 0a                	mov    %ecx,(%edx)
  800f7c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f83:	8b 45 10             	mov    0x10(%ebp),%eax
  800f86:	01 c2                	add    %eax,%edx
  800f88:	8b 45 08             	mov    0x8(%ebp),%eax
  800f8b:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800f8d:	eb 04                	jmp    800f93 <strsplit+0xb8>
			string++;
  800f8f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800f93:	8b 45 08             	mov    0x8(%ebp),%eax
  800f96:	0f b6 00             	movzbl (%eax),%eax
  800f99:	84 c0                	test   %al,%al
  800f9b:	74 1c                	je     800fb9 <strsplit+0xde>
  800f9d:	8b 45 08             	mov    0x8(%ebp),%eax
  800fa0:	0f b6 00             	movzbl (%eax),%eax
  800fa3:	0f be c0             	movsbl %al,%eax
  800fa6:	89 44 24 04          	mov    %eax,0x4(%esp)
  800faa:	8b 45 0c             	mov    0xc(%ebp),%eax
  800fad:	89 04 24             	mov    %eax,(%esp)
  800fb0:	e8 ec fb ff ff       	call   800ba1 <strchr>
  800fb5:	85 c0                	test   %eax,%eax
  800fb7:	74 d6                	je     800f8f <strsplit+0xb4>
			string++;
	}
  800fb9:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800fba:	e9 50 ff ff ff       	jmp    800f0f <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
  800fbf:	c9                   	leave  
  800fc0:	c3                   	ret    

00800fc1 <malloc>:

 
static uint8 *ptr_user_free_mem  = (uint8*) USER_HEAP_START;

void* malloc(uint32 size)
{	
  800fc1:	55                   	push   %ebp
  800fc2:	89 e5                	mov    %esp,%ebp
  800fc4:	83 ec 18             	sub    $0x18,%esp
	//PROJECT 2008: your code here
	//	

	panic("malloc is not implemented yet");
  800fc7:	c7 44 24 08 9c 18 80 	movl   $0x80189c,0x8(%esp)
  800fce:	00 
  800fcf:	c7 44 24 04 2b 00 00 	movl   $0x2b,0x4(%esp)
  800fd6:	00 
  800fd7:	c7 04 24 ba 18 80 00 	movl   $0x8018ba,(%esp)
  800fde:	e8 13 03 00 00       	call   8012f6 <_panic>

00800fe3 <freeHeap>:
//	freeMem(uint32* ptr_page_directory, void* start_virtual_address, uint32 size) in 
//	"memory_manager.c" then switch back to user mode, the later function is empty, 
//	please go fill it.

void freeHeap()
{
  800fe3:	55                   	push   %ebp
  800fe4:	89 e5                	mov    %esp,%ebp
  800fe6:	83 ec 18             	sub    $0x18,%esp
	//PROJECT 2008: your code here
	//	

	panic("freeHeap is not implemented yet");
  800fe9:	c7 44 24 08 c8 18 80 	movl   $0x8018c8,0x8(%esp)
  800ff0:	00 
  800ff1:	c7 44 24 04 6a 00 00 	movl   $0x6a,0x4(%esp)
  800ff8:	00 
  800ff9:	c7 04 24 ba 18 80 00 	movl   $0x8018ba,(%esp)
  801000:	e8 f1 02 00 00       	call   8012f6 <_panic>

00801005 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  801005:	55                   	push   %ebp
  801006:	89 e5                	mov    %esp,%ebp
  801008:	57                   	push   %edi
  801009:	56                   	push   %esi
  80100a:	53                   	push   %ebx
  80100b:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  80100e:	8b 45 08             	mov    0x8(%ebp),%eax
  801011:	8b 55 0c             	mov    0xc(%ebp),%edx
  801014:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801017:	8b 5d 14             	mov    0x14(%ebp),%ebx
  80101a:	8b 7d 18             	mov    0x18(%ebp),%edi
  80101d:	8b 75 1c             	mov    0x1c(%ebp),%esi
  801020:	cd 30                	int    $0x30
  801022:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  801025:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  801028:	83 c4 10             	add    $0x10,%esp
  80102b:	5b                   	pop    %ebx
  80102c:	5e                   	pop    %esi
  80102d:	5f                   	pop    %edi
  80102e:	5d                   	pop    %ebp
  80102f:	c3                   	ret    

00801030 <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  801030:	55                   	push   %ebp
  801031:	89 e5                	mov    %esp,%ebp
  801033:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  801036:	8b 45 08             	mov    0x8(%ebp),%eax
  801039:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801040:	00 
  801041:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801048:	00 
  801049:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801050:	00 
  801051:	8b 55 0c             	mov    0xc(%ebp),%edx
  801054:	89 54 24 08          	mov    %edx,0x8(%esp)
  801058:	89 44 24 04          	mov    %eax,0x4(%esp)
  80105c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  801063:	e8 9d ff ff ff       	call   801005 <syscall>
}
  801068:	c9                   	leave  
  801069:	c3                   	ret    

0080106a <sys_cgetc>:

int
sys_cgetc(void)
{
  80106a:	55                   	push   %ebp
  80106b:	89 e5                	mov    %esp,%ebp
  80106d:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  801070:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801077:	00 
  801078:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80107f:	00 
  801080:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801087:	00 
  801088:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  80108f:	00 
  801090:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  801097:	00 
  801098:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  80109f:	e8 61 ff ff ff       	call   801005 <syscall>
}
  8010a4:	c9                   	leave  
  8010a5:	c3                   	ret    

008010a6 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  8010a6:	55                   	push   %ebp
  8010a7:	89 e5                	mov    %esp,%ebp
  8010a9:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  8010ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8010af:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010b6:	00 
  8010b7:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010be:	00 
  8010bf:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010c6:	00 
  8010c7:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  8010ce:	00 
  8010cf:	89 44 24 04          	mov    %eax,0x4(%esp)
  8010d3:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  8010da:	e8 26 ff ff ff       	call   801005 <syscall>
}
  8010df:	c9                   	leave  
  8010e0:	c3                   	ret    

008010e1 <sys_getenvid>:

int32 sys_getenvid(void)
{
  8010e1:	55                   	push   %ebp
  8010e2:	89 e5                	mov    %esp,%ebp
  8010e4:	83 ec 18             	sub    $0x18,%esp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  8010e7:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010ee:	00 
  8010ef:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010f6:	00 
  8010f7:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010fe:	00 
  8010ff:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801106:	00 
  801107:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80110e:	00 
  80110f:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  801116:	e8 ea fe ff ff       	call   801005 <syscall>
}
  80111b:	c9                   	leave  
  80111c:	c3                   	ret    

0080111d <sys_env_sleep>:

void sys_env_sleep(void)
{
  80111d:	55                   	push   %ebp
  80111e:	89 e5                	mov    %esp,%ebp
  801120:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  801123:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80112a:	00 
  80112b:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801132:	00 
  801133:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80113a:	00 
  80113b:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801142:	00 
  801143:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80114a:	00 
  80114b:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  801152:	e8 ae fe ff ff       	call   801005 <syscall>
}
  801157:	c9                   	leave  
  801158:	c3                   	ret    

00801159 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  801159:	55                   	push   %ebp
  80115a:	89 e5                	mov    %esp,%ebp
  80115c:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  80115f:	8b 55 0c             	mov    0xc(%ebp),%edx
  801162:	8b 45 08             	mov    0x8(%ebp),%eax
  801165:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80116c:	00 
  80116d:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801174:	00 
  801175:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80117c:	00 
  80117d:	89 54 24 08          	mov    %edx,0x8(%esp)
  801181:	89 44 24 04          	mov    %eax,0x4(%esp)
  801185:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  80118c:	e8 74 fe ff ff       	call   801005 <syscall>
}
  801191:	c9                   	leave  
  801192:	c3                   	ret    

00801193 <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  801193:	55                   	push   %ebp
  801194:	89 e5                	mov    %esp,%ebp
  801196:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  801199:	8b 55 0c             	mov    0xc(%ebp),%edx
  80119c:	8b 45 08             	mov    0x8(%ebp),%eax
  80119f:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8011a6:	00 
  8011a7:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8011ae:	00 
  8011af:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8011b6:	00 
  8011b7:	89 54 24 08          	mov    %edx,0x8(%esp)
  8011bb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8011bf:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
  8011c6:	e8 3a fe ff ff       	call   801005 <syscall>
}
  8011cb:	c9                   	leave  
  8011cc:	c3                   	ret    

008011cd <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  8011cd:	55                   	push   %ebp
  8011ce:	89 e5                	mov    %esp,%ebp
  8011d0:	56                   	push   %esi
  8011d1:	53                   	push   %ebx
  8011d2:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  8011d5:	8b 75 18             	mov    0x18(%ebp),%esi
  8011d8:	8b 5d 14             	mov    0x14(%ebp),%ebx
  8011db:	8b 4d 10             	mov    0x10(%ebp),%ecx
  8011de:	8b 55 0c             	mov    0xc(%ebp),%edx
  8011e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8011e4:	89 74 24 14          	mov    %esi,0x14(%esp)
  8011e8:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  8011ec:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8011f0:	89 54 24 08          	mov    %edx,0x8(%esp)
  8011f4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8011f8:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  8011ff:	e8 01 fe ff ff       	call   801005 <syscall>
}
  801204:	83 c4 18             	add    $0x18,%esp
  801207:	5b                   	pop    %ebx
  801208:	5e                   	pop    %esi
  801209:	5d                   	pop    %ebp
  80120a:	c3                   	ret    

0080120b <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  80120b:	55                   	push   %ebp
  80120c:	89 e5                	mov    %esp,%ebp
  80120e:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  801211:	8b 55 0c             	mov    0xc(%ebp),%edx
  801214:	8b 45 08             	mov    0x8(%ebp),%eax
  801217:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80121e:	00 
  80121f:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801226:	00 
  801227:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80122e:	00 
  80122f:	89 54 24 08          	mov    %edx,0x8(%esp)
  801233:	89 44 24 04          	mov    %eax,0x4(%esp)
  801237:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  80123e:	e8 c2 fd ff ff       	call   801005 <syscall>
}
  801243:	c9                   	leave  
  801244:	c3                   	ret    

00801245 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  801245:	55                   	push   %ebp
  801246:	89 e5                	mov    %esp,%ebp
  801248:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  80124b:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801252:	00 
  801253:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80125a:	00 
  80125b:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801262:	00 
  801263:	8b 45 0c             	mov    0xc(%ebp),%eax
  801266:	89 44 24 08          	mov    %eax,0x8(%esp)
  80126a:	8b 45 08             	mov    0x8(%ebp),%eax
  80126d:	89 44 24 04          	mov    %eax,0x4(%esp)
  801271:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
  801278:	e8 88 fd ff ff       	call   801005 <syscall>
}
  80127d:	c9                   	leave  
  80127e:	c3                   	ret    

0080127f <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  80127f:	55                   	push   %ebp
  801280:	89 e5                	mov    %esp,%ebp
  801282:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  801285:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80128c:	00 
  80128d:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801294:	00 
  801295:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80129c:	00 
  80129d:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  8012a4:	00 
  8012a5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  8012ac:	00 
  8012ad:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  8012b4:	e8 4c fd ff ff       	call   801005 <syscall>
}
  8012b9:	c9                   	leave  
  8012ba:	c3                   	ret    

008012bb <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  8012bb:	55                   	push   %ebp
  8012bc:	89 e5                	mov    %esp,%ebp
  8012be:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  8012c1:	8b 45 08             	mov    0x8(%ebp),%eax
  8012c4:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8012cb:	00 
  8012cc:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8012d3:	00 
  8012d4:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8012db:	00 
  8012dc:	8b 55 0c             	mov    0xc(%ebp),%edx
  8012df:	89 54 24 08          	mov    %edx,0x8(%esp)
  8012e3:	89 44 24 04          	mov    %eax,0x4(%esp)
  8012e7:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
  8012ee:	e8 12 fd ff ff       	call   801005 <syscall>
	return;
  8012f3:	90                   	nop
}
  8012f4:	c9                   	leave  
  8012f5:	c3                   	ret    

008012f6 <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes FOS to enter the FOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
  8012f6:	55                   	push   %ebp
  8012f7:	89 e5                	mov    %esp,%ebp
  8012f9:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  8012fc:	8d 45 10             	lea    0x10(%ebp),%eax
  8012ff:	83 c0 04             	add    $0x4,%eax
  801302:	89 45 f4             	mov    %eax,-0xc(%ebp)

	// Print the panic message
	if (argv0)
  801305:	a1 0c 20 80 00       	mov    0x80200c,%eax
  80130a:	85 c0                	test   %eax,%eax
  80130c:	74 15                	je     801323 <_panic+0x2d>
		cprintf("%s: ", argv0);
  80130e:	a1 0c 20 80 00       	mov    0x80200c,%eax
  801313:	89 44 24 04          	mov    %eax,0x4(%esp)
  801317:	c7 04 24 e8 18 80 00 	movl   $0x8018e8,(%esp)
  80131e:	e8 e5 ef ff ff       	call   800308 <cprintf>
	cprintf("user panic in %s at %s:%d: ", binaryname, file, line);
  801323:	a1 00 20 80 00       	mov    0x802000,%eax
  801328:	8b 55 0c             	mov    0xc(%ebp),%edx
  80132b:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80132f:	8b 55 08             	mov    0x8(%ebp),%edx
  801332:	89 54 24 08          	mov    %edx,0x8(%esp)
  801336:	89 44 24 04          	mov    %eax,0x4(%esp)
  80133a:	c7 04 24 ed 18 80 00 	movl   $0x8018ed,(%esp)
  801341:	e8 c2 ef ff ff       	call   800308 <cprintf>
	vcprintf(fmt, ap);
  801346:	8b 45 10             	mov    0x10(%ebp),%eax
  801349:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80134c:	89 54 24 04          	mov    %edx,0x4(%esp)
  801350:	89 04 24             	mov    %eax,(%esp)
  801353:	e8 4c ef ff ff       	call   8002a4 <vcprintf>
	cprintf("\n");
  801358:	c7 04 24 09 19 80 00 	movl   $0x801909,(%esp)
  80135f:	e8 a4 ef ff ff       	call   800308 <cprintf>

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801364:	cc                   	int3   
  801365:	eb fd                	jmp    801364 <_panic+0x6e>
  801367:	66 90                	xchg   %ax,%ax
  801369:	66 90                	xchg   %ax,%ax
  80136b:	66 90                	xchg   %ax,%ax
  80136d:	66 90                	xchg   %ax,%ax
  80136f:	90                   	nop

00801370 <__udivdi3>:
  801370:	55                   	push   %ebp
  801371:	57                   	push   %edi
  801372:	56                   	push   %esi
  801373:	83 ec 0c             	sub    $0xc,%esp
  801376:	8b 44 24 28          	mov    0x28(%esp),%eax
  80137a:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
  80137e:	8b 6c 24 20          	mov    0x20(%esp),%ebp
  801382:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  801386:	85 c0                	test   %eax,%eax
  801388:	89 7c 24 04          	mov    %edi,0x4(%esp)
  80138c:	89 ea                	mov    %ebp,%edx
  80138e:	89 0c 24             	mov    %ecx,(%esp)
  801391:	75 2d                	jne    8013c0 <__udivdi3+0x50>
  801393:	39 e9                	cmp    %ebp,%ecx
  801395:	77 61                	ja     8013f8 <__udivdi3+0x88>
  801397:	85 c9                	test   %ecx,%ecx
  801399:	89 ce                	mov    %ecx,%esi
  80139b:	75 0b                	jne    8013a8 <__udivdi3+0x38>
  80139d:	b8 01 00 00 00       	mov    $0x1,%eax
  8013a2:	31 d2                	xor    %edx,%edx
  8013a4:	f7 f1                	div    %ecx
  8013a6:	89 c6                	mov    %eax,%esi
  8013a8:	31 d2                	xor    %edx,%edx
  8013aa:	89 e8                	mov    %ebp,%eax
  8013ac:	f7 f6                	div    %esi
  8013ae:	89 c5                	mov    %eax,%ebp
  8013b0:	89 f8                	mov    %edi,%eax
  8013b2:	f7 f6                	div    %esi
  8013b4:	89 ea                	mov    %ebp,%edx
  8013b6:	83 c4 0c             	add    $0xc,%esp
  8013b9:	5e                   	pop    %esi
  8013ba:	5f                   	pop    %edi
  8013bb:	5d                   	pop    %ebp
  8013bc:	c3                   	ret    
  8013bd:	8d 76 00             	lea    0x0(%esi),%esi
  8013c0:	39 e8                	cmp    %ebp,%eax
  8013c2:	77 24                	ja     8013e8 <__udivdi3+0x78>
  8013c4:	0f bd e8             	bsr    %eax,%ebp
  8013c7:	83 f5 1f             	xor    $0x1f,%ebp
  8013ca:	75 3c                	jne    801408 <__udivdi3+0x98>
  8013cc:	8b 74 24 04          	mov    0x4(%esp),%esi
  8013d0:	39 34 24             	cmp    %esi,(%esp)
  8013d3:	0f 86 9f 00 00 00    	jbe    801478 <__udivdi3+0x108>
  8013d9:	39 d0                	cmp    %edx,%eax
  8013db:	0f 82 97 00 00 00    	jb     801478 <__udivdi3+0x108>
  8013e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8013e8:	31 d2                	xor    %edx,%edx
  8013ea:	31 c0                	xor    %eax,%eax
  8013ec:	83 c4 0c             	add    $0xc,%esp
  8013ef:	5e                   	pop    %esi
  8013f0:	5f                   	pop    %edi
  8013f1:	5d                   	pop    %ebp
  8013f2:	c3                   	ret    
  8013f3:	90                   	nop
  8013f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8013f8:	89 f8                	mov    %edi,%eax
  8013fa:	f7 f1                	div    %ecx
  8013fc:	31 d2                	xor    %edx,%edx
  8013fe:	83 c4 0c             	add    $0xc,%esp
  801401:	5e                   	pop    %esi
  801402:	5f                   	pop    %edi
  801403:	5d                   	pop    %ebp
  801404:	c3                   	ret    
  801405:	8d 76 00             	lea    0x0(%esi),%esi
  801408:	89 e9                	mov    %ebp,%ecx
  80140a:	8b 3c 24             	mov    (%esp),%edi
  80140d:	d3 e0                	shl    %cl,%eax
  80140f:	89 c6                	mov    %eax,%esi
  801411:	b8 20 00 00 00       	mov    $0x20,%eax
  801416:	29 e8                	sub    %ebp,%eax
  801418:	89 c1                	mov    %eax,%ecx
  80141a:	d3 ef                	shr    %cl,%edi
  80141c:	89 e9                	mov    %ebp,%ecx
  80141e:	89 7c 24 08          	mov    %edi,0x8(%esp)
  801422:	8b 3c 24             	mov    (%esp),%edi
  801425:	09 74 24 08          	or     %esi,0x8(%esp)
  801429:	89 d6                	mov    %edx,%esi
  80142b:	d3 e7                	shl    %cl,%edi
  80142d:	89 c1                	mov    %eax,%ecx
  80142f:	89 3c 24             	mov    %edi,(%esp)
  801432:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801436:	d3 ee                	shr    %cl,%esi
  801438:	89 e9                	mov    %ebp,%ecx
  80143a:	d3 e2                	shl    %cl,%edx
  80143c:	89 c1                	mov    %eax,%ecx
  80143e:	d3 ef                	shr    %cl,%edi
  801440:	09 d7                	or     %edx,%edi
  801442:	89 f2                	mov    %esi,%edx
  801444:	89 f8                	mov    %edi,%eax
  801446:	f7 74 24 08          	divl   0x8(%esp)
  80144a:	89 d6                	mov    %edx,%esi
  80144c:	89 c7                	mov    %eax,%edi
  80144e:	f7 24 24             	mull   (%esp)
  801451:	39 d6                	cmp    %edx,%esi
  801453:	89 14 24             	mov    %edx,(%esp)
  801456:	72 30                	jb     801488 <__udivdi3+0x118>
  801458:	8b 54 24 04          	mov    0x4(%esp),%edx
  80145c:	89 e9                	mov    %ebp,%ecx
  80145e:	d3 e2                	shl    %cl,%edx
  801460:	39 c2                	cmp    %eax,%edx
  801462:	73 05                	jae    801469 <__udivdi3+0xf9>
  801464:	3b 34 24             	cmp    (%esp),%esi
  801467:	74 1f                	je     801488 <__udivdi3+0x118>
  801469:	89 f8                	mov    %edi,%eax
  80146b:	31 d2                	xor    %edx,%edx
  80146d:	e9 7a ff ff ff       	jmp    8013ec <__udivdi3+0x7c>
  801472:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801478:	31 d2                	xor    %edx,%edx
  80147a:	b8 01 00 00 00       	mov    $0x1,%eax
  80147f:	e9 68 ff ff ff       	jmp    8013ec <__udivdi3+0x7c>
  801484:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801488:	8d 47 ff             	lea    -0x1(%edi),%eax
  80148b:	31 d2                	xor    %edx,%edx
  80148d:	83 c4 0c             	add    $0xc,%esp
  801490:	5e                   	pop    %esi
  801491:	5f                   	pop    %edi
  801492:	5d                   	pop    %ebp
  801493:	c3                   	ret    
  801494:	66 90                	xchg   %ax,%ax
  801496:	66 90                	xchg   %ax,%ax
  801498:	66 90                	xchg   %ax,%ax
  80149a:	66 90                	xchg   %ax,%ax
  80149c:	66 90                	xchg   %ax,%ax
  80149e:	66 90                	xchg   %ax,%ax

008014a0 <__umoddi3>:
  8014a0:	55                   	push   %ebp
  8014a1:	57                   	push   %edi
  8014a2:	56                   	push   %esi
  8014a3:	83 ec 14             	sub    $0x14,%esp
  8014a6:	8b 44 24 28          	mov    0x28(%esp),%eax
  8014aa:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  8014ae:	8b 74 24 2c          	mov    0x2c(%esp),%esi
  8014b2:	89 c7                	mov    %eax,%edi
  8014b4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8014b8:	8b 44 24 30          	mov    0x30(%esp),%eax
  8014bc:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  8014c0:	89 34 24             	mov    %esi,(%esp)
  8014c3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8014c7:	85 c0                	test   %eax,%eax
  8014c9:	89 c2                	mov    %eax,%edx
  8014cb:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8014cf:	75 17                	jne    8014e8 <__umoddi3+0x48>
  8014d1:	39 fe                	cmp    %edi,%esi
  8014d3:	76 4b                	jbe    801520 <__umoddi3+0x80>
  8014d5:	89 c8                	mov    %ecx,%eax
  8014d7:	89 fa                	mov    %edi,%edx
  8014d9:	f7 f6                	div    %esi
  8014db:	89 d0                	mov    %edx,%eax
  8014dd:	31 d2                	xor    %edx,%edx
  8014df:	83 c4 14             	add    $0x14,%esp
  8014e2:	5e                   	pop    %esi
  8014e3:	5f                   	pop    %edi
  8014e4:	5d                   	pop    %ebp
  8014e5:	c3                   	ret    
  8014e6:	66 90                	xchg   %ax,%ax
  8014e8:	39 f8                	cmp    %edi,%eax
  8014ea:	77 54                	ja     801540 <__umoddi3+0xa0>
  8014ec:	0f bd e8             	bsr    %eax,%ebp
  8014ef:	83 f5 1f             	xor    $0x1f,%ebp
  8014f2:	75 5c                	jne    801550 <__umoddi3+0xb0>
  8014f4:	8b 7c 24 08          	mov    0x8(%esp),%edi
  8014f8:	39 3c 24             	cmp    %edi,(%esp)
  8014fb:	0f 87 e7 00 00 00    	ja     8015e8 <__umoddi3+0x148>
  801501:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801505:	29 f1                	sub    %esi,%ecx
  801507:	19 c7                	sbb    %eax,%edi
  801509:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80150d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801511:	8b 44 24 08          	mov    0x8(%esp),%eax
  801515:	8b 54 24 0c          	mov    0xc(%esp),%edx
  801519:	83 c4 14             	add    $0x14,%esp
  80151c:	5e                   	pop    %esi
  80151d:	5f                   	pop    %edi
  80151e:	5d                   	pop    %ebp
  80151f:	c3                   	ret    
  801520:	85 f6                	test   %esi,%esi
  801522:	89 f5                	mov    %esi,%ebp
  801524:	75 0b                	jne    801531 <__umoddi3+0x91>
  801526:	b8 01 00 00 00       	mov    $0x1,%eax
  80152b:	31 d2                	xor    %edx,%edx
  80152d:	f7 f6                	div    %esi
  80152f:	89 c5                	mov    %eax,%ebp
  801531:	8b 44 24 04          	mov    0x4(%esp),%eax
  801535:	31 d2                	xor    %edx,%edx
  801537:	f7 f5                	div    %ebp
  801539:	89 c8                	mov    %ecx,%eax
  80153b:	f7 f5                	div    %ebp
  80153d:	eb 9c                	jmp    8014db <__umoddi3+0x3b>
  80153f:	90                   	nop
  801540:	89 c8                	mov    %ecx,%eax
  801542:	89 fa                	mov    %edi,%edx
  801544:	83 c4 14             	add    $0x14,%esp
  801547:	5e                   	pop    %esi
  801548:	5f                   	pop    %edi
  801549:	5d                   	pop    %ebp
  80154a:	c3                   	ret    
  80154b:	90                   	nop
  80154c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801550:	8b 04 24             	mov    (%esp),%eax
  801553:	be 20 00 00 00       	mov    $0x20,%esi
  801558:	89 e9                	mov    %ebp,%ecx
  80155a:	29 ee                	sub    %ebp,%esi
  80155c:	d3 e2                	shl    %cl,%edx
  80155e:	89 f1                	mov    %esi,%ecx
  801560:	d3 e8                	shr    %cl,%eax
  801562:	89 e9                	mov    %ebp,%ecx
  801564:	89 44 24 04          	mov    %eax,0x4(%esp)
  801568:	8b 04 24             	mov    (%esp),%eax
  80156b:	09 54 24 04          	or     %edx,0x4(%esp)
  80156f:	89 fa                	mov    %edi,%edx
  801571:	d3 e0                	shl    %cl,%eax
  801573:	89 f1                	mov    %esi,%ecx
  801575:	89 44 24 08          	mov    %eax,0x8(%esp)
  801579:	8b 44 24 10          	mov    0x10(%esp),%eax
  80157d:	d3 ea                	shr    %cl,%edx
  80157f:	89 e9                	mov    %ebp,%ecx
  801581:	d3 e7                	shl    %cl,%edi
  801583:	89 f1                	mov    %esi,%ecx
  801585:	d3 e8                	shr    %cl,%eax
  801587:	89 e9                	mov    %ebp,%ecx
  801589:	09 f8                	or     %edi,%eax
  80158b:	8b 7c 24 10          	mov    0x10(%esp),%edi
  80158f:	f7 74 24 04          	divl   0x4(%esp)
  801593:	d3 e7                	shl    %cl,%edi
  801595:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801599:	89 d7                	mov    %edx,%edi
  80159b:	f7 64 24 08          	mull   0x8(%esp)
  80159f:	39 d7                	cmp    %edx,%edi
  8015a1:	89 c1                	mov    %eax,%ecx
  8015a3:	89 14 24             	mov    %edx,(%esp)
  8015a6:	72 2c                	jb     8015d4 <__umoddi3+0x134>
  8015a8:	39 44 24 0c          	cmp    %eax,0xc(%esp)
  8015ac:	72 22                	jb     8015d0 <__umoddi3+0x130>
  8015ae:	8b 44 24 0c          	mov    0xc(%esp),%eax
  8015b2:	29 c8                	sub    %ecx,%eax
  8015b4:	19 d7                	sbb    %edx,%edi
  8015b6:	89 e9                	mov    %ebp,%ecx
  8015b8:	89 fa                	mov    %edi,%edx
  8015ba:	d3 e8                	shr    %cl,%eax
  8015bc:	89 f1                	mov    %esi,%ecx
  8015be:	d3 e2                	shl    %cl,%edx
  8015c0:	89 e9                	mov    %ebp,%ecx
  8015c2:	d3 ef                	shr    %cl,%edi
  8015c4:	09 d0                	or     %edx,%eax
  8015c6:	89 fa                	mov    %edi,%edx
  8015c8:	83 c4 14             	add    $0x14,%esp
  8015cb:	5e                   	pop    %esi
  8015cc:	5f                   	pop    %edi
  8015cd:	5d                   	pop    %ebp
  8015ce:	c3                   	ret    
  8015cf:	90                   	nop
  8015d0:	39 d7                	cmp    %edx,%edi
  8015d2:	75 da                	jne    8015ae <__umoddi3+0x10e>
  8015d4:	8b 14 24             	mov    (%esp),%edx
  8015d7:	89 c1                	mov    %eax,%ecx
  8015d9:	2b 4c 24 08          	sub    0x8(%esp),%ecx
  8015dd:	1b 54 24 04          	sbb    0x4(%esp),%edx
  8015e1:	eb cb                	jmp    8015ae <__umoddi3+0x10e>
  8015e3:	90                   	nop
  8015e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8015e8:	3b 44 24 0c          	cmp    0xc(%esp),%eax
  8015ec:	0f 82 0f ff ff ff    	jb     801501 <__umoddi3+0x61>
  8015f2:	e9 1a ff ff ff       	jmp    801511 <__umoddi3+0x71>
