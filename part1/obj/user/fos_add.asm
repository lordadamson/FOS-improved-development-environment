
obj/user/fos_add:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 6f 00 00 00       	call   8000a5 <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
// hello, world
#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 28             	sub    $0x28,%esp
	int i1=0;
  80003e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i2=0;
  800045:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)

	i1 = strtol("1", NULL, 10);
  80004c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
  800053:	00 
  800054:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80005b:	00 
  80005c:	c7 04 24 00 14 80 00 	movl   $0x801400,(%esp)
  800063:	e8 cc 0b 00 00       	call   800c34 <strtol>
  800068:	89 45 f4             	mov    %eax,-0xc(%ebp)
	i2 = strtol("2", NULL, 10);
  80006b:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
  800072:	00 
  800073:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80007a:	00 
  80007b:	c7 04 24 02 14 80 00 	movl   $0x801402,(%esp)
  800082:	e8 ad 0b 00 00       	call   800c34 <strtol>
  800087:	89 45 f0             	mov    %eax,-0x10(%ebp)

	cprintf("number 1 + number 2 = %d\n",i1+i2);
  80008a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80008d:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800090:	01 d0                	add    %edx,%eax
  800092:	89 44 24 04          	mov    %eax,0x4(%esp)
  800096:	c7 04 24 04 14 80 00 	movl   $0x801404,(%esp)
  80009d:	e8 1d 01 00 00       	call   8001bf <cprintf>
	return;	
  8000a2:	90                   	nop
}
  8000a3:	c9                   	leave  
  8000a4:	c3                   	ret    

008000a5 <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8000a5:	55                   	push   %ebp
  8000a6:	89 e5                	mov    %esp,%ebp
  8000a8:	83 ec 18             	sub    $0x18,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8000ab:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000b2:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000b5:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000b9:	7e 0a                	jle    8000c5 <libmain+0x20>
		binaryname = argv[0];
  8000bb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000be:	8b 00                	mov    (%eax),%eax
  8000c0:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000c5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000c8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8000cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8000cf:	89 04 24             	mov    %eax,(%esp)
  8000d2:	e8 61 ff ff ff       	call   800038 <_main>

	// exit gracefully
	//exit();
	sleep();
  8000d7:	e8 16 00 00 00       	call   8000f2 <sleep>
}
  8000dc:	c9                   	leave  
  8000dd:	c3                   	ret    

008000de <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000de:	55                   	push   %ebp
  8000df:	89 e5                	mov    %esp,%ebp
  8000e1:	83 ec 18             	sub    $0x18,%esp
	sys_env_destroy(0);	
  8000e4:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  8000eb:	e8 29 0e 00 00       	call   800f19 <sys_env_destroy>
}
  8000f0:	c9                   	leave  
  8000f1:	c3                   	ret    

008000f2 <sleep>:

void
sleep(void)
{	
  8000f2:	55                   	push   %ebp
  8000f3:	89 e5                	mov    %esp,%ebp
  8000f5:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  8000f8:	e8 93 0e 00 00       	call   800f90 <sys_env_sleep>
}
  8000fd:	c9                   	leave  
  8000fe:	c3                   	ret    

008000ff <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000ff:	55                   	push   %ebp
  800100:	89 e5                	mov    %esp,%ebp
  800102:	83 ec 18             	sub    $0x18,%esp
	b->buf[b->idx++] = ch;
  800105:	8b 45 0c             	mov    0xc(%ebp),%eax
  800108:	8b 00                	mov    (%eax),%eax
  80010a:	8d 48 01             	lea    0x1(%eax),%ecx
  80010d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800110:	89 0a                	mov    %ecx,(%edx)
  800112:	8b 55 08             	mov    0x8(%ebp),%edx
  800115:	89 d1                	mov    %edx,%ecx
  800117:	8b 55 0c             	mov    0xc(%ebp),%edx
  80011a:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  80011e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800121:	8b 00                	mov    (%eax),%eax
  800123:	3d ff 00 00 00       	cmp    $0xff,%eax
  800128:	75 20                	jne    80014a <putch+0x4b>
		sys_cputs(b->buf, b->idx);
  80012a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80012d:	8b 00                	mov    (%eax),%eax
  80012f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800132:	83 c2 08             	add    $0x8,%edx
  800135:	89 44 24 04          	mov    %eax,0x4(%esp)
  800139:	89 14 24             	mov    %edx,(%esp)
  80013c:	e8 62 0d 00 00       	call   800ea3 <sys_cputs>
		b->idx = 0;
  800141:	8b 45 0c             	mov    0xc(%ebp),%eax
  800144:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  80014a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80014d:	8b 40 04             	mov    0x4(%eax),%eax
  800150:	8d 50 01             	lea    0x1(%eax),%edx
  800153:	8b 45 0c             	mov    0xc(%ebp),%eax
  800156:	89 50 04             	mov    %edx,0x4(%eax)
}
  800159:	c9                   	leave  
  80015a:	c3                   	ret    

0080015b <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80015b:	55                   	push   %ebp
  80015c:	89 e5                	mov    %esp,%ebp
  80015e:	81 ec 28 01 00 00    	sub    $0x128,%esp
	struct printbuf b;

	b.idx = 0;
  800164:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80016b:	00 00 00 
	b.cnt = 0;
  80016e:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800175:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800178:	8b 45 0c             	mov    0xc(%ebp),%eax
  80017b:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80017f:	8b 45 08             	mov    0x8(%ebp),%eax
  800182:	89 44 24 08          	mov    %eax,0x8(%esp)
  800186:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80018c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800190:	c7 04 24 ff 00 80 00 	movl   $0x8000ff,(%esp)
  800197:	e8 ed 01 00 00       	call   800389 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  80019c:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8001a2:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001a6:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001ac:	83 c0 08             	add    $0x8,%eax
  8001af:	89 04 24             	mov    %eax,(%esp)
  8001b2:	e8 ec 0c 00 00       	call   800ea3 <sys_cputs>

	return b.cnt;
  8001b7:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001bd:	c9                   	leave  
  8001be:	c3                   	ret    

008001bf <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001bf:	55                   	push   %ebp
  8001c0:	89 e5                	mov    %esp,%ebp
  8001c2:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001c5:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001c8:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001cb:	8b 45 08             	mov    0x8(%ebp),%eax
  8001ce:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8001d1:	89 54 24 04          	mov    %edx,0x4(%esp)
  8001d5:	89 04 24             	mov    %eax,(%esp)
  8001d8:	e8 7e ff ff ff       	call   80015b <vcprintf>
  8001dd:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  8001e0:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8001e3:	c9                   	leave  
  8001e4:	c3                   	ret    

008001e5 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001e5:	55                   	push   %ebp
  8001e6:	89 e5                	mov    %esp,%ebp
  8001e8:	53                   	push   %ebx
  8001e9:	83 ec 34             	sub    $0x34,%esp
  8001ec:	8b 45 10             	mov    0x10(%ebp),%eax
  8001ef:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8001f2:	8b 45 14             	mov    0x14(%ebp),%eax
  8001f5:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001f8:	8b 45 18             	mov    0x18(%ebp),%eax
  8001fb:	ba 00 00 00 00       	mov    $0x0,%edx
  800200:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800203:	77 72                	ja     800277 <printnum+0x92>
  800205:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800208:	72 05                	jb     80020f <printnum+0x2a>
  80020a:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  80020d:	77 68                	ja     800277 <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80020f:	8b 45 1c             	mov    0x1c(%ebp),%eax
  800212:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800215:	8b 45 18             	mov    0x18(%ebp),%eax
  800218:	ba 00 00 00 00       	mov    $0x0,%edx
  80021d:	89 44 24 08          	mov    %eax,0x8(%esp)
  800221:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800225:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800228:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80022b:	89 04 24             	mov    %eax,(%esp)
  80022e:	89 54 24 04          	mov    %edx,0x4(%esp)
  800232:	e8 39 0f 00 00       	call   801170 <__udivdi3>
  800237:	8b 4d 20             	mov    0x20(%ebp),%ecx
  80023a:	89 4c 24 18          	mov    %ecx,0x18(%esp)
  80023e:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  800242:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800245:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  800249:	89 44 24 08          	mov    %eax,0x8(%esp)
  80024d:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800251:	8b 45 0c             	mov    0xc(%ebp),%eax
  800254:	89 44 24 04          	mov    %eax,0x4(%esp)
  800258:	8b 45 08             	mov    0x8(%ebp),%eax
  80025b:	89 04 24             	mov    %eax,(%esp)
  80025e:	e8 82 ff ff ff       	call   8001e5 <printnum>
  800263:	eb 1c                	jmp    800281 <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800265:	8b 45 0c             	mov    0xc(%ebp),%eax
  800268:	89 44 24 04          	mov    %eax,0x4(%esp)
  80026c:	8b 45 20             	mov    0x20(%ebp),%eax
  80026f:	89 04 24             	mov    %eax,(%esp)
  800272:	8b 45 08             	mov    0x8(%ebp),%eax
  800275:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800277:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  80027b:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  80027f:	7f e4                	jg     800265 <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800281:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800284:	bb 00 00 00 00       	mov    $0x0,%ebx
  800289:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80028c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80028f:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  800293:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  800297:	89 04 24             	mov    %eax,(%esp)
  80029a:	89 54 24 04          	mov    %edx,0x4(%esp)
  80029e:	e8 fd 0f 00 00       	call   8012a0 <__umoddi3>
  8002a3:	05 e0 14 80 00       	add    $0x8014e0,%eax
  8002a8:	0f b6 00             	movzbl (%eax),%eax
  8002ab:	0f be c0             	movsbl %al,%eax
  8002ae:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002b1:	89 54 24 04          	mov    %edx,0x4(%esp)
  8002b5:	89 04 24             	mov    %eax,(%esp)
  8002b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002bb:	ff d0                	call   *%eax
}
  8002bd:	83 c4 34             	add    $0x34,%esp
  8002c0:	5b                   	pop    %ebx
  8002c1:	5d                   	pop    %ebp
  8002c2:	c3                   	ret    

008002c3 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002c3:	55                   	push   %ebp
  8002c4:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002c6:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002ca:	7e 1c                	jle    8002e8 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8002cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8002cf:	8b 00                	mov    (%eax),%eax
  8002d1:	8d 50 08             	lea    0x8(%eax),%edx
  8002d4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d7:	89 10                	mov    %edx,(%eax)
  8002d9:	8b 45 08             	mov    0x8(%ebp),%eax
  8002dc:	8b 00                	mov    (%eax),%eax
  8002de:	83 e8 08             	sub    $0x8,%eax
  8002e1:	8b 50 04             	mov    0x4(%eax),%edx
  8002e4:	8b 00                	mov    (%eax),%eax
  8002e6:	eb 40                	jmp    800328 <getuint+0x65>
	else if (lflag)
  8002e8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002ec:	74 1e                	je     80030c <getuint+0x49>
		return va_arg(*ap, unsigned long);
  8002ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f1:	8b 00                	mov    (%eax),%eax
  8002f3:	8d 50 04             	lea    0x4(%eax),%edx
  8002f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f9:	89 10                	mov    %edx,(%eax)
  8002fb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002fe:	8b 00                	mov    (%eax),%eax
  800300:	83 e8 04             	sub    $0x4,%eax
  800303:	8b 00                	mov    (%eax),%eax
  800305:	ba 00 00 00 00       	mov    $0x0,%edx
  80030a:	eb 1c                	jmp    800328 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  80030c:	8b 45 08             	mov    0x8(%ebp),%eax
  80030f:	8b 00                	mov    (%eax),%eax
  800311:	8d 50 04             	lea    0x4(%eax),%edx
  800314:	8b 45 08             	mov    0x8(%ebp),%eax
  800317:	89 10                	mov    %edx,(%eax)
  800319:	8b 45 08             	mov    0x8(%ebp),%eax
  80031c:	8b 00                	mov    (%eax),%eax
  80031e:	83 e8 04             	sub    $0x4,%eax
  800321:	8b 00                	mov    (%eax),%eax
  800323:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800328:	5d                   	pop    %ebp
  800329:	c3                   	ret    

0080032a <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  80032a:	55                   	push   %ebp
  80032b:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80032d:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800331:	7e 1c                	jle    80034f <getint+0x25>
		return va_arg(*ap, long long);
  800333:	8b 45 08             	mov    0x8(%ebp),%eax
  800336:	8b 00                	mov    (%eax),%eax
  800338:	8d 50 08             	lea    0x8(%eax),%edx
  80033b:	8b 45 08             	mov    0x8(%ebp),%eax
  80033e:	89 10                	mov    %edx,(%eax)
  800340:	8b 45 08             	mov    0x8(%ebp),%eax
  800343:	8b 00                	mov    (%eax),%eax
  800345:	83 e8 08             	sub    $0x8,%eax
  800348:	8b 50 04             	mov    0x4(%eax),%edx
  80034b:	8b 00                	mov    (%eax),%eax
  80034d:	eb 38                	jmp    800387 <getint+0x5d>
	else if (lflag)
  80034f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800353:	74 1a                	je     80036f <getint+0x45>
		return va_arg(*ap, long);
  800355:	8b 45 08             	mov    0x8(%ebp),%eax
  800358:	8b 00                	mov    (%eax),%eax
  80035a:	8d 50 04             	lea    0x4(%eax),%edx
  80035d:	8b 45 08             	mov    0x8(%ebp),%eax
  800360:	89 10                	mov    %edx,(%eax)
  800362:	8b 45 08             	mov    0x8(%ebp),%eax
  800365:	8b 00                	mov    (%eax),%eax
  800367:	83 e8 04             	sub    $0x4,%eax
  80036a:	8b 00                	mov    (%eax),%eax
  80036c:	99                   	cltd   
  80036d:	eb 18                	jmp    800387 <getint+0x5d>
	else
		return va_arg(*ap, int);
  80036f:	8b 45 08             	mov    0x8(%ebp),%eax
  800372:	8b 00                	mov    (%eax),%eax
  800374:	8d 50 04             	lea    0x4(%eax),%edx
  800377:	8b 45 08             	mov    0x8(%ebp),%eax
  80037a:	89 10                	mov    %edx,(%eax)
  80037c:	8b 45 08             	mov    0x8(%ebp),%eax
  80037f:	8b 00                	mov    (%eax),%eax
  800381:	83 e8 04             	sub    $0x4,%eax
  800384:	8b 00                	mov    (%eax),%eax
  800386:	99                   	cltd   
}
  800387:	5d                   	pop    %ebp
  800388:	c3                   	ret    

00800389 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800389:	55                   	push   %ebp
  80038a:	89 e5                	mov    %esp,%ebp
  80038c:	56                   	push   %esi
  80038d:	53                   	push   %ebx
  80038e:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800391:	eb 18                	jmp    8003ab <vprintfmt+0x22>
			if (ch == '\0')
  800393:	85 db                	test   %ebx,%ebx
  800395:	75 05                	jne    80039c <vprintfmt+0x13>
				return;
  800397:	e9 07 04 00 00       	jmp    8007a3 <vprintfmt+0x41a>
			putch(ch, putdat);
  80039c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80039f:	89 44 24 04          	mov    %eax,0x4(%esp)
  8003a3:	89 1c 24             	mov    %ebx,(%esp)
  8003a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8003a9:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003ab:	8b 45 10             	mov    0x10(%ebp),%eax
  8003ae:	8d 50 01             	lea    0x1(%eax),%edx
  8003b1:	89 55 10             	mov    %edx,0x10(%ebp)
  8003b4:	0f b6 00             	movzbl (%eax),%eax
  8003b7:	0f b6 d8             	movzbl %al,%ebx
  8003ba:	83 fb 25             	cmp    $0x25,%ebx
  8003bd:	75 d4                	jne    800393 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8003bf:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8003c3:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8003ca:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  8003d1:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  8003d8:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003df:	8b 45 10             	mov    0x10(%ebp),%eax
  8003e2:	8d 50 01             	lea    0x1(%eax),%edx
  8003e5:	89 55 10             	mov    %edx,0x10(%ebp)
  8003e8:	0f b6 00             	movzbl (%eax),%eax
  8003eb:	0f b6 d8             	movzbl %al,%ebx
  8003ee:	8d 43 dd             	lea    -0x23(%ebx),%eax
  8003f1:	83 f8 55             	cmp    $0x55,%eax
  8003f4:	0f 87 78 03 00 00    	ja     800772 <vprintfmt+0x3e9>
  8003fa:	8b 04 85 04 15 80 00 	mov    0x801504(,%eax,4),%eax
  800401:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  800403:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  800407:	eb d6                	jmp    8003df <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800409:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  80040d:	eb d0                	jmp    8003df <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80040f:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  800416:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800419:	89 d0                	mov    %edx,%eax
  80041b:	c1 e0 02             	shl    $0x2,%eax
  80041e:	01 d0                	add    %edx,%eax
  800420:	01 c0                	add    %eax,%eax
  800422:	01 d8                	add    %ebx,%eax
  800424:	83 e8 30             	sub    $0x30,%eax
  800427:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  80042a:	8b 45 10             	mov    0x10(%ebp),%eax
  80042d:	0f b6 00             	movzbl (%eax),%eax
  800430:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  800433:	83 fb 2f             	cmp    $0x2f,%ebx
  800436:	7e 0b                	jle    800443 <vprintfmt+0xba>
  800438:	83 fb 39             	cmp    $0x39,%ebx
  80043b:	7f 06                	jg     800443 <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80043d:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800441:	eb d3                	jmp    800416 <vprintfmt+0x8d>
			goto process_precision;
  800443:	eb 39                	jmp    80047e <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
  800445:	8b 45 14             	mov    0x14(%ebp),%eax
  800448:	83 c0 04             	add    $0x4,%eax
  80044b:	89 45 14             	mov    %eax,0x14(%ebp)
  80044e:	8b 45 14             	mov    0x14(%ebp),%eax
  800451:	83 e8 04             	sub    $0x4,%eax
  800454:	8b 00                	mov    (%eax),%eax
  800456:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800459:	eb 23                	jmp    80047e <vprintfmt+0xf5>

		case '.':
			if (width < 0)
  80045b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80045f:	79 0c                	jns    80046d <vprintfmt+0xe4>
				width = 0;
  800461:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  800468:	e9 72 ff ff ff       	jmp    8003df <vprintfmt+0x56>
  80046d:	e9 6d ff ff ff       	jmp    8003df <vprintfmt+0x56>

		case '#':
			altflag = 1;
  800472:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  800479:	e9 61 ff ff ff       	jmp    8003df <vprintfmt+0x56>

		process_precision:
			if (width < 0)
  80047e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800482:	79 12                	jns    800496 <vprintfmt+0x10d>
				width = precision, precision = -1;
  800484:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800487:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80048a:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  800491:	e9 49 ff ff ff       	jmp    8003df <vprintfmt+0x56>
  800496:	e9 44 ff ff ff       	jmp    8003df <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80049b:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  80049f:	e9 3b ff ff ff       	jmp    8003df <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004a4:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a7:	83 c0 04             	add    $0x4,%eax
  8004aa:	89 45 14             	mov    %eax,0x14(%ebp)
  8004ad:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b0:	83 e8 04             	sub    $0x4,%eax
  8004b3:	8b 00                	mov    (%eax),%eax
  8004b5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8004b8:	89 54 24 04          	mov    %edx,0x4(%esp)
  8004bc:	89 04 24             	mov    %eax,(%esp)
  8004bf:	8b 45 08             	mov    0x8(%ebp),%eax
  8004c2:	ff d0                	call   *%eax
			break;
  8004c4:	e9 d4 02 00 00       	jmp    80079d <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004c9:	8b 45 14             	mov    0x14(%ebp),%eax
  8004cc:	83 c0 04             	add    $0x4,%eax
  8004cf:	89 45 14             	mov    %eax,0x14(%ebp)
  8004d2:	8b 45 14             	mov    0x14(%ebp),%eax
  8004d5:	83 e8 04             	sub    $0x4,%eax
  8004d8:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  8004da:	85 db                	test   %ebx,%ebx
  8004dc:	79 02                	jns    8004e0 <vprintfmt+0x157>
				err = -err;
  8004de:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  8004e0:	83 fb 07             	cmp    $0x7,%ebx
  8004e3:	7f 0b                	jg     8004f0 <vprintfmt+0x167>
  8004e5:	8b 34 9d c0 14 80 00 	mov    0x8014c0(,%ebx,4),%esi
  8004ec:	85 f6                	test   %esi,%esi
  8004ee:	75 23                	jne    800513 <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
  8004f0:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  8004f4:	c7 44 24 08 f1 14 80 	movl   $0x8014f1,0x8(%esp)
  8004fb:	00 
  8004fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8004ff:	89 44 24 04          	mov    %eax,0x4(%esp)
  800503:	8b 45 08             	mov    0x8(%ebp),%eax
  800506:	89 04 24             	mov    %eax,(%esp)
  800509:	e8 9c 02 00 00       	call   8007aa <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
  80050e:	e9 8a 02 00 00       	jmp    80079d <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  800513:	89 74 24 0c          	mov    %esi,0xc(%esp)
  800517:	c7 44 24 08 fa 14 80 	movl   $0x8014fa,0x8(%esp)
  80051e:	00 
  80051f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800522:	89 44 24 04          	mov    %eax,0x4(%esp)
  800526:	8b 45 08             	mov    0x8(%ebp),%eax
  800529:	89 04 24             	mov    %eax,(%esp)
  80052c:	e8 79 02 00 00       	call   8007aa <printfmt>
			break;
  800531:	e9 67 02 00 00       	jmp    80079d <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800536:	8b 45 14             	mov    0x14(%ebp),%eax
  800539:	83 c0 04             	add    $0x4,%eax
  80053c:	89 45 14             	mov    %eax,0x14(%ebp)
  80053f:	8b 45 14             	mov    0x14(%ebp),%eax
  800542:	83 e8 04             	sub    $0x4,%eax
  800545:	8b 30                	mov    (%eax),%esi
  800547:	85 f6                	test   %esi,%esi
  800549:	75 05                	jne    800550 <vprintfmt+0x1c7>
				p = "(null)";
  80054b:	be fd 14 80 00       	mov    $0x8014fd,%esi
			if (width > 0 && padc != '-')
  800550:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800554:	7e 37                	jle    80058d <vprintfmt+0x204>
  800556:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  80055a:	74 31                	je     80058d <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
  80055c:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80055f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800563:	89 34 24             	mov    %esi,(%esp)
  800566:	e8 62 03 00 00       	call   8008cd <strnlen>
  80056b:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  80056e:	eb 17                	jmp    800587 <vprintfmt+0x1fe>
					putch(padc, putdat);
  800570:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  800574:	8b 55 0c             	mov    0xc(%ebp),%edx
  800577:	89 54 24 04          	mov    %edx,0x4(%esp)
  80057b:	89 04 24             	mov    %eax,(%esp)
  80057e:	8b 45 08             	mov    0x8(%ebp),%eax
  800581:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800583:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800587:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80058b:	7f e3                	jg     800570 <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80058d:	eb 38                	jmp    8005c7 <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
  80058f:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800593:	74 1f                	je     8005b4 <vprintfmt+0x22b>
  800595:	83 fb 1f             	cmp    $0x1f,%ebx
  800598:	7e 05                	jle    80059f <vprintfmt+0x216>
  80059a:	83 fb 7e             	cmp    $0x7e,%ebx
  80059d:	7e 15                	jle    8005b4 <vprintfmt+0x22b>
					putch('?', putdat);
  80059f:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005a2:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005a6:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  8005ad:	8b 45 08             	mov    0x8(%ebp),%eax
  8005b0:	ff d0                	call   *%eax
  8005b2:	eb 0f                	jmp    8005c3 <vprintfmt+0x23a>
				else
					putch(ch, putdat);
  8005b4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005b7:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005bb:	89 1c 24             	mov    %ebx,(%esp)
  8005be:	8b 45 08             	mov    0x8(%ebp),%eax
  8005c1:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005c3:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005c7:	89 f0                	mov    %esi,%eax
  8005c9:	8d 70 01             	lea    0x1(%eax),%esi
  8005cc:	0f b6 00             	movzbl (%eax),%eax
  8005cf:	0f be d8             	movsbl %al,%ebx
  8005d2:	85 db                	test   %ebx,%ebx
  8005d4:	74 10                	je     8005e6 <vprintfmt+0x25d>
  8005d6:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005da:	78 b3                	js     80058f <vprintfmt+0x206>
  8005dc:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  8005e0:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005e4:	79 a9                	jns    80058f <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005e6:	eb 17                	jmp    8005ff <vprintfmt+0x276>
				putch(' ', putdat);
  8005e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005eb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005ef:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  8005f6:	8b 45 08             	mov    0x8(%ebp),%eax
  8005f9:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005fb:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005ff:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800603:	7f e3                	jg     8005e8 <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
  800605:	e9 93 01 00 00       	jmp    80079d <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80060a:	8b 45 e8             	mov    -0x18(%ebp),%eax
  80060d:	89 44 24 04          	mov    %eax,0x4(%esp)
  800611:	8d 45 14             	lea    0x14(%ebp),%eax
  800614:	89 04 24             	mov    %eax,(%esp)
  800617:	e8 0e fd ff ff       	call   80032a <getint>
  80061c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80061f:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  800622:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800625:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800628:	85 d2                	test   %edx,%edx
  80062a:	79 26                	jns    800652 <vprintfmt+0x2c9>
				putch('-', putdat);
  80062c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80062f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800633:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  80063a:	8b 45 08             	mov    0x8(%ebp),%eax
  80063d:	ff d0                	call   *%eax
				num = -(long long) num;
  80063f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800642:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800645:	f7 d8                	neg    %eax
  800647:	83 d2 00             	adc    $0x0,%edx
  80064a:	f7 da                	neg    %edx
  80064c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80064f:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  800652:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800659:	e9 cb 00 00 00       	jmp    800729 <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80065e:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800661:	89 44 24 04          	mov    %eax,0x4(%esp)
  800665:	8d 45 14             	lea    0x14(%ebp),%eax
  800668:	89 04 24             	mov    %eax,(%esp)
  80066b:	e8 53 fc ff ff       	call   8002c3 <getuint>
  800670:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800673:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  800676:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  80067d:	e9 a7 00 00 00       	jmp    800729 <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  800682:	8b 45 0c             	mov    0xc(%ebp),%eax
  800685:	89 44 24 04          	mov    %eax,0x4(%esp)
  800689:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  800690:	8b 45 08             	mov    0x8(%ebp),%eax
  800693:	ff d0                	call   *%eax
			putch('X', putdat);
  800695:	8b 45 0c             	mov    0xc(%ebp),%eax
  800698:	89 44 24 04          	mov    %eax,0x4(%esp)
  80069c:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006a3:	8b 45 08             	mov    0x8(%ebp),%eax
  8006a6:	ff d0                	call   *%eax
			putch('X', putdat);
  8006a8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006ab:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006af:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006b6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006b9:	ff d0                	call   *%eax
			break;
  8006bb:	e9 dd 00 00 00       	jmp    80079d <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
  8006c0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006c3:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006c7:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  8006ce:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d1:	ff d0                	call   *%eax
			putch('x', putdat);
  8006d3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006d6:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006da:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  8006e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8006e4:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  8006e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8006e9:	83 c0 04             	add    $0x4,%eax
  8006ec:	89 45 14             	mov    %eax,0x14(%ebp)
  8006ef:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f2:	83 e8 04             	sub    $0x4,%eax
  8006f5:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006f7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006fa:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  800701:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  800708:	eb 1f                	jmp    800729 <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80070a:	8b 45 e8             	mov    -0x18(%ebp),%eax
  80070d:	89 44 24 04          	mov    %eax,0x4(%esp)
  800711:	8d 45 14             	lea    0x14(%ebp),%eax
  800714:	89 04 24             	mov    %eax,(%esp)
  800717:	e8 a7 fb ff ff       	call   8002c3 <getuint>
  80071c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80071f:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  800722:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  800729:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  80072d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800730:	89 54 24 18          	mov    %edx,0x18(%esp)
  800734:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  800737:	89 54 24 14          	mov    %edx,0x14(%esp)
  80073b:	89 44 24 10          	mov    %eax,0x10(%esp)
  80073f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800742:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800745:	89 44 24 08          	mov    %eax,0x8(%esp)
  800749:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80074d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800750:	89 44 24 04          	mov    %eax,0x4(%esp)
  800754:	8b 45 08             	mov    0x8(%ebp),%eax
  800757:	89 04 24             	mov    %eax,(%esp)
  80075a:	e8 86 fa ff ff       	call   8001e5 <printnum>
			break;
  80075f:	eb 3c                	jmp    80079d <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800761:	8b 45 0c             	mov    0xc(%ebp),%eax
  800764:	89 44 24 04          	mov    %eax,0x4(%esp)
  800768:	89 1c 24             	mov    %ebx,(%esp)
  80076b:	8b 45 08             	mov    0x8(%ebp),%eax
  80076e:	ff d0                	call   *%eax
			break;
  800770:	eb 2b                	jmp    80079d <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800772:	8b 45 0c             	mov    0xc(%ebp),%eax
  800775:	89 44 24 04          	mov    %eax,0x4(%esp)
  800779:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  800780:	8b 45 08             	mov    0x8(%ebp),%eax
  800783:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
  800785:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800789:	eb 04                	jmp    80078f <vprintfmt+0x406>
  80078b:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80078f:	8b 45 10             	mov    0x10(%ebp),%eax
  800792:	83 e8 01             	sub    $0x1,%eax
  800795:	0f b6 00             	movzbl (%eax),%eax
  800798:	3c 25                	cmp    $0x25,%al
  80079a:	75 ef                	jne    80078b <vprintfmt+0x402>
				/* do nothing */;
			break;
  80079c:	90                   	nop
		}
	}
  80079d:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80079e:	e9 08 fc ff ff       	jmp    8003ab <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  8007a3:	83 c4 40             	add    $0x40,%esp
  8007a6:	5b                   	pop    %ebx
  8007a7:	5e                   	pop    %esi
  8007a8:	5d                   	pop    %ebp
  8007a9:	c3                   	ret    

008007aa <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8007aa:	55                   	push   %ebp
  8007ab:	89 e5                	mov    %esp,%ebp
  8007ad:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  8007b0:	8d 45 10             	lea    0x10(%ebp),%eax
  8007b3:	83 c0 04             	add    $0x4,%eax
  8007b6:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  8007b9:	8b 45 10             	mov    0x10(%ebp),%eax
  8007bc:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8007bf:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8007c3:	89 44 24 08          	mov    %eax,0x8(%esp)
  8007c7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ca:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007ce:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d1:	89 04 24             	mov    %eax,(%esp)
  8007d4:	e8 b0 fb ff ff       	call   800389 <vprintfmt>
	va_end(ap);
}
  8007d9:	c9                   	leave  
  8007da:	c3                   	ret    

008007db <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8007db:	55                   	push   %ebp
  8007dc:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  8007de:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007e1:	8b 40 08             	mov    0x8(%eax),%eax
  8007e4:	8d 50 01             	lea    0x1(%eax),%edx
  8007e7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ea:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  8007ed:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007f0:	8b 10                	mov    (%eax),%edx
  8007f2:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007f5:	8b 40 04             	mov    0x4(%eax),%eax
  8007f8:	39 c2                	cmp    %eax,%edx
  8007fa:	73 12                	jae    80080e <sprintputch+0x33>
		*b->buf++ = ch;
  8007fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ff:	8b 00                	mov    (%eax),%eax
  800801:	8d 48 01             	lea    0x1(%eax),%ecx
  800804:	8b 55 0c             	mov    0xc(%ebp),%edx
  800807:	89 0a                	mov    %ecx,(%edx)
  800809:	8b 55 08             	mov    0x8(%ebp),%edx
  80080c:	88 10                	mov    %dl,(%eax)
}
  80080e:	5d                   	pop    %ebp
  80080f:	c3                   	ret    

00800810 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800810:	55                   	push   %ebp
  800811:	89 e5                	mov    %esp,%ebp
  800813:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  800816:	8b 45 08             	mov    0x8(%ebp),%eax
  800819:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80081c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80081f:	8d 50 ff             	lea    -0x1(%eax),%edx
  800822:	8b 45 08             	mov    0x8(%ebp),%eax
  800825:	01 d0                	add    %edx,%eax
  800827:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80082a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800831:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800835:	74 06                	je     80083d <vsnprintf+0x2d>
  800837:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80083b:	7f 07                	jg     800844 <vsnprintf+0x34>
		return -E_INVAL;
  80083d:	b8 03 00 00 00       	mov    $0x3,%eax
  800842:	eb 2a                	jmp    80086e <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800844:	8b 45 14             	mov    0x14(%ebp),%eax
  800847:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80084b:	8b 45 10             	mov    0x10(%ebp),%eax
  80084e:	89 44 24 08          	mov    %eax,0x8(%esp)
  800852:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800855:	89 44 24 04          	mov    %eax,0x4(%esp)
  800859:	c7 04 24 db 07 80 00 	movl   $0x8007db,(%esp)
  800860:	e8 24 fb ff ff       	call   800389 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800865:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800868:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80086b:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  80086e:	c9                   	leave  
  80086f:	c3                   	ret    

00800870 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800870:	55                   	push   %ebp
  800871:	89 e5                	mov    %esp,%ebp
  800873:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800876:	8d 45 10             	lea    0x10(%ebp),%eax
  800879:	83 c0 04             	add    $0x4,%eax
  80087c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  80087f:	8b 45 10             	mov    0x10(%ebp),%eax
  800882:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800885:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800889:	89 44 24 08          	mov    %eax,0x8(%esp)
  80088d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800890:	89 44 24 04          	mov    %eax,0x4(%esp)
  800894:	8b 45 08             	mov    0x8(%ebp),%eax
  800897:	89 04 24             	mov    %eax,(%esp)
  80089a:	e8 71 ff ff ff       	call   800810 <vsnprintf>
  80089f:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  8008a2:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8008a5:	c9                   	leave  
  8008a6:	c3                   	ret    

008008a7 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  8008a7:	55                   	push   %ebp
  8008a8:	89 e5                	mov    %esp,%ebp
  8008aa:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  8008ad:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008b4:	eb 08                	jmp    8008be <strlen+0x17>
		n++;
  8008b6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8008ba:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8008be:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c1:	0f b6 00             	movzbl (%eax),%eax
  8008c4:	84 c0                	test   %al,%al
  8008c6:	75 ee                	jne    8008b6 <strlen+0xf>
		n++;
	return n;
  8008c8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008cb:	c9                   	leave  
  8008cc:	c3                   	ret    

008008cd <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  8008cd:	55                   	push   %ebp
  8008ce:	89 e5                	mov    %esp,%ebp
  8008d0:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008d3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008da:	eb 0c                	jmp    8008e8 <strnlen+0x1b>
		n++;
  8008dc:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008e0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8008e4:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  8008e8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8008ec:	74 0a                	je     8008f8 <strnlen+0x2b>
  8008ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f1:	0f b6 00             	movzbl (%eax),%eax
  8008f4:	84 c0                	test   %al,%al
  8008f6:	75 e4                	jne    8008dc <strnlen+0xf>
		n++;
	return n;
  8008f8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008fb:	c9                   	leave  
  8008fc:	c3                   	ret    

008008fd <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8008fd:	55                   	push   %ebp
  8008fe:	89 e5                	mov    %esp,%ebp
  800900:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800903:	8b 45 08             	mov    0x8(%ebp),%eax
  800906:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800909:	90                   	nop
  80090a:	8b 45 08             	mov    0x8(%ebp),%eax
  80090d:	8d 50 01             	lea    0x1(%eax),%edx
  800910:	89 55 08             	mov    %edx,0x8(%ebp)
  800913:	8b 55 0c             	mov    0xc(%ebp),%edx
  800916:	8d 4a 01             	lea    0x1(%edx),%ecx
  800919:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  80091c:	0f b6 12             	movzbl (%edx),%edx
  80091f:	88 10                	mov    %dl,(%eax)
  800921:	0f b6 00             	movzbl (%eax),%eax
  800924:	84 c0                	test   %al,%al
  800926:	75 e2                	jne    80090a <strcpy+0xd>
		/* do nothing */;
	return ret;
  800928:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80092b:	c9                   	leave  
  80092c:	c3                   	ret    

0080092d <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  80092d:	55                   	push   %ebp
  80092e:	89 e5                	mov    %esp,%ebp
  800930:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  800933:	8b 45 08             	mov    0x8(%ebp),%eax
  800936:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800939:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800940:	eb 23                	jmp    800965 <strncpy+0x38>
		*dst++ = *src;
  800942:	8b 45 08             	mov    0x8(%ebp),%eax
  800945:	8d 50 01             	lea    0x1(%eax),%edx
  800948:	89 55 08             	mov    %edx,0x8(%ebp)
  80094b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80094e:	0f b6 12             	movzbl (%edx),%edx
  800951:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  800953:	8b 45 0c             	mov    0xc(%ebp),%eax
  800956:	0f b6 00             	movzbl (%eax),%eax
  800959:	84 c0                	test   %al,%al
  80095b:	74 04                	je     800961 <strncpy+0x34>
			src++;
  80095d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800961:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800965:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800968:	3b 45 10             	cmp    0x10(%ebp),%eax
  80096b:	72 d5                	jb     800942 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  80096d:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800970:	c9                   	leave  
  800971:	c3                   	ret    

00800972 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800972:	55                   	push   %ebp
  800973:	89 e5                	mov    %esp,%ebp
  800975:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800978:	8b 45 08             	mov    0x8(%ebp),%eax
  80097b:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  80097e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800982:	74 33                	je     8009b7 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800984:	eb 17                	jmp    80099d <strlcpy+0x2b>
			*dst++ = *src++;
  800986:	8b 45 08             	mov    0x8(%ebp),%eax
  800989:	8d 50 01             	lea    0x1(%eax),%edx
  80098c:	89 55 08             	mov    %edx,0x8(%ebp)
  80098f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800992:	8d 4a 01             	lea    0x1(%edx),%ecx
  800995:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800998:	0f b6 12             	movzbl (%edx),%edx
  80099b:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80099d:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8009a1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009a5:	74 0a                	je     8009b1 <strlcpy+0x3f>
  8009a7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009aa:	0f b6 00             	movzbl (%eax),%eax
  8009ad:	84 c0                	test   %al,%al
  8009af:	75 d5                	jne    800986 <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  8009b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009b4:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  8009b7:	8b 55 08             	mov    0x8(%ebp),%edx
  8009ba:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8009bd:	29 c2                	sub    %eax,%edx
  8009bf:	89 d0                	mov    %edx,%eax
}
  8009c1:	c9                   	leave  
  8009c2:	c3                   	ret    

008009c3 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8009c3:	55                   	push   %ebp
  8009c4:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  8009c6:	eb 08                	jmp    8009d0 <strcmp+0xd>
		p++, q++;
  8009c8:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009cc:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8009d0:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d3:	0f b6 00             	movzbl (%eax),%eax
  8009d6:	84 c0                	test   %al,%al
  8009d8:	74 10                	je     8009ea <strcmp+0x27>
  8009da:	8b 45 08             	mov    0x8(%ebp),%eax
  8009dd:	0f b6 10             	movzbl (%eax),%edx
  8009e0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009e3:	0f b6 00             	movzbl (%eax),%eax
  8009e6:	38 c2                	cmp    %al,%dl
  8009e8:	74 de                	je     8009c8 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8009ea:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ed:	0f b6 00             	movzbl (%eax),%eax
  8009f0:	0f b6 d0             	movzbl %al,%edx
  8009f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009f6:	0f b6 00             	movzbl (%eax),%eax
  8009f9:	0f b6 c0             	movzbl %al,%eax
  8009fc:	29 c2                	sub    %eax,%edx
  8009fe:	89 d0                	mov    %edx,%eax
}
  800a00:	5d                   	pop    %ebp
  800a01:	c3                   	ret    

00800a02 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800a02:	55                   	push   %ebp
  800a03:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800a05:	eb 0c                	jmp    800a13 <strncmp+0x11>
		n--, p++, q++;
  800a07:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800a0b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a0f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800a13:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a17:	74 1a                	je     800a33 <strncmp+0x31>
  800a19:	8b 45 08             	mov    0x8(%ebp),%eax
  800a1c:	0f b6 00             	movzbl (%eax),%eax
  800a1f:	84 c0                	test   %al,%al
  800a21:	74 10                	je     800a33 <strncmp+0x31>
  800a23:	8b 45 08             	mov    0x8(%ebp),%eax
  800a26:	0f b6 10             	movzbl (%eax),%edx
  800a29:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a2c:	0f b6 00             	movzbl (%eax),%eax
  800a2f:	38 c2                	cmp    %al,%dl
  800a31:	74 d4                	je     800a07 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800a33:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a37:	75 07                	jne    800a40 <strncmp+0x3e>
		return 0;
  800a39:	b8 00 00 00 00       	mov    $0x0,%eax
  800a3e:	eb 16                	jmp    800a56 <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a40:	8b 45 08             	mov    0x8(%ebp),%eax
  800a43:	0f b6 00             	movzbl (%eax),%eax
  800a46:	0f b6 d0             	movzbl %al,%edx
  800a49:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a4c:	0f b6 00             	movzbl (%eax),%eax
  800a4f:	0f b6 c0             	movzbl %al,%eax
  800a52:	29 c2                	sub    %eax,%edx
  800a54:	89 d0                	mov    %edx,%eax
}
  800a56:	5d                   	pop    %ebp
  800a57:	c3                   	ret    

00800a58 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a58:	55                   	push   %ebp
  800a59:	89 e5                	mov    %esp,%ebp
  800a5b:	83 ec 04             	sub    $0x4,%esp
  800a5e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a61:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a64:	eb 14                	jmp    800a7a <strchr+0x22>
		if (*s == c)
  800a66:	8b 45 08             	mov    0x8(%ebp),%eax
  800a69:	0f b6 00             	movzbl (%eax),%eax
  800a6c:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a6f:	75 05                	jne    800a76 <strchr+0x1e>
			return (char *) s;
  800a71:	8b 45 08             	mov    0x8(%ebp),%eax
  800a74:	eb 13                	jmp    800a89 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a76:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a7a:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7d:	0f b6 00             	movzbl (%eax),%eax
  800a80:	84 c0                	test   %al,%al
  800a82:	75 e2                	jne    800a66 <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800a84:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a89:	c9                   	leave  
  800a8a:	c3                   	ret    

00800a8b <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a8b:	55                   	push   %ebp
  800a8c:	89 e5                	mov    %esp,%ebp
  800a8e:	83 ec 04             	sub    $0x4,%esp
  800a91:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a94:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a97:	eb 11                	jmp    800aaa <strfind+0x1f>
		if (*s == c)
  800a99:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9c:	0f b6 00             	movzbl (%eax),%eax
  800a9f:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800aa2:	75 02                	jne    800aa6 <strfind+0x1b>
			break;
  800aa4:	eb 0e                	jmp    800ab4 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800aa6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800aaa:	8b 45 08             	mov    0x8(%ebp),%eax
  800aad:	0f b6 00             	movzbl (%eax),%eax
  800ab0:	84 c0                	test   %al,%al
  800ab2:	75 e5                	jne    800a99 <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
  800ab4:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ab7:	c9                   	leave  
  800ab8:	c3                   	ret    

00800ab9 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800ab9:	55                   	push   %ebp
  800aba:	89 e5                	mov    %esp,%ebp
  800abc:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800abf:	8b 45 08             	mov    0x8(%ebp),%eax
  800ac2:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800ac5:	8b 45 10             	mov    0x10(%ebp),%eax
  800ac8:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800acb:	eb 0e                	jmp    800adb <memset+0x22>
		*p++ = c;
  800acd:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ad0:	8d 50 01             	lea    0x1(%eax),%edx
  800ad3:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800ad6:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ad9:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800adb:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800adf:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800ae3:	79 e8                	jns    800acd <memset+0x14>
		*p++ = c;

	return v;
  800ae5:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ae8:	c9                   	leave  
  800ae9:	c3                   	ret    

00800aea <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800aea:	55                   	push   %ebp
  800aeb:	89 e5                	mov    %esp,%ebp
  800aed:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800af0:	8b 45 0c             	mov    0xc(%ebp),%eax
  800af3:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800af6:	8b 45 08             	mov    0x8(%ebp),%eax
  800af9:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800afc:	eb 17                	jmp    800b15 <memcpy+0x2b>
		*d++ = *s++;
  800afe:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b01:	8d 50 01             	lea    0x1(%eax),%edx
  800b04:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b07:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b0a:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b0d:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b10:	0f b6 12             	movzbl (%edx),%edx
  800b13:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800b15:	8b 45 10             	mov    0x10(%ebp),%eax
  800b18:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b1b:	89 55 10             	mov    %edx,0x10(%ebp)
  800b1e:	85 c0                	test   %eax,%eax
  800b20:	75 dc                	jne    800afe <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800b22:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b25:	c9                   	leave  
  800b26:	c3                   	ret    

00800b27 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800b27:	55                   	push   %ebp
  800b28:	89 e5                	mov    %esp,%ebp
  800b2a:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800b2d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b30:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800b33:	8b 45 08             	mov    0x8(%ebp),%eax
  800b36:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800b39:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b3c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800b3f:	73 3d                	jae    800b7e <memmove+0x57>
  800b41:	8b 45 10             	mov    0x10(%ebp),%eax
  800b44:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b47:	01 d0                	add    %edx,%eax
  800b49:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800b4c:	76 30                	jbe    800b7e <memmove+0x57>
		s += n;
  800b4e:	8b 45 10             	mov    0x10(%ebp),%eax
  800b51:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800b54:	8b 45 10             	mov    0x10(%ebp),%eax
  800b57:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800b5a:	eb 13                	jmp    800b6f <memmove+0x48>
			*--d = *--s;
  800b5c:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800b60:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800b64:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b67:	0f b6 10             	movzbl (%eax),%edx
  800b6a:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b6d:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800b6f:	8b 45 10             	mov    0x10(%ebp),%eax
  800b72:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b75:	89 55 10             	mov    %edx,0x10(%ebp)
  800b78:	85 c0                	test   %eax,%eax
  800b7a:	75 e0                	jne    800b5c <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b7c:	eb 26                	jmp    800ba4 <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b7e:	eb 17                	jmp    800b97 <memmove+0x70>
			*d++ = *s++;
  800b80:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b83:	8d 50 01             	lea    0x1(%eax),%edx
  800b86:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b89:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b8c:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b8f:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b92:	0f b6 12             	movzbl (%edx),%edx
  800b95:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b97:	8b 45 10             	mov    0x10(%ebp),%eax
  800b9a:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b9d:	89 55 10             	mov    %edx,0x10(%ebp)
  800ba0:	85 c0                	test   %eax,%eax
  800ba2:	75 dc                	jne    800b80 <memmove+0x59>
			*d++ = *s++;

	return dst;
  800ba4:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ba7:	c9                   	leave  
  800ba8:	c3                   	ret    

00800ba9 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800ba9:	55                   	push   %ebp
  800baa:	89 e5                	mov    %esp,%ebp
  800bac:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800baf:	8b 45 08             	mov    0x8(%ebp),%eax
  800bb2:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800bb5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bb8:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800bbb:	eb 30                	jmp    800bed <memcmp+0x44>
		if (*s1 != *s2)
  800bbd:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bc0:	0f b6 10             	movzbl (%eax),%edx
  800bc3:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800bc6:	0f b6 00             	movzbl (%eax),%eax
  800bc9:	38 c2                	cmp    %al,%dl
  800bcb:	74 18                	je     800be5 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800bcd:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bd0:	0f b6 00             	movzbl (%eax),%eax
  800bd3:	0f b6 d0             	movzbl %al,%edx
  800bd6:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800bd9:	0f b6 00             	movzbl (%eax),%eax
  800bdc:	0f b6 c0             	movzbl %al,%eax
  800bdf:	29 c2                	sub    %eax,%edx
  800be1:	89 d0                	mov    %edx,%eax
  800be3:	eb 1a                	jmp    800bff <memcmp+0x56>
		s1++, s2++;
  800be5:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800be9:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800bed:	8b 45 10             	mov    0x10(%ebp),%eax
  800bf0:	8d 50 ff             	lea    -0x1(%eax),%edx
  800bf3:	89 55 10             	mov    %edx,0x10(%ebp)
  800bf6:	85 c0                	test   %eax,%eax
  800bf8:	75 c3                	jne    800bbd <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800bfa:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bff:	c9                   	leave  
  800c00:	c3                   	ret    

00800c01 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800c01:	55                   	push   %ebp
  800c02:	89 e5                	mov    %esp,%ebp
  800c04:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800c07:	8b 45 10             	mov    0x10(%ebp),%eax
  800c0a:	8b 55 08             	mov    0x8(%ebp),%edx
  800c0d:	01 d0                	add    %edx,%eax
  800c0f:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800c12:	eb 13                	jmp    800c27 <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c14:	8b 45 08             	mov    0x8(%ebp),%eax
  800c17:	0f b6 10             	movzbl (%eax),%edx
  800c1a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c1d:	38 c2                	cmp    %al,%dl
  800c1f:	75 02                	jne    800c23 <memfind+0x22>
			break;
  800c21:	eb 0c                	jmp    800c2f <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c23:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c27:	8b 45 08             	mov    0x8(%ebp),%eax
  800c2a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800c2d:	72 e5                	jb     800c14 <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
  800c2f:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c32:	c9                   	leave  
  800c33:	c3                   	ret    

00800c34 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c34:	55                   	push   %ebp
  800c35:	89 e5                	mov    %esp,%ebp
  800c37:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800c3a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800c41:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c48:	eb 04                	jmp    800c4e <strtol+0x1a>
		s++;
  800c4a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c4e:	8b 45 08             	mov    0x8(%ebp),%eax
  800c51:	0f b6 00             	movzbl (%eax),%eax
  800c54:	3c 20                	cmp    $0x20,%al
  800c56:	74 f2                	je     800c4a <strtol+0x16>
  800c58:	8b 45 08             	mov    0x8(%ebp),%eax
  800c5b:	0f b6 00             	movzbl (%eax),%eax
  800c5e:	3c 09                	cmp    $0x9,%al
  800c60:	74 e8                	je     800c4a <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c62:	8b 45 08             	mov    0x8(%ebp),%eax
  800c65:	0f b6 00             	movzbl (%eax),%eax
  800c68:	3c 2b                	cmp    $0x2b,%al
  800c6a:	75 06                	jne    800c72 <strtol+0x3e>
		s++;
  800c6c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c70:	eb 15                	jmp    800c87 <strtol+0x53>
	else if (*s == '-')
  800c72:	8b 45 08             	mov    0x8(%ebp),%eax
  800c75:	0f b6 00             	movzbl (%eax),%eax
  800c78:	3c 2d                	cmp    $0x2d,%al
  800c7a:	75 0b                	jne    800c87 <strtol+0x53>
		s++, neg = 1;
  800c7c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c80:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c87:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c8b:	74 06                	je     800c93 <strtol+0x5f>
  800c8d:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c91:	75 24                	jne    800cb7 <strtol+0x83>
  800c93:	8b 45 08             	mov    0x8(%ebp),%eax
  800c96:	0f b6 00             	movzbl (%eax),%eax
  800c99:	3c 30                	cmp    $0x30,%al
  800c9b:	75 1a                	jne    800cb7 <strtol+0x83>
  800c9d:	8b 45 08             	mov    0x8(%ebp),%eax
  800ca0:	83 c0 01             	add    $0x1,%eax
  800ca3:	0f b6 00             	movzbl (%eax),%eax
  800ca6:	3c 78                	cmp    $0x78,%al
  800ca8:	75 0d                	jne    800cb7 <strtol+0x83>
		s += 2, base = 16;
  800caa:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800cae:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800cb5:	eb 2a                	jmp    800ce1 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800cb7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800cbb:	75 17                	jne    800cd4 <strtol+0xa0>
  800cbd:	8b 45 08             	mov    0x8(%ebp),%eax
  800cc0:	0f b6 00             	movzbl (%eax),%eax
  800cc3:	3c 30                	cmp    $0x30,%al
  800cc5:	75 0d                	jne    800cd4 <strtol+0xa0>
		s++, base = 8;
  800cc7:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ccb:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800cd2:	eb 0d                	jmp    800ce1 <strtol+0xad>
	else if (base == 0)
  800cd4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800cd8:	75 07                	jne    800ce1 <strtol+0xad>
		base = 10;
  800cda:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800ce1:	8b 45 08             	mov    0x8(%ebp),%eax
  800ce4:	0f b6 00             	movzbl (%eax),%eax
  800ce7:	3c 2f                	cmp    $0x2f,%al
  800ce9:	7e 1b                	jle    800d06 <strtol+0xd2>
  800ceb:	8b 45 08             	mov    0x8(%ebp),%eax
  800cee:	0f b6 00             	movzbl (%eax),%eax
  800cf1:	3c 39                	cmp    $0x39,%al
  800cf3:	7f 11                	jg     800d06 <strtol+0xd2>
			dig = *s - '0';
  800cf5:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf8:	0f b6 00             	movzbl (%eax),%eax
  800cfb:	0f be c0             	movsbl %al,%eax
  800cfe:	83 e8 30             	sub    $0x30,%eax
  800d01:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800d04:	eb 48                	jmp    800d4e <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800d06:	8b 45 08             	mov    0x8(%ebp),%eax
  800d09:	0f b6 00             	movzbl (%eax),%eax
  800d0c:	3c 60                	cmp    $0x60,%al
  800d0e:	7e 1b                	jle    800d2b <strtol+0xf7>
  800d10:	8b 45 08             	mov    0x8(%ebp),%eax
  800d13:	0f b6 00             	movzbl (%eax),%eax
  800d16:	3c 7a                	cmp    $0x7a,%al
  800d18:	7f 11                	jg     800d2b <strtol+0xf7>
			dig = *s - 'a' + 10;
  800d1a:	8b 45 08             	mov    0x8(%ebp),%eax
  800d1d:	0f b6 00             	movzbl (%eax),%eax
  800d20:	0f be c0             	movsbl %al,%eax
  800d23:	83 e8 57             	sub    $0x57,%eax
  800d26:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800d29:	eb 23                	jmp    800d4e <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800d2b:	8b 45 08             	mov    0x8(%ebp),%eax
  800d2e:	0f b6 00             	movzbl (%eax),%eax
  800d31:	3c 40                	cmp    $0x40,%al
  800d33:	7e 3d                	jle    800d72 <strtol+0x13e>
  800d35:	8b 45 08             	mov    0x8(%ebp),%eax
  800d38:	0f b6 00             	movzbl (%eax),%eax
  800d3b:	3c 5a                	cmp    $0x5a,%al
  800d3d:	7f 33                	jg     800d72 <strtol+0x13e>
			dig = *s - 'A' + 10;
  800d3f:	8b 45 08             	mov    0x8(%ebp),%eax
  800d42:	0f b6 00             	movzbl (%eax),%eax
  800d45:	0f be c0             	movsbl %al,%eax
  800d48:	83 e8 37             	sub    $0x37,%eax
  800d4b:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800d4e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800d51:	3b 45 10             	cmp    0x10(%ebp),%eax
  800d54:	7c 02                	jl     800d58 <strtol+0x124>
			break;
  800d56:	eb 1a                	jmp    800d72 <strtol+0x13e>
		s++, val = (val * base) + dig;
  800d58:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d5c:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d5f:	0f af 45 10          	imul   0x10(%ebp),%eax
  800d63:	89 c2                	mov    %eax,%edx
  800d65:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800d68:	01 d0                	add    %edx,%eax
  800d6a:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800d6d:	e9 6f ff ff ff       	jmp    800ce1 <strtol+0xad>

	if (endptr)
  800d72:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800d76:	74 08                	je     800d80 <strtol+0x14c>
		*endptr = (char *) s;
  800d78:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d7b:	8b 55 08             	mov    0x8(%ebp),%edx
  800d7e:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800d80:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800d84:	74 07                	je     800d8d <strtol+0x159>
  800d86:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d89:	f7 d8                	neg    %eax
  800d8b:	eb 03                	jmp    800d90 <strtol+0x15c>
  800d8d:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800d90:	c9                   	leave  
  800d91:	c3                   	ret    

00800d92 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800d92:	55                   	push   %ebp
  800d93:	89 e5                	mov    %esp,%ebp
  800d95:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800d98:	8b 45 14             	mov    0x14(%ebp),%eax
  800d9b:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800da1:	8b 45 14             	mov    0x14(%ebp),%eax
  800da4:	8b 00                	mov    (%eax),%eax
  800da6:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800dad:	8b 45 10             	mov    0x10(%ebp),%eax
  800db0:	01 d0                	add    %edx,%eax
  800db2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800db8:	eb 0c                	jmp    800dc6 <strsplit+0x34>
			*string++ = 0;
  800dba:	8b 45 08             	mov    0x8(%ebp),%eax
  800dbd:	8d 50 01             	lea    0x1(%eax),%edx
  800dc0:	89 55 08             	mov    %edx,0x8(%ebp)
  800dc3:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800dc6:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc9:	0f b6 00             	movzbl (%eax),%eax
  800dcc:	84 c0                	test   %al,%al
  800dce:	74 1c                	je     800dec <strsplit+0x5a>
  800dd0:	8b 45 08             	mov    0x8(%ebp),%eax
  800dd3:	0f b6 00             	movzbl (%eax),%eax
  800dd6:	0f be c0             	movsbl %al,%eax
  800dd9:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ddd:	8b 45 0c             	mov    0xc(%ebp),%eax
  800de0:	89 04 24             	mov    %eax,(%esp)
  800de3:	e8 70 fc ff ff       	call   800a58 <strchr>
  800de8:	85 c0                	test   %eax,%eax
  800dea:	75 ce                	jne    800dba <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800dec:	8b 45 08             	mov    0x8(%ebp),%eax
  800def:	0f b6 00             	movzbl (%eax),%eax
  800df2:	84 c0                	test   %al,%al
  800df4:	75 1f                	jne    800e15 <strsplit+0x83>
			break;
  800df6:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800df7:	8b 45 14             	mov    0x14(%ebp),%eax
  800dfa:	8b 00                	mov    (%eax),%eax
  800dfc:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e03:	8b 45 10             	mov    0x10(%ebp),%eax
  800e06:	01 d0                	add    %edx,%eax
  800e08:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800e0e:	b8 01 00 00 00       	mov    $0x1,%eax
  800e13:	eb 61                	jmp    800e76 <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800e15:	8b 45 14             	mov    0x14(%ebp),%eax
  800e18:	8b 00                	mov    (%eax),%eax
  800e1a:	83 f8 0f             	cmp    $0xf,%eax
  800e1d:	75 07                	jne    800e26 <strsplit+0x94>
		{
			return 0;
  800e1f:	b8 00 00 00 00       	mov    $0x0,%eax
  800e24:	eb 50                	jmp    800e76 <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800e26:	8b 45 14             	mov    0x14(%ebp),%eax
  800e29:	8b 00                	mov    (%eax),%eax
  800e2b:	8d 48 01             	lea    0x1(%eax),%ecx
  800e2e:	8b 55 14             	mov    0x14(%ebp),%edx
  800e31:	89 0a                	mov    %ecx,(%edx)
  800e33:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e3a:	8b 45 10             	mov    0x10(%ebp),%eax
  800e3d:	01 c2                	add    %eax,%edx
  800e3f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e42:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800e44:	eb 04                	jmp    800e4a <strsplit+0xb8>
			string++;
  800e46:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800e4a:	8b 45 08             	mov    0x8(%ebp),%eax
  800e4d:	0f b6 00             	movzbl (%eax),%eax
  800e50:	84 c0                	test   %al,%al
  800e52:	74 1c                	je     800e70 <strsplit+0xde>
  800e54:	8b 45 08             	mov    0x8(%ebp),%eax
  800e57:	0f b6 00             	movzbl (%eax),%eax
  800e5a:	0f be c0             	movsbl %al,%eax
  800e5d:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e61:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e64:	89 04 24             	mov    %eax,(%esp)
  800e67:	e8 ec fb ff ff       	call   800a58 <strchr>
  800e6c:	85 c0                	test   %eax,%eax
  800e6e:	74 d6                	je     800e46 <strsplit+0xb4>
			string++;
	}
  800e70:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e71:	e9 50 ff ff ff       	jmp    800dc6 <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
  800e76:	c9                   	leave  
  800e77:	c3                   	ret    

00800e78 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800e78:	55                   	push   %ebp
  800e79:	89 e5                	mov    %esp,%ebp
  800e7b:	57                   	push   %edi
  800e7c:	56                   	push   %esi
  800e7d:	53                   	push   %ebx
  800e7e:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e81:	8b 45 08             	mov    0x8(%ebp),%eax
  800e84:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e87:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800e8a:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800e8d:	8b 7d 18             	mov    0x18(%ebp),%edi
  800e90:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800e93:	cd 30                	int    $0x30
  800e95:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800e98:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800e9b:	83 c4 10             	add    $0x10,%esp
  800e9e:	5b                   	pop    %ebx
  800e9f:	5e                   	pop    %esi
  800ea0:	5f                   	pop    %edi
  800ea1:	5d                   	pop    %ebp
  800ea2:	c3                   	ret    

00800ea3 <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800ea3:	55                   	push   %ebp
  800ea4:	89 e5                	mov    %esp,%ebp
  800ea6:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800ea9:	8b 45 08             	mov    0x8(%ebp),%eax
  800eac:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800eb3:	00 
  800eb4:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800ebb:	00 
  800ebc:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800ec3:	00 
  800ec4:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ec7:	89 54 24 08          	mov    %edx,0x8(%esp)
  800ecb:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ecf:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800ed6:	e8 9d ff ff ff       	call   800e78 <syscall>
}
  800edb:	c9                   	leave  
  800edc:	c3                   	ret    

00800edd <sys_cgetc>:

int
sys_cgetc(void)
{
  800edd:	55                   	push   %ebp
  800ede:	89 e5                	mov    %esp,%ebp
  800ee0:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800ee3:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800eea:	00 
  800eeb:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800ef2:	00 
  800ef3:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800efa:	00 
  800efb:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f02:	00 
  800f03:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f0a:	00 
  800f0b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800f12:	e8 61 ff ff ff       	call   800e78 <syscall>
}
  800f17:	c9                   	leave  
  800f18:	c3                   	ret    

00800f19 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800f19:	55                   	push   %ebp
  800f1a:	89 e5                	mov    %esp,%ebp
  800f1c:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800f1f:	8b 45 08             	mov    0x8(%ebp),%eax
  800f22:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f29:	00 
  800f2a:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f31:	00 
  800f32:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f39:	00 
  800f3a:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f41:	00 
  800f42:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f46:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  800f4d:	e8 26 ff ff ff       	call   800e78 <syscall>
}
  800f52:	c9                   	leave  
  800f53:	c3                   	ret    

00800f54 <sys_getenvid>:

int32 sys_getenvid(void)
{
  800f54:	55                   	push   %ebp
  800f55:	89 e5                	mov    %esp,%ebp
  800f57:	83 ec 18             	sub    $0x18,%esp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800f5a:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f61:	00 
  800f62:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f69:	00 
  800f6a:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f71:	00 
  800f72:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f79:	00 
  800f7a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f81:	00 
  800f82:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  800f89:	e8 ea fe ff ff       	call   800e78 <syscall>
}
  800f8e:	c9                   	leave  
  800f8f:	c3                   	ret    

00800f90 <sys_env_sleep>:

void sys_env_sleep(void)
{
  800f90:	55                   	push   %ebp
  800f91:	89 e5                	mov    %esp,%ebp
  800f93:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800f96:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f9d:	00 
  800f9e:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800fa5:	00 
  800fa6:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800fad:	00 
  800fae:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800fb5:	00 
  800fb6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800fbd:	00 
  800fbe:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  800fc5:	e8 ae fe ff ff       	call   800e78 <syscall>
}
  800fca:	c9                   	leave  
  800fcb:	c3                   	ret    

00800fcc <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800fcc:	55                   	push   %ebp
  800fcd:	89 e5                	mov    %esp,%ebp
  800fcf:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800fd2:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fd5:	8b 45 08             	mov    0x8(%ebp),%eax
  800fd8:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800fdf:	00 
  800fe0:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800fe7:	00 
  800fe8:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800fef:	00 
  800ff0:	89 54 24 08          	mov    %edx,0x8(%esp)
  800ff4:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ff8:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  800fff:	e8 74 fe ff ff       	call   800e78 <syscall>
}
  801004:	c9                   	leave  
  801005:	c3                   	ret    

00801006 <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  801006:	55                   	push   %ebp
  801007:	89 e5                	mov    %esp,%ebp
  801009:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  80100c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80100f:	8b 45 08             	mov    0x8(%ebp),%eax
  801012:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801019:	00 
  80101a:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801021:	00 
  801022:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801029:	00 
  80102a:	89 54 24 08          	mov    %edx,0x8(%esp)
  80102e:	89 44 24 04          	mov    %eax,0x4(%esp)
  801032:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
  801039:	e8 3a fe ff ff       	call   800e78 <syscall>
}
  80103e:	c9                   	leave  
  80103f:	c3                   	ret    

00801040 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  801040:	55                   	push   %ebp
  801041:	89 e5                	mov    %esp,%ebp
  801043:	56                   	push   %esi
  801044:	53                   	push   %ebx
  801045:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  801048:	8b 75 18             	mov    0x18(%ebp),%esi
  80104b:	8b 5d 14             	mov    0x14(%ebp),%ebx
  80104e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801051:	8b 55 0c             	mov    0xc(%ebp),%edx
  801054:	8b 45 08             	mov    0x8(%ebp),%eax
  801057:	89 74 24 14          	mov    %esi,0x14(%esp)
  80105b:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  80105f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801063:	89 54 24 08          	mov    %edx,0x8(%esp)
  801067:	89 44 24 04          	mov    %eax,0x4(%esp)
  80106b:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  801072:	e8 01 fe ff ff       	call   800e78 <syscall>
}
  801077:	83 c4 18             	add    $0x18,%esp
  80107a:	5b                   	pop    %ebx
  80107b:	5e                   	pop    %esi
  80107c:	5d                   	pop    %ebp
  80107d:	c3                   	ret    

0080107e <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  80107e:	55                   	push   %ebp
  80107f:	89 e5                	mov    %esp,%ebp
  801081:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  801084:	8b 55 0c             	mov    0xc(%ebp),%edx
  801087:	8b 45 08             	mov    0x8(%ebp),%eax
  80108a:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801091:	00 
  801092:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801099:	00 
  80109a:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010a1:	00 
  8010a2:	89 54 24 08          	mov    %edx,0x8(%esp)
  8010a6:	89 44 24 04          	mov    %eax,0x4(%esp)
  8010aa:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  8010b1:	e8 c2 fd ff ff       	call   800e78 <syscall>
}
  8010b6:	c9                   	leave  
  8010b7:	c3                   	ret    

008010b8 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  8010b8:	55                   	push   %ebp
  8010b9:	89 e5                	mov    %esp,%ebp
  8010bb:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  8010be:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010c5:	00 
  8010c6:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010cd:	00 
  8010ce:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010d5:	00 
  8010d6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8010d9:	89 44 24 08          	mov    %eax,0x8(%esp)
  8010dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8010e0:	89 44 24 04          	mov    %eax,0x4(%esp)
  8010e4:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
  8010eb:	e8 88 fd ff ff       	call   800e78 <syscall>
}
  8010f0:	c9                   	leave  
  8010f1:	c3                   	ret    

008010f2 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  8010f2:	55                   	push   %ebp
  8010f3:	89 e5                	mov    %esp,%ebp
  8010f5:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  8010f8:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010ff:	00 
  801100:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801107:	00 
  801108:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80110f:	00 
  801110:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801117:	00 
  801118:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80111f:	00 
  801120:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  801127:	e8 4c fd ff ff       	call   800e78 <syscall>
}
  80112c:	c9                   	leave  
  80112d:	c3                   	ret    

0080112e <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  80112e:	55                   	push   %ebp
  80112f:	89 e5                	mov    %esp,%ebp
  801131:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  801134:	8b 45 08             	mov    0x8(%ebp),%eax
  801137:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80113e:	00 
  80113f:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801146:	00 
  801147:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80114e:	00 
  80114f:	8b 55 0c             	mov    0xc(%ebp),%edx
  801152:	89 54 24 08          	mov    %edx,0x8(%esp)
  801156:	89 44 24 04          	mov    %eax,0x4(%esp)
  80115a:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
  801161:	e8 12 fd ff ff       	call   800e78 <syscall>
	return;
  801166:	90                   	nop
}
  801167:	c9                   	leave  
  801168:	c3                   	ret    
  801169:	66 90                	xchg   %ax,%ax
  80116b:	66 90                	xchg   %ax,%ax
  80116d:	66 90                	xchg   %ax,%ax
  80116f:	90                   	nop

00801170 <__udivdi3>:
  801170:	55                   	push   %ebp
  801171:	57                   	push   %edi
  801172:	56                   	push   %esi
  801173:	83 ec 0c             	sub    $0xc,%esp
  801176:	8b 44 24 28          	mov    0x28(%esp),%eax
  80117a:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
  80117e:	8b 6c 24 20          	mov    0x20(%esp),%ebp
  801182:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  801186:	85 c0                	test   %eax,%eax
  801188:	89 7c 24 04          	mov    %edi,0x4(%esp)
  80118c:	89 ea                	mov    %ebp,%edx
  80118e:	89 0c 24             	mov    %ecx,(%esp)
  801191:	75 2d                	jne    8011c0 <__udivdi3+0x50>
  801193:	39 e9                	cmp    %ebp,%ecx
  801195:	77 61                	ja     8011f8 <__udivdi3+0x88>
  801197:	85 c9                	test   %ecx,%ecx
  801199:	89 ce                	mov    %ecx,%esi
  80119b:	75 0b                	jne    8011a8 <__udivdi3+0x38>
  80119d:	b8 01 00 00 00       	mov    $0x1,%eax
  8011a2:	31 d2                	xor    %edx,%edx
  8011a4:	f7 f1                	div    %ecx
  8011a6:	89 c6                	mov    %eax,%esi
  8011a8:	31 d2                	xor    %edx,%edx
  8011aa:	89 e8                	mov    %ebp,%eax
  8011ac:	f7 f6                	div    %esi
  8011ae:	89 c5                	mov    %eax,%ebp
  8011b0:	89 f8                	mov    %edi,%eax
  8011b2:	f7 f6                	div    %esi
  8011b4:	89 ea                	mov    %ebp,%edx
  8011b6:	83 c4 0c             	add    $0xc,%esp
  8011b9:	5e                   	pop    %esi
  8011ba:	5f                   	pop    %edi
  8011bb:	5d                   	pop    %ebp
  8011bc:	c3                   	ret    
  8011bd:	8d 76 00             	lea    0x0(%esi),%esi
  8011c0:	39 e8                	cmp    %ebp,%eax
  8011c2:	77 24                	ja     8011e8 <__udivdi3+0x78>
  8011c4:	0f bd e8             	bsr    %eax,%ebp
  8011c7:	83 f5 1f             	xor    $0x1f,%ebp
  8011ca:	75 3c                	jne    801208 <__udivdi3+0x98>
  8011cc:	8b 74 24 04          	mov    0x4(%esp),%esi
  8011d0:	39 34 24             	cmp    %esi,(%esp)
  8011d3:	0f 86 9f 00 00 00    	jbe    801278 <__udivdi3+0x108>
  8011d9:	39 d0                	cmp    %edx,%eax
  8011db:	0f 82 97 00 00 00    	jb     801278 <__udivdi3+0x108>
  8011e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8011e8:	31 d2                	xor    %edx,%edx
  8011ea:	31 c0                	xor    %eax,%eax
  8011ec:	83 c4 0c             	add    $0xc,%esp
  8011ef:	5e                   	pop    %esi
  8011f0:	5f                   	pop    %edi
  8011f1:	5d                   	pop    %ebp
  8011f2:	c3                   	ret    
  8011f3:	90                   	nop
  8011f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8011f8:	89 f8                	mov    %edi,%eax
  8011fa:	f7 f1                	div    %ecx
  8011fc:	31 d2                	xor    %edx,%edx
  8011fe:	83 c4 0c             	add    $0xc,%esp
  801201:	5e                   	pop    %esi
  801202:	5f                   	pop    %edi
  801203:	5d                   	pop    %ebp
  801204:	c3                   	ret    
  801205:	8d 76 00             	lea    0x0(%esi),%esi
  801208:	89 e9                	mov    %ebp,%ecx
  80120a:	8b 3c 24             	mov    (%esp),%edi
  80120d:	d3 e0                	shl    %cl,%eax
  80120f:	89 c6                	mov    %eax,%esi
  801211:	b8 20 00 00 00       	mov    $0x20,%eax
  801216:	29 e8                	sub    %ebp,%eax
  801218:	89 c1                	mov    %eax,%ecx
  80121a:	d3 ef                	shr    %cl,%edi
  80121c:	89 e9                	mov    %ebp,%ecx
  80121e:	89 7c 24 08          	mov    %edi,0x8(%esp)
  801222:	8b 3c 24             	mov    (%esp),%edi
  801225:	09 74 24 08          	or     %esi,0x8(%esp)
  801229:	89 d6                	mov    %edx,%esi
  80122b:	d3 e7                	shl    %cl,%edi
  80122d:	89 c1                	mov    %eax,%ecx
  80122f:	89 3c 24             	mov    %edi,(%esp)
  801232:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801236:	d3 ee                	shr    %cl,%esi
  801238:	89 e9                	mov    %ebp,%ecx
  80123a:	d3 e2                	shl    %cl,%edx
  80123c:	89 c1                	mov    %eax,%ecx
  80123e:	d3 ef                	shr    %cl,%edi
  801240:	09 d7                	or     %edx,%edi
  801242:	89 f2                	mov    %esi,%edx
  801244:	89 f8                	mov    %edi,%eax
  801246:	f7 74 24 08          	divl   0x8(%esp)
  80124a:	89 d6                	mov    %edx,%esi
  80124c:	89 c7                	mov    %eax,%edi
  80124e:	f7 24 24             	mull   (%esp)
  801251:	39 d6                	cmp    %edx,%esi
  801253:	89 14 24             	mov    %edx,(%esp)
  801256:	72 30                	jb     801288 <__udivdi3+0x118>
  801258:	8b 54 24 04          	mov    0x4(%esp),%edx
  80125c:	89 e9                	mov    %ebp,%ecx
  80125e:	d3 e2                	shl    %cl,%edx
  801260:	39 c2                	cmp    %eax,%edx
  801262:	73 05                	jae    801269 <__udivdi3+0xf9>
  801264:	3b 34 24             	cmp    (%esp),%esi
  801267:	74 1f                	je     801288 <__udivdi3+0x118>
  801269:	89 f8                	mov    %edi,%eax
  80126b:	31 d2                	xor    %edx,%edx
  80126d:	e9 7a ff ff ff       	jmp    8011ec <__udivdi3+0x7c>
  801272:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801278:	31 d2                	xor    %edx,%edx
  80127a:	b8 01 00 00 00       	mov    $0x1,%eax
  80127f:	e9 68 ff ff ff       	jmp    8011ec <__udivdi3+0x7c>
  801284:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801288:	8d 47 ff             	lea    -0x1(%edi),%eax
  80128b:	31 d2                	xor    %edx,%edx
  80128d:	83 c4 0c             	add    $0xc,%esp
  801290:	5e                   	pop    %esi
  801291:	5f                   	pop    %edi
  801292:	5d                   	pop    %ebp
  801293:	c3                   	ret    
  801294:	66 90                	xchg   %ax,%ax
  801296:	66 90                	xchg   %ax,%ax
  801298:	66 90                	xchg   %ax,%ax
  80129a:	66 90                	xchg   %ax,%ax
  80129c:	66 90                	xchg   %ax,%ax
  80129e:	66 90                	xchg   %ax,%ax

008012a0 <__umoddi3>:
  8012a0:	55                   	push   %ebp
  8012a1:	57                   	push   %edi
  8012a2:	56                   	push   %esi
  8012a3:	83 ec 14             	sub    $0x14,%esp
  8012a6:	8b 44 24 28          	mov    0x28(%esp),%eax
  8012aa:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  8012ae:	8b 74 24 2c          	mov    0x2c(%esp),%esi
  8012b2:	89 c7                	mov    %eax,%edi
  8012b4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8012b8:	8b 44 24 30          	mov    0x30(%esp),%eax
  8012bc:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  8012c0:	89 34 24             	mov    %esi,(%esp)
  8012c3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8012c7:	85 c0                	test   %eax,%eax
  8012c9:	89 c2                	mov    %eax,%edx
  8012cb:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8012cf:	75 17                	jne    8012e8 <__umoddi3+0x48>
  8012d1:	39 fe                	cmp    %edi,%esi
  8012d3:	76 4b                	jbe    801320 <__umoddi3+0x80>
  8012d5:	89 c8                	mov    %ecx,%eax
  8012d7:	89 fa                	mov    %edi,%edx
  8012d9:	f7 f6                	div    %esi
  8012db:	89 d0                	mov    %edx,%eax
  8012dd:	31 d2                	xor    %edx,%edx
  8012df:	83 c4 14             	add    $0x14,%esp
  8012e2:	5e                   	pop    %esi
  8012e3:	5f                   	pop    %edi
  8012e4:	5d                   	pop    %ebp
  8012e5:	c3                   	ret    
  8012e6:	66 90                	xchg   %ax,%ax
  8012e8:	39 f8                	cmp    %edi,%eax
  8012ea:	77 54                	ja     801340 <__umoddi3+0xa0>
  8012ec:	0f bd e8             	bsr    %eax,%ebp
  8012ef:	83 f5 1f             	xor    $0x1f,%ebp
  8012f2:	75 5c                	jne    801350 <__umoddi3+0xb0>
  8012f4:	8b 7c 24 08          	mov    0x8(%esp),%edi
  8012f8:	39 3c 24             	cmp    %edi,(%esp)
  8012fb:	0f 87 e7 00 00 00    	ja     8013e8 <__umoddi3+0x148>
  801301:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801305:	29 f1                	sub    %esi,%ecx
  801307:	19 c7                	sbb    %eax,%edi
  801309:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80130d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801311:	8b 44 24 08          	mov    0x8(%esp),%eax
  801315:	8b 54 24 0c          	mov    0xc(%esp),%edx
  801319:	83 c4 14             	add    $0x14,%esp
  80131c:	5e                   	pop    %esi
  80131d:	5f                   	pop    %edi
  80131e:	5d                   	pop    %ebp
  80131f:	c3                   	ret    
  801320:	85 f6                	test   %esi,%esi
  801322:	89 f5                	mov    %esi,%ebp
  801324:	75 0b                	jne    801331 <__umoddi3+0x91>
  801326:	b8 01 00 00 00       	mov    $0x1,%eax
  80132b:	31 d2                	xor    %edx,%edx
  80132d:	f7 f6                	div    %esi
  80132f:	89 c5                	mov    %eax,%ebp
  801331:	8b 44 24 04          	mov    0x4(%esp),%eax
  801335:	31 d2                	xor    %edx,%edx
  801337:	f7 f5                	div    %ebp
  801339:	89 c8                	mov    %ecx,%eax
  80133b:	f7 f5                	div    %ebp
  80133d:	eb 9c                	jmp    8012db <__umoddi3+0x3b>
  80133f:	90                   	nop
  801340:	89 c8                	mov    %ecx,%eax
  801342:	89 fa                	mov    %edi,%edx
  801344:	83 c4 14             	add    $0x14,%esp
  801347:	5e                   	pop    %esi
  801348:	5f                   	pop    %edi
  801349:	5d                   	pop    %ebp
  80134a:	c3                   	ret    
  80134b:	90                   	nop
  80134c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801350:	8b 04 24             	mov    (%esp),%eax
  801353:	be 20 00 00 00       	mov    $0x20,%esi
  801358:	89 e9                	mov    %ebp,%ecx
  80135a:	29 ee                	sub    %ebp,%esi
  80135c:	d3 e2                	shl    %cl,%edx
  80135e:	89 f1                	mov    %esi,%ecx
  801360:	d3 e8                	shr    %cl,%eax
  801362:	89 e9                	mov    %ebp,%ecx
  801364:	89 44 24 04          	mov    %eax,0x4(%esp)
  801368:	8b 04 24             	mov    (%esp),%eax
  80136b:	09 54 24 04          	or     %edx,0x4(%esp)
  80136f:	89 fa                	mov    %edi,%edx
  801371:	d3 e0                	shl    %cl,%eax
  801373:	89 f1                	mov    %esi,%ecx
  801375:	89 44 24 08          	mov    %eax,0x8(%esp)
  801379:	8b 44 24 10          	mov    0x10(%esp),%eax
  80137d:	d3 ea                	shr    %cl,%edx
  80137f:	89 e9                	mov    %ebp,%ecx
  801381:	d3 e7                	shl    %cl,%edi
  801383:	89 f1                	mov    %esi,%ecx
  801385:	d3 e8                	shr    %cl,%eax
  801387:	89 e9                	mov    %ebp,%ecx
  801389:	09 f8                	or     %edi,%eax
  80138b:	8b 7c 24 10          	mov    0x10(%esp),%edi
  80138f:	f7 74 24 04          	divl   0x4(%esp)
  801393:	d3 e7                	shl    %cl,%edi
  801395:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801399:	89 d7                	mov    %edx,%edi
  80139b:	f7 64 24 08          	mull   0x8(%esp)
  80139f:	39 d7                	cmp    %edx,%edi
  8013a1:	89 c1                	mov    %eax,%ecx
  8013a3:	89 14 24             	mov    %edx,(%esp)
  8013a6:	72 2c                	jb     8013d4 <__umoddi3+0x134>
  8013a8:	39 44 24 0c          	cmp    %eax,0xc(%esp)
  8013ac:	72 22                	jb     8013d0 <__umoddi3+0x130>
  8013ae:	8b 44 24 0c          	mov    0xc(%esp),%eax
  8013b2:	29 c8                	sub    %ecx,%eax
  8013b4:	19 d7                	sbb    %edx,%edi
  8013b6:	89 e9                	mov    %ebp,%ecx
  8013b8:	89 fa                	mov    %edi,%edx
  8013ba:	d3 e8                	shr    %cl,%eax
  8013bc:	89 f1                	mov    %esi,%ecx
  8013be:	d3 e2                	shl    %cl,%edx
  8013c0:	89 e9                	mov    %ebp,%ecx
  8013c2:	d3 ef                	shr    %cl,%edi
  8013c4:	09 d0                	or     %edx,%eax
  8013c6:	89 fa                	mov    %edi,%edx
  8013c8:	83 c4 14             	add    $0x14,%esp
  8013cb:	5e                   	pop    %esi
  8013cc:	5f                   	pop    %edi
  8013cd:	5d                   	pop    %ebp
  8013ce:	c3                   	ret    
  8013cf:	90                   	nop
  8013d0:	39 d7                	cmp    %edx,%edi
  8013d2:	75 da                	jne    8013ae <__umoddi3+0x10e>
  8013d4:	8b 14 24             	mov    (%esp),%edx
  8013d7:	89 c1                	mov    %eax,%ecx
  8013d9:	2b 4c 24 08          	sub    0x8(%esp),%ecx
  8013dd:	1b 54 24 04          	sbb    0x4(%esp),%edx
  8013e1:	eb cb                	jmp    8013ae <__umoddi3+0x10e>
  8013e3:	90                   	nop
  8013e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8013e8:	3b 44 24 0c          	cmp    0xc(%esp),%eax
  8013ec:	0f 82 0f ff ff ff    	jb     801301 <__umoddi3+0x61>
  8013f2:	e9 1a ff ff ff       	jmp    801311 <__umoddi3+0x71>
