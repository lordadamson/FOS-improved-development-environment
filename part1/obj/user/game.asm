
obj/user/game:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 79 00 00 00       	call   8000af <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
#include <inc/lib.h>
	
void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 28             	sub    $0x28,%esp
	int i=28;
  80003e:	c7 45 f4 1c 00 00 00 	movl   $0x1c,-0xc(%ebp)
	for(;i<128; i++)
  800045:	eb 5f                	jmp    8000a6 <_main+0x6e>
	{
		int c=0;
  800047:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
		for(;c<10; c++)
  80004e:	eb 17                	jmp    800067 <_main+0x2f>
		{
			cprintf("%c",i);
  800050:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800053:	89 44 24 04          	mov    %eax,0x4(%esp)
  800057:	c7 04 24 20 14 80 00 	movl   $0x801420,(%esp)
  80005e:	e8 66 01 00 00       	call   8001c9 <cprintf>
{	
	int i=28;
	for(;i<128; i++)
	{
		int c=0;
		for(;c<10; c++)
  800063:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  800067:	83 7d f0 09          	cmpl   $0x9,-0x10(%ebp)
  80006b:	7e e3                	jle    800050 <_main+0x18>
		{
			cprintf("%c",i);
		}
		int d=0;
  80006d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
		for(; d< 500000; d++);	
  800074:	eb 04                	jmp    80007a <_main+0x42>
  800076:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  80007a:	81 7d ec 1f a1 07 00 	cmpl   $0x7a11f,-0x14(%ebp)
  800081:	7e f3                	jle    800076 <_main+0x3e>
		c=0;
  800083:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
		for(;c<10; c++)
  80008a:	eb 10                	jmp    80009c <_main+0x64>
		{
			cprintf("\b");
  80008c:	c7 04 24 23 14 80 00 	movl   $0x801423,(%esp)
  800093:	e8 31 01 00 00       	call   8001c9 <cprintf>
			cprintf("%c",i);
		}
		int d=0;
		for(; d< 500000; d++);	
		c=0;
		for(;c<10; c++)
  800098:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  80009c:	83 7d f0 09          	cmpl   $0x9,-0x10(%ebp)
  8000a0:	7e ea                	jle    80008c <_main+0x54>
	
void
_main(void)
{	
	int i=28;
	for(;i<128; i++)
  8000a2:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8000a6:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
  8000aa:	7e 9b                	jle    800047 <_main+0xf>
		{
			cprintf("\b");
		}		
	}
	
	return;	
  8000ac:	90                   	nop
}
  8000ad:	c9                   	leave  
  8000ae:	c3                   	ret    

008000af <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8000af:	55                   	push   %ebp
  8000b0:	89 e5                	mov    %esp,%ebp
  8000b2:	83 ec 18             	sub    $0x18,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8000b5:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000bc:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000bf:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000c3:	7e 0a                	jle    8000cf <libmain+0x20>
		binaryname = argv[0];
  8000c5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000c8:	8b 00                	mov    (%eax),%eax
  8000ca:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000cf:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000d2:	89 44 24 04          	mov    %eax,0x4(%esp)
  8000d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8000d9:	89 04 24             	mov    %eax,(%esp)
  8000dc:	e8 57 ff ff ff       	call   800038 <_main>

	// exit gracefully
	//exit();
	sleep();
  8000e1:	e8 16 00 00 00       	call   8000fc <sleep>
}
  8000e6:	c9                   	leave  
  8000e7:	c3                   	ret    

008000e8 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000e8:	55                   	push   %ebp
  8000e9:	89 e5                	mov    %esp,%ebp
  8000eb:	83 ec 18             	sub    $0x18,%esp
	sys_env_destroy(0);	
  8000ee:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  8000f5:	e8 29 0e 00 00       	call   800f23 <sys_env_destroy>
}
  8000fa:	c9                   	leave  
  8000fb:	c3                   	ret    

008000fc <sleep>:

void
sleep(void)
{	
  8000fc:	55                   	push   %ebp
  8000fd:	89 e5                	mov    %esp,%ebp
  8000ff:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  800102:	e8 93 0e 00 00       	call   800f9a <sys_env_sleep>
}
  800107:	c9                   	leave  
  800108:	c3                   	ret    

00800109 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800109:	55                   	push   %ebp
  80010a:	89 e5                	mov    %esp,%ebp
  80010c:	83 ec 18             	sub    $0x18,%esp
	b->buf[b->idx++] = ch;
  80010f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800112:	8b 00                	mov    (%eax),%eax
  800114:	8d 48 01             	lea    0x1(%eax),%ecx
  800117:	8b 55 0c             	mov    0xc(%ebp),%edx
  80011a:	89 0a                	mov    %ecx,(%edx)
  80011c:	8b 55 08             	mov    0x8(%ebp),%edx
  80011f:	89 d1                	mov    %edx,%ecx
  800121:	8b 55 0c             	mov    0xc(%ebp),%edx
  800124:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800128:	8b 45 0c             	mov    0xc(%ebp),%eax
  80012b:	8b 00                	mov    (%eax),%eax
  80012d:	3d ff 00 00 00       	cmp    $0xff,%eax
  800132:	75 20                	jne    800154 <putch+0x4b>
		sys_cputs(b->buf, b->idx);
  800134:	8b 45 0c             	mov    0xc(%ebp),%eax
  800137:	8b 00                	mov    (%eax),%eax
  800139:	8b 55 0c             	mov    0xc(%ebp),%edx
  80013c:	83 c2 08             	add    $0x8,%edx
  80013f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800143:	89 14 24             	mov    %edx,(%esp)
  800146:	e8 62 0d 00 00       	call   800ead <sys_cputs>
		b->idx = 0;
  80014b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80014e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  800154:	8b 45 0c             	mov    0xc(%ebp),%eax
  800157:	8b 40 04             	mov    0x4(%eax),%eax
  80015a:	8d 50 01             	lea    0x1(%eax),%edx
  80015d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800160:	89 50 04             	mov    %edx,0x4(%eax)
}
  800163:	c9                   	leave  
  800164:	c3                   	ret    

00800165 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800165:	55                   	push   %ebp
  800166:	89 e5                	mov    %esp,%ebp
  800168:	81 ec 28 01 00 00    	sub    $0x128,%esp
	struct printbuf b;

	b.idx = 0;
  80016e:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800175:	00 00 00 
	b.cnt = 0;
  800178:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80017f:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800182:	8b 45 0c             	mov    0xc(%ebp),%eax
  800185:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800189:	8b 45 08             	mov    0x8(%ebp),%eax
  80018c:	89 44 24 08          	mov    %eax,0x8(%esp)
  800190:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800196:	89 44 24 04          	mov    %eax,0x4(%esp)
  80019a:	c7 04 24 09 01 80 00 	movl   $0x800109,(%esp)
  8001a1:	e8 ed 01 00 00       	call   800393 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001a6:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8001ac:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001b0:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001b6:	83 c0 08             	add    $0x8,%eax
  8001b9:	89 04 24             	mov    %eax,(%esp)
  8001bc:	e8 ec 0c 00 00       	call   800ead <sys_cputs>

	return b.cnt;
  8001c1:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001c7:	c9                   	leave  
  8001c8:	c3                   	ret    

008001c9 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001c9:	55                   	push   %ebp
  8001ca:	89 e5                	mov    %esp,%ebp
  8001cc:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001cf:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001d2:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8001d8:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8001db:	89 54 24 04          	mov    %edx,0x4(%esp)
  8001df:	89 04 24             	mov    %eax,(%esp)
  8001e2:	e8 7e ff ff ff       	call   800165 <vcprintf>
  8001e7:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  8001ea:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8001ed:	c9                   	leave  
  8001ee:	c3                   	ret    

008001ef <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001ef:	55                   	push   %ebp
  8001f0:	89 e5                	mov    %esp,%ebp
  8001f2:	53                   	push   %ebx
  8001f3:	83 ec 34             	sub    $0x34,%esp
  8001f6:	8b 45 10             	mov    0x10(%ebp),%eax
  8001f9:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8001fc:	8b 45 14             	mov    0x14(%ebp),%eax
  8001ff:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800202:	8b 45 18             	mov    0x18(%ebp),%eax
  800205:	ba 00 00 00 00       	mov    $0x0,%edx
  80020a:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80020d:	77 72                	ja     800281 <printnum+0x92>
  80020f:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800212:	72 05                	jb     800219 <printnum+0x2a>
  800214:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800217:	77 68                	ja     800281 <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800219:	8b 45 1c             	mov    0x1c(%ebp),%eax
  80021c:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80021f:	8b 45 18             	mov    0x18(%ebp),%eax
  800222:	ba 00 00 00 00       	mov    $0x0,%edx
  800227:	89 44 24 08          	mov    %eax,0x8(%esp)
  80022b:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80022f:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800232:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800235:	89 04 24             	mov    %eax,(%esp)
  800238:	89 54 24 04          	mov    %edx,0x4(%esp)
  80023c:	e8 3f 0f 00 00       	call   801180 <__udivdi3>
  800241:	8b 4d 20             	mov    0x20(%ebp),%ecx
  800244:	89 4c 24 18          	mov    %ecx,0x18(%esp)
  800248:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  80024c:	8b 4d 18             	mov    0x18(%ebp),%ecx
  80024f:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  800253:	89 44 24 08          	mov    %eax,0x8(%esp)
  800257:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80025b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80025e:	89 44 24 04          	mov    %eax,0x4(%esp)
  800262:	8b 45 08             	mov    0x8(%ebp),%eax
  800265:	89 04 24             	mov    %eax,(%esp)
  800268:	e8 82 ff ff ff       	call   8001ef <printnum>
  80026d:	eb 1c                	jmp    80028b <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80026f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800272:	89 44 24 04          	mov    %eax,0x4(%esp)
  800276:	8b 45 20             	mov    0x20(%ebp),%eax
  800279:	89 04 24             	mov    %eax,(%esp)
  80027c:	8b 45 08             	mov    0x8(%ebp),%eax
  80027f:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800281:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  800285:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  800289:	7f e4                	jg     80026f <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80028b:	8b 4d 18             	mov    0x18(%ebp),%ecx
  80028e:	bb 00 00 00 00       	mov    $0x0,%ebx
  800293:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800296:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800299:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80029d:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  8002a1:	89 04 24             	mov    %eax,(%esp)
  8002a4:	89 54 24 04          	mov    %edx,0x4(%esp)
  8002a8:	e8 03 10 00 00       	call   8012b0 <__umoddi3>
  8002ad:	05 e0 14 80 00       	add    $0x8014e0,%eax
  8002b2:	0f b6 00             	movzbl (%eax),%eax
  8002b5:	0f be c0             	movsbl %al,%eax
  8002b8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002bb:	89 54 24 04          	mov    %edx,0x4(%esp)
  8002bf:	89 04 24             	mov    %eax,(%esp)
  8002c2:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c5:	ff d0                	call   *%eax
}
  8002c7:	83 c4 34             	add    $0x34,%esp
  8002ca:	5b                   	pop    %ebx
  8002cb:	5d                   	pop    %ebp
  8002cc:	c3                   	ret    

008002cd <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002cd:	55                   	push   %ebp
  8002ce:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d0:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002d4:	7e 1c                	jle    8002f2 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8002d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d9:	8b 00                	mov    (%eax),%eax
  8002db:	8d 50 08             	lea    0x8(%eax),%edx
  8002de:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e1:	89 10                	mov    %edx,(%eax)
  8002e3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e6:	8b 00                	mov    (%eax),%eax
  8002e8:	83 e8 08             	sub    $0x8,%eax
  8002eb:	8b 50 04             	mov    0x4(%eax),%edx
  8002ee:	8b 00                	mov    (%eax),%eax
  8002f0:	eb 40                	jmp    800332 <getuint+0x65>
	else if (lflag)
  8002f2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002f6:	74 1e                	je     800316 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  8002f8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002fb:	8b 00                	mov    (%eax),%eax
  8002fd:	8d 50 04             	lea    0x4(%eax),%edx
  800300:	8b 45 08             	mov    0x8(%ebp),%eax
  800303:	89 10                	mov    %edx,(%eax)
  800305:	8b 45 08             	mov    0x8(%ebp),%eax
  800308:	8b 00                	mov    (%eax),%eax
  80030a:	83 e8 04             	sub    $0x4,%eax
  80030d:	8b 00                	mov    (%eax),%eax
  80030f:	ba 00 00 00 00       	mov    $0x0,%edx
  800314:	eb 1c                	jmp    800332 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  800316:	8b 45 08             	mov    0x8(%ebp),%eax
  800319:	8b 00                	mov    (%eax),%eax
  80031b:	8d 50 04             	lea    0x4(%eax),%edx
  80031e:	8b 45 08             	mov    0x8(%ebp),%eax
  800321:	89 10                	mov    %edx,(%eax)
  800323:	8b 45 08             	mov    0x8(%ebp),%eax
  800326:	8b 00                	mov    (%eax),%eax
  800328:	83 e8 04             	sub    $0x4,%eax
  80032b:	8b 00                	mov    (%eax),%eax
  80032d:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800332:	5d                   	pop    %ebp
  800333:	c3                   	ret    

00800334 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  800334:	55                   	push   %ebp
  800335:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800337:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  80033b:	7e 1c                	jle    800359 <getint+0x25>
		return va_arg(*ap, long long);
  80033d:	8b 45 08             	mov    0x8(%ebp),%eax
  800340:	8b 00                	mov    (%eax),%eax
  800342:	8d 50 08             	lea    0x8(%eax),%edx
  800345:	8b 45 08             	mov    0x8(%ebp),%eax
  800348:	89 10                	mov    %edx,(%eax)
  80034a:	8b 45 08             	mov    0x8(%ebp),%eax
  80034d:	8b 00                	mov    (%eax),%eax
  80034f:	83 e8 08             	sub    $0x8,%eax
  800352:	8b 50 04             	mov    0x4(%eax),%edx
  800355:	8b 00                	mov    (%eax),%eax
  800357:	eb 38                	jmp    800391 <getint+0x5d>
	else if (lflag)
  800359:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80035d:	74 1a                	je     800379 <getint+0x45>
		return va_arg(*ap, long);
  80035f:	8b 45 08             	mov    0x8(%ebp),%eax
  800362:	8b 00                	mov    (%eax),%eax
  800364:	8d 50 04             	lea    0x4(%eax),%edx
  800367:	8b 45 08             	mov    0x8(%ebp),%eax
  80036a:	89 10                	mov    %edx,(%eax)
  80036c:	8b 45 08             	mov    0x8(%ebp),%eax
  80036f:	8b 00                	mov    (%eax),%eax
  800371:	83 e8 04             	sub    $0x4,%eax
  800374:	8b 00                	mov    (%eax),%eax
  800376:	99                   	cltd   
  800377:	eb 18                	jmp    800391 <getint+0x5d>
	else
		return va_arg(*ap, int);
  800379:	8b 45 08             	mov    0x8(%ebp),%eax
  80037c:	8b 00                	mov    (%eax),%eax
  80037e:	8d 50 04             	lea    0x4(%eax),%edx
  800381:	8b 45 08             	mov    0x8(%ebp),%eax
  800384:	89 10                	mov    %edx,(%eax)
  800386:	8b 45 08             	mov    0x8(%ebp),%eax
  800389:	8b 00                	mov    (%eax),%eax
  80038b:	83 e8 04             	sub    $0x4,%eax
  80038e:	8b 00                	mov    (%eax),%eax
  800390:	99                   	cltd   
}
  800391:	5d                   	pop    %ebp
  800392:	c3                   	ret    

00800393 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800393:	55                   	push   %ebp
  800394:	89 e5                	mov    %esp,%ebp
  800396:	56                   	push   %esi
  800397:	53                   	push   %ebx
  800398:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80039b:	eb 18                	jmp    8003b5 <vprintfmt+0x22>
			if (ch == '\0')
  80039d:	85 db                	test   %ebx,%ebx
  80039f:	75 05                	jne    8003a6 <vprintfmt+0x13>
				return;
  8003a1:	e9 07 04 00 00       	jmp    8007ad <vprintfmt+0x41a>
			putch(ch, putdat);
  8003a6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8003a9:	89 44 24 04          	mov    %eax,0x4(%esp)
  8003ad:	89 1c 24             	mov    %ebx,(%esp)
  8003b0:	8b 45 08             	mov    0x8(%ebp),%eax
  8003b3:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003b5:	8b 45 10             	mov    0x10(%ebp),%eax
  8003b8:	8d 50 01             	lea    0x1(%eax),%edx
  8003bb:	89 55 10             	mov    %edx,0x10(%ebp)
  8003be:	0f b6 00             	movzbl (%eax),%eax
  8003c1:	0f b6 d8             	movzbl %al,%ebx
  8003c4:	83 fb 25             	cmp    $0x25,%ebx
  8003c7:	75 d4                	jne    80039d <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8003c9:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8003cd:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8003d4:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  8003db:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  8003e2:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003e9:	8b 45 10             	mov    0x10(%ebp),%eax
  8003ec:	8d 50 01             	lea    0x1(%eax),%edx
  8003ef:	89 55 10             	mov    %edx,0x10(%ebp)
  8003f2:	0f b6 00             	movzbl (%eax),%eax
  8003f5:	0f b6 d8             	movzbl %al,%ebx
  8003f8:	8d 43 dd             	lea    -0x23(%ebx),%eax
  8003fb:	83 f8 55             	cmp    $0x55,%eax
  8003fe:	0f 87 78 03 00 00    	ja     80077c <vprintfmt+0x3e9>
  800404:	8b 04 85 04 15 80 00 	mov    0x801504(,%eax,4),%eax
  80040b:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  80040d:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  800411:	eb d6                	jmp    8003e9 <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800413:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  800417:	eb d0                	jmp    8003e9 <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800419:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  800420:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800423:	89 d0                	mov    %edx,%eax
  800425:	c1 e0 02             	shl    $0x2,%eax
  800428:	01 d0                	add    %edx,%eax
  80042a:	01 c0                	add    %eax,%eax
  80042c:	01 d8                	add    %ebx,%eax
  80042e:	83 e8 30             	sub    $0x30,%eax
  800431:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  800434:	8b 45 10             	mov    0x10(%ebp),%eax
  800437:	0f b6 00             	movzbl (%eax),%eax
  80043a:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  80043d:	83 fb 2f             	cmp    $0x2f,%ebx
  800440:	7e 0b                	jle    80044d <vprintfmt+0xba>
  800442:	83 fb 39             	cmp    $0x39,%ebx
  800445:	7f 06                	jg     80044d <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800447:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80044b:	eb d3                	jmp    800420 <vprintfmt+0x8d>
			goto process_precision;
  80044d:	eb 39                	jmp    800488 <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
  80044f:	8b 45 14             	mov    0x14(%ebp),%eax
  800452:	83 c0 04             	add    $0x4,%eax
  800455:	89 45 14             	mov    %eax,0x14(%ebp)
  800458:	8b 45 14             	mov    0x14(%ebp),%eax
  80045b:	83 e8 04             	sub    $0x4,%eax
  80045e:	8b 00                	mov    (%eax),%eax
  800460:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800463:	eb 23                	jmp    800488 <vprintfmt+0xf5>

		case '.':
			if (width < 0)
  800465:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800469:	79 0c                	jns    800477 <vprintfmt+0xe4>
				width = 0;
  80046b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  800472:	e9 72 ff ff ff       	jmp    8003e9 <vprintfmt+0x56>
  800477:	e9 6d ff ff ff       	jmp    8003e9 <vprintfmt+0x56>

		case '#':
			altflag = 1;
  80047c:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  800483:	e9 61 ff ff ff       	jmp    8003e9 <vprintfmt+0x56>

		process_precision:
			if (width < 0)
  800488:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80048c:	79 12                	jns    8004a0 <vprintfmt+0x10d>
				width = precision, precision = -1;
  80048e:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800491:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800494:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  80049b:	e9 49 ff ff ff       	jmp    8003e9 <vprintfmt+0x56>
  8004a0:	e9 44 ff ff ff       	jmp    8003e9 <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8004a5:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  8004a9:	e9 3b ff ff ff       	jmp    8003e9 <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004ae:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b1:	83 c0 04             	add    $0x4,%eax
  8004b4:	89 45 14             	mov    %eax,0x14(%ebp)
  8004b7:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ba:	83 e8 04             	sub    $0x4,%eax
  8004bd:	8b 00                	mov    (%eax),%eax
  8004bf:	8b 55 0c             	mov    0xc(%ebp),%edx
  8004c2:	89 54 24 04          	mov    %edx,0x4(%esp)
  8004c6:	89 04 24             	mov    %eax,(%esp)
  8004c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8004cc:	ff d0                	call   *%eax
			break;
  8004ce:	e9 d4 02 00 00       	jmp    8007a7 <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004d3:	8b 45 14             	mov    0x14(%ebp),%eax
  8004d6:	83 c0 04             	add    $0x4,%eax
  8004d9:	89 45 14             	mov    %eax,0x14(%ebp)
  8004dc:	8b 45 14             	mov    0x14(%ebp),%eax
  8004df:	83 e8 04             	sub    $0x4,%eax
  8004e2:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  8004e4:	85 db                	test   %ebx,%ebx
  8004e6:	79 02                	jns    8004ea <vprintfmt+0x157>
				err = -err;
  8004e8:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  8004ea:	83 fb 07             	cmp    $0x7,%ebx
  8004ed:	7f 0b                	jg     8004fa <vprintfmt+0x167>
  8004ef:	8b 34 9d c0 14 80 00 	mov    0x8014c0(,%ebx,4),%esi
  8004f6:	85 f6                	test   %esi,%esi
  8004f8:	75 23                	jne    80051d <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
  8004fa:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  8004fe:	c7 44 24 08 f1 14 80 	movl   $0x8014f1,0x8(%esp)
  800505:	00 
  800506:	8b 45 0c             	mov    0xc(%ebp),%eax
  800509:	89 44 24 04          	mov    %eax,0x4(%esp)
  80050d:	8b 45 08             	mov    0x8(%ebp),%eax
  800510:	89 04 24             	mov    %eax,(%esp)
  800513:	e8 9c 02 00 00       	call   8007b4 <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
  800518:	e9 8a 02 00 00       	jmp    8007a7 <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  80051d:	89 74 24 0c          	mov    %esi,0xc(%esp)
  800521:	c7 44 24 08 fa 14 80 	movl   $0x8014fa,0x8(%esp)
  800528:	00 
  800529:	8b 45 0c             	mov    0xc(%ebp),%eax
  80052c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800530:	8b 45 08             	mov    0x8(%ebp),%eax
  800533:	89 04 24             	mov    %eax,(%esp)
  800536:	e8 79 02 00 00       	call   8007b4 <printfmt>
			break;
  80053b:	e9 67 02 00 00       	jmp    8007a7 <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800540:	8b 45 14             	mov    0x14(%ebp),%eax
  800543:	83 c0 04             	add    $0x4,%eax
  800546:	89 45 14             	mov    %eax,0x14(%ebp)
  800549:	8b 45 14             	mov    0x14(%ebp),%eax
  80054c:	83 e8 04             	sub    $0x4,%eax
  80054f:	8b 30                	mov    (%eax),%esi
  800551:	85 f6                	test   %esi,%esi
  800553:	75 05                	jne    80055a <vprintfmt+0x1c7>
				p = "(null)";
  800555:	be fd 14 80 00       	mov    $0x8014fd,%esi
			if (width > 0 && padc != '-')
  80055a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80055e:	7e 37                	jle    800597 <vprintfmt+0x204>
  800560:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  800564:	74 31                	je     800597 <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
  800566:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800569:	89 44 24 04          	mov    %eax,0x4(%esp)
  80056d:	89 34 24             	mov    %esi,(%esp)
  800570:	e8 62 03 00 00       	call   8008d7 <strnlen>
  800575:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  800578:	eb 17                	jmp    800591 <vprintfmt+0x1fe>
					putch(padc, putdat);
  80057a:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  80057e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800581:	89 54 24 04          	mov    %edx,0x4(%esp)
  800585:	89 04 24             	mov    %eax,(%esp)
  800588:	8b 45 08             	mov    0x8(%ebp),%eax
  80058b:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80058d:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800591:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800595:	7f e3                	jg     80057a <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800597:	eb 38                	jmp    8005d1 <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
  800599:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80059d:	74 1f                	je     8005be <vprintfmt+0x22b>
  80059f:	83 fb 1f             	cmp    $0x1f,%ebx
  8005a2:	7e 05                	jle    8005a9 <vprintfmt+0x216>
  8005a4:	83 fb 7e             	cmp    $0x7e,%ebx
  8005a7:	7e 15                	jle    8005be <vprintfmt+0x22b>
					putch('?', putdat);
  8005a9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005ac:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005b0:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  8005b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8005ba:	ff d0                	call   *%eax
  8005bc:	eb 0f                	jmp    8005cd <vprintfmt+0x23a>
				else
					putch(ch, putdat);
  8005be:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005c1:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005c5:	89 1c 24             	mov    %ebx,(%esp)
  8005c8:	8b 45 08             	mov    0x8(%ebp),%eax
  8005cb:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005cd:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005d1:	89 f0                	mov    %esi,%eax
  8005d3:	8d 70 01             	lea    0x1(%eax),%esi
  8005d6:	0f b6 00             	movzbl (%eax),%eax
  8005d9:	0f be d8             	movsbl %al,%ebx
  8005dc:	85 db                	test   %ebx,%ebx
  8005de:	74 10                	je     8005f0 <vprintfmt+0x25d>
  8005e0:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005e4:	78 b3                	js     800599 <vprintfmt+0x206>
  8005e6:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  8005ea:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005ee:	79 a9                	jns    800599 <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005f0:	eb 17                	jmp    800609 <vprintfmt+0x276>
				putch(' ', putdat);
  8005f2:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005f5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005f9:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  800600:	8b 45 08             	mov    0x8(%ebp),%eax
  800603:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800605:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800609:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80060d:	7f e3                	jg     8005f2 <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
  80060f:	e9 93 01 00 00       	jmp    8007a7 <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800614:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800617:	89 44 24 04          	mov    %eax,0x4(%esp)
  80061b:	8d 45 14             	lea    0x14(%ebp),%eax
  80061e:	89 04 24             	mov    %eax,(%esp)
  800621:	e8 0e fd ff ff       	call   800334 <getint>
  800626:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800629:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  80062c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80062f:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800632:	85 d2                	test   %edx,%edx
  800634:	79 26                	jns    80065c <vprintfmt+0x2c9>
				putch('-', putdat);
  800636:	8b 45 0c             	mov    0xc(%ebp),%eax
  800639:	89 44 24 04          	mov    %eax,0x4(%esp)
  80063d:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  800644:	8b 45 08             	mov    0x8(%ebp),%eax
  800647:	ff d0                	call   *%eax
				num = -(long long) num;
  800649:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80064c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80064f:	f7 d8                	neg    %eax
  800651:	83 d2 00             	adc    $0x0,%edx
  800654:	f7 da                	neg    %edx
  800656:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800659:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  80065c:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800663:	e9 cb 00 00 00       	jmp    800733 <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800668:	8b 45 e8             	mov    -0x18(%ebp),%eax
  80066b:	89 44 24 04          	mov    %eax,0x4(%esp)
  80066f:	8d 45 14             	lea    0x14(%ebp),%eax
  800672:	89 04 24             	mov    %eax,(%esp)
  800675:	e8 53 fc ff ff       	call   8002cd <getuint>
  80067a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80067d:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  800680:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800687:	e9 a7 00 00 00       	jmp    800733 <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  80068c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80068f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800693:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  80069a:	8b 45 08             	mov    0x8(%ebp),%eax
  80069d:	ff d0                	call   *%eax
			putch('X', putdat);
  80069f:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006a2:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006a6:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006ad:	8b 45 08             	mov    0x8(%ebp),%eax
  8006b0:	ff d0                	call   *%eax
			putch('X', putdat);
  8006b2:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006b5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006b9:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8006c3:	ff d0                	call   *%eax
			break;
  8006c5:	e9 dd 00 00 00       	jmp    8007a7 <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
  8006ca:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006cd:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006d1:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  8006d8:	8b 45 08             	mov    0x8(%ebp),%eax
  8006db:	ff d0                	call   *%eax
			putch('x', putdat);
  8006dd:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006e0:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006e4:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  8006eb:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ee:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  8006f0:	8b 45 14             	mov    0x14(%ebp),%eax
  8006f3:	83 c0 04             	add    $0x4,%eax
  8006f6:	89 45 14             	mov    %eax,0x14(%ebp)
  8006f9:	8b 45 14             	mov    0x14(%ebp),%eax
  8006fc:	83 e8 04             	sub    $0x4,%eax
  8006ff:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800701:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800704:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  80070b:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  800712:	eb 1f                	jmp    800733 <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800714:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800717:	89 44 24 04          	mov    %eax,0x4(%esp)
  80071b:	8d 45 14             	lea    0x14(%ebp),%eax
  80071e:	89 04 24             	mov    %eax,(%esp)
  800721:	e8 a7 fb ff ff       	call   8002cd <getuint>
  800726:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800729:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  80072c:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  800733:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  800737:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80073a:	89 54 24 18          	mov    %edx,0x18(%esp)
  80073e:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  800741:	89 54 24 14          	mov    %edx,0x14(%esp)
  800745:	89 44 24 10          	mov    %eax,0x10(%esp)
  800749:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80074c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80074f:	89 44 24 08          	mov    %eax,0x8(%esp)
  800753:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800757:	8b 45 0c             	mov    0xc(%ebp),%eax
  80075a:	89 44 24 04          	mov    %eax,0x4(%esp)
  80075e:	8b 45 08             	mov    0x8(%ebp),%eax
  800761:	89 04 24             	mov    %eax,(%esp)
  800764:	e8 86 fa ff ff       	call   8001ef <printnum>
			break;
  800769:	eb 3c                	jmp    8007a7 <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80076b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80076e:	89 44 24 04          	mov    %eax,0x4(%esp)
  800772:	89 1c 24             	mov    %ebx,(%esp)
  800775:	8b 45 08             	mov    0x8(%ebp),%eax
  800778:	ff d0                	call   *%eax
			break;
  80077a:	eb 2b                	jmp    8007a7 <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80077c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80077f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800783:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  80078a:	8b 45 08             	mov    0x8(%ebp),%eax
  80078d:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
  80078f:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800793:	eb 04                	jmp    800799 <vprintfmt+0x406>
  800795:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800799:	8b 45 10             	mov    0x10(%ebp),%eax
  80079c:	83 e8 01             	sub    $0x1,%eax
  80079f:	0f b6 00             	movzbl (%eax),%eax
  8007a2:	3c 25                	cmp    $0x25,%al
  8007a4:	75 ef                	jne    800795 <vprintfmt+0x402>
				/* do nothing */;
			break;
  8007a6:	90                   	nop
		}
	}
  8007a7:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8007a8:	e9 08 fc ff ff       	jmp    8003b5 <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  8007ad:	83 c4 40             	add    $0x40,%esp
  8007b0:	5b                   	pop    %ebx
  8007b1:	5e                   	pop    %esi
  8007b2:	5d                   	pop    %ebp
  8007b3:	c3                   	ret    

008007b4 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8007b4:	55                   	push   %ebp
  8007b5:	89 e5                	mov    %esp,%ebp
  8007b7:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  8007ba:	8d 45 10             	lea    0x10(%ebp),%eax
  8007bd:	83 c0 04             	add    $0x4,%eax
  8007c0:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  8007c3:	8b 45 10             	mov    0x10(%ebp),%eax
  8007c6:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8007c9:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8007cd:	89 44 24 08          	mov    %eax,0x8(%esp)
  8007d1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007d4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007d8:	8b 45 08             	mov    0x8(%ebp),%eax
  8007db:	89 04 24             	mov    %eax,(%esp)
  8007de:	e8 b0 fb ff ff       	call   800393 <vprintfmt>
	va_end(ap);
}
  8007e3:	c9                   	leave  
  8007e4:	c3                   	ret    

008007e5 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8007e5:	55                   	push   %ebp
  8007e6:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  8007e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007eb:	8b 40 08             	mov    0x8(%eax),%eax
  8007ee:	8d 50 01             	lea    0x1(%eax),%edx
  8007f1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007f4:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  8007f7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007fa:	8b 10                	mov    (%eax),%edx
  8007fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ff:	8b 40 04             	mov    0x4(%eax),%eax
  800802:	39 c2                	cmp    %eax,%edx
  800804:	73 12                	jae    800818 <sprintputch+0x33>
		*b->buf++ = ch;
  800806:	8b 45 0c             	mov    0xc(%ebp),%eax
  800809:	8b 00                	mov    (%eax),%eax
  80080b:	8d 48 01             	lea    0x1(%eax),%ecx
  80080e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800811:	89 0a                	mov    %ecx,(%edx)
  800813:	8b 55 08             	mov    0x8(%ebp),%edx
  800816:	88 10                	mov    %dl,(%eax)
}
  800818:	5d                   	pop    %ebp
  800819:	c3                   	ret    

0080081a <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  80081a:	55                   	push   %ebp
  80081b:	89 e5                	mov    %esp,%ebp
  80081d:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  800820:	8b 45 08             	mov    0x8(%ebp),%eax
  800823:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800826:	8b 45 0c             	mov    0xc(%ebp),%eax
  800829:	8d 50 ff             	lea    -0x1(%eax),%edx
  80082c:	8b 45 08             	mov    0x8(%ebp),%eax
  80082f:	01 d0                	add    %edx,%eax
  800831:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800834:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80083b:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  80083f:	74 06                	je     800847 <vsnprintf+0x2d>
  800841:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800845:	7f 07                	jg     80084e <vsnprintf+0x34>
		return -E_INVAL;
  800847:	b8 03 00 00 00       	mov    $0x3,%eax
  80084c:	eb 2a                	jmp    800878 <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  80084e:	8b 45 14             	mov    0x14(%ebp),%eax
  800851:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800855:	8b 45 10             	mov    0x10(%ebp),%eax
  800858:	89 44 24 08          	mov    %eax,0x8(%esp)
  80085c:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80085f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800863:	c7 04 24 e5 07 80 00 	movl   $0x8007e5,(%esp)
  80086a:	e8 24 fb ff ff       	call   800393 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80086f:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800872:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800875:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  800878:	c9                   	leave  
  800879:	c3                   	ret    

0080087a <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  80087a:	55                   	push   %ebp
  80087b:	89 e5                	mov    %esp,%ebp
  80087d:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800880:	8d 45 10             	lea    0x10(%ebp),%eax
  800883:	83 c0 04             	add    $0x4,%eax
  800886:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  800889:	8b 45 10             	mov    0x10(%ebp),%eax
  80088c:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80088f:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800893:	89 44 24 08          	mov    %eax,0x8(%esp)
  800897:	8b 45 0c             	mov    0xc(%ebp),%eax
  80089a:	89 44 24 04          	mov    %eax,0x4(%esp)
  80089e:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a1:	89 04 24             	mov    %eax,(%esp)
  8008a4:	e8 71 ff ff ff       	call   80081a <vsnprintf>
  8008a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  8008ac:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8008af:	c9                   	leave  
  8008b0:	c3                   	ret    

008008b1 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  8008b1:	55                   	push   %ebp
  8008b2:	89 e5                	mov    %esp,%ebp
  8008b4:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  8008b7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008be:	eb 08                	jmp    8008c8 <strlen+0x17>
		n++;
  8008c0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8008c4:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8008c8:	8b 45 08             	mov    0x8(%ebp),%eax
  8008cb:	0f b6 00             	movzbl (%eax),%eax
  8008ce:	84 c0                	test   %al,%al
  8008d0:	75 ee                	jne    8008c0 <strlen+0xf>
		n++;
	return n;
  8008d2:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008d5:	c9                   	leave  
  8008d6:	c3                   	ret    

008008d7 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  8008d7:	55                   	push   %ebp
  8008d8:	89 e5                	mov    %esp,%ebp
  8008da:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008dd:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008e4:	eb 0c                	jmp    8008f2 <strnlen+0x1b>
		n++;
  8008e6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8008ea:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8008ee:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  8008f2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8008f6:	74 0a                	je     800902 <strnlen+0x2b>
  8008f8:	8b 45 08             	mov    0x8(%ebp),%eax
  8008fb:	0f b6 00             	movzbl (%eax),%eax
  8008fe:	84 c0                	test   %al,%al
  800900:	75 e4                	jne    8008e6 <strnlen+0xf>
		n++;
	return n;
  800902:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800905:	c9                   	leave  
  800906:	c3                   	ret    

00800907 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800907:	55                   	push   %ebp
  800908:	89 e5                	mov    %esp,%ebp
  80090a:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  80090d:	8b 45 08             	mov    0x8(%ebp),%eax
  800910:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800913:	90                   	nop
  800914:	8b 45 08             	mov    0x8(%ebp),%eax
  800917:	8d 50 01             	lea    0x1(%eax),%edx
  80091a:	89 55 08             	mov    %edx,0x8(%ebp)
  80091d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800920:	8d 4a 01             	lea    0x1(%edx),%ecx
  800923:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800926:	0f b6 12             	movzbl (%edx),%edx
  800929:	88 10                	mov    %dl,(%eax)
  80092b:	0f b6 00             	movzbl (%eax),%eax
  80092e:	84 c0                	test   %al,%al
  800930:	75 e2                	jne    800914 <strcpy+0xd>
		/* do nothing */;
	return ret;
  800932:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800935:	c9                   	leave  
  800936:	c3                   	ret    

00800937 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  800937:	55                   	push   %ebp
  800938:	89 e5                	mov    %esp,%ebp
  80093a:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  80093d:	8b 45 08             	mov    0x8(%ebp),%eax
  800940:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800943:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80094a:	eb 23                	jmp    80096f <strncpy+0x38>
		*dst++ = *src;
  80094c:	8b 45 08             	mov    0x8(%ebp),%eax
  80094f:	8d 50 01             	lea    0x1(%eax),%edx
  800952:	89 55 08             	mov    %edx,0x8(%ebp)
  800955:	8b 55 0c             	mov    0xc(%ebp),%edx
  800958:	0f b6 12             	movzbl (%edx),%edx
  80095b:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  80095d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800960:	0f b6 00             	movzbl (%eax),%eax
  800963:	84 c0                	test   %al,%al
  800965:	74 04                	je     80096b <strncpy+0x34>
			src++;
  800967:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80096b:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  80096f:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800972:	3b 45 10             	cmp    0x10(%ebp),%eax
  800975:	72 d5                	jb     80094c <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800977:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  80097a:	c9                   	leave  
  80097b:	c3                   	ret    

0080097c <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  80097c:	55                   	push   %ebp
  80097d:	89 e5                	mov    %esp,%ebp
  80097f:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800982:	8b 45 08             	mov    0x8(%ebp),%eax
  800985:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800988:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80098c:	74 33                	je     8009c1 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  80098e:	eb 17                	jmp    8009a7 <strlcpy+0x2b>
			*dst++ = *src++;
  800990:	8b 45 08             	mov    0x8(%ebp),%eax
  800993:	8d 50 01             	lea    0x1(%eax),%edx
  800996:	89 55 08             	mov    %edx,0x8(%ebp)
  800999:	8b 55 0c             	mov    0xc(%ebp),%edx
  80099c:	8d 4a 01             	lea    0x1(%edx),%ecx
  80099f:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8009a2:	0f b6 12             	movzbl (%edx),%edx
  8009a5:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8009a7:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8009ab:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009af:	74 0a                	je     8009bb <strlcpy+0x3f>
  8009b1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009b4:	0f b6 00             	movzbl (%eax),%eax
  8009b7:	84 c0                	test   %al,%al
  8009b9:	75 d5                	jne    800990 <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  8009bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8009be:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  8009c1:	8b 55 08             	mov    0x8(%ebp),%edx
  8009c4:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8009c7:	29 c2                	sub    %eax,%edx
  8009c9:	89 d0                	mov    %edx,%eax
}
  8009cb:	c9                   	leave  
  8009cc:	c3                   	ret    

008009cd <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8009cd:	55                   	push   %ebp
  8009ce:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  8009d0:	eb 08                	jmp    8009da <strcmp+0xd>
		p++, q++;
  8009d2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009d6:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8009da:	8b 45 08             	mov    0x8(%ebp),%eax
  8009dd:	0f b6 00             	movzbl (%eax),%eax
  8009e0:	84 c0                	test   %al,%al
  8009e2:	74 10                	je     8009f4 <strcmp+0x27>
  8009e4:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e7:	0f b6 10             	movzbl (%eax),%edx
  8009ea:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009ed:	0f b6 00             	movzbl (%eax),%eax
  8009f0:	38 c2                	cmp    %al,%dl
  8009f2:	74 de                	je     8009d2 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  8009f4:	8b 45 08             	mov    0x8(%ebp),%eax
  8009f7:	0f b6 00             	movzbl (%eax),%eax
  8009fa:	0f b6 d0             	movzbl %al,%edx
  8009fd:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a00:	0f b6 00             	movzbl (%eax),%eax
  800a03:	0f b6 c0             	movzbl %al,%eax
  800a06:	29 c2                	sub    %eax,%edx
  800a08:	89 d0                	mov    %edx,%eax
}
  800a0a:	5d                   	pop    %ebp
  800a0b:	c3                   	ret    

00800a0c <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800a0c:	55                   	push   %ebp
  800a0d:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800a0f:	eb 0c                	jmp    800a1d <strncmp+0x11>
		n--, p++, q++;
  800a11:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800a15:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a19:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800a1d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a21:	74 1a                	je     800a3d <strncmp+0x31>
  800a23:	8b 45 08             	mov    0x8(%ebp),%eax
  800a26:	0f b6 00             	movzbl (%eax),%eax
  800a29:	84 c0                	test   %al,%al
  800a2b:	74 10                	je     800a3d <strncmp+0x31>
  800a2d:	8b 45 08             	mov    0x8(%ebp),%eax
  800a30:	0f b6 10             	movzbl (%eax),%edx
  800a33:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a36:	0f b6 00             	movzbl (%eax),%eax
  800a39:	38 c2                	cmp    %al,%dl
  800a3b:	74 d4                	je     800a11 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800a3d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a41:	75 07                	jne    800a4a <strncmp+0x3e>
		return 0;
  800a43:	b8 00 00 00 00       	mov    $0x0,%eax
  800a48:	eb 16                	jmp    800a60 <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800a4a:	8b 45 08             	mov    0x8(%ebp),%eax
  800a4d:	0f b6 00             	movzbl (%eax),%eax
  800a50:	0f b6 d0             	movzbl %al,%edx
  800a53:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a56:	0f b6 00             	movzbl (%eax),%eax
  800a59:	0f b6 c0             	movzbl %al,%eax
  800a5c:	29 c2                	sub    %eax,%edx
  800a5e:	89 d0                	mov    %edx,%eax
}
  800a60:	5d                   	pop    %ebp
  800a61:	c3                   	ret    

00800a62 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800a62:	55                   	push   %ebp
  800a63:	89 e5                	mov    %esp,%ebp
  800a65:	83 ec 04             	sub    $0x4,%esp
  800a68:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a6b:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a6e:	eb 14                	jmp    800a84 <strchr+0x22>
		if (*s == c)
  800a70:	8b 45 08             	mov    0x8(%ebp),%eax
  800a73:	0f b6 00             	movzbl (%eax),%eax
  800a76:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a79:	75 05                	jne    800a80 <strchr+0x1e>
			return (char *) s;
  800a7b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7e:	eb 13                	jmp    800a93 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a80:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a84:	8b 45 08             	mov    0x8(%ebp),%eax
  800a87:	0f b6 00             	movzbl (%eax),%eax
  800a8a:	84 c0                	test   %al,%al
  800a8c:	75 e2                	jne    800a70 <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800a8e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a93:	c9                   	leave  
  800a94:	c3                   	ret    

00800a95 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a95:	55                   	push   %ebp
  800a96:	89 e5                	mov    %esp,%ebp
  800a98:	83 ec 04             	sub    $0x4,%esp
  800a9b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a9e:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800aa1:	eb 11                	jmp    800ab4 <strfind+0x1f>
		if (*s == c)
  800aa3:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa6:	0f b6 00             	movzbl (%eax),%eax
  800aa9:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800aac:	75 02                	jne    800ab0 <strfind+0x1b>
			break;
  800aae:	eb 0e                	jmp    800abe <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800ab0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ab4:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab7:	0f b6 00             	movzbl (%eax),%eax
  800aba:	84 c0                	test   %al,%al
  800abc:	75 e5                	jne    800aa3 <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
  800abe:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ac1:	c9                   	leave  
  800ac2:	c3                   	ret    

00800ac3 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800ac3:	55                   	push   %ebp
  800ac4:	89 e5                	mov    %esp,%ebp
  800ac6:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800ac9:	8b 45 08             	mov    0x8(%ebp),%eax
  800acc:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800acf:	8b 45 10             	mov    0x10(%ebp),%eax
  800ad2:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800ad5:	eb 0e                	jmp    800ae5 <memset+0x22>
		*p++ = c;
  800ad7:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ada:	8d 50 01             	lea    0x1(%eax),%edx
  800add:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800ae0:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ae3:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800ae5:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800ae9:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800aed:	79 e8                	jns    800ad7 <memset+0x14>
		*p++ = c;

	return v;
  800aef:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800af2:	c9                   	leave  
  800af3:	c3                   	ret    

00800af4 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800af4:	55                   	push   %ebp
  800af5:	89 e5                	mov    %esp,%ebp
  800af7:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800afa:	8b 45 0c             	mov    0xc(%ebp),%eax
  800afd:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800b00:	8b 45 08             	mov    0x8(%ebp),%eax
  800b03:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800b06:	eb 17                	jmp    800b1f <memcpy+0x2b>
		*d++ = *s++;
  800b08:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b0b:	8d 50 01             	lea    0x1(%eax),%edx
  800b0e:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b11:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b14:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b17:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b1a:	0f b6 12             	movzbl (%edx),%edx
  800b1d:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800b1f:	8b 45 10             	mov    0x10(%ebp),%eax
  800b22:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b25:	89 55 10             	mov    %edx,0x10(%ebp)
  800b28:	85 c0                	test   %eax,%eax
  800b2a:	75 dc                	jne    800b08 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800b2c:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b2f:	c9                   	leave  
  800b30:	c3                   	ret    

00800b31 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800b31:	55                   	push   %ebp
  800b32:	89 e5                	mov    %esp,%ebp
  800b34:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800b37:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b3a:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800b3d:	8b 45 08             	mov    0x8(%ebp),%eax
  800b40:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800b43:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b46:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800b49:	73 3d                	jae    800b88 <memmove+0x57>
  800b4b:	8b 45 10             	mov    0x10(%ebp),%eax
  800b4e:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b51:	01 d0                	add    %edx,%eax
  800b53:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800b56:	76 30                	jbe    800b88 <memmove+0x57>
		s += n;
  800b58:	8b 45 10             	mov    0x10(%ebp),%eax
  800b5b:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800b5e:	8b 45 10             	mov    0x10(%ebp),%eax
  800b61:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800b64:	eb 13                	jmp    800b79 <memmove+0x48>
			*--d = *--s;
  800b66:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800b6a:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800b6e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b71:	0f b6 10             	movzbl (%eax),%edx
  800b74:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b77:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800b79:	8b 45 10             	mov    0x10(%ebp),%eax
  800b7c:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b7f:	89 55 10             	mov    %edx,0x10(%ebp)
  800b82:	85 c0                	test   %eax,%eax
  800b84:	75 e0                	jne    800b66 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b86:	eb 26                	jmp    800bae <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b88:	eb 17                	jmp    800ba1 <memmove+0x70>
			*d++ = *s++;
  800b8a:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b8d:	8d 50 01             	lea    0x1(%eax),%edx
  800b90:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b93:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b96:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b99:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b9c:	0f b6 12             	movzbl (%edx),%edx
  800b9f:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800ba1:	8b 45 10             	mov    0x10(%ebp),%eax
  800ba4:	8d 50 ff             	lea    -0x1(%eax),%edx
  800ba7:	89 55 10             	mov    %edx,0x10(%ebp)
  800baa:	85 c0                	test   %eax,%eax
  800bac:	75 dc                	jne    800b8a <memmove+0x59>
			*d++ = *s++;

	return dst;
  800bae:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bb1:	c9                   	leave  
  800bb2:	c3                   	ret    

00800bb3 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800bb3:	55                   	push   %ebp
  800bb4:	89 e5                	mov    %esp,%ebp
  800bb6:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800bb9:	8b 45 08             	mov    0x8(%ebp),%eax
  800bbc:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800bbf:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bc2:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800bc5:	eb 30                	jmp    800bf7 <memcmp+0x44>
		if (*s1 != *s2)
  800bc7:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bca:	0f b6 10             	movzbl (%eax),%edx
  800bcd:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800bd0:	0f b6 00             	movzbl (%eax),%eax
  800bd3:	38 c2                	cmp    %al,%dl
  800bd5:	74 18                	je     800bef <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800bd7:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bda:	0f b6 00             	movzbl (%eax),%eax
  800bdd:	0f b6 d0             	movzbl %al,%edx
  800be0:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800be3:	0f b6 00             	movzbl (%eax),%eax
  800be6:	0f b6 c0             	movzbl %al,%eax
  800be9:	29 c2                	sub    %eax,%edx
  800beb:	89 d0                	mov    %edx,%eax
  800bed:	eb 1a                	jmp    800c09 <memcmp+0x56>
		s1++, s2++;
  800bef:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800bf3:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800bf7:	8b 45 10             	mov    0x10(%ebp),%eax
  800bfa:	8d 50 ff             	lea    -0x1(%eax),%edx
  800bfd:	89 55 10             	mov    %edx,0x10(%ebp)
  800c00:	85 c0                	test   %eax,%eax
  800c02:	75 c3                	jne    800bc7 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800c04:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c09:	c9                   	leave  
  800c0a:	c3                   	ret    

00800c0b <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800c0b:	55                   	push   %ebp
  800c0c:	89 e5                	mov    %esp,%ebp
  800c0e:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800c11:	8b 45 10             	mov    0x10(%ebp),%eax
  800c14:	8b 55 08             	mov    0x8(%ebp),%edx
  800c17:	01 d0                	add    %edx,%eax
  800c19:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800c1c:	eb 13                	jmp    800c31 <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
  800c1e:	8b 45 08             	mov    0x8(%ebp),%eax
  800c21:	0f b6 10             	movzbl (%eax),%edx
  800c24:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c27:	38 c2                	cmp    %al,%dl
  800c29:	75 02                	jne    800c2d <memfind+0x22>
			break;
  800c2b:	eb 0c                	jmp    800c39 <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800c2d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c31:	8b 45 08             	mov    0x8(%ebp),%eax
  800c34:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800c37:	72 e5                	jb     800c1e <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
  800c39:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c3c:	c9                   	leave  
  800c3d:	c3                   	ret    

00800c3e <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800c3e:	55                   	push   %ebp
  800c3f:	89 e5                	mov    %esp,%ebp
  800c41:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800c44:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800c4b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c52:	eb 04                	jmp    800c58 <strtol+0x1a>
		s++;
  800c54:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800c58:	8b 45 08             	mov    0x8(%ebp),%eax
  800c5b:	0f b6 00             	movzbl (%eax),%eax
  800c5e:	3c 20                	cmp    $0x20,%al
  800c60:	74 f2                	je     800c54 <strtol+0x16>
  800c62:	8b 45 08             	mov    0x8(%ebp),%eax
  800c65:	0f b6 00             	movzbl (%eax),%eax
  800c68:	3c 09                	cmp    $0x9,%al
  800c6a:	74 e8                	je     800c54 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c6c:	8b 45 08             	mov    0x8(%ebp),%eax
  800c6f:	0f b6 00             	movzbl (%eax),%eax
  800c72:	3c 2b                	cmp    $0x2b,%al
  800c74:	75 06                	jne    800c7c <strtol+0x3e>
		s++;
  800c76:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c7a:	eb 15                	jmp    800c91 <strtol+0x53>
	else if (*s == '-')
  800c7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7f:	0f b6 00             	movzbl (%eax),%eax
  800c82:	3c 2d                	cmp    $0x2d,%al
  800c84:	75 0b                	jne    800c91 <strtol+0x53>
		s++, neg = 1;
  800c86:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c8a:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c91:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c95:	74 06                	je     800c9d <strtol+0x5f>
  800c97:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c9b:	75 24                	jne    800cc1 <strtol+0x83>
  800c9d:	8b 45 08             	mov    0x8(%ebp),%eax
  800ca0:	0f b6 00             	movzbl (%eax),%eax
  800ca3:	3c 30                	cmp    $0x30,%al
  800ca5:	75 1a                	jne    800cc1 <strtol+0x83>
  800ca7:	8b 45 08             	mov    0x8(%ebp),%eax
  800caa:	83 c0 01             	add    $0x1,%eax
  800cad:	0f b6 00             	movzbl (%eax),%eax
  800cb0:	3c 78                	cmp    $0x78,%al
  800cb2:	75 0d                	jne    800cc1 <strtol+0x83>
		s += 2, base = 16;
  800cb4:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800cb8:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800cbf:	eb 2a                	jmp    800ceb <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800cc1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800cc5:	75 17                	jne    800cde <strtol+0xa0>
  800cc7:	8b 45 08             	mov    0x8(%ebp),%eax
  800cca:	0f b6 00             	movzbl (%eax),%eax
  800ccd:	3c 30                	cmp    $0x30,%al
  800ccf:	75 0d                	jne    800cde <strtol+0xa0>
		s++, base = 8;
  800cd1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800cd5:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800cdc:	eb 0d                	jmp    800ceb <strtol+0xad>
	else if (base == 0)
  800cde:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ce2:	75 07                	jne    800ceb <strtol+0xad>
		base = 10;
  800ce4:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800ceb:	8b 45 08             	mov    0x8(%ebp),%eax
  800cee:	0f b6 00             	movzbl (%eax),%eax
  800cf1:	3c 2f                	cmp    $0x2f,%al
  800cf3:	7e 1b                	jle    800d10 <strtol+0xd2>
  800cf5:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf8:	0f b6 00             	movzbl (%eax),%eax
  800cfb:	3c 39                	cmp    $0x39,%al
  800cfd:	7f 11                	jg     800d10 <strtol+0xd2>
			dig = *s - '0';
  800cff:	8b 45 08             	mov    0x8(%ebp),%eax
  800d02:	0f b6 00             	movzbl (%eax),%eax
  800d05:	0f be c0             	movsbl %al,%eax
  800d08:	83 e8 30             	sub    $0x30,%eax
  800d0b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800d0e:	eb 48                	jmp    800d58 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800d10:	8b 45 08             	mov    0x8(%ebp),%eax
  800d13:	0f b6 00             	movzbl (%eax),%eax
  800d16:	3c 60                	cmp    $0x60,%al
  800d18:	7e 1b                	jle    800d35 <strtol+0xf7>
  800d1a:	8b 45 08             	mov    0x8(%ebp),%eax
  800d1d:	0f b6 00             	movzbl (%eax),%eax
  800d20:	3c 7a                	cmp    $0x7a,%al
  800d22:	7f 11                	jg     800d35 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800d24:	8b 45 08             	mov    0x8(%ebp),%eax
  800d27:	0f b6 00             	movzbl (%eax),%eax
  800d2a:	0f be c0             	movsbl %al,%eax
  800d2d:	83 e8 57             	sub    $0x57,%eax
  800d30:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800d33:	eb 23                	jmp    800d58 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800d35:	8b 45 08             	mov    0x8(%ebp),%eax
  800d38:	0f b6 00             	movzbl (%eax),%eax
  800d3b:	3c 40                	cmp    $0x40,%al
  800d3d:	7e 3d                	jle    800d7c <strtol+0x13e>
  800d3f:	8b 45 08             	mov    0x8(%ebp),%eax
  800d42:	0f b6 00             	movzbl (%eax),%eax
  800d45:	3c 5a                	cmp    $0x5a,%al
  800d47:	7f 33                	jg     800d7c <strtol+0x13e>
			dig = *s - 'A' + 10;
  800d49:	8b 45 08             	mov    0x8(%ebp),%eax
  800d4c:	0f b6 00             	movzbl (%eax),%eax
  800d4f:	0f be c0             	movsbl %al,%eax
  800d52:	83 e8 37             	sub    $0x37,%eax
  800d55:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800d58:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800d5b:	3b 45 10             	cmp    0x10(%ebp),%eax
  800d5e:	7c 02                	jl     800d62 <strtol+0x124>
			break;
  800d60:	eb 1a                	jmp    800d7c <strtol+0x13e>
		s++, val = (val * base) + dig;
  800d62:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d66:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d69:	0f af 45 10          	imul   0x10(%ebp),%eax
  800d6d:	89 c2                	mov    %eax,%edx
  800d6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800d72:	01 d0                	add    %edx,%eax
  800d74:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800d77:	e9 6f ff ff ff       	jmp    800ceb <strtol+0xad>

	if (endptr)
  800d7c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800d80:	74 08                	je     800d8a <strtol+0x14c>
		*endptr = (char *) s;
  800d82:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d85:	8b 55 08             	mov    0x8(%ebp),%edx
  800d88:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800d8a:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800d8e:	74 07                	je     800d97 <strtol+0x159>
  800d90:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d93:	f7 d8                	neg    %eax
  800d95:	eb 03                	jmp    800d9a <strtol+0x15c>
  800d97:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800d9a:	c9                   	leave  
  800d9b:	c3                   	ret    

00800d9c <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800d9c:	55                   	push   %ebp
  800d9d:	89 e5                	mov    %esp,%ebp
  800d9f:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800da2:	8b 45 14             	mov    0x14(%ebp),%eax
  800da5:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800dab:	8b 45 14             	mov    0x14(%ebp),%eax
  800dae:	8b 00                	mov    (%eax),%eax
  800db0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800db7:	8b 45 10             	mov    0x10(%ebp),%eax
  800dba:	01 d0                	add    %edx,%eax
  800dbc:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800dc2:	eb 0c                	jmp    800dd0 <strsplit+0x34>
			*string++ = 0;
  800dc4:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc7:	8d 50 01             	lea    0x1(%eax),%edx
  800dca:	89 55 08             	mov    %edx,0x8(%ebp)
  800dcd:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800dd0:	8b 45 08             	mov    0x8(%ebp),%eax
  800dd3:	0f b6 00             	movzbl (%eax),%eax
  800dd6:	84 c0                	test   %al,%al
  800dd8:	74 1c                	je     800df6 <strsplit+0x5a>
  800dda:	8b 45 08             	mov    0x8(%ebp),%eax
  800ddd:	0f b6 00             	movzbl (%eax),%eax
  800de0:	0f be c0             	movsbl %al,%eax
  800de3:	89 44 24 04          	mov    %eax,0x4(%esp)
  800de7:	8b 45 0c             	mov    0xc(%ebp),%eax
  800dea:	89 04 24             	mov    %eax,(%esp)
  800ded:	e8 70 fc ff ff       	call   800a62 <strchr>
  800df2:	85 c0                	test   %eax,%eax
  800df4:	75 ce                	jne    800dc4 <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800df6:	8b 45 08             	mov    0x8(%ebp),%eax
  800df9:	0f b6 00             	movzbl (%eax),%eax
  800dfc:	84 c0                	test   %al,%al
  800dfe:	75 1f                	jne    800e1f <strsplit+0x83>
			break;
  800e00:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800e01:	8b 45 14             	mov    0x14(%ebp),%eax
  800e04:	8b 00                	mov    (%eax),%eax
  800e06:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e0d:	8b 45 10             	mov    0x10(%ebp),%eax
  800e10:	01 d0                	add    %edx,%eax
  800e12:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800e18:	b8 01 00 00 00       	mov    $0x1,%eax
  800e1d:	eb 61                	jmp    800e80 <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800e1f:	8b 45 14             	mov    0x14(%ebp),%eax
  800e22:	8b 00                	mov    (%eax),%eax
  800e24:	83 f8 0f             	cmp    $0xf,%eax
  800e27:	75 07                	jne    800e30 <strsplit+0x94>
		{
			return 0;
  800e29:	b8 00 00 00 00       	mov    $0x0,%eax
  800e2e:	eb 50                	jmp    800e80 <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800e30:	8b 45 14             	mov    0x14(%ebp),%eax
  800e33:	8b 00                	mov    (%eax),%eax
  800e35:	8d 48 01             	lea    0x1(%eax),%ecx
  800e38:	8b 55 14             	mov    0x14(%ebp),%edx
  800e3b:	89 0a                	mov    %ecx,(%edx)
  800e3d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e44:	8b 45 10             	mov    0x10(%ebp),%eax
  800e47:	01 c2                	add    %eax,%edx
  800e49:	8b 45 08             	mov    0x8(%ebp),%eax
  800e4c:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800e4e:	eb 04                	jmp    800e54 <strsplit+0xb8>
			string++;
  800e50:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800e54:	8b 45 08             	mov    0x8(%ebp),%eax
  800e57:	0f b6 00             	movzbl (%eax),%eax
  800e5a:	84 c0                	test   %al,%al
  800e5c:	74 1c                	je     800e7a <strsplit+0xde>
  800e5e:	8b 45 08             	mov    0x8(%ebp),%eax
  800e61:	0f b6 00             	movzbl (%eax),%eax
  800e64:	0f be c0             	movsbl %al,%eax
  800e67:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e6b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e6e:	89 04 24             	mov    %eax,(%esp)
  800e71:	e8 ec fb ff ff       	call   800a62 <strchr>
  800e76:	85 c0                	test   %eax,%eax
  800e78:	74 d6                	je     800e50 <strsplit+0xb4>
			string++;
	}
  800e7a:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e7b:	e9 50 ff ff ff       	jmp    800dd0 <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
  800e80:	c9                   	leave  
  800e81:	c3                   	ret    

00800e82 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800e82:	55                   	push   %ebp
  800e83:	89 e5                	mov    %esp,%ebp
  800e85:	57                   	push   %edi
  800e86:	56                   	push   %esi
  800e87:	53                   	push   %ebx
  800e88:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e8b:	8b 45 08             	mov    0x8(%ebp),%eax
  800e8e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e91:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800e94:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800e97:	8b 7d 18             	mov    0x18(%ebp),%edi
  800e9a:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800e9d:	cd 30                	int    $0x30
  800e9f:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800ea2:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800ea5:	83 c4 10             	add    $0x10,%esp
  800ea8:	5b                   	pop    %ebx
  800ea9:	5e                   	pop    %esi
  800eaa:	5f                   	pop    %edi
  800eab:	5d                   	pop    %ebp
  800eac:	c3                   	ret    

00800ead <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800ead:	55                   	push   %ebp
  800eae:	89 e5                	mov    %esp,%ebp
  800eb0:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800eb3:	8b 45 08             	mov    0x8(%ebp),%eax
  800eb6:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800ebd:	00 
  800ebe:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800ec5:	00 
  800ec6:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800ecd:	00 
  800ece:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ed1:	89 54 24 08          	mov    %edx,0x8(%esp)
  800ed5:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ed9:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800ee0:	e8 9d ff ff ff       	call   800e82 <syscall>
}
  800ee5:	c9                   	leave  
  800ee6:	c3                   	ret    

00800ee7 <sys_cgetc>:

int
sys_cgetc(void)
{
  800ee7:	55                   	push   %ebp
  800ee8:	89 e5                	mov    %esp,%ebp
  800eea:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800eed:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800ef4:	00 
  800ef5:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800efc:	00 
  800efd:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f04:	00 
  800f05:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f0c:	00 
  800f0d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f14:	00 
  800f15:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800f1c:	e8 61 ff ff ff       	call   800e82 <syscall>
}
  800f21:	c9                   	leave  
  800f22:	c3                   	ret    

00800f23 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800f23:	55                   	push   %ebp
  800f24:	89 e5                	mov    %esp,%ebp
  800f26:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800f29:	8b 45 08             	mov    0x8(%ebp),%eax
  800f2c:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f33:	00 
  800f34:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f3b:	00 
  800f3c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f43:	00 
  800f44:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f4b:	00 
  800f4c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f50:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  800f57:	e8 26 ff ff ff       	call   800e82 <syscall>
}
  800f5c:	c9                   	leave  
  800f5d:	c3                   	ret    

00800f5e <sys_getenvid>:

int32 sys_getenvid(void)
{
  800f5e:	55                   	push   %ebp
  800f5f:	89 e5                	mov    %esp,%ebp
  800f61:	83 ec 18             	sub    $0x18,%esp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800f64:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f6b:	00 
  800f6c:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f73:	00 
  800f74:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f7b:	00 
  800f7c:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f83:	00 
  800f84:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f8b:	00 
  800f8c:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  800f93:	e8 ea fe ff ff       	call   800e82 <syscall>
}
  800f98:	c9                   	leave  
  800f99:	c3                   	ret    

00800f9a <sys_env_sleep>:

void sys_env_sleep(void)
{
  800f9a:	55                   	push   %ebp
  800f9b:	89 e5                	mov    %esp,%ebp
  800f9d:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800fa0:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800fa7:	00 
  800fa8:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800faf:	00 
  800fb0:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800fb7:	00 
  800fb8:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800fbf:	00 
  800fc0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800fc7:	00 
  800fc8:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  800fcf:	e8 ae fe ff ff       	call   800e82 <syscall>
}
  800fd4:	c9                   	leave  
  800fd5:	c3                   	ret    

00800fd6 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800fd6:	55                   	push   %ebp
  800fd7:	89 e5                	mov    %esp,%ebp
  800fd9:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800fdc:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fdf:	8b 45 08             	mov    0x8(%ebp),%eax
  800fe2:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800fe9:	00 
  800fea:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800ff1:	00 
  800ff2:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800ff9:	00 
  800ffa:	89 54 24 08          	mov    %edx,0x8(%esp)
  800ffe:	89 44 24 04          	mov    %eax,0x4(%esp)
  801002:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  801009:	e8 74 fe ff ff       	call   800e82 <syscall>
}
  80100e:	c9                   	leave  
  80100f:	c3                   	ret    

00801010 <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  801010:	55                   	push   %ebp
  801011:	89 e5                	mov    %esp,%ebp
  801013:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  801016:	8b 55 0c             	mov    0xc(%ebp),%edx
  801019:	8b 45 08             	mov    0x8(%ebp),%eax
  80101c:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801023:	00 
  801024:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80102b:	00 
  80102c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801033:	00 
  801034:	89 54 24 08          	mov    %edx,0x8(%esp)
  801038:	89 44 24 04          	mov    %eax,0x4(%esp)
  80103c:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
  801043:	e8 3a fe ff ff       	call   800e82 <syscall>
}
  801048:	c9                   	leave  
  801049:	c3                   	ret    

0080104a <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  80104a:	55                   	push   %ebp
  80104b:	89 e5                	mov    %esp,%ebp
  80104d:	56                   	push   %esi
  80104e:	53                   	push   %ebx
  80104f:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  801052:	8b 75 18             	mov    0x18(%ebp),%esi
  801055:	8b 5d 14             	mov    0x14(%ebp),%ebx
  801058:	8b 4d 10             	mov    0x10(%ebp),%ecx
  80105b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80105e:	8b 45 08             	mov    0x8(%ebp),%eax
  801061:	89 74 24 14          	mov    %esi,0x14(%esp)
  801065:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  801069:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80106d:	89 54 24 08          	mov    %edx,0x8(%esp)
  801071:	89 44 24 04          	mov    %eax,0x4(%esp)
  801075:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  80107c:	e8 01 fe ff ff       	call   800e82 <syscall>
}
  801081:	83 c4 18             	add    $0x18,%esp
  801084:	5b                   	pop    %ebx
  801085:	5e                   	pop    %esi
  801086:	5d                   	pop    %ebp
  801087:	c3                   	ret    

00801088 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  801088:	55                   	push   %ebp
  801089:	89 e5                	mov    %esp,%ebp
  80108b:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  80108e:	8b 55 0c             	mov    0xc(%ebp),%edx
  801091:	8b 45 08             	mov    0x8(%ebp),%eax
  801094:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80109b:	00 
  80109c:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010a3:	00 
  8010a4:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010ab:	00 
  8010ac:	89 54 24 08          	mov    %edx,0x8(%esp)
  8010b0:	89 44 24 04          	mov    %eax,0x4(%esp)
  8010b4:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  8010bb:	e8 c2 fd ff ff       	call   800e82 <syscall>
}
  8010c0:	c9                   	leave  
  8010c1:	c3                   	ret    

008010c2 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  8010c2:	55                   	push   %ebp
  8010c3:	89 e5                	mov    %esp,%ebp
  8010c5:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  8010c8:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010cf:	00 
  8010d0:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010d7:	00 
  8010d8:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010df:	00 
  8010e0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8010e3:	89 44 24 08          	mov    %eax,0x8(%esp)
  8010e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8010ea:	89 44 24 04          	mov    %eax,0x4(%esp)
  8010ee:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
  8010f5:	e8 88 fd ff ff       	call   800e82 <syscall>
}
  8010fa:	c9                   	leave  
  8010fb:	c3                   	ret    

008010fc <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  8010fc:	55                   	push   %ebp
  8010fd:	89 e5                	mov    %esp,%ebp
  8010ff:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  801102:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801109:	00 
  80110a:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801111:	00 
  801112:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801119:	00 
  80111a:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801121:	00 
  801122:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  801129:	00 
  80112a:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  801131:	e8 4c fd ff ff       	call   800e82 <syscall>
}
  801136:	c9                   	leave  
  801137:	c3                   	ret    

00801138 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  801138:	55                   	push   %ebp
  801139:	89 e5                	mov    %esp,%ebp
  80113b:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  80113e:	8b 45 08             	mov    0x8(%ebp),%eax
  801141:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801148:	00 
  801149:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801150:	00 
  801151:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801158:	00 
  801159:	8b 55 0c             	mov    0xc(%ebp),%edx
  80115c:	89 54 24 08          	mov    %edx,0x8(%esp)
  801160:	89 44 24 04          	mov    %eax,0x4(%esp)
  801164:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
  80116b:	e8 12 fd ff ff       	call   800e82 <syscall>
	return;
  801170:	90                   	nop
}
  801171:	c9                   	leave  
  801172:	c3                   	ret    
  801173:	66 90                	xchg   %ax,%ax
  801175:	66 90                	xchg   %ax,%ax
  801177:	66 90                	xchg   %ax,%ax
  801179:	66 90                	xchg   %ax,%ax
  80117b:	66 90                	xchg   %ax,%ax
  80117d:	66 90                	xchg   %ax,%ax
  80117f:	90                   	nop

00801180 <__udivdi3>:
  801180:	55                   	push   %ebp
  801181:	57                   	push   %edi
  801182:	56                   	push   %esi
  801183:	83 ec 0c             	sub    $0xc,%esp
  801186:	8b 44 24 28          	mov    0x28(%esp),%eax
  80118a:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
  80118e:	8b 6c 24 20          	mov    0x20(%esp),%ebp
  801192:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  801196:	85 c0                	test   %eax,%eax
  801198:	89 7c 24 04          	mov    %edi,0x4(%esp)
  80119c:	89 ea                	mov    %ebp,%edx
  80119e:	89 0c 24             	mov    %ecx,(%esp)
  8011a1:	75 2d                	jne    8011d0 <__udivdi3+0x50>
  8011a3:	39 e9                	cmp    %ebp,%ecx
  8011a5:	77 61                	ja     801208 <__udivdi3+0x88>
  8011a7:	85 c9                	test   %ecx,%ecx
  8011a9:	89 ce                	mov    %ecx,%esi
  8011ab:	75 0b                	jne    8011b8 <__udivdi3+0x38>
  8011ad:	b8 01 00 00 00       	mov    $0x1,%eax
  8011b2:	31 d2                	xor    %edx,%edx
  8011b4:	f7 f1                	div    %ecx
  8011b6:	89 c6                	mov    %eax,%esi
  8011b8:	31 d2                	xor    %edx,%edx
  8011ba:	89 e8                	mov    %ebp,%eax
  8011bc:	f7 f6                	div    %esi
  8011be:	89 c5                	mov    %eax,%ebp
  8011c0:	89 f8                	mov    %edi,%eax
  8011c2:	f7 f6                	div    %esi
  8011c4:	89 ea                	mov    %ebp,%edx
  8011c6:	83 c4 0c             	add    $0xc,%esp
  8011c9:	5e                   	pop    %esi
  8011ca:	5f                   	pop    %edi
  8011cb:	5d                   	pop    %ebp
  8011cc:	c3                   	ret    
  8011cd:	8d 76 00             	lea    0x0(%esi),%esi
  8011d0:	39 e8                	cmp    %ebp,%eax
  8011d2:	77 24                	ja     8011f8 <__udivdi3+0x78>
  8011d4:	0f bd e8             	bsr    %eax,%ebp
  8011d7:	83 f5 1f             	xor    $0x1f,%ebp
  8011da:	75 3c                	jne    801218 <__udivdi3+0x98>
  8011dc:	8b 74 24 04          	mov    0x4(%esp),%esi
  8011e0:	39 34 24             	cmp    %esi,(%esp)
  8011e3:	0f 86 9f 00 00 00    	jbe    801288 <__udivdi3+0x108>
  8011e9:	39 d0                	cmp    %edx,%eax
  8011eb:	0f 82 97 00 00 00    	jb     801288 <__udivdi3+0x108>
  8011f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8011f8:	31 d2                	xor    %edx,%edx
  8011fa:	31 c0                	xor    %eax,%eax
  8011fc:	83 c4 0c             	add    $0xc,%esp
  8011ff:	5e                   	pop    %esi
  801200:	5f                   	pop    %edi
  801201:	5d                   	pop    %ebp
  801202:	c3                   	ret    
  801203:	90                   	nop
  801204:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801208:	89 f8                	mov    %edi,%eax
  80120a:	f7 f1                	div    %ecx
  80120c:	31 d2                	xor    %edx,%edx
  80120e:	83 c4 0c             	add    $0xc,%esp
  801211:	5e                   	pop    %esi
  801212:	5f                   	pop    %edi
  801213:	5d                   	pop    %ebp
  801214:	c3                   	ret    
  801215:	8d 76 00             	lea    0x0(%esi),%esi
  801218:	89 e9                	mov    %ebp,%ecx
  80121a:	8b 3c 24             	mov    (%esp),%edi
  80121d:	d3 e0                	shl    %cl,%eax
  80121f:	89 c6                	mov    %eax,%esi
  801221:	b8 20 00 00 00       	mov    $0x20,%eax
  801226:	29 e8                	sub    %ebp,%eax
  801228:	89 c1                	mov    %eax,%ecx
  80122a:	d3 ef                	shr    %cl,%edi
  80122c:	89 e9                	mov    %ebp,%ecx
  80122e:	89 7c 24 08          	mov    %edi,0x8(%esp)
  801232:	8b 3c 24             	mov    (%esp),%edi
  801235:	09 74 24 08          	or     %esi,0x8(%esp)
  801239:	89 d6                	mov    %edx,%esi
  80123b:	d3 e7                	shl    %cl,%edi
  80123d:	89 c1                	mov    %eax,%ecx
  80123f:	89 3c 24             	mov    %edi,(%esp)
  801242:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801246:	d3 ee                	shr    %cl,%esi
  801248:	89 e9                	mov    %ebp,%ecx
  80124a:	d3 e2                	shl    %cl,%edx
  80124c:	89 c1                	mov    %eax,%ecx
  80124e:	d3 ef                	shr    %cl,%edi
  801250:	09 d7                	or     %edx,%edi
  801252:	89 f2                	mov    %esi,%edx
  801254:	89 f8                	mov    %edi,%eax
  801256:	f7 74 24 08          	divl   0x8(%esp)
  80125a:	89 d6                	mov    %edx,%esi
  80125c:	89 c7                	mov    %eax,%edi
  80125e:	f7 24 24             	mull   (%esp)
  801261:	39 d6                	cmp    %edx,%esi
  801263:	89 14 24             	mov    %edx,(%esp)
  801266:	72 30                	jb     801298 <__udivdi3+0x118>
  801268:	8b 54 24 04          	mov    0x4(%esp),%edx
  80126c:	89 e9                	mov    %ebp,%ecx
  80126e:	d3 e2                	shl    %cl,%edx
  801270:	39 c2                	cmp    %eax,%edx
  801272:	73 05                	jae    801279 <__udivdi3+0xf9>
  801274:	3b 34 24             	cmp    (%esp),%esi
  801277:	74 1f                	je     801298 <__udivdi3+0x118>
  801279:	89 f8                	mov    %edi,%eax
  80127b:	31 d2                	xor    %edx,%edx
  80127d:	e9 7a ff ff ff       	jmp    8011fc <__udivdi3+0x7c>
  801282:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801288:	31 d2                	xor    %edx,%edx
  80128a:	b8 01 00 00 00       	mov    $0x1,%eax
  80128f:	e9 68 ff ff ff       	jmp    8011fc <__udivdi3+0x7c>
  801294:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801298:	8d 47 ff             	lea    -0x1(%edi),%eax
  80129b:	31 d2                	xor    %edx,%edx
  80129d:	83 c4 0c             	add    $0xc,%esp
  8012a0:	5e                   	pop    %esi
  8012a1:	5f                   	pop    %edi
  8012a2:	5d                   	pop    %ebp
  8012a3:	c3                   	ret    
  8012a4:	66 90                	xchg   %ax,%ax
  8012a6:	66 90                	xchg   %ax,%ax
  8012a8:	66 90                	xchg   %ax,%ax
  8012aa:	66 90                	xchg   %ax,%ax
  8012ac:	66 90                	xchg   %ax,%ax
  8012ae:	66 90                	xchg   %ax,%ax

008012b0 <__umoddi3>:
  8012b0:	55                   	push   %ebp
  8012b1:	57                   	push   %edi
  8012b2:	56                   	push   %esi
  8012b3:	83 ec 14             	sub    $0x14,%esp
  8012b6:	8b 44 24 28          	mov    0x28(%esp),%eax
  8012ba:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  8012be:	8b 74 24 2c          	mov    0x2c(%esp),%esi
  8012c2:	89 c7                	mov    %eax,%edi
  8012c4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8012c8:	8b 44 24 30          	mov    0x30(%esp),%eax
  8012cc:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  8012d0:	89 34 24             	mov    %esi,(%esp)
  8012d3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8012d7:	85 c0                	test   %eax,%eax
  8012d9:	89 c2                	mov    %eax,%edx
  8012db:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8012df:	75 17                	jne    8012f8 <__umoddi3+0x48>
  8012e1:	39 fe                	cmp    %edi,%esi
  8012e3:	76 4b                	jbe    801330 <__umoddi3+0x80>
  8012e5:	89 c8                	mov    %ecx,%eax
  8012e7:	89 fa                	mov    %edi,%edx
  8012e9:	f7 f6                	div    %esi
  8012eb:	89 d0                	mov    %edx,%eax
  8012ed:	31 d2                	xor    %edx,%edx
  8012ef:	83 c4 14             	add    $0x14,%esp
  8012f2:	5e                   	pop    %esi
  8012f3:	5f                   	pop    %edi
  8012f4:	5d                   	pop    %ebp
  8012f5:	c3                   	ret    
  8012f6:	66 90                	xchg   %ax,%ax
  8012f8:	39 f8                	cmp    %edi,%eax
  8012fa:	77 54                	ja     801350 <__umoddi3+0xa0>
  8012fc:	0f bd e8             	bsr    %eax,%ebp
  8012ff:	83 f5 1f             	xor    $0x1f,%ebp
  801302:	75 5c                	jne    801360 <__umoddi3+0xb0>
  801304:	8b 7c 24 08          	mov    0x8(%esp),%edi
  801308:	39 3c 24             	cmp    %edi,(%esp)
  80130b:	0f 87 e7 00 00 00    	ja     8013f8 <__umoddi3+0x148>
  801311:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801315:	29 f1                	sub    %esi,%ecx
  801317:	19 c7                	sbb    %eax,%edi
  801319:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80131d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801321:	8b 44 24 08          	mov    0x8(%esp),%eax
  801325:	8b 54 24 0c          	mov    0xc(%esp),%edx
  801329:	83 c4 14             	add    $0x14,%esp
  80132c:	5e                   	pop    %esi
  80132d:	5f                   	pop    %edi
  80132e:	5d                   	pop    %ebp
  80132f:	c3                   	ret    
  801330:	85 f6                	test   %esi,%esi
  801332:	89 f5                	mov    %esi,%ebp
  801334:	75 0b                	jne    801341 <__umoddi3+0x91>
  801336:	b8 01 00 00 00       	mov    $0x1,%eax
  80133b:	31 d2                	xor    %edx,%edx
  80133d:	f7 f6                	div    %esi
  80133f:	89 c5                	mov    %eax,%ebp
  801341:	8b 44 24 04          	mov    0x4(%esp),%eax
  801345:	31 d2                	xor    %edx,%edx
  801347:	f7 f5                	div    %ebp
  801349:	89 c8                	mov    %ecx,%eax
  80134b:	f7 f5                	div    %ebp
  80134d:	eb 9c                	jmp    8012eb <__umoddi3+0x3b>
  80134f:	90                   	nop
  801350:	89 c8                	mov    %ecx,%eax
  801352:	89 fa                	mov    %edi,%edx
  801354:	83 c4 14             	add    $0x14,%esp
  801357:	5e                   	pop    %esi
  801358:	5f                   	pop    %edi
  801359:	5d                   	pop    %ebp
  80135a:	c3                   	ret    
  80135b:	90                   	nop
  80135c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801360:	8b 04 24             	mov    (%esp),%eax
  801363:	be 20 00 00 00       	mov    $0x20,%esi
  801368:	89 e9                	mov    %ebp,%ecx
  80136a:	29 ee                	sub    %ebp,%esi
  80136c:	d3 e2                	shl    %cl,%edx
  80136e:	89 f1                	mov    %esi,%ecx
  801370:	d3 e8                	shr    %cl,%eax
  801372:	89 e9                	mov    %ebp,%ecx
  801374:	89 44 24 04          	mov    %eax,0x4(%esp)
  801378:	8b 04 24             	mov    (%esp),%eax
  80137b:	09 54 24 04          	or     %edx,0x4(%esp)
  80137f:	89 fa                	mov    %edi,%edx
  801381:	d3 e0                	shl    %cl,%eax
  801383:	89 f1                	mov    %esi,%ecx
  801385:	89 44 24 08          	mov    %eax,0x8(%esp)
  801389:	8b 44 24 10          	mov    0x10(%esp),%eax
  80138d:	d3 ea                	shr    %cl,%edx
  80138f:	89 e9                	mov    %ebp,%ecx
  801391:	d3 e7                	shl    %cl,%edi
  801393:	89 f1                	mov    %esi,%ecx
  801395:	d3 e8                	shr    %cl,%eax
  801397:	89 e9                	mov    %ebp,%ecx
  801399:	09 f8                	or     %edi,%eax
  80139b:	8b 7c 24 10          	mov    0x10(%esp),%edi
  80139f:	f7 74 24 04          	divl   0x4(%esp)
  8013a3:	d3 e7                	shl    %cl,%edi
  8013a5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8013a9:	89 d7                	mov    %edx,%edi
  8013ab:	f7 64 24 08          	mull   0x8(%esp)
  8013af:	39 d7                	cmp    %edx,%edi
  8013b1:	89 c1                	mov    %eax,%ecx
  8013b3:	89 14 24             	mov    %edx,(%esp)
  8013b6:	72 2c                	jb     8013e4 <__umoddi3+0x134>
  8013b8:	39 44 24 0c          	cmp    %eax,0xc(%esp)
  8013bc:	72 22                	jb     8013e0 <__umoddi3+0x130>
  8013be:	8b 44 24 0c          	mov    0xc(%esp),%eax
  8013c2:	29 c8                	sub    %ecx,%eax
  8013c4:	19 d7                	sbb    %edx,%edi
  8013c6:	89 e9                	mov    %ebp,%ecx
  8013c8:	89 fa                	mov    %edi,%edx
  8013ca:	d3 e8                	shr    %cl,%eax
  8013cc:	89 f1                	mov    %esi,%ecx
  8013ce:	d3 e2                	shl    %cl,%edx
  8013d0:	89 e9                	mov    %ebp,%ecx
  8013d2:	d3 ef                	shr    %cl,%edi
  8013d4:	09 d0                	or     %edx,%eax
  8013d6:	89 fa                	mov    %edi,%edx
  8013d8:	83 c4 14             	add    $0x14,%esp
  8013db:	5e                   	pop    %esi
  8013dc:	5f                   	pop    %edi
  8013dd:	5d                   	pop    %ebp
  8013de:	c3                   	ret    
  8013df:	90                   	nop
  8013e0:	39 d7                	cmp    %edx,%edi
  8013e2:	75 da                	jne    8013be <__umoddi3+0x10e>
  8013e4:	8b 14 24             	mov    (%esp),%edx
  8013e7:	89 c1                	mov    %eax,%ecx
  8013e9:	2b 4c 24 08          	sub    0x8(%esp),%ecx
  8013ed:	1b 54 24 04          	sbb    0x4(%esp),%edx
  8013f1:	eb cb                	jmp    8013be <__umoddi3+0x10e>
  8013f3:	90                   	nop
  8013f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8013f8:	3b 44 24 0c          	cmp    0xc(%esp),%eax
  8013fc:	0f 82 0f ff ff ff    	jb     801311 <__umoddi3+0x61>
  801402:	e9 1a ff ff ff       	jmp    801321 <__umoddi3+0x71>
