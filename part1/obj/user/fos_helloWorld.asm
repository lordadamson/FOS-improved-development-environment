
obj/user/fos_helloWorld:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 16 00 00 00       	call   80004c <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
// hello, world
#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 18             	sub    $0x18,%esp
	cprintf("HELLO WORLD , FOS IS SAYING HI :D:D:D\n");	
  80003e:	c7 04 24 a0 13 80 00 	movl   $0x8013a0,(%esp)
  800045:	e8 1c 01 00 00       	call   800166 <cprintf>
}
  80004a:	c9                   	leave  
  80004b:	c3                   	ret    

0080004c <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  80004c:	55                   	push   %ebp
  80004d:	89 e5                	mov    %esp,%ebp
  80004f:	83 ec 18             	sub    $0x18,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  800052:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  800059:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  80005c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800060:	7e 0a                	jle    80006c <libmain+0x20>
		binaryname = argv[0];
  800062:	8b 45 0c             	mov    0xc(%ebp),%eax
  800065:	8b 00                	mov    (%eax),%eax
  800067:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  80006c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80006f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800073:	8b 45 08             	mov    0x8(%ebp),%eax
  800076:	89 04 24             	mov    %eax,(%esp)
  800079:	e8 ba ff ff ff       	call   800038 <_main>

	// exit gracefully
	//exit();
	sleep();
  80007e:	e8 16 00 00 00       	call   800099 <sleep>
}
  800083:	c9                   	leave  
  800084:	c3                   	ret    

00800085 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800085:	55                   	push   %ebp
  800086:	89 e5                	mov    %esp,%ebp
  800088:	83 ec 18             	sub    $0x18,%esp
	sys_env_destroy(0);	
  80008b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800092:	e8 29 0e 00 00       	call   800ec0 <sys_env_destroy>
}
  800097:	c9                   	leave  
  800098:	c3                   	ret    

00800099 <sleep>:

void
sleep(void)
{	
  800099:	55                   	push   %ebp
  80009a:	89 e5                	mov    %esp,%ebp
  80009c:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  80009f:	e8 93 0e 00 00       	call   800f37 <sys_env_sleep>
}
  8000a4:	c9                   	leave  
  8000a5:	c3                   	ret    

008000a6 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000a6:	55                   	push   %ebp
  8000a7:	89 e5                	mov    %esp,%ebp
  8000a9:	83 ec 18             	sub    $0x18,%esp
	b->buf[b->idx++] = ch;
  8000ac:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000af:	8b 00                	mov    (%eax),%eax
  8000b1:	8d 48 01             	lea    0x1(%eax),%ecx
  8000b4:	8b 55 0c             	mov    0xc(%ebp),%edx
  8000b7:	89 0a                	mov    %ecx,(%edx)
  8000b9:	8b 55 08             	mov    0x8(%ebp),%edx
  8000bc:	89 d1                	mov    %edx,%ecx
  8000be:	8b 55 0c             	mov    0xc(%ebp),%edx
  8000c1:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  8000c5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000c8:	8b 00                	mov    (%eax),%eax
  8000ca:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000cf:	75 20                	jne    8000f1 <putch+0x4b>
		sys_cputs(b->buf, b->idx);
  8000d1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000d4:	8b 00                	mov    (%eax),%eax
  8000d6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8000d9:	83 c2 08             	add    $0x8,%edx
  8000dc:	89 44 24 04          	mov    %eax,0x4(%esp)
  8000e0:	89 14 24             	mov    %edx,(%esp)
  8000e3:	e8 62 0d 00 00       	call   800e4a <sys_cputs>
		b->idx = 0;
  8000e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000eb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  8000f1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000f4:	8b 40 04             	mov    0x4(%eax),%eax
  8000f7:	8d 50 01             	lea    0x1(%eax),%edx
  8000fa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000fd:	89 50 04             	mov    %edx,0x4(%eax)
}
  800100:	c9                   	leave  
  800101:	c3                   	ret    

00800102 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800102:	55                   	push   %ebp
  800103:	89 e5                	mov    %esp,%ebp
  800105:	81 ec 28 01 00 00    	sub    $0x128,%esp
	struct printbuf b;

	b.idx = 0;
  80010b:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800112:	00 00 00 
	b.cnt = 0;
  800115:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80011c:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80011f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800122:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800126:	8b 45 08             	mov    0x8(%ebp),%eax
  800129:	89 44 24 08          	mov    %eax,0x8(%esp)
  80012d:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800133:	89 44 24 04          	mov    %eax,0x4(%esp)
  800137:	c7 04 24 a6 00 80 00 	movl   $0x8000a6,(%esp)
  80013e:	e8 ed 01 00 00       	call   800330 <vprintfmt>
	sys_cputs(b.buf, b.idx);
  800143:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  800149:	89 44 24 04          	mov    %eax,0x4(%esp)
  80014d:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800153:	83 c0 08             	add    $0x8,%eax
  800156:	89 04 24             	mov    %eax,(%esp)
  800159:	e8 ec 0c 00 00       	call   800e4a <sys_cputs>

	return b.cnt;
  80015e:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  800164:	c9                   	leave  
  800165:	c3                   	ret    

00800166 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800166:	55                   	push   %ebp
  800167:	89 e5                	mov    %esp,%ebp
  800169:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80016c:	8d 45 0c             	lea    0xc(%ebp),%eax
  80016f:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  800172:	8b 45 08             	mov    0x8(%ebp),%eax
  800175:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800178:	89 54 24 04          	mov    %edx,0x4(%esp)
  80017c:	89 04 24             	mov    %eax,(%esp)
  80017f:	e8 7e ff ff ff       	call   800102 <vcprintf>
  800184:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  800187:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80018a:	c9                   	leave  
  80018b:	c3                   	ret    

0080018c <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80018c:	55                   	push   %ebp
  80018d:	89 e5                	mov    %esp,%ebp
  80018f:	53                   	push   %ebx
  800190:	83 ec 34             	sub    $0x34,%esp
  800193:	8b 45 10             	mov    0x10(%ebp),%eax
  800196:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800199:	8b 45 14             	mov    0x14(%ebp),%eax
  80019c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80019f:	8b 45 18             	mov    0x18(%ebp),%eax
  8001a2:	ba 00 00 00 00       	mov    $0x0,%edx
  8001a7:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001aa:	77 72                	ja     80021e <printnum+0x92>
  8001ac:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001af:	72 05                	jb     8001b6 <printnum+0x2a>
  8001b1:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8001b4:	77 68                	ja     80021e <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b6:	8b 45 1c             	mov    0x1c(%ebp),%eax
  8001b9:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001bc:	8b 45 18             	mov    0x18(%ebp),%eax
  8001bf:	ba 00 00 00 00       	mov    $0x0,%edx
  8001c4:	89 44 24 08          	mov    %eax,0x8(%esp)
  8001c8:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8001cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8001cf:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8001d2:	89 04 24             	mov    %eax,(%esp)
  8001d5:	89 54 24 04          	mov    %edx,0x4(%esp)
  8001d9:	e8 32 0f 00 00       	call   801110 <__udivdi3>
  8001de:	8b 4d 20             	mov    0x20(%ebp),%ecx
  8001e1:	89 4c 24 18          	mov    %ecx,0x18(%esp)
  8001e5:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  8001e9:	8b 4d 18             	mov    0x18(%ebp),%ecx
  8001ec:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  8001f0:	89 44 24 08          	mov    %eax,0x8(%esp)
  8001f4:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8001f8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8001fb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800202:	89 04 24             	mov    %eax,(%esp)
  800205:	e8 82 ff ff ff       	call   80018c <printnum>
  80020a:	eb 1c                	jmp    800228 <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80020c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80020f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800213:	8b 45 20             	mov    0x20(%ebp),%eax
  800216:	89 04 24             	mov    %eax,(%esp)
  800219:	8b 45 08             	mov    0x8(%ebp),%eax
  80021c:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80021e:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  800222:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  800226:	7f e4                	jg     80020c <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800228:	8b 4d 18             	mov    0x18(%ebp),%ecx
  80022b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800230:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800233:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800236:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80023a:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  80023e:	89 04 24             	mov    %eax,(%esp)
  800241:	89 54 24 04          	mov    %edx,0x4(%esp)
  800245:	e8 f6 0f 00 00       	call   801240 <__umoddi3>
  80024a:	05 80 14 80 00       	add    $0x801480,%eax
  80024f:	0f b6 00             	movzbl (%eax),%eax
  800252:	0f be c0             	movsbl %al,%eax
  800255:	8b 55 0c             	mov    0xc(%ebp),%edx
  800258:	89 54 24 04          	mov    %edx,0x4(%esp)
  80025c:	89 04 24             	mov    %eax,(%esp)
  80025f:	8b 45 08             	mov    0x8(%ebp),%eax
  800262:	ff d0                	call   *%eax
}
  800264:	83 c4 34             	add    $0x34,%esp
  800267:	5b                   	pop    %ebx
  800268:	5d                   	pop    %ebp
  800269:	c3                   	ret    

0080026a <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80026a:	55                   	push   %ebp
  80026b:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80026d:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800271:	7e 1c                	jle    80028f <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  800273:	8b 45 08             	mov    0x8(%ebp),%eax
  800276:	8b 00                	mov    (%eax),%eax
  800278:	8d 50 08             	lea    0x8(%eax),%edx
  80027b:	8b 45 08             	mov    0x8(%ebp),%eax
  80027e:	89 10                	mov    %edx,(%eax)
  800280:	8b 45 08             	mov    0x8(%ebp),%eax
  800283:	8b 00                	mov    (%eax),%eax
  800285:	83 e8 08             	sub    $0x8,%eax
  800288:	8b 50 04             	mov    0x4(%eax),%edx
  80028b:	8b 00                	mov    (%eax),%eax
  80028d:	eb 40                	jmp    8002cf <getuint+0x65>
	else if (lflag)
  80028f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800293:	74 1e                	je     8002b3 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  800295:	8b 45 08             	mov    0x8(%ebp),%eax
  800298:	8b 00                	mov    (%eax),%eax
  80029a:	8d 50 04             	lea    0x4(%eax),%edx
  80029d:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a0:	89 10                	mov    %edx,(%eax)
  8002a2:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a5:	8b 00                	mov    (%eax),%eax
  8002a7:	83 e8 04             	sub    $0x4,%eax
  8002aa:	8b 00                	mov    (%eax),%eax
  8002ac:	ba 00 00 00 00       	mov    $0x0,%edx
  8002b1:	eb 1c                	jmp    8002cf <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  8002b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002b6:	8b 00                	mov    (%eax),%eax
  8002b8:	8d 50 04             	lea    0x4(%eax),%edx
  8002bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002be:	89 10                	mov    %edx,(%eax)
  8002c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c3:	8b 00                	mov    (%eax),%eax
  8002c5:	83 e8 04             	sub    $0x4,%eax
  8002c8:	8b 00                	mov    (%eax),%eax
  8002ca:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002cf:	5d                   	pop    %ebp
  8002d0:	c3                   	ret    

008002d1 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  8002d1:	55                   	push   %ebp
  8002d2:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002d4:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002d8:	7e 1c                	jle    8002f6 <getint+0x25>
		return va_arg(*ap, long long);
  8002da:	8b 45 08             	mov    0x8(%ebp),%eax
  8002dd:	8b 00                	mov    (%eax),%eax
  8002df:	8d 50 08             	lea    0x8(%eax),%edx
  8002e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e5:	89 10                	mov    %edx,(%eax)
  8002e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ea:	8b 00                	mov    (%eax),%eax
  8002ec:	83 e8 08             	sub    $0x8,%eax
  8002ef:	8b 50 04             	mov    0x4(%eax),%edx
  8002f2:	8b 00                	mov    (%eax),%eax
  8002f4:	eb 38                	jmp    80032e <getint+0x5d>
	else if (lflag)
  8002f6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002fa:	74 1a                	je     800316 <getint+0x45>
		return va_arg(*ap, long);
  8002fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ff:	8b 00                	mov    (%eax),%eax
  800301:	8d 50 04             	lea    0x4(%eax),%edx
  800304:	8b 45 08             	mov    0x8(%ebp),%eax
  800307:	89 10                	mov    %edx,(%eax)
  800309:	8b 45 08             	mov    0x8(%ebp),%eax
  80030c:	8b 00                	mov    (%eax),%eax
  80030e:	83 e8 04             	sub    $0x4,%eax
  800311:	8b 00                	mov    (%eax),%eax
  800313:	99                   	cltd   
  800314:	eb 18                	jmp    80032e <getint+0x5d>
	else
		return va_arg(*ap, int);
  800316:	8b 45 08             	mov    0x8(%ebp),%eax
  800319:	8b 00                	mov    (%eax),%eax
  80031b:	8d 50 04             	lea    0x4(%eax),%edx
  80031e:	8b 45 08             	mov    0x8(%ebp),%eax
  800321:	89 10                	mov    %edx,(%eax)
  800323:	8b 45 08             	mov    0x8(%ebp),%eax
  800326:	8b 00                	mov    (%eax),%eax
  800328:	83 e8 04             	sub    $0x4,%eax
  80032b:	8b 00                	mov    (%eax),%eax
  80032d:	99                   	cltd   
}
  80032e:	5d                   	pop    %ebp
  80032f:	c3                   	ret    

00800330 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800330:	55                   	push   %ebp
  800331:	89 e5                	mov    %esp,%ebp
  800333:	56                   	push   %esi
  800334:	53                   	push   %ebx
  800335:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800338:	eb 18                	jmp    800352 <vprintfmt+0x22>
			if (ch == '\0')
  80033a:	85 db                	test   %ebx,%ebx
  80033c:	75 05                	jne    800343 <vprintfmt+0x13>
				return;
  80033e:	e9 07 04 00 00       	jmp    80074a <vprintfmt+0x41a>
			putch(ch, putdat);
  800343:	8b 45 0c             	mov    0xc(%ebp),%eax
  800346:	89 44 24 04          	mov    %eax,0x4(%esp)
  80034a:	89 1c 24             	mov    %ebx,(%esp)
  80034d:	8b 45 08             	mov    0x8(%ebp),%eax
  800350:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800352:	8b 45 10             	mov    0x10(%ebp),%eax
  800355:	8d 50 01             	lea    0x1(%eax),%edx
  800358:	89 55 10             	mov    %edx,0x10(%ebp)
  80035b:	0f b6 00             	movzbl (%eax),%eax
  80035e:	0f b6 d8             	movzbl %al,%ebx
  800361:	83 fb 25             	cmp    $0x25,%ebx
  800364:	75 d4                	jne    80033a <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  800366:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  80036a:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  800371:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  800378:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  80037f:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800386:	8b 45 10             	mov    0x10(%ebp),%eax
  800389:	8d 50 01             	lea    0x1(%eax),%edx
  80038c:	89 55 10             	mov    %edx,0x10(%ebp)
  80038f:	0f b6 00             	movzbl (%eax),%eax
  800392:	0f b6 d8             	movzbl %al,%ebx
  800395:	8d 43 dd             	lea    -0x23(%ebx),%eax
  800398:	83 f8 55             	cmp    $0x55,%eax
  80039b:	0f 87 78 03 00 00    	ja     800719 <vprintfmt+0x3e9>
  8003a1:	8b 04 85 a4 14 80 00 	mov    0x8014a4(,%eax,4),%eax
  8003a8:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  8003aa:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  8003ae:	eb d6                	jmp    800386 <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003b0:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  8003b4:	eb d0                	jmp    800386 <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003b6:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  8003bd:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003c0:	89 d0                	mov    %edx,%eax
  8003c2:	c1 e0 02             	shl    $0x2,%eax
  8003c5:	01 d0                	add    %edx,%eax
  8003c7:	01 c0                	add    %eax,%eax
  8003c9:	01 d8                	add    %ebx,%eax
  8003cb:	83 e8 30             	sub    $0x30,%eax
  8003ce:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  8003d1:	8b 45 10             	mov    0x10(%ebp),%eax
  8003d4:	0f b6 00             	movzbl (%eax),%eax
  8003d7:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  8003da:	83 fb 2f             	cmp    $0x2f,%ebx
  8003dd:	7e 0b                	jle    8003ea <vprintfmt+0xba>
  8003df:	83 fb 39             	cmp    $0x39,%ebx
  8003e2:	7f 06                	jg     8003ea <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003e4:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003e8:	eb d3                	jmp    8003bd <vprintfmt+0x8d>
			goto process_precision;
  8003ea:	eb 39                	jmp    800425 <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
  8003ec:	8b 45 14             	mov    0x14(%ebp),%eax
  8003ef:	83 c0 04             	add    $0x4,%eax
  8003f2:	89 45 14             	mov    %eax,0x14(%ebp)
  8003f5:	8b 45 14             	mov    0x14(%ebp),%eax
  8003f8:	83 e8 04             	sub    $0x4,%eax
  8003fb:	8b 00                	mov    (%eax),%eax
  8003fd:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800400:	eb 23                	jmp    800425 <vprintfmt+0xf5>

		case '.':
			if (width < 0)
  800402:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800406:	79 0c                	jns    800414 <vprintfmt+0xe4>
				width = 0;
  800408:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  80040f:	e9 72 ff ff ff       	jmp    800386 <vprintfmt+0x56>
  800414:	e9 6d ff ff ff       	jmp    800386 <vprintfmt+0x56>

		case '#':
			altflag = 1;
  800419:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  800420:	e9 61 ff ff ff       	jmp    800386 <vprintfmt+0x56>

		process_precision:
			if (width < 0)
  800425:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800429:	79 12                	jns    80043d <vprintfmt+0x10d>
				width = precision, precision = -1;
  80042b:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80042e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800431:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  800438:	e9 49 ff ff ff       	jmp    800386 <vprintfmt+0x56>
  80043d:	e9 44 ff ff ff       	jmp    800386 <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800442:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  800446:	e9 3b ff ff ff       	jmp    800386 <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80044b:	8b 45 14             	mov    0x14(%ebp),%eax
  80044e:	83 c0 04             	add    $0x4,%eax
  800451:	89 45 14             	mov    %eax,0x14(%ebp)
  800454:	8b 45 14             	mov    0x14(%ebp),%eax
  800457:	83 e8 04             	sub    $0x4,%eax
  80045a:	8b 00                	mov    (%eax),%eax
  80045c:	8b 55 0c             	mov    0xc(%ebp),%edx
  80045f:	89 54 24 04          	mov    %edx,0x4(%esp)
  800463:	89 04 24             	mov    %eax,(%esp)
  800466:	8b 45 08             	mov    0x8(%ebp),%eax
  800469:	ff d0                	call   *%eax
			break;
  80046b:	e9 d4 02 00 00       	jmp    800744 <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800470:	8b 45 14             	mov    0x14(%ebp),%eax
  800473:	83 c0 04             	add    $0x4,%eax
  800476:	89 45 14             	mov    %eax,0x14(%ebp)
  800479:	8b 45 14             	mov    0x14(%ebp),%eax
  80047c:	83 e8 04             	sub    $0x4,%eax
  80047f:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  800481:	85 db                	test   %ebx,%ebx
  800483:	79 02                	jns    800487 <vprintfmt+0x157>
				err = -err;
  800485:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  800487:	83 fb 07             	cmp    $0x7,%ebx
  80048a:	7f 0b                	jg     800497 <vprintfmt+0x167>
  80048c:	8b 34 9d 60 14 80 00 	mov    0x801460(,%ebx,4),%esi
  800493:	85 f6                	test   %esi,%esi
  800495:	75 23                	jne    8004ba <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
  800497:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  80049b:	c7 44 24 08 91 14 80 	movl   $0x801491,0x8(%esp)
  8004a2:	00 
  8004a3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8004a6:	89 44 24 04          	mov    %eax,0x4(%esp)
  8004aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8004ad:	89 04 24             	mov    %eax,(%esp)
  8004b0:	e8 9c 02 00 00       	call   800751 <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
  8004b5:	e9 8a 02 00 00       	jmp    800744 <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  8004ba:	89 74 24 0c          	mov    %esi,0xc(%esp)
  8004be:	c7 44 24 08 9a 14 80 	movl   $0x80149a,0x8(%esp)
  8004c5:	00 
  8004c6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8004c9:	89 44 24 04          	mov    %eax,0x4(%esp)
  8004cd:	8b 45 08             	mov    0x8(%ebp),%eax
  8004d0:	89 04 24             	mov    %eax,(%esp)
  8004d3:	e8 79 02 00 00       	call   800751 <printfmt>
			break;
  8004d8:	e9 67 02 00 00       	jmp    800744 <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004dd:	8b 45 14             	mov    0x14(%ebp),%eax
  8004e0:	83 c0 04             	add    $0x4,%eax
  8004e3:	89 45 14             	mov    %eax,0x14(%ebp)
  8004e6:	8b 45 14             	mov    0x14(%ebp),%eax
  8004e9:	83 e8 04             	sub    $0x4,%eax
  8004ec:	8b 30                	mov    (%eax),%esi
  8004ee:	85 f6                	test   %esi,%esi
  8004f0:	75 05                	jne    8004f7 <vprintfmt+0x1c7>
				p = "(null)";
  8004f2:	be 9d 14 80 00       	mov    $0x80149d,%esi
			if (width > 0 && padc != '-')
  8004f7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004fb:	7e 37                	jle    800534 <vprintfmt+0x204>
  8004fd:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  800501:	74 31                	je     800534 <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
  800503:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800506:	89 44 24 04          	mov    %eax,0x4(%esp)
  80050a:	89 34 24             	mov    %esi,(%esp)
  80050d:	e8 62 03 00 00       	call   800874 <strnlen>
  800512:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  800515:	eb 17                	jmp    80052e <vprintfmt+0x1fe>
					putch(padc, putdat);
  800517:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  80051b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80051e:	89 54 24 04          	mov    %edx,0x4(%esp)
  800522:	89 04 24             	mov    %eax,(%esp)
  800525:	8b 45 08             	mov    0x8(%ebp),%eax
  800528:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80052a:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80052e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800532:	7f e3                	jg     800517 <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800534:	eb 38                	jmp    80056e <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
  800536:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80053a:	74 1f                	je     80055b <vprintfmt+0x22b>
  80053c:	83 fb 1f             	cmp    $0x1f,%ebx
  80053f:	7e 05                	jle    800546 <vprintfmt+0x216>
  800541:	83 fb 7e             	cmp    $0x7e,%ebx
  800544:	7e 15                	jle    80055b <vprintfmt+0x22b>
					putch('?', putdat);
  800546:	8b 45 0c             	mov    0xc(%ebp),%eax
  800549:	89 44 24 04          	mov    %eax,0x4(%esp)
  80054d:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  800554:	8b 45 08             	mov    0x8(%ebp),%eax
  800557:	ff d0                	call   *%eax
  800559:	eb 0f                	jmp    80056a <vprintfmt+0x23a>
				else
					putch(ch, putdat);
  80055b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80055e:	89 44 24 04          	mov    %eax,0x4(%esp)
  800562:	89 1c 24             	mov    %ebx,(%esp)
  800565:	8b 45 08             	mov    0x8(%ebp),%eax
  800568:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80056a:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80056e:	89 f0                	mov    %esi,%eax
  800570:	8d 70 01             	lea    0x1(%eax),%esi
  800573:	0f b6 00             	movzbl (%eax),%eax
  800576:	0f be d8             	movsbl %al,%ebx
  800579:	85 db                	test   %ebx,%ebx
  80057b:	74 10                	je     80058d <vprintfmt+0x25d>
  80057d:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800581:	78 b3                	js     800536 <vprintfmt+0x206>
  800583:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  800587:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  80058b:	79 a9                	jns    800536 <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80058d:	eb 17                	jmp    8005a6 <vprintfmt+0x276>
				putch(' ', putdat);
  80058f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800592:	89 44 24 04          	mov    %eax,0x4(%esp)
  800596:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  80059d:	8b 45 08             	mov    0x8(%ebp),%eax
  8005a0:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005a2:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005a6:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005aa:	7f e3                	jg     80058f <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
  8005ac:	e9 93 01 00 00       	jmp    800744 <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005b1:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8005b4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005b8:	8d 45 14             	lea    0x14(%ebp),%eax
  8005bb:	89 04 24             	mov    %eax,(%esp)
  8005be:	e8 0e fd ff ff       	call   8002d1 <getint>
  8005c3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005c6:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  8005c9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005cc:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005cf:	85 d2                	test   %edx,%edx
  8005d1:	79 26                	jns    8005f9 <vprintfmt+0x2c9>
				putch('-', putdat);
  8005d3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005d6:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005da:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  8005e1:	8b 45 08             	mov    0x8(%ebp),%eax
  8005e4:	ff d0                	call   *%eax
				num = -(long long) num;
  8005e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005e9:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005ec:	f7 d8                	neg    %eax
  8005ee:	83 d2 00             	adc    $0x0,%edx
  8005f1:	f7 da                	neg    %edx
  8005f3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005f6:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  8005f9:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800600:	e9 cb 00 00 00       	jmp    8006d0 <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800605:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800608:	89 44 24 04          	mov    %eax,0x4(%esp)
  80060c:	8d 45 14             	lea    0x14(%ebp),%eax
  80060f:	89 04 24             	mov    %eax,(%esp)
  800612:	e8 53 fc ff ff       	call   80026a <getuint>
  800617:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80061a:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  80061d:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800624:	e9 a7 00 00 00       	jmp    8006d0 <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  800629:	8b 45 0c             	mov    0xc(%ebp),%eax
  80062c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800630:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  800637:	8b 45 08             	mov    0x8(%ebp),%eax
  80063a:	ff d0                	call   *%eax
			putch('X', putdat);
  80063c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80063f:	89 44 24 04          	mov    %eax,0x4(%esp)
  800643:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  80064a:	8b 45 08             	mov    0x8(%ebp),%eax
  80064d:	ff d0                	call   *%eax
			putch('X', putdat);
  80064f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800652:	89 44 24 04          	mov    %eax,0x4(%esp)
  800656:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  80065d:	8b 45 08             	mov    0x8(%ebp),%eax
  800660:	ff d0                	call   *%eax
			break;
  800662:	e9 dd 00 00 00       	jmp    800744 <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
  800667:	8b 45 0c             	mov    0xc(%ebp),%eax
  80066a:	89 44 24 04          	mov    %eax,0x4(%esp)
  80066e:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  800675:	8b 45 08             	mov    0x8(%ebp),%eax
  800678:	ff d0                	call   *%eax
			putch('x', putdat);
  80067a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80067d:	89 44 24 04          	mov    %eax,0x4(%esp)
  800681:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  800688:	8b 45 08             	mov    0x8(%ebp),%eax
  80068b:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  80068d:	8b 45 14             	mov    0x14(%ebp),%eax
  800690:	83 c0 04             	add    $0x4,%eax
  800693:	89 45 14             	mov    %eax,0x14(%ebp)
  800696:	8b 45 14             	mov    0x14(%ebp),%eax
  800699:	83 e8 04             	sub    $0x4,%eax
  80069c:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80069e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006a1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  8006a8:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  8006af:	eb 1f                	jmp    8006d0 <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006b1:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8006b4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006b8:	8d 45 14             	lea    0x14(%ebp),%eax
  8006bb:	89 04 24             	mov    %eax,(%esp)
  8006be:	e8 a7 fb ff ff       	call   80026a <getuint>
  8006c3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006c6:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  8006c9:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006d0:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  8006d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006d7:	89 54 24 18          	mov    %edx,0x18(%esp)
  8006db:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  8006de:	89 54 24 14          	mov    %edx,0x14(%esp)
  8006e2:	89 44 24 10          	mov    %eax,0x10(%esp)
  8006e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8006e9:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8006ec:	89 44 24 08          	mov    %eax,0x8(%esp)
  8006f0:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8006f4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006f7:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006fb:	8b 45 08             	mov    0x8(%ebp),%eax
  8006fe:	89 04 24             	mov    %eax,(%esp)
  800701:	e8 86 fa ff ff       	call   80018c <printnum>
			break;
  800706:	eb 3c                	jmp    800744 <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800708:	8b 45 0c             	mov    0xc(%ebp),%eax
  80070b:	89 44 24 04          	mov    %eax,0x4(%esp)
  80070f:	89 1c 24             	mov    %ebx,(%esp)
  800712:	8b 45 08             	mov    0x8(%ebp),%eax
  800715:	ff d0                	call   *%eax
			break;
  800717:	eb 2b                	jmp    800744 <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800719:	8b 45 0c             	mov    0xc(%ebp),%eax
  80071c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800720:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  800727:	8b 45 08             	mov    0x8(%ebp),%eax
  80072a:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
  80072c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800730:	eb 04                	jmp    800736 <vprintfmt+0x406>
  800732:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800736:	8b 45 10             	mov    0x10(%ebp),%eax
  800739:	83 e8 01             	sub    $0x1,%eax
  80073c:	0f b6 00             	movzbl (%eax),%eax
  80073f:	3c 25                	cmp    $0x25,%al
  800741:	75 ef                	jne    800732 <vprintfmt+0x402>
				/* do nothing */;
			break;
  800743:	90                   	nop
		}
	}
  800744:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800745:	e9 08 fc ff ff       	jmp    800352 <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  80074a:	83 c4 40             	add    $0x40,%esp
  80074d:	5b                   	pop    %ebx
  80074e:	5e                   	pop    %esi
  80074f:	5d                   	pop    %ebp
  800750:	c3                   	ret    

00800751 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800751:	55                   	push   %ebp
  800752:	89 e5                	mov    %esp,%ebp
  800754:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  800757:	8d 45 10             	lea    0x10(%ebp),%eax
  80075a:	83 c0 04             	add    $0x4,%eax
  80075d:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  800760:	8b 45 10             	mov    0x10(%ebp),%eax
  800763:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800766:	89 54 24 0c          	mov    %edx,0xc(%esp)
  80076a:	89 44 24 08          	mov    %eax,0x8(%esp)
  80076e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800771:	89 44 24 04          	mov    %eax,0x4(%esp)
  800775:	8b 45 08             	mov    0x8(%ebp),%eax
  800778:	89 04 24             	mov    %eax,(%esp)
  80077b:	e8 b0 fb ff ff       	call   800330 <vprintfmt>
	va_end(ap);
}
  800780:	c9                   	leave  
  800781:	c3                   	ret    

00800782 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800782:	55                   	push   %ebp
  800783:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800785:	8b 45 0c             	mov    0xc(%ebp),%eax
  800788:	8b 40 08             	mov    0x8(%eax),%eax
  80078b:	8d 50 01             	lea    0x1(%eax),%edx
  80078e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800791:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800794:	8b 45 0c             	mov    0xc(%ebp),%eax
  800797:	8b 10                	mov    (%eax),%edx
  800799:	8b 45 0c             	mov    0xc(%ebp),%eax
  80079c:	8b 40 04             	mov    0x4(%eax),%eax
  80079f:	39 c2                	cmp    %eax,%edx
  8007a1:	73 12                	jae    8007b5 <sprintputch+0x33>
		*b->buf++ = ch;
  8007a3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007a6:	8b 00                	mov    (%eax),%eax
  8007a8:	8d 48 01             	lea    0x1(%eax),%ecx
  8007ab:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007ae:	89 0a                	mov    %ecx,(%edx)
  8007b0:	8b 55 08             	mov    0x8(%ebp),%edx
  8007b3:	88 10                	mov    %dl,(%eax)
}
  8007b5:	5d                   	pop    %ebp
  8007b6:	c3                   	ret    

008007b7 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007b7:	55                   	push   %ebp
  8007b8:	89 e5                	mov    %esp,%ebp
  8007ba:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007bd:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007c3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007c6:	8d 50 ff             	lea    -0x1(%eax),%edx
  8007c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8007cc:	01 d0                	add    %edx,%eax
  8007ce:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007d1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007d8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8007dc:	74 06                	je     8007e4 <vsnprintf+0x2d>
  8007de:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8007e2:	7f 07                	jg     8007eb <vsnprintf+0x34>
		return -E_INVAL;
  8007e4:	b8 03 00 00 00       	mov    $0x3,%eax
  8007e9:	eb 2a                	jmp    800815 <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007eb:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ee:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8007f2:	8b 45 10             	mov    0x10(%ebp),%eax
  8007f5:	89 44 24 08          	mov    %eax,0x8(%esp)
  8007f9:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007fc:	89 44 24 04          	mov    %eax,0x4(%esp)
  800800:	c7 04 24 82 07 80 00 	movl   $0x800782,(%esp)
  800807:	e8 24 fb ff ff       	call   800330 <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  80080c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80080f:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800812:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  800815:	c9                   	leave  
  800816:	c3                   	ret    

00800817 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800817:	55                   	push   %ebp
  800818:	89 e5                	mov    %esp,%ebp
  80081a:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80081d:	8d 45 10             	lea    0x10(%ebp),%eax
  800820:	83 c0 04             	add    $0x4,%eax
  800823:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  800826:	8b 45 10             	mov    0x10(%ebp),%eax
  800829:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80082c:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800830:	89 44 24 08          	mov    %eax,0x8(%esp)
  800834:	8b 45 0c             	mov    0xc(%ebp),%eax
  800837:	89 44 24 04          	mov    %eax,0x4(%esp)
  80083b:	8b 45 08             	mov    0x8(%ebp),%eax
  80083e:	89 04 24             	mov    %eax,(%esp)
  800841:	e8 71 ff ff ff       	call   8007b7 <vsnprintf>
  800846:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  800849:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80084c:	c9                   	leave  
  80084d:	c3                   	ret    

0080084e <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  80084e:	55                   	push   %ebp
  80084f:	89 e5                	mov    %esp,%ebp
  800851:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  800854:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80085b:	eb 08                	jmp    800865 <strlen+0x17>
		n++;
  80085d:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800861:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800865:	8b 45 08             	mov    0x8(%ebp),%eax
  800868:	0f b6 00             	movzbl (%eax),%eax
  80086b:	84 c0                	test   %al,%al
  80086d:	75 ee                	jne    80085d <strlen+0xf>
		n++;
	return n;
  80086f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800872:	c9                   	leave  
  800873:	c3                   	ret    

00800874 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  800874:	55                   	push   %ebp
  800875:	89 e5                	mov    %esp,%ebp
  800877:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80087a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800881:	eb 0c                	jmp    80088f <strnlen+0x1b>
		n++;
  800883:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800887:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80088b:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  80088f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800893:	74 0a                	je     80089f <strnlen+0x2b>
  800895:	8b 45 08             	mov    0x8(%ebp),%eax
  800898:	0f b6 00             	movzbl (%eax),%eax
  80089b:	84 c0                	test   %al,%al
  80089d:	75 e4                	jne    800883 <strnlen+0xf>
		n++;
	return n;
  80089f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008a2:	c9                   	leave  
  8008a3:	c3                   	ret    

008008a4 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8008a4:	55                   	push   %ebp
  8008a5:	89 e5                	mov    %esp,%ebp
  8008a7:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  8008aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ad:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  8008b0:	90                   	nop
  8008b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b4:	8d 50 01             	lea    0x1(%eax),%edx
  8008b7:	89 55 08             	mov    %edx,0x8(%ebp)
  8008ba:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008bd:	8d 4a 01             	lea    0x1(%edx),%ecx
  8008c0:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8008c3:	0f b6 12             	movzbl (%edx),%edx
  8008c6:	88 10                	mov    %dl,(%eax)
  8008c8:	0f b6 00             	movzbl (%eax),%eax
  8008cb:	84 c0                	test   %al,%al
  8008cd:	75 e2                	jne    8008b1 <strcpy+0xd>
		/* do nothing */;
	return ret;
  8008cf:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008d2:	c9                   	leave  
  8008d3:	c3                   	ret    

008008d4 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  8008d4:	55                   	push   %ebp
  8008d5:	89 e5                	mov    %esp,%ebp
  8008d7:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  8008da:	8b 45 08             	mov    0x8(%ebp),%eax
  8008dd:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  8008e0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008e7:	eb 23                	jmp    80090c <strncpy+0x38>
		*dst++ = *src;
  8008e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ec:	8d 50 01             	lea    0x1(%eax),%edx
  8008ef:	89 55 08             	mov    %edx,0x8(%ebp)
  8008f2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008f5:	0f b6 12             	movzbl (%edx),%edx
  8008f8:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  8008fa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008fd:	0f b6 00             	movzbl (%eax),%eax
  800900:	84 c0                	test   %al,%al
  800902:	74 04                	je     800908 <strncpy+0x34>
			src++;
  800904:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800908:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  80090c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80090f:	3b 45 10             	cmp    0x10(%ebp),%eax
  800912:	72 d5                	jb     8008e9 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800914:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800917:	c9                   	leave  
  800918:	c3                   	ret    

00800919 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800919:	55                   	push   %ebp
  80091a:	89 e5                	mov    %esp,%ebp
  80091c:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  80091f:	8b 45 08             	mov    0x8(%ebp),%eax
  800922:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800925:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800929:	74 33                	je     80095e <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  80092b:	eb 17                	jmp    800944 <strlcpy+0x2b>
			*dst++ = *src++;
  80092d:	8b 45 08             	mov    0x8(%ebp),%eax
  800930:	8d 50 01             	lea    0x1(%eax),%edx
  800933:	89 55 08             	mov    %edx,0x8(%ebp)
  800936:	8b 55 0c             	mov    0xc(%ebp),%edx
  800939:	8d 4a 01             	lea    0x1(%edx),%ecx
  80093c:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  80093f:	0f b6 12             	movzbl (%edx),%edx
  800942:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800944:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800948:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80094c:	74 0a                	je     800958 <strlcpy+0x3f>
  80094e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800951:	0f b6 00             	movzbl (%eax),%eax
  800954:	84 c0                	test   %al,%al
  800956:	75 d5                	jne    80092d <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800958:	8b 45 08             	mov    0x8(%ebp),%eax
  80095b:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  80095e:	8b 55 08             	mov    0x8(%ebp),%edx
  800961:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800964:	29 c2                	sub    %eax,%edx
  800966:	89 d0                	mov    %edx,%eax
}
  800968:	c9                   	leave  
  800969:	c3                   	ret    

0080096a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  80096a:	55                   	push   %ebp
  80096b:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  80096d:	eb 08                	jmp    800977 <strcmp+0xd>
		p++, q++;
  80096f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800973:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800977:	8b 45 08             	mov    0x8(%ebp),%eax
  80097a:	0f b6 00             	movzbl (%eax),%eax
  80097d:	84 c0                	test   %al,%al
  80097f:	74 10                	je     800991 <strcmp+0x27>
  800981:	8b 45 08             	mov    0x8(%ebp),%eax
  800984:	0f b6 10             	movzbl (%eax),%edx
  800987:	8b 45 0c             	mov    0xc(%ebp),%eax
  80098a:	0f b6 00             	movzbl (%eax),%eax
  80098d:	38 c2                	cmp    %al,%dl
  80098f:	74 de                	je     80096f <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800991:	8b 45 08             	mov    0x8(%ebp),%eax
  800994:	0f b6 00             	movzbl (%eax),%eax
  800997:	0f b6 d0             	movzbl %al,%edx
  80099a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80099d:	0f b6 00             	movzbl (%eax),%eax
  8009a0:	0f b6 c0             	movzbl %al,%eax
  8009a3:	29 c2                	sub    %eax,%edx
  8009a5:	89 d0                	mov    %edx,%eax
}
  8009a7:	5d                   	pop    %ebp
  8009a8:	c3                   	ret    

008009a9 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  8009a9:	55                   	push   %ebp
  8009aa:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  8009ac:	eb 0c                	jmp    8009ba <strncmp+0x11>
		n--, p++, q++;
  8009ae:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8009b2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009b6:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  8009ba:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009be:	74 1a                	je     8009da <strncmp+0x31>
  8009c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c3:	0f b6 00             	movzbl (%eax),%eax
  8009c6:	84 c0                	test   %al,%al
  8009c8:	74 10                	je     8009da <strncmp+0x31>
  8009ca:	8b 45 08             	mov    0x8(%ebp),%eax
  8009cd:	0f b6 10             	movzbl (%eax),%edx
  8009d0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009d3:	0f b6 00             	movzbl (%eax),%eax
  8009d6:	38 c2                	cmp    %al,%dl
  8009d8:	74 d4                	je     8009ae <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  8009da:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009de:	75 07                	jne    8009e7 <strncmp+0x3e>
		return 0;
  8009e0:	b8 00 00 00 00       	mov    $0x0,%eax
  8009e5:	eb 16                	jmp    8009fd <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8009e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ea:	0f b6 00             	movzbl (%eax),%eax
  8009ed:	0f b6 d0             	movzbl %al,%edx
  8009f0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009f3:	0f b6 00             	movzbl (%eax),%eax
  8009f6:	0f b6 c0             	movzbl %al,%eax
  8009f9:	29 c2                	sub    %eax,%edx
  8009fb:	89 d0                	mov    %edx,%eax
}
  8009fd:	5d                   	pop    %ebp
  8009fe:	c3                   	ret    

008009ff <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8009ff:	55                   	push   %ebp
  800a00:	89 e5                	mov    %esp,%ebp
  800a02:	83 ec 04             	sub    $0x4,%esp
  800a05:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a08:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a0b:	eb 14                	jmp    800a21 <strchr+0x22>
		if (*s == c)
  800a0d:	8b 45 08             	mov    0x8(%ebp),%eax
  800a10:	0f b6 00             	movzbl (%eax),%eax
  800a13:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a16:	75 05                	jne    800a1d <strchr+0x1e>
			return (char *) s;
  800a18:	8b 45 08             	mov    0x8(%ebp),%eax
  800a1b:	eb 13                	jmp    800a30 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800a1d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a21:	8b 45 08             	mov    0x8(%ebp),%eax
  800a24:	0f b6 00             	movzbl (%eax),%eax
  800a27:	84 c0                	test   %al,%al
  800a29:	75 e2                	jne    800a0d <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800a2b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a30:	c9                   	leave  
  800a31:	c3                   	ret    

00800a32 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a32:	55                   	push   %ebp
  800a33:	89 e5                	mov    %esp,%ebp
  800a35:	83 ec 04             	sub    $0x4,%esp
  800a38:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a3b:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a3e:	eb 11                	jmp    800a51 <strfind+0x1f>
		if (*s == c)
  800a40:	8b 45 08             	mov    0x8(%ebp),%eax
  800a43:	0f b6 00             	movzbl (%eax),%eax
  800a46:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a49:	75 02                	jne    800a4d <strfind+0x1b>
			break;
  800a4b:	eb 0e                	jmp    800a5b <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a4d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a51:	8b 45 08             	mov    0x8(%ebp),%eax
  800a54:	0f b6 00             	movzbl (%eax),%eax
  800a57:	84 c0                	test   %al,%al
  800a59:	75 e5                	jne    800a40 <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
  800a5b:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a5e:	c9                   	leave  
  800a5f:	c3                   	ret    

00800a60 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800a60:	55                   	push   %ebp
  800a61:	89 e5                	mov    %esp,%ebp
  800a63:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800a66:	8b 45 08             	mov    0x8(%ebp),%eax
  800a69:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800a6c:	8b 45 10             	mov    0x10(%ebp),%eax
  800a6f:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800a72:	eb 0e                	jmp    800a82 <memset+0x22>
		*p++ = c;
  800a74:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a77:	8d 50 01             	lea    0x1(%eax),%edx
  800a7a:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800a7d:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a80:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800a82:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800a86:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800a8a:	79 e8                	jns    800a74 <memset+0x14>
		*p++ = c;

	return v;
  800a8c:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a8f:	c9                   	leave  
  800a90:	c3                   	ret    

00800a91 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800a91:	55                   	push   %ebp
  800a92:	89 e5                	mov    %esp,%ebp
  800a94:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800a97:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a9a:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a9d:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa0:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800aa3:	eb 17                	jmp    800abc <memcpy+0x2b>
		*d++ = *s++;
  800aa5:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800aa8:	8d 50 01             	lea    0x1(%eax),%edx
  800aab:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800aae:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800ab1:	8d 4a 01             	lea    0x1(%edx),%ecx
  800ab4:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800ab7:	0f b6 12             	movzbl (%edx),%edx
  800aba:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800abc:	8b 45 10             	mov    0x10(%ebp),%eax
  800abf:	8d 50 ff             	lea    -0x1(%eax),%edx
  800ac2:	89 55 10             	mov    %edx,0x10(%ebp)
  800ac5:	85 c0                	test   %eax,%eax
  800ac7:	75 dc                	jne    800aa5 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800ac9:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800acc:	c9                   	leave  
  800acd:	c3                   	ret    

00800ace <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800ace:	55                   	push   %ebp
  800acf:	89 e5                	mov    %esp,%ebp
  800ad1:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800ad4:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ad7:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800ada:	8b 45 08             	mov    0x8(%ebp),%eax
  800add:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800ae0:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ae3:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800ae6:	73 3d                	jae    800b25 <memmove+0x57>
  800ae8:	8b 45 10             	mov    0x10(%ebp),%eax
  800aeb:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800aee:	01 d0                	add    %edx,%eax
  800af0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800af3:	76 30                	jbe    800b25 <memmove+0x57>
		s += n;
  800af5:	8b 45 10             	mov    0x10(%ebp),%eax
  800af8:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800afb:	8b 45 10             	mov    0x10(%ebp),%eax
  800afe:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800b01:	eb 13                	jmp    800b16 <memmove+0x48>
			*--d = *--s;
  800b03:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800b07:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800b0b:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b0e:	0f b6 10             	movzbl (%eax),%edx
  800b11:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b14:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800b16:	8b 45 10             	mov    0x10(%ebp),%eax
  800b19:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b1c:	89 55 10             	mov    %edx,0x10(%ebp)
  800b1f:	85 c0                	test   %eax,%eax
  800b21:	75 e0                	jne    800b03 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800b23:	eb 26                	jmp    800b4b <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b25:	eb 17                	jmp    800b3e <memmove+0x70>
			*d++ = *s++;
  800b27:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b2a:	8d 50 01             	lea    0x1(%eax),%edx
  800b2d:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b30:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b33:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b36:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b39:	0f b6 12             	movzbl (%edx),%edx
  800b3c:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b3e:	8b 45 10             	mov    0x10(%ebp),%eax
  800b41:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b44:	89 55 10             	mov    %edx,0x10(%ebp)
  800b47:	85 c0                	test   %eax,%eax
  800b49:	75 dc                	jne    800b27 <memmove+0x59>
			*d++ = *s++;

	return dst;
  800b4b:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b4e:	c9                   	leave  
  800b4f:	c3                   	ret    

00800b50 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800b50:	55                   	push   %ebp
  800b51:	89 e5                	mov    %esp,%ebp
  800b53:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800b56:	8b 45 08             	mov    0x8(%ebp),%eax
  800b59:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800b5c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b5f:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800b62:	eb 30                	jmp    800b94 <memcmp+0x44>
		if (*s1 != *s2)
  800b64:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b67:	0f b6 10             	movzbl (%eax),%edx
  800b6a:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b6d:	0f b6 00             	movzbl (%eax),%eax
  800b70:	38 c2                	cmp    %al,%dl
  800b72:	74 18                	je     800b8c <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800b74:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b77:	0f b6 00             	movzbl (%eax),%eax
  800b7a:	0f b6 d0             	movzbl %al,%edx
  800b7d:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b80:	0f b6 00             	movzbl (%eax),%eax
  800b83:	0f b6 c0             	movzbl %al,%eax
  800b86:	29 c2                	sub    %eax,%edx
  800b88:	89 d0                	mov    %edx,%eax
  800b8a:	eb 1a                	jmp    800ba6 <memcmp+0x56>
		s1++, s2++;
  800b8c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800b90:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800b94:	8b 45 10             	mov    0x10(%ebp),%eax
  800b97:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b9a:	89 55 10             	mov    %edx,0x10(%ebp)
  800b9d:	85 c0                	test   %eax,%eax
  800b9f:	75 c3                	jne    800b64 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800ba1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800ba6:	c9                   	leave  
  800ba7:	c3                   	ret    

00800ba8 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800ba8:	55                   	push   %ebp
  800ba9:	89 e5                	mov    %esp,%ebp
  800bab:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800bae:	8b 45 10             	mov    0x10(%ebp),%eax
  800bb1:	8b 55 08             	mov    0x8(%ebp),%edx
  800bb4:	01 d0                	add    %edx,%eax
  800bb6:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800bb9:	eb 13                	jmp    800bce <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
  800bbb:	8b 45 08             	mov    0x8(%ebp),%eax
  800bbe:	0f b6 10             	movzbl (%eax),%edx
  800bc1:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bc4:	38 c2                	cmp    %al,%dl
  800bc6:	75 02                	jne    800bca <memfind+0x22>
			break;
  800bc8:	eb 0c                	jmp    800bd6 <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800bca:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bce:	8b 45 08             	mov    0x8(%ebp),%eax
  800bd1:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800bd4:	72 e5                	jb     800bbb <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
  800bd6:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bd9:	c9                   	leave  
  800bda:	c3                   	ret    

00800bdb <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bdb:	55                   	push   %ebp
  800bdc:	89 e5                	mov    %esp,%ebp
  800bde:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800be1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800be8:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bef:	eb 04                	jmp    800bf5 <strtol+0x1a>
		s++;
  800bf1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bf5:	8b 45 08             	mov    0x8(%ebp),%eax
  800bf8:	0f b6 00             	movzbl (%eax),%eax
  800bfb:	3c 20                	cmp    $0x20,%al
  800bfd:	74 f2                	je     800bf1 <strtol+0x16>
  800bff:	8b 45 08             	mov    0x8(%ebp),%eax
  800c02:	0f b6 00             	movzbl (%eax),%eax
  800c05:	3c 09                	cmp    $0x9,%al
  800c07:	74 e8                	je     800bf1 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800c09:	8b 45 08             	mov    0x8(%ebp),%eax
  800c0c:	0f b6 00             	movzbl (%eax),%eax
  800c0f:	3c 2b                	cmp    $0x2b,%al
  800c11:	75 06                	jne    800c19 <strtol+0x3e>
		s++;
  800c13:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c17:	eb 15                	jmp    800c2e <strtol+0x53>
	else if (*s == '-')
  800c19:	8b 45 08             	mov    0x8(%ebp),%eax
  800c1c:	0f b6 00             	movzbl (%eax),%eax
  800c1f:	3c 2d                	cmp    $0x2d,%al
  800c21:	75 0b                	jne    800c2e <strtol+0x53>
		s++, neg = 1;
  800c23:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c27:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c2e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c32:	74 06                	je     800c3a <strtol+0x5f>
  800c34:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c38:	75 24                	jne    800c5e <strtol+0x83>
  800c3a:	8b 45 08             	mov    0x8(%ebp),%eax
  800c3d:	0f b6 00             	movzbl (%eax),%eax
  800c40:	3c 30                	cmp    $0x30,%al
  800c42:	75 1a                	jne    800c5e <strtol+0x83>
  800c44:	8b 45 08             	mov    0x8(%ebp),%eax
  800c47:	83 c0 01             	add    $0x1,%eax
  800c4a:	0f b6 00             	movzbl (%eax),%eax
  800c4d:	3c 78                	cmp    $0x78,%al
  800c4f:	75 0d                	jne    800c5e <strtol+0x83>
		s += 2, base = 16;
  800c51:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800c55:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c5c:	eb 2a                	jmp    800c88 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800c5e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c62:	75 17                	jne    800c7b <strtol+0xa0>
  800c64:	8b 45 08             	mov    0x8(%ebp),%eax
  800c67:	0f b6 00             	movzbl (%eax),%eax
  800c6a:	3c 30                	cmp    $0x30,%al
  800c6c:	75 0d                	jne    800c7b <strtol+0xa0>
		s++, base = 8;
  800c6e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c72:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c79:	eb 0d                	jmp    800c88 <strtol+0xad>
	else if (base == 0)
  800c7b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c7f:	75 07                	jne    800c88 <strtol+0xad>
		base = 10;
  800c81:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c88:	8b 45 08             	mov    0x8(%ebp),%eax
  800c8b:	0f b6 00             	movzbl (%eax),%eax
  800c8e:	3c 2f                	cmp    $0x2f,%al
  800c90:	7e 1b                	jle    800cad <strtol+0xd2>
  800c92:	8b 45 08             	mov    0x8(%ebp),%eax
  800c95:	0f b6 00             	movzbl (%eax),%eax
  800c98:	3c 39                	cmp    $0x39,%al
  800c9a:	7f 11                	jg     800cad <strtol+0xd2>
			dig = *s - '0';
  800c9c:	8b 45 08             	mov    0x8(%ebp),%eax
  800c9f:	0f b6 00             	movzbl (%eax),%eax
  800ca2:	0f be c0             	movsbl %al,%eax
  800ca5:	83 e8 30             	sub    $0x30,%eax
  800ca8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800cab:	eb 48                	jmp    800cf5 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800cad:	8b 45 08             	mov    0x8(%ebp),%eax
  800cb0:	0f b6 00             	movzbl (%eax),%eax
  800cb3:	3c 60                	cmp    $0x60,%al
  800cb5:	7e 1b                	jle    800cd2 <strtol+0xf7>
  800cb7:	8b 45 08             	mov    0x8(%ebp),%eax
  800cba:	0f b6 00             	movzbl (%eax),%eax
  800cbd:	3c 7a                	cmp    $0x7a,%al
  800cbf:	7f 11                	jg     800cd2 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800cc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800cc4:	0f b6 00             	movzbl (%eax),%eax
  800cc7:	0f be c0             	movsbl %al,%eax
  800cca:	83 e8 57             	sub    $0x57,%eax
  800ccd:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800cd0:	eb 23                	jmp    800cf5 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800cd2:	8b 45 08             	mov    0x8(%ebp),%eax
  800cd5:	0f b6 00             	movzbl (%eax),%eax
  800cd8:	3c 40                	cmp    $0x40,%al
  800cda:	7e 3d                	jle    800d19 <strtol+0x13e>
  800cdc:	8b 45 08             	mov    0x8(%ebp),%eax
  800cdf:	0f b6 00             	movzbl (%eax),%eax
  800ce2:	3c 5a                	cmp    $0x5a,%al
  800ce4:	7f 33                	jg     800d19 <strtol+0x13e>
			dig = *s - 'A' + 10;
  800ce6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ce9:	0f b6 00             	movzbl (%eax),%eax
  800cec:	0f be c0             	movsbl %al,%eax
  800cef:	83 e8 37             	sub    $0x37,%eax
  800cf2:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800cf5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800cf8:	3b 45 10             	cmp    0x10(%ebp),%eax
  800cfb:	7c 02                	jl     800cff <strtol+0x124>
			break;
  800cfd:	eb 1a                	jmp    800d19 <strtol+0x13e>
		s++, val = (val * base) + dig;
  800cff:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d03:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d06:	0f af 45 10          	imul   0x10(%ebp),%eax
  800d0a:	89 c2                	mov    %eax,%edx
  800d0c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800d0f:	01 d0                	add    %edx,%eax
  800d11:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800d14:	e9 6f ff ff ff       	jmp    800c88 <strtol+0xad>

	if (endptr)
  800d19:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800d1d:	74 08                	je     800d27 <strtol+0x14c>
		*endptr = (char *) s;
  800d1f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d22:	8b 55 08             	mov    0x8(%ebp),%edx
  800d25:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800d27:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800d2b:	74 07                	je     800d34 <strtol+0x159>
  800d2d:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d30:	f7 d8                	neg    %eax
  800d32:	eb 03                	jmp    800d37 <strtol+0x15c>
  800d34:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800d37:	c9                   	leave  
  800d38:	c3                   	ret    

00800d39 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800d39:	55                   	push   %ebp
  800d3a:	89 e5                	mov    %esp,%ebp
  800d3c:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800d3f:	8b 45 14             	mov    0x14(%ebp),%eax
  800d42:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800d48:	8b 45 14             	mov    0x14(%ebp),%eax
  800d4b:	8b 00                	mov    (%eax),%eax
  800d4d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d54:	8b 45 10             	mov    0x10(%ebp),%eax
  800d57:	01 d0                	add    %edx,%eax
  800d59:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d5f:	eb 0c                	jmp    800d6d <strsplit+0x34>
			*string++ = 0;
  800d61:	8b 45 08             	mov    0x8(%ebp),%eax
  800d64:	8d 50 01             	lea    0x1(%eax),%edx
  800d67:	89 55 08             	mov    %edx,0x8(%ebp)
  800d6a:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d6d:	8b 45 08             	mov    0x8(%ebp),%eax
  800d70:	0f b6 00             	movzbl (%eax),%eax
  800d73:	84 c0                	test   %al,%al
  800d75:	74 1c                	je     800d93 <strsplit+0x5a>
  800d77:	8b 45 08             	mov    0x8(%ebp),%eax
  800d7a:	0f b6 00             	movzbl (%eax),%eax
  800d7d:	0f be c0             	movsbl %al,%eax
  800d80:	89 44 24 04          	mov    %eax,0x4(%esp)
  800d84:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d87:	89 04 24             	mov    %eax,(%esp)
  800d8a:	e8 70 fc ff ff       	call   8009ff <strchr>
  800d8f:	85 c0                	test   %eax,%eax
  800d91:	75 ce                	jne    800d61 <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800d93:	8b 45 08             	mov    0x8(%ebp),%eax
  800d96:	0f b6 00             	movzbl (%eax),%eax
  800d99:	84 c0                	test   %al,%al
  800d9b:	75 1f                	jne    800dbc <strsplit+0x83>
			break;
  800d9d:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800d9e:	8b 45 14             	mov    0x14(%ebp),%eax
  800da1:	8b 00                	mov    (%eax),%eax
  800da3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800daa:	8b 45 10             	mov    0x10(%ebp),%eax
  800dad:	01 d0                	add    %edx,%eax
  800daf:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800db5:	b8 01 00 00 00       	mov    $0x1,%eax
  800dba:	eb 61                	jmp    800e1d <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800dbc:	8b 45 14             	mov    0x14(%ebp),%eax
  800dbf:	8b 00                	mov    (%eax),%eax
  800dc1:	83 f8 0f             	cmp    $0xf,%eax
  800dc4:	75 07                	jne    800dcd <strsplit+0x94>
		{
			return 0;
  800dc6:	b8 00 00 00 00       	mov    $0x0,%eax
  800dcb:	eb 50                	jmp    800e1d <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800dcd:	8b 45 14             	mov    0x14(%ebp),%eax
  800dd0:	8b 00                	mov    (%eax),%eax
  800dd2:	8d 48 01             	lea    0x1(%eax),%ecx
  800dd5:	8b 55 14             	mov    0x14(%ebp),%edx
  800dd8:	89 0a                	mov    %ecx,(%edx)
  800dda:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800de1:	8b 45 10             	mov    0x10(%ebp),%eax
  800de4:	01 c2                	add    %eax,%edx
  800de6:	8b 45 08             	mov    0x8(%ebp),%eax
  800de9:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800deb:	eb 04                	jmp    800df1 <strsplit+0xb8>
			string++;
  800ded:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800df1:	8b 45 08             	mov    0x8(%ebp),%eax
  800df4:	0f b6 00             	movzbl (%eax),%eax
  800df7:	84 c0                	test   %al,%al
  800df9:	74 1c                	je     800e17 <strsplit+0xde>
  800dfb:	8b 45 08             	mov    0x8(%ebp),%eax
  800dfe:	0f b6 00             	movzbl (%eax),%eax
  800e01:	0f be c0             	movsbl %al,%eax
  800e04:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e08:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e0b:	89 04 24             	mov    %eax,(%esp)
  800e0e:	e8 ec fb ff ff       	call   8009ff <strchr>
  800e13:	85 c0                	test   %eax,%eax
  800e15:	74 d6                	je     800ded <strsplit+0xb4>
			string++;
	}
  800e17:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e18:	e9 50 ff ff ff       	jmp    800d6d <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
  800e1d:	c9                   	leave  
  800e1e:	c3                   	ret    

00800e1f <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800e1f:	55                   	push   %ebp
  800e20:	89 e5                	mov    %esp,%ebp
  800e22:	57                   	push   %edi
  800e23:	56                   	push   %esi
  800e24:	53                   	push   %ebx
  800e25:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800e28:	8b 45 08             	mov    0x8(%ebp),%eax
  800e2b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e2e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800e31:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800e34:	8b 7d 18             	mov    0x18(%ebp),%edi
  800e37:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800e3a:	cd 30                	int    $0x30
  800e3c:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800e3f:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800e42:	83 c4 10             	add    $0x10,%esp
  800e45:	5b                   	pop    %ebx
  800e46:	5e                   	pop    %esi
  800e47:	5f                   	pop    %edi
  800e48:	5d                   	pop    %ebp
  800e49:	c3                   	ret    

00800e4a <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800e4a:	55                   	push   %ebp
  800e4b:	89 e5                	mov    %esp,%ebp
  800e4d:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800e50:	8b 45 08             	mov    0x8(%ebp),%eax
  800e53:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800e5a:	00 
  800e5b:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800e62:	00 
  800e63:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800e6a:	00 
  800e6b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e6e:	89 54 24 08          	mov    %edx,0x8(%esp)
  800e72:	89 44 24 04          	mov    %eax,0x4(%esp)
  800e76:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800e7d:	e8 9d ff ff ff       	call   800e1f <syscall>
}
  800e82:	c9                   	leave  
  800e83:	c3                   	ret    

00800e84 <sys_cgetc>:

int
sys_cgetc(void)
{
  800e84:	55                   	push   %ebp
  800e85:	89 e5                	mov    %esp,%ebp
  800e87:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800e8a:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800e91:	00 
  800e92:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800e99:	00 
  800e9a:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800ea1:	00 
  800ea2:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800ea9:	00 
  800eaa:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800eb1:	00 
  800eb2:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  800eb9:	e8 61 ff ff ff       	call   800e1f <syscall>
}
  800ebe:	c9                   	leave  
  800ebf:	c3                   	ret    

00800ec0 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800ec0:	55                   	push   %ebp
  800ec1:	89 e5                	mov    %esp,%ebp
  800ec3:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800ec6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ec9:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800ed0:	00 
  800ed1:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800ed8:	00 
  800ed9:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800ee0:	00 
  800ee1:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800ee8:	00 
  800ee9:	89 44 24 04          	mov    %eax,0x4(%esp)
  800eed:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  800ef4:	e8 26 ff ff ff       	call   800e1f <syscall>
}
  800ef9:	c9                   	leave  
  800efa:	c3                   	ret    

00800efb <sys_getenvid>:

int32 sys_getenvid(void)
{
  800efb:	55                   	push   %ebp
  800efc:	89 e5                	mov    %esp,%ebp
  800efe:	83 ec 18             	sub    $0x18,%esp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800f01:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f08:	00 
  800f09:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f10:	00 
  800f11:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f18:	00 
  800f19:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f20:	00 
  800f21:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f28:	00 
  800f29:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  800f30:	e8 ea fe ff ff       	call   800e1f <syscall>
}
  800f35:	c9                   	leave  
  800f36:	c3                   	ret    

00800f37 <sys_env_sleep>:

void sys_env_sleep(void)
{
  800f37:	55                   	push   %ebp
  800f38:	89 e5                	mov    %esp,%ebp
  800f3a:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800f3d:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f44:	00 
  800f45:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f4c:	00 
  800f4d:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f54:	00 
  800f55:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  800f5c:	00 
  800f5d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800f64:	00 
  800f65:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  800f6c:	e8 ae fe ff ff       	call   800e1f <syscall>
}
  800f71:	c9                   	leave  
  800f72:	c3                   	ret    

00800f73 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800f73:	55                   	push   %ebp
  800f74:	89 e5                	mov    %esp,%ebp
  800f76:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800f79:	8b 55 0c             	mov    0xc(%ebp),%edx
  800f7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800f7f:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800f86:	00 
  800f87:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800f8e:	00 
  800f8f:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800f96:	00 
  800f97:	89 54 24 08          	mov    %edx,0x8(%esp)
  800f9b:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f9f:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  800fa6:	e8 74 fe ff ff       	call   800e1f <syscall>
}
  800fab:	c9                   	leave  
  800fac:	c3                   	ret    

00800fad <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  800fad:	55                   	push   %ebp
  800fae:	89 e5                	mov    %esp,%ebp
  800fb0:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  800fb3:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fb6:	8b 45 08             	mov    0x8(%ebp),%eax
  800fb9:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800fc0:	00 
  800fc1:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800fc8:	00 
  800fc9:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800fd0:	00 
  800fd1:	89 54 24 08          	mov    %edx,0x8(%esp)
  800fd5:	89 44 24 04          	mov    %eax,0x4(%esp)
  800fd9:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
  800fe0:	e8 3a fe ff ff       	call   800e1f <syscall>
}
  800fe5:	c9                   	leave  
  800fe6:	c3                   	ret    

00800fe7 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  800fe7:	55                   	push   %ebp
  800fe8:	89 e5                	mov    %esp,%ebp
  800fea:	56                   	push   %esi
  800feb:	53                   	push   %ebx
  800fec:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  800fef:	8b 75 18             	mov    0x18(%ebp),%esi
  800ff2:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800ff5:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800ff8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ffb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ffe:	89 74 24 14          	mov    %esi,0x14(%esp)
  801002:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  801006:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80100a:	89 54 24 08          	mov    %edx,0x8(%esp)
  80100e:	89 44 24 04          	mov    %eax,0x4(%esp)
  801012:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  801019:	e8 01 fe ff ff       	call   800e1f <syscall>
}
  80101e:	83 c4 18             	add    $0x18,%esp
  801021:	5b                   	pop    %ebx
  801022:	5e                   	pop    %esi
  801023:	5d                   	pop    %ebp
  801024:	c3                   	ret    

00801025 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  801025:	55                   	push   %ebp
  801026:	89 e5                	mov    %esp,%ebp
  801028:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  80102b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80102e:	8b 45 08             	mov    0x8(%ebp),%eax
  801031:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801038:	00 
  801039:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801040:	00 
  801041:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801048:	00 
  801049:	89 54 24 08          	mov    %edx,0x8(%esp)
  80104d:	89 44 24 04          	mov    %eax,0x4(%esp)
  801051:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  801058:	e8 c2 fd ff ff       	call   800e1f <syscall>
}
  80105d:	c9                   	leave  
  80105e:	c3                   	ret    

0080105f <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  80105f:	55                   	push   %ebp
  801060:	89 e5                	mov    %esp,%ebp
  801062:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  801065:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80106c:	00 
  80106d:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801074:	00 
  801075:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80107c:	00 
  80107d:	8b 45 0c             	mov    0xc(%ebp),%eax
  801080:	89 44 24 08          	mov    %eax,0x8(%esp)
  801084:	8b 45 08             	mov    0x8(%ebp),%eax
  801087:	89 44 24 04          	mov    %eax,0x4(%esp)
  80108b:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
  801092:	e8 88 fd ff ff       	call   800e1f <syscall>
}
  801097:	c9                   	leave  
  801098:	c3                   	ret    

00801099 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  801099:	55                   	push   %ebp
  80109a:	89 e5                	mov    %esp,%ebp
  80109c:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  80109f:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010a6:	00 
  8010a7:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010ae:	00 
  8010af:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010b6:	00 
  8010b7:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  8010be:	00 
  8010bf:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  8010c6:	00 
  8010c7:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  8010ce:	e8 4c fd ff ff       	call   800e1f <syscall>
}
  8010d3:	c9                   	leave  
  8010d4:	c3                   	ret    

008010d5 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  8010d5:	55                   	push   %ebp
  8010d6:	89 e5                	mov    %esp,%ebp
  8010d8:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  8010db:	8b 45 08             	mov    0x8(%ebp),%eax
  8010de:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010e5:	00 
  8010e6:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010ed:	00 
  8010ee:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010f5:	00 
  8010f6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8010f9:	89 54 24 08          	mov    %edx,0x8(%esp)
  8010fd:	89 44 24 04          	mov    %eax,0x4(%esp)
  801101:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
  801108:	e8 12 fd ff ff       	call   800e1f <syscall>
	return;
  80110d:	90                   	nop
}
  80110e:	c9                   	leave  
  80110f:	c3                   	ret    

00801110 <__udivdi3>:
  801110:	55                   	push   %ebp
  801111:	57                   	push   %edi
  801112:	56                   	push   %esi
  801113:	83 ec 0c             	sub    $0xc,%esp
  801116:	8b 44 24 28          	mov    0x28(%esp),%eax
  80111a:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
  80111e:	8b 6c 24 20          	mov    0x20(%esp),%ebp
  801122:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  801126:	85 c0                	test   %eax,%eax
  801128:	89 7c 24 04          	mov    %edi,0x4(%esp)
  80112c:	89 ea                	mov    %ebp,%edx
  80112e:	89 0c 24             	mov    %ecx,(%esp)
  801131:	75 2d                	jne    801160 <__udivdi3+0x50>
  801133:	39 e9                	cmp    %ebp,%ecx
  801135:	77 61                	ja     801198 <__udivdi3+0x88>
  801137:	85 c9                	test   %ecx,%ecx
  801139:	89 ce                	mov    %ecx,%esi
  80113b:	75 0b                	jne    801148 <__udivdi3+0x38>
  80113d:	b8 01 00 00 00       	mov    $0x1,%eax
  801142:	31 d2                	xor    %edx,%edx
  801144:	f7 f1                	div    %ecx
  801146:	89 c6                	mov    %eax,%esi
  801148:	31 d2                	xor    %edx,%edx
  80114a:	89 e8                	mov    %ebp,%eax
  80114c:	f7 f6                	div    %esi
  80114e:	89 c5                	mov    %eax,%ebp
  801150:	89 f8                	mov    %edi,%eax
  801152:	f7 f6                	div    %esi
  801154:	89 ea                	mov    %ebp,%edx
  801156:	83 c4 0c             	add    $0xc,%esp
  801159:	5e                   	pop    %esi
  80115a:	5f                   	pop    %edi
  80115b:	5d                   	pop    %ebp
  80115c:	c3                   	ret    
  80115d:	8d 76 00             	lea    0x0(%esi),%esi
  801160:	39 e8                	cmp    %ebp,%eax
  801162:	77 24                	ja     801188 <__udivdi3+0x78>
  801164:	0f bd e8             	bsr    %eax,%ebp
  801167:	83 f5 1f             	xor    $0x1f,%ebp
  80116a:	75 3c                	jne    8011a8 <__udivdi3+0x98>
  80116c:	8b 74 24 04          	mov    0x4(%esp),%esi
  801170:	39 34 24             	cmp    %esi,(%esp)
  801173:	0f 86 9f 00 00 00    	jbe    801218 <__udivdi3+0x108>
  801179:	39 d0                	cmp    %edx,%eax
  80117b:	0f 82 97 00 00 00    	jb     801218 <__udivdi3+0x108>
  801181:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801188:	31 d2                	xor    %edx,%edx
  80118a:	31 c0                	xor    %eax,%eax
  80118c:	83 c4 0c             	add    $0xc,%esp
  80118f:	5e                   	pop    %esi
  801190:	5f                   	pop    %edi
  801191:	5d                   	pop    %ebp
  801192:	c3                   	ret    
  801193:	90                   	nop
  801194:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801198:	89 f8                	mov    %edi,%eax
  80119a:	f7 f1                	div    %ecx
  80119c:	31 d2                	xor    %edx,%edx
  80119e:	83 c4 0c             	add    $0xc,%esp
  8011a1:	5e                   	pop    %esi
  8011a2:	5f                   	pop    %edi
  8011a3:	5d                   	pop    %ebp
  8011a4:	c3                   	ret    
  8011a5:	8d 76 00             	lea    0x0(%esi),%esi
  8011a8:	89 e9                	mov    %ebp,%ecx
  8011aa:	8b 3c 24             	mov    (%esp),%edi
  8011ad:	d3 e0                	shl    %cl,%eax
  8011af:	89 c6                	mov    %eax,%esi
  8011b1:	b8 20 00 00 00       	mov    $0x20,%eax
  8011b6:	29 e8                	sub    %ebp,%eax
  8011b8:	89 c1                	mov    %eax,%ecx
  8011ba:	d3 ef                	shr    %cl,%edi
  8011bc:	89 e9                	mov    %ebp,%ecx
  8011be:	89 7c 24 08          	mov    %edi,0x8(%esp)
  8011c2:	8b 3c 24             	mov    (%esp),%edi
  8011c5:	09 74 24 08          	or     %esi,0x8(%esp)
  8011c9:	89 d6                	mov    %edx,%esi
  8011cb:	d3 e7                	shl    %cl,%edi
  8011cd:	89 c1                	mov    %eax,%ecx
  8011cf:	89 3c 24             	mov    %edi,(%esp)
  8011d2:	8b 7c 24 04          	mov    0x4(%esp),%edi
  8011d6:	d3 ee                	shr    %cl,%esi
  8011d8:	89 e9                	mov    %ebp,%ecx
  8011da:	d3 e2                	shl    %cl,%edx
  8011dc:	89 c1                	mov    %eax,%ecx
  8011de:	d3 ef                	shr    %cl,%edi
  8011e0:	09 d7                	or     %edx,%edi
  8011e2:	89 f2                	mov    %esi,%edx
  8011e4:	89 f8                	mov    %edi,%eax
  8011e6:	f7 74 24 08          	divl   0x8(%esp)
  8011ea:	89 d6                	mov    %edx,%esi
  8011ec:	89 c7                	mov    %eax,%edi
  8011ee:	f7 24 24             	mull   (%esp)
  8011f1:	39 d6                	cmp    %edx,%esi
  8011f3:	89 14 24             	mov    %edx,(%esp)
  8011f6:	72 30                	jb     801228 <__udivdi3+0x118>
  8011f8:	8b 54 24 04          	mov    0x4(%esp),%edx
  8011fc:	89 e9                	mov    %ebp,%ecx
  8011fe:	d3 e2                	shl    %cl,%edx
  801200:	39 c2                	cmp    %eax,%edx
  801202:	73 05                	jae    801209 <__udivdi3+0xf9>
  801204:	3b 34 24             	cmp    (%esp),%esi
  801207:	74 1f                	je     801228 <__udivdi3+0x118>
  801209:	89 f8                	mov    %edi,%eax
  80120b:	31 d2                	xor    %edx,%edx
  80120d:	e9 7a ff ff ff       	jmp    80118c <__udivdi3+0x7c>
  801212:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801218:	31 d2                	xor    %edx,%edx
  80121a:	b8 01 00 00 00       	mov    $0x1,%eax
  80121f:	e9 68 ff ff ff       	jmp    80118c <__udivdi3+0x7c>
  801224:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801228:	8d 47 ff             	lea    -0x1(%edi),%eax
  80122b:	31 d2                	xor    %edx,%edx
  80122d:	83 c4 0c             	add    $0xc,%esp
  801230:	5e                   	pop    %esi
  801231:	5f                   	pop    %edi
  801232:	5d                   	pop    %ebp
  801233:	c3                   	ret    
  801234:	66 90                	xchg   %ax,%ax
  801236:	66 90                	xchg   %ax,%ax
  801238:	66 90                	xchg   %ax,%ax
  80123a:	66 90                	xchg   %ax,%ax
  80123c:	66 90                	xchg   %ax,%ax
  80123e:	66 90                	xchg   %ax,%ax

00801240 <__umoddi3>:
  801240:	55                   	push   %ebp
  801241:	57                   	push   %edi
  801242:	56                   	push   %esi
  801243:	83 ec 14             	sub    $0x14,%esp
  801246:	8b 44 24 28          	mov    0x28(%esp),%eax
  80124a:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  80124e:	8b 74 24 2c          	mov    0x2c(%esp),%esi
  801252:	89 c7                	mov    %eax,%edi
  801254:	89 44 24 04          	mov    %eax,0x4(%esp)
  801258:	8b 44 24 30          	mov    0x30(%esp),%eax
  80125c:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  801260:	89 34 24             	mov    %esi,(%esp)
  801263:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801267:	85 c0                	test   %eax,%eax
  801269:	89 c2                	mov    %eax,%edx
  80126b:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80126f:	75 17                	jne    801288 <__umoddi3+0x48>
  801271:	39 fe                	cmp    %edi,%esi
  801273:	76 4b                	jbe    8012c0 <__umoddi3+0x80>
  801275:	89 c8                	mov    %ecx,%eax
  801277:	89 fa                	mov    %edi,%edx
  801279:	f7 f6                	div    %esi
  80127b:	89 d0                	mov    %edx,%eax
  80127d:	31 d2                	xor    %edx,%edx
  80127f:	83 c4 14             	add    $0x14,%esp
  801282:	5e                   	pop    %esi
  801283:	5f                   	pop    %edi
  801284:	5d                   	pop    %ebp
  801285:	c3                   	ret    
  801286:	66 90                	xchg   %ax,%ax
  801288:	39 f8                	cmp    %edi,%eax
  80128a:	77 54                	ja     8012e0 <__umoddi3+0xa0>
  80128c:	0f bd e8             	bsr    %eax,%ebp
  80128f:	83 f5 1f             	xor    $0x1f,%ebp
  801292:	75 5c                	jne    8012f0 <__umoddi3+0xb0>
  801294:	8b 7c 24 08          	mov    0x8(%esp),%edi
  801298:	39 3c 24             	cmp    %edi,(%esp)
  80129b:	0f 87 e7 00 00 00    	ja     801388 <__umoddi3+0x148>
  8012a1:	8b 7c 24 04          	mov    0x4(%esp),%edi
  8012a5:	29 f1                	sub    %esi,%ecx
  8012a7:	19 c7                	sbb    %eax,%edi
  8012a9:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8012ad:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8012b1:	8b 44 24 08          	mov    0x8(%esp),%eax
  8012b5:	8b 54 24 0c          	mov    0xc(%esp),%edx
  8012b9:	83 c4 14             	add    $0x14,%esp
  8012bc:	5e                   	pop    %esi
  8012bd:	5f                   	pop    %edi
  8012be:	5d                   	pop    %ebp
  8012bf:	c3                   	ret    
  8012c0:	85 f6                	test   %esi,%esi
  8012c2:	89 f5                	mov    %esi,%ebp
  8012c4:	75 0b                	jne    8012d1 <__umoddi3+0x91>
  8012c6:	b8 01 00 00 00       	mov    $0x1,%eax
  8012cb:	31 d2                	xor    %edx,%edx
  8012cd:	f7 f6                	div    %esi
  8012cf:	89 c5                	mov    %eax,%ebp
  8012d1:	8b 44 24 04          	mov    0x4(%esp),%eax
  8012d5:	31 d2                	xor    %edx,%edx
  8012d7:	f7 f5                	div    %ebp
  8012d9:	89 c8                	mov    %ecx,%eax
  8012db:	f7 f5                	div    %ebp
  8012dd:	eb 9c                	jmp    80127b <__umoddi3+0x3b>
  8012df:	90                   	nop
  8012e0:	89 c8                	mov    %ecx,%eax
  8012e2:	89 fa                	mov    %edi,%edx
  8012e4:	83 c4 14             	add    $0x14,%esp
  8012e7:	5e                   	pop    %esi
  8012e8:	5f                   	pop    %edi
  8012e9:	5d                   	pop    %ebp
  8012ea:	c3                   	ret    
  8012eb:	90                   	nop
  8012ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8012f0:	8b 04 24             	mov    (%esp),%eax
  8012f3:	be 20 00 00 00       	mov    $0x20,%esi
  8012f8:	89 e9                	mov    %ebp,%ecx
  8012fa:	29 ee                	sub    %ebp,%esi
  8012fc:	d3 e2                	shl    %cl,%edx
  8012fe:	89 f1                	mov    %esi,%ecx
  801300:	d3 e8                	shr    %cl,%eax
  801302:	89 e9                	mov    %ebp,%ecx
  801304:	89 44 24 04          	mov    %eax,0x4(%esp)
  801308:	8b 04 24             	mov    (%esp),%eax
  80130b:	09 54 24 04          	or     %edx,0x4(%esp)
  80130f:	89 fa                	mov    %edi,%edx
  801311:	d3 e0                	shl    %cl,%eax
  801313:	89 f1                	mov    %esi,%ecx
  801315:	89 44 24 08          	mov    %eax,0x8(%esp)
  801319:	8b 44 24 10          	mov    0x10(%esp),%eax
  80131d:	d3 ea                	shr    %cl,%edx
  80131f:	89 e9                	mov    %ebp,%ecx
  801321:	d3 e7                	shl    %cl,%edi
  801323:	89 f1                	mov    %esi,%ecx
  801325:	d3 e8                	shr    %cl,%eax
  801327:	89 e9                	mov    %ebp,%ecx
  801329:	09 f8                	or     %edi,%eax
  80132b:	8b 7c 24 10          	mov    0x10(%esp),%edi
  80132f:	f7 74 24 04          	divl   0x4(%esp)
  801333:	d3 e7                	shl    %cl,%edi
  801335:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801339:	89 d7                	mov    %edx,%edi
  80133b:	f7 64 24 08          	mull   0x8(%esp)
  80133f:	39 d7                	cmp    %edx,%edi
  801341:	89 c1                	mov    %eax,%ecx
  801343:	89 14 24             	mov    %edx,(%esp)
  801346:	72 2c                	jb     801374 <__umoddi3+0x134>
  801348:	39 44 24 0c          	cmp    %eax,0xc(%esp)
  80134c:	72 22                	jb     801370 <__umoddi3+0x130>
  80134e:	8b 44 24 0c          	mov    0xc(%esp),%eax
  801352:	29 c8                	sub    %ecx,%eax
  801354:	19 d7                	sbb    %edx,%edi
  801356:	89 e9                	mov    %ebp,%ecx
  801358:	89 fa                	mov    %edi,%edx
  80135a:	d3 e8                	shr    %cl,%eax
  80135c:	89 f1                	mov    %esi,%ecx
  80135e:	d3 e2                	shl    %cl,%edx
  801360:	89 e9                	mov    %ebp,%ecx
  801362:	d3 ef                	shr    %cl,%edi
  801364:	09 d0                	or     %edx,%eax
  801366:	89 fa                	mov    %edi,%edx
  801368:	83 c4 14             	add    $0x14,%esp
  80136b:	5e                   	pop    %esi
  80136c:	5f                   	pop    %edi
  80136d:	5d                   	pop    %ebp
  80136e:	c3                   	ret    
  80136f:	90                   	nop
  801370:	39 d7                	cmp    %edx,%edi
  801372:	75 da                	jne    80134e <__umoddi3+0x10e>
  801374:	8b 14 24             	mov    (%esp),%edx
  801377:	89 c1                	mov    %eax,%ecx
  801379:	2b 4c 24 08          	sub    0x8(%esp),%ecx
  80137d:	1b 54 24 04          	sbb    0x4(%esp),%edx
  801381:	eb cb                	jmp    80134e <__umoddi3+0x10e>
  801383:	90                   	nop
  801384:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801388:	3b 44 24 0c          	cmp    0xc(%esp),%eax
  80138c:	0f 82 0f ff ff ff    	jb     8012a1 <__umoddi3+0x61>
  801392:	e9 1a ff ff ff       	jmp    8012b1 <__umoddi3+0x71>
