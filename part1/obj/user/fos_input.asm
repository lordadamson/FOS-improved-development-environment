
obj/user/fos_input:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 a2 00 00 00       	call   8000d8 <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:

#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	81 ec 28 02 00 00    	sub    $0x228,%esp
	int i1=0;
  800041:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i2=0;
  800048:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
	char buff1[256];
	char buff2[256];	
	
	readline("Please enter first number :", buff1);	
  80004f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800055:	89 44 24 04          	mov    %eax,0x4(%esp)
  800059:	c7 04 24 60 15 80 00 	movl   $0x801560,(%esp)
  800060:	e8 75 08 00 00       	call   8008da <readline>
	i1 = strtol(buff1, NULL, 10);
  800065:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
  80006c:	00 
  80006d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  800074:	00 
  800075:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80007b:	89 04 24             	mov    %eax,(%esp)
  80007e:	e8 d5 0c 00 00       	call   800d58 <strtol>
  800083:	89 45 f4             	mov    %eax,-0xc(%ebp)
	readline("Please enter second number :", buff2);
  800086:	8d 85 f0 fd ff ff    	lea    -0x210(%ebp),%eax
  80008c:	89 44 24 04          	mov    %eax,0x4(%esp)
  800090:	c7 04 24 7c 15 80 00 	movl   $0x80157c,(%esp)
  800097:	e8 3e 08 00 00       	call   8008da <readline>
	
	i2 = strtol(buff2, NULL, 10);
  80009c:	c7 44 24 08 0a 00 00 	movl   $0xa,0x8(%esp)
  8000a3:	00 
  8000a4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  8000ab:	00 
  8000ac:	8d 85 f0 fd ff ff    	lea    -0x210(%ebp),%eax
  8000b2:	89 04 24             	mov    %eax,(%esp)
  8000b5:	e8 9e 0c 00 00       	call   800d58 <strtol>
  8000ba:	89 45 f0             	mov    %eax,-0x10(%ebp)

	cprintf("number 1 + number 2 = %d\n",i1+i2);
  8000bd:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8000c0:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8000c3:	01 d0                	add    %edx,%eax
  8000c5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8000c9:	c7 04 24 99 15 80 00 	movl   $0x801599,(%esp)
  8000d0:	e8 1d 01 00 00       	call   8001f2 <cprintf>
	return;	
  8000d5:	90                   	nop
}
  8000d6:	c9                   	leave  
  8000d7:	c3                   	ret    

008000d8 <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8000d8:	55                   	push   %ebp
  8000d9:	89 e5                	mov    %esp,%ebp
  8000db:	83 ec 18             	sub    $0x18,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8000de:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000e5:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000e8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000ec:	7e 0a                	jle    8000f8 <libmain+0x20>
		binaryname = argv[0];
  8000ee:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000f1:	8b 00                	mov    (%eax),%eax
  8000f3:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000f8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000fb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8000ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800102:	89 04 24             	mov    %eax,(%esp)
  800105:	e8 2e ff ff ff       	call   800038 <_main>

	// exit gracefully
	//exit();
	sleep();
  80010a:	e8 16 00 00 00       	call   800125 <sleep>
}
  80010f:	c9                   	leave  
  800110:	c3                   	ret    

00800111 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800111:	55                   	push   %ebp
  800112:	89 e5                	mov    %esp,%ebp
  800114:	83 ec 18             	sub    $0x18,%esp
	sys_env_destroy(0);	
  800117:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  80011e:	e8 1a 0f 00 00       	call   80103d <sys_env_destroy>
}
  800123:	c9                   	leave  
  800124:	c3                   	ret    

00800125 <sleep>:

void
sleep(void)
{	
  800125:	55                   	push   %ebp
  800126:	89 e5                	mov    %esp,%ebp
  800128:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  80012b:	e8 84 0f 00 00       	call   8010b4 <sys_env_sleep>
}
  800130:	c9                   	leave  
  800131:	c3                   	ret    

00800132 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800132:	55                   	push   %ebp
  800133:	89 e5                	mov    %esp,%ebp
  800135:	83 ec 18             	sub    $0x18,%esp
	b->buf[b->idx++] = ch;
  800138:	8b 45 0c             	mov    0xc(%ebp),%eax
  80013b:	8b 00                	mov    (%eax),%eax
  80013d:	8d 48 01             	lea    0x1(%eax),%ecx
  800140:	8b 55 0c             	mov    0xc(%ebp),%edx
  800143:	89 0a                	mov    %ecx,(%edx)
  800145:	8b 55 08             	mov    0x8(%ebp),%edx
  800148:	89 d1                	mov    %edx,%ecx
  80014a:	8b 55 0c             	mov    0xc(%ebp),%edx
  80014d:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800151:	8b 45 0c             	mov    0xc(%ebp),%eax
  800154:	8b 00                	mov    (%eax),%eax
  800156:	3d ff 00 00 00       	cmp    $0xff,%eax
  80015b:	75 20                	jne    80017d <putch+0x4b>
		sys_cputs(b->buf, b->idx);
  80015d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800160:	8b 00                	mov    (%eax),%eax
  800162:	8b 55 0c             	mov    0xc(%ebp),%edx
  800165:	83 c2 08             	add    $0x8,%edx
  800168:	89 44 24 04          	mov    %eax,0x4(%esp)
  80016c:	89 14 24             	mov    %edx,(%esp)
  80016f:	e8 53 0e 00 00       	call   800fc7 <sys_cputs>
		b->idx = 0;
  800174:	8b 45 0c             	mov    0xc(%ebp),%eax
  800177:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  80017d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800180:	8b 40 04             	mov    0x4(%eax),%eax
  800183:	8d 50 01             	lea    0x1(%eax),%edx
  800186:	8b 45 0c             	mov    0xc(%ebp),%eax
  800189:	89 50 04             	mov    %edx,0x4(%eax)
}
  80018c:	c9                   	leave  
  80018d:	c3                   	ret    

0080018e <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80018e:	55                   	push   %ebp
  80018f:	89 e5                	mov    %esp,%ebp
  800191:	81 ec 28 01 00 00    	sub    $0x128,%esp
	struct printbuf b;

	b.idx = 0;
  800197:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80019e:	00 00 00 
	b.cnt = 0;
  8001a1:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001a8:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001ab:	8b 45 0c             	mov    0xc(%ebp),%eax
  8001ae:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8001b2:	8b 45 08             	mov    0x8(%ebp),%eax
  8001b5:	89 44 24 08          	mov    %eax,0x8(%esp)
  8001b9:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001bf:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001c3:	c7 04 24 32 01 80 00 	movl   $0x800132,(%esp)
  8001ca:	e8 ed 01 00 00       	call   8003bc <vprintfmt>
	sys_cputs(b.buf, b.idx);
  8001cf:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8001d5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8001d9:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001df:	83 c0 08             	add    $0x8,%eax
  8001e2:	89 04 24             	mov    %eax,(%esp)
  8001e5:	e8 dd 0d 00 00       	call   800fc7 <sys_cputs>

	return b.cnt;
  8001ea:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001f0:	c9                   	leave  
  8001f1:	c3                   	ret    

008001f2 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001f2:	55                   	push   %ebp
  8001f3:	89 e5                	mov    %esp,%ebp
  8001f5:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001f8:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001fb:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001fe:	8b 45 08             	mov    0x8(%ebp),%eax
  800201:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800204:	89 54 24 04          	mov    %edx,0x4(%esp)
  800208:	89 04 24             	mov    %eax,(%esp)
  80020b:	e8 7e ff ff ff       	call   80018e <vcprintf>
  800210:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  800213:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800216:	c9                   	leave  
  800217:	c3                   	ret    

00800218 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800218:	55                   	push   %ebp
  800219:	89 e5                	mov    %esp,%ebp
  80021b:	53                   	push   %ebx
  80021c:	83 ec 34             	sub    $0x34,%esp
  80021f:	8b 45 10             	mov    0x10(%ebp),%eax
  800222:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800225:	8b 45 14             	mov    0x14(%ebp),%eax
  800228:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80022b:	8b 45 18             	mov    0x18(%ebp),%eax
  80022e:	ba 00 00 00 00       	mov    $0x0,%edx
  800233:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800236:	77 72                	ja     8002aa <printnum+0x92>
  800238:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80023b:	72 05                	jb     800242 <printnum+0x2a>
  80023d:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800240:	77 68                	ja     8002aa <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800242:	8b 45 1c             	mov    0x1c(%ebp),%eax
  800245:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800248:	8b 45 18             	mov    0x18(%ebp),%eax
  80024b:	ba 00 00 00 00       	mov    $0x0,%edx
  800250:	89 44 24 08          	mov    %eax,0x8(%esp)
  800254:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800258:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80025b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80025e:	89 04 24             	mov    %eax,(%esp)
  800261:	89 54 24 04          	mov    %edx,0x4(%esp)
  800265:	e8 66 10 00 00       	call   8012d0 <__udivdi3>
  80026a:	8b 4d 20             	mov    0x20(%ebp),%ecx
  80026d:	89 4c 24 18          	mov    %ecx,0x18(%esp)
  800271:	89 5c 24 14          	mov    %ebx,0x14(%esp)
  800275:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800278:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  80027c:	89 44 24 08          	mov    %eax,0x8(%esp)
  800280:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800284:	8b 45 0c             	mov    0xc(%ebp),%eax
  800287:	89 44 24 04          	mov    %eax,0x4(%esp)
  80028b:	8b 45 08             	mov    0x8(%ebp),%eax
  80028e:	89 04 24             	mov    %eax,(%esp)
  800291:	e8 82 ff ff ff       	call   800218 <printnum>
  800296:	eb 1c                	jmp    8002b4 <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800298:	8b 45 0c             	mov    0xc(%ebp),%eax
  80029b:	89 44 24 04          	mov    %eax,0x4(%esp)
  80029f:	8b 45 20             	mov    0x20(%ebp),%eax
  8002a2:	89 04 24             	mov    %eax,(%esp)
  8002a5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a8:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8002aa:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  8002ae:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  8002b2:	7f e4                	jg     800298 <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8002b4:	8b 4d 18             	mov    0x18(%ebp),%ecx
  8002b7:	bb 00 00 00 00       	mov    $0x0,%ebx
  8002bc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8002bf:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8002c2:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8002c6:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  8002ca:	89 04 24             	mov    %eax,(%esp)
  8002cd:	89 54 24 04          	mov    %edx,0x4(%esp)
  8002d1:	e8 2a 11 00 00       	call   801400 <__umoddi3>
  8002d6:	05 80 16 80 00       	add    $0x801680,%eax
  8002db:	0f b6 00             	movzbl (%eax),%eax
  8002de:	0f be c0             	movsbl %al,%eax
  8002e1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8002e4:	89 54 24 04          	mov    %edx,0x4(%esp)
  8002e8:	89 04 24             	mov    %eax,(%esp)
  8002eb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ee:	ff d0                	call   *%eax
}
  8002f0:	83 c4 34             	add    $0x34,%esp
  8002f3:	5b                   	pop    %ebx
  8002f4:	5d                   	pop    %ebp
  8002f5:	c3                   	ret    

008002f6 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002f6:	55                   	push   %ebp
  8002f7:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002f9:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002fd:	7e 1c                	jle    80031b <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8002ff:	8b 45 08             	mov    0x8(%ebp),%eax
  800302:	8b 00                	mov    (%eax),%eax
  800304:	8d 50 08             	lea    0x8(%eax),%edx
  800307:	8b 45 08             	mov    0x8(%ebp),%eax
  80030a:	89 10                	mov    %edx,(%eax)
  80030c:	8b 45 08             	mov    0x8(%ebp),%eax
  80030f:	8b 00                	mov    (%eax),%eax
  800311:	83 e8 08             	sub    $0x8,%eax
  800314:	8b 50 04             	mov    0x4(%eax),%edx
  800317:	8b 00                	mov    (%eax),%eax
  800319:	eb 40                	jmp    80035b <getuint+0x65>
	else if (lflag)
  80031b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80031f:	74 1e                	je     80033f <getuint+0x49>
		return va_arg(*ap, unsigned long);
  800321:	8b 45 08             	mov    0x8(%ebp),%eax
  800324:	8b 00                	mov    (%eax),%eax
  800326:	8d 50 04             	lea    0x4(%eax),%edx
  800329:	8b 45 08             	mov    0x8(%ebp),%eax
  80032c:	89 10                	mov    %edx,(%eax)
  80032e:	8b 45 08             	mov    0x8(%ebp),%eax
  800331:	8b 00                	mov    (%eax),%eax
  800333:	83 e8 04             	sub    $0x4,%eax
  800336:	8b 00                	mov    (%eax),%eax
  800338:	ba 00 00 00 00       	mov    $0x0,%edx
  80033d:	eb 1c                	jmp    80035b <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  80033f:	8b 45 08             	mov    0x8(%ebp),%eax
  800342:	8b 00                	mov    (%eax),%eax
  800344:	8d 50 04             	lea    0x4(%eax),%edx
  800347:	8b 45 08             	mov    0x8(%ebp),%eax
  80034a:	89 10                	mov    %edx,(%eax)
  80034c:	8b 45 08             	mov    0x8(%ebp),%eax
  80034f:	8b 00                	mov    (%eax),%eax
  800351:	83 e8 04             	sub    $0x4,%eax
  800354:	8b 00                	mov    (%eax),%eax
  800356:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80035b:	5d                   	pop    %ebp
  80035c:	c3                   	ret    

0080035d <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  80035d:	55                   	push   %ebp
  80035e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800360:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800364:	7e 1c                	jle    800382 <getint+0x25>
		return va_arg(*ap, long long);
  800366:	8b 45 08             	mov    0x8(%ebp),%eax
  800369:	8b 00                	mov    (%eax),%eax
  80036b:	8d 50 08             	lea    0x8(%eax),%edx
  80036e:	8b 45 08             	mov    0x8(%ebp),%eax
  800371:	89 10                	mov    %edx,(%eax)
  800373:	8b 45 08             	mov    0x8(%ebp),%eax
  800376:	8b 00                	mov    (%eax),%eax
  800378:	83 e8 08             	sub    $0x8,%eax
  80037b:	8b 50 04             	mov    0x4(%eax),%edx
  80037e:	8b 00                	mov    (%eax),%eax
  800380:	eb 38                	jmp    8003ba <getint+0x5d>
	else if (lflag)
  800382:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800386:	74 1a                	je     8003a2 <getint+0x45>
		return va_arg(*ap, long);
  800388:	8b 45 08             	mov    0x8(%ebp),%eax
  80038b:	8b 00                	mov    (%eax),%eax
  80038d:	8d 50 04             	lea    0x4(%eax),%edx
  800390:	8b 45 08             	mov    0x8(%ebp),%eax
  800393:	89 10                	mov    %edx,(%eax)
  800395:	8b 45 08             	mov    0x8(%ebp),%eax
  800398:	8b 00                	mov    (%eax),%eax
  80039a:	83 e8 04             	sub    $0x4,%eax
  80039d:	8b 00                	mov    (%eax),%eax
  80039f:	99                   	cltd   
  8003a0:	eb 18                	jmp    8003ba <getint+0x5d>
	else
		return va_arg(*ap, int);
  8003a2:	8b 45 08             	mov    0x8(%ebp),%eax
  8003a5:	8b 00                	mov    (%eax),%eax
  8003a7:	8d 50 04             	lea    0x4(%eax),%edx
  8003aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8003ad:	89 10                	mov    %edx,(%eax)
  8003af:	8b 45 08             	mov    0x8(%ebp),%eax
  8003b2:	8b 00                	mov    (%eax),%eax
  8003b4:	83 e8 04             	sub    $0x4,%eax
  8003b7:	8b 00                	mov    (%eax),%eax
  8003b9:	99                   	cltd   
}
  8003ba:	5d                   	pop    %ebp
  8003bb:	c3                   	ret    

008003bc <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8003bc:	55                   	push   %ebp
  8003bd:	89 e5                	mov    %esp,%ebp
  8003bf:	56                   	push   %esi
  8003c0:	53                   	push   %ebx
  8003c1:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003c4:	eb 18                	jmp    8003de <vprintfmt+0x22>
			if (ch == '\0')
  8003c6:	85 db                	test   %ebx,%ebx
  8003c8:	75 05                	jne    8003cf <vprintfmt+0x13>
				return;
  8003ca:	e9 07 04 00 00       	jmp    8007d6 <vprintfmt+0x41a>
			putch(ch, putdat);
  8003cf:	8b 45 0c             	mov    0xc(%ebp),%eax
  8003d2:	89 44 24 04          	mov    %eax,0x4(%esp)
  8003d6:	89 1c 24             	mov    %ebx,(%esp)
  8003d9:	8b 45 08             	mov    0x8(%ebp),%eax
  8003dc:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003de:	8b 45 10             	mov    0x10(%ebp),%eax
  8003e1:	8d 50 01             	lea    0x1(%eax),%edx
  8003e4:	89 55 10             	mov    %edx,0x10(%ebp)
  8003e7:	0f b6 00             	movzbl (%eax),%eax
  8003ea:	0f b6 d8             	movzbl %al,%ebx
  8003ed:	83 fb 25             	cmp    $0x25,%ebx
  8003f0:	75 d4                	jne    8003c6 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8003f2:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8003f6:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8003fd:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  800404:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  80040b:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800412:	8b 45 10             	mov    0x10(%ebp),%eax
  800415:	8d 50 01             	lea    0x1(%eax),%edx
  800418:	89 55 10             	mov    %edx,0x10(%ebp)
  80041b:	0f b6 00             	movzbl (%eax),%eax
  80041e:	0f b6 d8             	movzbl %al,%ebx
  800421:	8d 43 dd             	lea    -0x23(%ebx),%eax
  800424:	83 f8 55             	cmp    $0x55,%eax
  800427:	0f 87 78 03 00 00    	ja     8007a5 <vprintfmt+0x3e9>
  80042d:	8b 04 85 a4 16 80 00 	mov    0x8016a4(,%eax,4),%eax
  800434:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  800436:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  80043a:	eb d6                	jmp    800412 <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80043c:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  800440:	eb d0                	jmp    800412 <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800442:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  800449:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80044c:	89 d0                	mov    %edx,%eax
  80044e:	c1 e0 02             	shl    $0x2,%eax
  800451:	01 d0                	add    %edx,%eax
  800453:	01 c0                	add    %eax,%eax
  800455:	01 d8                	add    %ebx,%eax
  800457:	83 e8 30             	sub    $0x30,%eax
  80045a:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  80045d:	8b 45 10             	mov    0x10(%ebp),%eax
  800460:	0f b6 00             	movzbl (%eax),%eax
  800463:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  800466:	83 fb 2f             	cmp    $0x2f,%ebx
  800469:	7e 0b                	jle    800476 <vprintfmt+0xba>
  80046b:	83 fb 39             	cmp    $0x39,%ebx
  80046e:	7f 06                	jg     800476 <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800470:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800474:	eb d3                	jmp    800449 <vprintfmt+0x8d>
			goto process_precision;
  800476:	eb 39                	jmp    8004b1 <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
  800478:	8b 45 14             	mov    0x14(%ebp),%eax
  80047b:	83 c0 04             	add    $0x4,%eax
  80047e:	89 45 14             	mov    %eax,0x14(%ebp)
  800481:	8b 45 14             	mov    0x14(%ebp),%eax
  800484:	83 e8 04             	sub    $0x4,%eax
  800487:	8b 00                	mov    (%eax),%eax
  800489:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  80048c:	eb 23                	jmp    8004b1 <vprintfmt+0xf5>

		case '.':
			if (width < 0)
  80048e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800492:	79 0c                	jns    8004a0 <vprintfmt+0xe4>
				width = 0;
  800494:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  80049b:	e9 72 ff ff ff       	jmp    800412 <vprintfmt+0x56>
  8004a0:	e9 6d ff ff ff       	jmp    800412 <vprintfmt+0x56>

		case '#':
			altflag = 1;
  8004a5:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  8004ac:	e9 61 ff ff ff       	jmp    800412 <vprintfmt+0x56>

		process_precision:
			if (width < 0)
  8004b1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004b5:	79 12                	jns    8004c9 <vprintfmt+0x10d>
				width = precision, precision = -1;
  8004b7:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8004ba:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8004bd:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  8004c4:	e9 49 ff ff ff       	jmp    800412 <vprintfmt+0x56>
  8004c9:	e9 44 ff ff ff       	jmp    800412 <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8004ce:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  8004d2:	e9 3b ff ff ff       	jmp    800412 <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8004d7:	8b 45 14             	mov    0x14(%ebp),%eax
  8004da:	83 c0 04             	add    $0x4,%eax
  8004dd:	89 45 14             	mov    %eax,0x14(%ebp)
  8004e0:	8b 45 14             	mov    0x14(%ebp),%eax
  8004e3:	83 e8 04             	sub    $0x4,%eax
  8004e6:	8b 00                	mov    (%eax),%eax
  8004e8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8004eb:	89 54 24 04          	mov    %edx,0x4(%esp)
  8004ef:	89 04 24             	mov    %eax,(%esp)
  8004f2:	8b 45 08             	mov    0x8(%ebp),%eax
  8004f5:	ff d0                	call   *%eax
			break;
  8004f7:	e9 d4 02 00 00       	jmp    8007d0 <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004fc:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ff:	83 c0 04             	add    $0x4,%eax
  800502:	89 45 14             	mov    %eax,0x14(%ebp)
  800505:	8b 45 14             	mov    0x14(%ebp),%eax
  800508:	83 e8 04             	sub    $0x4,%eax
  80050b:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  80050d:	85 db                	test   %ebx,%ebx
  80050f:	79 02                	jns    800513 <vprintfmt+0x157>
				err = -err;
  800511:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  800513:	83 fb 07             	cmp    $0x7,%ebx
  800516:	7f 0b                	jg     800523 <vprintfmt+0x167>
  800518:	8b 34 9d 60 16 80 00 	mov    0x801660(,%ebx,4),%esi
  80051f:	85 f6                	test   %esi,%esi
  800521:	75 23                	jne    800546 <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
  800523:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  800527:	c7 44 24 08 91 16 80 	movl   $0x801691,0x8(%esp)
  80052e:	00 
  80052f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800532:	89 44 24 04          	mov    %eax,0x4(%esp)
  800536:	8b 45 08             	mov    0x8(%ebp),%eax
  800539:	89 04 24             	mov    %eax,(%esp)
  80053c:	e8 9c 02 00 00       	call   8007dd <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
  800541:	e9 8a 02 00 00       	jmp    8007d0 <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  800546:	89 74 24 0c          	mov    %esi,0xc(%esp)
  80054a:	c7 44 24 08 9a 16 80 	movl   $0x80169a,0x8(%esp)
  800551:	00 
  800552:	8b 45 0c             	mov    0xc(%ebp),%eax
  800555:	89 44 24 04          	mov    %eax,0x4(%esp)
  800559:	8b 45 08             	mov    0x8(%ebp),%eax
  80055c:	89 04 24             	mov    %eax,(%esp)
  80055f:	e8 79 02 00 00       	call   8007dd <printfmt>
			break;
  800564:	e9 67 02 00 00       	jmp    8007d0 <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800569:	8b 45 14             	mov    0x14(%ebp),%eax
  80056c:	83 c0 04             	add    $0x4,%eax
  80056f:	89 45 14             	mov    %eax,0x14(%ebp)
  800572:	8b 45 14             	mov    0x14(%ebp),%eax
  800575:	83 e8 04             	sub    $0x4,%eax
  800578:	8b 30                	mov    (%eax),%esi
  80057a:	85 f6                	test   %esi,%esi
  80057c:	75 05                	jne    800583 <vprintfmt+0x1c7>
				p = "(null)";
  80057e:	be 9d 16 80 00       	mov    $0x80169d,%esi
			if (width > 0 && padc != '-')
  800583:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800587:	7e 37                	jle    8005c0 <vprintfmt+0x204>
  800589:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  80058d:	74 31                	je     8005c0 <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
  80058f:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800592:	89 44 24 04          	mov    %eax,0x4(%esp)
  800596:	89 34 24             	mov    %esi,(%esp)
  800599:	e8 53 04 00 00       	call   8009f1 <strnlen>
  80059e:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  8005a1:	eb 17                	jmp    8005ba <vprintfmt+0x1fe>
					putch(padc, putdat);
  8005a3:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  8005a7:	8b 55 0c             	mov    0xc(%ebp),%edx
  8005aa:	89 54 24 04          	mov    %edx,0x4(%esp)
  8005ae:	89 04 24             	mov    %eax,(%esp)
  8005b1:	8b 45 08             	mov    0x8(%ebp),%eax
  8005b4:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8005b6:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005ba:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005be:	7f e3                	jg     8005a3 <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005c0:	eb 38                	jmp    8005fa <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
  8005c2:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8005c6:	74 1f                	je     8005e7 <vprintfmt+0x22b>
  8005c8:	83 fb 1f             	cmp    $0x1f,%ebx
  8005cb:	7e 05                	jle    8005d2 <vprintfmt+0x216>
  8005cd:	83 fb 7e             	cmp    $0x7e,%ebx
  8005d0:	7e 15                	jle    8005e7 <vprintfmt+0x22b>
					putch('?', putdat);
  8005d2:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005d5:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005d9:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  8005e0:	8b 45 08             	mov    0x8(%ebp),%eax
  8005e3:	ff d0                	call   *%eax
  8005e5:	eb 0f                	jmp    8005f6 <vprintfmt+0x23a>
				else
					putch(ch, putdat);
  8005e7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8005ea:	89 44 24 04          	mov    %eax,0x4(%esp)
  8005ee:	89 1c 24             	mov    %ebx,(%esp)
  8005f1:	8b 45 08             	mov    0x8(%ebp),%eax
  8005f4:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005f6:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005fa:	89 f0                	mov    %esi,%eax
  8005fc:	8d 70 01             	lea    0x1(%eax),%esi
  8005ff:	0f b6 00             	movzbl (%eax),%eax
  800602:	0f be d8             	movsbl %al,%ebx
  800605:	85 db                	test   %ebx,%ebx
  800607:	74 10                	je     800619 <vprintfmt+0x25d>
  800609:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  80060d:	78 b3                	js     8005c2 <vprintfmt+0x206>
  80060f:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  800613:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800617:	79 a9                	jns    8005c2 <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800619:	eb 17                	jmp    800632 <vprintfmt+0x276>
				putch(' ', putdat);
  80061b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80061e:	89 44 24 04          	mov    %eax,0x4(%esp)
  800622:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  800629:	8b 45 08             	mov    0x8(%ebp),%eax
  80062c:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80062e:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800632:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800636:	7f e3                	jg     80061b <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
  800638:	e9 93 01 00 00       	jmp    8007d0 <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80063d:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800640:	89 44 24 04          	mov    %eax,0x4(%esp)
  800644:	8d 45 14             	lea    0x14(%ebp),%eax
  800647:	89 04 24             	mov    %eax,(%esp)
  80064a:	e8 0e fd ff ff       	call   80035d <getint>
  80064f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800652:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  800655:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800658:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80065b:	85 d2                	test   %edx,%edx
  80065d:	79 26                	jns    800685 <vprintfmt+0x2c9>
				putch('-', putdat);
  80065f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800662:	89 44 24 04          	mov    %eax,0x4(%esp)
  800666:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  80066d:	8b 45 08             	mov    0x8(%ebp),%eax
  800670:	ff d0                	call   *%eax
				num = -(long long) num;
  800672:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800675:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800678:	f7 d8                	neg    %eax
  80067a:	83 d2 00             	adc    $0x0,%edx
  80067d:	f7 da                	neg    %edx
  80067f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800682:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  800685:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  80068c:	e9 cb 00 00 00       	jmp    80075c <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800691:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800694:	89 44 24 04          	mov    %eax,0x4(%esp)
  800698:	8d 45 14             	lea    0x14(%ebp),%eax
  80069b:	89 04 24             	mov    %eax,(%esp)
  80069e:	e8 53 fc ff ff       	call   8002f6 <getuint>
  8006a3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006a6:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  8006a9:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8006b0:	e9 a7 00 00 00       	jmp    80075c <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  8006b5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006b8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006bc:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8006c6:	ff d0                	call   *%eax
			putch('X', putdat);
  8006c8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006cb:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006cf:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d9:	ff d0                	call   *%eax
			putch('X', putdat);
  8006db:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006de:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006e2:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
  8006e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ec:	ff d0                	call   *%eax
			break;
  8006ee:	e9 dd 00 00 00       	jmp    8007d0 <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
  8006f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8006f6:	89 44 24 04          	mov    %eax,0x4(%esp)
  8006fa:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  800701:	8b 45 08             	mov    0x8(%ebp),%eax
  800704:	ff d0                	call   *%eax
			putch('x', putdat);
  800706:	8b 45 0c             	mov    0xc(%ebp),%eax
  800709:	89 44 24 04          	mov    %eax,0x4(%esp)
  80070d:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  800714:	8b 45 08             	mov    0x8(%ebp),%eax
  800717:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  800719:	8b 45 14             	mov    0x14(%ebp),%eax
  80071c:	83 c0 04             	add    $0x4,%eax
  80071f:	89 45 14             	mov    %eax,0x14(%ebp)
  800722:	8b 45 14             	mov    0x14(%ebp),%eax
  800725:	83 e8 04             	sub    $0x4,%eax
  800728:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80072a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80072d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  800734:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  80073b:	eb 1f                	jmp    80075c <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80073d:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800740:	89 44 24 04          	mov    %eax,0x4(%esp)
  800744:	8d 45 14             	lea    0x14(%ebp),%eax
  800747:	89 04 24             	mov    %eax,(%esp)
  80074a:	e8 a7 fb ff ff       	call   8002f6 <getuint>
  80074f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800752:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  800755:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  80075c:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  800760:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800763:	89 54 24 18          	mov    %edx,0x18(%esp)
  800767:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  80076a:	89 54 24 14          	mov    %edx,0x14(%esp)
  80076e:	89 44 24 10          	mov    %eax,0x10(%esp)
  800772:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800775:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800778:	89 44 24 08          	mov    %eax,0x8(%esp)
  80077c:	89 54 24 0c          	mov    %edx,0xc(%esp)
  800780:	8b 45 0c             	mov    0xc(%ebp),%eax
  800783:	89 44 24 04          	mov    %eax,0x4(%esp)
  800787:	8b 45 08             	mov    0x8(%ebp),%eax
  80078a:	89 04 24             	mov    %eax,(%esp)
  80078d:	e8 86 fa ff ff       	call   800218 <printnum>
			break;
  800792:	eb 3c                	jmp    8007d0 <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800794:	8b 45 0c             	mov    0xc(%ebp),%eax
  800797:	89 44 24 04          	mov    %eax,0x4(%esp)
  80079b:	89 1c 24             	mov    %ebx,(%esp)
  80079e:	8b 45 08             	mov    0x8(%ebp),%eax
  8007a1:	ff d0                	call   *%eax
			break;
  8007a3:	eb 2b                	jmp    8007d0 <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8007a5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007a8:	89 44 24 04          	mov    %eax,0x4(%esp)
  8007ac:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  8007b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b6:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
  8007b8:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8007bc:	eb 04                	jmp    8007c2 <vprintfmt+0x406>
  8007be:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8007c2:	8b 45 10             	mov    0x10(%ebp),%eax
  8007c5:	83 e8 01             	sub    $0x1,%eax
  8007c8:	0f b6 00             	movzbl (%eax),%eax
  8007cb:	3c 25                	cmp    $0x25,%al
  8007cd:	75 ef                	jne    8007be <vprintfmt+0x402>
				/* do nothing */;
			break;
  8007cf:	90                   	nop
		}
	}
  8007d0:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8007d1:	e9 08 fc ff ff       	jmp    8003de <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  8007d6:	83 c4 40             	add    $0x40,%esp
  8007d9:	5b                   	pop    %ebx
  8007da:	5e                   	pop    %esi
  8007db:	5d                   	pop    %ebp
  8007dc:	c3                   	ret    

008007dd <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8007dd:	55                   	push   %ebp
  8007de:	89 e5                	mov    %esp,%ebp
  8007e0:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
  8007e3:	8d 45 10             	lea    0x10(%ebp),%eax
  8007e6:	83 c0 04             	add    $0x4,%eax
  8007e9:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  8007ec:	8b 45 10             	mov    0x10(%ebp),%eax
  8007ef:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8007f2:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8007f6:	89 44 24 08          	mov    %eax,0x8(%esp)
  8007fa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007fd:	89 44 24 04          	mov    %eax,0x4(%esp)
  800801:	8b 45 08             	mov    0x8(%ebp),%eax
  800804:	89 04 24             	mov    %eax,(%esp)
  800807:	e8 b0 fb ff ff       	call   8003bc <vprintfmt>
	va_end(ap);
}
  80080c:	c9                   	leave  
  80080d:	c3                   	ret    

0080080e <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80080e:	55                   	push   %ebp
  80080f:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800811:	8b 45 0c             	mov    0xc(%ebp),%eax
  800814:	8b 40 08             	mov    0x8(%eax),%eax
  800817:	8d 50 01             	lea    0x1(%eax),%edx
  80081a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80081d:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800820:	8b 45 0c             	mov    0xc(%ebp),%eax
  800823:	8b 10                	mov    (%eax),%edx
  800825:	8b 45 0c             	mov    0xc(%ebp),%eax
  800828:	8b 40 04             	mov    0x4(%eax),%eax
  80082b:	39 c2                	cmp    %eax,%edx
  80082d:	73 12                	jae    800841 <sprintputch+0x33>
		*b->buf++ = ch;
  80082f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800832:	8b 00                	mov    (%eax),%eax
  800834:	8d 48 01             	lea    0x1(%eax),%ecx
  800837:	8b 55 0c             	mov    0xc(%ebp),%edx
  80083a:	89 0a                	mov    %ecx,(%edx)
  80083c:	8b 55 08             	mov    0x8(%ebp),%edx
  80083f:	88 10                	mov    %dl,(%eax)
}
  800841:	5d                   	pop    %ebp
  800842:	c3                   	ret    

00800843 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800843:	55                   	push   %ebp
  800844:	89 e5                	mov    %esp,%ebp
  800846:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  800849:	8b 45 08             	mov    0x8(%ebp),%eax
  80084c:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80084f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800852:	8d 50 ff             	lea    -0x1(%eax),%edx
  800855:	8b 45 08             	mov    0x8(%ebp),%eax
  800858:	01 d0                	add    %edx,%eax
  80085a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80085d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800864:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800868:	74 06                	je     800870 <vsnprintf+0x2d>
  80086a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80086e:	7f 07                	jg     800877 <vsnprintf+0x34>
		return -E_INVAL;
  800870:	b8 03 00 00 00       	mov    $0x3,%eax
  800875:	eb 2a                	jmp    8008a1 <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800877:	8b 45 14             	mov    0x14(%ebp),%eax
  80087a:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80087e:	8b 45 10             	mov    0x10(%ebp),%eax
  800881:	89 44 24 08          	mov    %eax,0x8(%esp)
  800885:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800888:	89 44 24 04          	mov    %eax,0x4(%esp)
  80088c:	c7 04 24 0e 08 80 00 	movl   $0x80080e,(%esp)
  800893:	e8 24 fb ff ff       	call   8003bc <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
  800898:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80089b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80089e:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  8008a1:	c9                   	leave  
  8008a2:	c3                   	ret    

008008a3 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8008a3:	55                   	push   %ebp
  8008a4:	89 e5                	mov    %esp,%ebp
  8008a6:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8008a9:	8d 45 10             	lea    0x10(%ebp),%eax
  8008ac:	83 c0 04             	add    $0x4,%eax
  8008af:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  8008b2:	8b 45 10             	mov    0x10(%ebp),%eax
  8008b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8008b8:	89 54 24 0c          	mov    %edx,0xc(%esp)
  8008bc:	89 44 24 08          	mov    %eax,0x8(%esp)
  8008c0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008c3:	89 44 24 04          	mov    %eax,0x4(%esp)
  8008c7:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ca:	89 04 24             	mov    %eax,(%esp)
  8008cd:	e8 71 ff ff ff       	call   800843 <vsnprintf>
  8008d2:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  8008d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8008d8:	c9                   	leave  
  8008d9:	c3                   	ret    

008008da <readline>:

#define BUFLEN 1024
//static char buf[BUFLEN];

void readline(const char *prompt, char* buf)
{
  8008da:	55                   	push   %ebp
  8008db:	89 e5                	mov    %esp,%ebp
  8008dd:	83 ec 28             	sub    $0x28,%esp
	int i, c, echoing;
	
	if (prompt != NULL)
  8008e0:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8008e4:	74 13                	je     8008f9 <readline+0x1f>
		cprintf("%s", prompt);
  8008e6:	8b 45 08             	mov    0x8(%ebp),%eax
  8008e9:	89 44 24 04          	mov    %eax,0x4(%esp)
  8008ed:	c7 04 24 fc 17 80 00 	movl   $0x8017fc,(%esp)
  8008f4:	e8 f9 f8 ff ff       	call   8001f2 <cprintf>

	
	i = 0;
  8008f9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	echoing = iscons(0);	
  800900:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800907:	e8 af 09 00 00       	call   8012bb <iscons>
  80090c:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (1) {
		c = getchar();
  80090f:	e8 9a 09 00 00       	call   8012ae <getchar>
  800914:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if (c < 0) {
  800917:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  80091b:	79 23                	jns    800940 <readline+0x66>
			if (c != -E_EOF)
  80091d:	83 7d ec 07          	cmpl   $0x7,-0x14(%ebp)
  800921:	74 18                	je     80093b <readline+0x61>
				cprintf("read error: %e\n", c);			
  800923:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800926:	89 44 24 04          	mov    %eax,0x4(%esp)
  80092a:	c7 04 24 ff 17 80 00 	movl   $0x8017ff,(%esp)
  800931:	e8 bc f8 ff ff       	call   8001f2 <cprintf>
			return;
  800936:	e9 8e 00 00 00       	jmp    8009c9 <readline+0xef>
  80093b:	e9 89 00 00 00       	jmp    8009c9 <readline+0xef>
		} else if (c >= ' ' && i < BUFLEN-1) {
  800940:	83 7d ec 1f          	cmpl   $0x1f,-0x14(%ebp)
  800944:	7e 31                	jle    800977 <readline+0x9d>
  800946:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
  80094d:	7f 28                	jg     800977 <readline+0x9d>
			if (echoing)
  80094f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  800953:	74 0b                	je     800960 <readline+0x86>
				cputchar(c);
  800955:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800958:	89 04 24             	mov    %eax,(%esp)
  80095b:	e8 2d 09 00 00       	call   80128d <cputchar>
			buf[i++] = c;
  800960:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800963:	8d 50 01             	lea    0x1(%eax),%edx
  800966:	89 55 f4             	mov    %edx,-0xc(%ebp)
  800969:	89 c2                	mov    %eax,%edx
  80096b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80096e:	01 c2                	add    %eax,%edx
  800970:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800973:	88 02                	mov    %al,(%edx)
  800975:	eb 4d                	jmp    8009c4 <readline+0xea>
		} else if (c == '\b' && i > 0) {
  800977:	83 7d ec 08          	cmpl   $0x8,-0x14(%ebp)
  80097b:	75 1d                	jne    80099a <readline+0xc0>
  80097d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  800981:	7e 17                	jle    80099a <readline+0xc0>
			if (echoing)
  800983:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  800987:	74 0b                	je     800994 <readline+0xba>
				cputchar(c);
  800989:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80098c:	89 04 24             	mov    %eax,(%esp)
  80098f:	e8 f9 08 00 00       	call   80128d <cputchar>
			i--;
  800994:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
  800998:	eb 2a                	jmp    8009c4 <readline+0xea>
		} else if (c == '\n' || c == '\r') {
  80099a:	83 7d ec 0a          	cmpl   $0xa,-0x14(%ebp)
  80099e:	74 06                	je     8009a6 <readline+0xcc>
  8009a0:	83 7d ec 0d          	cmpl   $0xd,-0x14(%ebp)
  8009a4:	75 1e                	jne    8009c4 <readline+0xea>
			if (echoing)
  8009a6:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  8009aa:	74 0b                	je     8009b7 <readline+0xdd>
				cputchar(c);
  8009ac:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8009af:	89 04 24             	mov    %eax,(%esp)
  8009b2:	e8 d6 08 00 00       	call   80128d <cputchar>
			buf[i] = 0;	
  8009b7:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8009ba:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009bd:	01 d0                	add    %edx,%eax
  8009bf:	c6 00 00             	movb   $0x0,(%eax)
			return;		
  8009c2:	eb 05                	jmp    8009c9 <readline+0xef>
		}
	}
  8009c4:	e9 46 ff ff ff       	jmp    80090f <readline+0x35>
}
  8009c9:	c9                   	leave  
  8009ca:	c3                   	ret    

008009cb <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  8009cb:	55                   	push   %ebp
  8009cc:	89 e5                	mov    %esp,%ebp
  8009ce:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  8009d1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8009d8:	eb 08                	jmp    8009e2 <strlen+0x17>
		n++;
  8009da:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8009de:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e5:	0f b6 00             	movzbl (%eax),%eax
  8009e8:	84 c0                	test   %al,%al
  8009ea:	75 ee                	jne    8009da <strlen+0xf>
		n++;
	return n;
  8009ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8009ef:	c9                   	leave  
  8009f0:	c3                   	ret    

008009f1 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  8009f1:	55                   	push   %ebp
  8009f2:	89 e5                	mov    %esp,%ebp
  8009f4:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009f7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8009fe:	eb 0c                	jmp    800a0c <strnlen+0x1b>
		n++;
  800a00:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800a04:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a08:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  800a0c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800a10:	74 0a                	je     800a1c <strnlen+0x2b>
  800a12:	8b 45 08             	mov    0x8(%ebp),%eax
  800a15:	0f b6 00             	movzbl (%eax),%eax
  800a18:	84 c0                	test   %al,%al
  800a1a:	75 e4                	jne    800a00 <strnlen+0xf>
		n++;
	return n;
  800a1c:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800a1f:	c9                   	leave  
  800a20:	c3                   	ret    

00800a21 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800a21:	55                   	push   %ebp
  800a22:	89 e5                	mov    %esp,%ebp
  800a24:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800a27:	8b 45 08             	mov    0x8(%ebp),%eax
  800a2a:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800a2d:	90                   	nop
  800a2e:	8b 45 08             	mov    0x8(%ebp),%eax
  800a31:	8d 50 01             	lea    0x1(%eax),%edx
  800a34:	89 55 08             	mov    %edx,0x8(%ebp)
  800a37:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a3a:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a3d:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800a40:	0f b6 12             	movzbl (%edx),%edx
  800a43:	88 10                	mov    %dl,(%eax)
  800a45:	0f b6 00             	movzbl (%eax),%eax
  800a48:	84 c0                	test   %al,%al
  800a4a:	75 e2                	jne    800a2e <strcpy+0xd>
		/* do nothing */;
	return ret;
  800a4c:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800a4f:	c9                   	leave  
  800a50:	c3                   	ret    

00800a51 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  800a51:	55                   	push   %ebp
  800a52:	89 e5                	mov    %esp,%ebp
  800a54:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  800a57:	8b 45 08             	mov    0x8(%ebp),%eax
  800a5a:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800a5d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800a64:	eb 23                	jmp    800a89 <strncpy+0x38>
		*dst++ = *src;
  800a66:	8b 45 08             	mov    0x8(%ebp),%eax
  800a69:	8d 50 01             	lea    0x1(%eax),%edx
  800a6c:	89 55 08             	mov    %edx,0x8(%ebp)
  800a6f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a72:	0f b6 12             	movzbl (%edx),%edx
  800a75:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  800a77:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a7a:	0f b6 00             	movzbl (%eax),%eax
  800a7d:	84 c0                	test   %al,%al
  800a7f:	74 04                	je     800a85 <strncpy+0x34>
			src++;
  800a81:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a85:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800a89:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a8c:	3b 45 10             	cmp    0x10(%ebp),%eax
  800a8f:	72 d5                	jb     800a66 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800a91:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800a94:	c9                   	leave  
  800a95:	c3                   	ret    

00800a96 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800a96:	55                   	push   %ebp
  800a97:	89 e5                	mov    %esp,%ebp
  800a99:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800a9c:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9f:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800aa2:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800aa6:	74 33                	je     800adb <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800aa8:	eb 17                	jmp    800ac1 <strlcpy+0x2b>
			*dst++ = *src++;
  800aaa:	8b 45 08             	mov    0x8(%ebp),%eax
  800aad:	8d 50 01             	lea    0x1(%eax),%edx
  800ab0:	89 55 08             	mov    %edx,0x8(%ebp)
  800ab3:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ab6:	8d 4a 01             	lea    0x1(%edx),%ecx
  800ab9:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800abc:	0f b6 12             	movzbl (%edx),%edx
  800abf:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800ac1:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800ac5:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ac9:	74 0a                	je     800ad5 <strlcpy+0x3f>
  800acb:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ace:	0f b6 00             	movzbl (%eax),%eax
  800ad1:	84 c0                	test   %al,%al
  800ad3:	75 d5                	jne    800aaa <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800ad5:	8b 45 08             	mov    0x8(%ebp),%eax
  800ad8:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  800adb:	8b 55 08             	mov    0x8(%ebp),%edx
  800ade:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ae1:	29 c2                	sub    %eax,%edx
  800ae3:	89 d0                	mov    %edx,%eax
}
  800ae5:	c9                   	leave  
  800ae6:	c3                   	ret    

00800ae7 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800ae7:	55                   	push   %ebp
  800ae8:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800aea:	eb 08                	jmp    800af4 <strcmp+0xd>
		p++, q++;
  800aec:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800af0:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800af4:	8b 45 08             	mov    0x8(%ebp),%eax
  800af7:	0f b6 00             	movzbl (%eax),%eax
  800afa:	84 c0                	test   %al,%al
  800afc:	74 10                	je     800b0e <strcmp+0x27>
  800afe:	8b 45 08             	mov    0x8(%ebp),%eax
  800b01:	0f b6 10             	movzbl (%eax),%edx
  800b04:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b07:	0f b6 00             	movzbl (%eax),%eax
  800b0a:	38 c2                	cmp    %al,%dl
  800b0c:	74 de                	je     800aec <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800b0e:	8b 45 08             	mov    0x8(%ebp),%eax
  800b11:	0f b6 00             	movzbl (%eax),%eax
  800b14:	0f b6 d0             	movzbl %al,%edx
  800b17:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b1a:	0f b6 00             	movzbl (%eax),%eax
  800b1d:	0f b6 c0             	movzbl %al,%eax
  800b20:	29 c2                	sub    %eax,%edx
  800b22:	89 d0                	mov    %edx,%eax
}
  800b24:	5d                   	pop    %ebp
  800b25:	c3                   	ret    

00800b26 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800b26:	55                   	push   %ebp
  800b27:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800b29:	eb 0c                	jmp    800b37 <strncmp+0x11>
		n--, p++, q++;
  800b2b:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800b2f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b33:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800b37:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b3b:	74 1a                	je     800b57 <strncmp+0x31>
  800b3d:	8b 45 08             	mov    0x8(%ebp),%eax
  800b40:	0f b6 00             	movzbl (%eax),%eax
  800b43:	84 c0                	test   %al,%al
  800b45:	74 10                	je     800b57 <strncmp+0x31>
  800b47:	8b 45 08             	mov    0x8(%ebp),%eax
  800b4a:	0f b6 10             	movzbl (%eax),%edx
  800b4d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b50:	0f b6 00             	movzbl (%eax),%eax
  800b53:	38 c2                	cmp    %al,%dl
  800b55:	74 d4                	je     800b2b <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800b57:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b5b:	75 07                	jne    800b64 <strncmp+0x3e>
		return 0;
  800b5d:	b8 00 00 00 00       	mov    $0x0,%eax
  800b62:	eb 16                	jmp    800b7a <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800b64:	8b 45 08             	mov    0x8(%ebp),%eax
  800b67:	0f b6 00             	movzbl (%eax),%eax
  800b6a:	0f b6 d0             	movzbl %al,%edx
  800b6d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b70:	0f b6 00             	movzbl (%eax),%eax
  800b73:	0f b6 c0             	movzbl %al,%eax
  800b76:	29 c2                	sub    %eax,%edx
  800b78:	89 d0                	mov    %edx,%eax
}
  800b7a:	5d                   	pop    %ebp
  800b7b:	c3                   	ret    

00800b7c <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800b7c:	55                   	push   %ebp
  800b7d:	89 e5                	mov    %esp,%ebp
  800b7f:	83 ec 04             	sub    $0x4,%esp
  800b82:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b85:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800b88:	eb 14                	jmp    800b9e <strchr+0x22>
		if (*s == c)
  800b8a:	8b 45 08             	mov    0x8(%ebp),%eax
  800b8d:	0f b6 00             	movzbl (%eax),%eax
  800b90:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800b93:	75 05                	jne    800b9a <strchr+0x1e>
			return (char *) s;
  800b95:	8b 45 08             	mov    0x8(%ebp),%eax
  800b98:	eb 13                	jmp    800bad <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800b9a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b9e:	8b 45 08             	mov    0x8(%ebp),%eax
  800ba1:	0f b6 00             	movzbl (%eax),%eax
  800ba4:	84 c0                	test   %al,%al
  800ba6:	75 e2                	jne    800b8a <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800ba8:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800bad:	c9                   	leave  
  800bae:	c3                   	ret    

00800baf <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800baf:	55                   	push   %ebp
  800bb0:	89 e5                	mov    %esp,%ebp
  800bb2:	83 ec 04             	sub    $0x4,%esp
  800bb5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bb8:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800bbb:	eb 11                	jmp    800bce <strfind+0x1f>
		if (*s == c)
  800bbd:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc0:	0f b6 00             	movzbl (%eax),%eax
  800bc3:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800bc6:	75 02                	jne    800bca <strfind+0x1b>
			break;
  800bc8:	eb 0e                	jmp    800bd8 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800bca:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bce:	8b 45 08             	mov    0x8(%ebp),%eax
  800bd1:	0f b6 00             	movzbl (%eax),%eax
  800bd4:	84 c0                	test   %al,%al
  800bd6:	75 e5                	jne    800bbd <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
  800bd8:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bdb:	c9                   	leave  
  800bdc:	c3                   	ret    

00800bdd <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800bdd:	55                   	push   %ebp
  800bde:	89 e5                	mov    %esp,%ebp
  800be0:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800be3:	8b 45 08             	mov    0x8(%ebp),%eax
  800be6:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800be9:	8b 45 10             	mov    0x10(%ebp),%eax
  800bec:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800bef:	eb 0e                	jmp    800bff <memset+0x22>
		*p++ = c;
  800bf1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bf4:	8d 50 01             	lea    0x1(%eax),%edx
  800bf7:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800bfa:	8b 55 0c             	mov    0xc(%ebp),%edx
  800bfd:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800bff:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800c03:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800c07:	79 e8                	jns    800bf1 <memset+0x14>
		*p++ = c;

	return v;
  800c09:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c0c:	c9                   	leave  
  800c0d:	c3                   	ret    

00800c0e <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800c0e:	55                   	push   %ebp
  800c0f:	89 e5                	mov    %esp,%ebp
  800c11:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800c14:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c17:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800c1a:	8b 45 08             	mov    0x8(%ebp),%eax
  800c1d:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800c20:	eb 17                	jmp    800c39 <memcpy+0x2b>
		*d++ = *s++;
  800c22:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c25:	8d 50 01             	lea    0x1(%eax),%edx
  800c28:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800c2b:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c2e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800c31:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800c34:	0f b6 12             	movzbl (%edx),%edx
  800c37:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800c39:	8b 45 10             	mov    0x10(%ebp),%eax
  800c3c:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c3f:	89 55 10             	mov    %edx,0x10(%ebp)
  800c42:	85 c0                	test   %eax,%eax
  800c44:	75 dc                	jne    800c22 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800c46:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c49:	c9                   	leave  
  800c4a:	c3                   	ret    

00800c4b <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800c4b:	55                   	push   %ebp
  800c4c:	89 e5                	mov    %esp,%ebp
  800c4e:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800c51:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c54:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800c57:	8b 45 08             	mov    0x8(%ebp),%eax
  800c5a:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800c5d:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c60:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c63:	73 3d                	jae    800ca2 <memmove+0x57>
  800c65:	8b 45 10             	mov    0x10(%ebp),%eax
  800c68:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c6b:	01 d0                	add    %edx,%eax
  800c6d:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c70:	76 30                	jbe    800ca2 <memmove+0x57>
		s += n;
  800c72:	8b 45 10             	mov    0x10(%ebp),%eax
  800c75:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800c78:	8b 45 10             	mov    0x10(%ebp),%eax
  800c7b:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800c7e:	eb 13                	jmp    800c93 <memmove+0x48>
			*--d = *--s;
  800c80:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800c84:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800c88:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c8b:	0f b6 10             	movzbl (%eax),%edx
  800c8e:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c91:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800c93:	8b 45 10             	mov    0x10(%ebp),%eax
  800c96:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c99:	89 55 10             	mov    %edx,0x10(%ebp)
  800c9c:	85 c0                	test   %eax,%eax
  800c9e:	75 e0                	jne    800c80 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ca0:	eb 26                	jmp    800cc8 <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800ca2:	eb 17                	jmp    800cbb <memmove+0x70>
			*d++ = *s++;
  800ca4:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ca7:	8d 50 01             	lea    0x1(%eax),%edx
  800caa:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800cad:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800cb0:	8d 4a 01             	lea    0x1(%edx),%ecx
  800cb3:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800cb6:	0f b6 12             	movzbl (%edx),%edx
  800cb9:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800cbb:	8b 45 10             	mov    0x10(%ebp),%eax
  800cbe:	8d 50 ff             	lea    -0x1(%eax),%edx
  800cc1:	89 55 10             	mov    %edx,0x10(%ebp)
  800cc4:	85 c0                	test   %eax,%eax
  800cc6:	75 dc                	jne    800ca4 <memmove+0x59>
			*d++ = *s++;

	return dst;
  800cc8:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ccb:	c9                   	leave  
  800ccc:	c3                   	ret    

00800ccd <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800ccd:	55                   	push   %ebp
  800cce:	89 e5                	mov    %esp,%ebp
  800cd0:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800cd3:	8b 45 08             	mov    0x8(%ebp),%eax
  800cd6:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800cd9:	8b 45 0c             	mov    0xc(%ebp),%eax
  800cdc:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800cdf:	eb 30                	jmp    800d11 <memcmp+0x44>
		if (*s1 != *s2)
  800ce1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ce4:	0f b6 10             	movzbl (%eax),%edx
  800ce7:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cea:	0f b6 00             	movzbl (%eax),%eax
  800ced:	38 c2                	cmp    %al,%dl
  800cef:	74 18                	je     800d09 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800cf1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800cf4:	0f b6 00             	movzbl (%eax),%eax
  800cf7:	0f b6 d0             	movzbl %al,%edx
  800cfa:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cfd:	0f b6 00             	movzbl (%eax),%eax
  800d00:	0f b6 c0             	movzbl %al,%eax
  800d03:	29 c2                	sub    %eax,%edx
  800d05:	89 d0                	mov    %edx,%eax
  800d07:	eb 1a                	jmp    800d23 <memcmp+0x56>
		s1++, s2++;
  800d09:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800d0d:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800d11:	8b 45 10             	mov    0x10(%ebp),%eax
  800d14:	8d 50 ff             	lea    -0x1(%eax),%edx
  800d17:	89 55 10             	mov    %edx,0x10(%ebp)
  800d1a:	85 c0                	test   %eax,%eax
  800d1c:	75 c3                	jne    800ce1 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800d1e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800d23:	c9                   	leave  
  800d24:	c3                   	ret    

00800d25 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800d25:	55                   	push   %ebp
  800d26:	89 e5                	mov    %esp,%ebp
  800d28:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800d2b:	8b 45 10             	mov    0x10(%ebp),%eax
  800d2e:	8b 55 08             	mov    0x8(%ebp),%edx
  800d31:	01 d0                	add    %edx,%eax
  800d33:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800d36:	eb 13                	jmp    800d4b <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
  800d38:	8b 45 08             	mov    0x8(%ebp),%eax
  800d3b:	0f b6 10             	movzbl (%eax),%edx
  800d3e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d41:	38 c2                	cmp    %al,%dl
  800d43:	75 02                	jne    800d47 <memfind+0x22>
			break;
  800d45:	eb 0c                	jmp    800d53 <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800d47:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d4b:	8b 45 08             	mov    0x8(%ebp),%eax
  800d4e:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800d51:	72 e5                	jb     800d38 <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
  800d53:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800d56:	c9                   	leave  
  800d57:	c3                   	ret    

00800d58 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800d58:	55                   	push   %ebp
  800d59:	89 e5                	mov    %esp,%ebp
  800d5b:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800d5e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800d65:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d6c:	eb 04                	jmp    800d72 <strtol+0x1a>
		s++;
  800d6e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d72:	8b 45 08             	mov    0x8(%ebp),%eax
  800d75:	0f b6 00             	movzbl (%eax),%eax
  800d78:	3c 20                	cmp    $0x20,%al
  800d7a:	74 f2                	je     800d6e <strtol+0x16>
  800d7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800d7f:	0f b6 00             	movzbl (%eax),%eax
  800d82:	3c 09                	cmp    $0x9,%al
  800d84:	74 e8                	je     800d6e <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800d86:	8b 45 08             	mov    0x8(%ebp),%eax
  800d89:	0f b6 00             	movzbl (%eax),%eax
  800d8c:	3c 2b                	cmp    $0x2b,%al
  800d8e:	75 06                	jne    800d96 <strtol+0x3e>
		s++;
  800d90:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d94:	eb 15                	jmp    800dab <strtol+0x53>
	else if (*s == '-')
  800d96:	8b 45 08             	mov    0x8(%ebp),%eax
  800d99:	0f b6 00             	movzbl (%eax),%eax
  800d9c:	3c 2d                	cmp    $0x2d,%al
  800d9e:	75 0b                	jne    800dab <strtol+0x53>
		s++, neg = 1;
  800da0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800da4:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800dab:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800daf:	74 06                	je     800db7 <strtol+0x5f>
  800db1:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800db5:	75 24                	jne    800ddb <strtol+0x83>
  800db7:	8b 45 08             	mov    0x8(%ebp),%eax
  800dba:	0f b6 00             	movzbl (%eax),%eax
  800dbd:	3c 30                	cmp    $0x30,%al
  800dbf:	75 1a                	jne    800ddb <strtol+0x83>
  800dc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc4:	83 c0 01             	add    $0x1,%eax
  800dc7:	0f b6 00             	movzbl (%eax),%eax
  800dca:	3c 78                	cmp    $0x78,%al
  800dcc:	75 0d                	jne    800ddb <strtol+0x83>
		s += 2, base = 16;
  800dce:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800dd2:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800dd9:	eb 2a                	jmp    800e05 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800ddb:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ddf:	75 17                	jne    800df8 <strtol+0xa0>
  800de1:	8b 45 08             	mov    0x8(%ebp),%eax
  800de4:	0f b6 00             	movzbl (%eax),%eax
  800de7:	3c 30                	cmp    $0x30,%al
  800de9:	75 0d                	jne    800df8 <strtol+0xa0>
		s++, base = 8;
  800deb:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800def:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800df6:	eb 0d                	jmp    800e05 <strtol+0xad>
	else if (base == 0)
  800df8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800dfc:	75 07                	jne    800e05 <strtol+0xad>
		base = 10;
  800dfe:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800e05:	8b 45 08             	mov    0x8(%ebp),%eax
  800e08:	0f b6 00             	movzbl (%eax),%eax
  800e0b:	3c 2f                	cmp    $0x2f,%al
  800e0d:	7e 1b                	jle    800e2a <strtol+0xd2>
  800e0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e12:	0f b6 00             	movzbl (%eax),%eax
  800e15:	3c 39                	cmp    $0x39,%al
  800e17:	7f 11                	jg     800e2a <strtol+0xd2>
			dig = *s - '0';
  800e19:	8b 45 08             	mov    0x8(%ebp),%eax
  800e1c:	0f b6 00             	movzbl (%eax),%eax
  800e1f:	0f be c0             	movsbl %al,%eax
  800e22:	83 e8 30             	sub    $0x30,%eax
  800e25:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800e28:	eb 48                	jmp    800e72 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800e2a:	8b 45 08             	mov    0x8(%ebp),%eax
  800e2d:	0f b6 00             	movzbl (%eax),%eax
  800e30:	3c 60                	cmp    $0x60,%al
  800e32:	7e 1b                	jle    800e4f <strtol+0xf7>
  800e34:	8b 45 08             	mov    0x8(%ebp),%eax
  800e37:	0f b6 00             	movzbl (%eax),%eax
  800e3a:	3c 7a                	cmp    $0x7a,%al
  800e3c:	7f 11                	jg     800e4f <strtol+0xf7>
			dig = *s - 'a' + 10;
  800e3e:	8b 45 08             	mov    0x8(%ebp),%eax
  800e41:	0f b6 00             	movzbl (%eax),%eax
  800e44:	0f be c0             	movsbl %al,%eax
  800e47:	83 e8 57             	sub    $0x57,%eax
  800e4a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800e4d:	eb 23                	jmp    800e72 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800e4f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e52:	0f b6 00             	movzbl (%eax),%eax
  800e55:	3c 40                	cmp    $0x40,%al
  800e57:	7e 3d                	jle    800e96 <strtol+0x13e>
  800e59:	8b 45 08             	mov    0x8(%ebp),%eax
  800e5c:	0f b6 00             	movzbl (%eax),%eax
  800e5f:	3c 5a                	cmp    $0x5a,%al
  800e61:	7f 33                	jg     800e96 <strtol+0x13e>
			dig = *s - 'A' + 10;
  800e63:	8b 45 08             	mov    0x8(%ebp),%eax
  800e66:	0f b6 00             	movzbl (%eax),%eax
  800e69:	0f be c0             	movsbl %al,%eax
  800e6c:	83 e8 37             	sub    $0x37,%eax
  800e6f:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800e72:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e75:	3b 45 10             	cmp    0x10(%ebp),%eax
  800e78:	7c 02                	jl     800e7c <strtol+0x124>
			break;
  800e7a:	eb 1a                	jmp    800e96 <strtol+0x13e>
		s++, val = (val * base) + dig;
  800e7c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800e80:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800e83:	0f af 45 10          	imul   0x10(%ebp),%eax
  800e87:	89 c2                	mov    %eax,%edx
  800e89:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e8c:	01 d0                	add    %edx,%eax
  800e8e:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800e91:	e9 6f ff ff ff       	jmp    800e05 <strtol+0xad>

	if (endptr)
  800e96:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800e9a:	74 08                	je     800ea4 <strtol+0x14c>
		*endptr = (char *) s;
  800e9c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e9f:	8b 55 08             	mov    0x8(%ebp),%edx
  800ea2:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800ea4:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800ea8:	74 07                	je     800eb1 <strtol+0x159>
  800eaa:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ead:	f7 d8                	neg    %eax
  800eaf:	eb 03                	jmp    800eb4 <strtol+0x15c>
  800eb1:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800eb4:	c9                   	leave  
  800eb5:	c3                   	ret    

00800eb6 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800eb6:	55                   	push   %ebp
  800eb7:	89 e5                	mov    %esp,%ebp
  800eb9:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800ebc:	8b 45 14             	mov    0x14(%ebp),%eax
  800ebf:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800ec5:	8b 45 14             	mov    0x14(%ebp),%eax
  800ec8:	8b 00                	mov    (%eax),%eax
  800eca:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ed1:	8b 45 10             	mov    0x10(%ebp),%eax
  800ed4:	01 d0                	add    %edx,%eax
  800ed6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800edc:	eb 0c                	jmp    800eea <strsplit+0x34>
			*string++ = 0;
  800ede:	8b 45 08             	mov    0x8(%ebp),%eax
  800ee1:	8d 50 01             	lea    0x1(%eax),%edx
  800ee4:	89 55 08             	mov    %edx,0x8(%ebp)
  800ee7:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800eea:	8b 45 08             	mov    0x8(%ebp),%eax
  800eed:	0f b6 00             	movzbl (%eax),%eax
  800ef0:	84 c0                	test   %al,%al
  800ef2:	74 1c                	je     800f10 <strsplit+0x5a>
  800ef4:	8b 45 08             	mov    0x8(%ebp),%eax
  800ef7:	0f b6 00             	movzbl (%eax),%eax
  800efa:	0f be c0             	movsbl %al,%eax
  800efd:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f01:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f04:	89 04 24             	mov    %eax,(%esp)
  800f07:	e8 70 fc ff ff       	call   800b7c <strchr>
  800f0c:	85 c0                	test   %eax,%eax
  800f0e:	75 ce                	jne    800ede <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800f10:	8b 45 08             	mov    0x8(%ebp),%eax
  800f13:	0f b6 00             	movzbl (%eax),%eax
  800f16:	84 c0                	test   %al,%al
  800f18:	75 1f                	jne    800f39 <strsplit+0x83>
			break;
  800f1a:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800f1b:	8b 45 14             	mov    0x14(%ebp),%eax
  800f1e:	8b 00                	mov    (%eax),%eax
  800f20:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f27:	8b 45 10             	mov    0x10(%ebp),%eax
  800f2a:	01 d0                	add    %edx,%eax
  800f2c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800f32:	b8 01 00 00 00       	mov    $0x1,%eax
  800f37:	eb 61                	jmp    800f9a <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800f39:	8b 45 14             	mov    0x14(%ebp),%eax
  800f3c:	8b 00                	mov    (%eax),%eax
  800f3e:	83 f8 0f             	cmp    $0xf,%eax
  800f41:	75 07                	jne    800f4a <strsplit+0x94>
		{
			return 0;
  800f43:	b8 00 00 00 00       	mov    $0x0,%eax
  800f48:	eb 50                	jmp    800f9a <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800f4a:	8b 45 14             	mov    0x14(%ebp),%eax
  800f4d:	8b 00                	mov    (%eax),%eax
  800f4f:	8d 48 01             	lea    0x1(%eax),%ecx
  800f52:	8b 55 14             	mov    0x14(%ebp),%edx
  800f55:	89 0a                	mov    %ecx,(%edx)
  800f57:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f5e:	8b 45 10             	mov    0x10(%ebp),%eax
  800f61:	01 c2                	add    %eax,%edx
  800f63:	8b 45 08             	mov    0x8(%ebp),%eax
  800f66:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800f68:	eb 04                	jmp    800f6e <strsplit+0xb8>
			string++;
  800f6a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800f6e:	8b 45 08             	mov    0x8(%ebp),%eax
  800f71:	0f b6 00             	movzbl (%eax),%eax
  800f74:	84 c0                	test   %al,%al
  800f76:	74 1c                	je     800f94 <strsplit+0xde>
  800f78:	8b 45 08             	mov    0x8(%ebp),%eax
  800f7b:	0f b6 00             	movzbl (%eax),%eax
  800f7e:	0f be c0             	movsbl %al,%eax
  800f81:	89 44 24 04          	mov    %eax,0x4(%esp)
  800f85:	8b 45 0c             	mov    0xc(%ebp),%eax
  800f88:	89 04 24             	mov    %eax,(%esp)
  800f8b:	e8 ec fb ff ff       	call   800b7c <strchr>
  800f90:	85 c0                	test   %eax,%eax
  800f92:	74 d6                	je     800f6a <strsplit+0xb4>
			string++;
	}
  800f94:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800f95:	e9 50 ff ff ff       	jmp    800eea <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
  800f9a:	c9                   	leave  
  800f9b:	c3                   	ret    

00800f9c <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800f9c:	55                   	push   %ebp
  800f9d:	89 e5                	mov    %esp,%ebp
  800f9f:	57                   	push   %edi
  800fa0:	56                   	push   %esi
  800fa1:	53                   	push   %ebx
  800fa2:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800fa5:	8b 45 08             	mov    0x8(%ebp),%eax
  800fa8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fab:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800fae:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800fb1:	8b 7d 18             	mov    0x18(%ebp),%edi
  800fb4:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800fb7:	cd 30                	int    $0x30
  800fb9:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800fbc:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800fbf:	83 c4 10             	add    $0x10,%esp
  800fc2:	5b                   	pop    %ebx
  800fc3:	5e                   	pop    %esi
  800fc4:	5f                   	pop    %edi
  800fc5:	5d                   	pop    %ebp
  800fc6:	c3                   	ret    

00800fc7 <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800fc7:	55                   	push   %ebp
  800fc8:	89 e5                	mov    %esp,%ebp
  800fca:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800fcd:	8b 45 08             	mov    0x8(%ebp),%eax
  800fd0:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  800fd7:	00 
  800fd8:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  800fdf:	00 
  800fe0:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  800fe7:	00 
  800fe8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800feb:	89 54 24 08          	mov    %edx,0x8(%esp)
  800fef:	89 44 24 04          	mov    %eax,0x4(%esp)
  800ff3:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  800ffa:	e8 9d ff ff ff       	call   800f9c <syscall>
}
  800fff:	c9                   	leave  
  801000:	c3                   	ret    

00801001 <sys_cgetc>:

int
sys_cgetc(void)
{
  801001:	55                   	push   %ebp
  801002:	89 e5                	mov    %esp,%ebp
  801004:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  801007:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80100e:	00 
  80100f:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801016:	00 
  801017:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80101e:	00 
  80101f:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801026:	00 
  801027:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  80102e:	00 
  80102f:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  801036:	e8 61 ff ff ff       	call   800f9c <syscall>
}
  80103b:	c9                   	leave  
  80103c:	c3                   	ret    

0080103d <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  80103d:	55                   	push   %ebp
  80103e:	89 e5                	mov    %esp,%ebp
  801040:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  801043:	8b 45 08             	mov    0x8(%ebp),%eax
  801046:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80104d:	00 
  80104e:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801055:	00 
  801056:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80105d:	00 
  80105e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  801065:	00 
  801066:	89 44 24 04          	mov    %eax,0x4(%esp)
  80106a:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  801071:	e8 26 ff ff ff       	call   800f9c <syscall>
}
  801076:	c9                   	leave  
  801077:	c3                   	ret    

00801078 <sys_getenvid>:

int32 sys_getenvid(void)
{
  801078:	55                   	push   %ebp
  801079:	89 e5                	mov    %esp,%ebp
  80107b:	83 ec 18             	sub    $0x18,%esp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  80107e:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801085:	00 
  801086:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80108d:	00 
  80108e:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801095:	00 
  801096:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  80109d:	00 
  80109e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  8010a5:	00 
  8010a6:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  8010ad:	e8 ea fe ff ff       	call   800f9c <syscall>
}
  8010b2:	c9                   	leave  
  8010b3:	c3                   	ret    

008010b4 <sys_env_sleep>:

void sys_env_sleep(void)
{
  8010b4:	55                   	push   %ebp
  8010b5:	89 e5                	mov    %esp,%ebp
  8010b7:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  8010ba:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8010c1:	00 
  8010c2:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8010c9:	00 
  8010ca:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8010d1:	00 
  8010d2:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  8010d9:	00 
  8010da:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  8010e1:	00 
  8010e2:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  8010e9:	e8 ae fe ff ff       	call   800f9c <syscall>
}
  8010ee:	c9                   	leave  
  8010ef:	c3                   	ret    

008010f0 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  8010f0:	55                   	push   %ebp
  8010f1:	89 e5                	mov    %esp,%ebp
  8010f3:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  8010f6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8010f9:	8b 45 08             	mov    0x8(%ebp),%eax
  8010fc:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801103:	00 
  801104:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80110b:	00 
  80110c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801113:	00 
  801114:	89 54 24 08          	mov    %edx,0x8(%esp)
  801118:	89 44 24 04          	mov    %eax,0x4(%esp)
  80111c:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  801123:	e8 74 fe ff ff       	call   800f9c <syscall>
}
  801128:	c9                   	leave  
  801129:	c3                   	ret    

0080112a <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  80112a:	55                   	push   %ebp
  80112b:	89 e5                	mov    %esp,%ebp
  80112d:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  801130:	8b 55 0c             	mov    0xc(%ebp),%edx
  801133:	8b 45 08             	mov    0x8(%ebp),%eax
  801136:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  80113d:	00 
  80113e:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  801145:	00 
  801146:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  80114d:	00 
  80114e:	89 54 24 08          	mov    %edx,0x8(%esp)
  801152:	89 44 24 04          	mov    %eax,0x4(%esp)
  801156:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
  80115d:	e8 3a fe ff ff       	call   800f9c <syscall>
}
  801162:	c9                   	leave  
  801163:	c3                   	ret    

00801164 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  801164:	55                   	push   %ebp
  801165:	89 e5                	mov    %esp,%ebp
  801167:	56                   	push   %esi
  801168:	53                   	push   %ebx
  801169:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  80116c:	8b 75 18             	mov    0x18(%ebp),%esi
  80116f:	8b 5d 14             	mov    0x14(%ebp),%ebx
  801172:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801175:	8b 55 0c             	mov    0xc(%ebp),%edx
  801178:	8b 45 08             	mov    0x8(%ebp),%eax
  80117b:	89 74 24 14          	mov    %esi,0x14(%esp)
  80117f:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  801183:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  801187:	89 54 24 08          	mov    %edx,0x8(%esp)
  80118b:	89 44 24 04          	mov    %eax,0x4(%esp)
  80118f:	c7 04 24 07 00 00 00 	movl   $0x7,(%esp)
  801196:	e8 01 fe ff ff       	call   800f9c <syscall>
}
  80119b:	83 c4 18             	add    $0x18,%esp
  80119e:	5b                   	pop    %ebx
  80119f:	5e                   	pop    %esi
  8011a0:	5d                   	pop    %ebp
  8011a1:	c3                   	ret    

008011a2 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  8011a2:	55                   	push   %ebp
  8011a3:	89 e5                	mov    %esp,%ebp
  8011a5:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  8011a8:	8b 55 0c             	mov    0xc(%ebp),%edx
  8011ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8011ae:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8011b5:	00 
  8011b6:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8011bd:	00 
  8011be:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8011c5:	00 
  8011c6:	89 54 24 08          	mov    %edx,0x8(%esp)
  8011ca:	89 44 24 04          	mov    %eax,0x4(%esp)
  8011ce:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  8011d5:	e8 c2 fd ff ff       	call   800f9c <syscall>
}
  8011da:	c9                   	leave  
  8011db:	c3                   	ret    

008011dc <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  8011dc:	55                   	push   %ebp
  8011dd:	89 e5                	mov    %esp,%ebp
  8011df:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  8011e2:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  8011e9:	00 
  8011ea:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  8011f1:	00 
  8011f2:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  8011f9:	00 
  8011fa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8011fd:	89 44 24 08          	mov    %eax,0x8(%esp)
  801201:	8b 45 08             	mov    0x8(%ebp),%eax
  801204:	89 44 24 04          	mov    %eax,0x4(%esp)
  801208:	c7 04 24 09 00 00 00 	movl   $0x9,(%esp)
  80120f:	e8 88 fd ff ff       	call   800f9c <syscall>
}
  801214:	c9                   	leave  
  801215:	c3                   	ret    

00801216 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  801216:	55                   	push   %ebp
  801217:	89 e5                	mov    %esp,%ebp
  801219:	83 ec 18             	sub    $0x18,%esp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  80121c:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801223:	00 
  801224:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80122b:	00 
  80122c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801233:	00 
  801234:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  80123b:	00 
  80123c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  801243:	00 
  801244:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  80124b:	e8 4c fd ff ff       	call   800f9c <syscall>
}
  801250:	c9                   	leave  
  801251:	c3                   	ret    

00801252 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  801252:	55                   	push   %ebp
  801253:	89 e5                	mov    %esp,%ebp
  801255:	83 ec 18             	sub    $0x18,%esp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  801258:	8b 45 08             	mov    0x8(%ebp),%eax
  80125b:	c7 44 24 14 00 00 00 	movl   $0x0,0x14(%esp)
  801262:	00 
  801263:	c7 44 24 10 00 00 00 	movl   $0x0,0x10(%esp)
  80126a:	00 
  80126b:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  801272:	00 
  801273:	8b 55 0c             	mov    0xc(%ebp),%edx
  801276:	89 54 24 08          	mov    %edx,0x8(%esp)
  80127a:	89 44 24 04          	mov    %eax,0x4(%esp)
  80127e:	c7 04 24 0b 00 00 00 	movl   $0xb,(%esp)
  801285:	e8 12 fd ff ff       	call   800f9c <syscall>
	return;
  80128a:	90                   	nop
}
  80128b:	c9                   	leave  
  80128c:	c3                   	ret    

0080128d <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  80128d:	55                   	push   %ebp
  80128e:	89 e5                	mov    %esp,%ebp
  801290:	83 ec 28             	sub    $0x28,%esp
	char c = ch;
  801293:	8b 45 08             	mov    0x8(%ebp),%eax
  801296:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801299:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  8012a0:	00 
  8012a1:	8d 45 f7             	lea    -0x9(%ebp),%eax
  8012a4:	89 04 24             	mov    %eax,(%esp)
  8012a7:	e8 1b fd ff ff       	call   800fc7 <sys_cputs>
}
  8012ac:	c9                   	leave  
  8012ad:	c3                   	ret    

008012ae <getchar>:

int
getchar(void)
{
  8012ae:	55                   	push   %ebp
  8012af:	89 e5                	mov    %esp,%ebp
  8012b1:	83 ec 08             	sub    $0x8,%esp
	return sys_cgetc();
  8012b4:	e8 48 fd ff ff       	call   801001 <sys_cgetc>
}
  8012b9:	c9                   	leave  
  8012ba:	c3                   	ret    

008012bb <iscons>:


int iscons(int fdnum)
{
  8012bb:	55                   	push   %ebp
  8012bc:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
  8012be:	b8 01 00 00 00       	mov    $0x1,%eax
}
  8012c3:	5d                   	pop    %ebp
  8012c4:	c3                   	ret    
  8012c5:	66 90                	xchg   %ax,%ax
  8012c7:	66 90                	xchg   %ax,%ax
  8012c9:	66 90                	xchg   %ax,%ax
  8012cb:	66 90                	xchg   %ax,%ax
  8012cd:	66 90                	xchg   %ax,%ax
  8012cf:	90                   	nop

008012d0 <__udivdi3>:
  8012d0:	55                   	push   %ebp
  8012d1:	57                   	push   %edi
  8012d2:	56                   	push   %esi
  8012d3:	83 ec 0c             	sub    $0xc,%esp
  8012d6:	8b 44 24 28          	mov    0x28(%esp),%eax
  8012da:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
  8012de:	8b 6c 24 20          	mov    0x20(%esp),%ebp
  8012e2:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  8012e6:	85 c0                	test   %eax,%eax
  8012e8:	89 7c 24 04          	mov    %edi,0x4(%esp)
  8012ec:	89 ea                	mov    %ebp,%edx
  8012ee:	89 0c 24             	mov    %ecx,(%esp)
  8012f1:	75 2d                	jne    801320 <__udivdi3+0x50>
  8012f3:	39 e9                	cmp    %ebp,%ecx
  8012f5:	77 61                	ja     801358 <__udivdi3+0x88>
  8012f7:	85 c9                	test   %ecx,%ecx
  8012f9:	89 ce                	mov    %ecx,%esi
  8012fb:	75 0b                	jne    801308 <__udivdi3+0x38>
  8012fd:	b8 01 00 00 00       	mov    $0x1,%eax
  801302:	31 d2                	xor    %edx,%edx
  801304:	f7 f1                	div    %ecx
  801306:	89 c6                	mov    %eax,%esi
  801308:	31 d2                	xor    %edx,%edx
  80130a:	89 e8                	mov    %ebp,%eax
  80130c:	f7 f6                	div    %esi
  80130e:	89 c5                	mov    %eax,%ebp
  801310:	89 f8                	mov    %edi,%eax
  801312:	f7 f6                	div    %esi
  801314:	89 ea                	mov    %ebp,%edx
  801316:	83 c4 0c             	add    $0xc,%esp
  801319:	5e                   	pop    %esi
  80131a:	5f                   	pop    %edi
  80131b:	5d                   	pop    %ebp
  80131c:	c3                   	ret    
  80131d:	8d 76 00             	lea    0x0(%esi),%esi
  801320:	39 e8                	cmp    %ebp,%eax
  801322:	77 24                	ja     801348 <__udivdi3+0x78>
  801324:	0f bd e8             	bsr    %eax,%ebp
  801327:	83 f5 1f             	xor    $0x1f,%ebp
  80132a:	75 3c                	jne    801368 <__udivdi3+0x98>
  80132c:	8b 74 24 04          	mov    0x4(%esp),%esi
  801330:	39 34 24             	cmp    %esi,(%esp)
  801333:	0f 86 9f 00 00 00    	jbe    8013d8 <__udivdi3+0x108>
  801339:	39 d0                	cmp    %edx,%eax
  80133b:	0f 82 97 00 00 00    	jb     8013d8 <__udivdi3+0x108>
  801341:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801348:	31 d2                	xor    %edx,%edx
  80134a:	31 c0                	xor    %eax,%eax
  80134c:	83 c4 0c             	add    $0xc,%esp
  80134f:	5e                   	pop    %esi
  801350:	5f                   	pop    %edi
  801351:	5d                   	pop    %ebp
  801352:	c3                   	ret    
  801353:	90                   	nop
  801354:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801358:	89 f8                	mov    %edi,%eax
  80135a:	f7 f1                	div    %ecx
  80135c:	31 d2                	xor    %edx,%edx
  80135e:	83 c4 0c             	add    $0xc,%esp
  801361:	5e                   	pop    %esi
  801362:	5f                   	pop    %edi
  801363:	5d                   	pop    %ebp
  801364:	c3                   	ret    
  801365:	8d 76 00             	lea    0x0(%esi),%esi
  801368:	89 e9                	mov    %ebp,%ecx
  80136a:	8b 3c 24             	mov    (%esp),%edi
  80136d:	d3 e0                	shl    %cl,%eax
  80136f:	89 c6                	mov    %eax,%esi
  801371:	b8 20 00 00 00       	mov    $0x20,%eax
  801376:	29 e8                	sub    %ebp,%eax
  801378:	89 c1                	mov    %eax,%ecx
  80137a:	d3 ef                	shr    %cl,%edi
  80137c:	89 e9                	mov    %ebp,%ecx
  80137e:	89 7c 24 08          	mov    %edi,0x8(%esp)
  801382:	8b 3c 24             	mov    (%esp),%edi
  801385:	09 74 24 08          	or     %esi,0x8(%esp)
  801389:	89 d6                	mov    %edx,%esi
  80138b:	d3 e7                	shl    %cl,%edi
  80138d:	89 c1                	mov    %eax,%ecx
  80138f:	89 3c 24             	mov    %edi,(%esp)
  801392:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801396:	d3 ee                	shr    %cl,%esi
  801398:	89 e9                	mov    %ebp,%ecx
  80139a:	d3 e2                	shl    %cl,%edx
  80139c:	89 c1                	mov    %eax,%ecx
  80139e:	d3 ef                	shr    %cl,%edi
  8013a0:	09 d7                	or     %edx,%edi
  8013a2:	89 f2                	mov    %esi,%edx
  8013a4:	89 f8                	mov    %edi,%eax
  8013a6:	f7 74 24 08          	divl   0x8(%esp)
  8013aa:	89 d6                	mov    %edx,%esi
  8013ac:	89 c7                	mov    %eax,%edi
  8013ae:	f7 24 24             	mull   (%esp)
  8013b1:	39 d6                	cmp    %edx,%esi
  8013b3:	89 14 24             	mov    %edx,(%esp)
  8013b6:	72 30                	jb     8013e8 <__udivdi3+0x118>
  8013b8:	8b 54 24 04          	mov    0x4(%esp),%edx
  8013bc:	89 e9                	mov    %ebp,%ecx
  8013be:	d3 e2                	shl    %cl,%edx
  8013c0:	39 c2                	cmp    %eax,%edx
  8013c2:	73 05                	jae    8013c9 <__udivdi3+0xf9>
  8013c4:	3b 34 24             	cmp    (%esp),%esi
  8013c7:	74 1f                	je     8013e8 <__udivdi3+0x118>
  8013c9:	89 f8                	mov    %edi,%eax
  8013cb:	31 d2                	xor    %edx,%edx
  8013cd:	e9 7a ff ff ff       	jmp    80134c <__udivdi3+0x7c>
  8013d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  8013d8:	31 d2                	xor    %edx,%edx
  8013da:	b8 01 00 00 00       	mov    $0x1,%eax
  8013df:	e9 68 ff ff ff       	jmp    80134c <__udivdi3+0x7c>
  8013e4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8013e8:	8d 47 ff             	lea    -0x1(%edi),%eax
  8013eb:	31 d2                	xor    %edx,%edx
  8013ed:	83 c4 0c             	add    $0xc,%esp
  8013f0:	5e                   	pop    %esi
  8013f1:	5f                   	pop    %edi
  8013f2:	5d                   	pop    %ebp
  8013f3:	c3                   	ret    
  8013f4:	66 90                	xchg   %ax,%ax
  8013f6:	66 90                	xchg   %ax,%ax
  8013f8:	66 90                	xchg   %ax,%ax
  8013fa:	66 90                	xchg   %ax,%ax
  8013fc:	66 90                	xchg   %ax,%ax
  8013fe:	66 90                	xchg   %ax,%ax

00801400 <__umoddi3>:
  801400:	55                   	push   %ebp
  801401:	57                   	push   %edi
  801402:	56                   	push   %esi
  801403:	83 ec 14             	sub    $0x14,%esp
  801406:	8b 44 24 28          	mov    0x28(%esp),%eax
  80140a:	8b 4c 24 24          	mov    0x24(%esp),%ecx
  80140e:	8b 74 24 2c          	mov    0x2c(%esp),%esi
  801412:	89 c7                	mov    %eax,%edi
  801414:	89 44 24 04          	mov    %eax,0x4(%esp)
  801418:	8b 44 24 30          	mov    0x30(%esp),%eax
  80141c:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  801420:	89 34 24             	mov    %esi,(%esp)
  801423:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801427:	85 c0                	test   %eax,%eax
  801429:	89 c2                	mov    %eax,%edx
  80142b:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  80142f:	75 17                	jne    801448 <__umoddi3+0x48>
  801431:	39 fe                	cmp    %edi,%esi
  801433:	76 4b                	jbe    801480 <__umoddi3+0x80>
  801435:	89 c8                	mov    %ecx,%eax
  801437:	89 fa                	mov    %edi,%edx
  801439:	f7 f6                	div    %esi
  80143b:	89 d0                	mov    %edx,%eax
  80143d:	31 d2                	xor    %edx,%edx
  80143f:	83 c4 14             	add    $0x14,%esp
  801442:	5e                   	pop    %esi
  801443:	5f                   	pop    %edi
  801444:	5d                   	pop    %ebp
  801445:	c3                   	ret    
  801446:	66 90                	xchg   %ax,%ax
  801448:	39 f8                	cmp    %edi,%eax
  80144a:	77 54                	ja     8014a0 <__umoddi3+0xa0>
  80144c:	0f bd e8             	bsr    %eax,%ebp
  80144f:	83 f5 1f             	xor    $0x1f,%ebp
  801452:	75 5c                	jne    8014b0 <__umoddi3+0xb0>
  801454:	8b 7c 24 08          	mov    0x8(%esp),%edi
  801458:	39 3c 24             	cmp    %edi,(%esp)
  80145b:	0f 87 e7 00 00 00    	ja     801548 <__umoddi3+0x148>
  801461:	8b 7c 24 04          	mov    0x4(%esp),%edi
  801465:	29 f1                	sub    %esi,%ecx
  801467:	19 c7                	sbb    %eax,%edi
  801469:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80146d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  801471:	8b 44 24 08          	mov    0x8(%esp),%eax
  801475:	8b 54 24 0c          	mov    0xc(%esp),%edx
  801479:	83 c4 14             	add    $0x14,%esp
  80147c:	5e                   	pop    %esi
  80147d:	5f                   	pop    %edi
  80147e:	5d                   	pop    %ebp
  80147f:	c3                   	ret    
  801480:	85 f6                	test   %esi,%esi
  801482:	89 f5                	mov    %esi,%ebp
  801484:	75 0b                	jne    801491 <__umoddi3+0x91>
  801486:	b8 01 00 00 00       	mov    $0x1,%eax
  80148b:	31 d2                	xor    %edx,%edx
  80148d:	f7 f6                	div    %esi
  80148f:	89 c5                	mov    %eax,%ebp
  801491:	8b 44 24 04          	mov    0x4(%esp),%eax
  801495:	31 d2                	xor    %edx,%edx
  801497:	f7 f5                	div    %ebp
  801499:	89 c8                	mov    %ecx,%eax
  80149b:	f7 f5                	div    %ebp
  80149d:	eb 9c                	jmp    80143b <__umoddi3+0x3b>
  80149f:	90                   	nop
  8014a0:	89 c8                	mov    %ecx,%eax
  8014a2:	89 fa                	mov    %edi,%edx
  8014a4:	83 c4 14             	add    $0x14,%esp
  8014a7:	5e                   	pop    %esi
  8014a8:	5f                   	pop    %edi
  8014a9:	5d                   	pop    %ebp
  8014aa:	c3                   	ret    
  8014ab:	90                   	nop
  8014ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8014b0:	8b 04 24             	mov    (%esp),%eax
  8014b3:	be 20 00 00 00       	mov    $0x20,%esi
  8014b8:	89 e9                	mov    %ebp,%ecx
  8014ba:	29 ee                	sub    %ebp,%esi
  8014bc:	d3 e2                	shl    %cl,%edx
  8014be:	89 f1                	mov    %esi,%ecx
  8014c0:	d3 e8                	shr    %cl,%eax
  8014c2:	89 e9                	mov    %ebp,%ecx
  8014c4:	89 44 24 04          	mov    %eax,0x4(%esp)
  8014c8:	8b 04 24             	mov    (%esp),%eax
  8014cb:	09 54 24 04          	or     %edx,0x4(%esp)
  8014cf:	89 fa                	mov    %edi,%edx
  8014d1:	d3 e0                	shl    %cl,%eax
  8014d3:	89 f1                	mov    %esi,%ecx
  8014d5:	89 44 24 08          	mov    %eax,0x8(%esp)
  8014d9:	8b 44 24 10          	mov    0x10(%esp),%eax
  8014dd:	d3 ea                	shr    %cl,%edx
  8014df:	89 e9                	mov    %ebp,%ecx
  8014e1:	d3 e7                	shl    %cl,%edi
  8014e3:	89 f1                	mov    %esi,%ecx
  8014e5:	d3 e8                	shr    %cl,%eax
  8014e7:	89 e9                	mov    %ebp,%ecx
  8014e9:	09 f8                	or     %edi,%eax
  8014eb:	8b 7c 24 10          	mov    0x10(%esp),%edi
  8014ef:	f7 74 24 04          	divl   0x4(%esp)
  8014f3:	d3 e7                	shl    %cl,%edi
  8014f5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
  8014f9:	89 d7                	mov    %edx,%edi
  8014fb:	f7 64 24 08          	mull   0x8(%esp)
  8014ff:	39 d7                	cmp    %edx,%edi
  801501:	89 c1                	mov    %eax,%ecx
  801503:	89 14 24             	mov    %edx,(%esp)
  801506:	72 2c                	jb     801534 <__umoddi3+0x134>
  801508:	39 44 24 0c          	cmp    %eax,0xc(%esp)
  80150c:	72 22                	jb     801530 <__umoddi3+0x130>
  80150e:	8b 44 24 0c          	mov    0xc(%esp),%eax
  801512:	29 c8                	sub    %ecx,%eax
  801514:	19 d7                	sbb    %edx,%edi
  801516:	89 e9                	mov    %ebp,%ecx
  801518:	89 fa                	mov    %edi,%edx
  80151a:	d3 e8                	shr    %cl,%eax
  80151c:	89 f1                	mov    %esi,%ecx
  80151e:	d3 e2                	shl    %cl,%edx
  801520:	89 e9                	mov    %ebp,%ecx
  801522:	d3 ef                	shr    %cl,%edi
  801524:	09 d0                	or     %edx,%eax
  801526:	89 fa                	mov    %edi,%edx
  801528:	83 c4 14             	add    $0x14,%esp
  80152b:	5e                   	pop    %esi
  80152c:	5f                   	pop    %edi
  80152d:	5d                   	pop    %ebp
  80152e:	c3                   	ret    
  80152f:	90                   	nop
  801530:	39 d7                	cmp    %edx,%edi
  801532:	75 da                	jne    80150e <__umoddi3+0x10e>
  801534:	8b 14 24             	mov    (%esp),%edx
  801537:	89 c1                	mov    %eax,%ecx
  801539:	2b 4c 24 08          	sub    0x8(%esp),%ecx
  80153d:	1b 54 24 04          	sbb    0x4(%esp),%edx
  801541:	eb cb                	jmp    80150e <__umoddi3+0x10e>
  801543:	90                   	nop
  801544:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801548:	3b 44 24 0c          	cmp    0xc(%esp),%eax
  80154c:	0f 82 0f ff ff ff    	jb     801461 <__umoddi3+0x61>
  801552:	e9 1a ff ff ff       	jmp    801471 <__umoddi3+0x71>
