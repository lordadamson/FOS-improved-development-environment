
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <start_of_kernel-0xc>:
.long MULTIBOOT_HEADER_FLAGS
.long CHECKSUM

.globl		start_of_kernel
start_of_kernel:
	movw	$0x1234,0x472			# warm boot
f0100000:	02 b0 ad 1b 03 00    	add    0x31bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fb                   	sti    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <start_of_kernel>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 

	# Establish our own GDT in place of the boot loader's temporary GDT.
	lgdt	RELOC(mygdtdesc)		# load descriptor table
f0100015:	0f 01 15 18 a0 11 00 	lgdtl  0x11a018

	# Immediately reload all segment registers (including CS!)
	# with segment selectors from the new GDT.
	movl	$DATA_SEL, %eax			# Data segment selector
f010001c:	b8 10 00 00 00       	mov    $0x10,%eax
	movw	%ax,%ds				# -> DS: Data Segment
f0100021:	8e d8                	mov    %eax,%ds
	movw	%ax,%es				# -> ES: Extra Segment
f0100023:	8e c0                	mov    %eax,%es
	movw	%ax,%ss				# -> SS: Stack Segment
f0100025:	8e d0                	mov    %eax,%ss
	ljmp	$CODE_SEL,$relocated		# reload CS by jumping
f0100027:	ea 2e 00 10 f0 08 00 	ljmp   $0x8,$0xf010002e

f010002e <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0,%ebp			# nuke frame pointer
f010002e:	bd 00 00 00 00       	mov    $0x0,%ebp

        # Leave a few words on the stack for the user trap frame
	movl	$(ptr_stack_top-SIZEOF_STRUCT_TRAPFRAME),%esp
f0100033:	bc bc 9f 11 f0       	mov    $0xf0119fbc,%esp

	# now to C code
	call	FOS_initialize
f0100038:	e8 02 00 00 00       	call   f010003f <FOS_initialize>

f010003d <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010003d:	eb fe                	jmp    f010003d <spin>

f010003f <FOS_initialize>:



//First ever function called in FOS kernel
void FOS_initialize()
{
f010003f:	55                   	push   %ebp
f0100040:	89 e5                	mov    %esp,%ebp
f0100042:	83 ec 18             	sub    $0x18,%esp
	extern char start_of_uninitialized_data_section[], end_of_kernel[];

	// Before doing anything else,
	// clear the uninitialized global data (BSS) section of our program, from start_of_uninitialized_data_section to end_of_kernel 
	// This ensures that all static/global variables start with zero value.
	memset(start_of_uninitialized_data_section, 0, end_of_kernel - start_of_uninitialized_data_section);
f0100045:	ba ac 63 14 f0       	mov    $0xf01463ac,%edx
f010004a:	b8 ab 58 14 f0       	mov    $0xf01458ab,%eax
f010004f:	29 c2                	sub    %eax,%edx
f0100051:	89 d0                	mov    %edx,%eax
f0100053:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100057:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f010005e:	00 
f010005f:	c7 04 24 ab 58 14 f0 	movl   $0xf01458ab,(%esp)
f0100066:	e8 93 47 00 00       	call   f01047fe <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	console_initialize();
f010006b:	e8 8b 08 00 00       	call   f01008fb <console_initialize>

	//print welcome message
	print_welcome_message();
f0100070:	e8 3d 00 00 00       	call   f01000b2 <print_welcome_message>

	// Lab 2 memory management initialization functions
	detect_memory();
f0100075:	e8 67 0b 00 00       	call   f0100be1 <detect_memory>
	initialize_kernel_VM();
f010007a:	e8 18 1c 00 00       	call   f0101c97 <initialize_kernel_VM>
	initialize_paging();
f010007f:	e8 6e 20 00 00       	call   f01020f2 <initialize_paging>
	page_check();
f0100084:	e8 aa 0f 00 00       	call   f0101033 <page_check>

	
	// Lab 3 user environment initialization functions
	env_init();
f0100089:	e8 9c 28 00 00       	call   f010292a <env_init>
	idt_init();
f010008e:	e8 c9 2f 00 00       	call   f010305c <idt_init>

	
	// start the kernel command prompt.
	while (1==1)
	{
		cprintf("\nWelcome to the FOS kernel command prompt!\n");
f0100093:	c7 04 24 60 4e 10 f0 	movl   $0xf0104e60,(%esp)
f010009a:	e8 6c 2f 00 00       	call   f010300b <cprintf>
		cprintf("Type 'help' for a list of commands.\n");	
f010009f:	c7 04 24 8c 4e 10 f0 	movl   $0xf0104e8c,(%esp)
f01000a6:	e8 60 2f 00 00       	call   f010300b <cprintf>
		run_command_prompt();
f01000ab:	e8 ad 08 00 00       	call   f010095d <run_command_prompt>
	}
f01000b0:	eb e1                	jmp    f0100093 <FOS_initialize+0x54>

f01000b2 <print_welcome_message>:
}


void print_welcome_message()
{
f01000b2:	55                   	push   %ebp
f01000b3:	89 e5                	mov    %esp,%ebp
f01000b5:	83 ec 18             	sub    $0x18,%esp
	cprintf("\n\n\n");
f01000b8:	c7 04 24 b1 4e 10 f0 	movl   $0xf0104eb1,(%esp)
f01000bf:	e8 47 2f 00 00       	call   f010300b <cprintf>
	cprintf("\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
f01000c4:	c7 04 24 b8 4e 10 f0 	movl   $0xf0104eb8,(%esp)
f01000cb:	e8 3b 2f 00 00       	call   f010300b <cprintf>
	cprintf("\t\t!!                                                             !!\n");
f01000d0:	c7 04 24 00 4f 10 f0 	movl   $0xf0104f00,(%esp)
f01000d7:	e8 2f 2f 00 00       	call   f010300b <cprintf>
	cprintf("\t\t!!                   !! FCIS says HELLO !!                     !!\n");
f01000dc:	c7 04 24 48 4f 10 f0 	movl   $0xf0104f48,(%esp)
f01000e3:	e8 23 2f 00 00       	call   f010300b <cprintf>
    cprintf("\t\t!!          In an improved development ma3sarheehi :D          !!\n");
f01000e8:	c7 04 24 90 4f 10 f0 	movl   $0xf0104f90,(%esp)
f01000ef:	e8 17 2f 00 00       	call   f010300b <cprintf>
	cprintf("\t\t!!                                                             !!\n");
f01000f4:	c7 04 24 00 4f 10 f0 	movl   $0xf0104f00,(%esp)
f01000fb:	e8 0b 2f 00 00       	call   f010300b <cprintf>
	cprintf("\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
f0100100:	c7 04 24 b8 4e 10 f0 	movl   $0xf0104eb8,(%esp)
f0100107:	e8 ff 2e 00 00       	call   f010300b <cprintf>
	cprintf("\n\n\n\n");	
f010010c:	c7 04 24 d5 4f 10 f0 	movl   $0xf0104fd5,(%esp)
f0100113:	e8 f3 2e 00 00       	call   f010300b <cprintf>
}
f0100118:	c9                   	leave  
f0100119:	c3                   	ret    

f010011a <_panic>:
/*
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel command prompt.
 */
void _panic(const char *file, int line, const char *fmt,...)
{
f010011a:	55                   	push   %ebp
f010011b:	89 e5                	mov    %esp,%ebp
f010011d:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	if (panicstr)
f0100120:	a1 c0 58 14 f0       	mov    0xf01458c0,%eax
f0100125:	85 c0                	test   %eax,%eax
f0100127:	74 02                	je     f010012b <_panic+0x11>
		goto dead;
f0100129:	eb 49                	jmp    f0100174 <_panic+0x5a>
	panicstr = fmt;
f010012b:	8b 45 10             	mov    0x10(%ebp),%eax
f010012e:	a3 c0 58 14 f0       	mov    %eax,0xf01458c0

	va_start(ap, fmt);
f0100133:	8d 45 10             	lea    0x10(%ebp),%eax
f0100136:	83 c0 04             	add    $0x4,%eax
f0100139:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cprintf("kernel panic at %s:%d: ", file, line);
f010013c:	8b 45 0c             	mov    0xc(%ebp),%eax
f010013f:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100143:	8b 45 08             	mov    0x8(%ebp),%eax
f0100146:	89 44 24 04          	mov    %eax,0x4(%esp)
f010014a:	c7 04 24 da 4f 10 f0 	movl   $0xf0104fda,(%esp)
f0100151:	e8 b5 2e 00 00       	call   f010300b <cprintf>
	vcprintf(fmt, ap);
f0100156:	8b 45 10             	mov    0x10(%ebp),%eax
f0100159:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010015c:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100160:	89 04 24             	mov    %eax,(%esp)
f0100163:	e8 70 2e 00 00       	call   f0102fd8 <vcprintf>
	cprintf("\n");
f0100168:	c7 04 24 f2 4f 10 f0 	movl   $0xf0104ff2,(%esp)
f010016f:	e8 97 2e 00 00       	call   f010300b <cprintf>
	va_end(ap);

dead:
	/* break into the kernel command prompt */
	while (1==1)
		run_command_prompt();
f0100174:	e8 e4 07 00 00       	call   f010095d <run_command_prompt>
f0100179:	eb f9                	jmp    f0100174 <_panic+0x5a>

f010017b <_warn>:
}

/* like panic, but don't enters the kernel command prompt*/
void _warn(const char *file, int line, const char *fmt,...)
{
f010017b:	55                   	push   %ebp
f010017c:	89 e5                	mov    %esp,%ebp
f010017e:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
f0100181:	8d 45 10             	lea    0x10(%ebp),%eax
f0100184:	83 c0 04             	add    $0x4,%eax
f0100187:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cprintf("kernel warning at %s:%d: ", file, line);
f010018a:	8b 45 0c             	mov    0xc(%ebp),%eax
f010018d:	89 44 24 08          	mov    %eax,0x8(%esp)
f0100191:	8b 45 08             	mov    0x8(%ebp),%eax
f0100194:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100198:	c7 04 24 f4 4f 10 f0 	movl   $0xf0104ff4,(%esp)
f010019f:	e8 67 2e 00 00       	call   f010300b <cprintf>
	vcprintf(fmt, ap);
f01001a4:	8b 45 10             	mov    0x10(%ebp),%eax
f01001a7:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01001aa:	89 54 24 04          	mov    %edx,0x4(%esp)
f01001ae:	89 04 24             	mov    %eax,(%esp)
f01001b1:	e8 22 2e 00 00       	call   f0102fd8 <vcprintf>
	cprintf("\n");
f01001b6:	c7 04 24 f2 4f 10 f0 	movl   $0xf0104ff2,(%esp)
f01001bd:	e8 49 2e 00 00       	call   f010300b <cprintf>
	va_end(ap);
}
f01001c2:	c9                   	leave  
f01001c3:	c3                   	ret    

f01001c4 <serial_proc_data>:

static bool serial_exists;

int
serial_proc_data(void)
{
f01001c4:	55                   	push   %ebp
f01001c5:	89 e5                	mov    %esp,%ebp
f01001c7:	83 ec 10             	sub    $0x10,%esp
f01001ca:	c7 45 fc fd 03 00 00 	movl   $0x3fd,-0x4(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01001d1:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01001d4:	89 c2                	mov    %eax,%edx
f01001d6:	ec                   	in     (%dx),%al
f01001d7:	88 45 fb             	mov    %al,-0x5(%ebp)
	return data;
f01001da:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f01001de:	0f b6 c0             	movzbl %al,%eax
f01001e1:	83 e0 01             	and    $0x1,%eax
f01001e4:	85 c0                	test   %eax,%eax
f01001e6:	75 07                	jne    f01001ef <serial_proc_data+0x2b>
		return -1;
f01001e8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01001ed:	eb 17                	jmp    f0100206 <serial_proc_data+0x42>
f01001ef:	c7 45 f4 f8 03 00 00 	movl   $0x3f8,-0xc(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01001f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01001f9:	89 c2                	mov    %eax,%edx
f01001fb:	ec                   	in     (%dx),%al
f01001fc:	88 45 f3             	mov    %al,-0xd(%ebp)
	return data;
f01001ff:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
	return inb(COM1+COM_RX);
f0100203:	0f b6 c0             	movzbl %al,%eax
}
f0100206:	c9                   	leave  
f0100207:	c3                   	ret    

f0100208 <serial_intr>:

void
serial_intr(void)
{
f0100208:	55                   	push   %ebp
f0100209:	89 e5                	mov    %esp,%ebp
f010020b:	83 ec 18             	sub    $0x18,%esp
	if (serial_exists)
f010020e:	a1 e0 58 14 f0       	mov    0xf01458e0,%eax
f0100213:	85 c0                	test   %eax,%eax
f0100215:	74 0c                	je     f0100223 <serial_intr+0x1b>
		cons_intr(serial_proc_data);
f0100217:	c7 04 24 c4 01 10 f0 	movl   $0xf01001c4,(%esp)
f010021e:	e8 11 06 00 00       	call   f0100834 <cons_intr>
}
f0100223:	c9                   	leave  
f0100224:	c3                   	ret    

f0100225 <serial_init>:

void
serial_init(void)
{
f0100225:	55                   	push   %ebp
f0100226:	89 e5                	mov    %esp,%ebp
f0100228:	83 ec 50             	sub    $0x50,%esp
f010022b:	c7 45 fc fa 03 00 00 	movl   $0x3fa,-0x4(%ebp)
f0100232:	c6 45 fb 00          	movb   $0x0,-0x5(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100236:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
f010023a:	8b 55 fc             	mov    -0x4(%ebp),%edx
f010023d:	ee                   	out    %al,(%dx)
f010023e:	c7 45 f4 fb 03 00 00 	movl   $0x3fb,-0xc(%ebp)
f0100245:	c6 45 f3 80          	movb   $0x80,-0xd(%ebp)
f0100249:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010024d:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100250:	ee                   	out    %al,(%dx)
f0100251:	c7 45 ec f8 03 00 00 	movl   $0x3f8,-0x14(%ebp)
f0100258:	c6 45 eb 0c          	movb   $0xc,-0x15(%ebp)
f010025c:	0f b6 45 eb          	movzbl -0x15(%ebp),%eax
f0100260:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0100263:	ee                   	out    %al,(%dx)
f0100264:	c7 45 e4 f9 03 00 00 	movl   $0x3f9,-0x1c(%ebp)
f010026b:	c6 45 e3 00          	movb   $0x0,-0x1d(%ebp)
f010026f:	0f b6 45 e3          	movzbl -0x1d(%ebp),%eax
f0100273:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0100276:	ee                   	out    %al,(%dx)
f0100277:	c7 45 dc fb 03 00 00 	movl   $0x3fb,-0x24(%ebp)
f010027e:	c6 45 db 03          	movb   $0x3,-0x25(%ebp)
f0100282:	0f b6 45 db          	movzbl -0x25(%ebp),%eax
f0100286:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100289:	ee                   	out    %al,(%dx)
f010028a:	c7 45 d4 fc 03 00 00 	movl   $0x3fc,-0x2c(%ebp)
f0100291:	c6 45 d3 00          	movb   $0x0,-0x2d(%ebp)
f0100295:	0f b6 45 d3          	movzbl -0x2d(%ebp),%eax
f0100299:	8b 55 d4             	mov    -0x2c(%ebp),%edx
f010029c:	ee                   	out    %al,(%dx)
f010029d:	c7 45 cc f9 03 00 00 	movl   $0x3f9,-0x34(%ebp)
f01002a4:	c6 45 cb 01          	movb   $0x1,-0x35(%ebp)
f01002a8:	0f b6 45 cb          	movzbl -0x35(%ebp),%eax
f01002ac:	8b 55 cc             	mov    -0x34(%ebp),%edx
f01002af:	ee                   	out    %al,(%dx)
f01002b0:	c7 45 c4 fd 03 00 00 	movl   $0x3fd,-0x3c(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01002b7:	8b 45 c4             	mov    -0x3c(%ebp),%eax
f01002ba:	89 c2                	mov    %eax,%edx
f01002bc:	ec                   	in     (%dx),%al
f01002bd:	88 45 c3             	mov    %al,-0x3d(%ebp)
	return data;
f01002c0:	0f b6 45 c3          	movzbl -0x3d(%ebp),%eax
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01002c4:	3c ff                	cmp    $0xff,%al
f01002c6:	0f 95 c0             	setne  %al
f01002c9:	0f b6 c0             	movzbl %al,%eax
f01002cc:	a3 e0 58 14 f0       	mov    %eax,0xf01458e0
f01002d1:	c7 45 bc fa 03 00 00 	movl   $0x3fa,-0x44(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01002d8:	8b 45 bc             	mov    -0x44(%ebp),%eax
f01002db:	89 c2                	mov    %eax,%edx
f01002dd:	ec                   	in     (%dx),%al
f01002de:	88 45 bb             	mov    %al,-0x45(%ebp)
f01002e1:	c7 45 b4 f8 03 00 00 	movl   $0x3f8,-0x4c(%ebp)
f01002e8:	8b 45 b4             	mov    -0x4c(%ebp),%eax
f01002eb:	89 c2                	mov    %eax,%edx
f01002ed:	ec                   	in     (%dx),%al
f01002ee:	88 45 b3             	mov    %al,-0x4d(%ebp)
	(void) inb(COM1+COM_IIR);
	(void) inb(COM1+COM_RX);

}
f01002f1:	c9                   	leave  
f01002f2:	c3                   	ret    

f01002f3 <delay>:
// page.

// Stupid I/O delay routine necessitated by historical PC design flaws
static void
delay(void)
{
f01002f3:	55                   	push   %ebp
f01002f4:	89 e5                	mov    %esp,%ebp
f01002f6:	83 ec 20             	sub    $0x20,%esp
f01002f9:	c7 45 fc 84 00 00 00 	movl   $0x84,-0x4(%ebp)
f0100300:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0100303:	89 c2                	mov    %eax,%edx
f0100305:	ec                   	in     (%dx),%al
f0100306:	88 45 fb             	mov    %al,-0x5(%ebp)
f0100309:	c7 45 f4 84 00 00 00 	movl   $0x84,-0xc(%ebp)
f0100310:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100313:	89 c2                	mov    %eax,%edx
f0100315:	ec                   	in     (%dx),%al
f0100316:	88 45 f3             	mov    %al,-0xd(%ebp)
f0100319:	c7 45 ec 84 00 00 00 	movl   $0x84,-0x14(%ebp)
f0100320:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100323:	89 c2                	mov    %eax,%edx
f0100325:	ec                   	in     (%dx),%al
f0100326:	88 45 eb             	mov    %al,-0x15(%ebp)
f0100329:	c7 45 e4 84 00 00 00 	movl   $0x84,-0x1c(%ebp)
f0100330:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100333:	89 c2                	mov    %eax,%edx
f0100335:	ec                   	in     (%dx),%al
f0100336:	88 45 e3             	mov    %al,-0x1d(%ebp)
	inb(0x84);
	inb(0x84);
	inb(0x84);
	inb(0x84);
}
f0100339:	c9                   	leave  
f010033a:	c3                   	ret    

f010033b <lpt_putc>:

static void
lpt_putc(int c)
{
f010033b:	55                   	push   %ebp
f010033c:	89 e5                	mov    %esp,%ebp
f010033e:	83 ec 30             	sub    $0x30,%esp
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 2800; i++) //12800
f0100341:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f0100348:	eb 09                	jmp    f0100353 <lpt_putc+0x18>
		delay();
f010034a:	e8 a4 ff ff ff       	call   f01002f3 <delay>
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 2800; i++) //12800
f010034f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f0100353:	c7 45 f8 79 03 00 00 	movl   $0x379,-0x8(%ebp)
f010035a:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010035d:	89 c2                	mov    %eax,%edx
f010035f:	ec                   	in     (%dx),%al
f0100360:	88 45 f7             	mov    %al,-0x9(%ebp)
	return data;
f0100363:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
f0100367:	84 c0                	test   %al,%al
f0100369:	78 09                	js     f0100374 <lpt_putc+0x39>
f010036b:	81 7d fc ef 0a 00 00 	cmpl   $0xaef,-0x4(%ebp)
f0100372:	7e d6                	jle    f010034a <lpt_putc+0xf>
		delay();
	outb(0x378+0, c);
f0100374:	8b 45 08             	mov    0x8(%ebp),%eax
f0100377:	0f b6 c0             	movzbl %al,%eax
f010037a:	c7 45 f0 78 03 00 00 	movl   $0x378,-0x10(%ebp)
f0100381:	88 45 ef             	mov    %al,-0x11(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100384:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
f0100388:	8b 55 f0             	mov    -0x10(%ebp),%edx
f010038b:	ee                   	out    %al,(%dx)
f010038c:	c7 45 e8 7a 03 00 00 	movl   $0x37a,-0x18(%ebp)
f0100393:	c6 45 e7 0d          	movb   $0xd,-0x19(%ebp)
f0100397:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
f010039b:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010039e:	ee                   	out    %al,(%dx)
f010039f:	c7 45 e0 7a 03 00 00 	movl   $0x37a,-0x20(%ebp)
f01003a6:	c6 45 df 08          	movb   $0x8,-0x21(%ebp)
f01003aa:	0f b6 45 df          	movzbl -0x21(%ebp),%eax
f01003ae:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01003b1:	ee                   	out    %al,(%dx)
	outb(0x378+2, 0x08|0x04|0x01);
	outb(0x378+2, 0x08);
}
f01003b2:	c9                   	leave  
f01003b3:	c3                   	ret    

f01003b4 <cga_init>:
static uint16 *crt_buf;
static uint16 crt_pos;

void
cga_init(void)
{
f01003b4:	55                   	push   %ebp
f01003b5:	89 e5                	mov    %esp,%ebp
f01003b7:	83 ec 30             	sub    $0x30,%esp
	volatile uint16 *cp;
	uint16 was;
	unsigned pos;

	cp = (uint16*) (KERNEL_BASE + CGA_BUF);
f01003ba:	c7 45 fc 00 80 0b f0 	movl   $0xf00b8000,-0x4(%ebp)
	was = *cp;
f01003c1:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003c4:	0f b7 00             	movzwl (%eax),%eax
f01003c7:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
	*cp = (uint16) 0xA55A;
f01003cb:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003ce:	66 c7 00 5a a5       	movw   $0xa55a,(%eax)
	if (*cp != 0xA55A) {
f01003d3:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003d6:	0f b7 00             	movzwl (%eax),%eax
f01003d9:	66 3d 5a a5          	cmp    $0xa55a,%ax
f01003dd:	74 13                	je     f01003f2 <cga_init+0x3e>
		cp = (uint16*) (KERNEL_BASE + MONO_BUF);
f01003df:	c7 45 fc 00 00 0b f0 	movl   $0xf00b0000,-0x4(%ebp)
		addr_6845 = MONO_BASE;
f01003e6:	c7 05 e4 58 14 f0 b4 	movl   $0x3b4,0xf01458e4
f01003ed:	03 00 00 
f01003f0:	eb 14                	jmp    f0100406 <cga_init+0x52>
	} else {
		*cp = was;
f01003f2:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003f5:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
f01003f9:	66 89 10             	mov    %dx,(%eax)
		addr_6845 = CGA_BASE;
f01003fc:	c7 05 e4 58 14 f0 d4 	movl   $0x3d4,0xf01458e4
f0100403:	03 00 00 
	}
	
	/* Extract cursor location */
	outb(addr_6845, 14);
f0100406:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f010040b:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010040e:	c6 45 ef 0e          	movb   $0xe,-0x11(%ebp)
f0100412:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
f0100416:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100419:	ee                   	out    %al,(%dx)
	pos = inb(addr_6845 + 1) << 8;
f010041a:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f010041f:	83 c0 01             	add    $0x1,%eax
f0100422:	89 45 e8             	mov    %eax,-0x18(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100425:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0100428:	89 c2                	mov    %eax,%edx
f010042a:	ec                   	in     (%dx),%al
f010042b:	88 45 e7             	mov    %al,-0x19(%ebp)
	return data;
f010042e:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
f0100432:	0f b6 c0             	movzbl %al,%eax
f0100435:	c1 e0 08             	shl    $0x8,%eax
f0100438:	89 45 f4             	mov    %eax,-0xc(%ebp)
	outb(addr_6845, 15);
f010043b:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f0100440:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0100443:	c6 45 df 0f          	movb   $0xf,-0x21(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100447:	0f b6 45 df          	movzbl -0x21(%ebp),%eax
f010044b:	8b 55 e0             	mov    -0x20(%ebp),%edx
f010044e:	ee                   	out    %al,(%dx)
	pos |= inb(addr_6845 + 1);
f010044f:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f0100454:	83 c0 01             	add    $0x1,%eax
f0100457:	89 45 d8             	mov    %eax,-0x28(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010045a:	8b 45 d8             	mov    -0x28(%ebp),%eax
f010045d:	89 c2                	mov    %eax,%edx
f010045f:	ec                   	in     (%dx),%al
f0100460:	88 45 d7             	mov    %al,-0x29(%ebp)
	return data;
f0100463:	0f b6 45 d7          	movzbl -0x29(%ebp),%eax
f0100467:	0f b6 c0             	movzbl %al,%eax
f010046a:	09 45 f4             	or     %eax,-0xc(%ebp)

	crt_buf = (uint16*) cp;
f010046d:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0100470:	a3 e8 58 14 f0       	mov    %eax,0xf01458e8
	crt_pos = pos;
f0100475:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100478:	66 a3 ec 58 14 f0    	mov    %ax,0xf01458ec
}
f010047e:	c9                   	leave  
f010047f:	c3                   	ret    

f0100480 <cga_putc>:



void
cga_putc(int c)
{
f0100480:	55                   	push   %ebp
f0100481:	89 e5                	mov    %esp,%ebp
f0100483:	53                   	push   %ebx
f0100484:	83 ec 44             	sub    $0x44,%esp
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f0100487:	8b 45 08             	mov    0x8(%ebp),%eax
f010048a:	b0 00                	mov    $0x0,%al
f010048c:	85 c0                	test   %eax,%eax
f010048e:	75 07                	jne    f0100497 <cga_putc+0x17>
		c |= 0x0700;
f0100490:	81 4d 08 00 07 00 00 	orl    $0x700,0x8(%ebp)

	switch (c & 0xff) {
f0100497:	8b 45 08             	mov    0x8(%ebp),%eax
f010049a:	0f b6 c0             	movzbl %al,%eax
f010049d:	83 f8 09             	cmp    $0x9,%eax
f01004a0:	0f 84 ac 00 00 00    	je     f0100552 <cga_putc+0xd2>
f01004a6:	83 f8 09             	cmp    $0x9,%eax
f01004a9:	7f 0a                	jg     f01004b5 <cga_putc+0x35>
f01004ab:	83 f8 08             	cmp    $0x8,%eax
f01004ae:	74 14                	je     f01004c4 <cga_putc+0x44>
f01004b0:	e9 db 00 00 00       	jmp    f0100590 <cga_putc+0x110>
f01004b5:	83 f8 0a             	cmp    $0xa,%eax
f01004b8:	74 4e                	je     f0100508 <cga_putc+0x88>
f01004ba:	83 f8 0d             	cmp    $0xd,%eax
f01004bd:	74 59                	je     f0100518 <cga_putc+0x98>
f01004bf:	e9 cc 00 00 00       	jmp    f0100590 <cga_putc+0x110>
	case '\b':
		if (crt_pos > 0) {
f01004c4:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f01004cb:	66 85 c0             	test   %ax,%ax
f01004ce:	74 33                	je     f0100503 <cga_putc+0x83>
			crt_pos--;
f01004d0:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f01004d7:	83 e8 01             	sub    $0x1,%eax
f01004da:	66 a3 ec 58 14 f0    	mov    %ax,0xf01458ec
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01004e0:	a1 e8 58 14 f0       	mov    0xf01458e8,%eax
f01004e5:	0f b7 15 ec 58 14 f0 	movzwl 0xf01458ec,%edx
f01004ec:	0f b7 d2             	movzwl %dx,%edx
f01004ef:	01 d2                	add    %edx,%edx
f01004f1:	01 c2                	add    %eax,%edx
f01004f3:	8b 45 08             	mov    0x8(%ebp),%eax
f01004f6:	b0 00                	mov    $0x0,%al
f01004f8:	83 c8 20             	or     $0x20,%eax
f01004fb:	66 89 02             	mov    %ax,(%edx)
		}
		break;
f01004fe:	e9 b3 00 00 00       	jmp    f01005b6 <cga_putc+0x136>
f0100503:	e9 ae 00 00 00       	jmp    f01005b6 <cga_putc+0x136>
	case '\n':
		crt_pos += CRT_COLS;
f0100508:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f010050f:	83 c0 50             	add    $0x50,%eax
f0100512:	66 a3 ec 58 14 f0    	mov    %ax,0xf01458ec
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f0100518:	0f b7 1d ec 58 14 f0 	movzwl 0xf01458ec,%ebx
f010051f:	0f b7 0d ec 58 14 f0 	movzwl 0xf01458ec,%ecx
f0100526:	0f b7 c1             	movzwl %cx,%eax
f0100529:	69 c0 cd cc 00 00    	imul   $0xcccd,%eax,%eax
f010052f:	c1 e8 10             	shr    $0x10,%eax
f0100532:	89 c2                	mov    %eax,%edx
f0100534:	66 c1 ea 06          	shr    $0x6,%dx
f0100538:	89 d0                	mov    %edx,%eax
f010053a:	c1 e0 02             	shl    $0x2,%eax
f010053d:	01 d0                	add    %edx,%eax
f010053f:	c1 e0 04             	shl    $0x4,%eax
f0100542:	29 c1                	sub    %eax,%ecx
f0100544:	89 ca                	mov    %ecx,%edx
f0100546:	89 d8                	mov    %ebx,%eax
f0100548:	29 d0                	sub    %edx,%eax
f010054a:	66 a3 ec 58 14 f0    	mov    %ax,0xf01458ec
		break;
f0100550:	eb 64                	jmp    f01005b6 <cga_putc+0x136>
	case '\t':
		cons_putc(' ');
f0100552:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f0100559:	e8 7f 03 00 00       	call   f01008dd <cons_putc>
		cons_putc(' ');
f010055e:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f0100565:	e8 73 03 00 00       	call   f01008dd <cons_putc>
		cons_putc(' ');
f010056a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f0100571:	e8 67 03 00 00       	call   f01008dd <cons_putc>
		cons_putc(' ');
f0100576:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f010057d:	e8 5b 03 00 00       	call   f01008dd <cons_putc>
		cons_putc(' ');
f0100582:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f0100589:	e8 4f 03 00 00       	call   f01008dd <cons_putc>
		break;
f010058e:	eb 26                	jmp    f01005b6 <cga_putc+0x136>
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f0100590:	8b 0d e8 58 14 f0    	mov    0xf01458e8,%ecx
f0100596:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f010059d:	8d 50 01             	lea    0x1(%eax),%edx
f01005a0:	66 89 15 ec 58 14 f0 	mov    %dx,0xf01458ec
f01005a7:	0f b7 c0             	movzwl %ax,%eax
f01005aa:	01 c0                	add    %eax,%eax
f01005ac:	8d 14 01             	lea    (%ecx,%eax,1),%edx
f01005af:	8b 45 08             	mov    0x8(%ebp),%eax
f01005b2:	66 89 02             	mov    %ax,(%edx)
		break;
f01005b5:	90                   	nop
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f01005b6:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f01005bd:	66 3d cf 07          	cmp    $0x7cf,%ax
f01005c1:	76 5b                	jbe    f010061e <cga_putc+0x19e>
		int i;

		memcpy(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16));
f01005c3:	a1 e8 58 14 f0       	mov    0xf01458e8,%eax
f01005c8:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f01005ce:	a1 e8 58 14 f0       	mov    0xf01458e8,%eax
f01005d3:	c7 44 24 08 00 0f 00 	movl   $0xf00,0x8(%esp)
f01005da:	00 
f01005db:	89 54 24 04          	mov    %edx,0x4(%esp)
f01005df:	89 04 24             	mov    %eax,(%esp)
f01005e2:	e8 48 42 00 00       	call   f010482f <memcpy>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f01005e7:	c7 45 f4 80 07 00 00 	movl   $0x780,-0xc(%ebp)
f01005ee:	eb 15                	jmp    f0100605 <cga_putc+0x185>
			crt_buf[i] = 0x0700 | ' ';
f01005f0:	a1 e8 58 14 f0       	mov    0xf01458e8,%eax
f01005f5:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01005f8:	01 d2                	add    %edx,%edx
f01005fa:	01 d0                	add    %edx,%eax
f01005fc:	66 c7 00 20 07       	movw   $0x720,(%eax)
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memcpy(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100601:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0100605:	81 7d f4 cf 07 00 00 	cmpl   $0x7cf,-0xc(%ebp)
f010060c:	7e e2                	jle    f01005f0 <cga_putc+0x170>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f010060e:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f0100615:	83 e8 50             	sub    $0x50,%eax
f0100618:	66 a3 ec 58 14 f0    	mov    %ax,0xf01458ec
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f010061e:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f0100623:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100626:	c6 45 ef 0e          	movb   $0xe,-0x11(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010062a:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
f010062e:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100631:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f0100632:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f0100639:	66 c1 e8 08          	shr    $0x8,%ax
f010063d:	0f b6 c0             	movzbl %al,%eax
f0100640:	8b 15 e4 58 14 f0    	mov    0xf01458e4,%edx
f0100646:	83 c2 01             	add    $0x1,%edx
f0100649:	89 55 e8             	mov    %edx,-0x18(%ebp)
f010064c:	88 45 e7             	mov    %al,-0x19(%ebp)
f010064f:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
f0100653:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0100656:	ee                   	out    %al,(%dx)
	outb(addr_6845, 15);
f0100657:	a1 e4 58 14 f0       	mov    0xf01458e4,%eax
f010065c:	89 45 e0             	mov    %eax,-0x20(%ebp)
f010065f:	c6 45 df 0f          	movb   $0xf,-0x21(%ebp)
f0100663:	0f b6 45 df          	movzbl -0x21(%ebp),%eax
f0100667:	8b 55 e0             	mov    -0x20(%ebp),%edx
f010066a:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos);
f010066b:	0f b7 05 ec 58 14 f0 	movzwl 0xf01458ec,%eax
f0100672:	0f b6 c0             	movzbl %al,%eax
f0100675:	8b 15 e4 58 14 f0    	mov    0xf01458e4,%edx
f010067b:	83 c2 01             	add    $0x1,%edx
f010067e:	89 55 d8             	mov    %edx,-0x28(%ebp)
f0100681:	88 45 d7             	mov    %al,-0x29(%ebp)
f0100684:	0f b6 45 d7          	movzbl -0x29(%ebp),%eax
f0100688:	8b 55 d8             	mov    -0x28(%ebp),%edx
f010068b:	ee                   	out    %al,(%dx)
}
f010068c:	83 c4 44             	add    $0x44,%esp
f010068f:	5b                   	pop    %ebx
f0100690:	5d                   	pop    %ebp
f0100691:	c3                   	ret    

f0100692 <kbd_proc_data>:
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f0100692:	55                   	push   %ebp
f0100693:	89 e5                	mov    %esp,%ebp
f0100695:	83 ec 38             	sub    $0x38,%esp
f0100698:	c7 45 ec 64 00 00 00 	movl   $0x64,-0x14(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010069f:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01006a2:	89 c2                	mov    %eax,%edx
f01006a4:	ec                   	in     (%dx),%al
f01006a5:	88 45 eb             	mov    %al,-0x15(%ebp)
	return data;
f01006a8:	0f b6 45 eb          	movzbl -0x15(%ebp),%eax
	int c;
	uint8 data;
	static uint32 shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
f01006ac:	0f b6 c0             	movzbl %al,%eax
f01006af:	83 e0 01             	and    $0x1,%eax
f01006b2:	85 c0                	test   %eax,%eax
f01006b4:	75 0a                	jne    f01006c0 <kbd_proc_data+0x2e>
		return -1;
f01006b6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01006bb:	e9 59 01 00 00       	jmp    f0100819 <kbd_proc_data+0x187>
f01006c0:	c7 45 e4 60 00 00 00 	movl   $0x60,-0x1c(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01006c7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01006ca:	89 c2                	mov    %eax,%edx
f01006cc:	ec                   	in     (%dx),%al
f01006cd:	88 45 e3             	mov    %al,-0x1d(%ebp)
	return data;
f01006d0:	0f b6 45 e3          	movzbl -0x1d(%ebp),%eax

	data = inb(KBDATAP);
f01006d4:	88 45 f3             	mov    %al,-0xd(%ebp)

	if (data == 0xE0) {
f01006d7:	80 7d f3 e0          	cmpb   $0xe0,-0xd(%ebp)
f01006db:	75 17                	jne    f01006f4 <kbd_proc_data+0x62>
		// E0 escape character
		shift |= E0ESC;
f01006dd:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f01006e2:	83 c8 40             	or     $0x40,%eax
f01006e5:	a3 08 5b 14 f0       	mov    %eax,0xf0145b08
		return 0;
f01006ea:	b8 00 00 00 00       	mov    $0x0,%eax
f01006ef:	e9 25 01 00 00       	jmp    f0100819 <kbd_proc_data+0x187>
	} else if (data & 0x80) {
f01006f4:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f01006f8:	84 c0                	test   %al,%al
f01006fa:	79 47                	jns    f0100743 <kbd_proc_data+0xb1>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f01006fc:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100701:	83 e0 40             	and    $0x40,%eax
f0100704:	85 c0                	test   %eax,%eax
f0100706:	75 09                	jne    f0100711 <kbd_proc_data+0x7f>
f0100708:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010070c:	83 e0 7f             	and    $0x7f,%eax
f010070f:	eb 04                	jmp    f0100715 <kbd_proc_data+0x83>
f0100711:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0100715:	88 45 f3             	mov    %al,-0xd(%ebp)
		shift &= ~(shiftcode[data] | E0ESC);
f0100718:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010071c:	0f b6 80 20 a0 11 f0 	movzbl -0xfee5fe0(%eax),%eax
f0100723:	83 c8 40             	or     $0x40,%eax
f0100726:	0f b6 c0             	movzbl %al,%eax
f0100729:	f7 d0                	not    %eax
f010072b:	89 c2                	mov    %eax,%edx
f010072d:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100732:	21 d0                	and    %edx,%eax
f0100734:	a3 08 5b 14 f0       	mov    %eax,0xf0145b08
		return 0;
f0100739:	b8 00 00 00 00       	mov    $0x0,%eax
f010073e:	e9 d6 00 00 00       	jmp    f0100819 <kbd_proc_data+0x187>
	} else if (shift & E0ESC) {
f0100743:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100748:	83 e0 40             	and    $0x40,%eax
f010074b:	85 c0                	test   %eax,%eax
f010074d:	74 11                	je     f0100760 <kbd_proc_data+0xce>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f010074f:	80 4d f3 80          	orb    $0x80,-0xd(%ebp)
		shift &= ~E0ESC;
f0100753:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100758:	83 e0 bf             	and    $0xffffffbf,%eax
f010075b:	a3 08 5b 14 f0       	mov    %eax,0xf0145b08
	}

	shift |= shiftcode[data];
f0100760:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0100764:	0f b6 80 20 a0 11 f0 	movzbl -0xfee5fe0(%eax),%eax
f010076b:	0f b6 d0             	movzbl %al,%edx
f010076e:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100773:	09 d0                	or     %edx,%eax
f0100775:	a3 08 5b 14 f0       	mov    %eax,0xf0145b08
	shift ^= togglecode[data];
f010077a:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010077e:	0f b6 80 20 a1 11 f0 	movzbl -0xfee5ee0(%eax),%eax
f0100785:	0f b6 d0             	movzbl %al,%edx
f0100788:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f010078d:	31 d0                	xor    %edx,%eax
f010078f:	a3 08 5b 14 f0       	mov    %eax,0xf0145b08

	c = charcode[shift & (CTL | SHIFT)][data];
f0100794:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f0100799:	83 e0 03             	and    $0x3,%eax
f010079c:	8b 14 85 20 a5 11 f0 	mov    -0xfee5ae0(,%eax,4),%edx
f01007a3:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f01007a7:	01 d0                	add    %edx,%eax
f01007a9:	0f b6 00             	movzbl (%eax),%eax
f01007ac:	0f b6 c0             	movzbl %al,%eax
f01007af:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (shift & CAPSLOCK) {
f01007b2:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f01007b7:	83 e0 08             	and    $0x8,%eax
f01007ba:	85 c0                	test   %eax,%eax
f01007bc:	74 22                	je     f01007e0 <kbd_proc_data+0x14e>
		if ('a' <= c && c <= 'z')
f01007be:	83 7d f4 60          	cmpl   $0x60,-0xc(%ebp)
f01007c2:	7e 0c                	jle    f01007d0 <kbd_proc_data+0x13e>
f01007c4:	83 7d f4 7a          	cmpl   $0x7a,-0xc(%ebp)
f01007c8:	7f 06                	jg     f01007d0 <kbd_proc_data+0x13e>
			c += 'A' - 'a';
f01007ca:	83 6d f4 20          	subl   $0x20,-0xc(%ebp)
f01007ce:	eb 10                	jmp    f01007e0 <kbd_proc_data+0x14e>
		else if ('A' <= c && c <= 'Z')
f01007d0:	83 7d f4 40          	cmpl   $0x40,-0xc(%ebp)
f01007d4:	7e 0a                	jle    f01007e0 <kbd_proc_data+0x14e>
f01007d6:	83 7d f4 5a          	cmpl   $0x5a,-0xc(%ebp)
f01007da:	7f 04                	jg     f01007e0 <kbd_proc_data+0x14e>
			c += 'a' - 'A';
f01007dc:	83 45 f4 20          	addl   $0x20,-0xc(%ebp)
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01007e0:	a1 08 5b 14 f0       	mov    0xf0145b08,%eax
f01007e5:	f7 d0                	not    %eax
f01007e7:	83 e0 06             	and    $0x6,%eax
f01007ea:	85 c0                	test   %eax,%eax
f01007ec:	75 28                	jne    f0100816 <kbd_proc_data+0x184>
f01007ee:	81 7d f4 e9 00 00 00 	cmpl   $0xe9,-0xc(%ebp)
f01007f5:	75 1f                	jne    f0100816 <kbd_proc_data+0x184>
		cprintf("Rebooting!\n");
f01007f7:	c7 04 24 0e 50 10 f0 	movl   $0xf010500e,(%esp)
f01007fe:	e8 08 28 00 00       	call   f010300b <cprintf>
f0100803:	c7 45 dc 92 00 00 00 	movl   $0x92,-0x24(%ebp)
f010080a:	c6 45 db 03          	movb   $0x3,-0x25(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010080e:	0f b6 45 db          	movzbl -0x25(%ebp),%eax
f0100812:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100815:	ee                   	out    %al,(%dx)
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f0100816:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f0100819:	c9                   	leave  
f010081a:	c3                   	ret    

f010081b <kbd_intr>:

void
kbd_intr(void)
{
f010081b:	55                   	push   %ebp
f010081c:	89 e5                	mov    %esp,%ebp
f010081e:	83 ec 18             	sub    $0x18,%esp
	cons_intr(kbd_proc_data);
f0100821:	c7 04 24 92 06 10 f0 	movl   $0xf0100692,(%esp)
f0100828:	e8 07 00 00 00       	call   f0100834 <cons_intr>
}
f010082d:	c9                   	leave  
f010082e:	c3                   	ret    

f010082f <kbd_init>:

void
kbd_init(void)
{
f010082f:	55                   	push   %ebp
f0100830:	89 e5                	mov    %esp,%ebp
}
f0100832:	5d                   	pop    %ebp
f0100833:	c3                   	ret    

f0100834 <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
void
cons_intr(int (*proc)(void))
{
f0100834:	55                   	push   %ebp
f0100835:	89 e5                	mov    %esp,%ebp
f0100837:	83 ec 18             	sub    $0x18,%esp
	int c;

	while ((c = (*proc)()) != -1) {
f010083a:	eb 35                	jmp    f0100871 <cons_intr+0x3d>
		if (c == 0)
f010083c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0100840:	75 02                	jne    f0100844 <cons_intr+0x10>
			continue;
f0100842:	eb 2d                	jmp    f0100871 <cons_intr+0x3d>
		cons.buf[cons.wpos++] = c;
f0100844:	a1 04 5b 14 f0       	mov    0xf0145b04,%eax
f0100849:	8d 50 01             	lea    0x1(%eax),%edx
f010084c:	89 15 04 5b 14 f0    	mov    %edx,0xf0145b04
f0100852:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100855:	88 90 00 59 14 f0    	mov    %dl,-0xfeba700(%eax)
		if (cons.wpos == CONSBUFSIZE)
f010085b:	a1 04 5b 14 f0       	mov    0xf0145b04,%eax
f0100860:	3d 00 02 00 00       	cmp    $0x200,%eax
f0100865:	75 0a                	jne    f0100871 <cons_intr+0x3d>
			cons.wpos = 0;
f0100867:	c7 05 04 5b 14 f0 00 	movl   $0x0,0xf0145b04
f010086e:	00 00 00 
void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f0100871:	8b 45 08             	mov    0x8(%ebp),%eax
f0100874:	ff d0                	call   *%eax
f0100876:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100879:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
f010087d:	75 bd                	jne    f010083c <cons_intr+0x8>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f010087f:	c9                   	leave  
f0100880:	c3                   	ret    

f0100881 <cons_getc>:

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f0100881:	55                   	push   %ebp
f0100882:	89 e5                	mov    %esp,%ebp
f0100884:	83 ec 18             	sub    $0x18,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f0100887:	e8 7c f9 ff ff       	call   f0100208 <serial_intr>
	kbd_intr();
f010088c:	e8 8a ff ff ff       	call   f010081b <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f0100891:	8b 15 00 5b 14 f0    	mov    0xf0145b00,%edx
f0100897:	a1 04 5b 14 f0       	mov    0xf0145b04,%eax
f010089c:	39 c2                	cmp    %eax,%edx
f010089e:	74 36                	je     f01008d6 <cons_getc+0x55>
		c = cons.buf[cons.rpos++];
f01008a0:	a1 00 5b 14 f0       	mov    0xf0145b00,%eax
f01008a5:	8d 50 01             	lea    0x1(%eax),%edx
f01008a8:	89 15 00 5b 14 f0    	mov    %edx,0xf0145b00
f01008ae:	0f b6 80 00 59 14 f0 	movzbl -0xfeba700(%eax),%eax
f01008b5:	0f b6 c0             	movzbl %al,%eax
f01008b8:	89 45 f4             	mov    %eax,-0xc(%ebp)
		if (cons.rpos == CONSBUFSIZE)
f01008bb:	a1 00 5b 14 f0       	mov    0xf0145b00,%eax
f01008c0:	3d 00 02 00 00       	cmp    $0x200,%eax
f01008c5:	75 0a                	jne    f01008d1 <cons_getc+0x50>
			cons.rpos = 0;
f01008c7:	c7 05 00 5b 14 f0 00 	movl   $0x0,0xf0145b00
f01008ce:	00 00 00 
		return c;
f01008d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01008d4:	eb 05                	jmp    f01008db <cons_getc+0x5a>
	}
	return 0;
f01008d6:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01008db:	c9                   	leave  
f01008dc:	c3                   	ret    

f01008dd <cons_putc>:

// output a character to the console
void
cons_putc(int c)
{
f01008dd:	55                   	push   %ebp
f01008de:	89 e5                	mov    %esp,%ebp
f01008e0:	83 ec 18             	sub    $0x18,%esp
	lpt_putc(c);
f01008e3:	8b 45 08             	mov    0x8(%ebp),%eax
f01008e6:	89 04 24             	mov    %eax,(%esp)
f01008e9:	e8 4d fa ff ff       	call   f010033b <lpt_putc>
	cga_putc(c);
f01008ee:	8b 45 08             	mov    0x8(%ebp),%eax
f01008f1:	89 04 24             	mov    %eax,(%esp)
f01008f4:	e8 87 fb ff ff       	call   f0100480 <cga_putc>
}
f01008f9:	c9                   	leave  
f01008fa:	c3                   	ret    

f01008fb <console_initialize>:

// initialize the console devices
void
console_initialize(void)
{
f01008fb:	55                   	push   %ebp
f01008fc:	89 e5                	mov    %esp,%ebp
f01008fe:	83 ec 18             	sub    $0x18,%esp
	cga_init();
f0100901:	e8 ae fa ff ff       	call   f01003b4 <cga_init>
	kbd_init();
f0100906:	e8 24 ff ff ff       	call   f010082f <kbd_init>
	serial_init();
f010090b:	e8 15 f9 ff ff       	call   f0100225 <serial_init>

	if (!serial_exists)
f0100910:	a1 e0 58 14 f0       	mov    0xf01458e0,%eax
f0100915:	85 c0                	test   %eax,%eax
f0100917:	75 0c                	jne    f0100925 <console_initialize+0x2a>
		cprintf("Serial port does not exist!\n");
f0100919:	c7 04 24 1a 50 10 f0 	movl   $0xf010501a,(%esp)
f0100920:	e8 e6 26 00 00       	call   f010300b <cprintf>
}
f0100925:	c9                   	leave  
f0100926:	c3                   	ret    

f0100927 <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f0100927:	55                   	push   %ebp
f0100928:	89 e5                	mov    %esp,%ebp
f010092a:	83 ec 18             	sub    $0x18,%esp
	cons_putc(c);
f010092d:	8b 45 08             	mov    0x8(%ebp),%eax
f0100930:	89 04 24             	mov    %eax,(%esp)
f0100933:	e8 a5 ff ff ff       	call   f01008dd <cons_putc>
}
f0100938:	c9                   	leave  
f0100939:	c3                   	ret    

f010093a <getchar>:

int
getchar(void)
{
f010093a:	55                   	push   %ebp
f010093b:	89 e5                	mov    %esp,%ebp
f010093d:	83 ec 18             	sub    $0x18,%esp
	int c;

	while ((c = cons_getc()) == 0)
f0100940:	e8 3c ff ff ff       	call   f0100881 <cons_getc>
f0100945:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100948:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f010094c:	74 f2                	je     f0100940 <getchar+0x6>
		/* do nothing */;
	return c;
f010094e:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f0100951:	c9                   	leave  
f0100952:	c3                   	ret    

f0100953 <iscons>:

int
iscons(int fdnum)
{
f0100953:	55                   	push   %ebp
f0100954:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
f0100956:	b8 01 00 00 00       	mov    $0x1,%eax
}
f010095b:	5d                   	pop    %ebp
f010095c:	c3                   	ret    

f010095d <run_command_prompt>:
unsigned read_eip();


//invoke the command prompt
void run_command_prompt()
{
f010095d:	55                   	push   %ebp
f010095e:	89 e5                	mov    %esp,%ebp
f0100960:	81 ec 18 04 00 00    	sub    $0x418,%esp
	char command_line[1024];

	while (1==1) 
	{
		//get command line
		readline("FOS> ", command_line);
f0100966:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
f010096c:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100970:	c7 04 24 8d 50 10 f0 	movl   $0xf010508d,(%esp)
f0100977:	e8 7f 3b 00 00       	call   f01044fb <readline>
		
		//parse and execute the command
		if (command_line != NULL)
			if (execute_command(command_line) < 0)
f010097c:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
f0100982:	89 04 24             	mov    %eax,(%esp)
f0100985:	e8 0a 00 00 00       	call   f0100994 <execute_command>
f010098a:	85 c0                	test   %eax,%eax
f010098c:	79 02                	jns    f0100990 <run_command_prompt+0x33>
				break;
f010098e:	eb 02                	jmp    f0100992 <run_command_prompt+0x35>
	}
f0100990:	eb d4                	jmp    f0100966 <run_command_prompt+0x9>
}
f0100992:	c9                   	leave  
f0100993:	c3                   	ret    

f0100994 <execute_command>:
#define WHITESPACE "\t\r\n "

//Function to parse any command and execute it 
//(simply by calling its corresponding function)
int execute_command(char *command_string)
{
f0100994:	55                   	push   %ebp
f0100995:	89 e5                	mov    %esp,%ebp
f0100997:	83 ec 68             	sub    $0x68,%esp
	int number_of_arguments;
	//allocate array of char * of size MAX_ARGUMENTS = 16 found in string.h
	char *arguments[MAX_ARGUMENTS];


	strsplit(command_string, WHITESPACE, arguments, &number_of_arguments) ;
f010099a:	8d 45 e8             	lea    -0x18(%ebp),%eax
f010099d:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01009a1:	8d 45 a8             	lea    -0x58(%ebp),%eax
f01009a4:	89 44 24 08          	mov    %eax,0x8(%esp)
f01009a8:	c7 44 24 04 93 50 10 	movl   $0xf0105093,0x4(%esp)
f01009af:	f0 
f01009b0:	8b 45 08             	mov    0x8(%ebp),%eax
f01009b3:	89 04 24             	mov    %eax,(%esp)
f01009b6:	e8 1c 41 00 00       	call   f0104ad7 <strsplit>
	if (number_of_arguments == 0)
f01009bb:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01009be:	85 c0                	test   %eax,%eax
f01009c0:	75 0a                	jne    f01009cc <execute_command+0x38>
		return 0;
f01009c2:	b8 00 00 00 00       	mov    $0x0,%eax
f01009c7:	e9 93 00 00 00       	jmp    f0100a5f <execute_command+0xcb>
	
	// Lookup in the commands array and execute the command
	int command_found = 0;
f01009cc:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i ;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f01009d3:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
f01009da:	eb 33                	jmp    f0100a0f <execute_command+0x7b>
	{
		if (strcmp(arguments[0], commands[i].name) == 0)
f01009dc:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01009df:	89 d0                	mov    %edx,%eax
f01009e1:	01 c0                	add    %eax,%eax
f01009e3:	01 d0                	add    %edx,%eax
f01009e5:	c1 e0 02             	shl    $0x2,%eax
f01009e8:	05 30 a5 11 f0       	add    $0xf011a530,%eax
f01009ed:	8b 10                	mov    (%eax),%edx
f01009ef:	8b 45 a8             	mov    -0x58(%ebp),%eax
f01009f2:	89 54 24 04          	mov    %edx,0x4(%esp)
f01009f6:	89 04 24             	mov    %eax,(%esp)
f01009f9:	e8 0a 3d 00 00       	call   f0104708 <strcmp>
f01009fe:	85 c0                	test   %eax,%eax
f0100a00:	75 09                	jne    f0100a0b <execute_command+0x77>
		{
			command_found = 1;
f0100a02:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
			break;
f0100a09:	eb 0c                	jmp    f0100a17 <execute_command+0x83>
		return 0;
	
	// Lookup in the commands array and execute the command
	int command_found = 0;
	int i ;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100a0b:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
f0100a0f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100a12:	83 f8 01             	cmp    $0x1,%eax
f0100a15:	76 c5                	jbe    f01009dc <execute_command+0x48>
			command_found = 1;
			break;
		}
	}
	
	if(command_found)
f0100a17:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0100a1b:	74 2a                	je     f0100a47 <execute_command+0xb3>
	{
		int return_value;
		return_value = commands[i].function_to_execute(number_of_arguments, arguments);			
f0100a1d:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100a20:	89 d0                	mov    %edx,%eax
f0100a22:	01 c0                	add    %eax,%eax
f0100a24:	01 d0                	add    %edx,%eax
f0100a26:	c1 e0 02             	shl    $0x2,%eax
f0100a29:	05 38 a5 11 f0       	add    $0xf011a538,%eax
f0100a2e:	8b 00                	mov    (%eax),%eax
f0100a30:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0100a33:	8d 4d a8             	lea    -0x58(%ebp),%ecx
f0100a36:	89 4c 24 04          	mov    %ecx,0x4(%esp)
f0100a3a:	89 14 24             	mov    %edx,(%esp)
f0100a3d:	ff d0                	call   *%eax
f0100a3f:	89 45 ec             	mov    %eax,-0x14(%ebp)
		return return_value;
f0100a42:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100a45:	eb 18                	jmp    f0100a5f <execute_command+0xcb>
	}
	else
	{
		//if not found, then it's unknown command
		cprintf("Unknown command '%s'\n", arguments[0]);
f0100a47:	8b 45 a8             	mov    -0x58(%ebp),%eax
f0100a4a:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100a4e:	c7 04 24 98 50 10 f0 	movl   $0xf0105098,(%esp)
f0100a55:	e8 b1 25 00 00       	call   f010300b <cprintf>
		return 0;
f0100a5a:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f0100a5f:	c9                   	leave  
f0100a60:	c3                   	ret    

f0100a61 <command_help>:

/***** Implementations of basic kernel command prompt commands *****/

//print name and description of each command
int command_help(int number_of_arguments, char **arguments)
{
f0100a61:	55                   	push   %ebp
f0100a62:	89 e5                	mov    %esp,%ebp
f0100a64:	83 ec 28             	sub    $0x28,%esp
	int i;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100a67:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100a6e:	eb 3e                	jmp    f0100aae <command_help+0x4d>
		cprintf("%s - %s\n", commands[i].name, commands[i].description);
f0100a70:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100a73:	89 d0                	mov    %edx,%eax
f0100a75:	01 c0                	add    %eax,%eax
f0100a77:	01 d0                	add    %edx,%eax
f0100a79:	c1 e0 02             	shl    $0x2,%eax
f0100a7c:	05 34 a5 11 f0       	add    $0xf011a534,%eax
f0100a81:	8b 08                	mov    (%eax),%ecx
f0100a83:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100a86:	89 d0                	mov    %edx,%eax
f0100a88:	01 c0                	add    %eax,%eax
f0100a8a:	01 d0                	add    %edx,%eax
f0100a8c:	c1 e0 02             	shl    $0x2,%eax
f0100a8f:	05 30 a5 11 f0       	add    $0xf011a530,%eax
f0100a94:	8b 00                	mov    (%eax),%eax
f0100a96:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0100a9a:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100a9e:	c7 04 24 ae 50 10 f0 	movl   $0xf01050ae,(%esp)
f0100aa5:	e8 61 25 00 00       	call   f010300b <cprintf>

//print name and description of each command
int command_help(int number_of_arguments, char **arguments)
{
	int i;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100aaa:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0100aae:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100ab1:	83 f8 01             	cmp    $0x1,%eax
f0100ab4:	76 ba                	jbe    f0100a70 <command_help+0xf>
		cprintf("%s - %s\n", commands[i].name, commands[i].description);
	
	cprintf("-------------------\n");
f0100ab6:	c7 04 24 b7 50 10 f0 	movl   $0xf01050b7,(%esp)
f0100abd:	e8 49 25 00 00       	call   f010300b <cprintf>
	
	return 0;
f0100ac2:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100ac7:	c9                   	leave  
f0100ac8:	c3                   	ret    

f0100ac9 <command_kernel_info>:

//print information about kernel addresses and kernel size
int command_kernel_info(int number_of_arguments, char **arguments )
{
f0100ac9:	55                   	push   %ebp
f0100aca:	89 e5                	mov    %esp,%ebp
f0100acc:	83 ec 18             	sub    $0x18,%esp
	extern char start_of_kernel[], end_of_kernel_code_section[], start_of_uninitialized_data_section[], end_of_kernel[];

	cprintf("Special kernel symbols:\n");
f0100acf:	c7 04 24 cc 50 10 f0 	movl   $0xf01050cc,(%esp)
f0100ad6:	e8 30 25 00 00       	call   f010300b <cprintf>
	cprintf("  Start Address of the kernel 			%08x (virt)  %08x (phys)\n", start_of_kernel, start_of_kernel - KERNEL_BASE);
f0100adb:	c7 44 24 08 0c 00 10 	movl   $0x10000c,0x8(%esp)
f0100ae2:	00 
f0100ae3:	c7 44 24 04 0c 00 10 	movl   $0xf010000c,0x4(%esp)
f0100aea:	f0 
f0100aeb:	c7 04 24 e8 50 10 f0 	movl   $0xf01050e8,(%esp)
f0100af2:	e8 14 25 00 00       	call   f010300b <cprintf>
	cprintf("  End address of kernel code  			%08x (virt)  %08x (phys)\n", end_of_kernel_code_section, end_of_kernel_code_section - KERNEL_BASE);
f0100af7:	c7 44 24 08 47 4e 10 	movl   $0x104e47,0x8(%esp)
f0100afe:	00 
f0100aff:	c7 44 24 04 47 4e 10 	movl   $0xf0104e47,0x4(%esp)
f0100b06:	f0 
f0100b07:	c7 04 24 24 51 10 f0 	movl   $0xf0105124,(%esp)
f0100b0e:	e8 f8 24 00 00       	call   f010300b <cprintf>
	cprintf("  Start addr. of uninitialized data section 	%08x (virt)  %08x (phys)\n", start_of_uninitialized_data_section, start_of_uninitialized_data_section - KERNEL_BASE);
f0100b13:	c7 44 24 08 ab 58 14 	movl   $0x1458ab,0x8(%esp)
f0100b1a:	00 
f0100b1b:	c7 44 24 04 ab 58 14 	movl   $0xf01458ab,0x4(%esp)
f0100b22:	f0 
f0100b23:	c7 04 24 60 51 10 f0 	movl   $0xf0105160,(%esp)
f0100b2a:	e8 dc 24 00 00       	call   f010300b <cprintf>
	cprintf("  End address of the kernel   			%08x (virt)  %08x (phys)\n", end_of_kernel, end_of_kernel - KERNEL_BASE);
f0100b2f:	c7 44 24 08 ac 63 14 	movl   $0x1463ac,0x8(%esp)
f0100b36:	00 
f0100b37:	c7 44 24 04 ac 63 14 	movl   $0xf01463ac,0x4(%esp)
f0100b3e:	f0 
f0100b3f:	c7 04 24 a8 51 10 f0 	movl   $0xf01051a8,(%esp)
f0100b46:	e8 c0 24 00 00       	call   f010300b <cprintf>
	cprintf("Kernel executable memory footprint: %d KB\n",
		(end_of_kernel-start_of_kernel+1023)/1024);
f0100b4b:	b8 ac 63 14 f0       	mov    $0xf01463ac,%eax
f0100b50:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f0100b56:	b8 0c 00 10 f0       	mov    $0xf010000c,%eax
f0100b5b:	29 c2                	sub    %eax,%edx
f0100b5d:	89 d0                	mov    %edx,%eax
	cprintf("Special kernel symbols:\n");
	cprintf("  Start Address of the kernel 			%08x (virt)  %08x (phys)\n", start_of_kernel, start_of_kernel - KERNEL_BASE);
	cprintf("  End address of kernel code  			%08x (virt)  %08x (phys)\n", end_of_kernel_code_section, end_of_kernel_code_section - KERNEL_BASE);
	cprintf("  Start addr. of uninitialized data section 	%08x (virt)  %08x (phys)\n", start_of_uninitialized_data_section, start_of_uninitialized_data_section - KERNEL_BASE);
	cprintf("  End address of the kernel   			%08x (virt)  %08x (phys)\n", end_of_kernel, end_of_kernel - KERNEL_BASE);
	cprintf("Kernel executable memory footprint: %d KB\n",
f0100b5f:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f0100b65:	85 c0                	test   %eax,%eax
f0100b67:	0f 48 c2             	cmovs  %edx,%eax
f0100b6a:	c1 f8 0a             	sar    $0xa,%eax
f0100b6d:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100b71:	c7 04 24 e4 51 10 f0 	movl   $0xf01051e4,(%esp)
f0100b78:	e8 8e 24 00 00       	call   f010300b <cprintf>
		(end_of_kernel-start_of_kernel+1023)/1024);
	return 0;
f0100b7d:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100b82:	c9                   	leave  
f0100b83:	c3                   	ret    

f0100b84 <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f0100b84:	55                   	push   %ebp
f0100b85:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0100b87:	8b 55 08             	mov    0x8(%ebp),%edx
f0100b8a:	a1 9c 63 14 f0       	mov    0xf014639c,%eax
f0100b8f:	29 c2                	sub    %eax,%edx
f0100b91:	89 d0                	mov    %edx,%eax
f0100b93:	c1 f8 02             	sar    $0x2,%eax
f0100b96:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f0100b9c:	5d                   	pop    %ebp
f0100b9d:	c3                   	ret    

f0100b9e <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f0100b9e:	55                   	push   %ebp
f0100b9f:	89 e5                	mov    %esp,%ebp
f0100ba1:	83 ec 04             	sub    $0x4,%esp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f0100ba4:	8b 45 08             	mov    0x8(%ebp),%eax
f0100ba7:	89 04 24             	mov    %eax,(%esp)
f0100baa:	e8 d5 ff ff ff       	call   f0100b84 <to_frame_number>
f0100baf:	c1 e0 0c             	shl    $0xc,%eax
}
f0100bb2:	c9                   	leave  
f0100bb3:	c3                   	ret    

f0100bb4 <nvram_read>:
{
	sizeof(gdt) - 1, (unsigned long) gdt
};

int nvram_read(int r)
{	
f0100bb4:	55                   	push   %ebp
f0100bb5:	89 e5                	mov    %esp,%ebp
f0100bb7:	53                   	push   %ebx
f0100bb8:	83 ec 14             	sub    $0x14,%esp
	return mc146818_read(r) | (mc146818_read(r + 1) << 8);
f0100bbb:	8b 45 08             	mov    0x8(%ebp),%eax
f0100bbe:	89 04 24             	mov    %eax,(%esp)
f0100bc1:	e8 87 23 00 00       	call   f0102f4d <mc146818_read>
f0100bc6:	89 c3                	mov    %eax,%ebx
f0100bc8:	8b 45 08             	mov    0x8(%ebp),%eax
f0100bcb:	83 c0 01             	add    $0x1,%eax
f0100bce:	89 04 24             	mov    %eax,(%esp)
f0100bd1:	e8 77 23 00 00       	call   f0102f4d <mc146818_read>
f0100bd6:	c1 e0 08             	shl    $0x8,%eax
f0100bd9:	09 d8                	or     %ebx,%eax
}
f0100bdb:	83 c4 14             	add    $0x14,%esp
f0100bde:	5b                   	pop    %ebx
f0100bdf:	5d                   	pop    %ebp
f0100be0:	c3                   	ret    

f0100be1 <detect_memory>:
	
void detect_memory()
{
f0100be1:	55                   	push   %ebp
f0100be2:	89 e5                	mov    %esp,%ebp
f0100be4:	83 ec 28             	sub    $0x28,%esp
	// CMOS tells us how many kilobytes there are
	size_of_base_mem = ROUNDDOWN(nvram_read(NVRAM_BASELO)*1024, PAGE_SIZE);
f0100be7:	c7 04 24 15 00 00 00 	movl   $0x15,(%esp)
f0100bee:	e8 c1 ff ff ff       	call   f0100bb4 <nvram_read>
f0100bf3:	c1 e0 0a             	shl    $0xa,%eax
f0100bf6:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100bf9:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100bfc:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100c01:	a3 94 63 14 f0       	mov    %eax,0xf0146394
	size_of_extended_mem = ROUNDDOWN(nvram_read(NVRAM_EXTLO)*1024, PAGE_SIZE);
f0100c06:	c7 04 24 17 00 00 00 	movl   $0x17,(%esp)
f0100c0d:	e8 a2 ff ff ff       	call   f0100bb4 <nvram_read>
f0100c12:	c1 e0 0a             	shl    $0xa,%eax
f0100c15:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100c18:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100c1b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100c20:	a3 8c 63 14 f0       	mov    %eax,0xf014638c

	// Calculate the maxmium physical address based on whether
	// or not there is any extended memory.  See comment in ../inc/mmu.h.
	if (size_of_extended_mem)
f0100c25:	a1 8c 63 14 f0       	mov    0xf014638c,%eax
f0100c2a:	85 c0                	test   %eax,%eax
f0100c2c:	74 11                	je     f0100c3f <detect_memory+0x5e>
		maxpa = PHYS_EXTENDED_MEM + size_of_extended_mem;
f0100c2e:	a1 8c 63 14 f0       	mov    0xf014638c,%eax
f0100c33:	05 00 00 10 00       	add    $0x100000,%eax
f0100c38:	a3 90 63 14 f0       	mov    %eax,0xf0146390
f0100c3d:	eb 0a                	jmp    f0100c49 <detect_memory+0x68>
	else
		maxpa = size_of_extended_mem;
f0100c3f:	a1 8c 63 14 f0       	mov    0xf014638c,%eax
f0100c44:	a3 90 63 14 f0       	mov    %eax,0xf0146390

	number_of_frames = maxpa / PAGE_SIZE;
f0100c49:	a1 90 63 14 f0       	mov    0xf0146390,%eax
f0100c4e:	c1 e8 0c             	shr    $0xc,%eax
f0100c51:	a3 88 63 14 f0       	mov    %eax,0xf0146388

	cprintf("Physical memory: %dK available, ", (int)(maxpa/1024));
f0100c56:	a1 90 63 14 f0       	mov    0xf0146390,%eax
f0100c5b:	c1 e8 0a             	shr    $0xa,%eax
f0100c5e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100c62:	c7 04 24 10 52 10 f0 	movl   $0xf0105210,(%esp)
f0100c69:	e8 9d 23 00 00       	call   f010300b <cprintf>
	cprintf("base = %dK, extended = %dK\n", (int)(size_of_base_mem/1024), (int)(size_of_extended_mem/1024));
f0100c6e:	a1 8c 63 14 f0       	mov    0xf014638c,%eax
f0100c73:	c1 e8 0a             	shr    $0xa,%eax
f0100c76:	89 c2                	mov    %eax,%edx
f0100c78:	a1 94 63 14 f0       	mov    0xf0146394,%eax
f0100c7d:	c1 e8 0a             	shr    $0xa,%eax
f0100c80:	89 54 24 08          	mov    %edx,0x8(%esp)
f0100c84:	89 44 24 04          	mov    %eax,0x4(%esp)
f0100c88:	c7 04 24 31 52 10 f0 	movl   $0xf0105231,(%esp)
f0100c8f:	e8 77 23 00 00       	call   f010300b <cprintf>
}
f0100c94:	c9                   	leave  
f0100c95:	c3                   	ret    

f0100c96 <check_boot_pgdir>:
// but it is a pretty good check.
//
uint32 check_va2pa(uint32 *ptr_page_directory, uint32 va);

void check_boot_pgdir()
{
f0100c96:	55                   	push   %ebp
f0100c97:	89 e5                	mov    %esp,%ebp
f0100c99:	83 ec 38             	sub    $0x38,%esp
	uint32 i, n;

	// check frames_info array
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
f0100c9c:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f0100ca3:	8b 15 88 63 14 f0    	mov    0xf0146388,%edx
f0100ca9:	89 d0                	mov    %edx,%eax
f0100cab:	01 c0                	add    %eax,%eax
f0100cad:	01 d0                	add    %edx,%eax
f0100caf:	c1 e0 02             	shl    $0x2,%eax
f0100cb2:	89 c2                	mov    %eax,%edx
f0100cb4:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100cb7:	01 d0                	add    %edx,%eax
f0100cb9:	83 e8 01             	sub    $0x1,%eax
f0100cbc:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0100cbf:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100cc2:	ba 00 00 00 00       	mov    $0x0,%edx
f0100cc7:	f7 75 f0             	divl   -0x10(%ebp)
f0100cca:	89 d0                	mov    %edx,%eax
f0100ccc:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0100ccf:	29 c2                	sub    %eax,%edx
f0100cd1:	89 d0                	mov    %edx,%eax
f0100cd3:	89 45 e8             	mov    %eax,-0x18(%ebp)
	for (i = 0; i < n; i += PAGE_SIZE)
f0100cd6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100cdd:	e9 8c 00 00 00       	jmp    f0100d6e <check_boot_pgdir+0xd8>
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);
f0100ce2:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100ce5:	8d 90 00 00 00 ef    	lea    -0x11000000(%eax),%edx
f0100ceb:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100cf0:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100cf4:	89 04 24             	mov    %eax,(%esp)
f0100cf7:	e8 66 02 00 00       	call   f0100f62 <check_va2pa>
f0100cfc:	8b 15 9c 63 14 f0    	mov    0xf014639c,%edx
f0100d02:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f0100d05:	81 7d e4 ff ff ff ef 	cmpl   $0xefffffff,-0x1c(%ebp)
f0100d0c:	77 23                	ja     f0100d31 <check_boot_pgdir+0x9b>
f0100d0e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100d11:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0100d15:	c7 44 24 08 50 52 10 	movl   $0xf0105250,0x8(%esp)
f0100d1c:	f0 
f0100d1d:	c7 44 24 04 5e 00 00 	movl   $0x5e,0x4(%esp)
f0100d24:	00 
f0100d25:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100d2c:	e8 e9 f3 ff ff       	call   f010011a <_panic>
f0100d31:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0100d34:	8d 8a 00 00 00 10    	lea    0x10000000(%edx),%ecx
f0100d3a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100d3d:	01 ca                	add    %ecx,%edx
f0100d3f:	39 d0                	cmp    %edx,%eax
f0100d41:	74 24                	je     f0100d67 <check_boot_pgdir+0xd1>
f0100d43:	c7 44 24 0c 90 52 10 	movl   $0xf0105290,0xc(%esp)
f0100d4a:	f0 
f0100d4b:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100d52:	f0 
f0100d53:	c7 44 24 04 5e 00 00 	movl   $0x5e,0x4(%esp)
f0100d5a:	00 
f0100d5b:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100d62:	e8 b3 f3 ff ff       	call   f010011a <_panic>
{
	uint32 i, n;

	// check frames_info array
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
	for (i = 0; i < n; i += PAGE_SIZE)
f0100d67:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0100d6e:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100d71:	3b 45 e8             	cmp    -0x18(%ebp),%eax
f0100d74:	0f 82 68 ff ff ff    	jb     f0100ce2 <check_boot_pgdir+0x4c>
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);

	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
f0100d7a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100d81:	eb 4a                	jmp    f0100dcd <check_boot_pgdir+0x137>
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);
f0100d83:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100d86:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f0100d8c:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100d91:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100d95:	89 04 24             	mov    %eax,(%esp)
f0100d98:	e8 c5 01 00 00       	call   f0100f62 <check_va2pa>
f0100d9d:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f0100da0:	74 24                	je     f0100dc6 <check_boot_pgdir+0x130>
f0100da2:	c7 44 24 0c 08 53 10 	movl   $0xf0105308,0xc(%esp)
f0100da9:	f0 
f0100daa:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100db1:	f0 
f0100db2:	c7 44 24 04 62 00 00 	movl   $0x62,0x4(%esp)
f0100db9:	00 
f0100dba:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100dc1:	e8 54 f3 ff ff       	call   f010011a <_panic>
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
	for (i = 0; i < n; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);

	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
f0100dc6:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0100dcd:	81 7d f4 00 00 00 10 	cmpl   $0x10000000,-0xc(%ebp)
f0100dd4:	75 ad                	jne    f0100d83 <check_boot_pgdir+0xed>
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);

	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
f0100dd6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100ddd:	e9 8a 00 00 00       	jmp    f0100e6c <check_boot_pgdir+0x1d6>
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);
f0100de2:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100de5:	8d 90 00 80 bf ef    	lea    -0x10408000(%eax),%edx
f0100deb:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100df0:	89 54 24 04          	mov    %edx,0x4(%esp)
f0100df4:	89 04 24             	mov    %eax,(%esp)
f0100df7:	e8 66 01 00 00       	call   f0100f62 <check_va2pa>
f0100dfc:	c7 45 e0 00 20 11 f0 	movl   $0xf0112000,-0x20(%ebp)
f0100e03:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0100e0a:	77 23                	ja     f0100e2f <check_boot_pgdir+0x199>
f0100e0c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100e0f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0100e13:	c7 44 24 08 50 52 10 	movl   $0xf0105250,0x8(%esp)
f0100e1a:	f0 
f0100e1b:	c7 44 24 04 66 00 00 	movl   $0x66,0x4(%esp)
f0100e22:	00 
f0100e23:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100e2a:	e8 eb f2 ff ff       	call   f010011a <_panic>
f0100e2f:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0100e32:	8d 8a 00 00 00 10    	lea    0x10000000(%edx),%ecx
f0100e38:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100e3b:	01 ca                	add    %ecx,%edx
f0100e3d:	39 d0                	cmp    %edx,%eax
f0100e3f:	74 24                	je     f0100e65 <check_boot_pgdir+0x1cf>
f0100e41:	c7 44 24 0c 40 53 10 	movl   $0xf0105340,0xc(%esp)
f0100e48:	f0 
f0100e49:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100e50:	f0 
f0100e51:	c7 44 24 04 66 00 00 	movl   $0x66,0x4(%esp)
f0100e58:	00 
f0100e59:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100e60:	e8 b5 f2 ff ff       	call   f010011a <_panic>
	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);

	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
f0100e65:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0100e6c:	81 7d f4 ff 7f 00 00 	cmpl   $0x7fff,-0xc(%ebp)
f0100e73:	0f 86 69 ff ff ff    	jbe    f0100de2 <check_boot_pgdir+0x14c>
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);

	// check for zero/non-zero in PDEs
	for (i = 0; i < NPDENTRIES; i++) {
f0100e79:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100e80:	e9 c2 00 00 00       	jmp    f0100f47 <check_boot_pgdir+0x2b1>
		switch (i) {
f0100e85:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100e88:	2d bb 03 00 00       	sub    $0x3bb,%eax
f0100e8d:	83 f8 04             	cmp    $0x4,%eax
f0100e90:	77 39                	ja     f0100ecb <check_boot_pgdir+0x235>
		case PDX(VPT):
		case PDX(UVPT):
		case PDX(KERNEL_STACK_TOP-1):
		case PDX(UENVS):
		case PDX(READ_ONLY_FRAMES_INFO):			
			assert(ptr_page_directory[i]);
f0100e92:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100e97:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100e9a:	c1 e2 02             	shl    $0x2,%edx
f0100e9d:	01 d0                	add    %edx,%eax
f0100e9f:	8b 00                	mov    (%eax),%eax
f0100ea1:	85 c0                	test   %eax,%eax
f0100ea3:	75 24                	jne    f0100ec9 <check_boot_pgdir+0x233>
f0100ea5:	c7 44 24 0c b6 53 10 	movl   $0xf01053b6,0xc(%esp)
f0100eac:	f0 
f0100ead:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100eb4:	f0 
f0100eb5:	c7 44 24 04 70 00 00 	movl   $0x70,0x4(%esp)
f0100ebc:	00 
f0100ebd:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100ec4:	e8 51 f2 ff ff       	call   f010011a <_panic>
			break;
f0100ec9:	eb 78                	jmp    f0100f43 <check_boot_pgdir+0x2ad>
		default:
			if (i >= PDX(KERNEL_BASE))
f0100ecb:	81 7d f4 bf 03 00 00 	cmpl   $0x3bf,-0xc(%ebp)
f0100ed2:	76 37                	jbe    f0100f0b <check_boot_pgdir+0x275>
				assert(ptr_page_directory[i]);
f0100ed4:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100ed9:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100edc:	c1 e2 02             	shl    $0x2,%edx
f0100edf:	01 d0                	add    %edx,%eax
f0100ee1:	8b 00                	mov    (%eax),%eax
f0100ee3:	85 c0                	test   %eax,%eax
f0100ee5:	75 5b                	jne    f0100f42 <check_boot_pgdir+0x2ac>
f0100ee7:	c7 44 24 0c b6 53 10 	movl   $0xf01053b6,0xc(%esp)
f0100eee:	f0 
f0100eef:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100ef6:	f0 
f0100ef7:	c7 44 24 04 74 00 00 	movl   $0x74,0x4(%esp)
f0100efe:	00 
f0100eff:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100f06:	e8 0f f2 ff ff       	call   f010011a <_panic>
			else				
				assert(ptr_page_directory[i] == 0);
f0100f0b:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0100f10:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100f13:	c1 e2 02             	shl    $0x2,%edx
f0100f16:	01 d0                	add    %edx,%eax
f0100f18:	8b 00                	mov    (%eax),%eax
f0100f1a:	85 c0                	test   %eax,%eax
f0100f1c:	74 24                	je     f0100f42 <check_boot_pgdir+0x2ac>
f0100f1e:	c7 44 24 0c cc 53 10 	movl   $0xf01053cc,0xc(%esp)
f0100f25:	f0 
f0100f26:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0100f2d:	f0 
f0100f2e:	c7 44 24 04 76 00 00 	movl   $0x76,0x4(%esp)
f0100f35:	00 
f0100f36:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100f3d:	e8 d8 f1 ff ff       	call   f010011a <_panic>
			break;
f0100f42:	90                   	nop
	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);

	// check for zero/non-zero in PDEs
	for (i = 0; i < NPDENTRIES; i++) {
f0100f43:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0100f47:	81 7d f4 ff 03 00 00 	cmpl   $0x3ff,-0xc(%ebp)
f0100f4e:	0f 86 31 ff ff ff    	jbe    f0100e85 <check_boot_pgdir+0x1ef>
			else				
				assert(ptr_page_directory[i] == 0);
			break;
		}
	}
	cprintf("check_boot_pgdir() succeeded!\n");
f0100f54:	c7 04 24 e8 53 10 f0 	movl   $0xf01053e8,(%esp)
f0100f5b:	e8 ab 20 00 00       	call   f010300b <cprintf>
}
f0100f60:	c9                   	leave  
f0100f61:	c3                   	ret    

f0100f62 <check_va2pa>:
// defined by the page directory 'ptr_page_directory'.  The hardware normally performs
// this functionality for us!  We define our own version to help check
// the check_boot_pgdir() function; it shouldn't be used elsewhere.

uint32 check_va2pa(uint32 *ptr_page_directory, uint32 va)
{
f0100f62:	55                   	push   %ebp
f0100f63:	89 e5                	mov    %esp,%ebp
f0100f65:	83 ec 28             	sub    $0x28,%esp
	uint32 *p;

	ptr_page_directory = &ptr_page_directory[PDX(va)];
f0100f68:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100f6b:	c1 e8 16             	shr    $0x16,%eax
f0100f6e:	c1 e0 02             	shl    $0x2,%eax
f0100f71:	01 45 08             	add    %eax,0x8(%ebp)
	if (!(*ptr_page_directory & PERM_PRESENT))
f0100f74:	8b 45 08             	mov    0x8(%ebp),%eax
f0100f77:	8b 00                	mov    (%eax),%eax
f0100f79:	83 e0 01             	and    $0x1,%eax
f0100f7c:	85 c0                	test   %eax,%eax
f0100f7e:	75 0a                	jne    f0100f8a <check_va2pa+0x28>
		return ~0;
f0100f80:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100f85:	e9 93 00 00 00       	jmp    f010101d <check_va2pa+0xbb>
	p = (uint32*) K_VIRTUAL_ADDRESS(EXTRACT_ADDRESS(*ptr_page_directory));
f0100f8a:	8b 45 08             	mov    0x8(%ebp),%eax
f0100f8d:	8b 00                	mov    (%eax),%eax
f0100f8f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100f94:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100f97:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100f9a:	c1 e8 0c             	shr    $0xc,%eax
f0100f9d:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100fa0:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f0100fa5:	39 45 f0             	cmp    %eax,-0x10(%ebp)
f0100fa8:	72 23                	jb     f0100fcd <check_va2pa+0x6b>
f0100faa:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100fad:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0100fb1:	c7 44 24 08 08 54 10 	movl   $0xf0105408,0x8(%esp)
f0100fb8:	f0 
f0100fb9:	c7 44 24 04 89 00 00 	movl   $0x89,0x4(%esp)
f0100fc0:	00 
f0100fc1:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0100fc8:	e8 4d f1 ff ff       	call   f010011a <_panic>
f0100fcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100fd0:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0100fd5:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if (!(p[PTX(va)] & PERM_PRESENT))
f0100fd8:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100fdb:	c1 e8 0c             	shr    $0xc,%eax
f0100fde:	25 ff 03 00 00       	and    $0x3ff,%eax
f0100fe3:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0100fea:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100fed:	01 d0                	add    %edx,%eax
f0100fef:	8b 00                	mov    (%eax),%eax
f0100ff1:	83 e0 01             	and    $0x1,%eax
f0100ff4:	85 c0                	test   %eax,%eax
f0100ff6:	75 07                	jne    f0100fff <check_va2pa+0x9d>
		return ~0;
f0100ff8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100ffd:	eb 1e                	jmp    f010101d <check_va2pa+0xbb>
	return EXTRACT_ADDRESS(p[PTX(va)]);
f0100fff:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101002:	c1 e8 0c             	shr    $0xc,%eax
f0101005:	25 ff 03 00 00       	and    $0x3ff,%eax
f010100a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0101011:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101014:	01 d0                	add    %edx,%eax
f0101016:	8b 00                	mov    (%eax),%eax
f0101018:	25 00 f0 ff ff       	and    $0xfffff000,%eax
}
f010101d:	c9                   	leave  
f010101e:	c3                   	ret    

f010101f <tlb_invalidate>:
		
void tlb_invalidate(uint32 *ptr_page_directory, void *virtual_address)
{
f010101f:	55                   	push   %ebp
f0101020:	89 e5                	mov    %esp,%ebp
f0101022:	83 ec 10             	sub    $0x10,%esp
f0101025:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101028:	89 45 fc             	mov    %eax,-0x4(%ebp)
}

static __inline void 
invlpg(void *addr)
{ 
	__asm __volatile("invlpg (%0)" : : "r" (addr) : "memory");
f010102b:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010102e:	0f 01 38             	invlpg (%eax)
	// Flush the entry only if we're modifying the current address space.
	// For now, there is only one address space, so always invalidate.
	invlpg(virtual_address);
}
f0101031:	c9                   	leave  
f0101032:	c3                   	ret    

f0101033 <page_check>:

void page_check()
{
f0101033:	55                   	push   %ebp
f0101034:	89 e5                	mov    %esp,%ebp
f0101036:	53                   	push   %ebx
f0101037:	83 ec 34             	sub    $0x34,%esp
	struct Frame_Info *pp, *pp0, *pp1, *pp2;
	struct Linked_List fl;

	// should be able to allocate three frames_info
	pp0 = pp1 = pp2 = 0;
f010103a:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
f0101041:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101044:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101047:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010104a:	89 45 f0             	mov    %eax,-0x10(%ebp)
	assert(allocate_frame(&pp0) == 0);
f010104d:	8d 45 f0             	lea    -0x10(%ebp),%eax
f0101050:	89 04 24             	mov    %eax,(%esp)
f0101053:	e8 4c 13 00 00       	call   f01023a4 <allocate_frame>
f0101058:	85 c0                	test   %eax,%eax
f010105a:	74 24                	je     f0101080 <page_check+0x4d>
f010105c:	c7 44 24 0c 37 54 10 	movl   $0xf0105437,0xc(%esp)
f0101063:	f0 
f0101064:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010106b:	f0 
f010106c:	c7 44 24 04 9d 00 00 	movl   $0x9d,0x4(%esp)
f0101073:	00 
f0101074:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010107b:	e8 9a f0 ff ff       	call   f010011a <_panic>
	assert(allocate_frame(&pp1) == 0);
f0101080:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0101083:	89 04 24             	mov    %eax,(%esp)
f0101086:	e8 19 13 00 00       	call   f01023a4 <allocate_frame>
f010108b:	85 c0                	test   %eax,%eax
f010108d:	74 24                	je     f01010b3 <page_check+0x80>
f010108f:	c7 44 24 0c 51 54 10 	movl   $0xf0105451,0xc(%esp)
f0101096:	f0 
f0101097:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010109e:	f0 
f010109f:	c7 44 24 04 9e 00 00 	movl   $0x9e,0x4(%esp)
f01010a6:	00 
f01010a7:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01010ae:	e8 67 f0 ff ff       	call   f010011a <_panic>
	assert(allocate_frame(&pp2) == 0);
f01010b3:	8d 45 e8             	lea    -0x18(%ebp),%eax
f01010b6:	89 04 24             	mov    %eax,(%esp)
f01010b9:	e8 e6 12 00 00       	call   f01023a4 <allocate_frame>
f01010be:	85 c0                	test   %eax,%eax
f01010c0:	74 24                	je     f01010e6 <page_check+0xb3>
f01010c2:	c7 44 24 0c 6b 54 10 	movl   $0xf010546b,0xc(%esp)
f01010c9:	f0 
f01010ca:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01010d1:	f0 
f01010d2:	c7 44 24 04 9f 00 00 	movl   $0x9f,0x4(%esp)
f01010d9:	00 
f01010da:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01010e1:	e8 34 f0 ff ff       	call   f010011a <_panic>

	assert(pp0);
f01010e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01010e9:	85 c0                	test   %eax,%eax
f01010eb:	75 24                	jne    f0101111 <page_check+0xde>
f01010ed:	c7 44 24 0c 85 54 10 	movl   $0xf0105485,0xc(%esp)
f01010f4:	f0 
f01010f5:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01010fc:	f0 
f01010fd:	c7 44 24 04 a1 00 00 	movl   $0xa1,0x4(%esp)
f0101104:	00 
f0101105:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010110c:	e8 09 f0 ff ff       	call   f010011a <_panic>
	assert(pp1 && pp1 != pp0);
f0101111:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101114:	85 c0                	test   %eax,%eax
f0101116:	74 0a                	je     f0101122 <page_check+0xef>
f0101118:	8b 55 ec             	mov    -0x14(%ebp),%edx
f010111b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010111e:	39 c2                	cmp    %eax,%edx
f0101120:	75 24                	jne    f0101146 <page_check+0x113>
f0101122:	c7 44 24 0c 89 54 10 	movl   $0xf0105489,0xc(%esp)
f0101129:	f0 
f010112a:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101131:	f0 
f0101132:	c7 44 24 04 a2 00 00 	movl   $0xa2,0x4(%esp)
f0101139:	00 
f010113a:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101141:	e8 d4 ef ff ff       	call   f010011a <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f0101146:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101149:	85 c0                	test   %eax,%eax
f010114b:	74 14                	je     f0101161 <page_check+0x12e>
f010114d:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0101150:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101153:	39 c2                	cmp    %eax,%edx
f0101155:	74 0a                	je     f0101161 <page_check+0x12e>
f0101157:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010115a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010115d:	39 c2                	cmp    %eax,%edx
f010115f:	75 24                	jne    f0101185 <page_check+0x152>
f0101161:	c7 44 24 0c 9c 54 10 	movl   $0xf010549c,0xc(%esp)
f0101168:	f0 
f0101169:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101170:	f0 
f0101171:	c7 44 24 04 a3 00 00 	movl   $0xa3,0x4(%esp)
f0101178:	00 
f0101179:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101180:	e8 95 ef ff ff       	call   f010011a <_panic>

	// temporarily steal the rest of the free frames_info
	fl = free_frame_list;
f0101185:	a1 98 63 14 f0       	mov    0xf0146398,%eax
f010118a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	LIST_INIT(&free_frame_list);
f010118d:	c7 05 98 63 14 f0 00 	movl   $0x0,0xf0146398
f0101194:	00 00 00 

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f0101197:	8d 45 f4             	lea    -0xc(%ebp),%eax
f010119a:	89 04 24             	mov    %eax,(%esp)
f010119d:	e8 02 12 00 00       	call   f01023a4 <allocate_frame>
f01011a2:	83 f8 fc             	cmp    $0xfffffffc,%eax
f01011a5:	74 24                	je     f01011cb <page_check+0x198>
f01011a7:	c7 44 24 0c bc 54 10 	movl   $0xf01054bc,0xc(%esp)
f01011ae:	f0 
f01011af:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01011b6:	f0 
f01011b7:	c7 44 24 04 aa 00 00 	movl   $0xaa,0x4(%esp)
f01011be:	00 
f01011bf:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01011c6:	e8 4f ef ff ff       	call   f010011a <_panic>

	// there is no free memory, so we can't allocate a page table 
	assert(map_frame(ptr_page_directory, pp1, 0x0, 0) < 0);
f01011cb:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01011ce:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01011d3:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f01011da:	00 
f01011db:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
f01011e2:	00 
f01011e3:	89 54 24 04          	mov    %edx,0x4(%esp)
f01011e7:	89 04 24             	mov    %eax,(%esp)
f01011ea:	e8 d8 13 00 00       	call   f01025c7 <map_frame>
f01011ef:	85 c0                	test   %eax,%eax
f01011f1:	78 24                	js     f0101217 <page_check+0x1e4>
f01011f3:	c7 44 24 0c dc 54 10 	movl   $0xf01054dc,0xc(%esp)
f01011fa:	f0 
f01011fb:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101202:	f0 
f0101203:	c7 44 24 04 ad 00 00 	movl   $0xad,0x4(%esp)
f010120a:	00 
f010120b:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101212:	e8 03 ef ff ff       	call   f010011a <_panic>

	// free pp0 and try again: pp0 should be used for page table
	free_frame(pp0);
f0101217:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010121a:	89 04 24             	mov    %eax,(%esp)
f010121d:	e8 e5 11 00 00       	call   f0102407 <free_frame>
	assert(map_frame(ptr_page_directory, pp1, 0x0, 0) == 0);
f0101222:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0101225:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f010122a:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0101231:	00 
f0101232:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
f0101239:	00 
f010123a:	89 54 24 04          	mov    %edx,0x4(%esp)
f010123e:	89 04 24             	mov    %eax,(%esp)
f0101241:	e8 81 13 00 00       	call   f01025c7 <map_frame>
f0101246:	85 c0                	test   %eax,%eax
f0101248:	74 24                	je     f010126e <page_check+0x23b>
f010124a:	c7 44 24 0c 0c 55 10 	movl   $0xf010550c,0xc(%esp)
f0101251:	f0 
f0101252:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101259:	f0 
f010125a:	c7 44 24 04 b1 00 00 	movl   $0xb1,0x4(%esp)
f0101261:	00 
f0101262:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101269:	e8 ac ee ff ff       	call   f010011a <_panic>
	assert(EXTRACT_ADDRESS(ptr_page_directory[0]) == to_physical_address(pp0));
f010126e:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101273:	8b 00                	mov    (%eax),%eax
f0101275:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f010127a:	89 c3                	mov    %eax,%ebx
f010127c:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010127f:	89 04 24             	mov    %eax,(%esp)
f0101282:	e8 17 f9 ff ff       	call   f0100b9e <to_physical_address>
f0101287:	39 c3                	cmp    %eax,%ebx
f0101289:	74 24                	je     f01012af <page_check+0x27c>
f010128b:	c7 44 24 0c 3c 55 10 	movl   $0xf010553c,0xc(%esp)
f0101292:	f0 
f0101293:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010129a:	f0 
f010129b:	c7 44 24 04 b2 00 00 	movl   $0xb2,0x4(%esp)
f01012a2:	00 
f01012a3:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01012aa:	e8 6b ee ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, 0x0) == to_physical_address(pp1));
f01012af:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01012b4:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f01012bb:	00 
f01012bc:	89 04 24             	mov    %eax,(%esp)
f01012bf:	e8 9e fc ff ff       	call   f0100f62 <check_va2pa>
f01012c4:	89 c3                	mov    %eax,%ebx
f01012c6:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01012c9:	89 04 24             	mov    %eax,(%esp)
f01012cc:	e8 cd f8 ff ff       	call   f0100b9e <to_physical_address>
f01012d1:	39 c3                	cmp    %eax,%ebx
f01012d3:	74 24                	je     f01012f9 <page_check+0x2c6>
f01012d5:	c7 44 24 0c 80 55 10 	movl   $0xf0105580,0xc(%esp)
f01012dc:	f0 
f01012dd:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01012e4:	f0 
f01012e5:	c7 44 24 04 b3 00 00 	movl   $0xb3,0x4(%esp)
f01012ec:	00 
f01012ed:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01012f4:	e8 21 ee ff ff       	call   f010011a <_panic>
	assert(pp1->references == 1);
f01012f9:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01012fc:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101300:	66 83 f8 01          	cmp    $0x1,%ax
f0101304:	74 24                	je     f010132a <page_check+0x2f7>
f0101306:	c7 44 24 0c c1 55 10 	movl   $0xf01055c1,0xc(%esp)
f010130d:	f0 
f010130e:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101315:	f0 
f0101316:	c7 44 24 04 b4 00 00 	movl   $0xb4,0x4(%esp)
f010131d:	00 
f010131e:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101325:	e8 f0 ed ff ff       	call   f010011a <_panic>
	assert(pp0->references == 1);
f010132a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010132d:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101331:	66 83 f8 01          	cmp    $0x1,%ax
f0101335:	74 24                	je     f010135b <page_check+0x328>
f0101337:	c7 44 24 0c d6 55 10 	movl   $0xf01055d6,0xc(%esp)
f010133e:	f0 
f010133f:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101346:	f0 
f0101347:	c7 44 24 04 b5 00 00 	movl   $0xb5,0x4(%esp)
f010134e:	00 
f010134f:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101356:	e8 bf ed ff ff       	call   f010011a <_panic>

	// should be able to map pp2 at PAGE_SIZE because pp0 is already allocated for page table
	assert(map_frame(ptr_page_directory, pp2, (void*) PAGE_SIZE, 0) == 0);
f010135b:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010135e:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101363:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f010136a:	00 
f010136b:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f0101372:	00 
f0101373:	89 54 24 04          	mov    %edx,0x4(%esp)
f0101377:	89 04 24             	mov    %eax,(%esp)
f010137a:	e8 48 12 00 00       	call   f01025c7 <map_frame>
f010137f:	85 c0                	test   %eax,%eax
f0101381:	74 24                	je     f01013a7 <page_check+0x374>
f0101383:	c7 44 24 0c ec 55 10 	movl   $0xf01055ec,0xc(%esp)
f010138a:	f0 
f010138b:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101392:	f0 
f0101393:	c7 44 24 04 b8 00 00 	movl   $0xb8,0x4(%esp)
f010139a:	00 
f010139b:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01013a2:	e8 73 ed ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp2));
f01013a7:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01013ac:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f01013b3:	00 
f01013b4:	89 04 24             	mov    %eax,(%esp)
f01013b7:	e8 a6 fb ff ff       	call   f0100f62 <check_va2pa>
f01013bc:	89 c3                	mov    %eax,%ebx
f01013be:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01013c1:	89 04 24             	mov    %eax,(%esp)
f01013c4:	e8 d5 f7 ff ff       	call   f0100b9e <to_physical_address>
f01013c9:	39 c3                	cmp    %eax,%ebx
f01013cb:	74 24                	je     f01013f1 <page_check+0x3be>
f01013cd:	c7 44 24 0c 2c 56 10 	movl   $0xf010562c,0xc(%esp)
f01013d4:	f0 
f01013d5:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01013dc:	f0 
f01013dd:	c7 44 24 04 b9 00 00 	movl   $0xb9,0x4(%esp)
f01013e4:	00 
f01013e5:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01013ec:	e8 29 ed ff ff       	call   f010011a <_panic>
	assert(pp2->references == 1);
f01013f1:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01013f4:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01013f8:	66 83 f8 01          	cmp    $0x1,%ax
f01013fc:	74 24                	je     f0101422 <page_check+0x3ef>
f01013fe:	c7 44 24 0c 73 56 10 	movl   $0xf0105673,0xc(%esp)
f0101405:	f0 
f0101406:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010140d:	f0 
f010140e:	c7 44 24 04 ba 00 00 	movl   $0xba,0x4(%esp)
f0101415:	00 
f0101416:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010141d:	e8 f8 ec ff ff       	call   f010011a <_panic>

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f0101422:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0101425:	89 04 24             	mov    %eax,(%esp)
f0101428:	e8 77 0f 00 00       	call   f01023a4 <allocate_frame>
f010142d:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0101430:	74 24                	je     f0101456 <page_check+0x423>
f0101432:	c7 44 24 0c bc 54 10 	movl   $0xf01054bc,0xc(%esp)
f0101439:	f0 
f010143a:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101441:	f0 
f0101442:	c7 44 24 04 bd 00 00 	movl   $0xbd,0x4(%esp)
f0101449:	00 
f010144a:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101451:	e8 c4 ec ff ff       	call   f010011a <_panic>

	// should be able to map pp2 at PAGE_SIZE because it's already there
	assert(map_frame(ptr_page_directory, pp2, (void*) PAGE_SIZE, 0) == 0);
f0101456:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0101459:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f010145e:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0101465:	00 
f0101466:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f010146d:	00 
f010146e:	89 54 24 04          	mov    %edx,0x4(%esp)
f0101472:	89 04 24             	mov    %eax,(%esp)
f0101475:	e8 4d 11 00 00       	call   f01025c7 <map_frame>
f010147a:	85 c0                	test   %eax,%eax
f010147c:	74 24                	je     f01014a2 <page_check+0x46f>
f010147e:	c7 44 24 0c ec 55 10 	movl   $0xf01055ec,0xc(%esp)
f0101485:	f0 
f0101486:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010148d:	f0 
f010148e:	c7 44 24 04 c0 00 00 	movl   $0xc0,0x4(%esp)
f0101495:	00 
f0101496:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010149d:	e8 78 ec ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp2));
f01014a2:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01014a7:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f01014ae:	00 
f01014af:	89 04 24             	mov    %eax,(%esp)
f01014b2:	e8 ab fa ff ff       	call   f0100f62 <check_va2pa>
f01014b7:	89 c3                	mov    %eax,%ebx
f01014b9:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01014bc:	89 04 24             	mov    %eax,(%esp)
f01014bf:	e8 da f6 ff ff       	call   f0100b9e <to_physical_address>
f01014c4:	39 c3                	cmp    %eax,%ebx
f01014c6:	74 24                	je     f01014ec <page_check+0x4b9>
f01014c8:	c7 44 24 0c 2c 56 10 	movl   $0xf010562c,0xc(%esp)
f01014cf:	f0 
f01014d0:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01014d7:	f0 
f01014d8:	c7 44 24 04 c1 00 00 	movl   $0xc1,0x4(%esp)
f01014df:	00 
f01014e0:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01014e7:	e8 2e ec ff ff       	call   f010011a <_panic>
	assert(pp2->references == 1);
f01014ec:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01014ef:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01014f3:	66 83 f8 01          	cmp    $0x1,%ax
f01014f7:	74 24                	je     f010151d <page_check+0x4ea>
f01014f9:	c7 44 24 0c 73 56 10 	movl   $0xf0105673,0xc(%esp)
f0101500:	f0 
f0101501:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101508:	f0 
f0101509:	c7 44 24 04 c2 00 00 	movl   $0xc2,0x4(%esp)
f0101510:	00 
f0101511:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101518:	e8 fd eb ff ff       	call   f010011a <_panic>

	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in map_frame
	assert(allocate_frame(&pp) == E_NO_MEM);
f010151d:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0101520:	89 04 24             	mov    %eax,(%esp)
f0101523:	e8 7c 0e 00 00       	call   f01023a4 <allocate_frame>
f0101528:	83 f8 fc             	cmp    $0xfffffffc,%eax
f010152b:	74 24                	je     f0101551 <page_check+0x51e>
f010152d:	c7 44 24 0c bc 54 10 	movl   $0xf01054bc,0xc(%esp)
f0101534:	f0 
f0101535:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010153c:	f0 
f010153d:	c7 44 24 04 c6 00 00 	movl   $0xc6,0x4(%esp)
f0101544:	00 
f0101545:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010154c:	e8 c9 eb ff ff       	call   f010011a <_panic>

	// should not be able to map at PTSIZE because need free frame for page table
	assert(map_frame(ptr_page_directory, pp0, (void*) PTSIZE, 0) < 0);
f0101551:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0101554:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101559:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0101560:	00 
f0101561:	c7 44 24 08 00 00 40 	movl   $0x400000,0x8(%esp)
f0101568:	00 
f0101569:	89 54 24 04          	mov    %edx,0x4(%esp)
f010156d:	89 04 24             	mov    %eax,(%esp)
f0101570:	e8 52 10 00 00       	call   f01025c7 <map_frame>
f0101575:	85 c0                	test   %eax,%eax
f0101577:	78 24                	js     f010159d <page_check+0x56a>
f0101579:	c7 44 24 0c 88 56 10 	movl   $0xf0105688,0xc(%esp)
f0101580:	f0 
f0101581:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101588:	f0 
f0101589:	c7 44 24 04 c9 00 00 	movl   $0xc9,0x4(%esp)
f0101590:	00 
f0101591:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101598:	e8 7d eb ff ff       	call   f010011a <_panic>

	// insert pp1 at PAGE_SIZE (replacing pp2)
	assert(map_frame(ptr_page_directory, pp1, (void*) PAGE_SIZE, 0) == 0);
f010159d:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01015a0:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01015a5:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f01015ac:	00 
f01015ad:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f01015b4:	00 
f01015b5:	89 54 24 04          	mov    %edx,0x4(%esp)
f01015b9:	89 04 24             	mov    %eax,(%esp)
f01015bc:	e8 06 10 00 00       	call   f01025c7 <map_frame>
f01015c1:	85 c0                	test   %eax,%eax
f01015c3:	74 24                	je     f01015e9 <page_check+0x5b6>
f01015c5:	c7 44 24 0c c4 56 10 	movl   $0xf01056c4,0xc(%esp)
f01015cc:	f0 
f01015cd:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01015d4:	f0 
f01015d5:	c7 44 24 04 cc 00 00 	movl   $0xcc,0x4(%esp)
f01015dc:	00 
f01015dd:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01015e4:	e8 31 eb ff ff       	call   f010011a <_panic>

	// should have pp1 at both 0 and PAGE_SIZE, pp2 nowhere, ...
	assert(check_va2pa(ptr_page_directory, 0) == to_physical_address(pp1));
f01015e9:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01015ee:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f01015f5:	00 
f01015f6:	89 04 24             	mov    %eax,(%esp)
f01015f9:	e8 64 f9 ff ff       	call   f0100f62 <check_va2pa>
f01015fe:	89 c3                	mov    %eax,%ebx
f0101600:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101603:	89 04 24             	mov    %eax,(%esp)
f0101606:	e8 93 f5 ff ff       	call   f0100b9e <to_physical_address>
f010160b:	39 c3                	cmp    %eax,%ebx
f010160d:	74 24                	je     f0101633 <page_check+0x600>
f010160f:	c7 44 24 0c 04 57 10 	movl   $0xf0105704,0xc(%esp)
f0101616:	f0 
f0101617:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f010161e:	f0 
f010161f:	c7 44 24 04 cf 00 00 	movl   $0xcf,0x4(%esp)
f0101626:	00 
f0101627:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f010162e:	e8 e7 ea ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp1));
f0101633:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101638:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f010163f:	00 
f0101640:	89 04 24             	mov    %eax,(%esp)
f0101643:	e8 1a f9 ff ff       	call   f0100f62 <check_va2pa>
f0101648:	89 c3                	mov    %eax,%ebx
f010164a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010164d:	89 04 24             	mov    %eax,(%esp)
f0101650:	e8 49 f5 ff ff       	call   f0100b9e <to_physical_address>
f0101655:	39 c3                	cmp    %eax,%ebx
f0101657:	74 24                	je     f010167d <page_check+0x64a>
f0101659:	c7 44 24 0c 44 57 10 	movl   $0xf0105744,0xc(%esp)
f0101660:	f0 
f0101661:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101668:	f0 
f0101669:	c7 44 24 04 d0 00 00 	movl   $0xd0,0x4(%esp)
f0101670:	00 
f0101671:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101678:	e8 9d ea ff ff       	call   f010011a <_panic>
	// ... and ref counts should reflect this
	assert(pp1->references == 2);
f010167d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101680:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101684:	66 83 f8 02          	cmp    $0x2,%ax
f0101688:	74 24                	je     f01016ae <page_check+0x67b>
f010168a:	c7 44 24 0c 8b 57 10 	movl   $0xf010578b,0xc(%esp)
f0101691:	f0 
f0101692:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101699:	f0 
f010169a:	c7 44 24 04 d2 00 00 	movl   $0xd2,0x4(%esp)
f01016a1:	00 
f01016a2:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01016a9:	e8 6c ea ff ff       	call   f010011a <_panic>
	assert(pp2->references == 0);
f01016ae:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01016b1:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01016b5:	66 85 c0             	test   %ax,%ax
f01016b8:	74 24                	je     f01016de <page_check+0x6ab>
f01016ba:	c7 44 24 0c a0 57 10 	movl   $0xf01057a0,0xc(%esp)
f01016c1:	f0 
f01016c2:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01016c9:	f0 
f01016ca:	c7 44 24 04 d3 00 00 	movl   $0xd3,0x4(%esp)
f01016d1:	00 
f01016d2:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01016d9:	e8 3c ea ff ff       	call   f010011a <_panic>

	// pp2 should be returned by allocate_frame
	assert(allocate_frame(&pp) == 0 && pp == pp2);
f01016de:	8d 45 f4             	lea    -0xc(%ebp),%eax
f01016e1:	89 04 24             	mov    %eax,(%esp)
f01016e4:	e8 bb 0c 00 00       	call   f01023a4 <allocate_frame>
f01016e9:	85 c0                	test   %eax,%eax
f01016eb:	75 0a                	jne    f01016f7 <page_check+0x6c4>
f01016ed:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01016f0:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01016f3:	39 c2                	cmp    %eax,%edx
f01016f5:	74 24                	je     f010171b <page_check+0x6e8>
f01016f7:	c7 44 24 0c b8 57 10 	movl   $0xf01057b8,0xc(%esp)
f01016fe:	f0 
f01016ff:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101706:	f0 
f0101707:	c7 44 24 04 d6 00 00 	movl   $0xd6,0x4(%esp)
f010170e:	00 
f010170f:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101716:	e8 ff e9 ff ff       	call   f010011a <_panic>

	// unmapping pp1 at 0 should keep pp1 at PAGE_SIZE
	unmap_frame(ptr_page_directory, 0x0);
f010171b:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101720:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0101727:	00 
f0101728:	89 04 24             	mov    %eax,(%esp)
f010172b:	e8 c5 0f 00 00       	call   f01026f5 <unmap_frame>
	assert(check_va2pa(ptr_page_directory, 0x0) == ~0);
f0101730:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101735:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f010173c:	00 
f010173d:	89 04 24             	mov    %eax,(%esp)
f0101740:	e8 1d f8 ff ff       	call   f0100f62 <check_va2pa>
f0101745:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101748:	74 24                	je     f010176e <page_check+0x73b>
f010174a:	c7 44 24 0c e0 57 10 	movl   $0xf01057e0,0xc(%esp)
f0101751:	f0 
f0101752:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101759:	f0 
f010175a:	c7 44 24 04 da 00 00 	movl   $0xda,0x4(%esp)
f0101761:	00 
f0101762:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101769:	e8 ac e9 ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp1));
f010176e:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101773:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f010177a:	00 
f010177b:	89 04 24             	mov    %eax,(%esp)
f010177e:	e8 df f7 ff ff       	call   f0100f62 <check_va2pa>
f0101783:	89 c3                	mov    %eax,%ebx
f0101785:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101788:	89 04 24             	mov    %eax,(%esp)
f010178b:	e8 0e f4 ff ff       	call   f0100b9e <to_physical_address>
f0101790:	39 c3                	cmp    %eax,%ebx
f0101792:	74 24                	je     f01017b8 <page_check+0x785>
f0101794:	c7 44 24 0c 44 57 10 	movl   $0xf0105744,0xc(%esp)
f010179b:	f0 
f010179c:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01017a3:	f0 
f01017a4:	c7 44 24 04 db 00 00 	movl   $0xdb,0x4(%esp)
f01017ab:	00 
f01017ac:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01017b3:	e8 62 e9 ff ff       	call   f010011a <_panic>
	assert(pp1->references == 1);
f01017b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01017bb:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01017bf:	66 83 f8 01          	cmp    $0x1,%ax
f01017c3:	74 24                	je     f01017e9 <page_check+0x7b6>
f01017c5:	c7 44 24 0c c1 55 10 	movl   $0xf01055c1,0xc(%esp)
f01017cc:	f0 
f01017cd:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01017d4:	f0 
f01017d5:	c7 44 24 04 dc 00 00 	movl   $0xdc,0x4(%esp)
f01017dc:	00 
f01017dd:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01017e4:	e8 31 e9 ff ff       	call   f010011a <_panic>
	assert(pp2->references == 0);
f01017e9:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01017ec:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01017f0:	66 85 c0             	test   %ax,%ax
f01017f3:	74 24                	je     f0101819 <page_check+0x7e6>
f01017f5:	c7 44 24 0c a0 57 10 	movl   $0xf01057a0,0xc(%esp)
f01017fc:	f0 
f01017fd:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101804:	f0 
f0101805:	c7 44 24 04 dd 00 00 	movl   $0xdd,0x4(%esp)
f010180c:	00 
f010180d:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101814:	e8 01 e9 ff ff       	call   f010011a <_panic>

	// unmapping pp1 at PAGE_SIZE should free it
	unmap_frame(ptr_page_directory, (void*) PAGE_SIZE);
f0101819:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f010181e:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f0101825:	00 
f0101826:	89 04 24             	mov    %eax,(%esp)
f0101829:	e8 c7 0e 00 00       	call   f01026f5 <unmap_frame>
	assert(check_va2pa(ptr_page_directory, 0x0) == ~0);
f010182e:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101833:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f010183a:	00 
f010183b:	89 04 24             	mov    %eax,(%esp)
f010183e:	e8 1f f7 ff ff       	call   f0100f62 <check_va2pa>
f0101843:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101846:	74 24                	je     f010186c <page_check+0x839>
f0101848:	c7 44 24 0c e0 57 10 	movl   $0xf01057e0,0xc(%esp)
f010184f:	f0 
f0101850:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101857:	f0 
f0101858:	c7 44 24 04 e1 00 00 	movl   $0xe1,0x4(%esp)
f010185f:	00 
f0101860:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101867:	e8 ae e8 ff ff       	call   f010011a <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == ~0);
f010186c:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101871:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f0101878:	00 
f0101879:	89 04 24             	mov    %eax,(%esp)
f010187c:	e8 e1 f6 ff ff       	call   f0100f62 <check_va2pa>
f0101881:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101884:	74 24                	je     f01018aa <page_check+0x877>
f0101886:	c7 44 24 0c 0c 58 10 	movl   $0xf010580c,0xc(%esp)
f010188d:	f0 
f010188e:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101895:	f0 
f0101896:	c7 44 24 04 e2 00 00 	movl   $0xe2,0x4(%esp)
f010189d:	00 
f010189e:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01018a5:	e8 70 e8 ff ff       	call   f010011a <_panic>
	assert(pp1->references == 0);
f01018aa:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01018ad:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01018b1:	66 85 c0             	test   %ax,%ax
f01018b4:	74 24                	je     f01018da <page_check+0x8a7>
f01018b6:	c7 44 24 0c 3d 58 10 	movl   $0xf010583d,0xc(%esp)
f01018bd:	f0 
f01018be:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01018c5:	f0 
f01018c6:	c7 44 24 04 e3 00 00 	movl   $0xe3,0x4(%esp)
f01018cd:	00 
f01018ce:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01018d5:	e8 40 e8 ff ff       	call   f010011a <_panic>
	assert(pp2->references == 0);
f01018da:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01018dd:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01018e1:	66 85 c0             	test   %ax,%ax
f01018e4:	74 24                	je     f010190a <page_check+0x8d7>
f01018e6:	c7 44 24 0c a0 57 10 	movl   $0xf01057a0,0xc(%esp)
f01018ed:	f0 
f01018ee:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01018f5:	f0 
f01018f6:	c7 44 24 04 e4 00 00 	movl   $0xe4,0x4(%esp)
f01018fd:	00 
f01018fe:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101905:	e8 10 e8 ff ff       	call   f010011a <_panic>

	// so it should be returned by allocate_frame
	assert(allocate_frame(&pp) == 0 && pp == pp1);
f010190a:	8d 45 f4             	lea    -0xc(%ebp),%eax
f010190d:	89 04 24             	mov    %eax,(%esp)
f0101910:	e8 8f 0a 00 00       	call   f01023a4 <allocate_frame>
f0101915:	85 c0                	test   %eax,%eax
f0101917:	75 0a                	jne    f0101923 <page_check+0x8f0>
f0101919:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010191c:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010191f:	39 c2                	cmp    %eax,%edx
f0101921:	74 24                	je     f0101947 <page_check+0x914>
f0101923:	c7 44 24 0c 54 58 10 	movl   $0xf0105854,0xc(%esp)
f010192a:	f0 
f010192b:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101932:	f0 
f0101933:	c7 44 24 04 e7 00 00 	movl   $0xe7,0x4(%esp)
f010193a:	00 
f010193b:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101942:	e8 d3 e7 ff ff       	call   f010011a <_panic>

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f0101947:	8d 45 f4             	lea    -0xc(%ebp),%eax
f010194a:	89 04 24             	mov    %eax,(%esp)
f010194d:	e8 52 0a 00 00       	call   f01023a4 <allocate_frame>
f0101952:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0101955:	74 24                	je     f010197b <page_check+0x948>
f0101957:	c7 44 24 0c bc 54 10 	movl   $0xf01054bc,0xc(%esp)
f010195e:	f0 
f010195f:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f0101966:	f0 
f0101967:	c7 44 24 04 ea 00 00 	movl   $0xea,0x4(%esp)
f010196e:	00 
f010196f:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101976:	e8 9f e7 ff ff       	call   f010011a <_panic>

	// forcibly take pp0 back
	assert(EXTRACT_ADDRESS(ptr_page_directory[0]) == to_physical_address(pp0));
f010197b:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101980:	8b 00                	mov    (%eax),%eax
f0101982:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0101987:	89 c3                	mov    %eax,%ebx
f0101989:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010198c:	89 04 24             	mov    %eax,(%esp)
f010198f:	e8 0a f2 ff ff       	call   f0100b9e <to_physical_address>
f0101994:	39 c3                	cmp    %eax,%ebx
f0101996:	74 24                	je     f01019bc <page_check+0x989>
f0101998:	c7 44 24 0c 3c 55 10 	movl   $0xf010553c,0xc(%esp)
f010199f:	f0 
f01019a0:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01019a7:	f0 
f01019a8:	c7 44 24 04 ed 00 00 	movl   $0xed,0x4(%esp)
f01019af:	00 
f01019b0:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01019b7:	e8 5e e7 ff ff       	call   f010011a <_panic>
	ptr_page_directory[0] = 0;
f01019bc:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f01019c1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(pp0->references == 1);
f01019c7:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01019ca:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01019ce:	66 83 f8 01          	cmp    $0x1,%ax
f01019d2:	74 24                	je     f01019f8 <page_check+0x9c5>
f01019d4:	c7 44 24 0c d6 55 10 	movl   $0xf01055d6,0xc(%esp)
f01019db:	f0 
f01019dc:	c7 44 24 08 f2 52 10 	movl   $0xf01052f2,0x8(%esp)
f01019e3:	f0 
f01019e4:	c7 44 24 04 ef 00 00 	movl   $0xef,0x4(%esp)
f01019eb:	00 
f01019ec:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f01019f3:	e8 22 e7 ff ff       	call   f010011a <_panic>
	pp0->references = 0;
f01019f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01019fb:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)

	// give free list back
	free_frame_list = fl;
f0101a01:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101a04:	a3 98 63 14 f0       	mov    %eax,0xf0146398

	// free the frames_info we took
	free_frame(pp0);
f0101a09:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101a0c:	89 04 24             	mov    %eax,(%esp)
f0101a0f:	e8 f3 09 00 00       	call   f0102407 <free_frame>
	free_frame(pp1);
f0101a14:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101a17:	89 04 24             	mov    %eax,(%esp)
f0101a1a:	e8 e8 09 00 00       	call   f0102407 <free_frame>
	free_frame(pp2);
f0101a1f:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101a22:	89 04 24             	mov    %eax,(%esp)
f0101a25:	e8 dd 09 00 00       	call   f0102407 <free_frame>

	cprintf("page_check() succeeded!\n");
f0101a2a:	c7 04 24 7a 58 10 f0 	movl   $0xf010587a,(%esp)
f0101a31:	e8 d5 15 00 00       	call   f010300b <cprintf>
}
f0101a36:	83 c4 34             	add    $0x34,%esp
f0101a39:	5b                   	pop    %ebx
f0101a3a:	5d                   	pop    %ebp
f0101a3b:	c3                   	ret    

f0101a3c <turn_on_paging>:

void turn_on_paging()
{
f0101a3c:	55                   	push   %ebp
f0101a3d:	89 e5                	mov    %esp,%ebp
f0101a3f:	83 ec 20             	sub    $0x20,%esp
	// mapping, even though we are turning on paging and reconfiguring
	// segmentation.

	// Map VA 0:4MB same as VA (KERNEL_BASE), i.e. to PA 0:4MB.
	// (Limits our kernel to <4MB)
	ptr_page_directory[0] = ptr_page_directory[PDX(KERNEL_BASE)];
f0101a42:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101a47:	8b 15 a4 63 14 f0    	mov    0xf01463a4,%edx
f0101a4d:	8b 92 00 0f 00 00    	mov    0xf00(%edx),%edx
f0101a53:	89 10                	mov    %edx,(%eax)

	// Install page table.
	lcr3(phys_page_directory);
f0101a55:	a1 a8 63 14 f0       	mov    0xf01463a8,%eax
f0101a5a:	89 45 f8             	mov    %eax,-0x8(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0101a5d:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0101a60:	0f 22 d8             	mov    %eax,%cr3

static __inline uint32
rcr0(void)
{
	uint32 val;
	__asm __volatile("movl %%cr0,%0" : "=r" (val));
f0101a63:	0f 20 c0             	mov    %cr0,%eax
f0101a66:	89 45 f4             	mov    %eax,-0xc(%ebp)
	return val;
f0101a69:	8b 45 f4             	mov    -0xc(%ebp),%eax

	// Turn on paging.
	uint32 cr0;
	cr0 = rcr0();
f0101a6c:	89 45 fc             	mov    %eax,-0x4(%ebp)
	cr0 |= CR0_PE|CR0_PG|CR0_AM|CR0_WP|CR0_NE|CR0_TS|CR0_EM|CR0_MP;
f0101a6f:	81 4d fc 2f 00 05 80 	orl    $0x8005002f,-0x4(%ebp)
	cr0 &= ~(CR0_TS|CR0_EM);
f0101a76:	83 65 fc f3          	andl   $0xfffffff3,-0x4(%ebp)
f0101a7a:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101a7d:	89 45 f0             	mov    %eax,-0x10(%ebp)
}

static __inline void
lcr0(uint32 val)
{
	__asm __volatile("movl %0,%%cr0" : : "r" (val));
f0101a80:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101a83:	0f 22 c0             	mov    %eax,%cr0

	// Current mapping: KERNEL_BASE+x => x => x.
	// (x < 4MB so uses paging ptr_page_directory[0])

	// Reload all segment registers.
	asm volatile("lgdt gdt_pd");
f0101a86:	0f 01 15 90 a5 11 f0 	lgdtl  0xf011a590
	asm volatile("movw %%ax,%%gs" :: "a" (GD_UD|3));
f0101a8d:	b8 23 00 00 00       	mov    $0x23,%eax
f0101a92:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" :: "a" (GD_UD|3));
f0101a94:	b8 23 00 00 00       	mov    $0x23,%eax
f0101a99:	8e e0                	mov    %eax,%fs
	asm volatile("movw %%ax,%%es" :: "a" (GD_KD));
f0101a9b:	b8 10 00 00 00       	mov    $0x10,%eax
f0101aa0:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" :: "a" (GD_KD));
f0101aa2:	b8 10 00 00 00       	mov    $0x10,%eax
f0101aa7:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" :: "a" (GD_KD));
f0101aa9:	b8 10 00 00 00       	mov    $0x10,%eax
f0101aae:	8e d0                	mov    %eax,%ss
	asm volatile("ljmp %0,$1f\n 1:\n" :: "i" (GD_KT));  // reload cs
f0101ab0:	ea b7 1a 10 f0 08 00 	ljmp   $0x8,$0xf0101ab7
	asm volatile("lldt %%ax" :: "a" (0));
f0101ab7:	b8 00 00 00 00       	mov    $0x0,%eax
f0101abc:	0f 00 d0             	lldt   %ax

	// Final mapping: KERNEL_BASE + x => KERNEL_BASE + x => x.

	// This mapping was only used after paging was turned on but
	// before the segment registers were reloaded.
	ptr_page_directory[0] = 0;
f0101abf:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101ac4:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

	// Flush the TLB for good measure, to kill the ptr_page_directory[0] mapping.
	lcr3(phys_page_directory);
f0101aca:	a1 a8 63 14 f0       	mov    0xf01463a8,%eax
f0101acf:	89 45 ec             	mov    %eax,-0x14(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0101ad2:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101ad5:	0f 22 d8             	mov    %eax,%cr3
}
f0101ad8:	c9                   	leave  
f0101ad9:	c3                   	ret    

f0101ada <setup_listing_to_all_page_tables_entries>:

void setup_listing_to_all_page_tables_entries()
{
f0101ada:	55                   	push   %ebp
f0101adb:	89 e5                	mov    %esp,%ebp
f0101add:	83 ec 28             	sub    $0x28,%esp
	//////////////////////////////////////////////////////////////////////
	// Recursively insert PD in itself as a page table, to form
	// a virtual page table at virtual address VPT.

	// Permissions: kernel RW, user NONE
	uint32 phys_frame_address = K_PHYSICAL_ADDRESS(ptr_page_directory);
f0101ae0:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101ae5:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0101ae8:	81 7d f4 ff ff ff ef 	cmpl   $0xefffffff,-0xc(%ebp)
f0101aef:	77 23                	ja     f0101b14 <setup_listing_to_all_page_tables_entries+0x3a>
f0101af1:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101af4:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101af8:	c7 44 24 08 50 52 10 	movl   $0xf0105250,0x8(%esp)
f0101aff:	f0 
f0101b00:	c7 44 24 04 39 01 00 	movl   $0x139,0x4(%esp)
f0101b07:	00 
f0101b08:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101b0f:	e8 06 e6 ff ff       	call   f010011a <_panic>
f0101b14:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101b17:	05 00 00 00 10       	add    $0x10000000,%eax
f0101b1c:	89 45 f0             	mov    %eax,-0x10(%ebp)
	ptr_page_directory[PDX(VPT)] = CONSTRUCT_ENTRY(phys_frame_address , PERM_PRESENT | PERM_WRITEABLE);
f0101b1f:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101b24:	05 fc 0e 00 00       	add    $0xefc,%eax
f0101b29:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0101b2c:	83 ca 03             	or     $0x3,%edx
f0101b2f:	89 10                	mov    %edx,(%eax)

	// same for UVPT
	//Permissions: kernel R, user R
	ptr_page_directory[PDX(UVPT)] = K_PHYSICAL_ADDRESS(ptr_page_directory)|PERM_USER|PERM_PRESENT;
f0101b31:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101b36:	8d 90 f4 0e 00 00    	lea    0xef4(%eax),%edx
f0101b3c:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101b41:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101b44:	81 7d ec ff ff ff ef 	cmpl   $0xefffffff,-0x14(%ebp)
f0101b4b:	77 23                	ja     f0101b70 <setup_listing_to_all_page_tables_entries+0x96>
f0101b4d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101b50:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101b54:	c7 44 24 08 50 52 10 	movl   $0xf0105250,0x8(%esp)
f0101b5b:	f0 
f0101b5c:	c7 44 24 04 3e 01 00 	movl   $0x13e,0x4(%esp)
f0101b63:	00 
f0101b64:	c7 04 24 81 52 10 f0 	movl   $0xf0105281,(%esp)
f0101b6b:	e8 aa e5 ff ff       	call   f010011a <_panic>
f0101b70:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101b73:	05 00 00 00 10       	add    $0x10000000,%eax
f0101b78:	83 c8 05             	or     $0x5,%eax
f0101b7b:	89 02                	mov    %eax,(%edx)

}
f0101b7d:	c9                   	leave  
f0101b7e:	c3                   	ret    

f0101b7f <envid2env>:
//   0 on success, -E_BAD_ENV on error.
//   On success, sets *penv to the environment.
//   On error, sets *penv to NULL.
//
int envid2env(int32  envid, struct Env **env_store, bool checkperm)
{
f0101b7f:	55                   	push   %ebp
f0101b80:	89 e5                	mov    %esp,%ebp
f0101b82:	83 ec 10             	sub    $0x10,%esp
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f0101b85:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0101b89:	75 12                	jne    f0101b9d <envid2env+0x1e>
		*env_store = curenv;
f0101b8b:	8b 15 10 5b 14 f0    	mov    0xf0145b10,%edx
f0101b91:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101b94:	89 10                	mov    %edx,(%eax)
		return 0;
f0101b96:	b8 00 00 00 00       	mov    $0x0,%eax
f0101b9b:	eb 7a                	jmp    f0101c17 <envid2env+0x98>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f0101b9d:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f0101ba3:	8b 45 08             	mov    0x8(%ebp),%eax
f0101ba6:	25 ff 03 00 00       	and    $0x3ff,%eax
f0101bab:	6b c0 64             	imul   $0x64,%eax,%eax
f0101bae:	01 d0                	add    %edx,%eax
f0101bb0:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0101bb3:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101bb6:	8b 40 54             	mov    0x54(%eax),%eax
f0101bb9:	85 c0                	test   %eax,%eax
f0101bbb:	74 0b                	je     f0101bc8 <envid2env+0x49>
f0101bbd:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101bc0:	8b 40 4c             	mov    0x4c(%eax),%eax
f0101bc3:	3b 45 08             	cmp    0x8(%ebp),%eax
f0101bc6:	74 10                	je     f0101bd8 <envid2env+0x59>
		*env_store = 0;
f0101bc8:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101bcb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0101bd1:	b8 02 00 00 00       	mov    $0x2,%eax
f0101bd6:	eb 3f                	jmp    f0101c17 <envid2env+0x98>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f0101bd8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0101bdc:	74 2c                	je     f0101c0a <envid2env+0x8b>
f0101bde:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0101be3:	39 45 fc             	cmp    %eax,-0x4(%ebp)
f0101be6:	74 22                	je     f0101c0a <envid2env+0x8b>
f0101be8:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101beb:	8b 50 50             	mov    0x50(%eax),%edx
f0101bee:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0101bf3:	8b 40 4c             	mov    0x4c(%eax),%eax
f0101bf6:	39 c2                	cmp    %eax,%edx
f0101bf8:	74 10                	je     f0101c0a <envid2env+0x8b>
		*env_store = 0;
f0101bfa:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101bfd:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0101c03:	b8 02 00 00 00       	mov    $0x2,%eax
f0101c08:	eb 0d                	jmp    f0101c17 <envid2env+0x98>
	}

	*env_store = e;
f0101c0a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101c0d:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0101c10:	89 10                	mov    %edx,(%eax)
	return 0;
f0101c12:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101c17:	c9                   	leave  
f0101c18:	c3                   	ret    

f0101c19 <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f0101c19:	55                   	push   %ebp
f0101c1a:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0101c1c:	8b 55 08             	mov    0x8(%ebp),%edx
f0101c1f:	a1 9c 63 14 f0       	mov    0xf014639c,%eax
f0101c24:	29 c2                	sub    %eax,%edx
f0101c26:	89 d0                	mov    %edx,%eax
f0101c28:	c1 f8 02             	sar    $0x2,%eax
f0101c2b:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f0101c31:	5d                   	pop    %ebp
f0101c32:	c3                   	ret    

f0101c33 <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f0101c33:	55                   	push   %ebp
f0101c34:	89 e5                	mov    %esp,%ebp
f0101c36:	83 ec 04             	sub    $0x4,%esp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f0101c39:	8b 45 08             	mov    0x8(%ebp),%eax
f0101c3c:	89 04 24             	mov    %eax,(%esp)
f0101c3f:	e8 d5 ff ff ff       	call   f0101c19 <to_frame_number>
f0101c44:	c1 e0 0c             	shl    $0xc,%eax
}
f0101c47:	c9                   	leave  
f0101c48:	c3                   	ret    

f0101c49 <to_frame_info>:

static inline struct Frame_Info* to_frame_info(uint32 physical_address)
{
f0101c49:	55                   	push   %ebp
f0101c4a:	89 e5                	mov    %esp,%ebp
f0101c4c:	83 ec 18             	sub    $0x18,%esp
	if (PPN(physical_address) >= number_of_frames)
f0101c4f:	8b 45 08             	mov    0x8(%ebp),%eax
f0101c52:	c1 e8 0c             	shr    $0xc,%eax
f0101c55:	89 c2                	mov    %eax,%edx
f0101c57:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f0101c5c:	39 c2                	cmp    %eax,%edx
f0101c5e:	72 1c                	jb     f0101c7c <to_frame_info+0x33>
		panic("to_frame_info called with invalid pa");
f0101c60:	c7 44 24 08 94 58 10 	movl   $0xf0105894,0x8(%esp)
f0101c67:	f0 
f0101c68:	c7 44 24 04 39 00 00 	movl   $0x39,0x4(%esp)
f0101c6f:	00 
f0101c70:	c7 04 24 b9 58 10 f0 	movl   $0xf01058b9,(%esp)
f0101c77:	e8 9e e4 ff ff       	call   f010011a <_panic>
	return &frames_info[PPN(physical_address)];
f0101c7c:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0101c82:	8b 45 08             	mov    0x8(%ebp),%eax
f0101c85:	c1 e8 0c             	shr    $0xc,%eax
f0101c88:	89 c2                	mov    %eax,%edx
f0101c8a:	89 d0                	mov    %edx,%eax
f0101c8c:	01 c0                	add    %eax,%eax
f0101c8e:	01 d0                	add    %edx,%eax
f0101c90:	c1 e0 02             	shl    $0x2,%eax
f0101c93:	01 c8                	add    %ecx,%eax
}
f0101c95:	c9                   	leave  
f0101c96:	c3                   	ret    

f0101c97 <initialize_kernel_VM>:
//
// From USER_TOP to USER_LIMIT, the user is allowed to read but not write.
// Above USER_LIMIT the user cannot read (or write).

void initialize_kernel_VM()
{
f0101c97:	55                   	push   %ebp
f0101c98:	89 e5                	mov    %esp,%ebp
f0101c9a:	83 ec 48             	sub    $0x48,%esp
	//panic("initialize_kernel_VM: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.

	ptr_page_directory = boot_allocate_space(PAGE_SIZE, PAGE_SIZE);
f0101c9d:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f0101ca4:	00 
f0101ca5:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
f0101cac:	e8 36 02 00 00       	call   f0101ee7 <boot_allocate_space>
f0101cb1:	a3 a4 63 14 f0       	mov    %eax,0xf01463a4
	memset(ptr_page_directory, 0, PAGE_SIZE);
f0101cb6:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101cbb:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f0101cc2:	00 
f0101cc3:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0101cca:	00 
f0101ccb:	89 04 24             	mov    %eax,(%esp)
f0101cce:	e8 2b 2b 00 00       	call   f01047fe <memset>
	phys_page_directory = K_PHYSICAL_ADDRESS(ptr_page_directory);
f0101cd3:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101cd8:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0101cdb:	81 7d f4 ff ff ff ef 	cmpl   $0xefffffff,-0xc(%ebp)
f0101ce2:	77 23                	ja     f0101d07 <initialize_kernel_VM+0x70>
f0101ce4:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101ce7:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101ceb:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f0101cf2:	f0 
f0101cf3:	c7 44 24 04 3c 00 00 	movl   $0x3c,0x4(%esp)
f0101cfa:	00 
f0101cfb:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0101d02:	e8 13 e4 ff ff       	call   f010011a <_panic>
f0101d07:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101d0a:	05 00 00 00 10       	add    $0x10000000,%eax
f0101d0f:	a3 a8 63 14 f0       	mov    %eax,0xf01463a8
	// Map the kernel stack with VA range :
	//  [KERNEL_STACK_TOP-KERNEL_STACK_SIZE, KERNEL_STACK_TOP), 
	// to physical address : "phys_stack_bottom".
	//     Permissions: kernel RW, user NONE
	// Your code goes here:
	boot_map_range(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE, KERNEL_STACK_SIZE, K_PHYSICAL_ADDRESS(ptr_stack_bottom), PERM_WRITEABLE) ;
f0101d14:	c7 45 f0 00 20 11 f0 	movl   $0xf0112000,-0x10(%ebp)
f0101d1b:	81 7d f0 ff ff ff ef 	cmpl   $0xefffffff,-0x10(%ebp)
f0101d22:	77 23                	ja     f0101d47 <initialize_kernel_VM+0xb0>
f0101d24:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101d27:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101d2b:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f0101d32:	f0 
f0101d33:	c7 44 24 04 44 00 00 	movl   $0x44,0x4(%esp)
f0101d3a:	00 
f0101d3b:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0101d42:	e8 d3 e3 ff ff       	call   f010011a <_panic>
f0101d47:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101d4a:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101d50:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101d55:	c7 44 24 10 02 00 00 	movl   $0x2,0x10(%esp)
f0101d5c:	00 
f0101d5d:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0101d61:	c7 44 24 08 00 80 00 	movl   $0x8000,0x8(%esp)
f0101d68:	00 
f0101d69:	c7 44 24 04 00 80 bf 	movl   $0xefbf8000,0x4(%esp)
f0101d70:	ef 
f0101d71:	89 04 24             	mov    %eax,(%esp)
f0101d74:	e8 d6 01 00 00       	call   f0101f4f <boot_map_range>
	//      the PA range [0, 2^32 - KERNEL_BASE)
	// We might not have 2^32 - KERNEL_BASE bytes of physical memory, but
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here: 
	boot_map_range(ptr_page_directory, KERNEL_BASE, 0xFFFFFFFF - KERNEL_BASE, 0, PERM_WRITEABLE) ;
f0101d79:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101d7e:	c7 44 24 10 02 00 00 	movl   $0x2,0x10(%esp)
f0101d85:	00 
f0101d86:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
f0101d8d:	00 
f0101d8e:	c7 44 24 08 ff ff ff 	movl   $0xfffffff,0x8(%esp)
f0101d95:	0f 
f0101d96:	c7 44 24 04 00 00 00 	movl   $0xf0000000,0x4(%esp)
f0101d9d:	f0 
f0101d9e:	89 04 24             	mov    %eax,(%esp)
f0101da1:	e8 a9 01 00 00       	call   f0101f4f <boot_map_range>
	// Permissions:
	//    - frames_info -- kernel RW, user NONE
	//    - the image mapped at READ_ONLY_FRAMES_INFO  -- kernel R, user R
	// Your code goes here:
	uint32 array_size;
	array_size = number_of_frames * sizeof(struct Frame_Info) ;
f0101da6:	8b 15 88 63 14 f0    	mov    0xf0146388,%edx
f0101dac:	89 d0                	mov    %edx,%eax
f0101dae:	01 c0                	add    %eax,%eax
f0101db0:	01 d0                	add    %edx,%eax
f0101db2:	c1 e0 02             	shl    $0x2,%eax
f0101db5:	89 45 ec             	mov    %eax,-0x14(%ebp)
	frames_info = boot_allocate_space(array_size, PAGE_SIZE);
f0101db8:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f0101dbf:	00 
f0101dc0:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101dc3:	89 04 24             	mov    %eax,(%esp)
f0101dc6:	e8 1c 01 00 00       	call   f0101ee7 <boot_allocate_space>
f0101dcb:	a3 9c 63 14 f0       	mov    %eax,0xf014639c
	boot_map_range(ptr_page_directory, READ_ONLY_FRAMES_INFO, array_size, K_PHYSICAL_ADDRESS(frames_info), PERM_USER) ;
f0101dd0:	a1 9c 63 14 f0       	mov    0xf014639c,%eax
f0101dd5:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0101dd8:	81 7d e8 ff ff ff ef 	cmpl   $0xefffffff,-0x18(%ebp)
f0101ddf:	77 23                	ja     f0101e04 <initialize_kernel_VM+0x16d>
f0101de1:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101de4:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101de8:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f0101def:	f0 
f0101df0:	c7 44 24 04 5f 00 00 	movl   $0x5f,0x4(%esp)
f0101df7:	00 
f0101df8:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0101dff:	e8 16 e3 ff ff       	call   f010011a <_panic>
f0101e04:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101e07:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101e0d:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101e12:	c7 44 24 10 04 00 00 	movl   $0x4,0x10(%esp)
f0101e19:	00 
f0101e1a:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0101e1e:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0101e21:	89 54 24 08          	mov    %edx,0x8(%esp)
f0101e25:	c7 44 24 04 00 00 00 	movl   $0xef000000,0x4(%esp)
f0101e2c:	ef 
f0101e2d:	89 04 24             	mov    %eax,(%esp)
f0101e30:	e8 1a 01 00 00       	call   f0101f4f <boot_map_range>


	// This allows the kernel & user to access any page table entry using a
	// specified VA for each: VPT for kernel and UVPT for User.
	setup_listing_to_all_page_tables_entries();
f0101e35:	e8 a0 fc ff ff       	call   f0101ada <setup_listing_to_all_page_tables_entries>
	// Permissions:
	//    - envs itself -- kernel RW, user NONE
	//    - the image of envs mapped at UENVS  -- kernel R, user R
	
	// LAB 3: Your code here.
	int envs_size = NENV * sizeof(struct Env) ;
f0101e3a:	c7 45 e4 00 90 01 00 	movl   $0x19000,-0x1c(%ebp)

	//allocate space for "envs" array aligned on 4KB boundary
	envs = boot_allocate_space(envs_size, PAGE_SIZE);
f0101e41:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101e44:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f0101e4b:	00 
f0101e4c:	89 04 24             	mov    %eax,(%esp)
f0101e4f:	e8 93 00 00 00       	call   f0101ee7 <boot_allocate_space>
f0101e54:	a3 0c 5b 14 f0       	mov    %eax,0xf0145b0c

	//make the user to access this array by mapping it to UPAGES linear address (UPAGES is in User/Kernel space)
	boot_map_range(ptr_page_directory, UENVS, envs_size, K_PHYSICAL_ADDRESS(envs), PERM_USER) ;
f0101e59:	a1 0c 5b 14 f0       	mov    0xf0145b0c,%eax
f0101e5e:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0101e61:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0101e68:	77 23                	ja     f0101e8d <initialize_kernel_VM+0x1f6>
f0101e6a:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0101e6d:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0101e71:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f0101e78:	f0 
f0101e79:	c7 44 24 04 75 00 00 	movl   $0x75,0x4(%esp)
f0101e80:	00 
f0101e81:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0101e88:	e8 8d e2 ff ff       	call   f010011a <_panic>
f0101e8d:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0101e90:	8d 88 00 00 00 10    	lea    0x10000000(%eax),%ecx
f0101e96:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0101e99:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101e9e:	c7 44 24 10 04 00 00 	movl   $0x4,0x10(%esp)
f0101ea5:	00 
f0101ea6:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0101eaa:	89 54 24 08          	mov    %edx,0x8(%esp)
f0101eae:	c7 44 24 04 00 00 c0 	movl   $0xeec00000,0x4(%esp)
f0101eb5:	ee 
f0101eb6:	89 04 24             	mov    %eax,(%esp)
f0101eb9:	e8 91 00 00 00       	call   f0101f4f <boot_map_range>

	//update permissions of the corresponding entry in page directory to make it USER with PERMISSION read only
	ptr_page_directory[PDX(UENVS)] = ptr_page_directory[PDX(UENVS)]|(PERM_USER|(PERM_PRESENT & (~PERM_WRITEABLE)));
f0101ebe:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0101ec3:	05 ec 0e 00 00       	add    $0xeec,%eax
f0101ec8:	8b 15 a4 63 14 f0    	mov    0xf01463a4,%edx
f0101ece:	81 c2 ec 0e 00 00    	add    $0xeec,%edx
f0101ed4:	8b 12                	mov    (%edx),%edx
f0101ed6:	83 ca 05             	or     $0x5,%edx
f0101ed9:	89 10                	mov    %edx,(%eax)


	// Check that the initial page directory has been set up correctly.
	check_boot_pgdir();
f0101edb:	e8 b6 ed ff ff       	call   f0100c96 <check_boot_pgdir>
	
	// NOW: Turn off the segmentation by setting the segments' base to 0, and
	// turn on the paging by setting the corresponding flags in control register 0 (cr0)
	turn_on_paging() ;
f0101ee0:	e8 57 fb ff ff       	call   f0101a3c <turn_on_paging>
}
f0101ee5:	c9                   	leave  
f0101ee6:	c3                   	ret    

f0101ee7 <boot_allocate_space>:
// It's too early to run out of memory.
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
// 
void* boot_allocate_space(uint32 size, uint32 align)
{
f0101ee7:	55                   	push   %ebp
f0101ee8:	89 e5                	mov    %esp,%ebp
f0101eea:	83 ec 10             	sub    $0x10,%esp
	// Initialize ptr_free_mem if this is the first time.
	// 'end_of_kernel' is a symbol automatically generated by the linker,
	// which points to the end of the kernel-
	// i.e., the first virtual address that the linker
	// did not assign to any kernel code or global variables.
	if (ptr_free_mem == 0)
f0101eed:	a1 a0 63 14 f0       	mov    0xf01463a0,%eax
f0101ef2:	85 c0                	test   %eax,%eax
f0101ef4:	75 0a                	jne    f0101f00 <boot_allocate_space+0x19>
		ptr_free_mem = end_of_kernel;
f0101ef6:	c7 05 a0 63 14 f0 ac 	movl   $0xf01463ac,0xf01463a0
f0101efd:	63 14 f0 

	// Your code here:
	//	Step 1: round ptr_free_mem up to be aligned properly
	ptr_free_mem = ROUNDUP(ptr_free_mem, PAGE_SIZE) ;
f0101f00:	c7 45 fc 00 10 00 00 	movl   $0x1000,-0x4(%ebp)
f0101f07:	a1 a0 63 14 f0       	mov    0xf01463a0,%eax
f0101f0c:	89 c2                	mov    %eax,%edx
f0101f0e:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101f11:	01 d0                	add    %edx,%eax
f0101f13:	83 e8 01             	sub    $0x1,%eax
f0101f16:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0101f19:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0101f1c:	ba 00 00 00 00       	mov    $0x0,%edx
f0101f21:	f7 75 fc             	divl   -0x4(%ebp)
f0101f24:	89 d0                	mov    %edx,%eax
f0101f26:	8b 55 f8             	mov    -0x8(%ebp),%edx
f0101f29:	29 c2                	sub    %eax,%edx
f0101f2b:	89 d0                	mov    %edx,%eax
f0101f2d:	a3 a0 63 14 f0       	mov    %eax,0xf01463a0
	
	//	Step 2: save current value of ptr_free_mem as allocated space
	void *ptr_allocated_mem;
	ptr_allocated_mem = ptr_free_mem ;
f0101f32:	a1 a0 63 14 f0       	mov    0xf01463a0,%eax
f0101f37:	89 45 f4             	mov    %eax,-0xc(%ebp)

	//	Step 3: increase ptr_free_mem to record allocation
	ptr_free_mem += size ;
f0101f3a:	8b 15 a0 63 14 f0    	mov    0xf01463a0,%edx
f0101f40:	8b 45 08             	mov    0x8(%ebp),%eax
f0101f43:	01 d0                	add    %edx,%eax
f0101f45:	a3 a0 63 14 f0       	mov    %eax,0xf01463a0

	//	Step 4: return allocated space
	return ptr_allocated_mem ;
f0101f4a:	8b 45 f4             	mov    -0xc(%ebp),%eax

}
f0101f4d:	c9                   	leave  
f0101f4e:	c3                   	ret    

f0101f4f <boot_map_range>:
//
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
//
void boot_map_range(uint32 *ptr_page_directory, uint32 virtual_address, uint32 size, uint32 physical_address, int perm)
{
f0101f4f:	55                   	push   %ebp
f0101f50:	89 e5                	mov    %esp,%ebp
f0101f52:	83 ec 38             	sub    $0x38,%esp
	int i = 0 ;
f0101f55:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	physical_address = ROUNDUP(physical_address, PAGE_SIZE) ;
f0101f5c:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f0101f63:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101f66:	8b 55 14             	mov    0x14(%ebp),%edx
f0101f69:	01 d0                	add    %edx,%eax
f0101f6b:	83 e8 01             	sub    $0x1,%eax
f0101f6e:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101f71:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101f74:	ba 00 00 00 00       	mov    $0x0,%edx
f0101f79:	f7 75 f0             	divl   -0x10(%ebp)
f0101f7c:	89 d0                	mov    %edx,%eax
f0101f7e:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0101f81:	29 c2                	sub    %eax,%edx
f0101f83:	89 d0                	mov    %edx,%eax
f0101f85:	89 45 14             	mov    %eax,0x14(%ebp)
	for (i = 0 ; i < size ; i += PAGE_SIZE)
f0101f88:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0101f8f:	eb 5a                	jmp    f0101feb <boot_map_range+0x9c>
	{
		uint32 *ptr_page_table = boot_get_page_table(ptr_page_directory, virtual_address, 1) ;
f0101f91:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
f0101f98:	00 
f0101f99:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101f9c:	89 44 24 04          	mov    %eax,0x4(%esp)
f0101fa0:	8b 45 08             	mov    0x8(%ebp),%eax
f0101fa3:	89 04 24             	mov    %eax,(%esp)
f0101fa6:	e8 4a 00 00 00       	call   f0101ff5 <boot_get_page_table>
f0101fab:	89 45 e8             	mov    %eax,-0x18(%ebp)
		uint32 index_page_table = PTX(virtual_address);
f0101fae:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101fb1:	c1 e8 0c             	shr    $0xc,%eax
f0101fb4:	25 ff 03 00 00       	and    $0x3ff,%eax
f0101fb9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		ptr_page_table[index_page_table] = CONSTRUCT_ENTRY(physical_address, perm | PERM_PRESENT) ;
f0101fbc:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101fbf:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0101fc6:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101fc9:	01 c2                	add    %eax,%edx
f0101fcb:	8b 45 18             	mov    0x18(%ebp),%eax
f0101fce:	0b 45 14             	or     0x14(%ebp),%eax
f0101fd1:	83 c8 01             	or     $0x1,%eax
f0101fd4:	89 02                	mov    %eax,(%edx)
		physical_address += PAGE_SIZE ;
f0101fd6:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
		virtual_address += PAGE_SIZE ;
f0101fdd:	81 45 0c 00 10 00 00 	addl   $0x1000,0xc(%ebp)
//
void boot_map_range(uint32 *ptr_page_directory, uint32 virtual_address, uint32 size, uint32 physical_address, int perm)
{
	int i = 0 ;
	physical_address = ROUNDUP(physical_address, PAGE_SIZE) ;
	for (i = 0 ; i < size ; i += PAGE_SIZE)
f0101fe4:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0101feb:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101fee:	3b 45 10             	cmp    0x10(%ebp),%eax
f0101ff1:	72 9e                	jb     f0101f91 <boot_map_range+0x42>
		uint32 index_page_table = PTX(virtual_address);
		ptr_page_table[index_page_table] = CONSTRUCT_ENTRY(physical_address, perm | PERM_PRESENT) ;
		physical_address += PAGE_SIZE ;
		virtual_address += PAGE_SIZE ;
	}
}
f0101ff3:	c9                   	leave  
f0101ff4:	c3                   	ret    

f0101ff5 <boot_get_page_table>:
// boot_get_page_table cannot fail.  It's too early to fail.
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
//
uint32* boot_get_page_table(uint32 *ptr_page_directory, uint32 virtual_address, int create)
{
f0101ff5:	55                   	push   %ebp
f0101ff6:	89 e5                	mov    %esp,%ebp
f0101ff8:	83 ec 38             	sub    $0x38,%esp
	uint32 index_page_directory = PDX(virtual_address);
f0101ffb:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101ffe:	c1 e8 16             	shr    $0x16,%eax
f0102001:	89 45 f4             	mov    %eax,-0xc(%ebp)
	uint32 page_directory_entry = ptr_page_directory[index_page_directory];
f0102004:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102007:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f010200e:	8b 45 08             	mov    0x8(%ebp),%eax
f0102011:	01 d0                	add    %edx,%eax
f0102013:	8b 00                	mov    (%eax),%eax
f0102015:	89 45 f0             	mov    %eax,-0x10(%ebp)
	
	uint32 phys_page_table = EXTRACT_ADDRESS(page_directory_entry);
f0102018:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010201b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0102020:	89 45 ec             	mov    %eax,-0x14(%ebp)
	uint32 *ptr_page_table = K_VIRTUAL_ADDRESS(phys_page_table);
f0102023:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102026:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0102029:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010202c:	c1 e8 0c             	shr    $0xc,%eax
f010202f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0102032:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f0102037:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f010203a:	72 23                	jb     f010205f <boot_get_page_table+0x6a>
f010203c:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010203f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0102043:	c7 44 24 08 1c 59 10 	movl   $0xf010591c,0x8(%esp)
f010204a:	f0 
f010204b:	c7 44 24 04 db 00 00 	movl   $0xdb,0x4(%esp)
f0102052:	00 
f0102053:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f010205a:	e8 bb e0 ff ff       	call   f010011a <_panic>
f010205f:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102062:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102067:	89 45 e0             	mov    %eax,-0x20(%ebp)
	if (phys_page_table == 0)
f010206a:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f010206e:	75 7d                	jne    f01020ed <boot_get_page_table+0xf8>
	{
		if (create)
f0102070:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0102074:	74 70                	je     f01020e6 <boot_get_page_table+0xf1>
		{
			ptr_page_table = boot_allocate_space(PAGE_SIZE, PAGE_SIZE) ;
f0102076:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
f010207d:	00 
f010207e:	c7 04 24 00 10 00 00 	movl   $0x1000,(%esp)
f0102085:	e8 5d fe ff ff       	call   f0101ee7 <boot_allocate_space>
f010208a:	89 45 e0             	mov    %eax,-0x20(%ebp)
			phys_page_table = K_PHYSICAL_ADDRESS(ptr_page_table);
f010208d:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0102090:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0102093:	81 7d dc ff ff ff ef 	cmpl   $0xefffffff,-0x24(%ebp)
f010209a:	77 23                	ja     f01020bf <boot_get_page_table+0xca>
f010209c:	8b 45 dc             	mov    -0x24(%ebp),%eax
f010209f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01020a3:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f01020aa:	f0 
f01020ab:	c7 44 24 04 e1 00 00 	movl   $0xe1,0x4(%esp)
f01020b2:	00 
f01020b3:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f01020ba:	e8 5b e0 ff ff       	call   f010011a <_panic>
f01020bf:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01020c2:	05 00 00 00 10       	add    $0x10000000,%eax
f01020c7:	89 45 ec             	mov    %eax,-0x14(%ebp)
			ptr_page_directory[index_page_directory] = CONSTRUCT_ENTRY(phys_page_table, PERM_PRESENT | PERM_WRITEABLE);
f01020ca:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01020cd:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f01020d4:	8b 45 08             	mov    0x8(%ebp),%eax
f01020d7:	01 d0                	add    %edx,%eax
f01020d9:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01020dc:	83 ca 03             	or     $0x3,%edx
f01020df:	89 10                	mov    %edx,(%eax)
			return ptr_page_table ;
f01020e1:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01020e4:	eb 0a                	jmp    f01020f0 <boot_get_page_table+0xfb>
		}
		else
			return 0 ;
f01020e6:	b8 00 00 00 00       	mov    $0x0,%eax
f01020eb:	eb 03                	jmp    f01020f0 <boot_get_page_table+0xfb>
	}
	return ptr_page_table ;
f01020ed:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
f01020f0:	c9                   	leave  
f01020f1:	c3                   	ret    

f01020f2 <initialize_paging>:
// After this point, ONLY use the functions below
// to allocate and deallocate physical memory via the free_frame_list,
// and NEVER use boot_allocate_space() or the related boot-time functions above.
//
void initialize_paging()
{
f01020f2:	55                   	push   %ebp
f01020f3:	89 e5                	mov    %esp,%ebp
f01020f5:	53                   	push   %ebx
f01020f6:	83 ec 34             	sub    $0x34,%esp
	//     Some of it is in use, some is free. Where is the kernel?
	//     Which frames are used for page tables and other data structures?
	//
	// Change the code to reflect this.
	int i;
	LIST_INIT(&free_frame_list);
f01020f9:	c7 05 98 63 14 f0 00 	movl   $0x0,0xf0146398
f0102100:	00 00 00 
	
	frames_info[0].references = 1;
f0102103:	a1 9c 63 14 f0       	mov    0xf014639c,%eax
f0102108:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
	
	int range_end = ROUNDUP(PHYS_IO_MEM,PAGE_SIZE);
f010210e:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f0102115:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102118:	05 ff ff 09 00       	add    $0x9ffff,%eax
f010211d:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102120:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102123:	ba 00 00 00 00       	mov    $0x0,%edx
f0102128:	f7 75 f0             	divl   -0x10(%ebp)
f010212b:	89 d0                	mov    %edx,%eax
f010212d:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0102130:	29 c2                	sub    %eax,%edx
f0102132:	89 d0                	mov    %edx,%eax
f0102134:	89 45 e8             	mov    %eax,-0x18(%ebp)
			
	for (i = 1; i < range_end/PAGE_SIZE; i++)
f0102137:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
f010213e:	e9 91 00 00 00       	jmp    f01021d4 <initialize_paging+0xe2>
	{
		frames_info[i].references = 0;
f0102143:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0102149:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010214c:	89 d0                	mov    %edx,%eax
f010214e:	01 c0                	add    %eax,%eax
f0102150:	01 d0                	add    %edx,%eax
f0102152:	c1 e0 02             	shl    $0x2,%eax
f0102155:	01 c8                	add    %ecx,%eax
f0102157:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
f010215d:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0102163:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102166:	89 d0                	mov    %edx,%eax
f0102168:	01 c0                	add    %eax,%eax
f010216a:	01 d0                	add    %edx,%eax
f010216c:	c1 e0 02             	shl    $0x2,%eax
f010216f:	01 c8                	add    %ecx,%eax
f0102171:	8b 15 98 63 14 f0    	mov    0xf0146398,%edx
f0102177:	89 10                	mov    %edx,(%eax)
f0102179:	8b 00                	mov    (%eax),%eax
f010217b:	85 c0                	test   %eax,%eax
f010217d:	74 1d                	je     f010219c <initialize_paging+0xaa>
f010217f:	8b 0d 98 63 14 f0    	mov    0xf0146398,%ecx
f0102185:	8b 1d 9c 63 14 f0    	mov    0xf014639c,%ebx
f010218b:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010218e:	89 d0                	mov    %edx,%eax
f0102190:	01 c0                	add    %eax,%eax
f0102192:	01 d0                	add    %edx,%eax
f0102194:	c1 e0 02             	shl    $0x2,%eax
f0102197:	01 d8                	add    %ebx,%eax
f0102199:	89 41 04             	mov    %eax,0x4(%ecx)
f010219c:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f01021a2:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01021a5:	89 d0                	mov    %edx,%eax
f01021a7:	01 c0                	add    %eax,%eax
f01021a9:	01 d0                	add    %edx,%eax
f01021ab:	c1 e0 02             	shl    $0x2,%eax
f01021ae:	01 c8                	add    %ecx,%eax
f01021b0:	a3 98 63 14 f0       	mov    %eax,0xf0146398
f01021b5:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f01021bb:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01021be:	89 d0                	mov    %edx,%eax
f01021c0:	01 c0                	add    %eax,%eax
f01021c2:	01 d0                	add    %edx,%eax
f01021c4:	c1 e0 02             	shl    $0x2,%eax
f01021c7:	01 c8                	add    %ecx,%eax
f01021c9:	c7 40 04 98 63 14 f0 	movl   $0xf0146398,0x4(%eax)
	
	frames_info[0].references = 1;
	
	int range_end = ROUNDUP(PHYS_IO_MEM,PAGE_SIZE);
			
	for (i = 1; i < range_end/PAGE_SIZE; i++)
f01021d0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f01021d4:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01021d7:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f01021dd:	85 c0                	test   %eax,%eax
f01021df:	0f 48 c2             	cmovs  %edx,%eax
f01021e2:	c1 f8 0c             	sar    $0xc,%eax
f01021e5:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f01021e8:	0f 8f 55 ff ff ff    	jg     f0102143 <initialize_paging+0x51>
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
	
	for (i = PHYS_IO_MEM/PAGE_SIZE ; i < PHYS_EXTENDED_MEM/PAGE_SIZE; i++)
f01021ee:	c7 45 f4 a0 00 00 00 	movl   $0xa0,-0xc(%ebp)
f01021f5:	eb 1e                	jmp    f0102215 <initialize_paging+0x123>
	{
		frames_info[i].references = 1;
f01021f7:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f01021fd:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102200:	89 d0                	mov    %edx,%eax
f0102202:	01 c0                	add    %eax,%eax
f0102204:	01 d0                	add    %edx,%eax
f0102206:	c1 e0 02             	shl    $0x2,%eax
f0102209:	01 c8                	add    %ecx,%eax
f010220b:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
	
	for (i = PHYS_IO_MEM/PAGE_SIZE ; i < PHYS_EXTENDED_MEM/PAGE_SIZE; i++)
f0102211:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0102215:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
f010221c:	7e d9                	jle    f01021f7 <initialize_paging+0x105>
	{
		frames_info[i].references = 1;
	}
		
	range_end = ROUNDUP(K_PHYSICAL_ADDRESS(ptr_free_mem), PAGE_SIZE);
f010221e:	c7 45 e4 00 10 00 00 	movl   $0x1000,-0x1c(%ebp)
f0102225:	a1 a0 63 14 f0       	mov    0xf01463a0,%eax
f010222a:	89 45 e0             	mov    %eax,-0x20(%ebp)
f010222d:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0102234:	77 23                	ja     f0102259 <initialize_paging+0x167>
f0102236:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0102239:	89 44 24 0c          	mov    %eax,0xc(%esp)
f010223d:	c7 44 24 08 d4 58 10 	movl   $0xf01058d4,0x8(%esp)
f0102244:	f0 
f0102245:	c7 44 24 04 1e 01 00 	movl   $0x11e,0x4(%esp)
f010224c:	00 
f010224d:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0102254:	e8 c1 de ff ff       	call   f010011a <_panic>
f0102259:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010225c:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0102262:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0102265:	01 d0                	add    %edx,%eax
f0102267:	83 e8 01             	sub    $0x1,%eax
f010226a:	89 45 dc             	mov    %eax,-0x24(%ebp)
f010226d:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0102270:	ba 00 00 00 00       	mov    $0x0,%edx
f0102275:	f7 75 e4             	divl   -0x1c(%ebp)
f0102278:	89 d0                	mov    %edx,%eax
f010227a:	8b 55 dc             	mov    -0x24(%ebp),%edx
f010227d:	29 c2                	sub    %eax,%edx
f010227f:	89 d0                	mov    %edx,%eax
f0102281:	89 45 e8             	mov    %eax,-0x18(%ebp)
	
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
f0102284:	c7 45 f4 00 01 00 00 	movl   $0x100,-0xc(%ebp)
f010228b:	eb 1e                	jmp    f01022ab <initialize_paging+0x1b9>
	{
		frames_info[i].references = 1;
f010228d:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0102293:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102296:	89 d0                	mov    %edx,%eax
f0102298:	01 c0                	add    %eax,%eax
f010229a:	01 d0                	add    %edx,%eax
f010229c:	c1 e0 02             	shl    $0x2,%eax
f010229f:	01 c8                	add    %ecx,%eax
f01022a1:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
		frames_info[i].references = 1;
	}
		
	range_end = ROUNDUP(K_PHYSICAL_ADDRESS(ptr_free_mem), PAGE_SIZE);
	
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
f01022a7:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f01022ab:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01022ae:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f01022b4:	85 c0                	test   %eax,%eax
f01022b6:	0f 48 c2             	cmovs  %edx,%eax
f01022b9:	c1 f8 0c             	sar    $0xc,%eax
f01022bc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f01022bf:	7f cc                	jg     f010228d <initialize_paging+0x19b>
	{
		frames_info[i].references = 1;
	}
	
	for (i = range_end/PAGE_SIZE ; i < number_of_frames; i++)
f01022c1:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01022c4:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f01022ca:	85 c0                	test   %eax,%eax
f01022cc:	0f 48 c2             	cmovs  %edx,%eax
f01022cf:	c1 f8 0c             	sar    $0xc,%eax
f01022d2:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01022d5:	e9 91 00 00 00       	jmp    f010236b <initialize_paging+0x279>
	{
		frames_info[i].references = 0;
f01022da:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f01022e0:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01022e3:	89 d0                	mov    %edx,%eax
f01022e5:	01 c0                	add    %eax,%eax
f01022e7:	01 d0                	add    %edx,%eax
f01022e9:	c1 e0 02             	shl    $0x2,%eax
f01022ec:	01 c8                	add    %ecx,%eax
f01022ee:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
f01022f4:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f01022fa:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01022fd:	89 d0                	mov    %edx,%eax
f01022ff:	01 c0                	add    %eax,%eax
f0102301:	01 d0                	add    %edx,%eax
f0102303:	c1 e0 02             	shl    $0x2,%eax
f0102306:	01 c8                	add    %ecx,%eax
f0102308:	8b 15 98 63 14 f0    	mov    0xf0146398,%edx
f010230e:	89 10                	mov    %edx,(%eax)
f0102310:	8b 00                	mov    (%eax),%eax
f0102312:	85 c0                	test   %eax,%eax
f0102314:	74 1d                	je     f0102333 <initialize_paging+0x241>
f0102316:	8b 0d 98 63 14 f0    	mov    0xf0146398,%ecx
f010231c:	8b 1d 9c 63 14 f0    	mov    0xf014639c,%ebx
f0102322:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102325:	89 d0                	mov    %edx,%eax
f0102327:	01 c0                	add    %eax,%eax
f0102329:	01 d0                	add    %edx,%eax
f010232b:	c1 e0 02             	shl    $0x2,%eax
f010232e:	01 d8                	add    %ebx,%eax
f0102330:	89 41 04             	mov    %eax,0x4(%ecx)
f0102333:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0102339:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010233c:	89 d0                	mov    %edx,%eax
f010233e:	01 c0                	add    %eax,%eax
f0102340:	01 d0                	add    %edx,%eax
f0102342:	c1 e0 02             	shl    $0x2,%eax
f0102345:	01 c8                	add    %ecx,%eax
f0102347:	a3 98 63 14 f0       	mov    %eax,0xf0146398
f010234c:	8b 0d 9c 63 14 f0    	mov    0xf014639c,%ecx
f0102352:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102355:	89 d0                	mov    %edx,%eax
f0102357:	01 c0                	add    %eax,%eax
f0102359:	01 d0                	add    %edx,%eax
f010235b:	c1 e0 02             	shl    $0x2,%eax
f010235e:	01 c8                	add    %ecx,%eax
f0102360:	c7 40 04 98 63 14 f0 	movl   $0xf0146398,0x4(%eax)
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
	{
		frames_info[i].references = 1;
	}
	
	for (i = range_end/PAGE_SIZE ; i < number_of_frames; i++)
f0102367:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f010236b:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010236e:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f0102373:	39 c2                	cmp    %eax,%edx
f0102375:	0f 82 5f ff ff ff    	jb     f01022da <initialize_paging+0x1e8>
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
}
f010237b:	83 c4 34             	add    $0x34,%esp
f010237e:	5b                   	pop    %ebx
f010237f:	5d                   	pop    %ebp
f0102380:	c3                   	ret    

f0102381 <initialize_frame_info>:
// Initialize a Frame_Info structure.
// The result has null links and 0 references.
// Note that the corresponding physical frame is NOT initialized!
//
void initialize_frame_info(struct Frame_Info *ptr_frame_info)
{
f0102381:	55                   	push   %ebp
f0102382:	89 e5                	mov    %esp,%ebp
f0102384:	83 ec 18             	sub    $0x18,%esp
	memset(ptr_frame_info, 0, sizeof(*ptr_frame_info));
f0102387:	c7 44 24 08 0c 00 00 	movl   $0xc,0x8(%esp)
f010238e:	00 
f010238f:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0102396:	00 
f0102397:	8b 45 08             	mov    0x8(%ebp),%eax
f010239a:	89 04 24             	mov    %eax,(%esp)
f010239d:	e8 5c 24 00 00       	call   f01047fe <memset>
}
f01023a2:	c9                   	leave  
f01023a3:	c3                   	ret    

f01023a4 <allocate_frame>:
//   E_NO_MEM -- otherwise
//
// Hint: use LIST_FIRST, LIST_REMOVE, and initialize_frame_info
// Hint: references should not be incremented
int allocate_frame(struct Frame_Info **ptr_frame_info)
{
f01023a4:	55                   	push   %ebp
f01023a5:	89 e5                	mov    %esp,%ebp
f01023a7:	83 ec 18             	sub    $0x18,%esp
	// Fill this function in	
	*ptr_frame_info = LIST_FIRST(&free_frame_list);
f01023aa:	8b 15 98 63 14 f0    	mov    0xf0146398,%edx
f01023b0:	8b 45 08             	mov    0x8(%ebp),%eax
f01023b3:	89 10                	mov    %edx,(%eax)
	if(*ptr_frame_info == NULL)
f01023b5:	8b 45 08             	mov    0x8(%ebp),%eax
f01023b8:	8b 00                	mov    (%eax),%eax
f01023ba:	85 c0                	test   %eax,%eax
f01023bc:	75 07                	jne    f01023c5 <allocate_frame+0x21>
		return E_NO_MEM;
f01023be:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
f01023c3:	eb 40                	jmp    f0102405 <allocate_frame+0x61>
	
	LIST_REMOVE(*ptr_frame_info);
f01023c5:	8b 45 08             	mov    0x8(%ebp),%eax
f01023c8:	8b 00                	mov    (%eax),%eax
f01023ca:	8b 00                	mov    (%eax),%eax
f01023cc:	85 c0                	test   %eax,%eax
f01023ce:	74 12                	je     f01023e2 <allocate_frame+0x3e>
f01023d0:	8b 45 08             	mov    0x8(%ebp),%eax
f01023d3:	8b 00                	mov    (%eax),%eax
f01023d5:	8b 00                	mov    (%eax),%eax
f01023d7:	8b 55 08             	mov    0x8(%ebp),%edx
f01023da:	8b 12                	mov    (%edx),%edx
f01023dc:	8b 52 04             	mov    0x4(%edx),%edx
f01023df:	89 50 04             	mov    %edx,0x4(%eax)
f01023e2:	8b 45 08             	mov    0x8(%ebp),%eax
f01023e5:	8b 00                	mov    (%eax),%eax
f01023e7:	8b 40 04             	mov    0x4(%eax),%eax
f01023ea:	8b 55 08             	mov    0x8(%ebp),%edx
f01023ed:	8b 12                	mov    (%edx),%edx
f01023ef:	8b 12                	mov    (%edx),%edx
f01023f1:	89 10                	mov    %edx,(%eax)
	initialize_frame_info(*ptr_frame_info);
f01023f3:	8b 45 08             	mov    0x8(%ebp),%eax
f01023f6:	8b 00                	mov    (%eax),%eax
f01023f8:	89 04 24             	mov    %eax,(%esp)
f01023fb:	e8 81 ff ff ff       	call   f0102381 <initialize_frame_info>
	return 0;
f0102400:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0102405:	c9                   	leave  
f0102406:	c3                   	ret    

f0102407 <free_frame>:
//
// Return a frame to the free_frame_list.
// (This function should only be called when ptr_frame_info->references reaches 0.)
//
void free_frame(struct Frame_Info *ptr_frame_info)
{
f0102407:	55                   	push   %ebp
f0102408:	89 e5                	mov    %esp,%ebp
	// Fill this function in
	LIST_INSERT_HEAD(&free_frame_list, ptr_frame_info);
f010240a:	8b 15 98 63 14 f0    	mov    0xf0146398,%edx
f0102410:	8b 45 08             	mov    0x8(%ebp),%eax
f0102413:	89 10                	mov    %edx,(%eax)
f0102415:	8b 45 08             	mov    0x8(%ebp),%eax
f0102418:	8b 00                	mov    (%eax),%eax
f010241a:	85 c0                	test   %eax,%eax
f010241c:	74 0b                	je     f0102429 <free_frame+0x22>
f010241e:	a1 98 63 14 f0       	mov    0xf0146398,%eax
f0102423:	8b 55 08             	mov    0x8(%ebp),%edx
f0102426:	89 50 04             	mov    %edx,0x4(%eax)
f0102429:	8b 45 08             	mov    0x8(%ebp),%eax
f010242c:	a3 98 63 14 f0       	mov    %eax,0xf0146398
f0102431:	8b 45 08             	mov    0x8(%ebp),%eax
f0102434:	c7 40 04 98 63 14 f0 	movl   $0xf0146398,0x4(%eax)
}
f010243b:	5d                   	pop    %ebp
f010243c:	c3                   	ret    

f010243d <decrement_references>:
//
// Decrement the reference count on a frame
// freeing it if there are no more references.
//
void decrement_references(struct Frame_Info* ptr_frame_info)
{
f010243d:	55                   	push   %ebp
f010243e:	89 e5                	mov    %esp,%ebp
f0102440:	83 ec 04             	sub    $0x4,%esp
	if (--(ptr_frame_info->references) == 0)
f0102443:	8b 45 08             	mov    0x8(%ebp),%eax
f0102446:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f010244a:	8d 50 ff             	lea    -0x1(%eax),%edx
f010244d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102450:	66 89 50 08          	mov    %dx,0x8(%eax)
f0102454:	8b 45 08             	mov    0x8(%ebp),%eax
f0102457:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f010245b:	66 85 c0             	test   %ax,%ax
f010245e:	75 0b                	jne    f010246b <decrement_references+0x2e>
		free_frame(ptr_frame_info);
f0102460:	8b 45 08             	mov    0x8(%ebp),%eax
f0102463:	89 04 24             	mov    %eax,(%esp)
f0102466:	e8 9c ff ff ff       	call   f0102407 <free_frame>
}
f010246b:	c9                   	leave  
f010246c:	c3                   	ret    

f010246d <get_page_table>:
//
// Hint: you can use "to_physical_address()" to turn a Frame_Info*
// into the physical address of the frame it refers to. 

int get_page_table(uint32 *ptr_page_directory, const void *virtual_address, int create, uint32 **ptr_page_table)
{
f010246d:	55                   	push   %ebp
f010246e:	89 e5                	mov    %esp,%ebp
f0102470:	83 ec 38             	sub    $0x38,%esp
	// Fill this function in
	uint32 page_directory_entry = ptr_page_directory[PDX(virtual_address)];
f0102473:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102476:	c1 e8 16             	shr    $0x16,%eax
f0102479:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0102480:	8b 45 08             	mov    0x8(%ebp),%eax
f0102483:	01 d0                	add    %edx,%eax
f0102485:	8b 00                	mov    (%eax),%eax
f0102487:	89 45 f4             	mov    %eax,-0xc(%ebp)

	*ptr_page_table = K_VIRTUAL_ADDRESS(EXTRACT_ADDRESS(page_directory_entry)) ;
f010248a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f010248d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0102492:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0102495:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102498:	c1 e8 0c             	shr    $0xc,%eax
f010249b:	89 45 ec             	mov    %eax,-0x14(%ebp)
f010249e:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f01024a3:	39 45 ec             	cmp    %eax,-0x14(%ebp)
f01024a6:	72 23                	jb     f01024cb <get_page_table+0x5e>
f01024a8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01024ab:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01024af:	c7 44 24 08 1c 59 10 	movl   $0xf010591c,0x8(%esp)
f01024b6:	f0 
f01024b7:	c7 44 24 04 79 01 00 	movl   $0x179,0x4(%esp)
f01024be:	00 
f01024bf:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f01024c6:	e8 4f dc ff ff       	call   f010011a <_panic>
f01024cb:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01024ce:	2d 00 00 00 10       	sub    $0x10000000,%eax
f01024d3:	89 c2                	mov    %eax,%edx
f01024d5:	8b 45 14             	mov    0x14(%ebp),%eax
f01024d8:	89 10                	mov    %edx,(%eax)
	
	if (page_directory_entry == 0)
f01024da:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01024de:	0f 85 dc 00 00 00    	jne    f01025c0 <get_page_table+0x153>
	{
		if (create)
f01024e4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01024e8:	0f 84 c2 00 00 00    	je     f01025b0 <get_page_table+0x143>
		{
			struct Frame_Info* ptr_frame_info;
			int err = allocate_frame(&ptr_frame_info) ;
f01024ee:	8d 45 d8             	lea    -0x28(%ebp),%eax
f01024f1:	89 04 24             	mov    %eax,(%esp)
f01024f4:	e8 ab fe ff ff       	call   f01023a4 <allocate_frame>
f01024f9:	89 45 e8             	mov    %eax,-0x18(%ebp)
			if(err == E_NO_MEM)
f01024fc:	83 7d e8 fc          	cmpl   $0xfffffffc,-0x18(%ebp)
f0102500:	75 13                	jne    f0102515 <get_page_table+0xa8>
			{
				*ptr_page_table = 0;
f0102502:	8b 45 14             	mov    0x14(%ebp),%eax
f0102505:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
				return E_NO_MEM;
f010250b:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
f0102510:	e9 b0 00 00 00       	jmp    f01025c5 <get_page_table+0x158>
			}

			uint32 phys_page_table = to_physical_address(ptr_frame_info);
f0102515:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0102518:	89 04 24             	mov    %eax,(%esp)
f010251b:	e8 13 f7 ff ff       	call   f0101c33 <to_physical_address>
f0102520:	89 45 e4             	mov    %eax,-0x1c(%ebp)
			*ptr_page_table = K_VIRTUAL_ADDRESS(phys_page_table) ;
f0102523:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0102526:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0102529:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010252c:	c1 e8 0c             	shr    $0xc,%eax
f010252f:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0102532:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f0102537:	39 45 dc             	cmp    %eax,-0x24(%ebp)
f010253a:	72 23                	jb     f010255f <get_page_table+0xf2>
f010253c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010253f:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0102543:	c7 44 24 08 1c 59 10 	movl   $0xf010591c,0x8(%esp)
f010254a:	f0 
f010254b:	c7 44 24 04 88 01 00 	movl   $0x188,0x4(%esp)
f0102552:	00 
f0102553:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f010255a:	e8 bb db ff ff       	call   f010011a <_panic>
f010255f:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0102562:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102567:	89 c2                	mov    %eax,%edx
f0102569:	8b 45 14             	mov    0x14(%ebp),%eax
f010256c:	89 10                	mov    %edx,(%eax)
			
			//initialize new page table by 0's
			memset(*ptr_page_table , 0, PAGE_SIZE);
f010256e:	8b 45 14             	mov    0x14(%ebp),%eax
f0102571:	8b 00                	mov    (%eax),%eax
f0102573:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f010257a:	00 
f010257b:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0102582:	00 
f0102583:	89 04 24             	mov    %eax,(%esp)
f0102586:	e8 73 22 00 00       	call   f01047fe <memset>

			ptr_frame_info->references = 1;
f010258b:	8b 45 d8             	mov    -0x28(%ebp),%eax
f010258e:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
			ptr_page_directory[PDX(virtual_address)] = CONSTRUCT_ENTRY(phys_page_table, PERM_PRESENT | PERM_USER | PERM_WRITEABLE);
f0102594:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102597:	c1 e8 16             	shr    $0x16,%eax
f010259a:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f01025a1:	8b 45 08             	mov    0x8(%ebp),%eax
f01025a4:	01 d0                	add    %edx,%eax
f01025a6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f01025a9:	83 ca 07             	or     $0x7,%edx
f01025ac:	89 10                	mov    %edx,(%eax)
f01025ae:	eb 10                	jmp    f01025c0 <get_page_table+0x153>
		}
		else
		{
			*ptr_page_table = 0;
f01025b0:	8b 45 14             	mov    0x14(%ebp),%eax
f01025b3:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
			return 0;
f01025b9:	b8 00 00 00 00       	mov    $0x0,%eax
f01025be:	eb 05                	jmp    f01025c5 <get_page_table+0x158>
		}
	}	
	return 0;
f01025c0:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01025c5:	c9                   	leave  
f01025c6:	c3                   	ret    

f01025c7 <map_frame>:
//   E_NO_MEM, if page table couldn't be allocated
//
// Hint: implement using get_page_table() and unmap_frame().
//
int map_frame(uint32 *ptr_page_directory, struct Frame_Info *ptr_frame_info, void *virtual_address, int perm)
{
f01025c7:	55                   	push   %ebp
f01025c8:	89 e5                	mov    %esp,%ebp
f01025ca:	83 ec 28             	sub    $0x28,%esp
	// Fill this function in
	uint32 physical_address = to_physical_address(ptr_frame_info);
f01025cd:	8b 45 0c             	mov    0xc(%ebp),%eax
f01025d0:	89 04 24             	mov    %eax,(%esp)
f01025d3:	e8 5b f6 ff ff       	call   f0101c33 <to_physical_address>
f01025d8:	89 45 f4             	mov    %eax,-0xc(%ebp)
	uint32 *ptr_page_table;
	if( get_page_table(ptr_page_directory, virtual_address, 1, &ptr_page_table) == 0)
f01025db:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01025de:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01025e2:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
f01025e9:	00 
f01025ea:	8b 45 10             	mov    0x10(%ebp),%eax
f01025ed:	89 44 24 04          	mov    %eax,0x4(%esp)
f01025f1:	8b 45 08             	mov    0x8(%ebp),%eax
f01025f4:	89 04 24             	mov    %eax,(%esp)
f01025f7:	e8 71 fe ff ff       	call   f010246d <get_page_table>
f01025fc:	85 c0                	test   %eax,%eax
f01025fe:	75 75                	jne    f0102675 <map_frame+0xae>
	{
		uint32 page_table_entry = ptr_page_table[PTX(virtual_address)];
f0102600:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102603:	8b 55 10             	mov    0x10(%ebp),%edx
f0102606:	c1 ea 0c             	shr    $0xc,%edx
f0102609:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f010260f:	c1 e2 02             	shl    $0x2,%edx
f0102612:	01 d0                	add    %edx,%eax
f0102614:	8b 00                	mov    (%eax),%eax
f0102616:	89 45 f0             	mov    %eax,-0x10(%ebp)
		
		
		if( EXTRACT_ADDRESS(page_table_entry) != physical_address)
f0102619:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010261c:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0102621:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f0102624:	74 48                	je     f010266e <map_frame+0xa7>
		{
			if( page_table_entry != 0)
f0102626:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f010262a:	74 12                	je     f010263e <map_frame+0x77>
			{				
				unmap_frame(ptr_page_directory , virtual_address);
f010262c:	8b 45 10             	mov    0x10(%ebp),%eax
f010262f:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102633:	8b 45 08             	mov    0x8(%ebp),%eax
f0102636:	89 04 24             	mov    %eax,(%esp)
f0102639:	e8 b7 00 00 00       	call   f01026f5 <unmap_frame>
			}
			ptr_frame_info->references++;
f010263e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102641:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0102645:	8d 50 01             	lea    0x1(%eax),%edx
f0102648:	8b 45 0c             	mov    0xc(%ebp),%eax
f010264b:	66 89 50 08          	mov    %dx,0x8(%eax)
			ptr_page_table[PTX(virtual_address)] = CONSTRUCT_ENTRY(physical_address , perm | PERM_PRESENT);
f010264f:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102652:	8b 55 10             	mov    0x10(%ebp),%edx
f0102655:	c1 ea 0c             	shr    $0xc,%edx
f0102658:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f010265e:	c1 e2 02             	shl    $0x2,%edx
f0102661:	01 c2                	add    %eax,%edx
f0102663:	8b 45 14             	mov    0x14(%ebp),%eax
f0102666:	0b 45 f4             	or     -0xc(%ebp),%eax
f0102669:	83 c8 01             	or     $0x1,%eax
f010266c:	89 02                	mov    %eax,(%edx)
			
		}		
		return 0;
f010266e:	b8 00 00 00 00       	mov    $0x0,%eax
f0102673:	eb 05                	jmp    f010267a <map_frame+0xb3>
	}	
	return E_NO_MEM;
f0102675:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
}
f010267a:	c9                   	leave  
f010267b:	c3                   	ret    

f010267c <get_frame_info>:
// Return 0 if there is no frame mapped at virtual_address.
//
// Hint: implement using get_page_table() and get_frame_info().
//
struct Frame_Info * get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table)
{
f010267c:	55                   	push   %ebp
f010267d:	89 e5                	mov    %esp,%ebp
f010267f:	83 ec 28             	sub    $0x28,%esp
	// Fill this function in	
	uint32 ret =  get_page_table(ptr_page_directory, virtual_address, 0, ptr_page_table) ;
f0102682:	8b 45 10             	mov    0x10(%ebp),%eax
f0102685:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0102689:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
f0102690:	00 
f0102691:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102694:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102698:	8b 45 08             	mov    0x8(%ebp),%eax
f010269b:	89 04 24             	mov    %eax,(%esp)
f010269e:	e8 ca fd ff ff       	call   f010246d <get_page_table>
f01026a3:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if((*ptr_page_table) != 0)
f01026a6:	8b 45 10             	mov    0x10(%ebp),%eax
f01026a9:	8b 00                	mov    (%eax),%eax
f01026ab:	85 c0                	test   %eax,%eax
f01026ad:	74 3f                	je     f01026ee <get_frame_info+0x72>
	{	
		uint32 index_page_table = PTX(virtual_address);
f01026af:	8b 45 0c             	mov    0xc(%ebp),%eax
f01026b2:	c1 e8 0c             	shr    $0xc,%eax
f01026b5:	25 ff 03 00 00       	and    $0x3ff,%eax
f01026ba:	89 45 f0             	mov    %eax,-0x10(%ebp)
		uint32 page_table_entry = (*ptr_page_table)[index_page_table];
f01026bd:	8b 45 10             	mov    0x10(%ebp),%eax
f01026c0:	8b 00                	mov    (%eax),%eax
f01026c2:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01026c5:	c1 e2 02             	shl    $0x2,%edx
f01026c8:	01 d0                	add    %edx,%eax
f01026ca:	8b 00                	mov    (%eax),%eax
f01026cc:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if( page_table_entry != 0)	
f01026cf:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f01026d3:	74 12                	je     f01026e7 <get_frame_info+0x6b>
			return to_frame_info( EXTRACT_ADDRESS ( page_table_entry ) );
f01026d5:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01026d8:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f01026dd:	89 04 24             	mov    %eax,(%esp)
f01026e0:	e8 64 f5 ff ff       	call   f0101c49 <to_frame_info>
f01026e5:	eb 0c                	jmp    f01026f3 <get_frame_info+0x77>
		return 0;
f01026e7:	b8 00 00 00 00       	mov    $0x0,%eax
f01026ec:	eb 05                	jmp    f01026f3 <get_frame_info+0x77>
	}
	return 0;
f01026ee:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01026f3:	c9                   	leave  
f01026f4:	c3                   	ret    

f01026f5 <unmap_frame>:
//
// Hint: implement using get_frame_info(),
// 	tlb_invalidate(), and decrement_references().
//
void unmap_frame(uint32 *ptr_page_directory, void *virtual_address)
{
f01026f5:	55                   	push   %ebp
f01026f6:	89 e5                	mov    %esp,%ebp
f01026f8:	83 ec 28             	sub    $0x28,%esp
	// Fill this function in
	uint32 *ptr_page_table;
	struct Frame_Info* ptr_frame_info = get_frame_info(ptr_page_directory, virtual_address, &ptr_page_table);
f01026fb:	8d 45 f0             	lea    -0x10(%ebp),%eax
f01026fe:	89 44 24 08          	mov    %eax,0x8(%esp)
f0102702:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102705:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102709:	8b 45 08             	mov    0x8(%ebp),%eax
f010270c:	89 04 24             	mov    %eax,(%esp)
f010270f:	e8 68 ff ff ff       	call   f010267c <get_frame_info>
f0102714:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if( ptr_frame_info != 0 )
f0102717:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f010271b:	74 37                	je     f0102754 <unmap_frame+0x5f>
	{
		decrement_references(ptr_frame_info);
f010271d:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102720:	89 04 24             	mov    %eax,(%esp)
f0102723:	e8 15 fd ff ff       	call   f010243d <decrement_references>
		ptr_page_table[PTX(virtual_address)] = 0;
f0102728:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010272b:	8b 55 0c             	mov    0xc(%ebp),%edx
f010272e:	c1 ea 0c             	shr    $0xc,%edx
f0102731:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0102737:	c1 e2 02             	shl    $0x2,%edx
f010273a:	01 d0                	add    %edx,%eax
f010273c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		tlb_invalidate(ptr_page_directory, virtual_address);
f0102742:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102745:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102749:	8b 45 08             	mov    0x8(%ebp),%eax
f010274c:	89 04 24             	mov    %eax,(%esp)
f010274f:	e8 cb e8 ff ff       	call   f010101f <tlb_invalidate>
	}	
}
f0102754:	c9                   	leave  
f0102755:	c3                   	ret    

f0102756 <get_page>:
//		or to allocate any necessary page tables.
// 	HINT: 	remember to free the allocated frame if there is no space 
//		for the necessary page tables

int get_page(uint32* ptr_page_directory, void *virtual_address, int perm)
{
f0102756:	55                   	push   %ebp
f0102757:	89 e5                	mov    %esp,%ebp
f0102759:	83 ec 18             	sub    $0x18,%esp
	// PROJECT 2008: Your code here.
	panic("get_page function is not completed yet") ;
f010275c:	c7 44 24 08 4c 59 10 	movl   $0xf010594c,0x8(%esp)
f0102763:	f0 
f0102764:	c7 44 24 04 12 02 00 	movl   $0x212,0x4(%esp)
f010276b:	00 
f010276c:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0102773:	e8 a2 d9 ff ff       	call   f010011a <_panic>

f0102778 <calculate_required_frames>:
	return 0 ;
}

//[2] calculate_required_frames: 
uint32 calculate_required_frames(uint32* ptr_page_directory, uint32 start_virtual_address, uint32 size)
{
f0102778:	55                   	push   %ebp
f0102779:	89 e5                	mov    %esp,%ebp
f010277b:	83 ec 18             	sub    $0x18,%esp
	// PROJECT 2008: Your code here.
	panic("calculate_required_frames function is not completed yet") ;
f010277e:	c7 44 24 08 74 59 10 	movl   $0xf0105974,0x8(%esp)
f0102785:	f0 
f0102786:	c7 44 24 04 29 02 00 	movl   $0x229,0x4(%esp)
f010278d:	00 
f010278e:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f0102795:	e8 80 d9 ff ff       	call   f010011a <_panic>

f010279a <calculate_free_frames>:


//[3] calculate_free_frames:

uint32 calculate_free_frames()
{
f010279a:	55                   	push   %ebp
f010279b:	89 e5                	mov    %esp,%ebp
f010279d:	83 ec 10             	sub    $0x10,%esp
	// PROJECT 2008: Your code here.
	//panic("calculate_free_frames function is not completed yet") ;
	
	//calculate the free frames from the free frame list
	struct Frame_Info *ptr;
	uint32 cnt = 0 ; 
f01027a0:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
	LIST_FOREACH(ptr, &free_frame_list)
f01027a7:	a1 98 63 14 f0       	mov    0xf0146398,%eax
f01027ac:	89 45 fc             	mov    %eax,-0x4(%ebp)
f01027af:	eb 0c                	jmp    f01027bd <calculate_free_frames+0x23>
	{
		cnt++ ;
f01027b1:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
	//panic("calculate_free_frames function is not completed yet") ;
	
	//calculate the free frames from the free frame list
	struct Frame_Info *ptr;
	uint32 cnt = 0 ; 
	LIST_FOREACH(ptr, &free_frame_list)
f01027b5:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01027b8:	8b 00                	mov    (%eax),%eax
f01027ba:	89 45 fc             	mov    %eax,-0x4(%ebp)
f01027bd:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f01027c1:	75 ee                	jne    f01027b1 <calculate_free_frames+0x17>
	{
		cnt++ ;
	}
	return cnt;
f01027c3:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f01027c6:	c9                   	leave  
f01027c7:	c3                   	ret    

f01027c8 <freeMem>:
//	Steps:
//		1) Unmap all mapped pages in the range [virtual_address, virtual_address + size ]
//		2) Free all mapped page tables in this range

void freeMem(uint32* ptr_page_directory, void *virtual_address, uint32 size)
{
f01027c8:	55                   	push   %ebp
f01027c9:	89 e5                	mov    %esp,%ebp
f01027cb:	83 ec 18             	sub    $0x18,%esp
	// PROJECT 2008: Your code here.
	panic("freeMem function is not completed yet") ;
f01027ce:	c7 44 24 08 ac 59 10 	movl   $0xf01059ac,0x8(%esp)
f01027d5:	f0 
f01027d6:	c7 44 24 04 50 02 00 	movl   $0x250,0x4(%esp)
f01027dd:	00 
f01027de:	c7 04 24 05 59 10 f0 	movl   $0xf0105905,(%esp)
f01027e5:	e8 30 d9 ff ff       	call   f010011a <_panic>

f01027ea <allocate_environment>:
//
// Returns 0 on success, < 0 on failure.  Errors include:
//	E_NO_FREE_ENV if all NENVS environments are allocated
//
int allocate_environment(struct Env** e)
{	
f01027ea:	55                   	push   %ebp
f01027eb:	89 e5                	mov    %esp,%ebp
	if (!(*e = LIST_FIRST(&env_free_list)))
f01027ed:	8b 15 14 5b 14 f0    	mov    0xf0145b14,%edx
f01027f3:	8b 45 08             	mov    0x8(%ebp),%eax
f01027f6:	89 10                	mov    %edx,(%eax)
f01027f8:	8b 45 08             	mov    0x8(%ebp),%eax
f01027fb:	8b 00                	mov    (%eax),%eax
f01027fd:	85 c0                	test   %eax,%eax
f01027ff:	75 07                	jne    f0102808 <allocate_environment+0x1e>
		return E_NO_FREE_ENV;
f0102801:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f0102806:	eb 05                	jmp    f010280d <allocate_environment+0x23>
	return 0;
f0102808:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010280d:	5d                   	pop    %ebp
f010280e:	c3                   	ret    

f010280f <free_environment>:

// Free the given environment "e", simply by adding it to the free environment list.
void free_environment(struct Env* e)
{
f010280f:	55                   	push   %ebp
f0102810:	89 e5                	mov    %esp,%ebp
	curenv = NULL;	
f0102812:	c7 05 10 5b 14 f0 00 	movl   $0x0,0xf0145b10
f0102819:	00 00 00 
	// return the environment to the free list
	e->env_status = ENV_FREE;
f010281c:	8b 45 08             	mov    0x8(%ebp),%eax
f010281f:	c7 40 54 00 00 00 00 	movl   $0x0,0x54(%eax)
	LIST_INSERT_HEAD(&env_free_list, e);
f0102826:	8b 15 14 5b 14 f0    	mov    0xf0145b14,%edx
f010282c:	8b 45 08             	mov    0x8(%ebp),%eax
f010282f:	89 50 44             	mov    %edx,0x44(%eax)
f0102832:	8b 45 08             	mov    0x8(%ebp),%eax
f0102835:	8b 40 44             	mov    0x44(%eax),%eax
f0102838:	85 c0                	test   %eax,%eax
f010283a:	74 0e                	je     f010284a <free_environment+0x3b>
f010283c:	a1 14 5b 14 f0       	mov    0xf0145b14,%eax
f0102841:	8b 55 08             	mov    0x8(%ebp),%edx
f0102844:	83 c2 44             	add    $0x44,%edx
f0102847:	89 50 48             	mov    %edx,0x48(%eax)
f010284a:	8b 45 08             	mov    0x8(%ebp),%eax
f010284d:	a3 14 5b 14 f0       	mov    %eax,0xf0145b14
f0102852:	8b 45 08             	mov    0x8(%ebp),%eax
f0102855:	c7 40 48 14 5b 14 f0 	movl   $0xf0145b14,0x48(%eax)
}
f010285c:	5d                   	pop    %ebp
f010285d:	c3                   	ret    

f010285e <initialize_environment>:
// and initialize the kernel portion of the new environment's address space.
// Do NOT (yet) map anything into the user portion
// of the environment's virtual address space.
//
void initialize_environment(struct Env* e, uint32* ptr_user_page_directory)
{	
f010285e:	55                   	push   %ebp
f010285f:	89 e5                	mov    %esp,%ebp
f0102861:	83 ec 18             	sub    $0x18,%esp
	// PROJECT 2008: Your code here.
	panic("initialize_environment function is not completed yet") ;
f0102864:	c7 44 24 08 30 5a 10 	movl   $0xf0105a30,0x8(%esp)
f010286b:	f0 
f010286c:	c7 44 24 04 77 00 00 	movl   $0x77,0x4(%esp)
f0102873:	00 
f0102874:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f010287b:	e8 9a d8 ff ff       	call   f010011a <_panic>

f0102880 <program_segment_alloc_map>:
//
// if the allocation failed, return E_NO_MEM 
// otherwise return 0
//
static int program_segment_alloc_map(struct Env *e, void *va, uint32 length)
{
f0102880:	55                   	push   %ebp
f0102881:	89 e5                	mov    %esp,%ebp
f0102883:	83 ec 18             	sub    $0x18,%esp
	//PROJECT 2008: Your code here.	
	panic("program_segment_alloc_map function is not completed yet") ;
f0102886:	c7 44 24 08 80 5a 10 	movl   $0xf0105a80,0x8(%esp)
f010288d:	f0 
f010288e:	c7 44 24 04 8e 00 00 	movl   $0x8e,0x4(%esp)
f0102895:	00 
f0102896:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f010289d:	e8 78 d8 ff ff       	call   f010011a <_panic>

f01028a2 <env_create>:
}

//
// Allocates a new env and loads the named user program into it.
struct UserProgramInfo* env_create(char* user_program_name)
{
f01028a2:	55                   	push   %ebp
f01028a3:	89 e5                	mov    %esp,%ebp
f01028a5:	83 ec 38             	sub    $0x38,%esp
	// PROJECT 2008: Your code here.
	panic("env_create function is not completed yet") ;
f01028a8:	c7 44 24 08 b8 5a 10 	movl   $0xf0105ab8,0x8(%esp)
f01028af:	f0 
f01028b0:	c7 44 24 04 99 00 00 	movl   $0x99,0x4(%esp)
f01028b7:	00 
f01028b8:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f01028bf:	e8 56 d8 ff ff       	call   f010011a <_panic>

f01028c4 <env_run>:
// Used to run the given environment "e", simply by 
// context switch from curenv to env e.
//  (This function does not return.)
//
void env_run(struct Env *e)
{
f01028c4:	55                   	push   %ebp
f01028c5:	89 e5                	mov    %esp,%ebp
f01028c7:	83 ec 28             	sub    $0x28,%esp
	if(curenv != e)
f01028ca:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01028cf:	3b 45 08             	cmp    0x8(%ebp),%eax
f01028d2:	74 27                	je     f01028fb <env_run+0x37>
	{		
		curenv = e ;
f01028d4:	8b 45 08             	mov    0x8(%ebp),%eax
f01028d7:	a3 10 5b 14 f0       	mov    %eax,0xf0145b10
		curenv->env_runs++ ;
f01028dc:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01028e1:	8b 50 58             	mov    0x58(%eax),%edx
f01028e4:	83 c2 01             	add    $0x1,%edx
f01028e7:	89 50 58             	mov    %edx,0x58(%eax)
		lcr3(curenv->env_cr3) ;	
f01028ea:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01028ef:	8b 40 60             	mov    0x60(%eax),%eax
f01028f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01028f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01028f8:	0f 22 d8             	mov    %eax,%cr3
	}	
	env_pop_tf(&(curenv->env_tf));
f01028fb:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102900:	89 04 24             	mov    %eax,(%esp)
f0102903:	e8 19 06 00 00       	call   f0102f21 <env_pop_tf>

f0102908 <env_free>:

//
// Frees environment "e" and all memory it uses.
// 
void env_free(struct Env *e)
{
f0102908:	55                   	push   %ebp
f0102909:	89 e5                	mov    %esp,%ebp
f010290b:	83 ec 18             	sub    $0x18,%esp
	// PROJECT 2008: Your code here.
	panic("env_free function is not completed yet") ;
f010290e:	c7 44 24 08 e4 5a 10 	movl   $0xf0105ae4,0x8(%esp)
f0102915:	f0 
f0102916:	c7 44 24 04 ef 00 00 	movl   $0xef,0x4(%esp)
f010291d:	00 
f010291e:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102925:	e8 f0 d7 ff ff       	call   f010011a <_panic>

f010292a <env_init>:
// Insert in reverse order, so that the first call to allocate_environment()
// returns envs[0].
//
void
env_init(void)
{	
f010292a:	55                   	push   %ebp
f010292b:	89 e5                	mov    %esp,%ebp
f010292d:	83 ec 10             	sub    $0x10,%esp
	int iEnv = NENV-1;
f0102930:	c7 45 fc ff 03 00 00 	movl   $0x3ff,-0x4(%ebp)
	for(; iEnv >= 0; iEnv--)
f0102937:	e9 8d 00 00 00       	jmp    f01029c9 <env_init+0x9f>
	{
		envs[iEnv].env_status = ENV_FREE;
f010293c:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f0102942:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0102945:	6b c0 64             	imul   $0x64,%eax,%eax
f0102948:	01 d0                	add    %edx,%eax
f010294a:	c7 40 54 00 00 00 00 	movl   $0x0,0x54(%eax)
		envs[iEnv].env_id = 0;
f0102951:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f0102957:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010295a:	6b c0 64             	imul   $0x64,%eax,%eax
f010295d:	01 d0                	add    %edx,%eax
f010295f:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
		LIST_INSERT_HEAD(&env_free_list, &envs[iEnv]);	
f0102966:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f010296c:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010296f:	6b c0 64             	imul   $0x64,%eax,%eax
f0102972:	01 d0                	add    %edx,%eax
f0102974:	8b 15 14 5b 14 f0    	mov    0xf0145b14,%edx
f010297a:	89 50 44             	mov    %edx,0x44(%eax)
f010297d:	8b 40 44             	mov    0x44(%eax),%eax
f0102980:	85 c0                	test   %eax,%eax
f0102982:	74 19                	je     f010299d <env_init+0x73>
f0102984:	a1 14 5b 14 f0       	mov    0xf0145b14,%eax
f0102989:	8b 0d 0c 5b 14 f0    	mov    0xf0145b0c,%ecx
f010298f:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102992:	6b d2 64             	imul   $0x64,%edx,%edx
f0102995:	01 ca                	add    %ecx,%edx
f0102997:	83 c2 44             	add    $0x44,%edx
f010299a:	89 50 48             	mov    %edx,0x48(%eax)
f010299d:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f01029a3:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01029a6:	6b c0 64             	imul   $0x64,%eax,%eax
f01029a9:	01 d0                	add    %edx,%eax
f01029ab:	a3 14 5b 14 f0       	mov    %eax,0xf0145b14
f01029b0:	8b 15 0c 5b 14 f0    	mov    0xf0145b0c,%edx
f01029b6:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01029b9:	6b c0 64             	imul   $0x64,%eax,%eax
f01029bc:	01 d0                	add    %edx,%eax
f01029be:	c7 40 48 14 5b 14 f0 	movl   $0xf0145b14,0x48(%eax)
//
void
env_init(void)
{	
	int iEnv = NENV-1;
	for(; iEnv >= 0; iEnv--)
f01029c5:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
f01029c9:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f01029cd:	0f 89 69 ff ff ff    	jns    f010293c <env_init+0x12>
	{
		envs[iEnv].env_status = ENV_FREE;
		envs[iEnv].env_id = 0;
		LIST_INSERT_HEAD(&env_free_list, &envs[iEnv]);	
	}
}
f01029d3:	c9                   	leave  
f01029d4:	c3                   	ret    

f01029d5 <complete_environment_initialization>:

void complete_environment_initialization(struct Env* e)
{	
f01029d5:	55                   	push   %ebp
f01029d6:	89 e5                	mov    %esp,%ebp
f01029d8:	83 ec 28             	sub    $0x28,%esp
	//VPT and UVPT map the env's own page table, with
	//different permissions.
	e->env_pgdir[PDX(VPT)]  = e->env_cr3 | PERM_PRESENT | PERM_WRITEABLE;
f01029db:	8b 45 08             	mov    0x8(%ebp),%eax
f01029de:	8b 40 5c             	mov    0x5c(%eax),%eax
f01029e1:	8d 90 fc 0e 00 00    	lea    0xefc(%eax),%edx
f01029e7:	8b 45 08             	mov    0x8(%ebp),%eax
f01029ea:	8b 40 60             	mov    0x60(%eax),%eax
f01029ed:	83 c8 03             	or     $0x3,%eax
f01029f0:	89 02                	mov    %eax,(%edx)
	e->env_pgdir[PDX(UVPT)] = e->env_cr3 | PERM_PRESENT | PERM_USER;
f01029f2:	8b 45 08             	mov    0x8(%ebp),%eax
f01029f5:	8b 40 5c             	mov    0x5c(%eax),%eax
f01029f8:	8d 90 f4 0e 00 00    	lea    0xef4(%eax),%edx
f01029fe:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a01:	8b 40 60             	mov    0x60(%eax),%eax
f0102a04:	83 c8 05             	or     $0x5,%eax
f0102a07:	89 02                	mov    %eax,(%edx)
	
	int32 generation;	
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f0102a09:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a0c:	8b 40 4c             	mov    0x4c(%eax),%eax
f0102a0f:	05 00 10 00 00       	add    $0x1000,%eax
f0102a14:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0102a19:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (generation <= 0)	// Don't create a negative env_id.
f0102a1c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0102a20:	7f 07                	jg     f0102a29 <complete_environment_initialization+0x54>
		generation = 1 << ENVGENSHIFT;
f0102a22:	c7 45 f4 00 10 00 00 	movl   $0x1000,-0xc(%ebp)
	e->env_id = generation | (e - envs);
f0102a29:	8b 55 08             	mov    0x8(%ebp),%edx
f0102a2c:	a1 0c 5b 14 f0       	mov    0xf0145b0c,%eax
f0102a31:	29 c2                	sub    %eax,%edx
f0102a33:	89 d0                	mov    %edx,%eax
f0102a35:	c1 f8 02             	sar    $0x2,%eax
f0102a38:	69 c0 29 5c 8f c2    	imul   $0xc28f5c29,%eax,%eax
f0102a3e:	0b 45 f4             	or     -0xc(%ebp),%eax
f0102a41:	89 c2                	mov    %eax,%edx
f0102a43:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a46:	89 50 4c             	mov    %edx,0x4c(%eax)
	
	// Set the basic status variables.
	e->env_parent_id = 0;//parent_id;
f0102a49:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a4c:	c7 40 50 00 00 00 00 	movl   $0x0,0x50(%eax)
	e->env_status = ENV_RUNNABLE;
f0102a53:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a56:	c7 40 54 01 00 00 00 	movl   $0x1,0x54(%eax)
	e->env_runs = 0;
f0102a5d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a60:	c7 40 58 00 00 00 00 	movl   $0x0,0x58(%eax)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f0102a67:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a6a:	c7 44 24 08 44 00 00 	movl   $0x44,0x8(%esp)
f0102a71:	00 
f0102a72:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0102a79:	00 
f0102a7a:	89 04 24             	mov    %eax,(%esp)
f0102a7d:	e8 7c 1d 00 00       	call   f01047fe <memset>
	// GD_UD is the user data segment selector in the GDT, and 
	// GD_UT is the user text segment selector (see inc/memlayout.h).
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.
	
	e->env_tf.tf_ds = GD_UD | 3;
f0102a82:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a85:	66 c7 40 24 23 00    	movw   $0x23,0x24(%eax)
	e->env_tf.tf_es = GD_UD | 3;
f0102a8b:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a8e:	66 c7 40 20 23 00    	movw   $0x23,0x20(%eax)
	e->env_tf.tf_ss = GD_UD | 3;
f0102a94:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a97:	66 c7 40 40 23 00    	movw   $0x23,0x40(%eax)
	e->env_tf.tf_esp = (uint32*)USTACKTOP;
f0102a9d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102aa0:	c7 40 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%eax)
	e->env_tf.tf_cs = GD_UT | 3;
f0102aa7:	8b 45 08             	mov    0x8(%ebp),%eax
f0102aaa:	66 c7 40 34 1b 00    	movw   $0x1b,0x34(%eax)
	// You will set e->env_tf.tf_eip later.

	// commit the allocation
	LIST_REMOVE(e);	
f0102ab0:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ab3:	8b 40 44             	mov    0x44(%eax),%eax
f0102ab6:	85 c0                	test   %eax,%eax
f0102ab8:	74 0f                	je     f0102ac9 <complete_environment_initialization+0xf4>
f0102aba:	8b 45 08             	mov    0x8(%ebp),%eax
f0102abd:	8b 40 44             	mov    0x44(%eax),%eax
f0102ac0:	8b 55 08             	mov    0x8(%ebp),%edx
f0102ac3:	8b 52 48             	mov    0x48(%edx),%edx
f0102ac6:	89 50 48             	mov    %edx,0x48(%eax)
f0102ac9:	8b 45 08             	mov    0x8(%ebp),%eax
f0102acc:	8b 40 48             	mov    0x48(%eax),%eax
f0102acf:	8b 55 08             	mov    0x8(%ebp),%edx
f0102ad2:	8b 52 44             	mov    0x44(%edx),%edx
f0102ad5:	89 10                	mov    %edx,(%eax)
	return ;
f0102ad7:	90                   	nop
}
f0102ad8:	c9                   	leave  
f0102ad9:	c3                   	ret    

f0102ada <PROGRAM_SEGMENT_NEXT>:

struct ProgramSegment* PROGRAM_SEGMENT_NEXT(struct ProgramSegment* seg, uint8* ptr_program_start)
{
f0102ada:	55                   	push   %ebp
f0102adb:	89 e5                	mov    %esp,%ebp
f0102add:	83 ec 28             	sub    $0x28,%esp
	int index = (*seg).segment_id++;
f0102ae0:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ae3:	8b 40 10             	mov    0x10(%eax),%eax
f0102ae6:	8d 48 01             	lea    0x1(%eax),%ecx
f0102ae9:	8b 55 08             	mov    0x8(%ebp),%edx
f0102aec:	89 4a 10             	mov    %ecx,0x10(%edx)
f0102aef:	89 45 f4             	mov    %eax,-0xc(%ebp)

	struct Proghdr *ph, *eph; 
	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102af2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102af5:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102af8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102afb:	8b 00                	mov    (%eax),%eax
f0102afd:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102b02:	74 1c                	je     f0102b20 <PROGRAM_SEGMENT_NEXT+0x46>
		panic("Matafa2nash 3ala Keda"); 
f0102b04:	c7 44 24 08 0b 5b 10 	movl   $0xf0105b0b,0x8(%esp)
f0102b0b:	f0 
f0102b0c:	c7 44 24 04 48 01 00 	movl   $0x148,0x4(%esp)
f0102b13:	00 
f0102b14:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102b1b:	e8 fa d5 ff ff       	call   f010011a <_panic>
	ph = (struct Proghdr *) ( ((uint8 *) ptr_program_start) + pELFHDR->e_phoff);
f0102b20:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102b23:	8b 50 1c             	mov    0x1c(%eax),%edx
f0102b26:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102b29:	01 d0                	add    %edx,%eax
f0102b2b:	89 45 ec             	mov    %eax,-0x14(%ebp)
	
	while (ph[(*seg).segment_id].p_type != ELF_PROG_LOAD && ((*seg).segment_id < pELFHDR->e_phnum)) (*seg).segment_id++;	
f0102b2e:	eb 0f                	jmp    f0102b3f <PROGRAM_SEGMENT_NEXT+0x65>
f0102b30:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b33:	8b 40 10             	mov    0x10(%eax),%eax
f0102b36:	8d 50 01             	lea    0x1(%eax),%edx
f0102b39:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b3c:	89 50 10             	mov    %edx,0x10(%eax)
f0102b3f:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b42:	8b 40 10             	mov    0x10(%eax),%eax
f0102b45:	c1 e0 05             	shl    $0x5,%eax
f0102b48:	89 c2                	mov    %eax,%edx
f0102b4a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102b4d:	01 d0                	add    %edx,%eax
f0102b4f:	8b 00                	mov    (%eax),%eax
f0102b51:	83 f8 01             	cmp    $0x1,%eax
f0102b54:	74 14                	je     f0102b6a <PROGRAM_SEGMENT_NEXT+0x90>
f0102b56:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b59:	8b 50 10             	mov    0x10(%eax),%edx
f0102b5c:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102b5f:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102b63:	0f b7 c0             	movzwl %ax,%eax
f0102b66:	39 c2                	cmp    %eax,%edx
f0102b68:	72 c6                	jb     f0102b30 <PROGRAM_SEGMENT_NEXT+0x56>
	index = (*seg).segment_id;
f0102b6a:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b6d:	8b 40 10             	mov    0x10(%eax),%eax
f0102b70:	89 45 f4             	mov    %eax,-0xc(%ebp)

	if(index < pELFHDR->e_phnum)
f0102b73:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102b76:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102b7a:	0f b7 c0             	movzwl %ax,%eax
f0102b7d:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f0102b80:	7e 63                	jle    f0102be5 <PROGRAM_SEGMENT_NEXT+0x10b>
	{
		(*seg).ptr_start = (uint8 *) ptr_program_start + ph[index].p_offset;
f0102b82:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102b85:	c1 e0 05             	shl    $0x5,%eax
f0102b88:	89 c2                	mov    %eax,%edx
f0102b8a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102b8d:	01 d0                	add    %edx,%eax
f0102b8f:	8b 50 04             	mov    0x4(%eax),%edx
f0102b92:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102b95:	01 c2                	add    %eax,%edx
f0102b97:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b9a:	89 10                	mov    %edx,(%eax)
		(*seg).size_in_memory =  ph[index].p_memsz;
f0102b9c:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102b9f:	c1 e0 05             	shl    $0x5,%eax
f0102ba2:	89 c2                	mov    %eax,%edx
f0102ba4:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102ba7:	01 d0                	add    %edx,%eax
f0102ba9:	8b 50 14             	mov    0x14(%eax),%edx
f0102bac:	8b 45 08             	mov    0x8(%ebp),%eax
f0102baf:	89 50 08             	mov    %edx,0x8(%eax)
		(*seg).size_in_file = ph[index].p_filesz;
f0102bb2:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102bb5:	c1 e0 05             	shl    $0x5,%eax
f0102bb8:	89 c2                	mov    %eax,%edx
f0102bba:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102bbd:	01 d0                	add    %edx,%eax
f0102bbf:	8b 50 10             	mov    0x10(%eax),%edx
f0102bc2:	8b 45 08             	mov    0x8(%ebp),%eax
f0102bc5:	89 50 04             	mov    %edx,0x4(%eax)
		(*seg).virtual_address = (uint8*)ph[index].p_va;
f0102bc8:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102bcb:	c1 e0 05             	shl    $0x5,%eax
f0102bce:	89 c2                	mov    %eax,%edx
f0102bd0:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102bd3:	01 d0                	add    %edx,%eax
f0102bd5:	8b 40 08             	mov    0x8(%eax),%eax
f0102bd8:	89 c2                	mov    %eax,%edx
f0102bda:	8b 45 08             	mov    0x8(%ebp),%eax
f0102bdd:	89 50 0c             	mov    %edx,0xc(%eax)
		return seg;
f0102be0:	8b 45 08             	mov    0x8(%ebp),%eax
f0102be3:	eb 05                	jmp    f0102bea <PROGRAM_SEGMENT_NEXT+0x110>
	}
	return 0;
f0102be5:	b8 00 00 00 00       	mov    $0x0,%eax
}	
f0102bea:	c9                   	leave  
f0102beb:	c3                   	ret    

f0102bec <PROGRAM_SEGMENT_FIRST>:

struct ProgramSegment PROGRAM_SEGMENT_FIRST( uint8* ptr_program_start)
{
f0102bec:	55                   	push   %ebp
f0102bed:	89 e5                	mov    %esp,%ebp
f0102bef:	83 ec 38             	sub    $0x38,%esp
	struct ProgramSegment seg;
	seg.segment_id = 0;
f0102bf2:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	struct Proghdr *ph, *eph; 
	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102bf9:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102bfc:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102bff:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c02:	8b 00                	mov    (%eax),%eax
f0102c04:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102c09:	74 1c                	je     f0102c27 <PROGRAM_SEGMENT_FIRST+0x3b>
		panic("Matafa2nash 3ala Keda"); 
f0102c0b:	c7 44 24 08 0b 5b 10 	movl   $0xf0105b0b,0x8(%esp)
f0102c12:	f0 
f0102c13:	c7 44 24 04 61 01 00 	movl   $0x161,0x4(%esp)
f0102c1a:	00 
f0102c1b:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102c22:	e8 f3 d4 ff ff       	call   f010011a <_panic>
	ph = (struct Proghdr *) ( ((uint8 *) ptr_program_start) + pELFHDR->e_phoff);
f0102c27:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c2a:	8b 50 1c             	mov    0x1c(%eax),%edx
f0102c2d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c30:	01 d0                	add    %edx,%eax
f0102c32:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (ph[(seg).segment_id].p_type != ELF_PROG_LOAD && ((seg).segment_id < pELFHDR->e_phnum)) (seg).segment_id++;
f0102c35:	eb 09                	jmp    f0102c40 <PROGRAM_SEGMENT_FIRST+0x54>
f0102c37:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102c3a:	83 c0 01             	add    $0x1,%eax
f0102c3d:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0102c40:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102c43:	c1 e0 05             	shl    $0x5,%eax
f0102c46:	89 c2                	mov    %eax,%edx
f0102c48:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102c4b:	01 d0                	add    %edx,%eax
f0102c4d:	8b 00                	mov    (%eax),%eax
f0102c4f:	83 f8 01             	cmp    $0x1,%eax
f0102c52:	74 11                	je     f0102c65 <PROGRAM_SEGMENT_FIRST+0x79>
f0102c54:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c5a:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102c5e:	0f b7 c0             	movzwl %ax,%eax
f0102c61:	39 c2                	cmp    %eax,%edx
f0102c63:	72 d2                	jb     f0102c37 <PROGRAM_SEGMENT_FIRST+0x4b>
	int index = (seg).segment_id;
f0102c65:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102c68:	89 45 ec             	mov    %eax,-0x14(%ebp)

	if(index < pELFHDR->e_phnum)
f0102c6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c6e:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102c72:	0f b7 c0             	movzwl %ax,%eax
f0102c75:	3b 45 ec             	cmp    -0x14(%ebp),%eax
f0102c78:	7e 73                	jle    f0102ced <PROGRAM_SEGMENT_FIRST+0x101>
	{	
		(seg).ptr_start = (uint8 *) ptr_program_start + ph[index].p_offset;
f0102c7a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102c7d:	c1 e0 05             	shl    $0x5,%eax
f0102c80:	89 c2                	mov    %eax,%edx
f0102c82:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102c85:	01 d0                	add    %edx,%eax
f0102c87:	8b 50 04             	mov    0x4(%eax),%edx
f0102c8a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c8d:	01 d0                	add    %edx,%eax
f0102c8f:	89 45 d8             	mov    %eax,-0x28(%ebp)
		(seg).size_in_memory =  ph[index].p_memsz;
f0102c92:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102c95:	c1 e0 05             	shl    $0x5,%eax
f0102c98:	89 c2                	mov    %eax,%edx
f0102c9a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102c9d:	01 d0                	add    %edx,%eax
f0102c9f:	8b 40 14             	mov    0x14(%eax),%eax
f0102ca2:	89 45 e0             	mov    %eax,-0x20(%ebp)
		(seg).size_in_file = ph[index].p_filesz;
f0102ca5:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102ca8:	c1 e0 05             	shl    $0x5,%eax
f0102cab:	89 c2                	mov    %eax,%edx
f0102cad:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102cb0:	01 d0                	add    %edx,%eax
f0102cb2:	8b 40 10             	mov    0x10(%eax),%eax
f0102cb5:	89 45 dc             	mov    %eax,-0x24(%ebp)
		(seg).virtual_address = (uint8*)ph[index].p_va;
f0102cb8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102cbb:	c1 e0 05             	shl    $0x5,%eax
f0102cbe:	89 c2                	mov    %eax,%edx
f0102cc0:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102cc3:	01 d0                	add    %edx,%eax
f0102cc5:	8b 40 08             	mov    0x8(%eax),%eax
f0102cc8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		return seg;
f0102ccb:	8b 45 08             	mov    0x8(%ebp),%eax
f0102cce:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0102cd1:	89 10                	mov    %edx,(%eax)
f0102cd3:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0102cd6:	89 50 04             	mov    %edx,0x4(%eax)
f0102cd9:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0102cdc:	89 50 08             	mov    %edx,0x8(%eax)
f0102cdf:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0102ce2:	89 50 0c             	mov    %edx,0xc(%eax)
f0102ce5:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102ce8:	89 50 10             	mov    %edx,0x10(%eax)
f0102ceb:	eb 27                	jmp    f0102d14 <PROGRAM_SEGMENT_FIRST+0x128>
	}
	seg.segment_id = -1;
f0102ced:	c7 45 e8 ff ff ff ff 	movl   $0xffffffff,-0x18(%ebp)
	return seg;
f0102cf4:	8b 45 08             	mov    0x8(%ebp),%eax
f0102cf7:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0102cfa:	89 10                	mov    %edx,(%eax)
f0102cfc:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0102cff:	89 50 04             	mov    %edx,0x4(%eax)
f0102d02:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0102d05:	89 50 08             	mov    %edx,0x8(%eax)
f0102d08:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0102d0b:	89 50 0c             	mov    %edx,0xc(%eax)
f0102d0e:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102d11:	89 50 10             	mov    %edx,0x10(%eax)
}
f0102d14:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d17:	c9                   	leave  
f0102d18:	c2 04 00             	ret    $0x4

f0102d1b <get_user_program_info>:

struct UserProgramInfo* get_user_program_info(char* user_program_name)
{
f0102d1b:	55                   	push   %ebp
f0102d1c:	89 e5                	mov    %esp,%ebp
f0102d1e:	83 ec 28             	sub    $0x28,%esp
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102d21:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0102d28:	eb 26                	jmp    f0102d50 <get_user_program_info+0x35>
		if (strcmp(user_program_name, userPrograms[i].name) == 0)
f0102d2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102d2d:	c1 e0 04             	shl    $0x4,%eax
f0102d30:	05 a0 a5 11 f0       	add    $0xf011a5a0,%eax
f0102d35:	8b 00                	mov    (%eax),%eax
f0102d37:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102d3b:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d3e:	89 04 24             	mov    %eax,(%esp)
f0102d41:	e8 c2 19 00 00       	call   f0104708 <strcmp>
f0102d46:	85 c0                	test   %eax,%eax
f0102d48:	75 02                	jne    f0102d4c <get_user_program_info+0x31>
			break;
f0102d4a:	eb 0e                	jmp    f0102d5a <get_user_program_info+0x3f>
}

struct UserProgramInfo* get_user_program_info(char* user_program_name)
{
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102d4c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0102d50:	a1 f4 a5 11 f0       	mov    0xf011a5f4,%eax
f0102d55:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102d58:	7c d0                	jl     f0102d2a <get_user_program_info+0xf>
		if (strcmp(user_program_name, userPrograms[i].name) == 0)
			break;
	}
	if(i==NUM_USER_PROGS) 
f0102d5a:	a1 f4 a5 11 f0       	mov    0xf011a5f4,%eax
f0102d5f:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102d62:	75 1a                	jne    f0102d7e <get_user_program_info+0x63>
	{
		cprintf("Unknown user program '%s'\n", user_program_name);
f0102d64:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d67:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102d6b:	c7 04 24 21 5b 10 f0 	movl   $0xf0105b21,(%esp)
f0102d72:	e8 94 02 00 00       	call   f010300b <cprintf>
		return 0;
f0102d77:	b8 00 00 00 00       	mov    $0x0,%eax
f0102d7c:	eb 0b                	jmp    f0102d89 <get_user_program_info+0x6e>
	}

	return &userPrograms[i];
f0102d7e:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102d81:	c1 e0 04             	shl    $0x4,%eax
f0102d84:	05 a0 a5 11 f0       	add    $0xf011a5a0,%eax
}
f0102d89:	c9                   	leave  
f0102d8a:	c3                   	ret    

f0102d8b <get_user_program_info_by_env>:

struct UserProgramInfo* get_user_program_info_by_env(struct Env* e)
{
f0102d8b:	55                   	push   %ebp
f0102d8c:	89 e5                	mov    %esp,%ebp
f0102d8e:	83 ec 28             	sub    $0x28,%esp
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102d91:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0102d98:	eb 19                	jmp    f0102db3 <get_user_program_info_by_env+0x28>
		if (e== userPrograms[i].environment)
f0102d9a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102d9d:	c1 e0 04             	shl    $0x4,%eax
f0102da0:	05 a0 a5 11 f0       	add    $0xf011a5a0,%eax
f0102da5:	8b 40 0c             	mov    0xc(%eax),%eax
f0102da8:	3b 45 08             	cmp    0x8(%ebp),%eax
f0102dab:	75 02                	jne    f0102daf <get_user_program_info_by_env+0x24>
			break;
f0102dad:	eb 0e                	jmp    f0102dbd <get_user_program_info_by_env+0x32>
}

struct UserProgramInfo* get_user_program_info_by_env(struct Env* e)
{
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102daf:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0102db3:	a1 f4 a5 11 f0       	mov    0xf011a5f4,%eax
f0102db8:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102dbb:	7c dd                	jl     f0102d9a <get_user_program_info_by_env+0xf>
		if (e== userPrograms[i].environment)
			break;
	}
	if(i==NUM_USER_PROGS) 
f0102dbd:	a1 f4 a5 11 f0       	mov    0xf011a5f4,%eax
f0102dc2:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102dc5:	75 13                	jne    f0102dda <get_user_program_info_by_env+0x4f>
	{
		cprintf("Unknown user program \n");
f0102dc7:	c7 04 24 3c 5b 10 f0 	movl   $0xf0105b3c,(%esp)
f0102dce:	e8 38 02 00 00       	call   f010300b <cprintf>
		return 0;
f0102dd3:	b8 00 00 00 00       	mov    $0x0,%eax
f0102dd8:	eb 0b                	jmp    f0102de5 <get_user_program_info_by_env+0x5a>
	}

	return &userPrograms[i];
f0102dda:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102ddd:	c1 e0 04             	shl    $0x4,%eax
f0102de0:	05 a0 a5 11 f0       	add    $0xf011a5a0,%eax
}
f0102de5:	c9                   	leave  
f0102de6:	c3                   	ret    

f0102de7 <set_environment_entry_point>:

void set_environment_entry_point(struct UserProgramInfo* ptr_user_program)
{
f0102de7:	55                   	push   %ebp
f0102de8:	89 e5                	mov    %esp,%ebp
f0102dea:	83 ec 28             	sub    $0x28,%esp
	uint8* ptr_program_start=ptr_user_program->ptr_start;
f0102ded:	8b 45 08             	mov    0x8(%ebp),%eax
f0102df0:	8b 40 08             	mov    0x8(%eax),%eax
f0102df3:	89 45 f4             	mov    %eax,-0xc(%ebp)
	struct Env* e = ptr_user_program->environment;
f0102df6:	8b 45 08             	mov    0x8(%ebp),%eax
f0102df9:	8b 40 0c             	mov    0xc(%eax),%eax
f0102dfc:	89 45 f0             	mov    %eax,-0x10(%ebp)

	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102dff:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102e02:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102e05:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102e08:	8b 00                	mov    (%eax),%eax
f0102e0a:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102e0f:	74 1c                	je     f0102e2d <set_environment_entry_point+0x46>
		panic("Matafa2nash 3ala Keda"); 
f0102e11:	c7 44 24 08 0b 5b 10 	movl   $0xf0105b0b,0x8(%esp)
f0102e18:	f0 
f0102e19:	c7 44 24 04 99 01 00 	movl   $0x199,0x4(%esp)
f0102e20:	00 
f0102e21:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102e28:	e8 ed d2 ff ff       	call   f010011a <_panic>
	e->env_tf.tf_eip = (uint32*)pELFHDR->e_entry ;
f0102e2d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102e30:	8b 40 18             	mov    0x18(%eax),%eax
f0102e33:	89 c2                	mov    %eax,%edx
f0102e35:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102e38:	89 50 30             	mov    %edx,0x30(%eax)
}
f0102e3b:	c9                   	leave  
f0102e3c:	c3                   	ret    

f0102e3d <env_destroy>:
// If e was the current env, then runs a new environment (and does not return
// to the caller).
//
void
env_destroy(struct Env *e) 
{
f0102e3d:	55                   	push   %ebp
f0102e3e:	89 e5                	mov    %esp,%ebp
f0102e40:	83 ec 18             	sub    $0x18,%esp
	env_free(e);
f0102e43:	8b 45 08             	mov    0x8(%ebp),%eax
f0102e46:	89 04 24             	mov    %eax,(%esp)
f0102e49:	e8 ba fa ff ff       	call   f0102908 <env_free>

	//cprintf("Destroyed the only environment - nothing more to do!\n");
	while (1)
		run_command_prompt();
f0102e4e:	e8 0a db ff ff       	call   f010095d <run_command_prompt>
f0102e53:	eb f9                	jmp    f0102e4e <env_destroy+0x11>

f0102e55 <env_run_cmd_prmpt>:
}

void env_run_cmd_prmpt()
{
f0102e55:	55                   	push   %ebp
f0102e56:	89 e5                	mov    %esp,%ebp
f0102e58:	83 ec 28             	sub    $0x28,%esp
	struct UserProgramInfo* upi= get_user_program_info_by_env(curenv);	
f0102e5b:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102e60:	89 04 24             	mov    %eax,(%esp)
f0102e63:	e8 23 ff ff ff       	call   f0102d8b <get_user_program_info_by_env>
f0102e68:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&curenv->env_tf, 0, sizeof(curenv->env_tf));
f0102e6b:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102e70:	c7 44 24 08 44 00 00 	movl   $0x44,0x8(%esp)
f0102e77:	00 
f0102e78:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f0102e7f:	00 
f0102e80:	89 04 24             	mov    %eax,(%esp)
f0102e83:	e8 76 19 00 00       	call   f01047fe <memset>
	// GD_UD is the user data segment selector in the GDT, and 
	// GD_UT is the user text segment selector (see inc/memlayout.h).
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.
	
	curenv->env_tf.tf_ds = GD_UD | 3;
f0102e88:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102e8d:	66 c7 40 24 23 00    	movw   $0x23,0x24(%eax)
	curenv->env_tf.tf_es = GD_UD | 3;
f0102e93:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102e98:	66 c7 40 20 23 00    	movw   $0x23,0x20(%eax)
	curenv->env_tf.tf_ss = GD_UD | 3;
f0102e9e:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102ea3:	66 c7 40 40 23 00    	movw   $0x23,0x40(%eax)
	curenv->env_tf.tf_esp = (uint32*)USTACKTOP;
f0102ea9:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102eae:	c7 40 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%eax)
	curenv->env_tf.tf_cs = GD_UT | 3;
f0102eb5:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0102eba:	66 c7 40 34 1b 00    	movw   $0x1b,0x34(%eax)
	set_environment_entry_point(upi);
f0102ec0:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102ec3:	89 04 24             	mov    %eax,(%esp)
f0102ec6:	e8 1c ff ff ff       	call   f0102de7 <set_environment_entry_point>
	
	lcr3(K_PHYSICAL_ADDRESS(ptr_page_directory));
f0102ecb:	a1 a4 63 14 f0       	mov    0xf01463a4,%eax
f0102ed0:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0102ed3:	81 7d f0 ff ff ff ef 	cmpl   $0xefffffff,-0x10(%ebp)
f0102eda:	77 23                	ja     f0102eff <env_run_cmd_prmpt+0xaa>
f0102edc:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102edf:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0102ee3:	c7 44 24 08 54 5b 10 	movl   $0xf0105b54,0x8(%esp)
f0102eea:	f0 
f0102eeb:	c7 44 24 04 c4 01 00 	movl   $0x1c4,0x4(%esp)
f0102ef2:	00 
f0102ef3:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102efa:	e8 1b d2 ff ff       	call   f010011a <_panic>
f0102eff:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102f02:	05 00 00 00 10       	add    $0x10000000,%eax
f0102f07:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102f0a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102f0d:	0f 22 d8             	mov    %eax,%cr3
	
	curenv = NULL;
f0102f10:	c7 05 10 5b 14 f0 00 	movl   $0x0,0xf0145b10
f0102f17:	00 00 00 
	
	while (1)
		run_command_prompt();
f0102f1a:	e8 3e da ff ff       	call   f010095d <run_command_prompt>
f0102f1f:	eb f9                	jmp    f0102f1a <env_run_cmd_prmpt+0xc5>

f0102f21 <env_pop_tf>:
// This exits the kernel and starts executing some environment's code.
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f0102f21:	55                   	push   %ebp
f0102f22:	89 e5                	mov    %esp,%ebp
f0102f24:	83 ec 18             	sub    $0x18,%esp
	__asm __volatile("movl %0,%%esp\n"
f0102f27:	8b 65 08             	mov    0x8(%ebp),%esp
f0102f2a:	61                   	popa   
f0102f2b:	07                   	pop    %es
f0102f2c:	1f                   	pop    %ds
f0102f2d:	83 c4 08             	add    $0x8,%esp
f0102f30:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f0102f31:	c7 44 24 08 85 5b 10 	movl   $0xf0105b85,0x8(%esp)
f0102f38:	f0 
f0102f39:	c7 44 24 04 db 01 00 	movl   $0x1db,0x4(%esp)
f0102f40:	00 
f0102f41:	c7 04 24 65 5a 10 f0 	movl   $0xf0105a65,(%esp)
f0102f48:	e8 cd d1 ff ff       	call   f010011a <_panic>

f0102f4d <mc146818_read>:
#include <kern/kclock.h>


unsigned
mc146818_read(unsigned reg)
{
f0102f4d:	55                   	push   %ebp
f0102f4e:	89 e5                	mov    %esp,%ebp
f0102f50:	83 ec 10             	sub    $0x10,%esp
	outb(IO_RTC, reg);
f0102f53:	8b 45 08             	mov    0x8(%ebp),%eax
f0102f56:	0f b6 c0             	movzbl %al,%eax
f0102f59:	c7 45 fc 70 00 00 00 	movl   $0x70,-0x4(%ebp)
f0102f60:	88 45 fb             	mov    %al,-0x5(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0102f63:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
f0102f67:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102f6a:	ee                   	out    %al,(%dx)
f0102f6b:	c7 45 f4 71 00 00 00 	movl   $0x71,-0xc(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0102f72:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102f75:	89 c2                	mov    %eax,%edx
f0102f77:	ec                   	in     (%dx),%al
f0102f78:	88 45 f3             	mov    %al,-0xd(%ebp)
	return data;
f0102f7b:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
	return inb(IO_RTC+1);
f0102f7f:	0f b6 c0             	movzbl %al,%eax
}
f0102f82:	c9                   	leave  
f0102f83:	c3                   	ret    

f0102f84 <mc146818_write>:

void
mc146818_write(unsigned reg, unsigned datum)
{
f0102f84:	55                   	push   %ebp
f0102f85:	89 e5                	mov    %esp,%ebp
f0102f87:	83 ec 10             	sub    $0x10,%esp
	outb(IO_RTC, reg);
f0102f8a:	8b 45 08             	mov    0x8(%ebp),%eax
f0102f8d:	0f b6 c0             	movzbl %al,%eax
f0102f90:	c7 45 fc 70 00 00 00 	movl   $0x70,-0x4(%ebp)
f0102f97:	88 45 fb             	mov    %al,-0x5(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0102f9a:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
f0102f9e:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102fa1:	ee                   	out    %al,(%dx)
	outb(IO_RTC+1, datum);
f0102fa2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102fa5:	0f b6 c0             	movzbl %al,%eax
f0102fa8:	c7 45 f4 71 00 00 00 	movl   $0x71,-0xc(%ebp)
f0102faf:	88 45 f3             	mov    %al,-0xd(%ebp)
f0102fb2:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0102fb6:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102fb9:	ee                   	out    %al,(%dx)
}
f0102fba:	c9                   	leave  
f0102fbb:	c3                   	ret    

f0102fbc <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0102fbc:	55                   	push   %ebp
f0102fbd:	89 e5                	mov    %esp,%ebp
f0102fbf:	83 ec 18             	sub    $0x18,%esp
	cputchar(ch);
f0102fc2:	8b 45 08             	mov    0x8(%ebp),%eax
f0102fc5:	89 04 24             	mov    %eax,(%esp)
f0102fc8:	e8 5a d9 ff ff       	call   f0100927 <cputchar>
	*cnt++;
f0102fcd:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102fd0:	83 c0 04             	add    $0x4,%eax
f0102fd3:	89 45 0c             	mov    %eax,0xc(%ebp)
}
f0102fd6:	c9                   	leave  
f0102fd7:	c3                   	ret    

f0102fd8 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f0102fd8:	55                   	push   %ebp
f0102fd9:	89 e5                	mov    %esp,%ebp
f0102fdb:	83 ec 28             	sub    $0x28,%esp
	int cnt = 0;
f0102fde:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0102fe5:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102fe8:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0102fec:	8b 45 08             	mov    0x8(%ebp),%eax
f0102fef:	89 44 24 08          	mov    %eax,0x8(%esp)
f0102ff3:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0102ff6:	89 44 24 04          	mov    %eax,0x4(%esp)
f0102ffa:	c7 04 24 bc 2f 10 f0 	movl   $0xf0102fbc,(%esp)
f0103001:	e8 d7 0f 00 00       	call   f0103fdd <vprintfmt>
	return cnt;
f0103006:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f0103009:	c9                   	leave  
f010300a:	c3                   	ret    

f010300b <cprintf>:

int
cprintf(const char *fmt, ...)
{
f010300b:	55                   	push   %ebp
f010300c:	89 e5                	mov    %esp,%ebp
f010300e:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0103011:	8d 45 0c             	lea    0xc(%ebp),%eax
f0103014:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
f0103017:	8b 45 08             	mov    0x8(%ebp),%eax
f010301a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010301d:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103021:	89 04 24             	mov    %eax,(%esp)
f0103024:	e8 af ff ff ff       	call   f0102fd8 <vcprintf>
f0103029:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
f010302c:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
f010302f:	c9                   	leave  
f0103030:	c3                   	ret    

f0103031 <trapname>:
};
extern  void (*PAGE_FAULT)();
extern  void (*SYSCALL_HANDLER)();

static const char *trapname(int trapno)
{
f0103031:	55                   	push   %ebp
f0103032:	89 e5                	mov    %esp,%ebp
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < sizeof(excnames)/sizeof(excnames[0]))
f0103034:	8b 45 08             	mov    0x8(%ebp),%eax
f0103037:	83 f8 13             	cmp    $0x13,%eax
f010303a:	77 0c                	ja     f0103048 <trapname+0x17>
		return excnames[trapno];
f010303c:	8b 45 08             	mov    0x8(%ebp),%eax
f010303f:	8b 04 85 c0 5e 10 f0 	mov    -0xfefa140(,%eax,4),%eax
f0103046:	eb 12                	jmp    f010305a <trapname+0x29>
	if (trapno == T_SYSCALL)
f0103048:	83 7d 08 30          	cmpl   $0x30,0x8(%ebp)
f010304c:	75 07                	jne    f0103055 <trapname+0x24>
		return "System call";
f010304e:	b8 a0 5b 10 f0       	mov    $0xf0105ba0,%eax
f0103053:	eb 05                	jmp    f010305a <trapname+0x29>
	return "(unknown trap)";
f0103055:	b8 ac 5b 10 f0       	mov    $0xf0105bac,%eax
}
f010305a:	5d                   	pop    %ebp
f010305b:	c3                   	ret    

f010305c <idt_init>:


void
idt_init(void)
{
f010305c:	55                   	push   %ebp
f010305d:	89 e5                	mov    %esp,%ebp
f010305f:	83 ec 10             	sub    $0x10,%esp
	extern struct Segdesc gdt[];
	
	// LAB 3: Your code here.
	//initialize idt
	SETGATE(idt[T_PGFLT], 0, GD_KT , &PAGE_FAULT, 0) ;
f0103062:	b8 ec 35 10 f0       	mov    $0xf01035ec,%eax
f0103067:	66 a3 90 5b 14 f0    	mov    %ax,0xf0145b90
f010306d:	66 c7 05 92 5b 14 f0 	movw   $0x8,0xf0145b92
f0103074:	08 00 
f0103076:	0f b6 05 94 5b 14 f0 	movzbl 0xf0145b94,%eax
f010307d:	83 e0 e0             	and    $0xffffffe0,%eax
f0103080:	a2 94 5b 14 f0       	mov    %al,0xf0145b94
f0103085:	0f b6 05 94 5b 14 f0 	movzbl 0xf0145b94,%eax
f010308c:	83 e0 1f             	and    $0x1f,%eax
f010308f:	a2 94 5b 14 f0       	mov    %al,0xf0145b94
f0103094:	0f b6 05 95 5b 14 f0 	movzbl 0xf0145b95,%eax
f010309b:	83 e0 f0             	and    $0xfffffff0,%eax
f010309e:	83 c8 0e             	or     $0xe,%eax
f01030a1:	a2 95 5b 14 f0       	mov    %al,0xf0145b95
f01030a6:	0f b6 05 95 5b 14 f0 	movzbl 0xf0145b95,%eax
f01030ad:	83 e0 ef             	and    $0xffffffef,%eax
f01030b0:	a2 95 5b 14 f0       	mov    %al,0xf0145b95
f01030b5:	0f b6 05 95 5b 14 f0 	movzbl 0xf0145b95,%eax
f01030bc:	83 e0 9f             	and    $0xffffff9f,%eax
f01030bf:	a2 95 5b 14 f0       	mov    %al,0xf0145b95
f01030c4:	0f b6 05 95 5b 14 f0 	movzbl 0xf0145b95,%eax
f01030cb:	83 c8 80             	or     $0xffffff80,%eax
f01030ce:	a2 95 5b 14 f0       	mov    %al,0xf0145b95
f01030d3:	b8 ec 35 10 f0       	mov    $0xf01035ec,%eax
f01030d8:	c1 e8 10             	shr    $0x10,%eax
f01030db:	66 a3 96 5b 14 f0    	mov    %ax,0xf0145b96
	SETGATE(idt[T_SYSCALL], 0, GD_KT , &SYSCALL_HANDLER, 3) ;
f01030e1:	b8 f0 35 10 f0       	mov    $0xf01035f0,%eax
f01030e6:	66 a3 a0 5c 14 f0    	mov    %ax,0xf0145ca0
f01030ec:	66 c7 05 a2 5c 14 f0 	movw   $0x8,0xf0145ca2
f01030f3:	08 00 
f01030f5:	0f b6 05 a4 5c 14 f0 	movzbl 0xf0145ca4,%eax
f01030fc:	83 e0 e0             	and    $0xffffffe0,%eax
f01030ff:	a2 a4 5c 14 f0       	mov    %al,0xf0145ca4
f0103104:	0f b6 05 a4 5c 14 f0 	movzbl 0xf0145ca4,%eax
f010310b:	83 e0 1f             	and    $0x1f,%eax
f010310e:	a2 a4 5c 14 f0       	mov    %al,0xf0145ca4
f0103113:	0f b6 05 a5 5c 14 f0 	movzbl 0xf0145ca5,%eax
f010311a:	83 e0 f0             	and    $0xfffffff0,%eax
f010311d:	83 c8 0e             	or     $0xe,%eax
f0103120:	a2 a5 5c 14 f0       	mov    %al,0xf0145ca5
f0103125:	0f b6 05 a5 5c 14 f0 	movzbl 0xf0145ca5,%eax
f010312c:	83 e0 ef             	and    $0xffffffef,%eax
f010312f:	a2 a5 5c 14 f0       	mov    %al,0xf0145ca5
f0103134:	0f b6 05 a5 5c 14 f0 	movzbl 0xf0145ca5,%eax
f010313b:	83 c8 60             	or     $0x60,%eax
f010313e:	a2 a5 5c 14 f0       	mov    %al,0xf0145ca5
f0103143:	0f b6 05 a5 5c 14 f0 	movzbl 0xf0145ca5,%eax
f010314a:	83 c8 80             	or     $0xffffff80,%eax
f010314d:	a2 a5 5c 14 f0       	mov    %al,0xf0145ca5
f0103152:	b8 f0 35 10 f0       	mov    $0xf01035f0,%eax
f0103157:	c1 e8 10             	shr    $0x10,%eax
f010315a:	66 a3 a6 5c 14 f0    	mov    %ax,0xf0145ca6

	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	ts.ts_esp0 = KERNEL_STACK_TOP;
f0103160:	c7 05 24 63 14 f0 00 	movl   $0xefc00000,0xf0146324
f0103167:	00 c0 ef 
	ts.ts_ss0 = GD_KD;
f010316a:	66 c7 05 28 63 14 f0 	movw   $0x10,0xf0146328
f0103171:	10 00 

	// Initialize the TSS field of the gdt.
	gdt[GD_TSS >> 3] = SEG16(STS_T32A, (uint32) (&ts),
f0103173:	66 c7 05 88 a5 11 f0 	movw   $0x68,0xf011a588
f010317a:	68 00 
f010317c:	b8 20 63 14 f0       	mov    $0xf0146320,%eax
f0103181:	66 a3 8a a5 11 f0    	mov    %ax,0xf011a58a
f0103187:	b8 20 63 14 f0       	mov    $0xf0146320,%eax
f010318c:	c1 e8 10             	shr    $0x10,%eax
f010318f:	a2 8c a5 11 f0       	mov    %al,0xf011a58c
f0103194:	0f b6 05 8d a5 11 f0 	movzbl 0xf011a58d,%eax
f010319b:	83 e0 f0             	and    $0xfffffff0,%eax
f010319e:	83 c8 09             	or     $0x9,%eax
f01031a1:	a2 8d a5 11 f0       	mov    %al,0xf011a58d
f01031a6:	0f b6 05 8d a5 11 f0 	movzbl 0xf011a58d,%eax
f01031ad:	83 c8 10             	or     $0x10,%eax
f01031b0:	a2 8d a5 11 f0       	mov    %al,0xf011a58d
f01031b5:	0f b6 05 8d a5 11 f0 	movzbl 0xf011a58d,%eax
f01031bc:	83 e0 9f             	and    $0xffffff9f,%eax
f01031bf:	a2 8d a5 11 f0       	mov    %al,0xf011a58d
f01031c4:	0f b6 05 8d a5 11 f0 	movzbl 0xf011a58d,%eax
f01031cb:	83 c8 80             	or     $0xffffff80,%eax
f01031ce:	a2 8d a5 11 f0       	mov    %al,0xf011a58d
f01031d3:	0f b6 05 8e a5 11 f0 	movzbl 0xf011a58e,%eax
f01031da:	83 e0 f0             	and    $0xfffffff0,%eax
f01031dd:	a2 8e a5 11 f0       	mov    %al,0xf011a58e
f01031e2:	0f b6 05 8e a5 11 f0 	movzbl 0xf011a58e,%eax
f01031e9:	83 e0 ef             	and    $0xffffffef,%eax
f01031ec:	a2 8e a5 11 f0       	mov    %al,0xf011a58e
f01031f1:	0f b6 05 8e a5 11 f0 	movzbl 0xf011a58e,%eax
f01031f8:	83 e0 df             	and    $0xffffffdf,%eax
f01031fb:	a2 8e a5 11 f0       	mov    %al,0xf011a58e
f0103200:	0f b6 05 8e a5 11 f0 	movzbl 0xf011a58e,%eax
f0103207:	83 c8 40             	or     $0x40,%eax
f010320a:	a2 8e a5 11 f0       	mov    %al,0xf011a58e
f010320f:	0f b6 05 8e a5 11 f0 	movzbl 0xf011a58e,%eax
f0103216:	83 e0 7f             	and    $0x7f,%eax
f0103219:	a2 8e a5 11 f0       	mov    %al,0xf011a58e
f010321e:	b8 20 63 14 f0       	mov    $0xf0146320,%eax
f0103223:	c1 e8 18             	shr    $0x18,%eax
f0103226:	a2 8f a5 11 f0       	mov    %al,0xf011a58f
					sizeof(struct Taskstate), 0);
	gdt[GD_TSS >> 3].sd_s = 0;
f010322b:	0f b6 05 8d a5 11 f0 	movzbl 0xf011a58d,%eax
f0103232:	83 e0 ef             	and    $0xffffffef,%eax
f0103235:	a2 8d a5 11 f0       	mov    %al,0xf011a58d
f010323a:	66 c7 45 fe 28 00    	movw   $0x28,-0x2(%ebp)
}

static __inline void
ltr(uint16 sel)
{
	__asm __volatile("ltr %0" : : "r" (sel));
f0103240:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
f0103244:	0f 00 d8             	ltr    %ax

	// Load the TSS
	ltr(GD_TSS);

	// Load the IDT
	asm volatile("lidt idt_pd");
f0103247:	0f 01 1d f8 a5 11 f0 	lidtl  0xf011a5f8
}
f010324e:	c9                   	leave  
f010324f:	c3                   	ret    

f0103250 <print_trapframe>:

void
print_trapframe(struct Trapframe *tf)
{
f0103250:	55                   	push   %ebp
f0103251:	89 e5                	mov    %esp,%ebp
f0103253:	83 ec 18             	sub    $0x18,%esp
	cprintf("TRAP frame at %p\n", tf);
f0103256:	8b 45 08             	mov    0x8(%ebp),%eax
f0103259:	89 44 24 04          	mov    %eax,0x4(%esp)
f010325d:	c7 04 24 bb 5b 10 f0 	movl   $0xf0105bbb,(%esp)
f0103264:	e8 a2 fd ff ff       	call   f010300b <cprintf>
	print_regs(&tf->tf_regs);
f0103269:	8b 45 08             	mov    0x8(%ebp),%eax
f010326c:	89 04 24             	mov    %eax,(%esp)
f010326f:	e8 ea 00 00 00       	call   f010335e <print_regs>
	cprintf("  es   0x----%04x\n", tf->tf_es);
f0103274:	8b 45 08             	mov    0x8(%ebp),%eax
f0103277:	0f b7 40 20          	movzwl 0x20(%eax),%eax
f010327b:	0f b7 c0             	movzwl %ax,%eax
f010327e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103282:	c7 04 24 cd 5b 10 f0 	movl   $0xf0105bcd,(%esp)
f0103289:	e8 7d fd ff ff       	call   f010300b <cprintf>
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f010328e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103291:	0f b7 40 24          	movzwl 0x24(%eax),%eax
f0103295:	0f b7 c0             	movzwl %ax,%eax
f0103298:	89 44 24 04          	mov    %eax,0x4(%esp)
f010329c:	c7 04 24 e0 5b 10 f0 	movl   $0xf0105be0,(%esp)
f01032a3:	e8 63 fd ff ff       	call   f010300b <cprintf>
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f01032a8:	8b 45 08             	mov    0x8(%ebp),%eax
f01032ab:	8b 40 28             	mov    0x28(%eax),%eax
f01032ae:	89 04 24             	mov    %eax,(%esp)
f01032b1:	e8 7b fd ff ff       	call   f0103031 <trapname>
f01032b6:	8b 55 08             	mov    0x8(%ebp),%edx
f01032b9:	8b 52 28             	mov    0x28(%edx),%edx
f01032bc:	89 44 24 08          	mov    %eax,0x8(%esp)
f01032c0:	89 54 24 04          	mov    %edx,0x4(%esp)
f01032c4:	c7 04 24 f3 5b 10 f0 	movl   $0xf0105bf3,(%esp)
f01032cb:	e8 3b fd ff ff       	call   f010300b <cprintf>
	cprintf("  err  0x%08x\n", tf->tf_err);
f01032d0:	8b 45 08             	mov    0x8(%ebp),%eax
f01032d3:	8b 40 2c             	mov    0x2c(%eax),%eax
f01032d6:	89 44 24 04          	mov    %eax,0x4(%esp)
f01032da:	c7 04 24 05 5c 10 f0 	movl   $0xf0105c05,(%esp)
f01032e1:	e8 25 fd ff ff       	call   f010300b <cprintf>
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f01032e6:	8b 45 08             	mov    0x8(%ebp),%eax
f01032e9:	8b 40 30             	mov    0x30(%eax),%eax
f01032ec:	89 44 24 04          	mov    %eax,0x4(%esp)
f01032f0:	c7 04 24 14 5c 10 f0 	movl   $0xf0105c14,(%esp)
f01032f7:	e8 0f fd ff ff       	call   f010300b <cprintf>
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f01032fc:	8b 45 08             	mov    0x8(%ebp),%eax
f01032ff:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f0103303:	0f b7 c0             	movzwl %ax,%eax
f0103306:	89 44 24 04          	mov    %eax,0x4(%esp)
f010330a:	c7 04 24 23 5c 10 f0 	movl   $0xf0105c23,(%esp)
f0103311:	e8 f5 fc ff ff       	call   f010300b <cprintf>
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f0103316:	8b 45 08             	mov    0x8(%ebp),%eax
f0103319:	8b 40 38             	mov    0x38(%eax),%eax
f010331c:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103320:	c7 04 24 36 5c 10 f0 	movl   $0xf0105c36,(%esp)
f0103327:	e8 df fc ff ff       	call   f010300b <cprintf>
	cprintf("  esp  0x%08x\n", tf->tf_esp);
f010332c:	8b 45 08             	mov    0x8(%ebp),%eax
f010332f:	8b 40 3c             	mov    0x3c(%eax),%eax
f0103332:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103336:	c7 04 24 45 5c 10 f0 	movl   $0xf0105c45,(%esp)
f010333d:	e8 c9 fc ff ff       	call   f010300b <cprintf>
	cprintf("  ss   0x----%04x\n", tf->tf_ss);
f0103342:	8b 45 08             	mov    0x8(%ebp),%eax
f0103345:	0f b7 40 40          	movzwl 0x40(%eax),%eax
f0103349:	0f b7 c0             	movzwl %ax,%eax
f010334c:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103350:	c7 04 24 54 5c 10 f0 	movl   $0xf0105c54,(%esp)
f0103357:	e8 af fc ff ff       	call   f010300b <cprintf>
}
f010335c:	c9                   	leave  
f010335d:	c3                   	ret    

f010335e <print_regs>:

void
print_regs(struct PushRegs *regs)
{
f010335e:	55                   	push   %ebp
f010335f:	89 e5                	mov    %esp,%ebp
f0103361:	83 ec 18             	sub    $0x18,%esp
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f0103364:	8b 45 08             	mov    0x8(%ebp),%eax
f0103367:	8b 00                	mov    (%eax),%eax
f0103369:	89 44 24 04          	mov    %eax,0x4(%esp)
f010336d:	c7 04 24 67 5c 10 f0 	movl   $0xf0105c67,(%esp)
f0103374:	e8 92 fc ff ff       	call   f010300b <cprintf>
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f0103379:	8b 45 08             	mov    0x8(%ebp),%eax
f010337c:	8b 40 04             	mov    0x4(%eax),%eax
f010337f:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103383:	c7 04 24 76 5c 10 f0 	movl   $0xf0105c76,(%esp)
f010338a:	e8 7c fc ff ff       	call   f010300b <cprintf>
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f010338f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103392:	8b 40 08             	mov    0x8(%eax),%eax
f0103395:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103399:	c7 04 24 85 5c 10 f0 	movl   $0xf0105c85,(%esp)
f01033a0:	e8 66 fc ff ff       	call   f010300b <cprintf>
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f01033a5:	8b 45 08             	mov    0x8(%ebp),%eax
f01033a8:	8b 40 0c             	mov    0xc(%eax),%eax
f01033ab:	89 44 24 04          	mov    %eax,0x4(%esp)
f01033af:	c7 04 24 94 5c 10 f0 	movl   $0xf0105c94,(%esp)
f01033b6:	e8 50 fc ff ff       	call   f010300b <cprintf>
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f01033bb:	8b 45 08             	mov    0x8(%ebp),%eax
f01033be:	8b 40 10             	mov    0x10(%eax),%eax
f01033c1:	89 44 24 04          	mov    %eax,0x4(%esp)
f01033c5:	c7 04 24 a3 5c 10 f0 	movl   $0xf0105ca3,(%esp)
f01033cc:	e8 3a fc ff ff       	call   f010300b <cprintf>
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f01033d1:	8b 45 08             	mov    0x8(%ebp),%eax
f01033d4:	8b 40 14             	mov    0x14(%eax),%eax
f01033d7:	89 44 24 04          	mov    %eax,0x4(%esp)
f01033db:	c7 04 24 b2 5c 10 f0 	movl   $0xf0105cb2,(%esp)
f01033e2:	e8 24 fc ff ff       	call   f010300b <cprintf>
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f01033e7:	8b 45 08             	mov    0x8(%ebp),%eax
f01033ea:	8b 40 18             	mov    0x18(%eax),%eax
f01033ed:	89 44 24 04          	mov    %eax,0x4(%esp)
f01033f1:	c7 04 24 c1 5c 10 f0 	movl   $0xf0105cc1,(%esp)
f01033f8:	e8 0e fc ff ff       	call   f010300b <cprintf>
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f01033fd:	8b 45 08             	mov    0x8(%ebp),%eax
f0103400:	8b 40 1c             	mov    0x1c(%eax),%eax
f0103403:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103407:	c7 04 24 d0 5c 10 f0 	movl   $0xf0105cd0,(%esp)
f010340e:	e8 f8 fb ff ff       	call   f010300b <cprintf>
}
f0103413:	c9                   	leave  
f0103414:	c3                   	ret    

f0103415 <trap_dispatch>:

static void
trap_dispatch(struct Trapframe *tf)
{
f0103415:	55                   	push   %ebp
f0103416:	89 e5                	mov    %esp,%ebp
f0103418:	57                   	push   %edi
f0103419:	56                   	push   %esi
f010341a:	53                   	push   %ebx
f010341b:	83 ec 3c             	sub    $0x3c,%esp
	// Handle processor exceptions.
	// LAB 3: Your code here.
	
	if(tf->tf_trapno == T_PGFLT)
f010341e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103421:	8b 40 28             	mov    0x28(%eax),%eax
f0103424:	83 f8 0e             	cmp    $0xe,%eax
f0103427:	75 10                	jne    f0103439 <trap_dispatch+0x24>
	{
		page_fault_handler(tf);
f0103429:	8b 45 08             	mov    0x8(%ebp),%eax
f010342c:	89 04 24             	mov    %eax,(%esp)
f010342f:	e8 63 01 00 00       	call   f0103597 <page_fault_handler>
f0103434:	e9 9b 00 00 00       	jmp    f01034d4 <trap_dispatch+0xbf>
	}
	else if (tf->tf_trapno == T_SYSCALL)
f0103439:	8b 45 08             	mov    0x8(%ebp),%eax
f010343c:	8b 40 28             	mov    0x28(%eax),%eax
f010343f:	83 f8 30             	cmp    $0x30,%eax
f0103442:	75 4d                	jne    f0103491 <trap_dispatch+0x7c>
	{
		uint32 ret = syscall(tf->tf_regs.reg_eax
f0103444:	8b 45 08             	mov    0x8(%ebp),%eax
f0103447:	8b 78 04             	mov    0x4(%eax),%edi
f010344a:	8b 45 08             	mov    0x8(%ebp),%eax
f010344d:	8b 30                	mov    (%eax),%esi
f010344f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103452:	8b 58 10             	mov    0x10(%eax),%ebx
f0103455:	8b 45 08             	mov    0x8(%ebp),%eax
f0103458:	8b 48 18             	mov    0x18(%eax),%ecx
f010345b:	8b 45 08             	mov    0x8(%ebp),%eax
f010345e:	8b 50 14             	mov    0x14(%eax),%edx
f0103461:	8b 45 08             	mov    0x8(%ebp),%eax
f0103464:	8b 40 1c             	mov    0x1c(%eax),%eax
f0103467:	89 7c 24 14          	mov    %edi,0x14(%esp)
f010346b:	89 74 24 10          	mov    %esi,0x10(%esp)
f010346f:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
f0103473:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0103477:	89 54 24 04          	mov    %edx,0x4(%esp)
f010347b:	89 04 24             	mov    %eax,(%esp)
f010347e:	e8 6b 04 00 00       	call   f01038ee <syscall>
f0103483:	89 45 e4             	mov    %eax,-0x1c(%ebp)
			,tf->tf_regs.reg_edx
			,tf->tf_regs.reg_ecx
			,tf->tf_regs.reg_ebx
			,tf->tf_regs.reg_edi
					,tf->tf_regs.reg_esi);
		tf->tf_regs.reg_eax = ret;
f0103486:	8b 45 08             	mov    0x8(%ebp),%eax
f0103489:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f010348c:	89 50 1c             	mov    %edx,0x1c(%eax)
f010348f:	eb 43                	jmp    f01034d4 <trap_dispatch+0xbf>
	}
	else
	{
		// Unexpected trap: The user process or the kernel has a bug.
		print_trapframe(tf);
f0103491:	8b 45 08             	mov    0x8(%ebp),%eax
f0103494:	89 04 24             	mov    %eax,(%esp)
f0103497:	e8 b4 fd ff ff       	call   f0103250 <print_trapframe>
		if (tf->tf_cs == GD_KT)
f010349c:	8b 45 08             	mov    0x8(%ebp),%eax
f010349f:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f01034a3:	66 83 f8 08          	cmp    $0x8,%ax
f01034a7:	75 1c                	jne    f01034c5 <trap_dispatch+0xb0>
			panic("unhandled trap in kernel");
f01034a9:	c7 44 24 08 df 5c 10 	movl   $0xf0105cdf,0x8(%esp)
f01034b0:	f0 
f01034b1:	c7 44 24 04 8a 00 00 	movl   $0x8a,0x4(%esp)
f01034b8:	00 
f01034b9:	c7 04 24 f8 5c 10 f0 	movl   $0xf0105cf8,(%esp)
f01034c0:	e8 55 cc ff ff       	call   f010011a <_panic>
		else {
			env_destroy(curenv);
f01034c5:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01034ca:	89 04 24             	mov    %eax,(%esp)
f01034cd:	e8 6b f9 ff ff       	call   f0102e3d <env_destroy>
			return;	
f01034d2:	eb 01                	jmp    f01034d5 <trap_dispatch+0xc0>
		}
	}
	return;
f01034d4:	90                   	nop
}
f01034d5:	83 c4 3c             	add    $0x3c,%esp
f01034d8:	5b                   	pop    %ebx
f01034d9:	5e                   	pop    %esi
f01034da:	5f                   	pop    %edi
f01034db:	5d                   	pop    %ebp
f01034dc:	c3                   	ret    

f01034dd <trap>:

void
trap(struct Trapframe *tf)
{
f01034dd:	55                   	push   %ebp
f01034de:	89 e5                	mov    %esp,%ebp
f01034e0:	57                   	push   %edi
f01034e1:	56                   	push   %esi
f01034e2:	53                   	push   %ebx
f01034e3:	83 ec 1c             	sub    $0x1c,%esp
	//cprintf("Incoming TRAP frame at %p\n", tf);

	if ((tf->tf_cs & 3) == 3) {
f01034e6:	8b 45 08             	mov    0x8(%ebp),%eax
f01034e9:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f01034ed:	0f b7 c0             	movzwl %ax,%eax
f01034f0:	83 e0 03             	and    $0x3,%eax
f01034f3:	83 f8 03             	cmp    $0x3,%eax
f01034f6:	75 4d                	jne    f0103545 <trap+0x68>
		// Trapped from user mode.
		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		assert(curenv);
f01034f8:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01034fd:	85 c0                	test   %eax,%eax
f01034ff:	75 24                	jne    f0103525 <trap+0x48>
f0103501:	c7 44 24 0c 04 5d 10 	movl   $0xf0105d04,0xc(%esp)
f0103508:	f0 
f0103509:	c7 44 24 08 0b 5d 10 	movl   $0xf0105d0b,0x8(%esp)
f0103510:	f0 
f0103511:	c7 44 24 04 9d 00 00 	movl   $0x9d,0x4(%esp)
f0103518:	00 
f0103519:	c7 04 24 f8 5c 10 f0 	movl   $0xf0105cf8,(%esp)
f0103520:	e8 f5 cb ff ff       	call   f010011a <_panic>
		curenv->env_tf = *tf;
f0103525:	8b 15 10 5b 14 f0    	mov    0xf0145b10,%edx
f010352b:	8b 45 08             	mov    0x8(%ebp),%eax
f010352e:	89 c3                	mov    %eax,%ebx
f0103530:	b8 11 00 00 00       	mov    $0x11,%eax
f0103535:	89 d7                	mov    %edx,%edi
f0103537:	89 de                	mov    %ebx,%esi
f0103539:	89 c1                	mov    %eax,%ecx
f010353b:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f010353d:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0103542:	89 45 08             	mov    %eax,0x8(%ebp)
	}
	
	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);
f0103545:	8b 45 08             	mov    0x8(%ebp),%eax
f0103548:	89 04 24             	mov    %eax,(%esp)
f010354b:	e8 c5 fe ff ff       	call   f0103415 <trap_dispatch>

        // Return to the current environment, which should be runnable.
        assert(curenv && curenv->env_status == ENV_RUNNABLE);
f0103550:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0103555:	85 c0                	test   %eax,%eax
f0103557:	74 0d                	je     f0103566 <trap+0x89>
f0103559:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f010355e:	8b 40 54             	mov    0x54(%eax),%eax
f0103561:	83 f8 01             	cmp    $0x1,%eax
f0103564:	74 24                	je     f010358a <trap+0xad>
f0103566:	c7 44 24 0c 20 5d 10 	movl   $0xf0105d20,0xc(%esp)
f010356d:	f0 
f010356e:	c7 44 24 08 0b 5d 10 	movl   $0xf0105d0b,0x8(%esp)
f0103575:	f0 
f0103576:	c7 44 24 04 a7 00 00 	movl   $0xa7,0x4(%esp)
f010357d:	00 
f010357e:	c7 04 24 f8 5c 10 f0 	movl   $0xf0105cf8,(%esp)
f0103585:	e8 90 cb ff ff       	call   f010011a <_panic>
        env_run(curenv);
f010358a:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f010358f:	89 04 24             	mov    %eax,(%esp)
f0103592:	e8 2d f3 ff ff       	call   f01028c4 <env_run>

f0103597 <page_fault_handler>:
}


void
page_fault_handler(struct Trapframe *tf)
{
f0103597:	55                   	push   %ebp
f0103598:	89 e5                	mov    %esp,%ebp
f010359a:	83 ec 28             	sub    $0x28,%esp

static __inline uint32
rcr2(void)
{
	uint32 val;
	__asm __volatile("movl %%cr2,%0" : "=r" (val));
f010359d:	0f 20 d0             	mov    %cr2,%eax
f01035a0:	89 45 f0             	mov    %eax,-0x10(%ebp)
	return val;
f01035a3:	8b 45 f0             	mov    -0x10(%ebp),%eax
	uint32 fault_va;

	// Read processor's CR2 register to find the faulting address
	fault_va = rcr2();
f01035a6:	89 45 f4             	mov    %eax,-0xc(%ebp)
	//   (the 'tf' variable points at 'curenv->env_tf').
	
	// LAB 4: Your code here.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f01035a9:	8b 45 08             	mov    0x8(%ebp),%eax
f01035ac:	8b 50 30             	mov    0x30(%eax),%edx
		curenv->env_id, fault_va, tf->tf_eip);
f01035af:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
	//   (the 'tf' variable points at 'curenv->env_tf').
	
	// LAB 4: Your code here.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f01035b4:	8b 40 4c             	mov    0x4c(%eax),%eax
f01035b7:	89 54 24 0c          	mov    %edx,0xc(%esp)
f01035bb:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01035be:	89 54 24 08          	mov    %edx,0x8(%esp)
f01035c2:	89 44 24 04          	mov    %eax,0x4(%esp)
f01035c6:	c7 04 24 50 5d 10 f0 	movl   $0xf0105d50,(%esp)
f01035cd:	e8 39 fa ff ff       	call   f010300b <cprintf>
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f01035d2:	8b 45 08             	mov    0x8(%ebp),%eax
f01035d5:	89 04 24             	mov    %eax,(%esp)
f01035d8:	e8 73 fc ff ff       	call   f0103250 <print_trapframe>
	env_destroy(curenv);
f01035dd:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01035e2:	89 04 24             	mov    %eax,(%esp)
f01035e5:	e8 53 f8 ff ff       	call   f0102e3d <env_destroy>
	
}
f01035ea:	c9                   	leave  
f01035eb:	c3                   	ret    

f01035ec <PAGE_FAULT>:

/*
 * Lab 3: Your code here for generating entry points for the different traps.
 */

TRAPHANDLER(PAGE_FAULT, T_PGFLT)		
f01035ec:	6a 0e                	push   $0xe
f01035ee:	eb 06                	jmp    f01035f6 <_alltraps>

f01035f0 <SYSCALL_HANDLER>:

TRAPHANDLER_NOEC(SYSCALL_HANDLER, T_SYSCALL)
f01035f0:	6a 00                	push   $0x0
f01035f2:	6a 30                	push   $0x30
f01035f4:	eb 00                	jmp    f01035f6 <_alltraps>

f01035f6 <_alltraps>:
/*
 * Lab 3: Your code here for _alltraps
 */
_alltraps:

push %ds 
f01035f6:	1e                   	push   %ds
push %es 
f01035f7:	06                   	push   %es
pushal 	
f01035f8:	60                   	pusha  

mov $(GD_KD), %ax 
f01035f9:	66 b8 10 00          	mov    $0x10,%ax
mov %ax,%ds
f01035fd:	8e d8                	mov    %eax,%ds
mov %ax,%es
f01035ff:	8e c0                	mov    %eax,%es

push %esp
f0103601:	54                   	push   %esp

call trap
f0103602:	e8 d6 fe ff ff       	call   f01034dd <trap>

pop %ecx /* poping the pointer to the tf from the stack so that the stack top is at the values of the registers posuhed by pusha*/
f0103607:	59                   	pop    %ecx
popal 	
f0103608:	61                   	popa   
pop %es 
f0103609:	07                   	pop    %es
pop %ds    
f010360a:	1f                   	pop    %ds

/*skipping the trap_no and the error code so that the stack top is at the old eip value*/
add $(8),%esp
f010360b:	83 c4 08             	add    $0x8,%esp

iret
f010360e:	cf                   	iret   

f010360f <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f010360f:	55                   	push   %ebp
f0103610:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0103612:	8b 55 08             	mov    0x8(%ebp),%edx
f0103615:	a1 9c 63 14 f0       	mov    0xf014639c,%eax
f010361a:	29 c2                	sub    %eax,%edx
f010361c:	89 d0                	mov    %edx,%eax
f010361e:	c1 f8 02             	sar    $0x2,%eax
f0103621:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f0103627:	5d                   	pop    %ebp
f0103628:	c3                   	ret    

f0103629 <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f0103629:	55                   	push   %ebp
f010362a:	89 e5                	mov    %esp,%ebp
f010362c:	83 ec 04             	sub    $0x4,%esp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f010362f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103632:	89 04 24             	mov    %eax,(%esp)
f0103635:	e8 d5 ff ff ff       	call   f010360f <to_frame_number>
f010363a:	c1 e0 0c             	shl    $0xc,%eax
}
f010363d:	c9                   	leave  
f010363e:	c3                   	ret    

f010363f <sys_cputs>:

// Print a string to the system console.
// The string is exactly 'len' characters long.
// Destroys the environment on memory errors.
static void sys_cputs(const char *s, uint32 len)
{
f010363f:	55                   	push   %ebp
f0103640:	89 e5                	mov    %esp,%ebp
f0103642:	83 ec 18             	sub    $0x18,%esp
	// Destroy the environment if not.
	
	// LAB 3: Your code here.

	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0103645:	8b 45 08             	mov    0x8(%ebp),%eax
f0103648:	89 44 24 08          	mov    %eax,0x8(%esp)
f010364c:	8b 45 0c             	mov    0xc(%ebp),%eax
f010364f:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103653:	c7 04 24 10 5f 10 f0 	movl   $0xf0105f10,(%esp)
f010365a:	e8 ac f9 ff ff       	call   f010300b <cprintf>
}
f010365f:	c9                   	leave  
f0103660:	c3                   	ret    

f0103661 <sys_cgetc>:

// Read a character from the system console.
// Returns the character.
static int
sys_cgetc(void)
{
f0103661:	55                   	push   %ebp
f0103662:	89 e5                	mov    %esp,%ebp
f0103664:	83 ec 18             	sub    $0x18,%esp
	int c;

	// The cons_getc() primitive doesn't wait for a character,
	// but the sys_cgetc() system call does.
	while ((c = cons_getc()) == 0)
f0103667:	e8 15 d2 ff ff       	call   f0100881 <cons_getc>
f010366c:	89 45 f4             	mov    %eax,-0xc(%ebp)
f010366f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0103673:	74 f2                	je     f0103667 <sys_cgetc+0x6>
		/* do nothing */;

	return c;
f0103675:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f0103678:	c9                   	leave  
f0103679:	c3                   	ret    

f010367a <sys_getenvid>:

// Returns the current environment's envid.
static int32 sys_getenvid(void)
{
f010367a:	55                   	push   %ebp
f010367b:	89 e5                	mov    %esp,%ebp
	return curenv->env_id;
f010367d:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0103682:	8b 40 4c             	mov    0x4c(%eax),%eax
}
f0103685:	5d                   	pop    %ebp
f0103686:	c3                   	ret    

f0103687 <sys_env_destroy>:
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int sys_env_destroy(int32  envid)
{
f0103687:	55                   	push   %ebp
f0103688:	89 e5                	mov    %esp,%ebp
f010368a:	83 ec 28             	sub    $0x28,%esp
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
f010368d:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
f0103694:	00 
f0103695:	8d 45 f0             	lea    -0x10(%ebp),%eax
f0103698:	89 44 24 04          	mov    %eax,0x4(%esp)
f010369c:	8b 45 08             	mov    0x8(%ebp),%eax
f010369f:	89 04 24             	mov    %eax,(%esp)
f01036a2:	e8 d8 e4 ff ff       	call   f0101b7f <envid2env>
f01036a7:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01036aa:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01036ae:	79 05                	jns    f01036b5 <sys_env_destroy+0x2e>
		return r;
f01036b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01036b3:	eb 58                	jmp    f010370d <sys_env_destroy+0x86>
	if (e == curenv)
f01036b5:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01036b8:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01036bd:	39 c2                	cmp    %eax,%edx
f01036bf:	75 1a                	jne    f01036db <sys_env_destroy+0x54>
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
f01036c1:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01036c6:	8b 40 4c             	mov    0x4c(%eax),%eax
f01036c9:	89 44 24 04          	mov    %eax,0x4(%esp)
f01036cd:	c7 04 24 15 5f 10 f0 	movl   $0xf0105f15,(%esp)
f01036d4:	e8 32 f9 ff ff       	call   f010300b <cprintf>
f01036d9:	eb 22                	jmp    f01036fd <sys_env_destroy+0x76>
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
f01036db:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01036de:	8b 50 4c             	mov    0x4c(%eax),%edx
f01036e1:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01036e6:	8b 40 4c             	mov    0x4c(%eax),%eax
f01036e9:	89 54 24 08          	mov    %edx,0x8(%esp)
f01036ed:	89 44 24 04          	mov    %eax,0x4(%esp)
f01036f1:	c7 04 24 30 5f 10 f0 	movl   $0xf0105f30,(%esp)
f01036f8:	e8 0e f9 ff ff       	call   f010300b <cprintf>
	env_destroy(e);
f01036fd:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103700:	89 04 24             	mov    %eax,(%esp)
f0103703:	e8 35 f7 ff ff       	call   f0102e3d <env_destroy>
	return 0;
f0103708:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010370d:	c9                   	leave  
f010370e:	c3                   	ret    

f010370f <sys_env_sleep>:

static void sys_env_sleep()
{
f010370f:	55                   	push   %ebp
f0103710:	89 e5                	mov    %esp,%ebp
f0103712:	83 ec 08             	sub    $0x8,%esp
	env_run_cmd_prmpt();
f0103715:	e8 3b f7 ff ff       	call   f0102e55 <env_run_cmd_prmpt>
}
f010371a:	c9                   	leave  
f010371b:	c3                   	ret    

f010371c <sys_allocate_page>:
//	E_INVAL if va >= UTOP, or va is not page-aligned.
//	E_INVAL if perm is inappropriate (see above).
//	E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_allocate_page(void *va, int perm)
{
f010371c:	55                   	push   %ebp
f010371d:	89 e5                	mov    %esp,%ebp
f010371f:	83 ec 38             	sub    $0x38,%esp
	//   parameters for correctness.
	//   If page_insert() fails, remember to free the page you
	//   allocated!
	
	int r;
	struct Env *e = curenv;
f0103722:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0103727:	89 45 f4             	mov    %eax,-0xc(%ebp)

	//if ((r = envid2env(envid, &e, 1)) < 0)
		//return r;
	
	struct Frame_Info *ptr_frame_info ;
	r = allocate_frame(&ptr_frame_info) ;
f010372a:	8d 45 e0             	lea    -0x20(%ebp),%eax
f010372d:	89 04 24             	mov    %eax,(%esp)
f0103730:	e8 6f ec ff ff       	call   f01023a4 <allocate_frame>
f0103735:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (r == E_NO_MEM)
f0103738:	83 7d f0 fc          	cmpl   $0xfffffffc,-0x10(%ebp)
f010373c:	75 08                	jne    f0103746 <sys_allocate_page+0x2a>
		return r ;
f010373e:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103741:	e9 e2 00 00 00       	jmp    f0103828 <sys_allocate_page+0x10c>
	
	//check virtual address to be paged_aligned and < USER_TOP
	if ((uint32)va >= USER_TOP || (uint32)va % PAGE_SIZE != 0)
f0103746:	8b 45 08             	mov    0x8(%ebp),%eax
f0103749:	3d ff ff bf ee       	cmp    $0xeebfffff,%eax
f010374e:	77 0c                	ja     f010375c <sys_allocate_page+0x40>
f0103750:	8b 45 08             	mov    0x8(%ebp),%eax
f0103753:	25 ff 0f 00 00       	and    $0xfff,%eax
f0103758:	85 c0                	test   %eax,%eax
f010375a:	74 0a                	je     f0103766 <sys_allocate_page+0x4a>
		return E_INVAL;
f010375c:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f0103761:	e9 c2 00 00 00       	jmp    f0103828 <sys_allocate_page+0x10c>
	
	//check permissions to be appropriatess
	if ((perm & (~PERM_AVAILABLE & ~PERM_WRITEABLE)) != (PERM_USER))
f0103766:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103769:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f010376e:	83 f8 04             	cmp    $0x4,%eax
f0103771:	74 0a                	je     f010377d <sys_allocate_page+0x61>
		return E_INVAL;
f0103773:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f0103778:	e9 ab 00 00 00       	jmp    f0103828 <sys_allocate_page+0x10c>
	
			
	uint32 physical_address = to_physical_address(ptr_frame_info) ;
f010377d:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103780:	89 04 24             	mov    %eax,(%esp)
f0103783:	e8 a1 fe ff ff       	call   f0103629 <to_physical_address>
f0103788:	89 45 ec             	mov    %eax,-0x14(%ebp)
	
	memset(K_VIRTUAL_ADDRESS(physical_address), 0, PAGE_SIZE);
f010378b:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010378e:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0103791:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0103794:	c1 e8 0c             	shr    $0xc,%eax
f0103797:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f010379a:	a1 88 63 14 f0       	mov    0xf0146388,%eax
f010379f:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f01037a2:	72 23                	jb     f01037c7 <sys_allocate_page+0xab>
f01037a4:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01037a7:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01037ab:	c7 44 24 08 48 5f 10 	movl   $0xf0105f48,0x8(%esp)
f01037b2:	f0 
f01037b3:	c7 44 24 04 7a 00 00 	movl   $0x7a,0x4(%esp)
f01037ba:	00 
f01037bb:	c7 04 24 77 5f 10 f0 	movl   $0xf0105f77,(%esp)
f01037c2:	e8 53 c9 ff ff       	call   f010011a <_panic>
f01037c7:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01037ca:	2d 00 00 00 10       	sub    $0x10000000,%eax
f01037cf:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
f01037d6:	00 
f01037d7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
f01037de:	00 
f01037df:	89 04 24             	mov    %eax,(%esp)
f01037e2:	e8 17 10 00 00       	call   f01047fe <memset>
		
	r = map_frame(e->env_pgdir, ptr_frame_info, va, perm) ;
f01037e7:	8b 55 e0             	mov    -0x20(%ebp),%edx
f01037ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01037ed:	8b 40 5c             	mov    0x5c(%eax),%eax
f01037f0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f01037f3:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f01037f7:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01037fa:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f01037fe:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103802:	89 04 24             	mov    %eax,(%esp)
f0103805:	e8 bd ed ff ff       	call   f01025c7 <map_frame>
f010380a:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (r == E_NO_MEM)
f010380d:	83 7d f0 fc          	cmpl   $0xfffffffc,-0x10(%ebp)
f0103811:	75 10                	jne    f0103823 <sys_allocate_page+0x107>
	{
		decrement_references(ptr_frame_info);
f0103813:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103816:	89 04 24             	mov    %eax,(%esp)
f0103819:	e8 1f ec ff ff       	call   f010243d <decrement_references>
		return r;
f010381e:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103821:	eb 05                	jmp    f0103828 <sys_allocate_page+0x10c>
	}
	return 0 ;
f0103823:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0103828:	c9                   	leave  
f0103829:	c3                   	ret    

f010382a <sys_get_page>:
//	E_INVAL if va >= UTOP, or va is not page-aligned.
//	E_INVAL if perm is inappropriate (see above).
//	E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_get_page(void *va, int perm)
{
f010382a:	55                   	push   %ebp
f010382b:	89 e5                	mov    %esp,%ebp
f010382d:	83 ec 18             	sub    $0x18,%esp
	return get_page(curenv->env_pgdir, va, perm) ;
f0103830:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f0103835:	8b 40 5c             	mov    0x5c(%eax),%eax
f0103838:	8b 55 0c             	mov    0xc(%ebp),%edx
f010383b:	89 54 24 08          	mov    %edx,0x8(%esp)
f010383f:	8b 55 08             	mov    0x8(%ebp),%edx
f0103842:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103846:	89 04 24             	mov    %eax,(%esp)
f0103849:	e8 08 ef ff ff       	call   f0102756 <get_page>
}
f010384e:	c9                   	leave  
f010384f:	c3                   	ret    

f0103850 <sys_map_frame>:
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_map_frame(int32 srcenvid, void *srcva, int32 dstenvid, void *dstva, int perm)
{
f0103850:	55                   	push   %ebp
f0103851:	89 e5                	mov    %esp,%ebp
f0103853:	83 ec 18             	sub    $0x18,%esp
	//   parameters for correctness.
	//   Use the third argument to page_lookup() to
	//   check the current permissions on the page.

	// LAB 4: Your code here.
	panic("sys_map_frame not implemented");
f0103856:	c7 44 24 08 86 5f 10 	movl   $0xf0105f86,0x8(%esp)
f010385d:	f0 
f010385e:	c7 44 24 04 b1 00 00 	movl   $0xb1,0x4(%esp)
f0103865:	00 
f0103866:	c7 04 24 77 5f 10 f0 	movl   $0xf0105f77,(%esp)
f010386d:	e8 a8 c8 ff ff       	call   f010011a <_panic>

f0103872 <sys_unmap_frame>:
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
static int sys_unmap_frame(int32 envid, void *va)
{
f0103872:	55                   	push   %ebp
f0103873:	89 e5                	mov    %esp,%ebp
f0103875:	83 ec 18             	sub    $0x18,%esp
	// Hint: This function is a wrapper around page_remove().
	
	// LAB 4: Your code here.
	panic("sys_page_unmap not implemented");
f0103878:	c7 44 24 08 a4 5f 10 	movl   $0xf0105fa4,0x8(%esp)
f010387f:	f0 
f0103880:	c7 44 24 04 c0 00 00 	movl   $0xc0,0x4(%esp)
f0103887:	00 
f0103888:	c7 04 24 77 5f 10 f0 	movl   $0xf0105f77,(%esp)
f010388f:	e8 86 c8 ff ff       	call   f010011a <_panic>

f0103894 <sys_calculate_required_frames>:
}

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
f0103894:	55                   	push   %ebp
f0103895:	89 e5                	mov    %esp,%ebp
f0103897:	83 ec 18             	sub    $0x18,%esp
	return calculate_required_frames(curenv->env_pgdir, start_virtual_address, size); 
f010389a:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f010389f:	8b 40 5c             	mov    0x5c(%eax),%eax
f01038a2:	8b 55 0c             	mov    0xc(%ebp),%edx
f01038a5:	89 54 24 08          	mov    %edx,0x8(%esp)
f01038a9:	8b 55 08             	mov    0x8(%ebp),%edx
f01038ac:	89 54 24 04          	mov    %edx,0x4(%esp)
f01038b0:	89 04 24             	mov    %eax,(%esp)
f01038b3:	e8 c0 ee ff ff       	call   f0102778 <calculate_required_frames>
}
f01038b8:	c9                   	leave  
f01038b9:	c3                   	ret    

f01038ba <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
f01038ba:	55                   	push   %ebp
f01038bb:	89 e5                	mov    %esp,%ebp
f01038bd:	83 ec 08             	sub    $0x8,%esp
	return calculate_free_frames();
f01038c0:	e8 d5 ee ff ff       	call   f010279a <calculate_free_frames>
}
f01038c5:	c9                   	leave  
f01038c6:	c3                   	ret    

f01038c7 <sys_freeMem>:
void sys_freeMem(void* start_virtual_address, uint32 size)
{
f01038c7:	55                   	push   %ebp
f01038c8:	89 e5                	mov    %esp,%ebp
f01038ca:	83 ec 18             	sub    $0x18,%esp
	freeMem((uint32*)curenv->env_pgdir, (void*)start_virtual_address, size);
f01038cd:	a1 10 5b 14 f0       	mov    0xf0145b10,%eax
f01038d2:	8b 40 5c             	mov    0x5c(%eax),%eax
f01038d5:	8b 55 0c             	mov    0xc(%ebp),%edx
f01038d8:	89 54 24 08          	mov    %edx,0x8(%esp)
f01038dc:	8b 55 08             	mov    0x8(%ebp),%edx
f01038df:	89 54 24 04          	mov    %edx,0x4(%esp)
f01038e3:	89 04 24             	mov    %eax,(%esp)
f01038e6:	e8 dd ee ff ff       	call   f01027c8 <freeMem>
	return;
f01038eb:	90                   	nop
}
f01038ec:	c9                   	leave  
f01038ed:	c3                   	ret    

f01038ee <syscall>:
// Dispatches to the correct kernel function, passing the arguments.
uint32
syscall(uint32 syscallno, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
f01038ee:	55                   	push   %ebp
f01038ef:	89 e5                	mov    %esp,%ebp
f01038f1:	56                   	push   %esi
f01038f2:	53                   	push   %ebx
f01038f3:	83 ec 20             	sub    $0x20,%esp
	// Call the function corresponding to the 'syscallno' parameter.
	// Return any appropriate return value.
	// LAB 3: Your code here.
	switch(syscallno)
f01038f6:	83 7d 08 0c          	cmpl   $0xc,0x8(%ebp)
f01038fa:	0f 87 1b 01 00 00    	ja     f0103a1b <syscall+0x12d>
f0103900:	8b 45 08             	mov    0x8(%ebp),%eax
f0103903:	c1 e0 02             	shl    $0x2,%eax
f0103906:	05 c4 5f 10 f0       	add    $0xf0105fc4,%eax
f010390b:	8b 00                	mov    (%eax),%eax
f010390d:	ff e0                	jmp    *%eax
	{
		case SYS_cputs:
			sys_cputs((const char*)a1,a2);
f010390f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103912:	8b 55 10             	mov    0x10(%ebp),%edx
f0103915:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103919:	89 04 24             	mov    %eax,(%esp)
f010391c:	e8 1e fd ff ff       	call   f010363f <sys_cputs>
			return 0;
f0103921:	b8 00 00 00 00       	mov    $0x0,%eax
f0103926:	e9 f5 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_cgetc:
			return sys_cgetc();
f010392b:	e8 31 fd ff ff       	call   f0103661 <sys_cgetc>
f0103930:	e9 eb 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_getenvid:
			return sys_getenvid();
f0103935:	e8 40 fd ff ff       	call   f010367a <sys_getenvid>
f010393a:	e9 e1 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_env_destroy:
			return sys_env_destroy(a1);
f010393f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103942:	89 04 24             	mov    %eax,(%esp)
f0103945:	e8 3d fd ff ff       	call   f0103687 <sys_env_destroy>
f010394a:	e9 d1 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_env_sleep:
			sys_env_sleep();
f010394f:	e8 bb fd ff ff       	call   f010370f <sys_env_sleep>
			return 0;
f0103954:	b8 00 00 00 00       	mov    $0x0,%eax
f0103959:	e9 c2 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_calc_req_frames:
			return sys_calculate_required_frames(a1, a2);			
f010395e:	8b 45 10             	mov    0x10(%ebp),%eax
f0103961:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103965:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103968:	89 04 24             	mov    %eax,(%esp)
f010396b:	e8 24 ff ff ff       	call   f0103894 <sys_calculate_required_frames>
f0103970:	e9 ab 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_calc_free_frames:
			return sys_calculate_free_frames();			
f0103975:	e8 40 ff ff ff       	call   f01038ba <sys_calculate_free_frames>
f010397a:	e9 a1 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_freeMem:
			sys_freeMem((void*)a1, a2);
f010397f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103982:	8b 55 10             	mov    0x10(%ebp),%edx
f0103985:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103989:	89 04 24             	mov    %eax,(%esp)
f010398c:	e8 36 ff ff ff       	call   f01038c7 <sys_freeMem>
			return 0;			
f0103991:	b8 00 00 00 00       	mov    $0x0,%eax
f0103996:	e9 85 00 00 00       	jmp    f0103a20 <syscall+0x132>
			break;
		//======================
		
		case SYS_allocate_page:
			sys_allocate_page((void*)a1, a2);
f010399b:	8b 55 10             	mov    0x10(%ebp),%edx
f010399e:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039a1:	89 54 24 04          	mov    %edx,0x4(%esp)
f01039a5:	89 04 24             	mov    %eax,(%esp)
f01039a8:	e8 6f fd ff ff       	call   f010371c <sys_allocate_page>
			return 0;
f01039ad:	b8 00 00 00 00       	mov    $0x0,%eax
f01039b2:	eb 6c                	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_get_page:
			sys_get_page((void*)a1, a2);
f01039b4:	8b 55 10             	mov    0x10(%ebp),%edx
f01039b7:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039ba:	89 54 24 04          	mov    %edx,0x4(%esp)
f01039be:	89 04 24             	mov    %eax,(%esp)
f01039c1:	e8 64 fe ff ff       	call   f010382a <sys_get_page>
			return 0;
f01039c6:	b8 00 00 00 00       	mov    $0x0,%eax
f01039cb:	eb 53                	jmp    f0103a20 <syscall+0x132>
		break;case SYS_map_frame:
			sys_map_frame(a1, (void*)a2, a3, (void*)a4, a5);
f01039cd:	8b 75 1c             	mov    0x1c(%ebp),%esi
f01039d0:	8b 5d 18             	mov    0x18(%ebp),%ebx
f01039d3:	8b 4d 14             	mov    0x14(%ebp),%ecx
f01039d6:	8b 55 10             	mov    0x10(%ebp),%edx
f01039d9:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039dc:	89 74 24 10          	mov    %esi,0x10(%esp)
f01039e0:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
f01039e4:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f01039e8:	89 54 24 04          	mov    %edx,0x4(%esp)
f01039ec:	89 04 24             	mov    %eax,(%esp)
f01039ef:	e8 5c fe ff ff       	call   f0103850 <sys_map_frame>
			return 0;
f01039f4:	b8 00 00 00 00       	mov    $0x0,%eax
f01039f9:	eb 25                	jmp    f0103a20 <syscall+0x132>
			break;
		case SYS_unmap_frame:
			sys_unmap_frame(a1, (void*)a2);
f01039fb:	8b 55 10             	mov    0x10(%ebp),%edx
f01039fe:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103a01:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103a05:	89 04 24             	mov    %eax,(%esp)
f0103a08:	e8 65 fe ff ff       	call   f0103872 <sys_unmap_frame>
			return 0;
f0103a0d:	b8 00 00 00 00       	mov    $0x0,%eax
f0103a12:	eb 0c                	jmp    f0103a20 <syscall+0x132>
			break;
		case NSYSCALLS:	
			return 	-E_INVAL;
f0103a14:	b8 03 00 00 00       	mov    $0x3,%eax
f0103a19:	eb 05                	jmp    f0103a20 <syscall+0x132>
			break;
	}
	//panic("syscall not implemented");
	return -E_INVAL;
f0103a1b:	b8 03 00 00 00       	mov    $0x3,%eax
}
f0103a20:	83 c4 20             	add    $0x20,%esp
f0103a23:	5b                   	pop    %ebx
f0103a24:	5e                   	pop    %esi
f0103a25:	5d                   	pop    %ebp
f0103a26:	c3                   	ret    

f0103a27 <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uint32*  addr)
{
f0103a27:	55                   	push   %ebp
f0103a28:	89 e5                	mov    %esp,%ebp
f0103a2a:	83 ec 20             	sub    $0x20,%esp
	int l = *region_left, r = *region_right, any_matches = 0;
f0103a2d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103a30:	8b 00                	mov    (%eax),%eax
f0103a32:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103a35:	8b 45 10             	mov    0x10(%ebp),%eax
f0103a38:	8b 00                	mov    (%eax),%eax
f0103a3a:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0103a3d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	
	while (l <= r) {
f0103a44:	e9 d2 00 00 00       	jmp    f0103b1b <stab_binsearch+0xf4>
		int true_m = (l + r) / 2, m = true_m;
f0103a49:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0103a4c:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103a4f:	01 d0                	add    %edx,%eax
f0103a51:	89 c2                	mov    %eax,%edx
f0103a53:	c1 ea 1f             	shr    $0x1f,%edx
f0103a56:	01 d0                	add    %edx,%eax
f0103a58:	d1 f8                	sar    %eax
f0103a5a:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0103a5d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103a60:	89 45 f0             	mov    %eax,-0x10(%ebp)
		
		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103a63:	eb 04                	jmp    f0103a69 <stab_binsearch+0x42>
			m--;
f0103a65:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
	
	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;
		
		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103a69:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103a6c:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103a6f:	7c 1f                	jl     f0103a90 <stab_binsearch+0x69>
f0103a71:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103a74:	89 d0                	mov    %edx,%eax
f0103a76:	01 c0                	add    %eax,%eax
f0103a78:	01 d0                	add    %edx,%eax
f0103a7a:	c1 e0 02             	shl    $0x2,%eax
f0103a7d:	89 c2                	mov    %eax,%edx
f0103a7f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103a82:	01 d0                	add    %edx,%eax
f0103a84:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103a88:	0f b6 c0             	movzbl %al,%eax
f0103a8b:	3b 45 14             	cmp    0x14(%ebp),%eax
f0103a8e:	75 d5                	jne    f0103a65 <stab_binsearch+0x3e>
			m--;
		if (m < l) {	// no match in [l, m]
f0103a90:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103a93:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103a96:	7d 0b                	jge    f0103aa3 <stab_binsearch+0x7c>
			l = true_m + 1;
f0103a98:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103a9b:	83 c0 01             	add    $0x1,%eax
f0103a9e:	89 45 fc             	mov    %eax,-0x4(%ebp)
			continue;
f0103aa1:	eb 78                	jmp    f0103b1b <stab_binsearch+0xf4>
		}

		// actual binary search
		any_matches = 1;
f0103aa3:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
		if (stabs[m].n_value < addr) {
f0103aaa:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103aad:	89 d0                	mov    %edx,%eax
f0103aaf:	01 c0                	add    %eax,%eax
f0103ab1:	01 d0                	add    %edx,%eax
f0103ab3:	c1 e0 02             	shl    $0x2,%eax
f0103ab6:	89 c2                	mov    %eax,%edx
f0103ab8:	8b 45 08             	mov    0x8(%ebp),%eax
f0103abb:	01 d0                	add    %edx,%eax
f0103abd:	8b 40 08             	mov    0x8(%eax),%eax
f0103ac0:	3b 45 18             	cmp    0x18(%ebp),%eax
f0103ac3:	73 13                	jae    f0103ad8 <stab_binsearch+0xb1>
			*region_left = m;
f0103ac5:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ac8:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103acb:	89 10                	mov    %edx,(%eax)
			l = true_m + 1;
f0103acd:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103ad0:	83 c0 01             	add    $0x1,%eax
f0103ad3:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103ad6:	eb 43                	jmp    f0103b1b <stab_binsearch+0xf4>
		} else if (stabs[m].n_value > addr) {
f0103ad8:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103adb:	89 d0                	mov    %edx,%eax
f0103add:	01 c0                	add    %eax,%eax
f0103adf:	01 d0                	add    %edx,%eax
f0103ae1:	c1 e0 02             	shl    $0x2,%eax
f0103ae4:	89 c2                	mov    %eax,%edx
f0103ae6:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ae9:	01 d0                	add    %edx,%eax
f0103aeb:	8b 40 08             	mov    0x8(%eax),%eax
f0103aee:	3b 45 18             	cmp    0x18(%ebp),%eax
f0103af1:	76 16                	jbe    f0103b09 <stab_binsearch+0xe2>
			*region_right = m - 1;
f0103af3:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103af6:	8d 50 ff             	lea    -0x1(%eax),%edx
f0103af9:	8b 45 10             	mov    0x10(%ebp),%eax
f0103afc:	89 10                	mov    %edx,(%eax)
			r = m - 1;
f0103afe:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103b01:	83 e8 01             	sub    $0x1,%eax
f0103b04:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0103b07:	eb 12                	jmp    f0103b1b <stab_binsearch+0xf4>
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0103b09:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b0c:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103b0f:	89 10                	mov    %edx,(%eax)
			l = m;
f0103b11:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103b14:	89 45 fc             	mov    %eax,-0x4(%ebp)
			addr++;
f0103b17:	83 45 18 04          	addl   $0x4,0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uint32*  addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;
	
	while (l <= r) {
f0103b1b:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0103b1e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0103b21:	0f 8e 22 ff ff ff    	jle    f0103a49 <stab_binsearch+0x22>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0103b27:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0103b2b:	75 0f                	jne    f0103b3c <stab_binsearch+0x115>
		*region_right = *region_left - 1;
f0103b2d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b30:	8b 00                	mov    (%eax),%eax
f0103b32:	8d 50 ff             	lea    -0x1(%eax),%edx
f0103b35:	8b 45 10             	mov    0x10(%ebp),%eax
f0103b38:	89 10                	mov    %edx,(%eax)
f0103b3a:	eb 3f                	jmp    f0103b7b <stab_binsearch+0x154>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103b3c:	8b 45 10             	mov    0x10(%ebp),%eax
f0103b3f:	8b 00                	mov    (%eax),%eax
f0103b41:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103b44:	eb 04                	jmp    f0103b4a <stab_binsearch+0x123>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0103b46:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
		     l > *region_left && stabs[l].n_type != type;
f0103b4a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b4d:	8b 00                	mov    (%eax),%eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103b4f:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103b52:	7d 1f                	jge    f0103b73 <stab_binsearch+0x14c>
		     l > *region_left && stabs[l].n_type != type;
f0103b54:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103b57:	89 d0                	mov    %edx,%eax
f0103b59:	01 c0                	add    %eax,%eax
f0103b5b:	01 d0                	add    %edx,%eax
f0103b5d:	c1 e0 02             	shl    $0x2,%eax
f0103b60:	89 c2                	mov    %eax,%edx
f0103b62:	8b 45 08             	mov    0x8(%ebp),%eax
f0103b65:	01 d0                	add    %edx,%eax
f0103b67:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103b6b:	0f b6 c0             	movzbl %al,%eax
f0103b6e:	3b 45 14             	cmp    0x14(%ebp),%eax
f0103b71:	75 d3                	jne    f0103b46 <stab_binsearch+0x11f>
		     l--)
			/* do nothing */;
		*region_left = l;
f0103b73:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b76:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103b79:	89 10                	mov    %edx,(%eax)
	}
}
f0103b7b:	c9                   	leave  
f0103b7c:	c3                   	ret    

f0103b7d <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uint32*  addr, struct Eipdebuginfo *info)
{
f0103b7d:	55                   	push   %ebp
f0103b7e:	89 e5                	mov    %esp,%ebp
f0103b80:	83 ec 58             	sub    $0x58,%esp
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0103b83:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b86:	c7 00 f8 5f 10 f0    	movl   $0xf0105ff8,(%eax)
	info->eip_line = 0;
f0103b8c:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b8f:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
	info->eip_fn_name = "<unknown>";
f0103b96:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b99:	c7 40 08 f8 5f 10 f0 	movl   $0xf0105ff8,0x8(%eax)
	info->eip_fn_namelen = 9;
f0103ba0:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ba3:	c7 40 0c 09 00 00 00 	movl   $0x9,0xc(%eax)
	info->eip_fn_addr = addr;
f0103baa:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bad:	8b 55 08             	mov    0x8(%ebp),%edx
f0103bb0:	89 50 10             	mov    %edx,0x10(%eax)
	info->eip_fn_narg = 0;
f0103bb3:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bb6:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)

	// Find the relevant set of stabs
	if ((uint32)addr >= USER_LIMIT) {
f0103bbd:	8b 45 08             	mov    0x8(%ebp),%eax
f0103bc0:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f0103bc5:	76 1e                	jbe    f0103be5 <debuginfo_eip+0x68>
		stabs = __STAB_BEGIN__;
f0103bc7:	c7 45 f4 50 62 10 f0 	movl   $0xf0106250,-0xc(%ebp)
		stab_end = __STAB_END__;
f0103bce:	c7 45 f0 e0 e3 10 f0 	movl   $0xf010e3e0,-0x10(%ebp)
		stabstr = __STABSTR_BEGIN__;
f0103bd5:	c7 45 ec e1 e3 10 f0 	movl   $0xf010e3e1,-0x14(%ebp)
		stabstr_end = __STABSTR_END__;
f0103bdc:	c7 45 e8 94 1a 11 f0 	movl   $0xf0111a94,-0x18(%ebp)
f0103be3:	eb 2a                	jmp    f0103c0f <debuginfo_eip+0x92>
		// The user-application linker script, user/user.ld,
		// puts information about the application's stabs (equivalent
		// to __STAB_BEGIN__, __STAB_END__, __STABSTR_BEGIN__, and
		// __STABSTR_END__) in a structure located at virtual address
		// USTABDATA.
		const struct UserStabData *usd = (const struct UserStabData *) USTABDATA;
f0103be5:	c7 45 e0 00 00 20 00 	movl   $0x200000,-0x20(%ebp)

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.
		
		stabs = usd->stabs;
f0103bec:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103bef:	8b 00                	mov    (%eax),%eax
f0103bf1:	89 45 f4             	mov    %eax,-0xc(%ebp)
		stab_end = usd->stab_end;
f0103bf4:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103bf7:	8b 40 04             	mov    0x4(%eax),%eax
f0103bfa:	89 45 f0             	mov    %eax,-0x10(%ebp)
		stabstr = usd->stabstr;
f0103bfd:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c00:	8b 40 08             	mov    0x8(%eax),%eax
f0103c03:	89 45 ec             	mov    %eax,-0x14(%ebp)
		stabstr_end = usd->stabstr_end;
f0103c06:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c09:	8b 40 0c             	mov    0xc(%eax),%eax
f0103c0c:	89 45 e8             	mov    %eax,-0x18(%ebp)
		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0103c0f:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0103c12:	3b 45 ec             	cmp    -0x14(%ebp),%eax
f0103c15:	76 0d                	jbe    f0103c24 <debuginfo_eip+0xa7>
f0103c17:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0103c1a:	83 e8 01             	sub    $0x1,%eax
f0103c1d:	0f b6 00             	movzbl (%eax),%eax
f0103c20:	84 c0                	test   %al,%al
f0103c22:	74 0a                	je     f0103c2e <debuginfo_eip+0xb1>
		return -1;
f0103c24:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0103c29:	e9 09 02 00 00       	jmp    f0103e37 <debuginfo_eip+0x2ba>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.
	
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0103c2e:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
	rfile = (stab_end - stabs) - 1;
f0103c35:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103c38:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103c3b:	29 c2                	sub    %eax,%edx
f0103c3d:	89 d0                	mov    %edx,%eax
f0103c3f:	c1 f8 02             	sar    $0x2,%eax
f0103c42:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
f0103c48:	83 e8 01             	sub    $0x1,%eax
f0103c4b:	89 45 d4             	mov    %eax,-0x2c(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f0103c4e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103c51:	89 44 24 10          	mov    %eax,0x10(%esp)
f0103c55:	c7 44 24 0c 64 00 00 	movl   $0x64,0xc(%esp)
f0103c5c:	00 
f0103c5d:	8d 45 d4             	lea    -0x2c(%ebp),%eax
f0103c60:	89 44 24 08          	mov    %eax,0x8(%esp)
f0103c64:	8d 45 d8             	lea    -0x28(%ebp),%eax
f0103c67:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103c6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103c6e:	89 04 24             	mov    %eax,(%esp)
f0103c71:	e8 b1 fd ff ff       	call   f0103a27 <stab_binsearch>
	if (lfile == 0)
f0103c76:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103c79:	85 c0                	test   %eax,%eax
f0103c7b:	75 0a                	jne    f0103c87 <debuginfo_eip+0x10a>
		return -1;
f0103c7d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0103c82:	e9 b0 01 00 00       	jmp    f0103e37 <debuginfo_eip+0x2ba>

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0103c87:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103c8a:	89 45 d0             	mov    %eax,-0x30(%ebp)
	rfun = rfile;
f0103c8d:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103c90:	89 45 cc             	mov    %eax,-0x34(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f0103c93:	8b 45 08             	mov    0x8(%ebp),%eax
f0103c96:	89 44 24 10          	mov    %eax,0x10(%esp)
f0103c9a:	c7 44 24 0c 24 00 00 	movl   $0x24,0xc(%esp)
f0103ca1:	00 
f0103ca2:	8d 45 cc             	lea    -0x34(%ebp),%eax
f0103ca5:	89 44 24 08          	mov    %eax,0x8(%esp)
f0103ca9:	8d 45 d0             	lea    -0x30(%ebp),%eax
f0103cac:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103cb0:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103cb3:	89 04 24             	mov    %eax,(%esp)
f0103cb6:	e8 6c fd ff ff       	call   f0103a27 <stab_binsearch>

	if (lfun <= rfun) {
f0103cbb:	8b 55 d0             	mov    -0x30(%ebp),%edx
f0103cbe:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0103cc1:	39 c2                	cmp    %eax,%edx
f0103cc3:	0f 8f 86 00 00 00    	jg     f0103d4f <debuginfo_eip+0x1d2>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0103cc9:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103ccc:	89 c2                	mov    %eax,%edx
f0103cce:	89 d0                	mov    %edx,%eax
f0103cd0:	01 c0                	add    %eax,%eax
f0103cd2:	01 d0                	add    %edx,%eax
f0103cd4:	c1 e0 02             	shl    $0x2,%eax
f0103cd7:	89 c2                	mov    %eax,%edx
f0103cd9:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103cdc:	01 d0                	add    %edx,%eax
f0103cde:	8b 10                	mov    (%eax),%edx
f0103ce0:	8b 4d e8             	mov    -0x18(%ebp),%ecx
f0103ce3:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103ce6:	29 c1                	sub    %eax,%ecx
f0103ce8:	89 c8                	mov    %ecx,%eax
f0103cea:	39 c2                	cmp    %eax,%edx
f0103cec:	73 22                	jae    f0103d10 <debuginfo_eip+0x193>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0103cee:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103cf1:	89 c2                	mov    %eax,%edx
f0103cf3:	89 d0                	mov    %edx,%eax
f0103cf5:	01 c0                	add    %eax,%eax
f0103cf7:	01 d0                	add    %edx,%eax
f0103cf9:	c1 e0 02             	shl    $0x2,%eax
f0103cfc:	89 c2                	mov    %eax,%edx
f0103cfe:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103d01:	01 d0                	add    %edx,%eax
f0103d03:	8b 10                	mov    (%eax),%edx
f0103d05:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103d08:	01 c2                	add    %eax,%edx
f0103d0a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d0d:	89 50 08             	mov    %edx,0x8(%eax)
		info->eip_fn_addr = (uint32*) stabs[lfun].n_value;
f0103d10:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103d13:	89 c2                	mov    %eax,%edx
f0103d15:	89 d0                	mov    %edx,%eax
f0103d17:	01 c0                	add    %eax,%eax
f0103d19:	01 d0                	add    %edx,%eax
f0103d1b:	c1 e0 02             	shl    $0x2,%eax
f0103d1e:	89 c2                	mov    %eax,%edx
f0103d20:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103d23:	01 d0                	add    %edx,%eax
f0103d25:	8b 50 08             	mov    0x8(%eax),%edx
f0103d28:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d2b:	89 50 10             	mov    %edx,0x10(%eax)
		addr = (uint32*)(addr - (info->eip_fn_addr));
f0103d2e:	8b 55 08             	mov    0x8(%ebp),%edx
f0103d31:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d34:	8b 40 10             	mov    0x10(%eax),%eax
f0103d37:	29 c2                	sub    %eax,%edx
f0103d39:	89 d0                	mov    %edx,%eax
f0103d3b:	c1 f8 02             	sar    $0x2,%eax
f0103d3e:	89 45 08             	mov    %eax,0x8(%ebp)
		// Search within the function definition for the line number.
		lline = lfun;
f0103d41:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103d44:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		rline = rfun;
f0103d47:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0103d4a:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0103d4d:	eb 15                	jmp    f0103d64 <debuginfo_eip+0x1e7>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0103d4f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d52:	8b 55 08             	mov    0x8(%ebp),%edx
f0103d55:	89 50 10             	mov    %edx,0x10(%eax)
		lline = lfile;
f0103d58:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103d5b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		rline = rfile;
f0103d5e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103d61:	89 45 dc             	mov    %eax,-0x24(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0103d64:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d67:	8b 40 08             	mov    0x8(%eax),%eax
f0103d6a:	c7 44 24 04 3a 00 00 	movl   $0x3a,0x4(%esp)
f0103d71:	00 
f0103d72:	89 04 24             	mov    %eax,(%esp)
f0103d75:	e8 56 0a 00 00       	call   f01047d0 <strfind>
f0103d7a:	89 c2                	mov    %eax,%edx
f0103d7c:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d7f:	8b 40 08             	mov    0x8(%eax),%eax
f0103d82:	29 c2                	sub    %eax,%edx
f0103d84:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d87:	89 50 0c             	mov    %edx,0xc(%eax)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0103d8a:	eb 04                	jmp    f0103d90 <debuginfo_eip+0x213>
	       && stabs[lline].n_type != N_SOL
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
		lline--;
f0103d8c:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0103d90:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103d93:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f0103d96:	7c 50                	jl     f0103de8 <debuginfo_eip+0x26b>
	       && stabs[lline].n_type != N_SOL
f0103d98:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103d9b:	89 d0                	mov    %edx,%eax
f0103d9d:	01 c0                	add    %eax,%eax
f0103d9f:	01 d0                	add    %edx,%eax
f0103da1:	c1 e0 02             	shl    $0x2,%eax
f0103da4:	89 c2                	mov    %eax,%edx
f0103da6:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103da9:	01 d0                	add    %edx,%eax
f0103dab:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103daf:	3c 84                	cmp    $0x84,%al
f0103db1:	74 35                	je     f0103de8 <debuginfo_eip+0x26b>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f0103db3:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103db6:	89 d0                	mov    %edx,%eax
f0103db8:	01 c0                	add    %eax,%eax
f0103dba:	01 d0                	add    %edx,%eax
f0103dbc:	c1 e0 02             	shl    $0x2,%eax
f0103dbf:	89 c2                	mov    %eax,%edx
f0103dc1:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103dc4:	01 d0                	add    %edx,%eax
f0103dc6:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103dca:	3c 64                	cmp    $0x64,%al
f0103dcc:	75 be                	jne    f0103d8c <debuginfo_eip+0x20f>
f0103dce:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103dd1:	89 d0                	mov    %edx,%eax
f0103dd3:	01 c0                	add    %eax,%eax
f0103dd5:	01 d0                	add    %edx,%eax
f0103dd7:	c1 e0 02             	shl    $0x2,%eax
f0103dda:	89 c2                	mov    %eax,%edx
f0103ddc:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103ddf:	01 d0                	add    %edx,%eax
f0103de1:	8b 40 08             	mov    0x8(%eax),%eax
f0103de4:	85 c0                	test   %eax,%eax
f0103de6:	74 a4                	je     f0103d8c <debuginfo_eip+0x20f>
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f0103de8:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103deb:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f0103dee:	7c 42                	jl     f0103e32 <debuginfo_eip+0x2b5>
f0103df0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103df3:	89 d0                	mov    %edx,%eax
f0103df5:	01 c0                	add    %eax,%eax
f0103df7:	01 d0                	add    %edx,%eax
f0103df9:	c1 e0 02             	shl    $0x2,%eax
f0103dfc:	89 c2                	mov    %eax,%edx
f0103dfe:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103e01:	01 d0                	add    %edx,%eax
f0103e03:	8b 10                	mov    (%eax),%edx
f0103e05:	8b 4d e8             	mov    -0x18(%ebp),%ecx
f0103e08:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103e0b:	29 c1                	sub    %eax,%ecx
f0103e0d:	89 c8                	mov    %ecx,%eax
f0103e0f:	39 c2                	cmp    %eax,%edx
f0103e11:	73 1f                	jae    f0103e32 <debuginfo_eip+0x2b5>
		info->eip_file = stabstr + stabs[lline].n_strx;
f0103e13:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103e16:	89 d0                	mov    %edx,%eax
f0103e18:	01 c0                	add    %eax,%eax
f0103e1a:	01 d0                	add    %edx,%eax
f0103e1c:	c1 e0 02             	shl    $0x2,%eax
f0103e1f:	89 c2                	mov    %eax,%edx
f0103e21:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103e24:	01 d0                	add    %edx,%eax
f0103e26:	8b 10                	mov    (%eax),%edx
f0103e28:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103e2b:	01 c2                	add    %eax,%edx
f0103e2d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103e30:	89 10                	mov    %edx,(%eax)
	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	// Your code here.

	
	return 0;
f0103e32:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0103e37:	c9                   	leave  
f0103e38:	c3                   	ret    

f0103e39 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f0103e39:	55                   	push   %ebp
f0103e3a:	89 e5                	mov    %esp,%ebp
f0103e3c:	53                   	push   %ebx
f0103e3d:	83 ec 34             	sub    $0x34,%esp
f0103e40:	8b 45 10             	mov    0x10(%ebp),%eax
f0103e43:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0103e46:	8b 45 14             	mov    0x14(%ebp),%eax
f0103e49:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f0103e4c:	8b 45 18             	mov    0x18(%ebp),%eax
f0103e4f:	ba 00 00 00 00       	mov    $0x0,%edx
f0103e54:	3b 55 f4             	cmp    -0xc(%ebp),%edx
f0103e57:	77 72                	ja     f0103ecb <printnum+0x92>
f0103e59:	3b 55 f4             	cmp    -0xc(%ebp),%edx
f0103e5c:	72 05                	jb     f0103e63 <printnum+0x2a>
f0103e5e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
f0103e61:	77 68                	ja     f0103ecb <printnum+0x92>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f0103e63:	8b 45 1c             	mov    0x1c(%ebp),%eax
f0103e66:	8d 58 ff             	lea    -0x1(%eax),%ebx
f0103e69:	8b 45 18             	mov    0x18(%ebp),%eax
f0103e6c:	ba 00 00 00 00       	mov    $0x0,%edx
f0103e71:	89 44 24 08          	mov    %eax,0x8(%esp)
f0103e75:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0103e79:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103e7c:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0103e7f:	89 04 24             	mov    %eax,(%esp)
f0103e82:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103e86:	e8 35 0d 00 00       	call   f0104bc0 <__udivdi3>
f0103e8b:	8b 4d 20             	mov    0x20(%ebp),%ecx
f0103e8e:	89 4c 24 18          	mov    %ecx,0x18(%esp)
f0103e92:	89 5c 24 14          	mov    %ebx,0x14(%esp)
f0103e96:	8b 4d 18             	mov    0x18(%ebp),%ecx
f0103e99:	89 4c 24 10          	mov    %ecx,0x10(%esp)
f0103e9d:	89 44 24 08          	mov    %eax,0x8(%esp)
f0103ea1:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0103ea5:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ea8:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103eac:	8b 45 08             	mov    0x8(%ebp),%eax
f0103eaf:	89 04 24             	mov    %eax,(%esp)
f0103eb2:	e8 82 ff ff ff       	call   f0103e39 <printnum>
f0103eb7:	eb 1c                	jmp    f0103ed5 <printnum+0x9c>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0103eb9:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ebc:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103ec0:	8b 45 20             	mov    0x20(%ebp),%eax
f0103ec3:	89 04 24             	mov    %eax,(%esp)
f0103ec6:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ec9:	ff d0                	call   *%eax
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0103ecb:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
f0103ecf:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
f0103ed3:	7f e4                	jg     f0103eb9 <printnum+0x80>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0103ed5:	8b 4d 18             	mov    0x18(%ebp),%ecx
f0103ed8:	bb 00 00 00 00       	mov    $0x0,%ebx
f0103edd:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103ee0:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0103ee3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0103ee7:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
f0103eeb:	89 04 24             	mov    %eax,(%esp)
f0103eee:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103ef2:	e8 f9 0d 00 00       	call   f0104cf0 <__umoddi3>
f0103ef7:	05 c0 60 10 f0       	add    $0xf01060c0,%eax
f0103efc:	0f b6 00             	movzbl (%eax),%eax
f0103eff:	0f be c0             	movsbl %al,%eax
f0103f02:	8b 55 0c             	mov    0xc(%ebp),%edx
f0103f05:	89 54 24 04          	mov    %edx,0x4(%esp)
f0103f09:	89 04 24             	mov    %eax,(%esp)
f0103f0c:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f0f:	ff d0                	call   *%eax
}
f0103f11:	83 c4 34             	add    $0x34,%esp
f0103f14:	5b                   	pop    %ebx
f0103f15:	5d                   	pop    %ebp
f0103f16:	c3                   	ret    

f0103f17 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0103f17:	55                   	push   %ebp
f0103f18:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0103f1a:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
f0103f1e:	7e 1c                	jle    f0103f3c <getuint+0x25>
		return va_arg(*ap, unsigned long long);
f0103f20:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f23:	8b 00                	mov    (%eax),%eax
f0103f25:	8d 50 08             	lea    0x8(%eax),%edx
f0103f28:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f2b:	89 10                	mov    %edx,(%eax)
f0103f2d:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f30:	8b 00                	mov    (%eax),%eax
f0103f32:	83 e8 08             	sub    $0x8,%eax
f0103f35:	8b 50 04             	mov    0x4(%eax),%edx
f0103f38:	8b 00                	mov    (%eax),%eax
f0103f3a:	eb 40                	jmp    f0103f7c <getuint+0x65>
	else if (lflag)
f0103f3c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0103f40:	74 1e                	je     f0103f60 <getuint+0x49>
		return va_arg(*ap, unsigned long);
f0103f42:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f45:	8b 00                	mov    (%eax),%eax
f0103f47:	8d 50 04             	lea    0x4(%eax),%edx
f0103f4a:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f4d:	89 10                	mov    %edx,(%eax)
f0103f4f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f52:	8b 00                	mov    (%eax),%eax
f0103f54:	83 e8 04             	sub    $0x4,%eax
f0103f57:	8b 00                	mov    (%eax),%eax
f0103f59:	ba 00 00 00 00       	mov    $0x0,%edx
f0103f5e:	eb 1c                	jmp    f0103f7c <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
f0103f60:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f63:	8b 00                	mov    (%eax),%eax
f0103f65:	8d 50 04             	lea    0x4(%eax),%edx
f0103f68:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f6b:	89 10                	mov    %edx,(%eax)
f0103f6d:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f70:	8b 00                	mov    (%eax),%eax
f0103f72:	83 e8 04             	sub    $0x4,%eax
f0103f75:	8b 00                	mov    (%eax),%eax
f0103f77:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0103f7c:	5d                   	pop    %ebp
f0103f7d:	c3                   	ret    

f0103f7e <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
f0103f7e:	55                   	push   %ebp
f0103f7f:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0103f81:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
f0103f85:	7e 1c                	jle    f0103fa3 <getint+0x25>
		return va_arg(*ap, long long);
f0103f87:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f8a:	8b 00                	mov    (%eax),%eax
f0103f8c:	8d 50 08             	lea    0x8(%eax),%edx
f0103f8f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f92:	89 10                	mov    %edx,(%eax)
f0103f94:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f97:	8b 00                	mov    (%eax),%eax
f0103f99:	83 e8 08             	sub    $0x8,%eax
f0103f9c:	8b 50 04             	mov    0x4(%eax),%edx
f0103f9f:	8b 00                	mov    (%eax),%eax
f0103fa1:	eb 38                	jmp    f0103fdb <getint+0x5d>
	else if (lflag)
f0103fa3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0103fa7:	74 1a                	je     f0103fc3 <getint+0x45>
		return va_arg(*ap, long);
f0103fa9:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fac:	8b 00                	mov    (%eax),%eax
f0103fae:	8d 50 04             	lea    0x4(%eax),%edx
f0103fb1:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fb4:	89 10                	mov    %edx,(%eax)
f0103fb6:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fb9:	8b 00                	mov    (%eax),%eax
f0103fbb:	83 e8 04             	sub    $0x4,%eax
f0103fbe:	8b 00                	mov    (%eax),%eax
f0103fc0:	99                   	cltd   
f0103fc1:	eb 18                	jmp    f0103fdb <getint+0x5d>
	else
		return va_arg(*ap, int);
f0103fc3:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fc6:	8b 00                	mov    (%eax),%eax
f0103fc8:	8d 50 04             	lea    0x4(%eax),%edx
f0103fcb:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fce:	89 10                	mov    %edx,(%eax)
f0103fd0:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fd3:	8b 00                	mov    (%eax),%eax
f0103fd5:	83 e8 04             	sub    $0x4,%eax
f0103fd8:	8b 00                	mov    (%eax),%eax
f0103fda:	99                   	cltd   
}
f0103fdb:	5d                   	pop    %ebp
f0103fdc:	c3                   	ret    

f0103fdd <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f0103fdd:	55                   	push   %ebp
f0103fde:	89 e5                	mov    %esp,%ebp
f0103fe0:	56                   	push   %esi
f0103fe1:	53                   	push   %ebx
f0103fe2:	83 ec 40             	sub    $0x40,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0103fe5:	eb 18                	jmp    f0103fff <vprintfmt+0x22>
			if (ch == '\0')
f0103fe7:	85 db                	test   %ebx,%ebx
f0103fe9:	75 05                	jne    f0103ff0 <vprintfmt+0x13>
				return;
f0103feb:	e9 07 04 00 00       	jmp    f01043f7 <vprintfmt+0x41a>
			putch(ch, putdat);
f0103ff0:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ff3:	89 44 24 04          	mov    %eax,0x4(%esp)
f0103ff7:	89 1c 24             	mov    %ebx,(%esp)
f0103ffa:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ffd:	ff d0                	call   *%eax
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0103fff:	8b 45 10             	mov    0x10(%ebp),%eax
f0104002:	8d 50 01             	lea    0x1(%eax),%edx
f0104005:	89 55 10             	mov    %edx,0x10(%ebp)
f0104008:	0f b6 00             	movzbl (%eax),%eax
f010400b:	0f b6 d8             	movzbl %al,%ebx
f010400e:	83 fb 25             	cmp    $0x25,%ebx
f0104011:	75 d4                	jne    f0103fe7 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
f0104013:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
f0104017:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
f010401e:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
f0104025:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
f010402c:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0104033:	8b 45 10             	mov    0x10(%ebp),%eax
f0104036:	8d 50 01             	lea    0x1(%eax),%edx
f0104039:	89 55 10             	mov    %edx,0x10(%ebp)
f010403c:	0f b6 00             	movzbl (%eax),%eax
f010403f:	0f b6 d8             	movzbl %al,%ebx
f0104042:	8d 43 dd             	lea    -0x23(%ebx),%eax
f0104045:	83 f8 55             	cmp    $0x55,%eax
f0104048:	0f 87 78 03 00 00    	ja     f01043c6 <vprintfmt+0x3e9>
f010404e:	8b 04 85 e4 60 10 f0 	mov    -0xfef9f1c(,%eax,4),%eax
f0104055:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
f0104057:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
f010405b:	eb d6                	jmp    f0104033 <vprintfmt+0x56>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f010405d:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
f0104061:	eb d0                	jmp    f0104033 <vprintfmt+0x56>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0104063:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
f010406a:	8b 55 e0             	mov    -0x20(%ebp),%edx
f010406d:	89 d0                	mov    %edx,%eax
f010406f:	c1 e0 02             	shl    $0x2,%eax
f0104072:	01 d0                	add    %edx,%eax
f0104074:	01 c0                	add    %eax,%eax
f0104076:	01 d8                	add    %ebx,%eax
f0104078:	83 e8 30             	sub    $0x30,%eax
f010407b:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
f010407e:	8b 45 10             	mov    0x10(%ebp),%eax
f0104081:	0f b6 00             	movzbl (%eax),%eax
f0104084:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
f0104087:	83 fb 2f             	cmp    $0x2f,%ebx
f010408a:	7e 0b                	jle    f0104097 <vprintfmt+0xba>
f010408c:	83 fb 39             	cmp    $0x39,%ebx
f010408f:	7f 06                	jg     f0104097 <vprintfmt+0xba>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0104091:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f0104095:	eb d3                	jmp    f010406a <vprintfmt+0x8d>
			goto process_precision;
f0104097:	eb 39                	jmp    f01040d2 <vprintfmt+0xf5>

		case '*':
			precision = va_arg(ap, int);
f0104099:	8b 45 14             	mov    0x14(%ebp),%eax
f010409c:	83 c0 04             	add    $0x4,%eax
f010409f:	89 45 14             	mov    %eax,0x14(%ebp)
f01040a2:	8b 45 14             	mov    0x14(%ebp),%eax
f01040a5:	83 e8 04             	sub    $0x4,%eax
f01040a8:	8b 00                	mov    (%eax),%eax
f01040aa:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
f01040ad:	eb 23                	jmp    f01040d2 <vprintfmt+0xf5>

		case '.':
			if (width < 0)
f01040af:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01040b3:	79 0c                	jns    f01040c1 <vprintfmt+0xe4>
				width = 0;
f01040b5:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
f01040bc:	e9 72 ff ff ff       	jmp    f0104033 <vprintfmt+0x56>
f01040c1:	e9 6d ff ff ff       	jmp    f0104033 <vprintfmt+0x56>

		case '#':
			altflag = 1;
f01040c6:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
f01040cd:	e9 61 ff ff ff       	jmp    f0104033 <vprintfmt+0x56>

		process_precision:
			if (width < 0)
f01040d2:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01040d6:	79 12                	jns    f01040ea <vprintfmt+0x10d>
				width = precision, precision = -1;
f01040d8:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01040db:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01040de:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
f01040e5:	e9 49 ff ff ff       	jmp    f0104033 <vprintfmt+0x56>
f01040ea:	e9 44 ff ff ff       	jmp    f0104033 <vprintfmt+0x56>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01040ef:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
f01040f3:	e9 3b ff ff ff       	jmp    f0104033 <vprintfmt+0x56>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f01040f8:	8b 45 14             	mov    0x14(%ebp),%eax
f01040fb:	83 c0 04             	add    $0x4,%eax
f01040fe:	89 45 14             	mov    %eax,0x14(%ebp)
f0104101:	8b 45 14             	mov    0x14(%ebp),%eax
f0104104:	83 e8 04             	sub    $0x4,%eax
f0104107:	8b 00                	mov    (%eax),%eax
f0104109:	8b 55 0c             	mov    0xc(%ebp),%edx
f010410c:	89 54 24 04          	mov    %edx,0x4(%esp)
f0104110:	89 04 24             	mov    %eax,(%esp)
f0104113:	8b 45 08             	mov    0x8(%ebp),%eax
f0104116:	ff d0                	call   *%eax
			break;
f0104118:	e9 d4 02 00 00       	jmp    f01043f1 <vprintfmt+0x414>

		// error message
		case 'e':
			err = va_arg(ap, int);
f010411d:	8b 45 14             	mov    0x14(%ebp),%eax
f0104120:	83 c0 04             	add    $0x4,%eax
f0104123:	89 45 14             	mov    %eax,0x14(%ebp)
f0104126:	8b 45 14             	mov    0x14(%ebp),%eax
f0104129:	83 e8 04             	sub    $0x4,%eax
f010412c:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
f010412e:	85 db                	test   %ebx,%ebx
f0104130:	79 02                	jns    f0104134 <vprintfmt+0x157>
				err = -err;
f0104132:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
f0104134:	83 fb 07             	cmp    $0x7,%ebx
f0104137:	7f 0b                	jg     f0104144 <vprintfmt+0x167>
f0104139:	8b 34 9d a0 60 10 f0 	mov    -0xfef9f60(,%ebx,4),%esi
f0104140:	85 f6                	test   %esi,%esi
f0104142:	75 23                	jne    f0104167 <vprintfmt+0x18a>
				printfmt(putch, putdat, "error %d", err);
f0104144:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
f0104148:	c7 44 24 08 d1 60 10 	movl   $0xf01060d1,0x8(%esp)
f010414f:	f0 
f0104150:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104153:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104157:	8b 45 08             	mov    0x8(%ebp),%eax
f010415a:	89 04 24             	mov    %eax,(%esp)
f010415d:	e8 9c 02 00 00       	call   f01043fe <printfmt>
			else
				printfmt(putch, putdat, "%s", p);
			break;
f0104162:	e9 8a 02 00 00       	jmp    f01043f1 <vprintfmt+0x414>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
f0104167:	89 74 24 0c          	mov    %esi,0xc(%esp)
f010416b:	c7 44 24 08 da 60 10 	movl   $0xf01060da,0x8(%esp)
f0104172:	f0 
f0104173:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104176:	89 44 24 04          	mov    %eax,0x4(%esp)
f010417a:	8b 45 08             	mov    0x8(%ebp),%eax
f010417d:	89 04 24             	mov    %eax,(%esp)
f0104180:	e8 79 02 00 00       	call   f01043fe <printfmt>
			break;
f0104185:	e9 67 02 00 00       	jmp    f01043f1 <vprintfmt+0x414>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f010418a:	8b 45 14             	mov    0x14(%ebp),%eax
f010418d:	83 c0 04             	add    $0x4,%eax
f0104190:	89 45 14             	mov    %eax,0x14(%ebp)
f0104193:	8b 45 14             	mov    0x14(%ebp),%eax
f0104196:	83 e8 04             	sub    $0x4,%eax
f0104199:	8b 30                	mov    (%eax),%esi
f010419b:	85 f6                	test   %esi,%esi
f010419d:	75 05                	jne    f01041a4 <vprintfmt+0x1c7>
				p = "(null)";
f010419f:	be dd 60 10 f0       	mov    $0xf01060dd,%esi
			if (width > 0 && padc != '-')
f01041a4:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01041a8:	7e 37                	jle    f01041e1 <vprintfmt+0x204>
f01041aa:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
f01041ae:	74 31                	je     f01041e1 <vprintfmt+0x204>
				for (width -= strnlen(p, precision); width > 0; width--)
f01041b0:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01041b3:	89 44 24 04          	mov    %eax,0x4(%esp)
f01041b7:	89 34 24             	mov    %esi,(%esp)
f01041ba:	e8 53 04 00 00       	call   f0104612 <strnlen>
f01041bf:	29 45 e4             	sub    %eax,-0x1c(%ebp)
f01041c2:	eb 17                	jmp    f01041db <vprintfmt+0x1fe>
					putch(padc, putdat);
f01041c4:	0f be 45 db          	movsbl -0x25(%ebp),%eax
f01041c8:	8b 55 0c             	mov    0xc(%ebp),%edx
f01041cb:	89 54 24 04          	mov    %edx,0x4(%esp)
f01041cf:	89 04 24             	mov    %eax,(%esp)
f01041d2:	8b 45 08             	mov    0x8(%ebp),%eax
f01041d5:	ff d0                	call   *%eax
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f01041d7:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f01041db:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01041df:	7f e3                	jg     f01041c4 <vprintfmt+0x1e7>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f01041e1:	eb 38                	jmp    f010421b <vprintfmt+0x23e>
				if (altflag && (ch < ' ' || ch > '~'))
f01041e3:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f01041e7:	74 1f                	je     f0104208 <vprintfmt+0x22b>
f01041e9:	83 fb 1f             	cmp    $0x1f,%ebx
f01041ec:	7e 05                	jle    f01041f3 <vprintfmt+0x216>
f01041ee:	83 fb 7e             	cmp    $0x7e,%ebx
f01041f1:	7e 15                	jle    f0104208 <vprintfmt+0x22b>
					putch('?', putdat);
f01041f3:	8b 45 0c             	mov    0xc(%ebp),%eax
f01041f6:	89 44 24 04          	mov    %eax,0x4(%esp)
f01041fa:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
f0104201:	8b 45 08             	mov    0x8(%ebp),%eax
f0104204:	ff d0                	call   *%eax
f0104206:	eb 0f                	jmp    f0104217 <vprintfmt+0x23a>
				else
					putch(ch, putdat);
f0104208:	8b 45 0c             	mov    0xc(%ebp),%eax
f010420b:	89 44 24 04          	mov    %eax,0x4(%esp)
f010420f:	89 1c 24             	mov    %ebx,(%esp)
f0104212:	8b 45 08             	mov    0x8(%ebp),%eax
f0104215:	ff d0                	call   *%eax
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f0104217:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f010421b:	89 f0                	mov    %esi,%eax
f010421d:	8d 70 01             	lea    0x1(%eax),%esi
f0104220:	0f b6 00             	movzbl (%eax),%eax
f0104223:	0f be d8             	movsbl %al,%ebx
f0104226:	85 db                	test   %ebx,%ebx
f0104228:	74 10                	je     f010423a <vprintfmt+0x25d>
f010422a:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f010422e:	78 b3                	js     f01041e3 <vprintfmt+0x206>
f0104230:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
f0104234:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f0104238:	79 a9                	jns    f01041e3 <vprintfmt+0x206>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f010423a:	eb 17                	jmp    f0104253 <vprintfmt+0x276>
				putch(' ', putdat);
f010423c:	8b 45 0c             	mov    0xc(%ebp),%eax
f010423f:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104243:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
f010424a:	8b 45 08             	mov    0x8(%ebp),%eax
f010424d:	ff d0                	call   *%eax
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f010424f:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f0104253:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104257:	7f e3                	jg     f010423c <vprintfmt+0x25f>
				putch(' ', putdat);
			break;
f0104259:	e9 93 01 00 00       	jmp    f01043f1 <vprintfmt+0x414>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f010425e:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0104261:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104265:	8d 45 14             	lea    0x14(%ebp),%eax
f0104268:	89 04 24             	mov    %eax,(%esp)
f010426b:	e8 0e fd ff ff       	call   f0103f7e <getint>
f0104270:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104273:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
f0104276:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0104279:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010427c:	85 d2                	test   %edx,%edx
f010427e:	79 26                	jns    f01042a6 <vprintfmt+0x2c9>
				putch('-', putdat);
f0104280:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104283:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104287:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
f010428e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104291:	ff d0                	call   *%eax
				num = -(long long) num;
f0104293:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0104296:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0104299:	f7 d8                	neg    %eax
f010429b:	83 d2 00             	adc    $0x0,%edx
f010429e:	f7 da                	neg    %edx
f01042a0:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01042a3:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
f01042a6:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
f01042ad:	e9 cb 00 00 00       	jmp    f010437d <vprintfmt+0x3a0>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f01042b2:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01042b5:	89 44 24 04          	mov    %eax,0x4(%esp)
f01042b9:	8d 45 14             	lea    0x14(%ebp),%eax
f01042bc:	89 04 24             	mov    %eax,(%esp)
f01042bf:	e8 53 fc ff ff       	call   f0103f17 <getuint>
f01042c4:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01042c7:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
f01042ca:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
f01042d1:	e9 a7 00 00 00       	jmp    f010437d <vprintfmt+0x3a0>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
f01042d6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01042d9:	89 44 24 04          	mov    %eax,0x4(%esp)
f01042dd:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f01042e4:	8b 45 08             	mov    0x8(%ebp),%eax
f01042e7:	ff d0                	call   *%eax
			putch('X', putdat);
f01042e9:	8b 45 0c             	mov    0xc(%ebp),%eax
f01042ec:	89 44 24 04          	mov    %eax,0x4(%esp)
f01042f0:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f01042f7:	8b 45 08             	mov    0x8(%ebp),%eax
f01042fa:	ff d0                	call   *%eax
			putch('X', putdat);
f01042fc:	8b 45 0c             	mov    0xc(%ebp),%eax
f01042ff:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104303:	c7 04 24 58 00 00 00 	movl   $0x58,(%esp)
f010430a:	8b 45 08             	mov    0x8(%ebp),%eax
f010430d:	ff d0                	call   *%eax
			break;
f010430f:	e9 dd 00 00 00       	jmp    f01043f1 <vprintfmt+0x414>

		// pointer
		case 'p':
			putch('0', putdat);
f0104314:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104317:	89 44 24 04          	mov    %eax,0x4(%esp)
f010431b:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
f0104322:	8b 45 08             	mov    0x8(%ebp),%eax
f0104325:	ff d0                	call   *%eax
			putch('x', putdat);
f0104327:	8b 45 0c             	mov    0xc(%ebp),%eax
f010432a:	89 44 24 04          	mov    %eax,0x4(%esp)
f010432e:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
f0104335:	8b 45 08             	mov    0x8(%ebp),%eax
f0104338:	ff d0                	call   *%eax
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
f010433a:	8b 45 14             	mov    0x14(%ebp),%eax
f010433d:	83 c0 04             	add    $0x4,%eax
f0104340:	89 45 14             	mov    %eax,0x14(%ebp)
f0104343:	8b 45 14             	mov    0x14(%ebp),%eax
f0104346:	83 e8 04             	sub    $0x4,%eax
f0104349:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f010434b:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010434e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
f0104355:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
f010435c:	eb 1f                	jmp    f010437d <vprintfmt+0x3a0>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f010435e:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0104361:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104365:	8d 45 14             	lea    0x14(%ebp),%eax
f0104368:	89 04 24             	mov    %eax,(%esp)
f010436b:	e8 a7 fb ff ff       	call   f0103f17 <getuint>
f0104370:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104373:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
f0104376:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
f010437d:	0f be 55 db          	movsbl -0x25(%ebp),%edx
f0104381:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104384:	89 54 24 18          	mov    %edx,0x18(%esp)
f0104388:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f010438b:	89 54 24 14          	mov    %edx,0x14(%esp)
f010438f:	89 44 24 10          	mov    %eax,0x10(%esp)
f0104393:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0104396:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0104399:	89 44 24 08          	mov    %eax,0x8(%esp)
f010439d:	89 54 24 0c          	mov    %edx,0xc(%esp)
f01043a1:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043a4:	89 44 24 04          	mov    %eax,0x4(%esp)
f01043a8:	8b 45 08             	mov    0x8(%ebp),%eax
f01043ab:	89 04 24             	mov    %eax,(%esp)
f01043ae:	e8 86 fa ff ff       	call   f0103e39 <printnum>
			break;
f01043b3:	eb 3c                	jmp    f01043f1 <vprintfmt+0x414>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f01043b5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043b8:	89 44 24 04          	mov    %eax,0x4(%esp)
f01043bc:	89 1c 24             	mov    %ebx,(%esp)
f01043bf:	8b 45 08             	mov    0x8(%ebp),%eax
f01043c2:	ff d0                	call   *%eax
			break;
f01043c4:	eb 2b                	jmp    f01043f1 <vprintfmt+0x414>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f01043c6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043c9:	89 44 24 04          	mov    %eax,0x4(%esp)
f01043cd:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
f01043d4:	8b 45 08             	mov    0x8(%ebp),%eax
f01043d7:	ff d0                	call   *%eax
			for (fmt--; fmt[-1] != '%'; fmt--)
f01043d9:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f01043dd:	eb 04                	jmp    f01043e3 <vprintfmt+0x406>
f01043df:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f01043e3:	8b 45 10             	mov    0x10(%ebp),%eax
f01043e6:	83 e8 01             	sub    $0x1,%eax
f01043e9:	0f b6 00             	movzbl (%eax),%eax
f01043ec:	3c 25                	cmp    $0x25,%al
f01043ee:	75 ef                	jne    f01043df <vprintfmt+0x402>
				/* do nothing */;
			break;
f01043f0:	90                   	nop
		}
	}
f01043f1:	90                   	nop
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f01043f2:	e9 08 fc ff ff       	jmp    f0103fff <vprintfmt+0x22>
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
f01043f7:	83 c4 40             	add    $0x40,%esp
f01043fa:	5b                   	pop    %ebx
f01043fb:	5e                   	pop    %esi
f01043fc:	5d                   	pop    %ebp
f01043fd:	c3                   	ret    

f01043fe <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f01043fe:	55                   	push   %ebp
f01043ff:	89 e5                	mov    %esp,%ebp
f0104401:	83 ec 28             	sub    $0x28,%esp
	va_list ap;

	va_start(ap, fmt);
f0104404:	8d 45 10             	lea    0x10(%ebp),%eax
f0104407:	83 c0 04             	add    $0x4,%eax
f010440a:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
f010440d:	8b 45 10             	mov    0x10(%ebp),%eax
f0104410:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0104413:	89 54 24 0c          	mov    %edx,0xc(%esp)
f0104417:	89 44 24 08          	mov    %eax,0x8(%esp)
f010441b:	8b 45 0c             	mov    0xc(%ebp),%eax
f010441e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104422:	8b 45 08             	mov    0x8(%ebp),%eax
f0104425:	89 04 24             	mov    %eax,(%esp)
f0104428:	e8 b0 fb ff ff       	call   f0103fdd <vprintfmt>
	va_end(ap);
}
f010442d:	c9                   	leave  
f010442e:	c3                   	ret    

f010442f <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f010442f:	55                   	push   %ebp
f0104430:	89 e5                	mov    %esp,%ebp
	b->cnt++;
f0104432:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104435:	8b 40 08             	mov    0x8(%eax),%eax
f0104438:	8d 50 01             	lea    0x1(%eax),%edx
f010443b:	8b 45 0c             	mov    0xc(%ebp),%eax
f010443e:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
f0104441:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104444:	8b 10                	mov    (%eax),%edx
f0104446:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104449:	8b 40 04             	mov    0x4(%eax),%eax
f010444c:	39 c2                	cmp    %eax,%edx
f010444e:	73 12                	jae    f0104462 <sprintputch+0x33>
		*b->buf++ = ch;
f0104450:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104453:	8b 00                	mov    (%eax),%eax
f0104455:	8d 48 01             	lea    0x1(%eax),%ecx
f0104458:	8b 55 0c             	mov    0xc(%ebp),%edx
f010445b:	89 0a                	mov    %ecx,(%edx)
f010445d:	8b 55 08             	mov    0x8(%ebp),%edx
f0104460:	88 10                	mov    %dl,(%eax)
}
f0104462:	5d                   	pop    %ebp
f0104463:	c3                   	ret    

f0104464 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0104464:	55                   	push   %ebp
f0104465:	89 e5                	mov    %esp,%ebp
f0104467:	83 ec 28             	sub    $0x28,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
f010446a:	8b 45 08             	mov    0x8(%ebp),%eax
f010446d:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0104470:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104473:	8d 50 ff             	lea    -0x1(%eax),%edx
f0104476:	8b 45 08             	mov    0x8(%ebp),%eax
f0104479:	01 d0                	add    %edx,%eax
f010447b:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010447e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f0104485:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0104489:	74 06                	je     f0104491 <vsnprintf+0x2d>
f010448b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f010448f:	7f 07                	jg     f0104498 <vsnprintf+0x34>
		return -E_INVAL;
f0104491:	b8 03 00 00 00       	mov    $0x3,%eax
f0104496:	eb 2a                	jmp    f01044c2 <vsnprintf+0x5e>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f0104498:	8b 45 14             	mov    0x14(%ebp),%eax
f010449b:	89 44 24 0c          	mov    %eax,0xc(%esp)
f010449f:	8b 45 10             	mov    0x10(%ebp),%eax
f01044a2:	89 44 24 08          	mov    %eax,0x8(%esp)
f01044a6:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01044a9:	89 44 24 04          	mov    %eax,0x4(%esp)
f01044ad:	c7 04 24 2f 44 10 f0 	movl   $0xf010442f,(%esp)
f01044b4:	e8 24 fb ff ff       	call   f0103fdd <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f01044b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01044bc:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f01044bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f01044c2:	c9                   	leave  
f01044c3:	c3                   	ret    

f01044c4 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f01044c4:	55                   	push   %ebp
f01044c5:	89 e5                	mov    %esp,%ebp
f01044c7:	83 ec 28             	sub    $0x28,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f01044ca:	8d 45 10             	lea    0x10(%ebp),%eax
f01044cd:	83 c0 04             	add    $0x4,%eax
f01044d0:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
f01044d3:	8b 45 10             	mov    0x10(%ebp),%eax
f01044d6:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01044d9:	89 54 24 0c          	mov    %edx,0xc(%esp)
f01044dd:	89 44 24 08          	mov    %eax,0x8(%esp)
f01044e1:	8b 45 0c             	mov    0xc(%ebp),%eax
f01044e4:	89 44 24 04          	mov    %eax,0x4(%esp)
f01044e8:	8b 45 08             	mov    0x8(%ebp),%eax
f01044eb:	89 04 24             	mov    %eax,(%esp)
f01044ee:	e8 71 ff ff ff       	call   f0104464 <vsnprintf>
f01044f3:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
f01044f6:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
f01044f9:	c9                   	leave  
f01044fa:	c3                   	ret    

f01044fb <readline>:

#define BUFLEN 1024
//static char buf[BUFLEN];

void readline(const char *prompt, char* buf)
{
f01044fb:	55                   	push   %ebp
f01044fc:	89 e5                	mov    %esp,%ebp
f01044fe:	83 ec 28             	sub    $0x28,%esp
	int i, c, echoing;
	
	if (prompt != NULL)
f0104501:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0104505:	74 13                	je     f010451a <readline+0x1f>
		cprintf("%s", prompt);
f0104507:	8b 45 08             	mov    0x8(%ebp),%eax
f010450a:	89 44 24 04          	mov    %eax,0x4(%esp)
f010450e:	c7 04 24 3c 62 10 f0 	movl   $0xf010623c,(%esp)
f0104515:	e8 f1 ea ff ff       	call   f010300b <cprintf>

	
	i = 0;
f010451a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	echoing = iscons(0);	
f0104521:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
f0104528:	e8 26 c4 ff ff       	call   f0100953 <iscons>
f010452d:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (1) {
		c = getchar();
f0104530:	e8 05 c4 ff ff       	call   f010093a <getchar>
f0104535:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if (c < 0) {
f0104538:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f010453c:	79 23                	jns    f0104561 <readline+0x66>
			if (c != -E_EOF)
f010453e:	83 7d ec 07          	cmpl   $0x7,-0x14(%ebp)
f0104542:	74 18                	je     f010455c <readline+0x61>
				cprintf("read error: %e\n", c);			
f0104544:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104547:	89 44 24 04          	mov    %eax,0x4(%esp)
f010454b:	c7 04 24 3f 62 10 f0 	movl   $0xf010623f,(%esp)
f0104552:	e8 b4 ea ff ff       	call   f010300b <cprintf>
			return;
f0104557:	e9 8e 00 00 00       	jmp    f01045ea <readline+0xef>
f010455c:	e9 89 00 00 00       	jmp    f01045ea <readline+0xef>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0104561:	83 7d ec 1f          	cmpl   $0x1f,-0x14(%ebp)
f0104565:	7e 31                	jle    f0104598 <readline+0x9d>
f0104567:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
f010456e:	7f 28                	jg     f0104598 <readline+0x9d>
			if (echoing)
f0104570:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f0104574:	74 0b                	je     f0104581 <readline+0x86>
				cputchar(c);
f0104576:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104579:	89 04 24             	mov    %eax,(%esp)
f010457c:	e8 a6 c3 ff ff       	call   f0100927 <cputchar>
			buf[i++] = c;
f0104581:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0104584:	8d 50 01             	lea    0x1(%eax),%edx
f0104587:	89 55 f4             	mov    %edx,-0xc(%ebp)
f010458a:	89 c2                	mov    %eax,%edx
f010458c:	8b 45 0c             	mov    0xc(%ebp),%eax
f010458f:	01 c2                	add    %eax,%edx
f0104591:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104594:	88 02                	mov    %al,(%edx)
f0104596:	eb 4d                	jmp    f01045e5 <readline+0xea>
		} else if (c == '\b' && i > 0) {
f0104598:	83 7d ec 08          	cmpl   $0x8,-0x14(%ebp)
f010459c:	75 1d                	jne    f01045bb <readline+0xc0>
f010459e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01045a2:	7e 17                	jle    f01045bb <readline+0xc0>
			if (echoing)
f01045a4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f01045a8:	74 0b                	je     f01045b5 <readline+0xba>
				cputchar(c);
f01045aa:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01045ad:	89 04 24             	mov    %eax,(%esp)
f01045b0:	e8 72 c3 ff ff       	call   f0100927 <cputchar>
			i--;
f01045b5:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
f01045b9:	eb 2a                	jmp    f01045e5 <readline+0xea>
		} else if (c == '\n' || c == '\r') {
f01045bb:	83 7d ec 0a          	cmpl   $0xa,-0x14(%ebp)
f01045bf:	74 06                	je     f01045c7 <readline+0xcc>
f01045c1:	83 7d ec 0d          	cmpl   $0xd,-0x14(%ebp)
f01045c5:	75 1e                	jne    f01045e5 <readline+0xea>
			if (echoing)
f01045c7:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f01045cb:	74 0b                	je     f01045d8 <readline+0xdd>
				cputchar(c);
f01045cd:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01045d0:	89 04 24             	mov    %eax,(%esp)
f01045d3:	e8 4f c3 ff ff       	call   f0100927 <cputchar>
			buf[i] = 0;	
f01045d8:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01045db:	8b 45 0c             	mov    0xc(%ebp),%eax
f01045de:	01 d0                	add    %edx,%eax
f01045e0:	c6 00 00             	movb   $0x0,(%eax)
			return;		
f01045e3:	eb 05                	jmp    f01045ea <readline+0xef>
		}
	}
f01045e5:	e9 46 ff ff ff       	jmp    f0104530 <readline+0x35>
}
f01045ea:	c9                   	leave  
f01045eb:	c3                   	ret    

f01045ec <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
f01045ec:	55                   	push   %ebp
f01045ed:	89 e5                	mov    %esp,%ebp
f01045ef:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
f01045f2:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f01045f9:	eb 08                	jmp    f0104603 <strlen+0x17>
		n++;
f01045fb:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f01045ff:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104603:	8b 45 08             	mov    0x8(%ebp),%eax
f0104606:	0f b6 00             	movzbl (%eax),%eax
f0104609:	84 c0                	test   %al,%al
f010460b:	75 ee                	jne    f01045fb <strlen+0xf>
		n++;
	return n;
f010460d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f0104610:	c9                   	leave  
f0104611:	c3                   	ret    

f0104612 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
f0104612:	55                   	push   %ebp
f0104613:	89 e5                	mov    %esp,%ebp
f0104615:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0104618:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f010461f:	eb 0c                	jmp    f010462d <strnlen+0x1b>
		n++;
f0104621:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0104625:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104629:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
f010462d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104631:	74 0a                	je     f010463d <strnlen+0x2b>
f0104633:	8b 45 08             	mov    0x8(%ebp),%eax
f0104636:	0f b6 00             	movzbl (%eax),%eax
f0104639:	84 c0                	test   %al,%al
f010463b:	75 e4                	jne    f0104621 <strnlen+0xf>
		n++;
	return n;
f010463d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f0104640:	c9                   	leave  
f0104641:	c3                   	ret    

f0104642 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f0104642:	55                   	push   %ebp
f0104643:	89 e5                	mov    %esp,%ebp
f0104645:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
f0104648:	8b 45 08             	mov    0x8(%ebp),%eax
f010464b:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
f010464e:	90                   	nop
f010464f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104652:	8d 50 01             	lea    0x1(%eax),%edx
f0104655:	89 55 08             	mov    %edx,0x8(%ebp)
f0104658:	8b 55 0c             	mov    0xc(%ebp),%edx
f010465b:	8d 4a 01             	lea    0x1(%edx),%ecx
f010465e:	89 4d 0c             	mov    %ecx,0xc(%ebp)
f0104661:	0f b6 12             	movzbl (%edx),%edx
f0104664:	88 10                	mov    %dl,(%eax)
f0104666:	0f b6 00             	movzbl (%eax),%eax
f0104669:	84 c0                	test   %al,%al
f010466b:	75 e2                	jne    f010464f <strcpy+0xd>
		/* do nothing */;
	return ret;
f010466d:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f0104670:	c9                   	leave  
f0104671:	c3                   	ret    

f0104672 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
f0104672:	55                   	push   %ebp
f0104673:	89 e5                	mov    %esp,%ebp
f0104675:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
f0104678:	8b 45 08             	mov    0x8(%ebp),%eax
f010467b:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
f010467e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f0104685:	eb 23                	jmp    f01046aa <strncpy+0x38>
		*dst++ = *src;
f0104687:	8b 45 08             	mov    0x8(%ebp),%eax
f010468a:	8d 50 01             	lea    0x1(%eax),%edx
f010468d:	89 55 08             	mov    %edx,0x8(%ebp)
f0104690:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104693:	0f b6 12             	movzbl (%edx),%edx
f0104696:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
f0104698:	8b 45 0c             	mov    0xc(%ebp),%eax
f010469b:	0f b6 00             	movzbl (%eax),%eax
f010469e:	84 c0                	test   %al,%al
f01046a0:	74 04                	je     f01046a6 <strncpy+0x34>
			src++;
f01046a2:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f01046a6:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f01046aa:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01046ad:	3b 45 10             	cmp    0x10(%ebp),%eax
f01046b0:	72 d5                	jb     f0104687 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
f01046b2:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f01046b5:	c9                   	leave  
f01046b6:	c3                   	ret    

f01046b7 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
f01046b7:	55                   	push   %ebp
f01046b8:	89 e5                	mov    %esp,%ebp
f01046ba:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
f01046bd:	8b 45 08             	mov    0x8(%ebp),%eax
f01046c0:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
f01046c3:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01046c7:	74 33                	je     f01046fc <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
f01046c9:	eb 17                	jmp    f01046e2 <strlcpy+0x2b>
			*dst++ = *src++;
f01046cb:	8b 45 08             	mov    0x8(%ebp),%eax
f01046ce:	8d 50 01             	lea    0x1(%eax),%edx
f01046d1:	89 55 08             	mov    %edx,0x8(%ebp)
f01046d4:	8b 55 0c             	mov    0xc(%ebp),%edx
f01046d7:	8d 4a 01             	lea    0x1(%edx),%ecx
f01046da:	89 4d 0c             	mov    %ecx,0xc(%ebp)
f01046dd:	0f b6 12             	movzbl (%edx),%edx
f01046e0:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f01046e2:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f01046e6:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01046ea:	74 0a                	je     f01046f6 <strlcpy+0x3f>
f01046ec:	8b 45 0c             	mov    0xc(%ebp),%eax
f01046ef:	0f b6 00             	movzbl (%eax),%eax
f01046f2:	84 c0                	test   %al,%al
f01046f4:	75 d5                	jne    f01046cb <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
f01046f6:	8b 45 08             	mov    0x8(%ebp),%eax
f01046f9:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
f01046fc:	8b 55 08             	mov    0x8(%ebp),%edx
f01046ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104702:	29 c2                	sub    %eax,%edx
f0104704:	89 d0                	mov    %edx,%eax
}
f0104706:	c9                   	leave  
f0104707:	c3                   	ret    

f0104708 <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0104708:	55                   	push   %ebp
f0104709:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
f010470b:	eb 08                	jmp    f0104715 <strcmp+0xd>
		p++, q++;
f010470d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104711:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0104715:	8b 45 08             	mov    0x8(%ebp),%eax
f0104718:	0f b6 00             	movzbl (%eax),%eax
f010471b:	84 c0                	test   %al,%al
f010471d:	74 10                	je     f010472f <strcmp+0x27>
f010471f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104722:	0f b6 10             	movzbl (%eax),%edx
f0104725:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104728:	0f b6 00             	movzbl (%eax),%eax
f010472b:	38 c2                	cmp    %al,%dl
f010472d:	74 de                	je     f010470d <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f010472f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104732:	0f b6 00             	movzbl (%eax),%eax
f0104735:	0f b6 d0             	movzbl %al,%edx
f0104738:	8b 45 0c             	mov    0xc(%ebp),%eax
f010473b:	0f b6 00             	movzbl (%eax),%eax
f010473e:	0f b6 c0             	movzbl %al,%eax
f0104741:	29 c2                	sub    %eax,%edx
f0104743:	89 d0                	mov    %edx,%eax
}
f0104745:	5d                   	pop    %ebp
f0104746:	c3                   	ret    

f0104747 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
f0104747:	55                   	push   %ebp
f0104748:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
f010474a:	eb 0c                	jmp    f0104758 <strncmp+0x11>
		n--, p++, q++;
f010474c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f0104750:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104754:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
f0104758:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f010475c:	74 1a                	je     f0104778 <strncmp+0x31>
f010475e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104761:	0f b6 00             	movzbl (%eax),%eax
f0104764:	84 c0                	test   %al,%al
f0104766:	74 10                	je     f0104778 <strncmp+0x31>
f0104768:	8b 45 08             	mov    0x8(%ebp),%eax
f010476b:	0f b6 10             	movzbl (%eax),%edx
f010476e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104771:	0f b6 00             	movzbl (%eax),%eax
f0104774:	38 c2                	cmp    %al,%dl
f0104776:	74 d4                	je     f010474c <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
f0104778:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f010477c:	75 07                	jne    f0104785 <strncmp+0x3e>
		return 0;
f010477e:	b8 00 00 00 00       	mov    $0x0,%eax
f0104783:	eb 16                	jmp    f010479b <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0104785:	8b 45 08             	mov    0x8(%ebp),%eax
f0104788:	0f b6 00             	movzbl (%eax),%eax
f010478b:	0f b6 d0             	movzbl %al,%edx
f010478e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104791:	0f b6 00             	movzbl (%eax),%eax
f0104794:	0f b6 c0             	movzbl %al,%eax
f0104797:	29 c2                	sub    %eax,%edx
f0104799:	89 d0                	mov    %edx,%eax
}
f010479b:	5d                   	pop    %ebp
f010479c:	c3                   	ret    

f010479d <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f010479d:	55                   	push   %ebp
f010479e:	89 e5                	mov    %esp,%ebp
f01047a0:	83 ec 04             	sub    $0x4,%esp
f01047a3:	8b 45 0c             	mov    0xc(%ebp),%eax
f01047a6:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
f01047a9:	eb 14                	jmp    f01047bf <strchr+0x22>
		if (*s == c)
f01047ab:	8b 45 08             	mov    0x8(%ebp),%eax
f01047ae:	0f b6 00             	movzbl (%eax),%eax
f01047b1:	3a 45 fc             	cmp    -0x4(%ebp),%al
f01047b4:	75 05                	jne    f01047bb <strchr+0x1e>
			return (char *) s;
f01047b6:	8b 45 08             	mov    0x8(%ebp),%eax
f01047b9:	eb 13                	jmp    f01047ce <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f01047bb:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01047bf:	8b 45 08             	mov    0x8(%ebp),%eax
f01047c2:	0f b6 00             	movzbl (%eax),%eax
f01047c5:	84 c0                	test   %al,%al
f01047c7:	75 e2                	jne    f01047ab <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
f01047c9:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01047ce:	c9                   	leave  
f01047cf:	c3                   	ret    

f01047d0 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f01047d0:	55                   	push   %ebp
f01047d1:	89 e5                	mov    %esp,%ebp
f01047d3:	83 ec 04             	sub    $0x4,%esp
f01047d6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01047d9:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
f01047dc:	eb 11                	jmp    f01047ef <strfind+0x1f>
		if (*s == c)
f01047de:	8b 45 08             	mov    0x8(%ebp),%eax
f01047e1:	0f b6 00             	movzbl (%eax),%eax
f01047e4:	3a 45 fc             	cmp    -0x4(%ebp),%al
f01047e7:	75 02                	jne    f01047eb <strfind+0x1b>
			break;
f01047e9:	eb 0e                	jmp    f01047f9 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f01047eb:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01047ef:	8b 45 08             	mov    0x8(%ebp),%eax
f01047f2:	0f b6 00             	movzbl (%eax),%eax
f01047f5:	84 c0                	test   %al,%al
f01047f7:	75 e5                	jne    f01047de <strfind+0xe>
		if (*s == c)
			break;
	return (char *) s;
f01047f9:	8b 45 08             	mov    0x8(%ebp),%eax
}
f01047fc:	c9                   	leave  
f01047fd:	c3                   	ret    

f01047fe <memset>:


void *
memset(void *v, int c, uint32 n)
{
f01047fe:	55                   	push   %ebp
f01047ff:	89 e5                	mov    %esp,%ebp
f0104801:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
f0104804:	8b 45 08             	mov    0x8(%ebp),%eax
f0104807:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
f010480a:	8b 45 10             	mov    0x10(%ebp),%eax
f010480d:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
f0104810:	eb 0e                	jmp    f0104820 <memset+0x22>
		*p++ = c;
f0104812:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104815:	8d 50 01             	lea    0x1(%eax),%edx
f0104818:	89 55 fc             	mov    %edx,-0x4(%ebp)
f010481b:	8b 55 0c             	mov    0xc(%ebp),%edx
f010481e:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
f0104820:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
f0104824:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
f0104828:	79 e8                	jns    f0104812 <memset+0x14>
		*p++ = c;

	return v;
f010482a:	8b 45 08             	mov    0x8(%ebp),%eax
}
f010482d:	c9                   	leave  
f010482e:	c3                   	ret    

f010482f <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
f010482f:	55                   	push   %ebp
f0104830:	89 e5                	mov    %esp,%ebp
f0104832:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
f0104835:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104838:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
f010483b:	8b 45 08             	mov    0x8(%ebp),%eax
f010483e:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
f0104841:	eb 17                	jmp    f010485a <memcpy+0x2b>
		*d++ = *s++;
f0104843:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104846:	8d 50 01             	lea    0x1(%eax),%edx
f0104849:	89 55 f8             	mov    %edx,-0x8(%ebp)
f010484c:	8b 55 fc             	mov    -0x4(%ebp),%edx
f010484f:	8d 4a 01             	lea    0x1(%edx),%ecx
f0104852:	89 4d fc             	mov    %ecx,-0x4(%ebp)
f0104855:	0f b6 12             	movzbl (%edx),%edx
f0104858:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
f010485a:	8b 45 10             	mov    0x10(%ebp),%eax
f010485d:	8d 50 ff             	lea    -0x1(%eax),%edx
f0104860:	89 55 10             	mov    %edx,0x10(%ebp)
f0104863:	85 c0                	test   %eax,%eax
f0104865:	75 dc                	jne    f0104843 <memcpy+0x14>
		*d++ = *s++;

	return dst;
f0104867:	8b 45 08             	mov    0x8(%ebp),%eax
}
f010486a:	c9                   	leave  
f010486b:	c3                   	ret    

f010486c <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
f010486c:	55                   	push   %ebp
f010486d:	89 e5                	mov    %esp,%ebp
f010486f:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
f0104872:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104875:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
f0104878:	8b 45 08             	mov    0x8(%ebp),%eax
f010487b:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
f010487e:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104881:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0104884:	73 3d                	jae    f01048c3 <memmove+0x57>
f0104886:	8b 45 10             	mov    0x10(%ebp),%eax
f0104889:	8b 55 fc             	mov    -0x4(%ebp),%edx
f010488c:	01 d0                	add    %edx,%eax
f010488e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0104891:	76 30                	jbe    f01048c3 <memmove+0x57>
		s += n;
f0104893:	8b 45 10             	mov    0x10(%ebp),%eax
f0104896:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
f0104899:	8b 45 10             	mov    0x10(%ebp),%eax
f010489c:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
f010489f:	eb 13                	jmp    f01048b4 <memmove+0x48>
			*--d = *--s;
f01048a1:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
f01048a5:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
f01048a9:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01048ac:	0f b6 10             	movzbl (%eax),%edx
f01048af:	8b 45 f8             	mov    -0x8(%ebp),%eax
f01048b2:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
f01048b4:	8b 45 10             	mov    0x10(%ebp),%eax
f01048b7:	8d 50 ff             	lea    -0x1(%eax),%edx
f01048ba:	89 55 10             	mov    %edx,0x10(%ebp)
f01048bd:	85 c0                	test   %eax,%eax
f01048bf:	75 e0                	jne    f01048a1 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
f01048c1:	eb 26                	jmp    f01048e9 <memmove+0x7d>
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
f01048c3:	eb 17                	jmp    f01048dc <memmove+0x70>
			*d++ = *s++;
f01048c5:	8b 45 f8             	mov    -0x8(%ebp),%eax
f01048c8:	8d 50 01             	lea    0x1(%eax),%edx
f01048cb:	89 55 f8             	mov    %edx,-0x8(%ebp)
f01048ce:	8b 55 fc             	mov    -0x4(%ebp),%edx
f01048d1:	8d 4a 01             	lea    0x1(%edx),%ecx
f01048d4:	89 4d fc             	mov    %ecx,-0x4(%ebp)
f01048d7:	0f b6 12             	movzbl (%edx),%edx
f01048da:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
f01048dc:	8b 45 10             	mov    0x10(%ebp),%eax
f01048df:	8d 50 ff             	lea    -0x1(%eax),%edx
f01048e2:	89 55 10             	mov    %edx,0x10(%ebp)
f01048e5:	85 c0                	test   %eax,%eax
f01048e7:	75 dc                	jne    f01048c5 <memmove+0x59>
			*d++ = *s++;

	return dst;
f01048e9:	8b 45 08             	mov    0x8(%ebp),%eax
}
f01048ec:	c9                   	leave  
f01048ed:	c3                   	ret    

f01048ee <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
f01048ee:	55                   	push   %ebp
f01048ef:	89 e5                	mov    %esp,%ebp
f01048f1:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
f01048f4:	8b 45 08             	mov    0x8(%ebp),%eax
f01048f7:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
f01048fa:	8b 45 0c             	mov    0xc(%ebp),%eax
f01048fd:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
f0104900:	eb 30                	jmp    f0104932 <memcmp+0x44>
		if (*s1 != *s2)
f0104902:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104905:	0f b6 10             	movzbl (%eax),%edx
f0104908:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010490b:	0f b6 00             	movzbl (%eax),%eax
f010490e:	38 c2                	cmp    %al,%dl
f0104910:	74 18                	je     f010492a <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
f0104912:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104915:	0f b6 00             	movzbl (%eax),%eax
f0104918:	0f b6 d0             	movzbl %al,%edx
f010491b:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010491e:	0f b6 00             	movzbl (%eax),%eax
f0104921:	0f b6 c0             	movzbl %al,%eax
f0104924:	29 c2                	sub    %eax,%edx
f0104926:	89 d0                	mov    %edx,%eax
f0104928:	eb 1a                	jmp    f0104944 <memcmp+0x56>
		s1++, s2++;
f010492a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f010492e:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
f0104932:	8b 45 10             	mov    0x10(%ebp),%eax
f0104935:	8d 50 ff             	lea    -0x1(%eax),%edx
f0104938:	89 55 10             	mov    %edx,0x10(%ebp)
f010493b:	85 c0                	test   %eax,%eax
f010493d:	75 c3                	jne    f0104902 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f010493f:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0104944:	c9                   	leave  
f0104945:	c3                   	ret    

f0104946 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
f0104946:	55                   	push   %ebp
f0104947:	89 e5                	mov    %esp,%ebp
f0104949:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
f010494c:	8b 45 10             	mov    0x10(%ebp),%eax
f010494f:	8b 55 08             	mov    0x8(%ebp),%edx
f0104952:	01 d0                	add    %edx,%eax
f0104954:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
f0104957:	eb 13                	jmp    f010496c <memfind+0x26>
		if (*(const unsigned char *) s == (unsigned char) c)
f0104959:	8b 45 08             	mov    0x8(%ebp),%eax
f010495c:	0f b6 10             	movzbl (%eax),%edx
f010495f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104962:	38 c2                	cmp    %al,%dl
f0104964:	75 02                	jne    f0104968 <memfind+0x22>
			break;
f0104966:	eb 0c                	jmp    f0104974 <memfind+0x2e>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0104968:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f010496c:	8b 45 08             	mov    0x8(%ebp),%eax
f010496f:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0104972:	72 e5                	jb     f0104959 <memfind+0x13>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
f0104974:	8b 45 08             	mov    0x8(%ebp),%eax
}
f0104977:	c9                   	leave  
f0104978:	c3                   	ret    

f0104979 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f0104979:	55                   	push   %ebp
f010497a:	89 e5                	mov    %esp,%ebp
f010497c:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
f010497f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
f0104986:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010498d:	eb 04                	jmp    f0104993 <strtol+0x1a>
		s++;
f010498f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104993:	8b 45 08             	mov    0x8(%ebp),%eax
f0104996:	0f b6 00             	movzbl (%eax),%eax
f0104999:	3c 20                	cmp    $0x20,%al
f010499b:	74 f2                	je     f010498f <strtol+0x16>
f010499d:	8b 45 08             	mov    0x8(%ebp),%eax
f01049a0:	0f b6 00             	movzbl (%eax),%eax
f01049a3:	3c 09                	cmp    $0x9,%al
f01049a5:	74 e8                	je     f010498f <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
f01049a7:	8b 45 08             	mov    0x8(%ebp),%eax
f01049aa:	0f b6 00             	movzbl (%eax),%eax
f01049ad:	3c 2b                	cmp    $0x2b,%al
f01049af:	75 06                	jne    f01049b7 <strtol+0x3e>
		s++;
f01049b1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01049b5:	eb 15                	jmp    f01049cc <strtol+0x53>
	else if (*s == '-')
f01049b7:	8b 45 08             	mov    0x8(%ebp),%eax
f01049ba:	0f b6 00             	movzbl (%eax),%eax
f01049bd:	3c 2d                	cmp    $0x2d,%al
f01049bf:	75 0b                	jne    f01049cc <strtol+0x53>
		s++, neg = 1;
f01049c1:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01049c5:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f01049cc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01049d0:	74 06                	je     f01049d8 <strtol+0x5f>
f01049d2:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f01049d6:	75 24                	jne    f01049fc <strtol+0x83>
f01049d8:	8b 45 08             	mov    0x8(%ebp),%eax
f01049db:	0f b6 00             	movzbl (%eax),%eax
f01049de:	3c 30                	cmp    $0x30,%al
f01049e0:	75 1a                	jne    f01049fc <strtol+0x83>
f01049e2:	8b 45 08             	mov    0x8(%ebp),%eax
f01049e5:	83 c0 01             	add    $0x1,%eax
f01049e8:	0f b6 00             	movzbl (%eax),%eax
f01049eb:	3c 78                	cmp    $0x78,%al
f01049ed:	75 0d                	jne    f01049fc <strtol+0x83>
		s += 2, base = 16;
f01049ef:	83 45 08 02          	addl   $0x2,0x8(%ebp)
f01049f3:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f01049fa:	eb 2a                	jmp    f0104a26 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
f01049fc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104a00:	75 17                	jne    f0104a19 <strtol+0xa0>
f0104a02:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a05:	0f b6 00             	movzbl (%eax),%eax
f0104a08:	3c 30                	cmp    $0x30,%al
f0104a0a:	75 0d                	jne    f0104a19 <strtol+0xa0>
		s++, base = 8;
f0104a0c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104a10:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f0104a17:	eb 0d                	jmp    f0104a26 <strtol+0xad>
	else if (base == 0)
f0104a19:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104a1d:	75 07                	jne    f0104a26 <strtol+0xad>
		base = 10;
f0104a1f:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f0104a26:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a29:	0f b6 00             	movzbl (%eax),%eax
f0104a2c:	3c 2f                	cmp    $0x2f,%al
f0104a2e:	7e 1b                	jle    f0104a4b <strtol+0xd2>
f0104a30:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a33:	0f b6 00             	movzbl (%eax),%eax
f0104a36:	3c 39                	cmp    $0x39,%al
f0104a38:	7f 11                	jg     f0104a4b <strtol+0xd2>
			dig = *s - '0';
f0104a3a:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a3d:	0f b6 00             	movzbl (%eax),%eax
f0104a40:	0f be c0             	movsbl %al,%eax
f0104a43:	83 e8 30             	sub    $0x30,%eax
f0104a46:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0104a49:	eb 48                	jmp    f0104a93 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
f0104a4b:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a4e:	0f b6 00             	movzbl (%eax),%eax
f0104a51:	3c 60                	cmp    $0x60,%al
f0104a53:	7e 1b                	jle    f0104a70 <strtol+0xf7>
f0104a55:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a58:	0f b6 00             	movzbl (%eax),%eax
f0104a5b:	3c 7a                	cmp    $0x7a,%al
f0104a5d:	7f 11                	jg     f0104a70 <strtol+0xf7>
			dig = *s - 'a' + 10;
f0104a5f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a62:	0f b6 00             	movzbl (%eax),%eax
f0104a65:	0f be c0             	movsbl %al,%eax
f0104a68:	83 e8 57             	sub    $0x57,%eax
f0104a6b:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0104a6e:	eb 23                	jmp    f0104a93 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
f0104a70:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a73:	0f b6 00             	movzbl (%eax),%eax
f0104a76:	3c 40                	cmp    $0x40,%al
f0104a78:	7e 3d                	jle    f0104ab7 <strtol+0x13e>
f0104a7a:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a7d:	0f b6 00             	movzbl (%eax),%eax
f0104a80:	3c 5a                	cmp    $0x5a,%al
f0104a82:	7f 33                	jg     f0104ab7 <strtol+0x13e>
			dig = *s - 'A' + 10;
f0104a84:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a87:	0f b6 00             	movzbl (%eax),%eax
f0104a8a:	0f be c0             	movsbl %al,%eax
f0104a8d:	83 e8 37             	sub    $0x37,%eax
f0104a90:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
f0104a93:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0104a96:	3b 45 10             	cmp    0x10(%ebp),%eax
f0104a99:	7c 02                	jl     f0104a9d <strtol+0x124>
			break;
f0104a9b:	eb 1a                	jmp    f0104ab7 <strtol+0x13e>
		s++, val = (val * base) + dig;
f0104a9d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104aa1:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104aa4:	0f af 45 10          	imul   0x10(%ebp),%eax
f0104aa8:	89 c2                	mov    %eax,%edx
f0104aaa:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0104aad:	01 d0                	add    %edx,%eax
f0104aaf:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
f0104ab2:	e9 6f ff ff ff       	jmp    f0104a26 <strtol+0xad>

	if (endptr)
f0104ab7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104abb:	74 08                	je     f0104ac5 <strtol+0x14c>
		*endptr = (char *) s;
f0104abd:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104ac0:	8b 55 08             	mov    0x8(%ebp),%edx
f0104ac3:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
f0104ac5:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f0104ac9:	74 07                	je     f0104ad2 <strtol+0x159>
f0104acb:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104ace:	f7 d8                	neg    %eax
f0104ad0:	eb 03                	jmp    f0104ad5 <strtol+0x15c>
f0104ad2:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f0104ad5:	c9                   	leave  
f0104ad6:	c3                   	ret    

f0104ad7 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
f0104ad7:	55                   	push   %ebp
f0104ad8:	89 e5                	mov    %esp,%ebp
f0104ada:	83 ec 08             	sub    $0x8,%esp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
f0104add:	8b 45 14             	mov    0x14(%ebp),%eax
f0104ae0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
f0104ae6:	8b 45 14             	mov    0x14(%ebp),%eax
f0104ae9:	8b 00                	mov    (%eax),%eax
f0104aeb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104af2:	8b 45 10             	mov    0x10(%ebp),%eax
f0104af5:	01 d0                	add    %edx,%eax
f0104af7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
f0104afd:	eb 0c                	jmp    f0104b0b <strsplit+0x34>
			*string++ = 0;
f0104aff:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b02:	8d 50 01             	lea    0x1(%eax),%edx
f0104b05:	89 55 08             	mov    %edx,0x8(%ebp)
f0104b08:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
f0104b0b:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b0e:	0f b6 00             	movzbl (%eax),%eax
f0104b11:	84 c0                	test   %al,%al
f0104b13:	74 1c                	je     f0104b31 <strsplit+0x5a>
f0104b15:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b18:	0f b6 00             	movzbl (%eax),%eax
f0104b1b:	0f be c0             	movsbl %al,%eax
f0104b1e:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104b22:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104b25:	89 04 24             	mov    %eax,(%esp)
f0104b28:	e8 70 fc ff ff       	call   f010479d <strchr>
f0104b2d:	85 c0                	test   %eax,%eax
f0104b2f:	75 ce                	jne    f0104aff <strsplit+0x28>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
f0104b31:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b34:	0f b6 00             	movzbl (%eax),%eax
f0104b37:	84 c0                	test   %al,%al
f0104b39:	75 1f                	jne    f0104b5a <strsplit+0x83>
			break;
f0104b3b:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
f0104b3c:	8b 45 14             	mov    0x14(%ebp),%eax
f0104b3f:	8b 00                	mov    (%eax),%eax
f0104b41:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104b48:	8b 45 10             	mov    0x10(%ebp),%eax
f0104b4b:	01 d0                	add    %edx,%eax
f0104b4d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
f0104b53:	b8 01 00 00 00       	mov    $0x1,%eax
f0104b58:	eb 61                	jmp    f0104bbb <strsplit+0xe4>
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
f0104b5a:	8b 45 14             	mov    0x14(%ebp),%eax
f0104b5d:	8b 00                	mov    (%eax),%eax
f0104b5f:	83 f8 0f             	cmp    $0xf,%eax
f0104b62:	75 07                	jne    f0104b6b <strsplit+0x94>
		{
			return 0;
f0104b64:	b8 00 00 00 00       	mov    $0x0,%eax
f0104b69:	eb 50                	jmp    f0104bbb <strsplit+0xe4>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
f0104b6b:	8b 45 14             	mov    0x14(%ebp),%eax
f0104b6e:	8b 00                	mov    (%eax),%eax
f0104b70:	8d 48 01             	lea    0x1(%eax),%ecx
f0104b73:	8b 55 14             	mov    0x14(%ebp),%edx
f0104b76:	89 0a                	mov    %ecx,(%edx)
f0104b78:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104b7f:	8b 45 10             	mov    0x10(%ebp),%eax
f0104b82:	01 c2                	add    %eax,%edx
f0104b84:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b87:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
f0104b89:	eb 04                	jmp    f0104b8f <strsplit+0xb8>
			string++;
f0104b8b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
f0104b8f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b92:	0f b6 00             	movzbl (%eax),%eax
f0104b95:	84 c0                	test   %al,%al
f0104b97:	74 1c                	je     f0104bb5 <strsplit+0xde>
f0104b99:	8b 45 08             	mov    0x8(%ebp),%eax
f0104b9c:	0f b6 00             	movzbl (%eax),%eax
f0104b9f:	0f be c0             	movsbl %al,%eax
f0104ba2:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104ba6:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104ba9:	89 04 24             	mov    %eax,(%esp)
f0104bac:	e8 ec fb ff ff       	call   f010479d <strchr>
f0104bb1:	85 c0                	test   %eax,%eax
f0104bb3:	74 d6                	je     f0104b8b <strsplit+0xb4>
			string++;
	}
f0104bb5:	90                   	nop
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
f0104bb6:	e9 50 ff ff ff       	jmp    f0104b0b <strsplit+0x34>
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
	return 1 ;
}
f0104bbb:	c9                   	leave  
f0104bbc:	c3                   	ret    
f0104bbd:	66 90                	xchg   %ax,%ax
f0104bbf:	90                   	nop

f0104bc0 <__udivdi3>:
f0104bc0:	55                   	push   %ebp
f0104bc1:	57                   	push   %edi
f0104bc2:	56                   	push   %esi
f0104bc3:	83 ec 0c             	sub    $0xc,%esp
f0104bc6:	8b 44 24 28          	mov    0x28(%esp),%eax
f0104bca:	8b 7c 24 1c          	mov    0x1c(%esp),%edi
f0104bce:	8b 6c 24 20          	mov    0x20(%esp),%ebp
f0104bd2:	8b 4c 24 24          	mov    0x24(%esp),%ecx
f0104bd6:	85 c0                	test   %eax,%eax
f0104bd8:	89 7c 24 04          	mov    %edi,0x4(%esp)
f0104bdc:	89 ea                	mov    %ebp,%edx
f0104bde:	89 0c 24             	mov    %ecx,(%esp)
f0104be1:	75 2d                	jne    f0104c10 <__udivdi3+0x50>
f0104be3:	39 e9                	cmp    %ebp,%ecx
f0104be5:	77 61                	ja     f0104c48 <__udivdi3+0x88>
f0104be7:	85 c9                	test   %ecx,%ecx
f0104be9:	89 ce                	mov    %ecx,%esi
f0104beb:	75 0b                	jne    f0104bf8 <__udivdi3+0x38>
f0104bed:	b8 01 00 00 00       	mov    $0x1,%eax
f0104bf2:	31 d2                	xor    %edx,%edx
f0104bf4:	f7 f1                	div    %ecx
f0104bf6:	89 c6                	mov    %eax,%esi
f0104bf8:	31 d2                	xor    %edx,%edx
f0104bfa:	89 e8                	mov    %ebp,%eax
f0104bfc:	f7 f6                	div    %esi
f0104bfe:	89 c5                	mov    %eax,%ebp
f0104c00:	89 f8                	mov    %edi,%eax
f0104c02:	f7 f6                	div    %esi
f0104c04:	89 ea                	mov    %ebp,%edx
f0104c06:	83 c4 0c             	add    $0xc,%esp
f0104c09:	5e                   	pop    %esi
f0104c0a:	5f                   	pop    %edi
f0104c0b:	5d                   	pop    %ebp
f0104c0c:	c3                   	ret    
f0104c0d:	8d 76 00             	lea    0x0(%esi),%esi
f0104c10:	39 e8                	cmp    %ebp,%eax
f0104c12:	77 24                	ja     f0104c38 <__udivdi3+0x78>
f0104c14:	0f bd e8             	bsr    %eax,%ebp
f0104c17:	83 f5 1f             	xor    $0x1f,%ebp
f0104c1a:	75 3c                	jne    f0104c58 <__udivdi3+0x98>
f0104c1c:	8b 74 24 04          	mov    0x4(%esp),%esi
f0104c20:	39 34 24             	cmp    %esi,(%esp)
f0104c23:	0f 86 9f 00 00 00    	jbe    f0104cc8 <__udivdi3+0x108>
f0104c29:	39 d0                	cmp    %edx,%eax
f0104c2b:	0f 82 97 00 00 00    	jb     f0104cc8 <__udivdi3+0x108>
f0104c31:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0104c38:	31 d2                	xor    %edx,%edx
f0104c3a:	31 c0                	xor    %eax,%eax
f0104c3c:	83 c4 0c             	add    $0xc,%esp
f0104c3f:	5e                   	pop    %esi
f0104c40:	5f                   	pop    %edi
f0104c41:	5d                   	pop    %ebp
f0104c42:	c3                   	ret    
f0104c43:	90                   	nop
f0104c44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104c48:	89 f8                	mov    %edi,%eax
f0104c4a:	f7 f1                	div    %ecx
f0104c4c:	31 d2                	xor    %edx,%edx
f0104c4e:	83 c4 0c             	add    $0xc,%esp
f0104c51:	5e                   	pop    %esi
f0104c52:	5f                   	pop    %edi
f0104c53:	5d                   	pop    %ebp
f0104c54:	c3                   	ret    
f0104c55:	8d 76 00             	lea    0x0(%esi),%esi
f0104c58:	89 e9                	mov    %ebp,%ecx
f0104c5a:	8b 3c 24             	mov    (%esp),%edi
f0104c5d:	d3 e0                	shl    %cl,%eax
f0104c5f:	89 c6                	mov    %eax,%esi
f0104c61:	b8 20 00 00 00       	mov    $0x20,%eax
f0104c66:	29 e8                	sub    %ebp,%eax
f0104c68:	89 c1                	mov    %eax,%ecx
f0104c6a:	d3 ef                	shr    %cl,%edi
f0104c6c:	89 e9                	mov    %ebp,%ecx
f0104c6e:	89 7c 24 08          	mov    %edi,0x8(%esp)
f0104c72:	8b 3c 24             	mov    (%esp),%edi
f0104c75:	09 74 24 08          	or     %esi,0x8(%esp)
f0104c79:	89 d6                	mov    %edx,%esi
f0104c7b:	d3 e7                	shl    %cl,%edi
f0104c7d:	89 c1                	mov    %eax,%ecx
f0104c7f:	89 3c 24             	mov    %edi,(%esp)
f0104c82:	8b 7c 24 04          	mov    0x4(%esp),%edi
f0104c86:	d3 ee                	shr    %cl,%esi
f0104c88:	89 e9                	mov    %ebp,%ecx
f0104c8a:	d3 e2                	shl    %cl,%edx
f0104c8c:	89 c1                	mov    %eax,%ecx
f0104c8e:	d3 ef                	shr    %cl,%edi
f0104c90:	09 d7                	or     %edx,%edi
f0104c92:	89 f2                	mov    %esi,%edx
f0104c94:	89 f8                	mov    %edi,%eax
f0104c96:	f7 74 24 08          	divl   0x8(%esp)
f0104c9a:	89 d6                	mov    %edx,%esi
f0104c9c:	89 c7                	mov    %eax,%edi
f0104c9e:	f7 24 24             	mull   (%esp)
f0104ca1:	39 d6                	cmp    %edx,%esi
f0104ca3:	89 14 24             	mov    %edx,(%esp)
f0104ca6:	72 30                	jb     f0104cd8 <__udivdi3+0x118>
f0104ca8:	8b 54 24 04          	mov    0x4(%esp),%edx
f0104cac:	89 e9                	mov    %ebp,%ecx
f0104cae:	d3 e2                	shl    %cl,%edx
f0104cb0:	39 c2                	cmp    %eax,%edx
f0104cb2:	73 05                	jae    f0104cb9 <__udivdi3+0xf9>
f0104cb4:	3b 34 24             	cmp    (%esp),%esi
f0104cb7:	74 1f                	je     f0104cd8 <__udivdi3+0x118>
f0104cb9:	89 f8                	mov    %edi,%eax
f0104cbb:	31 d2                	xor    %edx,%edx
f0104cbd:	e9 7a ff ff ff       	jmp    f0104c3c <__udivdi3+0x7c>
f0104cc2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f0104cc8:	31 d2                	xor    %edx,%edx
f0104cca:	b8 01 00 00 00       	mov    $0x1,%eax
f0104ccf:	e9 68 ff ff ff       	jmp    f0104c3c <__udivdi3+0x7c>
f0104cd4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104cd8:	8d 47 ff             	lea    -0x1(%edi),%eax
f0104cdb:	31 d2                	xor    %edx,%edx
f0104cdd:	83 c4 0c             	add    $0xc,%esp
f0104ce0:	5e                   	pop    %esi
f0104ce1:	5f                   	pop    %edi
f0104ce2:	5d                   	pop    %ebp
f0104ce3:	c3                   	ret    
f0104ce4:	66 90                	xchg   %ax,%ax
f0104ce6:	66 90                	xchg   %ax,%ax
f0104ce8:	66 90                	xchg   %ax,%ax
f0104cea:	66 90                	xchg   %ax,%ax
f0104cec:	66 90                	xchg   %ax,%ax
f0104cee:	66 90                	xchg   %ax,%ax

f0104cf0 <__umoddi3>:
f0104cf0:	55                   	push   %ebp
f0104cf1:	57                   	push   %edi
f0104cf2:	56                   	push   %esi
f0104cf3:	83 ec 14             	sub    $0x14,%esp
f0104cf6:	8b 44 24 28          	mov    0x28(%esp),%eax
f0104cfa:	8b 4c 24 24          	mov    0x24(%esp),%ecx
f0104cfe:	8b 74 24 2c          	mov    0x2c(%esp),%esi
f0104d02:	89 c7                	mov    %eax,%edi
f0104d04:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104d08:	8b 44 24 30          	mov    0x30(%esp),%eax
f0104d0c:	89 4c 24 10          	mov    %ecx,0x10(%esp)
f0104d10:	89 34 24             	mov    %esi,(%esp)
f0104d13:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104d17:	85 c0                	test   %eax,%eax
f0104d19:	89 c2                	mov    %eax,%edx
f0104d1b:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0104d1f:	75 17                	jne    f0104d38 <__umoddi3+0x48>
f0104d21:	39 fe                	cmp    %edi,%esi
f0104d23:	76 4b                	jbe    f0104d70 <__umoddi3+0x80>
f0104d25:	89 c8                	mov    %ecx,%eax
f0104d27:	89 fa                	mov    %edi,%edx
f0104d29:	f7 f6                	div    %esi
f0104d2b:	89 d0                	mov    %edx,%eax
f0104d2d:	31 d2                	xor    %edx,%edx
f0104d2f:	83 c4 14             	add    $0x14,%esp
f0104d32:	5e                   	pop    %esi
f0104d33:	5f                   	pop    %edi
f0104d34:	5d                   	pop    %ebp
f0104d35:	c3                   	ret    
f0104d36:	66 90                	xchg   %ax,%ax
f0104d38:	39 f8                	cmp    %edi,%eax
f0104d3a:	77 54                	ja     f0104d90 <__umoddi3+0xa0>
f0104d3c:	0f bd e8             	bsr    %eax,%ebp
f0104d3f:	83 f5 1f             	xor    $0x1f,%ebp
f0104d42:	75 5c                	jne    f0104da0 <__umoddi3+0xb0>
f0104d44:	8b 7c 24 08          	mov    0x8(%esp),%edi
f0104d48:	39 3c 24             	cmp    %edi,(%esp)
f0104d4b:	0f 87 e7 00 00 00    	ja     f0104e38 <__umoddi3+0x148>
f0104d51:	8b 7c 24 04          	mov    0x4(%esp),%edi
f0104d55:	29 f1                	sub    %esi,%ecx
f0104d57:	19 c7                	sbb    %eax,%edi
f0104d59:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104d5d:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0104d61:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104d65:	8b 54 24 0c          	mov    0xc(%esp),%edx
f0104d69:	83 c4 14             	add    $0x14,%esp
f0104d6c:	5e                   	pop    %esi
f0104d6d:	5f                   	pop    %edi
f0104d6e:	5d                   	pop    %ebp
f0104d6f:	c3                   	ret    
f0104d70:	85 f6                	test   %esi,%esi
f0104d72:	89 f5                	mov    %esi,%ebp
f0104d74:	75 0b                	jne    f0104d81 <__umoddi3+0x91>
f0104d76:	b8 01 00 00 00       	mov    $0x1,%eax
f0104d7b:	31 d2                	xor    %edx,%edx
f0104d7d:	f7 f6                	div    %esi
f0104d7f:	89 c5                	mov    %eax,%ebp
f0104d81:	8b 44 24 04          	mov    0x4(%esp),%eax
f0104d85:	31 d2                	xor    %edx,%edx
f0104d87:	f7 f5                	div    %ebp
f0104d89:	89 c8                	mov    %ecx,%eax
f0104d8b:	f7 f5                	div    %ebp
f0104d8d:	eb 9c                	jmp    f0104d2b <__umoddi3+0x3b>
f0104d8f:	90                   	nop
f0104d90:	89 c8                	mov    %ecx,%eax
f0104d92:	89 fa                	mov    %edi,%edx
f0104d94:	83 c4 14             	add    $0x14,%esp
f0104d97:	5e                   	pop    %esi
f0104d98:	5f                   	pop    %edi
f0104d99:	5d                   	pop    %ebp
f0104d9a:	c3                   	ret    
f0104d9b:	90                   	nop
f0104d9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104da0:	8b 04 24             	mov    (%esp),%eax
f0104da3:	be 20 00 00 00       	mov    $0x20,%esi
f0104da8:	89 e9                	mov    %ebp,%ecx
f0104daa:	29 ee                	sub    %ebp,%esi
f0104dac:	d3 e2                	shl    %cl,%edx
f0104dae:	89 f1                	mov    %esi,%ecx
f0104db0:	d3 e8                	shr    %cl,%eax
f0104db2:	89 e9                	mov    %ebp,%ecx
f0104db4:	89 44 24 04          	mov    %eax,0x4(%esp)
f0104db8:	8b 04 24             	mov    (%esp),%eax
f0104dbb:	09 54 24 04          	or     %edx,0x4(%esp)
f0104dbf:	89 fa                	mov    %edi,%edx
f0104dc1:	d3 e0                	shl    %cl,%eax
f0104dc3:	89 f1                	mov    %esi,%ecx
f0104dc5:	89 44 24 08          	mov    %eax,0x8(%esp)
f0104dc9:	8b 44 24 10          	mov    0x10(%esp),%eax
f0104dcd:	d3 ea                	shr    %cl,%edx
f0104dcf:	89 e9                	mov    %ebp,%ecx
f0104dd1:	d3 e7                	shl    %cl,%edi
f0104dd3:	89 f1                	mov    %esi,%ecx
f0104dd5:	d3 e8                	shr    %cl,%eax
f0104dd7:	89 e9                	mov    %ebp,%ecx
f0104dd9:	09 f8                	or     %edi,%eax
f0104ddb:	8b 7c 24 10          	mov    0x10(%esp),%edi
f0104ddf:	f7 74 24 04          	divl   0x4(%esp)
f0104de3:	d3 e7                	shl    %cl,%edi
f0104de5:	89 7c 24 0c          	mov    %edi,0xc(%esp)
f0104de9:	89 d7                	mov    %edx,%edi
f0104deb:	f7 64 24 08          	mull   0x8(%esp)
f0104def:	39 d7                	cmp    %edx,%edi
f0104df1:	89 c1                	mov    %eax,%ecx
f0104df3:	89 14 24             	mov    %edx,(%esp)
f0104df6:	72 2c                	jb     f0104e24 <__umoddi3+0x134>
f0104df8:	39 44 24 0c          	cmp    %eax,0xc(%esp)
f0104dfc:	72 22                	jb     f0104e20 <__umoddi3+0x130>
f0104dfe:	8b 44 24 0c          	mov    0xc(%esp),%eax
f0104e02:	29 c8                	sub    %ecx,%eax
f0104e04:	19 d7                	sbb    %edx,%edi
f0104e06:	89 e9                	mov    %ebp,%ecx
f0104e08:	89 fa                	mov    %edi,%edx
f0104e0a:	d3 e8                	shr    %cl,%eax
f0104e0c:	89 f1                	mov    %esi,%ecx
f0104e0e:	d3 e2                	shl    %cl,%edx
f0104e10:	89 e9                	mov    %ebp,%ecx
f0104e12:	d3 ef                	shr    %cl,%edi
f0104e14:	09 d0                	or     %edx,%eax
f0104e16:	89 fa                	mov    %edi,%edx
f0104e18:	83 c4 14             	add    $0x14,%esp
f0104e1b:	5e                   	pop    %esi
f0104e1c:	5f                   	pop    %edi
f0104e1d:	5d                   	pop    %ebp
f0104e1e:	c3                   	ret    
f0104e1f:	90                   	nop
f0104e20:	39 d7                	cmp    %edx,%edi
f0104e22:	75 da                	jne    f0104dfe <__umoddi3+0x10e>
f0104e24:	8b 14 24             	mov    (%esp),%edx
f0104e27:	89 c1                	mov    %eax,%ecx
f0104e29:	2b 4c 24 08          	sub    0x8(%esp),%ecx
f0104e2d:	1b 54 24 04          	sbb    0x4(%esp),%edx
f0104e31:	eb cb                	jmp    f0104dfe <__umoddi3+0x10e>
f0104e33:	90                   	nop
f0104e34:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104e38:	3b 44 24 0c          	cmp    0xc(%esp),%eax
f0104e3c:	0f 82 0f ff ff ff    	jb     f0104d51 <__umoddi3+0x61>
f0104e42:	e9 1a ff ff ff       	jmp    f0104d61 <__umoddi3+0x71>
