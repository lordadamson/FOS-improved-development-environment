
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <start_of_kernel-0xc>:
.long MULTIBOOT_HEADER_FLAGS
.long CHECKSUM

.globl		start_of_kernel
start_of_kernel:
	movw	$0x1234,0x472			# warm boot
f0100000:	02 b0 ad 1b 03 00    	add    0x31bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fb                   	sti    
f0100009:	4f                   	dec    %edi
f010000a:	52                   	push   %edx
f010000b:	e4 66                	in     $0x66,%al

f010000c <start_of_kernel>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 

	# Establish our own GDT in place of the boot loader's temporary GDT.
	lgdt	RELOC(mygdtdesc)		# load descriptor table
f0100015:	0f 01 15 18 b0 11 00 	lgdtl  0x11b018

	# Immediately reload all segment registers (including CS!)
	# with segment selectors from the new GDT.
	movl	$DATA_SEL, %eax			# Data segment selector
f010001c:	b8 10 00 00 00       	mov    $0x10,%eax
	movw	%ax,%ds				# -> DS: Data Segment
f0100021:	8e d8                	mov    %eax,%ds
	movw	%ax,%es				# -> ES: Extra Segment
f0100023:	8e c0                	mov    %eax,%es
	movw	%ax,%ss				# -> SS: Stack Segment
f0100025:	8e d0                	mov    %eax,%ss
	ljmp	$CODE_SEL,$relocated		# reload CS by jumping
f0100027:	ea 2e 00 10 f0 08 00 	ljmp   $0x8,$0xf010002e

f010002e <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0,%ebp			# nuke frame pointer
f010002e:	bd 00 00 00 00       	mov    $0x0,%ebp

        # Leave a few words on the stack for the user trap frame
	movl	$(ptr_stack_top-SIZEOF_STRUCT_TRAPFRAME),%esp
f0100033:	bc bc af 11 f0       	mov    $0xf011afbc,%esp

	# now to C code
	call	FOS_initialize
f0100038:	e8 02 00 00 00       	call   f010003f <FOS_initialize>

f010003d <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010003d:	eb fe                	jmp    f010003d <spin>

f010003f <FOS_initialize>:



//First ever function called in FOS kernel
void FOS_initialize()
{
f010003f:	55                   	push   %ebp
f0100040:	89 e5                	mov    %esp,%ebp
f0100042:	83 ec 08             	sub    $0x8,%esp
	extern char start_of_uninitialized_data_section[], end_of_kernel[];

	// Before doing anything else,
	// clear the uninitialized global data (BSS) section of our program, from start_of_uninitialized_data_section to end_of_kernel 
	// This ensures that all static/global variables start with zero value.
	memset(start_of_uninitialized_data_section, 0, end_of_kernel - start_of_uninitialized_data_section);
f0100045:	ba cc 73 14 f0       	mov    $0xf01473cc,%edx
f010004a:	b8 c2 68 14 f0       	mov    $0xf01468c2,%eax
f010004f:	29 c2                	sub    %eax,%edx
f0100051:	89 d0                	mov    %edx,%eax
f0100053:	83 ec 04             	sub    $0x4,%esp
f0100056:	50                   	push   %eax
f0100057:	6a 00                	push   $0x0
f0100059:	68 c2 68 14 f0       	push   $0xf01468c2
f010005e:	e8 1b 47 00 00       	call   f010477e <memset>
f0100063:	83 c4 10             	add    $0x10,%esp

	// Initialize the console.
	// Can't call cprintf until after we do this!
	console_initialize();
f0100066:	e8 c0 08 00 00       	call   f010092b <console_initialize>

	//print welcome message
	print_welcome_message();
f010006b:	e8 45 00 00 00       	call   f01000b5 <print_welcome_message>

	// Lab 2 memory management initialization functions
	detect_memory();
f0100070:	e8 31 0f 00 00       	call   f0100fa6 <detect_memory>
	initialize_kernel_VM();
f0100075:	e8 93 1d 00 00       	call   f0101e0d <initialize_kernel_VM>
	initialize_paging();
f010007a:	e8 56 21 00 00       	call   f01021d5 <initialize_paging>
	page_check();
f010007f:	e8 f3 12 00 00       	call   f0101377 <page_check>

	
	// Lab 3 user environment initialization functions
	env_init();
f0100084:	e8 19 29 00 00       	call   f01029a2 <env_init>
	idt_init();
f0100089:	e8 21 30 00 00       	call   f01030af <idt_init>

	
	// start the kernel command prompt.
	while (1==1)
	{
		cprintf("\nWelcome to the FOS kernel command prompt!\n");
f010008e:	83 ec 0c             	sub    $0xc,%esp
f0100091:	68 e0 4d 10 f0       	push   $0xf0104de0
f0100096:	e8 c3 2f 00 00       	call   f010305e <cprintf>
f010009b:	83 c4 10             	add    $0x10,%esp
		cprintf("Type 'help' for a list of commands.\n");	
f010009e:	83 ec 0c             	sub    $0xc,%esp
f01000a1:	68 0c 4e 10 f0       	push   $0xf0104e0c
f01000a6:	e8 b3 2f 00 00       	call   f010305e <cprintf>
f01000ab:	83 c4 10             	add    $0x10,%esp
		run_command_prompt();
f01000ae:	e8 e3 08 00 00       	call   f0100996 <run_command_prompt>
	}
f01000b3:	eb d9                	jmp    f010008e <FOS_initialize+0x4f>

f01000b5 <print_welcome_message>:
}


void print_welcome_message()
{
f01000b5:	55                   	push   %ebp
f01000b6:	89 e5                	mov    %esp,%ebp
f01000b8:	83 ec 08             	sub    $0x8,%esp
	cprintf("\n\n\n");
f01000bb:	83 ec 0c             	sub    $0xc,%esp
f01000be:	68 31 4e 10 f0       	push   $0xf0104e31
f01000c3:	e8 96 2f 00 00       	call   f010305e <cprintf>
f01000c8:	83 c4 10             	add    $0x10,%esp
	cprintf("\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
f01000cb:	83 ec 0c             	sub    $0xc,%esp
f01000ce:	68 38 4e 10 f0       	push   $0xf0104e38
f01000d3:	e8 86 2f 00 00       	call   f010305e <cprintf>
f01000d8:	83 c4 10             	add    $0x10,%esp
	cprintf("\t\t!!                                                             !!\n");
f01000db:	83 ec 0c             	sub    $0xc,%esp
f01000de:	68 80 4e 10 f0       	push   $0xf0104e80
f01000e3:	e8 76 2f 00 00       	call   f010305e <cprintf>
f01000e8:	83 c4 10             	add    $0x10,%esp
	cprintf("\t\t!!                   !! FCIS says HELLO !!                     !!\n");
f01000eb:	83 ec 0c             	sub    $0xc,%esp
f01000ee:	68 c8 4e 10 f0       	push   $0xf0104ec8
f01000f3:	e8 66 2f 00 00       	call   f010305e <cprintf>
f01000f8:	83 c4 10             	add    $0x10,%esp
	cprintf("\t\t!!                                                             !!\n");
f01000fb:	83 ec 0c             	sub    $0xc,%esp
f01000fe:	68 80 4e 10 f0       	push   $0xf0104e80
f0100103:	e8 56 2f 00 00       	call   f010305e <cprintf>
f0100108:	83 c4 10             	add    $0x10,%esp
	cprintf("\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
f010010b:	83 ec 0c             	sub    $0xc,%esp
f010010e:	68 38 4e 10 f0       	push   $0xf0104e38
f0100113:	e8 46 2f 00 00       	call   f010305e <cprintf>
f0100118:	83 c4 10             	add    $0x10,%esp
	cprintf("\n\n\n\n");	
f010011b:	83 ec 0c             	sub    $0xc,%esp
f010011e:	68 0d 4f 10 f0       	push   $0xf0104f0d
f0100123:	e8 36 2f 00 00       	call   f010305e <cprintf>
f0100128:	83 c4 10             	add    $0x10,%esp
}
f010012b:	90                   	nop
f010012c:	c9                   	leave  
f010012d:	c3                   	ret    

f010012e <_panic>:
/*
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel command prompt.
 */
void _panic(const char *file, int line, const char *fmt,...)
{
f010012e:	55                   	push   %ebp
f010012f:	89 e5                	mov    %esp,%ebp
f0100131:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	if (panicstr)
f0100134:	a1 e0 68 14 f0       	mov    0xf01468e0,%eax
f0100139:	85 c0                	test   %eax,%eax
f010013b:	74 02                	je     f010013f <_panic+0x11>
		goto dead;
f010013d:	eb 49                	jmp    f0100188 <_panic+0x5a>
	panicstr = fmt;
f010013f:	8b 45 10             	mov    0x10(%ebp),%eax
f0100142:	a3 e0 68 14 f0       	mov    %eax,0xf01468e0

	va_start(ap, fmt);
f0100147:	8d 45 10             	lea    0x10(%ebp),%eax
f010014a:	83 c0 04             	add    $0x4,%eax
f010014d:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cprintf("kernel panic at %s:%d: ", file, line);
f0100150:	83 ec 04             	sub    $0x4,%esp
f0100153:	ff 75 0c             	pushl  0xc(%ebp)
f0100156:	ff 75 08             	pushl  0x8(%ebp)
f0100159:	68 12 4f 10 f0       	push   $0xf0104f12
f010015e:	e8 fb 2e 00 00       	call   f010305e <cprintf>
f0100163:	83 c4 10             	add    $0x10,%esp
	vcprintf(fmt, ap);
f0100166:	8b 45 10             	mov    0x10(%ebp),%eax
f0100169:	83 ec 08             	sub    $0x8,%esp
f010016c:	ff 75 f4             	pushl  -0xc(%ebp)
f010016f:	50                   	push   %eax
f0100170:	e8 c0 2e 00 00       	call   f0103035 <vcprintf>
f0100175:	83 c4 10             	add    $0x10,%esp
	cprintf("\n");
f0100178:	83 ec 0c             	sub    $0xc,%esp
f010017b:	68 2a 4f 10 f0       	push   $0xf0104f2a
f0100180:	e8 d9 2e 00 00       	call   f010305e <cprintf>
f0100185:	83 c4 10             	add    $0x10,%esp
	va_end(ap);

dead:
	/* break into the kernel command prompt */
	while (1==1)
		run_command_prompt();
f0100188:	e8 09 08 00 00       	call   f0100996 <run_command_prompt>
f010018d:	eb f9                	jmp    f0100188 <_panic+0x5a>

f010018f <_warn>:
}

/* like panic, but don't enters the kernel command prompt*/
void _warn(const char *file, int line, const char *fmt,...)
{
f010018f:	55                   	push   %ebp
f0100190:	89 e5                	mov    %esp,%ebp
f0100192:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
f0100195:	8d 45 10             	lea    0x10(%ebp),%eax
f0100198:	83 c0 04             	add    $0x4,%eax
f010019b:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cprintf("kernel warning at %s:%d: ", file, line);
f010019e:	83 ec 04             	sub    $0x4,%esp
f01001a1:	ff 75 0c             	pushl  0xc(%ebp)
f01001a4:	ff 75 08             	pushl  0x8(%ebp)
f01001a7:	68 2c 4f 10 f0       	push   $0xf0104f2c
f01001ac:	e8 ad 2e 00 00       	call   f010305e <cprintf>
f01001b1:	83 c4 10             	add    $0x10,%esp
	vcprintf(fmt, ap);
f01001b4:	8b 45 10             	mov    0x10(%ebp),%eax
f01001b7:	83 ec 08             	sub    $0x8,%esp
f01001ba:	ff 75 f4             	pushl  -0xc(%ebp)
f01001bd:	50                   	push   %eax
f01001be:	e8 72 2e 00 00       	call   f0103035 <vcprintf>
f01001c3:	83 c4 10             	add    $0x10,%esp
	cprintf("\n");
f01001c6:	83 ec 0c             	sub    $0xc,%esp
f01001c9:	68 2a 4f 10 f0       	push   $0xf0104f2a
f01001ce:	e8 8b 2e 00 00       	call   f010305e <cprintf>
f01001d3:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
f01001d6:	90                   	nop
f01001d7:	c9                   	leave  
f01001d8:	c3                   	ret    

f01001d9 <serial_proc_data>:

static bool serial_exists;

int
serial_proc_data(void)
{
f01001d9:	55                   	push   %ebp
f01001da:	89 e5                	mov    %esp,%ebp
f01001dc:	83 ec 10             	sub    $0x10,%esp
f01001df:	c7 45 f8 fd 03 00 00 	movl   $0x3fd,-0x8(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01001e6:	8b 45 f8             	mov    -0x8(%ebp),%eax
f01001e9:	89 c2                	mov    %eax,%edx
f01001eb:	ec                   	in     (%dx),%al
f01001ec:	88 45 f7             	mov    %al,-0x9(%ebp)
	return data;
f01001ef:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f01001f3:	0f b6 c0             	movzbl %al,%eax
f01001f6:	83 e0 01             	and    $0x1,%eax
f01001f9:	85 c0                	test   %eax,%eax
f01001fb:	75 07                	jne    f0100204 <serial_proc_data+0x2b>
		return -1;
f01001fd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100202:	eb 17                	jmp    f010021b <serial_proc_data+0x42>
f0100204:	c7 45 fc f8 03 00 00 	movl   $0x3f8,-0x4(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010020b:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010020e:	89 c2                	mov    %eax,%edx
f0100210:	ec                   	in     (%dx),%al
f0100211:	88 45 f6             	mov    %al,-0xa(%ebp)
	return data;
f0100214:	0f b6 45 f6          	movzbl -0xa(%ebp),%eax
	return inb(COM1+COM_RX);
f0100218:	0f b6 c0             	movzbl %al,%eax
}
f010021b:	c9                   	leave  
f010021c:	c3                   	ret    

f010021d <serial_intr>:

void
serial_intr(void)
{
f010021d:	55                   	push   %ebp
f010021e:	89 e5                	mov    %esp,%ebp
f0100220:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
f0100223:	a1 00 69 14 f0       	mov    0xf0146900,%eax
f0100228:	85 c0                	test   %eax,%eax
f010022a:	74 10                	je     f010023c <serial_intr+0x1f>
		cons_intr(serial_proc_data);
f010022c:	83 ec 0c             	sub    $0xc,%esp
f010022f:	68 d9 01 10 f0       	push   $0xf01001d9
f0100234:	e8 26 06 00 00       	call   f010085f <cons_intr>
f0100239:	83 c4 10             	add    $0x10,%esp
}
f010023c:	90                   	nop
f010023d:	c9                   	leave  
f010023e:	c3                   	ret    

f010023f <serial_init>:

void
serial_init(void)
{
f010023f:	55                   	push   %ebp
f0100240:	89 e5                	mov    %esp,%ebp
f0100242:	83 ec 40             	sub    $0x40,%esp
f0100245:	c7 45 fc fa 03 00 00 	movl   $0x3fa,-0x4(%ebp)
f010024c:	c6 45 ce 00          	movb   $0x0,-0x32(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100250:	0f b6 45 ce          	movzbl -0x32(%ebp),%eax
f0100254:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0100257:	ee                   	out    %al,(%dx)
f0100258:	c7 45 f8 fb 03 00 00 	movl   $0x3fb,-0x8(%ebp)
f010025f:	c6 45 cf 80          	movb   $0x80,-0x31(%ebp)
f0100263:	0f b6 45 cf          	movzbl -0x31(%ebp),%eax
f0100267:	8b 55 f8             	mov    -0x8(%ebp),%edx
f010026a:	ee                   	out    %al,(%dx)
f010026b:	c7 45 f4 f8 03 00 00 	movl   $0x3f8,-0xc(%ebp)
f0100272:	c6 45 d0 0c          	movb   $0xc,-0x30(%ebp)
f0100276:	0f b6 45 d0          	movzbl -0x30(%ebp),%eax
f010027a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010027d:	ee                   	out    %al,(%dx)
f010027e:	c7 45 f0 f9 03 00 00 	movl   $0x3f9,-0x10(%ebp)
f0100285:	c6 45 d1 00          	movb   $0x0,-0x2f(%ebp)
f0100289:	0f b6 45 d1          	movzbl -0x2f(%ebp),%eax
f010028d:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100290:	ee                   	out    %al,(%dx)
f0100291:	c7 45 ec fb 03 00 00 	movl   $0x3fb,-0x14(%ebp)
f0100298:	c6 45 d2 03          	movb   $0x3,-0x2e(%ebp)
f010029c:	0f b6 45 d2          	movzbl -0x2e(%ebp),%eax
f01002a0:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01002a3:	ee                   	out    %al,(%dx)
f01002a4:	c7 45 e8 fc 03 00 00 	movl   $0x3fc,-0x18(%ebp)
f01002ab:	c6 45 d3 00          	movb   $0x0,-0x2d(%ebp)
f01002af:	0f b6 45 d3          	movzbl -0x2d(%ebp),%eax
f01002b3:	8b 55 e8             	mov    -0x18(%ebp),%edx
f01002b6:	ee                   	out    %al,(%dx)
f01002b7:	c7 45 e4 f9 03 00 00 	movl   $0x3f9,-0x1c(%ebp)
f01002be:	c6 45 d4 01          	movb   $0x1,-0x2c(%ebp)
f01002c2:	0f b6 45 d4          	movzbl -0x2c(%ebp),%eax
f01002c6:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f01002c9:	ee                   	out    %al,(%dx)
f01002ca:	c7 45 e0 fd 03 00 00 	movl   $0x3fd,-0x20(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01002d1:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01002d4:	89 c2                	mov    %eax,%edx
f01002d6:	ec                   	in     (%dx),%al
f01002d7:	88 45 d5             	mov    %al,-0x2b(%ebp)
	return data;
f01002da:	0f b6 45 d5          	movzbl -0x2b(%ebp),%eax
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f01002de:	3c ff                	cmp    $0xff,%al
f01002e0:	0f 95 c0             	setne  %al
f01002e3:	0f b6 c0             	movzbl %al,%eax
f01002e6:	a3 00 69 14 f0       	mov    %eax,0xf0146900
f01002eb:	c7 45 dc fa 03 00 00 	movl   $0x3fa,-0x24(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01002f2:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01002f5:	89 c2                	mov    %eax,%edx
f01002f7:	ec                   	in     (%dx),%al
f01002f8:	88 45 d6             	mov    %al,-0x2a(%ebp)
f01002fb:	c7 45 d8 f8 03 00 00 	movl   $0x3f8,-0x28(%ebp)
f0100302:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100305:	89 c2                	mov    %eax,%edx
f0100307:	ec                   	in     (%dx),%al
f0100308:	88 45 d7             	mov    %al,-0x29(%ebp)
	(void) inb(COM1+COM_IIR);
	(void) inb(COM1+COM_RX);

}
f010030b:	90                   	nop
f010030c:	c9                   	leave  
f010030d:	c3                   	ret    

f010030e <delay>:
// page.

// Stupid I/O delay routine necessitated by historical PC design flaws
static void
delay(void)
{
f010030e:	55                   	push   %ebp
f010030f:	89 e5                	mov    %esp,%ebp
f0100311:	83 ec 20             	sub    $0x20,%esp
f0100314:	c7 45 fc 84 00 00 00 	movl   $0x84,-0x4(%ebp)
f010031b:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010031e:	89 c2                	mov    %eax,%edx
f0100320:	ec                   	in     (%dx),%al
f0100321:	88 45 ec             	mov    %al,-0x14(%ebp)
f0100324:	c7 45 f8 84 00 00 00 	movl   $0x84,-0x8(%ebp)
f010032b:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010032e:	89 c2                	mov    %eax,%edx
f0100330:	ec                   	in     (%dx),%al
f0100331:	88 45 ed             	mov    %al,-0x13(%ebp)
f0100334:	c7 45 f4 84 00 00 00 	movl   $0x84,-0xc(%ebp)
f010033b:	8b 45 f4             	mov    -0xc(%ebp),%eax
f010033e:	89 c2                	mov    %eax,%edx
f0100340:	ec                   	in     (%dx),%al
f0100341:	88 45 ee             	mov    %al,-0x12(%ebp)
f0100344:	c7 45 f0 84 00 00 00 	movl   $0x84,-0x10(%ebp)
f010034b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010034e:	89 c2                	mov    %eax,%edx
f0100350:	ec                   	in     (%dx),%al
f0100351:	88 45 ef             	mov    %al,-0x11(%ebp)
	inb(0x84);
	inb(0x84);
	inb(0x84);
	inb(0x84);
}
f0100354:	90                   	nop
f0100355:	c9                   	leave  
f0100356:	c3                   	ret    

f0100357 <lpt_putc>:

static void
lpt_putc(int c)
{
f0100357:	55                   	push   %ebp
f0100358:	89 e5                	mov    %esp,%ebp
f010035a:	83 ec 20             	sub    $0x20,%esp
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 2800; i++) //12800
f010035d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f0100364:	eb 09                	jmp    f010036f <lpt_putc+0x18>
		delay();
f0100366:	e8 a3 ff ff ff       	call   f010030e <delay>
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 2800; i++) //12800
f010036b:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f010036f:	c7 45 ec 79 03 00 00 	movl   $0x379,-0x14(%ebp)
f0100376:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100379:	89 c2                	mov    %eax,%edx
f010037b:	ec                   	in     (%dx),%al
f010037c:	88 45 eb             	mov    %al,-0x15(%ebp)
	return data;
f010037f:	0f b6 45 eb          	movzbl -0x15(%ebp),%eax
f0100383:	84 c0                	test   %al,%al
f0100385:	78 09                	js     f0100390 <lpt_putc+0x39>
f0100387:	81 7d fc ef 0a 00 00 	cmpl   $0xaef,-0x4(%ebp)
f010038e:	7e d6                	jle    f0100366 <lpt_putc+0xf>
		delay();
	outb(0x378+0, c);
f0100390:	8b 45 08             	mov    0x8(%ebp),%eax
f0100393:	0f b6 c0             	movzbl %al,%eax
f0100396:	c7 45 f4 78 03 00 00 	movl   $0x378,-0xc(%ebp)
f010039d:	88 45 e8             	mov    %al,-0x18(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01003a0:	0f b6 45 e8          	movzbl -0x18(%ebp),%eax
f01003a4:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01003a7:	ee                   	out    %al,(%dx)
f01003a8:	c7 45 f0 7a 03 00 00 	movl   $0x37a,-0x10(%ebp)
f01003af:	c6 45 e9 0d          	movb   $0xd,-0x17(%ebp)
f01003b3:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
f01003b7:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01003ba:	ee                   	out    %al,(%dx)
f01003bb:	c7 45 f8 7a 03 00 00 	movl   $0x37a,-0x8(%ebp)
f01003c2:	c6 45 ea 08          	movb   $0x8,-0x16(%ebp)
f01003c6:	0f b6 45 ea          	movzbl -0x16(%ebp),%eax
f01003ca:	8b 55 f8             	mov    -0x8(%ebp),%edx
f01003cd:	ee                   	out    %al,(%dx)
	outb(0x378+2, 0x08|0x04|0x01);
	outb(0x378+2, 0x08);
}
f01003ce:	90                   	nop
f01003cf:	c9                   	leave  
f01003d0:	c3                   	ret    

f01003d1 <cga_init>:
static uint16 *crt_buf;
static uint16 crt_pos;

void
cga_init(void)
{
f01003d1:	55                   	push   %ebp
f01003d2:	89 e5                	mov    %esp,%ebp
f01003d4:	83 ec 20             	sub    $0x20,%esp
	volatile uint16 *cp;
	uint16 was;
	unsigned pos;

	cp = (uint16*) (KERNEL_BASE + CGA_BUF);
f01003d7:	c7 45 fc 00 80 0b f0 	movl   $0xf00b8000,-0x4(%ebp)
	was = *cp;
f01003de:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003e1:	0f b7 00             	movzwl (%eax),%eax
f01003e4:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
	*cp = (uint16) 0xA55A;
f01003e8:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003eb:	66 c7 00 5a a5       	movw   $0xa55a,(%eax)
	if (*cp != 0xA55A) {
f01003f0:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01003f3:	0f b7 00             	movzwl (%eax),%eax
f01003f6:	66 3d 5a a5          	cmp    $0xa55a,%ax
f01003fa:	74 13                	je     f010040f <cga_init+0x3e>
		cp = (uint16*) (KERNEL_BASE + MONO_BUF);
f01003fc:	c7 45 fc 00 00 0b f0 	movl   $0xf00b0000,-0x4(%ebp)
		addr_6845 = MONO_BASE;
f0100403:	c7 05 04 69 14 f0 b4 	movl   $0x3b4,0xf0146904
f010040a:	03 00 00 
f010040d:	eb 14                	jmp    f0100423 <cga_init+0x52>
	} else {
		*cp = was;
f010040f:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0100412:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
f0100416:	66 89 10             	mov    %dx,(%eax)
		addr_6845 = CGA_BASE;
f0100419:	c7 05 04 69 14 f0 d4 	movl   $0x3d4,0xf0146904
f0100420:	03 00 00 
	}
	
	/* Extract cursor location */
	outb(addr_6845, 14);
f0100423:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f0100428:	89 45 f4             	mov    %eax,-0xc(%ebp)
f010042b:	c6 45 e0 0e          	movb   $0xe,-0x20(%ebp)
f010042f:	0f b6 45 e0          	movzbl -0x20(%ebp),%eax
f0100433:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100436:	ee                   	out    %al,(%dx)
	pos = inb(addr_6845 + 1) << 8;
f0100437:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f010043c:	83 c0 01             	add    $0x1,%eax
f010043f:	89 45 ec             	mov    %eax,-0x14(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100442:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100445:	89 c2                	mov    %eax,%edx
f0100447:	ec                   	in     (%dx),%al
f0100448:	88 45 e1             	mov    %al,-0x1f(%ebp)
	return data;
f010044b:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
f010044f:	0f b6 c0             	movzbl %al,%eax
f0100452:	c1 e0 08             	shl    $0x8,%eax
f0100455:	89 45 f0             	mov    %eax,-0x10(%ebp)
	outb(addr_6845, 15);
f0100458:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f010045d:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0100460:	c6 45 e2 0f          	movb   $0xf,-0x1e(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100464:	0f b6 45 e2          	movzbl -0x1e(%ebp),%eax
f0100468:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010046b:	ee                   	out    %al,(%dx)
	pos |= inb(addr_6845 + 1);
f010046c:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f0100471:	83 c0 01             	add    $0x1,%eax
f0100474:	89 45 e4             	mov    %eax,-0x1c(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100477:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f010047a:	89 c2                	mov    %eax,%edx
f010047c:	ec                   	in     (%dx),%al
f010047d:	88 45 e3             	mov    %al,-0x1d(%ebp)
	return data;
f0100480:	0f b6 45 e3          	movzbl -0x1d(%ebp),%eax
f0100484:	0f b6 c0             	movzbl %al,%eax
f0100487:	09 45 f0             	or     %eax,-0x10(%ebp)

	crt_buf = (uint16*) cp;
f010048a:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010048d:	a3 08 69 14 f0       	mov    %eax,0xf0146908
	crt_pos = pos;
f0100492:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100495:	66 a3 0c 69 14 f0    	mov    %ax,0xf014690c
}
f010049b:	90                   	nop
f010049c:	c9                   	leave  
f010049d:	c3                   	ret    

f010049e <cga_putc>:



void
cga_putc(int c)
{
f010049e:	55                   	push   %ebp
f010049f:	89 e5                	mov    %esp,%ebp
f01004a1:	53                   	push   %ebx
f01004a2:	83 ec 24             	sub    $0x24,%esp
	// if no attribute given, then use black on white
	if (!(c & ~0xFF))
f01004a5:	8b 45 08             	mov    0x8(%ebp),%eax
f01004a8:	b0 00                	mov    $0x0,%al
f01004aa:	85 c0                	test   %eax,%eax
f01004ac:	75 07                	jne    f01004b5 <cga_putc+0x17>
		c |= 0x0700;
f01004ae:	81 4d 08 00 07 00 00 	orl    $0x700,0x8(%ebp)

	switch (c & 0xff) {
f01004b5:	8b 45 08             	mov    0x8(%ebp),%eax
f01004b8:	0f b6 c0             	movzbl %al,%eax
f01004bb:	83 f8 09             	cmp    $0x9,%eax
f01004be:	0f 84 ab 00 00 00    	je     f010056f <cga_putc+0xd1>
f01004c4:	83 f8 09             	cmp    $0x9,%eax
f01004c7:	7f 0a                	jg     f01004d3 <cga_putc+0x35>
f01004c9:	83 f8 08             	cmp    $0x8,%eax
f01004cc:	74 14                	je     f01004e2 <cga_putc+0x44>
f01004ce:	e9 df 00 00 00       	jmp    f01005b2 <cga_putc+0x114>
f01004d3:	83 f8 0a             	cmp    $0xa,%eax
f01004d6:	74 4d                	je     f0100525 <cga_putc+0x87>
f01004d8:	83 f8 0d             	cmp    $0xd,%eax
f01004db:	74 58                	je     f0100535 <cga_putc+0x97>
f01004dd:	e9 d0 00 00 00       	jmp    f01005b2 <cga_putc+0x114>
	case '\b':
		if (crt_pos > 0) {
f01004e2:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f01004e9:	66 85 c0             	test   %ax,%ax
f01004ec:	0f 84 e6 00 00 00    	je     f01005d8 <cga_putc+0x13a>
			crt_pos--;
f01004f2:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f01004f9:	83 e8 01             	sub    $0x1,%eax
f01004fc:	66 a3 0c 69 14 f0    	mov    %ax,0xf014690c
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f0100502:	a1 08 69 14 f0       	mov    0xf0146908,%eax
f0100507:	0f b7 15 0c 69 14 f0 	movzwl 0xf014690c,%edx
f010050e:	0f b7 d2             	movzwl %dx,%edx
f0100511:	01 d2                	add    %edx,%edx
f0100513:	01 d0                	add    %edx,%eax
f0100515:	8b 55 08             	mov    0x8(%ebp),%edx
f0100518:	b2 00                	mov    $0x0,%dl
f010051a:	83 ca 20             	or     $0x20,%edx
f010051d:	66 89 10             	mov    %dx,(%eax)
		}
		break;
f0100520:	e9 b3 00 00 00       	jmp    f01005d8 <cga_putc+0x13a>
	case '\n':
		crt_pos += CRT_COLS;
f0100525:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f010052c:	83 c0 50             	add    $0x50,%eax
f010052f:	66 a3 0c 69 14 f0    	mov    %ax,0xf014690c
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f0100535:	0f b7 1d 0c 69 14 f0 	movzwl 0xf014690c,%ebx
f010053c:	0f b7 0d 0c 69 14 f0 	movzwl 0xf014690c,%ecx
f0100543:	0f b7 c1             	movzwl %cx,%eax
f0100546:	69 c0 cd cc 00 00    	imul   $0xcccd,%eax,%eax
f010054c:	c1 e8 10             	shr    $0x10,%eax
f010054f:	89 c2                	mov    %eax,%edx
f0100551:	66 c1 ea 06          	shr    $0x6,%dx
f0100555:	89 d0                	mov    %edx,%eax
f0100557:	c1 e0 02             	shl    $0x2,%eax
f010055a:	01 d0                	add    %edx,%eax
f010055c:	c1 e0 04             	shl    $0x4,%eax
f010055f:	29 c1                	sub    %eax,%ecx
f0100561:	89 ca                	mov    %ecx,%edx
f0100563:	89 d8                	mov    %ebx,%eax
f0100565:	29 d0                	sub    %edx,%eax
f0100567:	66 a3 0c 69 14 f0    	mov    %ax,0xf014690c
		break;
f010056d:	eb 6a                	jmp    f01005d9 <cga_putc+0x13b>
	case '\t':
		cons_putc(' ');
f010056f:	83 ec 0c             	sub    $0xc,%esp
f0100572:	6a 20                	push   $0x20
f0100574:	e8 90 03 00 00       	call   f0100909 <cons_putc>
f0100579:	83 c4 10             	add    $0x10,%esp
		cons_putc(' ');
f010057c:	83 ec 0c             	sub    $0xc,%esp
f010057f:	6a 20                	push   $0x20
f0100581:	e8 83 03 00 00       	call   f0100909 <cons_putc>
f0100586:	83 c4 10             	add    $0x10,%esp
		cons_putc(' ');
f0100589:	83 ec 0c             	sub    $0xc,%esp
f010058c:	6a 20                	push   $0x20
f010058e:	e8 76 03 00 00       	call   f0100909 <cons_putc>
f0100593:	83 c4 10             	add    $0x10,%esp
		cons_putc(' ');
f0100596:	83 ec 0c             	sub    $0xc,%esp
f0100599:	6a 20                	push   $0x20
f010059b:	e8 69 03 00 00       	call   f0100909 <cons_putc>
f01005a0:	83 c4 10             	add    $0x10,%esp
		cons_putc(' ');
f01005a3:	83 ec 0c             	sub    $0xc,%esp
f01005a6:	6a 20                	push   $0x20
f01005a8:	e8 5c 03 00 00       	call   f0100909 <cons_putc>
f01005ad:	83 c4 10             	add    $0x10,%esp
		break;
f01005b0:	eb 27                	jmp    f01005d9 <cga_putc+0x13b>
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f01005b2:	8b 0d 08 69 14 f0    	mov    0xf0146908,%ecx
f01005b8:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f01005bf:	8d 50 01             	lea    0x1(%eax),%edx
f01005c2:	66 89 15 0c 69 14 f0 	mov    %dx,0xf014690c
f01005c9:	0f b7 c0             	movzwl %ax,%eax
f01005cc:	01 c0                	add    %eax,%eax
f01005ce:	01 c8                	add    %ecx,%eax
f01005d0:	8b 55 08             	mov    0x8(%ebp),%edx
f01005d3:	66 89 10             	mov    %dx,(%eax)
		break;
f01005d6:	eb 01                	jmp    f01005d9 <cga_putc+0x13b>
	case '\b':
		if (crt_pos > 0) {
			crt_pos--;
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
		}
		break;
f01005d8:	90                   	nop
		crt_buf[crt_pos++] = c;		/* write the character */
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f01005d9:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f01005e0:	66 3d cf 07          	cmp    $0x7cf,%ax
f01005e4:	76 59                	jbe    f010063f <cga_putc+0x1a1>
		int i;

		memcpy(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16));
f01005e6:	a1 08 69 14 f0       	mov    0xf0146908,%eax
f01005eb:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f01005f1:	a1 08 69 14 f0       	mov    0xf0146908,%eax
f01005f6:	83 ec 04             	sub    $0x4,%esp
f01005f9:	68 00 0f 00 00       	push   $0xf00
f01005fe:	52                   	push   %edx
f01005ff:	50                   	push   %eax
f0100600:	e8 aa 41 00 00       	call   f01047af <memcpy>
f0100605:	83 c4 10             	add    $0x10,%esp
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100608:	c7 45 f4 80 07 00 00 	movl   $0x780,-0xc(%ebp)
f010060f:	eb 15                	jmp    f0100626 <cga_putc+0x188>
			crt_buf[i] = 0x0700 | ' ';
f0100611:	a1 08 69 14 f0       	mov    0xf0146908,%eax
f0100616:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100619:	01 d2                	add    %edx,%edx
f010061b:	01 d0                	add    %edx,%eax
f010061d:	66 c7 00 20 07       	movw   $0x720,(%eax)
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memcpy(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100622:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0100626:	81 7d f4 cf 07 00 00 	cmpl   $0x7cf,-0xc(%ebp)
f010062d:	7e e2                	jle    f0100611 <cga_putc+0x173>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f010062f:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f0100636:	83 e8 50             	sub    $0x50,%eax
f0100639:	66 a3 0c 69 14 f0    	mov    %ax,0xf014690c
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f010063f:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f0100644:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100647:	c6 45 e0 0e          	movb   $0xe,-0x20(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f010064b:	0f b6 45 e0          	movzbl -0x20(%ebp),%eax
f010064f:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100652:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f0100653:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f010065a:	66 c1 e8 08          	shr    $0x8,%ax
f010065e:	0f b6 c0             	movzbl %al,%eax
f0100661:	8b 15 04 69 14 f0    	mov    0xf0146904,%edx
f0100667:	83 c2 01             	add    $0x1,%edx
f010066a:	89 55 ec             	mov    %edx,-0x14(%ebp)
f010066d:	88 45 e1             	mov    %al,-0x1f(%ebp)
f0100670:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
f0100674:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0100677:	ee                   	out    %al,(%dx)
	outb(addr_6845, 15);
f0100678:	a1 04 69 14 f0       	mov    0xf0146904,%eax
f010067d:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0100680:	c6 45 e2 0f          	movb   $0xf,-0x1e(%ebp)
f0100684:	0f b6 45 e2          	movzbl -0x1e(%ebp),%eax
f0100688:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010068b:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos);
f010068c:	0f b7 05 0c 69 14 f0 	movzwl 0xf014690c,%eax
f0100693:	0f b6 c0             	movzbl %al,%eax
f0100696:	8b 15 04 69 14 f0    	mov    0xf0146904,%edx
f010069c:	83 c2 01             	add    $0x1,%edx
f010069f:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f01006a2:	88 45 e3             	mov    %al,-0x1d(%ebp)
f01006a5:	0f b6 45 e3          	movzbl -0x1d(%ebp),%eax
f01006a9:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f01006ac:	ee                   	out    %al,(%dx)
}
f01006ad:	90                   	nop
f01006ae:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01006b1:	c9                   	leave  
f01006b2:	c3                   	ret    

f01006b3 <kbd_proc_data>:
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f01006b3:	55                   	push   %ebp
f01006b4:	89 e5                	mov    %esp,%ebp
f01006b6:	83 ec 28             	sub    $0x28,%esp
f01006b9:	c7 45 e4 64 00 00 00 	movl   $0x64,-0x1c(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01006c0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01006c3:	89 c2                	mov    %eax,%edx
f01006c5:	ec                   	in     (%dx),%al
f01006c6:	88 45 e3             	mov    %al,-0x1d(%ebp)
	return data;
f01006c9:	0f b6 45 e3          	movzbl -0x1d(%ebp),%eax
	int c;
	uint8 data;
	static uint32 shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
f01006cd:	0f b6 c0             	movzbl %al,%eax
f01006d0:	83 e0 01             	and    $0x1,%eax
f01006d3:	85 c0                	test   %eax,%eax
f01006d5:	75 0a                	jne    f01006e1 <kbd_proc_data+0x2e>
		return -1;
f01006d7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01006dc:	e9 5d 01 00 00       	jmp    f010083e <kbd_proc_data+0x18b>
f01006e1:	c7 45 ec 60 00 00 00 	movl   $0x60,-0x14(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01006e8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01006eb:	89 c2                	mov    %eax,%edx
f01006ed:	ec                   	in     (%dx),%al
f01006ee:	88 45 e2             	mov    %al,-0x1e(%ebp)
	return data;
f01006f1:	0f b6 45 e2          	movzbl -0x1e(%ebp),%eax

	data = inb(KBDATAP);
f01006f5:	88 45 f3             	mov    %al,-0xd(%ebp)

	if (data == 0xE0) {
f01006f8:	80 7d f3 e0          	cmpb   $0xe0,-0xd(%ebp)
f01006fc:	75 17                	jne    f0100715 <kbd_proc_data+0x62>
		// E0 escape character
		shift |= E0ESC;
f01006fe:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100703:	83 c8 40             	or     $0x40,%eax
f0100706:	a3 28 6b 14 f0       	mov    %eax,0xf0146b28
		return 0;
f010070b:	b8 00 00 00 00       	mov    $0x0,%eax
f0100710:	e9 29 01 00 00       	jmp    f010083e <kbd_proc_data+0x18b>
	} else if (data & 0x80) {
f0100715:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0100719:	84 c0                	test   %al,%al
f010071b:	79 47                	jns    f0100764 <kbd_proc_data+0xb1>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f010071d:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100722:	83 e0 40             	and    $0x40,%eax
f0100725:	85 c0                	test   %eax,%eax
f0100727:	75 09                	jne    f0100732 <kbd_proc_data+0x7f>
f0100729:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010072d:	83 e0 7f             	and    $0x7f,%eax
f0100730:	eb 04                	jmp    f0100736 <kbd_proc_data+0x83>
f0100732:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0100736:	88 45 f3             	mov    %al,-0xd(%ebp)
		shift &= ~(shiftcode[data] | E0ESC);
f0100739:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010073d:	0f b6 80 20 b0 11 f0 	movzbl -0xfee4fe0(%eax),%eax
f0100744:	83 c8 40             	or     $0x40,%eax
f0100747:	0f b6 c0             	movzbl %al,%eax
f010074a:	f7 d0                	not    %eax
f010074c:	89 c2                	mov    %eax,%edx
f010074e:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100753:	21 d0                	and    %edx,%eax
f0100755:	a3 28 6b 14 f0       	mov    %eax,0xf0146b28
		return 0;
f010075a:	b8 00 00 00 00       	mov    $0x0,%eax
f010075f:	e9 da 00 00 00       	jmp    f010083e <kbd_proc_data+0x18b>
	} else if (shift & E0ESC) {
f0100764:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100769:	83 e0 40             	and    $0x40,%eax
f010076c:	85 c0                	test   %eax,%eax
f010076e:	74 11                	je     f0100781 <kbd_proc_data+0xce>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100770:	80 4d f3 80          	orb    $0x80,-0xd(%ebp)
		shift &= ~E0ESC;
f0100774:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100779:	83 e0 bf             	and    $0xffffffbf,%eax
f010077c:	a3 28 6b 14 f0       	mov    %eax,0xf0146b28
	}

	shift |= shiftcode[data];
f0100781:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f0100785:	0f b6 80 20 b0 11 f0 	movzbl -0xfee4fe0(%eax),%eax
f010078c:	0f b6 d0             	movzbl %al,%edx
f010078f:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100794:	09 d0                	or     %edx,%eax
f0100796:	a3 28 6b 14 f0       	mov    %eax,0xf0146b28
	shift ^= togglecode[data];
f010079b:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f010079f:	0f b6 80 20 b1 11 f0 	movzbl -0xfee4ee0(%eax),%eax
f01007a6:	0f b6 d0             	movzbl %al,%edx
f01007a9:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f01007ae:	31 d0                	xor    %edx,%eax
f01007b0:	a3 28 6b 14 f0       	mov    %eax,0xf0146b28

	c = charcode[shift & (CTL | SHIFT)][data];
f01007b5:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f01007ba:	83 e0 03             	and    $0x3,%eax
f01007bd:	8b 14 85 20 b5 11 f0 	mov    -0xfee4ae0(,%eax,4),%edx
f01007c4:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
f01007c8:	01 d0                	add    %edx,%eax
f01007ca:	0f b6 00             	movzbl (%eax),%eax
f01007cd:	0f b6 c0             	movzbl %al,%eax
f01007d0:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (shift & CAPSLOCK) {
f01007d3:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f01007d8:	83 e0 08             	and    $0x8,%eax
f01007db:	85 c0                	test   %eax,%eax
f01007dd:	74 22                	je     f0100801 <kbd_proc_data+0x14e>
		if ('a' <= c && c <= 'z')
f01007df:	83 7d f4 60          	cmpl   $0x60,-0xc(%ebp)
f01007e3:	7e 0c                	jle    f01007f1 <kbd_proc_data+0x13e>
f01007e5:	83 7d f4 7a          	cmpl   $0x7a,-0xc(%ebp)
f01007e9:	7f 06                	jg     f01007f1 <kbd_proc_data+0x13e>
			c += 'A' - 'a';
f01007eb:	83 6d f4 20          	subl   $0x20,-0xc(%ebp)
f01007ef:	eb 10                	jmp    f0100801 <kbd_proc_data+0x14e>
		else if ('A' <= c && c <= 'Z')
f01007f1:	83 7d f4 40          	cmpl   $0x40,-0xc(%ebp)
f01007f5:	7e 0a                	jle    f0100801 <kbd_proc_data+0x14e>
f01007f7:	83 7d f4 5a          	cmpl   $0x5a,-0xc(%ebp)
f01007fb:	7f 04                	jg     f0100801 <kbd_proc_data+0x14e>
			c += 'a' - 'A';
f01007fd:	83 45 f4 20          	addl   $0x20,-0xc(%ebp)
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f0100801:	a1 28 6b 14 f0       	mov    0xf0146b28,%eax
f0100806:	f7 d0                	not    %eax
f0100808:	83 e0 06             	and    $0x6,%eax
f010080b:	85 c0                	test   %eax,%eax
f010080d:	75 2c                	jne    f010083b <kbd_proc_data+0x188>
f010080f:	81 7d f4 e9 00 00 00 	cmpl   $0xe9,-0xc(%ebp)
f0100816:	75 23                	jne    f010083b <kbd_proc_data+0x188>
		cprintf("Rebooting!\n");
f0100818:	83 ec 0c             	sub    $0xc,%esp
f010081b:	68 46 4f 10 f0       	push   $0xf0104f46
f0100820:	e8 39 28 00 00       	call   f010305e <cprintf>
f0100825:	83 c4 10             	add    $0x10,%esp
f0100828:	c7 45 e8 92 00 00 00 	movl   $0x92,-0x18(%ebp)
f010082f:	c6 45 e1 03          	movb   $0x3,-0x1f(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100833:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
f0100837:	8b 55 e8             	mov    -0x18(%ebp),%edx
f010083a:	ee                   	out    %al,(%dx)
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f010083b:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f010083e:	c9                   	leave  
f010083f:	c3                   	ret    

f0100840 <kbd_intr>:

void
kbd_intr(void)
{
f0100840:	55                   	push   %ebp
f0100841:	89 e5                	mov    %esp,%ebp
f0100843:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f0100846:	83 ec 0c             	sub    $0xc,%esp
f0100849:	68 b3 06 10 f0       	push   $0xf01006b3
f010084e:	e8 0c 00 00 00       	call   f010085f <cons_intr>
f0100853:	83 c4 10             	add    $0x10,%esp
}
f0100856:	90                   	nop
f0100857:	c9                   	leave  
f0100858:	c3                   	ret    

f0100859 <kbd_init>:

void
kbd_init(void)
{
f0100859:	55                   	push   %ebp
f010085a:	89 e5                	mov    %esp,%ebp
}
f010085c:	90                   	nop
f010085d:	5d                   	pop    %ebp
f010085e:	c3                   	ret    

f010085f <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
void
cons_intr(int (*proc)(void))
{
f010085f:	55                   	push   %ebp
f0100860:	89 e5                	mov    %esp,%ebp
f0100862:	83 ec 18             	sub    $0x18,%esp
	int c;

	while ((c = (*proc)()) != -1) {
f0100865:	eb 35                	jmp    f010089c <cons_intr+0x3d>
		if (c == 0)
f0100867:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f010086b:	75 02                	jne    f010086f <cons_intr+0x10>
			continue;
f010086d:	eb 2d                	jmp    f010089c <cons_intr+0x3d>
		cons.buf[cons.wpos++] = c;
f010086f:	a1 24 6b 14 f0       	mov    0xf0146b24,%eax
f0100874:	8d 50 01             	lea    0x1(%eax),%edx
f0100877:	89 15 24 6b 14 f0    	mov    %edx,0xf0146b24
f010087d:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100880:	88 90 20 69 14 f0    	mov    %dl,-0xfeb96e0(%eax)
		if (cons.wpos == CONSBUFSIZE)
f0100886:	a1 24 6b 14 f0       	mov    0xf0146b24,%eax
f010088b:	3d 00 02 00 00       	cmp    $0x200,%eax
f0100890:	75 0a                	jne    f010089c <cons_intr+0x3d>
			cons.wpos = 0;
f0100892:	c7 05 24 6b 14 f0 00 	movl   $0x0,0xf0146b24
f0100899:	00 00 00 
void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f010089c:	8b 45 08             	mov    0x8(%ebp),%eax
f010089f:	ff d0                	call   *%eax
f01008a1:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01008a4:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
f01008a8:	75 bd                	jne    f0100867 <cons_intr+0x8>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01008aa:	90                   	nop
f01008ab:	c9                   	leave  
f01008ac:	c3                   	ret    

f01008ad <cons_getc>:

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01008ad:	55                   	push   %ebp
f01008ae:	89 e5                	mov    %esp,%ebp
f01008b0:	83 ec 18             	sub    $0x18,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01008b3:	e8 65 f9 ff ff       	call   f010021d <serial_intr>
	kbd_intr();
f01008b8:	e8 83 ff ff ff       	call   f0100840 <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f01008bd:	8b 15 20 6b 14 f0    	mov    0xf0146b20,%edx
f01008c3:	a1 24 6b 14 f0       	mov    0xf0146b24,%eax
f01008c8:	39 c2                	cmp    %eax,%edx
f01008ca:	74 36                	je     f0100902 <cons_getc+0x55>
		c = cons.buf[cons.rpos++];
f01008cc:	a1 20 6b 14 f0       	mov    0xf0146b20,%eax
f01008d1:	8d 50 01             	lea    0x1(%eax),%edx
f01008d4:	89 15 20 6b 14 f0    	mov    %edx,0xf0146b20
f01008da:	0f b6 80 20 69 14 f0 	movzbl -0xfeb96e0(%eax),%eax
f01008e1:	0f b6 c0             	movzbl %al,%eax
f01008e4:	89 45 f4             	mov    %eax,-0xc(%ebp)
		if (cons.rpos == CONSBUFSIZE)
f01008e7:	a1 20 6b 14 f0       	mov    0xf0146b20,%eax
f01008ec:	3d 00 02 00 00       	cmp    $0x200,%eax
f01008f1:	75 0a                	jne    f01008fd <cons_getc+0x50>
			cons.rpos = 0;
f01008f3:	c7 05 20 6b 14 f0 00 	movl   $0x0,0xf0146b20
f01008fa:	00 00 00 
		return c;
f01008fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100900:	eb 05                	jmp    f0100907 <cons_getc+0x5a>
	}
	return 0;
f0100902:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100907:	c9                   	leave  
f0100908:	c3                   	ret    

f0100909 <cons_putc>:

// output a character to the console
void
cons_putc(int c)
{
f0100909:	55                   	push   %ebp
f010090a:	89 e5                	mov    %esp,%ebp
f010090c:	83 ec 08             	sub    $0x8,%esp
	lpt_putc(c);
f010090f:	ff 75 08             	pushl  0x8(%ebp)
f0100912:	e8 40 fa ff ff       	call   f0100357 <lpt_putc>
f0100917:	83 c4 04             	add    $0x4,%esp
	cga_putc(c);
f010091a:	83 ec 0c             	sub    $0xc,%esp
f010091d:	ff 75 08             	pushl  0x8(%ebp)
f0100920:	e8 79 fb ff ff       	call   f010049e <cga_putc>
f0100925:	83 c4 10             	add    $0x10,%esp
}
f0100928:	90                   	nop
f0100929:	c9                   	leave  
f010092a:	c3                   	ret    

f010092b <console_initialize>:

// initialize the console devices
void
console_initialize(void)
{
f010092b:	55                   	push   %ebp
f010092c:	89 e5                	mov    %esp,%ebp
f010092e:	83 ec 08             	sub    $0x8,%esp
	cga_init();
f0100931:	e8 9b fa ff ff       	call   f01003d1 <cga_init>
	kbd_init();
f0100936:	e8 1e ff ff ff       	call   f0100859 <kbd_init>
	serial_init();
f010093b:	e8 ff f8 ff ff       	call   f010023f <serial_init>

	if (!serial_exists)
f0100940:	a1 00 69 14 f0       	mov    0xf0146900,%eax
f0100945:	85 c0                	test   %eax,%eax
f0100947:	75 10                	jne    f0100959 <console_initialize+0x2e>
		cprintf("Serial port does not exist!\n");
f0100949:	83 ec 0c             	sub    $0xc,%esp
f010094c:	68 52 4f 10 f0       	push   $0xf0104f52
f0100951:	e8 08 27 00 00       	call   f010305e <cprintf>
f0100956:	83 c4 10             	add    $0x10,%esp
}
f0100959:	90                   	nop
f010095a:	c9                   	leave  
f010095b:	c3                   	ret    

f010095c <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f010095c:	55                   	push   %ebp
f010095d:	89 e5                	mov    %esp,%ebp
f010095f:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f0100962:	83 ec 0c             	sub    $0xc,%esp
f0100965:	ff 75 08             	pushl  0x8(%ebp)
f0100968:	e8 9c ff ff ff       	call   f0100909 <cons_putc>
f010096d:	83 c4 10             	add    $0x10,%esp
}
f0100970:	90                   	nop
f0100971:	c9                   	leave  
f0100972:	c3                   	ret    

f0100973 <getchar>:

int
getchar(void)
{
f0100973:	55                   	push   %ebp
f0100974:	89 e5                	mov    %esp,%ebp
f0100976:	83 ec 18             	sub    $0x18,%esp
	int c;

	while ((c = cons_getc()) == 0)
f0100979:	e8 2f ff ff ff       	call   f01008ad <cons_getc>
f010097e:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100981:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0100985:	74 f2                	je     f0100979 <getchar+0x6>
		/* do nothing */;
	return c;
f0100987:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f010098a:	c9                   	leave  
f010098b:	c3                   	ret    

f010098c <iscons>:

int
iscons(int fdnum)
{
f010098c:	55                   	push   %ebp
f010098d:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
f010098f:	b8 01 00 00 00       	mov    $0x1,%eax
}
f0100994:	5d                   	pop    %ebp
f0100995:	c3                   	ret    

f0100996 <run_command_prompt>:
unsigned read_eip();


//invoke the command prompt
void run_command_prompt()
{
f0100996:	55                   	push   %ebp
f0100997:	89 e5                	mov    %esp,%ebp
f0100999:	81 ec 08 04 00 00    	sub    $0x408,%esp
	char command_line[1024];

	while (1==1) 
	{
		//get command line
		readline("FOS> ", command_line);
f010099f:	83 ec 08             	sub    $0x8,%esp
f01009a2:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
f01009a8:	50                   	push   %eax
f01009a9:	68 87 51 10 f0       	push   $0xf0105187
f01009ae:	e8 b9 3a 00 00       	call   f010446c <readline>
f01009b3:	83 c4 10             	add    $0x10,%esp
		
		//parse and execute the command
		if (command_line != NULL)
			if (execute_command(command_line) < 0)
f01009b6:	83 ec 0c             	sub    $0xc,%esp
f01009b9:	8d 85 f8 fb ff ff    	lea    -0x408(%ebp),%eax
f01009bf:	50                   	push   %eax
f01009c0:	e8 0d 00 00 00       	call   f01009d2 <execute_command>
f01009c5:	83 c4 10             	add    $0x10,%esp
f01009c8:	85 c0                	test   %eax,%eax
f01009ca:	78 02                	js     f01009ce <run_command_prompt+0x38>
				break;
	}
f01009cc:	eb d1                	jmp    f010099f <run_command_prompt+0x9>
		readline("FOS> ", command_line);
		
		//parse and execute the command
		if (command_line != NULL)
			if (execute_command(command_line) < 0)
				break;
f01009ce:	90                   	nop
	}
}
f01009cf:	90                   	nop
f01009d0:	c9                   	leave  
f01009d1:	c3                   	ret    

f01009d2 <execute_command>:
#define WHITESPACE "\t\r\n "

//Function to parse any command and execute it 
//(simply by calling its corresponding function)
int execute_command(char *command_string)
{
f01009d2:	55                   	push   %ebp
f01009d3:	89 e5                	mov    %esp,%ebp
f01009d5:	83 ec 58             	sub    $0x58,%esp
	int number_of_arguments;
	//allocate array of char * of size MAX_ARGUMENTS = 16 found in string.h
	char *arguments[MAX_ARGUMENTS];


	strsplit(command_string, WHITESPACE, arguments, &number_of_arguments) ;
f01009d8:	8d 45 e8             	lea    -0x18(%ebp),%eax
f01009db:	50                   	push   %eax
f01009dc:	8d 45 a8             	lea    -0x58(%ebp),%eax
f01009df:	50                   	push   %eax
f01009e0:	68 8d 51 10 f0       	push   $0xf010518d
f01009e5:	ff 75 08             	pushl  0x8(%ebp)
f01009e8:	e8 6e 40 00 00       	call   f0104a5b <strsplit>
f01009ed:	83 c4 10             	add    $0x10,%esp
	if (number_of_arguments == 0)
f01009f0:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01009f3:	85 c0                	test   %eax,%eax
f01009f5:	75 0a                	jne    f0100a01 <execute_command+0x2f>
		return 0;
f01009f7:	b8 00 00 00 00       	mov    $0x0,%eax
f01009fc:	e9 96 00 00 00       	jmp    f0100a97 <execute_command+0xc5>
	
	// Lookup in the commands array and execute the command
	int command_found = 0;
f0100a01:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i ;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100a08:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
f0100a0f:	eb 34                	jmp    f0100a45 <execute_command+0x73>
	{
		if (strcmp(arguments[0], commands[i].name) == 0)
f0100a11:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100a14:	89 d0                	mov    %edx,%eax
f0100a16:	01 c0                	add    %eax,%eax
f0100a18:	01 d0                	add    %edx,%eax
f0100a1a:	c1 e0 02             	shl    $0x2,%eax
f0100a1d:	05 40 b5 11 f0       	add    $0xf011b540,%eax
f0100a22:	8b 10                	mov    (%eax),%edx
f0100a24:	8b 45 a8             	mov    -0x58(%ebp),%eax
f0100a27:	83 ec 08             	sub    $0x8,%esp
f0100a2a:	52                   	push   %edx
f0100a2b:	50                   	push   %eax
f0100a2c:	e8 56 3c 00 00       	call   f0104687 <strcmp>
f0100a31:	83 c4 10             	add    $0x10,%esp
f0100a34:	85 c0                	test   %eax,%eax
f0100a36:	75 09                	jne    f0100a41 <execute_command+0x6f>
		{
			command_found = 1;
f0100a38:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
			break;
f0100a3f:	eb 0c                	jmp    f0100a4d <execute_command+0x7b>
		return 0;
	
	// Lookup in the commands array and execute the command
	int command_found = 0;
	int i ;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100a41:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
f0100a45:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100a48:	83 f8 08             	cmp    $0x8,%eax
f0100a4b:	76 c4                	jbe    f0100a11 <execute_command+0x3f>
			command_found = 1;
			break;
		}
	}
	
	if(command_found)
f0100a4d:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0100a51:	74 2b                	je     f0100a7e <execute_command+0xac>
	{
		int return_value;
		return_value = commands[i].function_to_execute(number_of_arguments, arguments);			
f0100a53:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0100a56:	89 d0                	mov    %edx,%eax
f0100a58:	01 c0                	add    %eax,%eax
f0100a5a:	01 d0                	add    %edx,%eax
f0100a5c:	c1 e0 02             	shl    $0x2,%eax
f0100a5f:	05 48 b5 11 f0       	add    $0xf011b548,%eax
f0100a64:	8b 00                	mov    (%eax),%eax
f0100a66:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0100a69:	83 ec 08             	sub    $0x8,%esp
f0100a6c:	8d 4d a8             	lea    -0x58(%ebp),%ecx
f0100a6f:	51                   	push   %ecx
f0100a70:	52                   	push   %edx
f0100a71:	ff d0                	call   *%eax
f0100a73:	83 c4 10             	add    $0x10,%esp
f0100a76:	89 45 ec             	mov    %eax,-0x14(%ebp)
		return return_value;
f0100a79:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100a7c:	eb 19                	jmp    f0100a97 <execute_command+0xc5>
	}
	else
	{
		//if not found, then it's unknown command
		cprintf("Unknown command '%s'\n", arguments[0]);
f0100a7e:	8b 45 a8             	mov    -0x58(%ebp),%eax
f0100a81:	83 ec 08             	sub    $0x8,%esp
f0100a84:	50                   	push   %eax
f0100a85:	68 92 51 10 f0       	push   $0xf0105192
f0100a8a:	e8 cf 25 00 00       	call   f010305e <cprintf>
f0100a8f:	83 c4 10             	add    $0x10,%esp
		return 0;
f0100a92:	b8 00 00 00 00       	mov    $0x0,%eax
	}
}
f0100a97:	c9                   	leave  
f0100a98:	c3                   	ret    

f0100a99 <command_help>:

/***** Implementations of basic kernel command prompt commands *****/

//print name and description of each command
int command_help(int number_of_arguments, char **arguments)
{
f0100a99:	55                   	push   %ebp
f0100a9a:	89 e5                	mov    %esp,%ebp
f0100a9c:	83 ec 18             	sub    $0x18,%esp
	int i;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100a9f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0100aa6:	eb 3c                	jmp    f0100ae4 <command_help+0x4b>
		cprintf("%s - %s\n", commands[i].name, commands[i].description);
f0100aa8:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100aab:	89 d0                	mov    %edx,%eax
f0100aad:	01 c0                	add    %eax,%eax
f0100aaf:	01 d0                	add    %edx,%eax
f0100ab1:	c1 e0 02             	shl    $0x2,%eax
f0100ab4:	05 44 b5 11 f0       	add    $0xf011b544,%eax
f0100ab9:	8b 08                	mov    (%eax),%ecx
f0100abb:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0100abe:	89 d0                	mov    %edx,%eax
f0100ac0:	01 c0                	add    %eax,%eax
f0100ac2:	01 d0                	add    %edx,%eax
f0100ac4:	c1 e0 02             	shl    $0x2,%eax
f0100ac7:	05 40 b5 11 f0       	add    $0xf011b540,%eax
f0100acc:	8b 00                	mov    (%eax),%eax
f0100ace:	83 ec 04             	sub    $0x4,%esp
f0100ad1:	51                   	push   %ecx
f0100ad2:	50                   	push   %eax
f0100ad3:	68 a8 51 10 f0       	push   $0xf01051a8
f0100ad8:	e8 81 25 00 00       	call   f010305e <cprintf>
f0100add:	83 c4 10             	add    $0x10,%esp

//print name and description of each command
int command_help(int number_of_arguments, char **arguments)
{
	int i;
	for (i = 0; i < NUM_OF_COMMANDS; i++)
f0100ae0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0100ae4:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100ae7:	83 f8 08             	cmp    $0x8,%eax
f0100aea:	76 bc                	jbe    f0100aa8 <command_help+0xf>
		cprintf("%s - %s\n", commands[i].name, commands[i].description);
	
	cprintf("-------------------\n");
f0100aec:	83 ec 0c             	sub    $0xc,%esp
f0100aef:	68 b1 51 10 f0       	push   $0xf01051b1
f0100af4:	e8 65 25 00 00       	call   f010305e <cprintf>
f0100af9:	83 c4 10             	add    $0x10,%esp
	
	return 0;
f0100afc:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100b01:	c9                   	leave  
f0100b02:	c3                   	ret    

f0100b03 <command_kernel_info>:

//print information about kernel addresses and kernel size
int command_kernel_info(int number_of_arguments, char **arguments )
{
f0100b03:	55                   	push   %ebp
f0100b04:	89 e5                	mov    %esp,%ebp
f0100b06:	83 ec 08             	sub    $0x8,%esp
	extern char start_of_kernel[], end_of_kernel_code_section[], start_of_uninitialized_data_section[], end_of_kernel[];

	cprintf("Special kernel symbols:\n");
f0100b09:	83 ec 0c             	sub    $0xc,%esp
f0100b0c:	68 c6 51 10 f0       	push   $0xf01051c6
f0100b11:	e8 48 25 00 00       	call   f010305e <cprintf>
f0100b16:	83 c4 10             	add    $0x10,%esp
	cprintf("  Start Address of the kernel 			%08x (virt)  %08x (phys)\n", start_of_kernel, start_of_kernel - KERNEL_BASE);
f0100b19:	b8 0c 00 10 00       	mov    $0x10000c,%eax
f0100b1e:	83 ec 04             	sub    $0x4,%esp
f0100b21:	50                   	push   %eax
f0100b22:	68 0c 00 10 f0       	push   $0xf010000c
f0100b27:	68 e0 51 10 f0       	push   $0xf01051e0
f0100b2c:	e8 2d 25 00 00       	call   f010305e <cprintf>
f0100b31:	83 c4 10             	add    $0x10,%esp
	cprintf("  End address of kernel code  			%08x (virt)  %08x (phys)\n", end_of_kernel_code_section, end_of_kernel_code_section - KERNEL_BASE);
f0100b34:	b8 c1 4d 10 00       	mov    $0x104dc1,%eax
f0100b39:	83 ec 04             	sub    $0x4,%esp
f0100b3c:	50                   	push   %eax
f0100b3d:	68 c1 4d 10 f0       	push   $0xf0104dc1
f0100b42:	68 1c 52 10 f0       	push   $0xf010521c
f0100b47:	e8 12 25 00 00       	call   f010305e <cprintf>
f0100b4c:	83 c4 10             	add    $0x10,%esp
	cprintf("  Start addr. of uninitialized data section 	%08x (virt)  %08x (phys)\n", start_of_uninitialized_data_section, start_of_uninitialized_data_section - KERNEL_BASE);
f0100b4f:	b8 c2 68 14 00       	mov    $0x1468c2,%eax
f0100b54:	83 ec 04             	sub    $0x4,%esp
f0100b57:	50                   	push   %eax
f0100b58:	68 c2 68 14 f0       	push   $0xf01468c2
f0100b5d:	68 58 52 10 f0       	push   $0xf0105258
f0100b62:	e8 f7 24 00 00       	call   f010305e <cprintf>
f0100b67:	83 c4 10             	add    $0x10,%esp
	cprintf("  End address of the kernel   			%08x (virt)  %08x (phys)\n", end_of_kernel, end_of_kernel - KERNEL_BASE);
f0100b6a:	b8 cc 73 14 00       	mov    $0x1473cc,%eax
f0100b6f:	83 ec 04             	sub    $0x4,%esp
f0100b72:	50                   	push   %eax
f0100b73:	68 cc 73 14 f0       	push   $0xf01473cc
f0100b78:	68 a0 52 10 f0       	push   $0xf01052a0
f0100b7d:	e8 dc 24 00 00       	call   f010305e <cprintf>
f0100b82:	83 c4 10             	add    $0x10,%esp
	cprintf("Kernel executable memory footprint: %d KB\n",
		(end_of_kernel-start_of_kernel+1023)/1024);
f0100b85:	b8 cc 73 14 f0       	mov    $0xf01473cc,%eax
f0100b8a:	05 ff 03 00 00       	add    $0x3ff,%eax
f0100b8f:	ba 0c 00 10 f0       	mov    $0xf010000c,%edx
f0100b94:	29 d0                	sub    %edx,%eax
	cprintf("Special kernel symbols:\n");
	cprintf("  Start Address of the kernel 			%08x (virt)  %08x (phys)\n", start_of_kernel, start_of_kernel - KERNEL_BASE);
	cprintf("  End address of kernel code  			%08x (virt)  %08x (phys)\n", end_of_kernel_code_section, end_of_kernel_code_section - KERNEL_BASE);
	cprintf("  Start addr. of uninitialized data section 	%08x (virt)  %08x (phys)\n", start_of_uninitialized_data_section, start_of_uninitialized_data_section - KERNEL_BASE);
	cprintf("  End address of the kernel   			%08x (virt)  %08x (phys)\n", end_of_kernel, end_of_kernel - KERNEL_BASE);
	cprintf("Kernel executable memory footprint: %d KB\n",
f0100b96:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
f0100b9c:	85 c0                	test   %eax,%eax
f0100b9e:	0f 48 c2             	cmovs  %edx,%eax
f0100ba1:	c1 f8 0a             	sar    $0xa,%eax
f0100ba4:	83 ec 08             	sub    $0x8,%esp
f0100ba7:	50                   	push   %eax
f0100ba8:	68 dc 52 10 f0       	push   $0xf01052dc
f0100bad:	e8 ac 24 00 00       	call   f010305e <cprintf>
f0100bb2:	83 c4 10             	add    $0x10,%esp
		(end_of_kernel-start_of_kernel+1023)/1024);
	return 0;
f0100bb5:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100bba:	c9                   	leave  
f0100bbb:	c3                   	ret    

f0100bbc <command_writemem>:

int command_writemem(int number_of_arguments, char **arguments)
{
f0100bbc:	55                   	push   %ebp
f0100bbd:	89 e5                	mov    %esp,%ebp
f0100bbf:	83 ec 38             	sub    $0x38,%esp
	char* user_program_name = arguments[1];		
f0100bc2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100bc5:	8b 40 04             	mov    0x4(%eax),%eax
f0100bc8:	89 45 f4             	mov    %eax,-0xc(%ebp)
	int address = strtol(arguments[3], NULL, 16);
f0100bcb:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100bce:	83 c0 0c             	add    $0xc,%eax
f0100bd1:	8b 00                	mov    (%eax),%eax
f0100bd3:	83 ec 04             	sub    $0x4,%esp
f0100bd6:	6a 10                	push   $0x10
f0100bd8:	6a 00                	push   $0x0
f0100bda:	50                   	push   %eax
f0100bdb:	e8 1e 3d 00 00       	call   f01048fe <strtol>
f0100be0:	83 c4 10             	add    $0x10,%esp
f0100be3:	89 45 f0             	mov    %eax,-0x10(%ebp)

	struct UserProgramInfo* ptr_user_program_info = get_user_program_info(user_program_name);
f0100be6:	83 ec 0c             	sub    $0xc,%esp
f0100be9:	ff 75 f4             	pushl  -0xc(%ebp)
f0100bec:	e8 90 21 00 00       	call   f0102d81 <get_user_program_info>
f0100bf1:	83 c4 10             	add    $0x10,%esp
f0100bf4:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if(ptr_user_program_info == NULL) return 0;	
f0100bf7:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f0100bfb:	75 07                	jne    f0100c04 <command_writemem+0x48>
f0100bfd:	b8 00 00 00 00       	mov    $0x0,%eax
f0100c02:	eb 72                	jmp    f0100c76 <command_writemem+0xba>

static __inline uint32
rcr3(void)
{
	uint32 val;
	__asm __volatile("movl %%cr3,%0" : "=r" (val));
f0100c04:	0f 20 d8             	mov    %cr3,%eax
f0100c07:	89 45 d4             	mov    %eax,-0x2c(%ebp)
	return val;
f0100c0a:	8b 45 d4             	mov    -0x2c(%ebp),%eax

	uint32 oldDir = rcr3();
f0100c0d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	lcr3((uint32) K_PHYSICAL_ADDRESS( ptr_user_program_info->environment->env_pgdir));
f0100c10:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100c13:	8b 40 0c             	mov    0xc(%eax),%eax
f0100c16:	8b 40 5c             	mov    0x5c(%eax),%eax
f0100c19:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0100c1c:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0100c23:	77 17                	ja     f0100c3c <command_writemem+0x80>
f0100c25:	ff 75 e0             	pushl  -0x20(%ebp)
f0100c28:	68 08 53 10 f0       	push   $0xf0105308
f0100c2d:	68 a3 00 00 00       	push   $0xa3
f0100c32:	68 39 53 10 f0       	push   $0xf0105339
f0100c37:	e8 f2 f4 ff ff       	call   f010012e <_panic>
f0100c3c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100c3f:	05 00 00 00 10       	add    $0x10000000,%eax
f0100c44:	89 45 d8             	mov    %eax,-0x28(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0100c47:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100c4a:	0f 22 d8             	mov    %eax,%cr3

	unsigned char *ptr = (unsigned char *)(address) ; 
f0100c4d:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100c50:	89 45 dc             	mov    %eax,-0x24(%ebp)

	//Write the given Character
	*ptr = arguments[2][0];
f0100c53:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100c56:	83 c0 08             	add    $0x8,%eax
f0100c59:	8b 00                	mov    (%eax),%eax
f0100c5b:	0f b6 00             	movzbl (%eax),%eax
f0100c5e:	89 c2                	mov    %eax,%edx
f0100c60:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0100c63:	88 10                	mov    %dl,(%eax)
f0100c65:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100c68:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0100c6b:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0100c6e:	0f 22 d8             	mov    %eax,%cr3
	lcr3(oldDir);	

	return 0;
f0100c71:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100c76:	c9                   	leave  
f0100c77:	c3                   	ret    

f0100c78 <command_readmem>:

int command_readmem(int number_of_arguments, char **arguments)
{
f0100c78:	55                   	push   %ebp
f0100c79:	89 e5                	mov    %esp,%ebp
f0100c7b:	83 ec 38             	sub    $0x38,%esp
	char* user_program_name = arguments[1];		
f0100c7e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100c81:	8b 40 04             	mov    0x4(%eax),%eax
f0100c84:	89 45 f4             	mov    %eax,-0xc(%ebp)
	int address = strtol(arguments[2], NULL, 16);
f0100c87:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100c8a:	83 c0 08             	add    $0x8,%eax
f0100c8d:	8b 00                	mov    (%eax),%eax
f0100c8f:	83 ec 04             	sub    $0x4,%esp
f0100c92:	6a 10                	push   $0x10
f0100c94:	6a 00                	push   $0x0
f0100c96:	50                   	push   %eax
f0100c97:	e8 62 3c 00 00       	call   f01048fe <strtol>
f0100c9c:	83 c4 10             	add    $0x10,%esp
f0100c9f:	89 45 f0             	mov    %eax,-0x10(%ebp)

	struct UserProgramInfo* ptr_user_program_info = get_user_program_info(user_program_name);
f0100ca2:	83 ec 0c             	sub    $0xc,%esp
f0100ca5:	ff 75 f4             	pushl  -0xc(%ebp)
f0100ca8:	e8 d4 20 00 00       	call   f0102d81 <get_user_program_info>
f0100cad:	83 c4 10             	add    $0x10,%esp
f0100cb0:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if(ptr_user_program_info == NULL) return 0;	
f0100cb3:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f0100cb7:	75 07                	jne    f0100cc0 <command_readmem+0x48>
f0100cb9:	b8 00 00 00 00       	mov    $0x0,%eax
f0100cbe:	eb 7d                	jmp    f0100d3d <command_readmem+0xc5>

static __inline uint32
rcr3(void)
{
	uint32 val;
	__asm __volatile("movl %%cr3,%0" : "=r" (val));
f0100cc0:	0f 20 d8             	mov    %cr3,%eax
f0100cc3:	89 45 d4             	mov    %eax,-0x2c(%ebp)
	return val;
f0100cc6:	8b 45 d4             	mov    -0x2c(%ebp),%eax

	uint32 oldDir = rcr3();
f0100cc9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	lcr3((uint32) K_PHYSICAL_ADDRESS( ptr_user_program_info->environment->env_pgdir));
f0100ccc:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0100ccf:	8b 40 0c             	mov    0xc(%eax),%eax
f0100cd2:	8b 40 5c             	mov    0x5c(%eax),%eax
f0100cd5:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0100cd8:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0100cdf:	77 17                	ja     f0100cf8 <command_readmem+0x80>
f0100ce1:	ff 75 e0             	pushl  -0x20(%ebp)
f0100ce4:	68 08 53 10 f0       	push   $0xf0105308
f0100ce9:	68 b7 00 00 00       	push   $0xb7
f0100cee:	68 39 53 10 f0       	push   $0xf0105339
f0100cf3:	e8 36 f4 ff ff       	call   f010012e <_panic>
f0100cf8:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100cfb:	05 00 00 00 10       	add    $0x10000000,%eax
f0100d00:	89 45 d8             	mov    %eax,-0x28(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0100d03:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100d06:	0f 22 d8             	mov    %eax,%cr3

	unsigned char *ptr = (unsigned char *)(address) ;
f0100d09:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100d0c:	89 45 dc             	mov    %eax,-0x24(%ebp)
	
	//Write the given Character
	cprintf("value at address %x = %c\n", address, *ptr);
f0100d0f:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0100d12:	0f b6 00             	movzbl (%eax),%eax
f0100d15:	0f b6 c0             	movzbl %al,%eax
f0100d18:	83 ec 04             	sub    $0x4,%esp
f0100d1b:	50                   	push   %eax
f0100d1c:	ff 75 f0             	pushl  -0x10(%ebp)
f0100d1f:	68 4f 53 10 f0       	push   $0xf010534f
f0100d24:	e8 35 23 00 00       	call   f010305e <cprintf>
f0100d29:	83 c4 10             	add    $0x10,%esp
f0100d2c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100d2f:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0100d32:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0100d35:	0f 22 d8             	mov    %eax,%cr3
	
	lcr3(oldDir);	
	return 0;
f0100d38:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100d3d:	c9                   	leave  
f0100d3e:	c3                   	ret    

f0100d3f <command_readblock>:

int command_readblock(int number_of_arguments, char **arguments)
{
f0100d3f:	55                   	push   %ebp
f0100d40:	89 e5                	mov    %esp,%ebp
f0100d42:	83 ec 38             	sub    $0x38,%esp
	char* user_program_name = arguments[1];	
f0100d45:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100d48:	8b 40 04             	mov    0x4(%eax),%eax
f0100d4b:	89 45 ec             	mov    %eax,-0x14(%ebp)
	int address = strtol(arguments[2], NULL, 16);
f0100d4e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100d51:	83 c0 08             	add    $0x8,%eax
f0100d54:	8b 00                	mov    (%eax),%eax
f0100d56:	83 ec 04             	sub    $0x4,%esp
f0100d59:	6a 10                	push   $0x10
f0100d5b:	6a 00                	push   $0x0
f0100d5d:	50                   	push   %eax
f0100d5e:	e8 9b 3b 00 00       	call   f01048fe <strtol>
f0100d63:	83 c4 10             	add    $0x10,%esp
f0100d66:	89 45 e8             	mov    %eax,-0x18(%ebp)
	int nBytes = strtol(arguments[3], NULL, 10);
f0100d69:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100d6c:	83 c0 0c             	add    $0xc,%eax
f0100d6f:	8b 00                	mov    (%eax),%eax
f0100d71:	83 ec 04             	sub    $0x4,%esp
f0100d74:	6a 0a                	push   $0xa
f0100d76:	6a 00                	push   $0x0
f0100d78:	50                   	push   %eax
f0100d79:	e8 80 3b 00 00       	call   f01048fe <strtol>
f0100d7e:	83 c4 10             	add    $0x10,%esp
f0100d81:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	 
	unsigned char *ptr = (unsigned char *)(address) ;
f0100d84:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0100d87:	89 45 f4             	mov    %eax,-0xc(%ebp)
	//Write the given Character	
		
	struct UserProgramInfo* ptr_user_program_info = get_user_program_info(user_program_name);
f0100d8a:	83 ec 0c             	sub    $0xc,%esp
f0100d8d:	ff 75 ec             	pushl  -0x14(%ebp)
f0100d90:	e8 ec 1f 00 00       	call   f0102d81 <get_user_program_info>
f0100d95:	83 c4 10             	add    $0x10,%esp
f0100d98:	89 45 e0             	mov    %eax,-0x20(%ebp)
	if(ptr_user_program_info == NULL) return 0;	
f0100d9b:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f0100d9f:	75 0a                	jne    f0100dab <command_readblock+0x6c>
f0100da1:	b8 00 00 00 00       	mov    $0x0,%eax
f0100da6:	e9 97 00 00 00       	jmp    f0100e42 <command_readblock+0x103>

static __inline uint32
rcr3(void)
{
	uint32 val;
	__asm __volatile("movl %%cr3,%0" : "=r" (val));
f0100dab:	0f 20 d8             	mov    %cr3,%eax
f0100dae:	89 45 cc             	mov    %eax,-0x34(%ebp)
	return val;
f0100db1:	8b 45 cc             	mov    -0x34(%ebp),%eax

	uint32 oldDir = rcr3();
f0100db4:	89 45 d8             	mov    %eax,-0x28(%ebp)
	lcr3((uint32) K_PHYSICAL_ADDRESS( ptr_user_program_info->environment->env_pgdir));
f0100db7:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100dba:	8b 40 0c             	mov    0xc(%eax),%eax
f0100dbd:	8b 40 5c             	mov    0x5c(%eax),%eax
f0100dc0:	89 45 d4             	mov    %eax,-0x2c(%ebp)
f0100dc3:	81 7d d4 ff ff ff ef 	cmpl   $0xefffffff,-0x2c(%ebp)
f0100dca:	77 17                	ja     f0100de3 <command_readblock+0xa4>
f0100dcc:	ff 75 d4             	pushl  -0x2c(%ebp)
f0100dcf:	68 08 53 10 f0       	push   $0xf0105308
f0100dd4:	68 cf 00 00 00       	push   $0xcf
f0100dd9:	68 39 53 10 f0       	push   $0xf0105339
f0100dde:	e8 4b f3 ff ff       	call   f010012e <_panic>
f0100de3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0100de6:	05 00 00 00 10       	add    $0x10000000,%eax
f0100deb:	89 45 dc             	mov    %eax,-0x24(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0100dee:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0100df1:	0f 22 d8             	mov    %eax,%cr3

	int i;	
	for(i = 0;i<nBytes; i++)
f0100df4:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
f0100dfb:	eb 2c                	jmp    f0100e29 <command_readblock+0xea>
	{
		cprintf("%08x : %02x  %c\n", ptr, *ptr, *ptr);
f0100dfd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100e00:	0f b6 00             	movzbl (%eax),%eax
f0100e03:	0f b6 d0             	movzbl %al,%edx
f0100e06:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100e09:	0f b6 00             	movzbl (%eax),%eax
f0100e0c:	0f b6 c0             	movzbl %al,%eax
f0100e0f:	52                   	push   %edx
f0100e10:	50                   	push   %eax
f0100e11:	ff 75 f4             	pushl  -0xc(%ebp)
f0100e14:	68 69 53 10 f0       	push   $0xf0105369
f0100e19:	e8 40 22 00 00       	call   f010305e <cprintf>
f0100e1e:	83 c4 10             	add    $0x10,%esp
		ptr++;
f0100e21:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)

	uint32 oldDir = rcr3();
	lcr3((uint32) K_PHYSICAL_ADDRESS( ptr_user_program_info->environment->env_pgdir));

	int i;	
	for(i = 0;i<nBytes; i++)
f0100e25:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
f0100e29:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100e2c:	3b 45 e4             	cmp    -0x1c(%ebp),%eax
f0100e2f:	7c cc                	jl     f0100dfd <command_readblock+0xbe>
f0100e31:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0100e34:	89 45 d0             	mov    %eax,-0x30(%ebp)
f0100e37:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0100e3a:	0f 22 d8             	mov    %eax,%cr3
		cprintf("%08x : %02x  %c\n", ptr, *ptr, *ptr);
		ptr++;
	}
	lcr3(oldDir);

	return 0;
f0100e3d:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100e42:	c9                   	leave  
f0100e43:	c3                   	ret    

f0100e44 <command_allocpage>:

int command_allocpage(int number_of_arguments, char **arguments)
{
f0100e44:	55                   	push   %ebp
f0100e45:	89 e5                	mov    %esp,%ebp
f0100e47:	83 ec 18             	sub    $0x18,%esp
	unsigned int address = strtol(arguments[1], NULL, 16); 
f0100e4a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100e4d:	83 c0 04             	add    $0x4,%eax
f0100e50:	8b 00                	mov    (%eax),%eax
f0100e52:	83 ec 04             	sub    $0x4,%esp
f0100e55:	6a 10                	push   $0x10
f0100e57:	6a 00                	push   $0x0
f0100e59:	50                   	push   %eax
f0100e5a:	e8 9f 3a 00 00       	call   f01048fe <strtol>
f0100e5f:	83 c4 10             	add    $0x10,%esp
f0100e62:	89 45 f4             	mov    %eax,-0xc(%ebp)
	unsigned char *ptr = (unsigned char *)(address) ;
f0100e65:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100e68:	89 45 f0             	mov    %eax,-0x10(%ebp)
	
	struct Frame_Info * ptr_frame_info ;
	allocate_frame(&ptr_frame_info);
f0100e6b:	83 ec 0c             	sub    $0xc,%esp
f0100e6e:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0100e71:	50                   	push   %eax
f0100e72:	e8 f4 15 00 00       	call   f010246b <allocate_frame>
f0100e77:	83 c4 10             	add    $0x10,%esp
	
	map_frame(ptr_page_directory, ptr_frame_info, ptr, PERM_WRITEABLE|PERM_USER);
f0100e7a:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0100e7d:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0100e82:	6a 06                	push   $0x6
f0100e84:	ff 75 f0             	pushl  -0x10(%ebp)
f0100e87:	52                   	push   %edx
f0100e88:	50                   	push   %eax
f0100e89:	e8 ee 17 00 00       	call   f010267c <map_frame>
f0100e8e:	83 c4 10             	add    $0x10,%esp
	
	return 0;
f0100e91:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100e96:	c9                   	leave  
f0100e97:	c3                   	ret    

f0100e98 <command_readusermem>:


int command_readusermem(int number_of_arguments, char **arguments)
{
f0100e98:	55                   	push   %ebp
f0100e99:	89 e5                	mov    %esp,%ebp
f0100e9b:	83 ec 18             	sub    $0x18,%esp
	unsigned int address = strtol(arguments[1], NULL, 16); 
f0100e9e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100ea1:	83 c0 04             	add    $0x4,%eax
f0100ea4:	8b 00                	mov    (%eax),%eax
f0100ea6:	83 ec 04             	sub    $0x4,%esp
f0100ea9:	6a 10                	push   $0x10
f0100eab:	6a 00                	push   $0x0
f0100ead:	50                   	push   %eax
f0100eae:	e8 4b 3a 00 00       	call   f01048fe <strtol>
f0100eb3:	83 c4 10             	add    $0x10,%esp
f0100eb6:	89 45 f4             	mov    %eax,-0xc(%ebp)
	unsigned char *ptr = (unsigned char *)(address) ;
f0100eb9:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100ebc:	89 45 f0             	mov    %eax,-0x10(%ebp)
	
	cprintf("value at address %x = %c\n", ptr, *ptr);
f0100ebf:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100ec2:	0f b6 00             	movzbl (%eax),%eax
f0100ec5:	0f b6 c0             	movzbl %al,%eax
f0100ec8:	83 ec 04             	sub    $0x4,%esp
f0100ecb:	50                   	push   %eax
f0100ecc:	ff 75 f0             	pushl  -0x10(%ebp)
f0100ecf:	68 4f 53 10 f0       	push   $0xf010534f
f0100ed4:	e8 85 21 00 00       	call   f010305e <cprintf>
f0100ed9:	83 c4 10             	add    $0x10,%esp
	
	return 0;
f0100edc:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100ee1:	c9                   	leave  
f0100ee2:	c3                   	ret    

f0100ee3 <command_writeusermem>:
int command_writeusermem(int number_of_arguments, char **arguments)
{
f0100ee3:	55                   	push   %ebp
f0100ee4:	89 e5                	mov    %esp,%ebp
f0100ee6:	83 ec 18             	sub    $0x18,%esp
	unsigned int address = strtol(arguments[1], NULL, 16); 
f0100ee9:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100eec:	83 c0 04             	add    $0x4,%eax
f0100eef:	8b 00                	mov    (%eax),%eax
f0100ef1:	83 ec 04             	sub    $0x4,%esp
f0100ef4:	6a 10                	push   $0x10
f0100ef6:	6a 00                	push   $0x0
f0100ef8:	50                   	push   %eax
f0100ef9:	e8 00 3a 00 00       	call   f01048fe <strtol>
f0100efe:	83 c4 10             	add    $0x10,%esp
f0100f01:	89 45 f4             	mov    %eax,-0xc(%ebp)
	unsigned char *ptr = (unsigned char *)(address) ;
f0100f04:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100f07:	89 45 f0             	mov    %eax,-0x10(%ebp)
	
	*ptr = arguments[2][0];
f0100f0a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0100f0d:	83 c0 08             	add    $0x8,%eax
f0100f10:	8b 00                	mov    (%eax),%eax
f0100f12:	0f b6 00             	movzbl (%eax),%eax
f0100f15:	89 c2                	mov    %eax,%edx
f0100f17:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100f1a:	88 10                	mov    %dl,(%eax)
		
	return 0;
f0100f1c:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100f21:	c9                   	leave  
f0100f22:	c3                   	ret    

f0100f23 <command_meminfo>:

int command_meminfo(int number_of_arguments, char **arguments)
{
f0100f23:	55                   	push   %ebp
f0100f24:	89 e5                	mov    %esp,%ebp
f0100f26:	83 ec 08             	sub    $0x8,%esp
	cprintf("Free frames = %d\n", calculate_free_frames());
f0100f29:	e8 fe 18 00 00       	call   f010282c <calculate_free_frames>
f0100f2e:	83 ec 08             	sub    $0x8,%esp
f0100f31:	50                   	push   %eax
f0100f32:	68 7a 53 10 f0       	push   $0xf010537a
f0100f37:	e8 22 21 00 00       	call   f010305e <cprintf>
f0100f3c:	83 c4 10             	add    $0x10,%esp
		
	return 0;
f0100f3f:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100f44:	c9                   	leave  
f0100f45:	c3                   	ret    

f0100f46 <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f0100f46:	55                   	push   %ebp
f0100f47:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0100f49:	8b 45 08             	mov    0x8(%ebp),%eax
f0100f4c:	8b 15 bc 73 14 f0    	mov    0xf01473bc,%edx
f0100f52:	29 d0                	sub    %edx,%eax
f0100f54:	c1 f8 02             	sar    $0x2,%eax
f0100f57:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f0100f5d:	5d                   	pop    %ebp
f0100f5e:	c3                   	ret    

f0100f5f <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f0100f5f:	55                   	push   %ebp
f0100f60:	89 e5                	mov    %esp,%ebp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f0100f62:	ff 75 08             	pushl  0x8(%ebp)
f0100f65:	e8 dc ff ff ff       	call   f0100f46 <to_frame_number>
f0100f6a:	83 c4 04             	add    $0x4,%esp
f0100f6d:	c1 e0 0c             	shl    $0xc,%eax
}
f0100f70:	c9                   	leave  
f0100f71:	c3                   	ret    

f0100f72 <nvram_read>:
{
	sizeof(gdt) - 1, (unsigned long) gdt
};

int nvram_read(int r)
{	
f0100f72:	55                   	push   %ebp
f0100f73:	89 e5                	mov    %esp,%ebp
f0100f75:	53                   	push   %ebx
f0100f76:	83 ec 04             	sub    $0x4,%esp
	return mc146818_read(r) | (mc146818_read(r + 1) << 8);
f0100f79:	8b 45 08             	mov    0x8(%ebp),%eax
f0100f7c:	83 ec 0c             	sub    $0xc,%esp
f0100f7f:	50                   	push   %eax
f0100f80:	e8 20 20 00 00       	call   f0102fa5 <mc146818_read>
f0100f85:	83 c4 10             	add    $0x10,%esp
f0100f88:	89 c3                	mov    %eax,%ebx
f0100f8a:	8b 45 08             	mov    0x8(%ebp),%eax
f0100f8d:	83 c0 01             	add    $0x1,%eax
f0100f90:	83 ec 0c             	sub    $0xc,%esp
f0100f93:	50                   	push   %eax
f0100f94:	e8 0c 20 00 00       	call   f0102fa5 <mc146818_read>
f0100f99:	83 c4 10             	add    $0x10,%esp
f0100f9c:	c1 e0 08             	shl    $0x8,%eax
f0100f9f:	09 d8                	or     %ebx,%eax
}
f0100fa1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100fa4:	c9                   	leave  
f0100fa5:	c3                   	ret    

f0100fa6 <detect_memory>:
	
void detect_memory()
{
f0100fa6:	55                   	push   %ebp
f0100fa7:	89 e5                	mov    %esp,%ebp
f0100fa9:	83 ec 18             	sub    $0x18,%esp
	// CMOS tells us how many kilobytes there are
	size_of_base_mem = ROUNDDOWN(nvram_read(NVRAM_BASELO)*1024, PAGE_SIZE);
f0100fac:	83 ec 0c             	sub    $0xc,%esp
f0100faf:	6a 15                	push   $0x15
f0100fb1:	e8 bc ff ff ff       	call   f0100f72 <nvram_read>
f0100fb6:	83 c4 10             	add    $0x10,%esp
f0100fb9:	c1 e0 0a             	shl    $0xa,%eax
f0100fbc:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0100fbf:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100fc2:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100fc7:	a3 b4 73 14 f0       	mov    %eax,0xf01473b4
	size_of_extended_mem = ROUNDDOWN(nvram_read(NVRAM_EXTLO)*1024, PAGE_SIZE);
f0100fcc:	83 ec 0c             	sub    $0xc,%esp
f0100fcf:	6a 17                	push   $0x17
f0100fd1:	e8 9c ff ff ff       	call   f0100f72 <nvram_read>
f0100fd6:	83 c4 10             	add    $0x10,%esp
f0100fd9:	c1 e0 0a             	shl    $0xa,%eax
f0100fdc:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100fdf:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0100fe2:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0100fe7:	a3 ac 73 14 f0       	mov    %eax,0xf01473ac

	// Calculate the maxmium physical address based on whether
	// or not there is any extended memory.  See comment in ../inc/mmu.h.
	if (size_of_extended_mem)
f0100fec:	a1 ac 73 14 f0       	mov    0xf01473ac,%eax
f0100ff1:	85 c0                	test   %eax,%eax
f0100ff3:	74 11                	je     f0101006 <detect_memory+0x60>
		maxpa = PHYS_EXTENDED_MEM + size_of_extended_mem;
f0100ff5:	a1 ac 73 14 f0       	mov    0xf01473ac,%eax
f0100ffa:	05 00 00 10 00       	add    $0x100000,%eax
f0100fff:	a3 b0 73 14 f0       	mov    %eax,0xf01473b0
f0101004:	eb 0a                	jmp    f0101010 <detect_memory+0x6a>
	else
		maxpa = size_of_extended_mem;
f0101006:	a1 ac 73 14 f0       	mov    0xf01473ac,%eax
f010100b:	a3 b0 73 14 f0       	mov    %eax,0xf01473b0

	number_of_frames = maxpa / PAGE_SIZE;
f0101010:	a1 b0 73 14 f0       	mov    0xf01473b0,%eax
f0101015:	c1 e8 0c             	shr    $0xc,%eax
f0101018:	a3 a8 73 14 f0       	mov    %eax,0xf01473a8

	cprintf("Physical memory: %dK available, ", (int)(maxpa/1024));
f010101d:	a1 b0 73 14 f0       	mov    0xf01473b0,%eax
f0101022:	c1 e8 0a             	shr    $0xa,%eax
f0101025:	83 ec 08             	sub    $0x8,%esp
f0101028:	50                   	push   %eax
f0101029:	68 8c 53 10 f0       	push   $0xf010538c
f010102e:	e8 2b 20 00 00       	call   f010305e <cprintf>
f0101033:	83 c4 10             	add    $0x10,%esp
	cprintf("base = %dK, extended = %dK\n", (int)(size_of_base_mem/1024), (int)(size_of_extended_mem/1024));
f0101036:	a1 ac 73 14 f0       	mov    0xf01473ac,%eax
f010103b:	c1 e8 0a             	shr    $0xa,%eax
f010103e:	89 c2                	mov    %eax,%edx
f0101040:	a1 b4 73 14 f0       	mov    0xf01473b4,%eax
f0101045:	c1 e8 0a             	shr    $0xa,%eax
f0101048:	83 ec 04             	sub    $0x4,%esp
f010104b:	52                   	push   %edx
f010104c:	50                   	push   %eax
f010104d:	68 ad 53 10 f0       	push   $0xf01053ad
f0101052:	e8 07 20 00 00       	call   f010305e <cprintf>
f0101057:	83 c4 10             	add    $0x10,%esp
}
f010105a:	90                   	nop
f010105b:	c9                   	leave  
f010105c:	c3                   	ret    

f010105d <check_boot_pgdir>:
// but it is a pretty good check.
//
uint32 check_va2pa(uint32 *ptr_page_directory, uint32 va);

void check_boot_pgdir()
{
f010105d:	55                   	push   %ebp
f010105e:	89 e5                	mov    %esp,%ebp
f0101060:	83 ec 28             	sub    $0x28,%esp
	uint32 i, n;

	// check frames_info array
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
f0101063:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f010106a:	8b 15 a8 73 14 f0    	mov    0xf01473a8,%edx
f0101070:	89 d0                	mov    %edx,%eax
f0101072:	01 c0                	add    %eax,%eax
f0101074:	01 d0                	add    %edx,%eax
f0101076:	c1 e0 02             	shl    $0x2,%eax
f0101079:	89 c2                	mov    %eax,%edx
f010107b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010107e:	01 d0                	add    %edx,%eax
f0101080:	83 e8 01             	sub    $0x1,%eax
f0101083:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101086:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101089:	ba 00 00 00 00       	mov    $0x0,%edx
f010108e:	f7 75 f0             	divl   -0x10(%ebp)
f0101091:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101094:	29 d0                	sub    %edx,%eax
f0101096:	89 45 e8             	mov    %eax,-0x18(%ebp)
	for (i = 0; i < n; i += PAGE_SIZE)
f0101099:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f01010a0:	eb 71                	jmp    f0101113 <check_boot_pgdir+0xb6>
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);
f01010a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01010a5:	8d 90 00 00 00 ef    	lea    -0x11000000(%eax),%edx
f01010ab:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01010b0:	83 ec 08             	sub    $0x8,%esp
f01010b3:	52                   	push   %edx
f01010b4:	50                   	push   %eax
f01010b5:	e8 f7 01 00 00       	call   f01012b1 <check_va2pa>
f01010ba:	83 c4 10             	add    $0x10,%esp
f01010bd:	89 c1                	mov    %eax,%ecx
f01010bf:	a1 bc 73 14 f0       	mov    0xf01473bc,%eax
f01010c4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01010c7:	81 7d e4 ff ff ff ef 	cmpl   $0xefffffff,-0x1c(%ebp)
f01010ce:	77 14                	ja     f01010e4 <check_boot_pgdir+0x87>
f01010d0:	ff 75 e4             	pushl  -0x1c(%ebp)
f01010d3:	68 cc 53 10 f0       	push   $0xf01053cc
f01010d8:	6a 5e                	push   $0x5e
f01010da:	68 fd 53 10 f0       	push   $0xf01053fd
f01010df:	e8 4a f0 ff ff       	call   f010012e <_panic>
f01010e4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01010e7:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01010ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01010f0:	01 d0                	add    %edx,%eax
f01010f2:	39 c1                	cmp    %eax,%ecx
f01010f4:	74 16                	je     f010110c <check_boot_pgdir+0xaf>
f01010f6:	68 0c 54 10 f0       	push   $0xf010540c
f01010fb:	68 6e 54 10 f0       	push   $0xf010546e
f0101100:	6a 5e                	push   $0x5e
f0101102:	68 fd 53 10 f0       	push   $0xf01053fd
f0101107:	e8 22 f0 ff ff       	call   f010012e <_panic>
{
	uint32 i, n;

	// check frames_info array
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
	for (i = 0; i < n; i += PAGE_SIZE)
f010110c:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0101113:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101116:	3b 45 e8             	cmp    -0x18(%ebp),%eax
f0101119:	72 87                	jb     f01010a2 <check_boot_pgdir+0x45>
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);

	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
f010111b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0101122:	eb 3d                	jmp    f0101161 <check_boot_pgdir+0x104>
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);
f0101124:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101127:	8d 90 00 00 00 f0    	lea    -0x10000000(%eax),%edx
f010112d:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101132:	83 ec 08             	sub    $0x8,%esp
f0101135:	52                   	push   %edx
f0101136:	50                   	push   %eax
f0101137:	e8 75 01 00 00       	call   f01012b1 <check_va2pa>
f010113c:	83 c4 10             	add    $0x10,%esp
f010113f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f0101142:	74 16                	je     f010115a <check_boot_pgdir+0xfd>
f0101144:	68 84 54 10 f0       	push   $0xf0105484
f0101149:	68 6e 54 10 f0       	push   $0xf010546e
f010114e:	6a 62                	push   $0x62
f0101150:	68 fd 53 10 f0       	push   $0xf01053fd
f0101155:	e8 d4 ef ff ff       	call   f010012e <_panic>
	n = ROUNDUP(number_of_frames*sizeof(struct Frame_Info), PAGE_SIZE);
	for (i = 0; i < n; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, READ_ONLY_FRAMES_INFO + i) == K_PHYSICAL_ADDRESS(frames_info) + i);

	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
f010115a:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f0101161:	81 7d f4 00 00 00 10 	cmpl   $0x10000000,-0xc(%ebp)
f0101168:	75 ba                	jne    f0101124 <check_boot_pgdir+0xc7>
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);

	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
f010116a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0101171:	eb 70                	jmp    f01011e3 <check_boot_pgdir+0x186>
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);
f0101173:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101176:	8d 90 00 80 bf ef    	lea    -0x10408000(%eax),%edx
f010117c:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101181:	83 ec 08             	sub    $0x8,%esp
f0101184:	52                   	push   %edx
f0101185:	50                   	push   %eax
f0101186:	e8 26 01 00 00       	call   f01012b1 <check_va2pa>
f010118b:	83 c4 10             	add    $0x10,%esp
f010118e:	89 c1                	mov    %eax,%ecx
f0101190:	c7 45 e0 00 30 11 f0 	movl   $0xf0113000,-0x20(%ebp)
f0101197:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f010119e:	77 14                	ja     f01011b4 <check_boot_pgdir+0x157>
f01011a0:	ff 75 e0             	pushl  -0x20(%ebp)
f01011a3:	68 cc 53 10 f0       	push   $0xf01053cc
f01011a8:	6a 66                	push   $0x66
f01011aa:	68 fd 53 10 f0       	push   $0xf01053fd
f01011af:	e8 7a ef ff ff       	call   f010012e <_panic>
f01011b4:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01011b7:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f01011bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01011c0:	01 d0                	add    %edx,%eax
f01011c2:	39 c1                	cmp    %eax,%ecx
f01011c4:	74 16                	je     f01011dc <check_boot_pgdir+0x17f>
f01011c6:	68 bc 54 10 f0       	push   $0xf01054bc
f01011cb:	68 6e 54 10 f0       	push   $0xf010546e
f01011d0:	6a 66                	push   $0x66
f01011d2:	68 fd 53 10 f0       	push   $0xf01053fd
f01011d7:	e8 52 ef ff ff       	call   f010012e <_panic>
	// check phys mem
	for (i = 0; KERNEL_BASE + i != 0; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, KERNEL_BASE + i) == i);

	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
f01011dc:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f01011e3:	81 7d f4 ff 7f 00 00 	cmpl   $0x7fff,-0xc(%ebp)
f01011ea:	76 87                	jbe    f0101173 <check_boot_pgdir+0x116>
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);

	// check for zero/non-zero in PDEs
	for (i = 0; i < NPDENTRIES; i++) {
f01011ec:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f01011f3:	e9 99 00 00 00       	jmp    f0101291 <check_boot_pgdir+0x234>
		switch (i) {
f01011f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01011fb:	2d bb 03 00 00       	sub    $0x3bb,%eax
f0101200:	83 f8 04             	cmp    $0x4,%eax
f0101203:	77 29                	ja     f010122e <check_boot_pgdir+0x1d1>
		case PDX(VPT):
		case PDX(UVPT):
		case PDX(KERNEL_STACK_TOP-1):
		case PDX(UENVS):
		case PDX(READ_ONLY_FRAMES_INFO):			
			assert(ptr_page_directory[i]);
f0101205:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010120a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010120d:	c1 e2 02             	shl    $0x2,%edx
f0101210:	01 d0                	add    %edx,%eax
f0101212:	8b 00                	mov    (%eax),%eax
f0101214:	85 c0                	test   %eax,%eax
f0101216:	75 71                	jne    f0101289 <check_boot_pgdir+0x22c>
f0101218:	68 32 55 10 f0       	push   $0xf0105532
f010121d:	68 6e 54 10 f0       	push   $0xf010546e
f0101222:	6a 70                	push   $0x70
f0101224:	68 fd 53 10 f0       	push   $0xf01053fd
f0101229:	e8 00 ef ff ff       	call   f010012e <_panic>
			break;
		default:
			if (i >= PDX(KERNEL_BASE))
f010122e:	81 7d f4 bf 03 00 00 	cmpl   $0x3bf,-0xc(%ebp)
f0101235:	76 29                	jbe    f0101260 <check_boot_pgdir+0x203>
				assert(ptr_page_directory[i]);
f0101237:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010123c:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010123f:	c1 e2 02             	shl    $0x2,%edx
f0101242:	01 d0                	add    %edx,%eax
f0101244:	8b 00                	mov    (%eax),%eax
f0101246:	85 c0                	test   %eax,%eax
f0101248:	75 42                	jne    f010128c <check_boot_pgdir+0x22f>
f010124a:	68 32 55 10 f0       	push   $0xf0105532
f010124f:	68 6e 54 10 f0       	push   $0xf010546e
f0101254:	6a 74                	push   $0x74
f0101256:	68 fd 53 10 f0       	push   $0xf01053fd
f010125b:	e8 ce ee ff ff       	call   f010012e <_panic>
			else				
				assert(ptr_page_directory[i] == 0);
f0101260:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101265:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0101268:	c1 e2 02             	shl    $0x2,%edx
f010126b:	01 d0                	add    %edx,%eax
f010126d:	8b 00                	mov    (%eax),%eax
f010126f:	85 c0                	test   %eax,%eax
f0101271:	74 19                	je     f010128c <check_boot_pgdir+0x22f>
f0101273:	68 48 55 10 f0       	push   $0xf0105548
f0101278:	68 6e 54 10 f0       	push   $0xf010546e
f010127d:	6a 76                	push   $0x76
f010127f:	68 fd 53 10 f0       	push   $0xf01053fd
f0101284:	e8 a5 ee ff ff       	call   f010012e <_panic>
		case PDX(UVPT):
		case PDX(KERNEL_STACK_TOP-1):
		case PDX(UENVS):
		case PDX(READ_ONLY_FRAMES_INFO):			
			assert(ptr_page_directory[i]);
			break;
f0101289:	90                   	nop
f010128a:	eb 01                	jmp    f010128d <check_boot_pgdir+0x230>
		default:
			if (i >= PDX(KERNEL_BASE))
				assert(ptr_page_directory[i]);
			else				
				assert(ptr_page_directory[i] == 0);
			break;
f010128c:	90                   	nop
	// check kernel stack
	for (i = 0; i < KERNEL_STACK_SIZE; i += PAGE_SIZE)
		assert(check_va2pa(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE + i) == K_PHYSICAL_ADDRESS(ptr_stack_bottom) + i);

	// check for zero/non-zero in PDEs
	for (i = 0; i < NPDENTRIES; i++) {
f010128d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0101291:	81 7d f4 ff 03 00 00 	cmpl   $0x3ff,-0xc(%ebp)
f0101298:	0f 86 5a ff ff ff    	jbe    f01011f8 <check_boot_pgdir+0x19b>
			else				
				assert(ptr_page_directory[i] == 0);
			break;
		}
	}
	cprintf("check_boot_pgdir() succeeded!\n");
f010129e:	83 ec 0c             	sub    $0xc,%esp
f01012a1:	68 64 55 10 f0       	push   $0xf0105564
f01012a6:	e8 b3 1d 00 00       	call   f010305e <cprintf>
f01012ab:	83 c4 10             	add    $0x10,%esp
}
f01012ae:	90                   	nop
f01012af:	c9                   	leave  
f01012b0:	c3                   	ret    

f01012b1 <check_va2pa>:
// defined by the page directory 'ptr_page_directory'.  The hardware normally performs
// this functionality for us!  We define our own version to help check
// the check_boot_pgdir() function; it shouldn't be used elsewhere.

uint32 check_va2pa(uint32 *ptr_page_directory, uint32 va)
{
f01012b1:	55                   	push   %ebp
f01012b2:	89 e5                	mov    %esp,%ebp
f01012b4:	83 ec 18             	sub    $0x18,%esp
	uint32 *p;

	ptr_page_directory = &ptr_page_directory[PDX(va)];
f01012b7:	8b 45 0c             	mov    0xc(%ebp),%eax
f01012ba:	c1 e8 16             	shr    $0x16,%eax
f01012bd:	c1 e0 02             	shl    $0x2,%eax
f01012c0:	01 45 08             	add    %eax,0x8(%ebp)
	if (!(*ptr_page_directory & PERM_PRESENT))
f01012c3:	8b 45 08             	mov    0x8(%ebp),%eax
f01012c6:	8b 00                	mov    (%eax),%eax
f01012c8:	83 e0 01             	and    $0x1,%eax
f01012cb:	85 c0                	test   %eax,%eax
f01012cd:	75 0a                	jne    f01012d9 <check_va2pa+0x28>
		return ~0;
f01012cf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01012d4:	e9 87 00 00 00       	jmp    f0101360 <check_va2pa+0xaf>
	p = (uint32*) K_VIRTUAL_ADDRESS(EXTRACT_ADDRESS(*ptr_page_directory));
f01012d9:	8b 45 08             	mov    0x8(%ebp),%eax
f01012dc:	8b 00                	mov    (%eax),%eax
f01012de:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f01012e3:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01012e6:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01012e9:	c1 e8 0c             	shr    $0xc,%eax
f01012ec:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01012ef:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f01012f4:	39 45 f0             	cmp    %eax,-0x10(%ebp)
f01012f7:	72 17                	jb     f0101310 <check_va2pa+0x5f>
f01012f9:	ff 75 f4             	pushl  -0xc(%ebp)
f01012fc:	68 84 55 10 f0       	push   $0xf0105584
f0101301:	68 89 00 00 00       	push   $0x89
f0101306:	68 fd 53 10 f0       	push   $0xf01053fd
f010130b:	e8 1e ee ff ff       	call   f010012e <_panic>
f0101310:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101313:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0101318:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if (!(p[PTX(va)] & PERM_PRESENT))
f010131b:	8b 45 0c             	mov    0xc(%ebp),%eax
f010131e:	c1 e8 0c             	shr    $0xc,%eax
f0101321:	25 ff 03 00 00       	and    $0x3ff,%eax
f0101326:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f010132d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101330:	01 d0                	add    %edx,%eax
f0101332:	8b 00                	mov    (%eax),%eax
f0101334:	83 e0 01             	and    $0x1,%eax
f0101337:	85 c0                	test   %eax,%eax
f0101339:	75 07                	jne    f0101342 <check_va2pa+0x91>
		return ~0;
f010133b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0101340:	eb 1e                	jmp    f0101360 <check_va2pa+0xaf>
	return EXTRACT_ADDRESS(p[PTX(va)]);
f0101342:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101345:	c1 e8 0c             	shr    $0xc,%eax
f0101348:	25 ff 03 00 00       	and    $0x3ff,%eax
f010134d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0101354:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101357:	01 d0                	add    %edx,%eax
f0101359:	8b 00                	mov    (%eax),%eax
f010135b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
}
f0101360:	c9                   	leave  
f0101361:	c3                   	ret    

f0101362 <tlb_invalidate>:
		
void tlb_invalidate(uint32 *ptr_page_directory, void *virtual_address)
{
f0101362:	55                   	push   %ebp
f0101363:	89 e5                	mov    %esp,%ebp
f0101365:	83 ec 10             	sub    $0x10,%esp
f0101368:	8b 45 0c             	mov    0xc(%ebp),%eax
f010136b:	89 45 fc             	mov    %eax,-0x4(%ebp)
}

static __inline void 
invlpg(void *addr)
{ 
	__asm __volatile("invlpg (%0)" : : "r" (addr) : "memory");
f010136e:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101371:	0f 01 38             	invlpg (%eax)
	// Flush the entry only if we're modifying the current address space.
	// For now, there is only one address space, so always invalidate.
	invlpg(virtual_address);
}
f0101374:	90                   	nop
f0101375:	c9                   	leave  
f0101376:	c3                   	ret    

f0101377 <page_check>:

void page_check()
{
f0101377:	55                   	push   %ebp
f0101378:	89 e5                	mov    %esp,%ebp
f010137a:	53                   	push   %ebx
f010137b:	83 ec 24             	sub    $0x24,%esp
	struct Frame_Info *pp, *pp0, *pp1, *pp2;
	struct Linked_List fl;

	// should be able to allocate three frames_info
	pp0 = pp1 = pp2 = 0;
f010137e:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
f0101385:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101388:	89 45 ec             	mov    %eax,-0x14(%ebp)
f010138b:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010138e:	89 45 f0             	mov    %eax,-0x10(%ebp)
	assert(allocate_frame(&pp0) == 0);
f0101391:	83 ec 0c             	sub    $0xc,%esp
f0101394:	8d 45 f0             	lea    -0x10(%ebp),%eax
f0101397:	50                   	push   %eax
f0101398:	e8 ce 10 00 00       	call   f010246b <allocate_frame>
f010139d:	83 c4 10             	add    $0x10,%esp
f01013a0:	85 c0                	test   %eax,%eax
f01013a2:	74 19                	je     f01013bd <page_check+0x46>
f01013a4:	68 b3 55 10 f0       	push   $0xf01055b3
f01013a9:	68 6e 54 10 f0       	push   $0xf010546e
f01013ae:	68 9d 00 00 00       	push   $0x9d
f01013b3:	68 fd 53 10 f0       	push   $0xf01053fd
f01013b8:	e8 71 ed ff ff       	call   f010012e <_panic>
	assert(allocate_frame(&pp1) == 0);
f01013bd:	83 ec 0c             	sub    $0xc,%esp
f01013c0:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01013c3:	50                   	push   %eax
f01013c4:	e8 a2 10 00 00       	call   f010246b <allocate_frame>
f01013c9:	83 c4 10             	add    $0x10,%esp
f01013cc:	85 c0                	test   %eax,%eax
f01013ce:	74 19                	je     f01013e9 <page_check+0x72>
f01013d0:	68 cd 55 10 f0       	push   $0xf01055cd
f01013d5:	68 6e 54 10 f0       	push   $0xf010546e
f01013da:	68 9e 00 00 00       	push   $0x9e
f01013df:	68 fd 53 10 f0       	push   $0xf01053fd
f01013e4:	e8 45 ed ff ff       	call   f010012e <_panic>
	assert(allocate_frame(&pp2) == 0);
f01013e9:	83 ec 0c             	sub    $0xc,%esp
f01013ec:	8d 45 e8             	lea    -0x18(%ebp),%eax
f01013ef:	50                   	push   %eax
f01013f0:	e8 76 10 00 00       	call   f010246b <allocate_frame>
f01013f5:	83 c4 10             	add    $0x10,%esp
f01013f8:	85 c0                	test   %eax,%eax
f01013fa:	74 19                	je     f0101415 <page_check+0x9e>
f01013fc:	68 e7 55 10 f0       	push   $0xf01055e7
f0101401:	68 6e 54 10 f0       	push   $0xf010546e
f0101406:	68 9f 00 00 00       	push   $0x9f
f010140b:	68 fd 53 10 f0       	push   $0xf01053fd
f0101410:	e8 19 ed ff ff       	call   f010012e <_panic>

	assert(pp0);
f0101415:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101418:	85 c0                	test   %eax,%eax
f010141a:	75 19                	jne    f0101435 <page_check+0xbe>
f010141c:	68 01 56 10 f0       	push   $0xf0105601
f0101421:	68 6e 54 10 f0       	push   $0xf010546e
f0101426:	68 a1 00 00 00       	push   $0xa1
f010142b:	68 fd 53 10 f0       	push   $0xf01053fd
f0101430:	e8 f9 ec ff ff       	call   f010012e <_panic>
	assert(pp1 && pp1 != pp0);
f0101435:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101438:	85 c0                	test   %eax,%eax
f010143a:	74 0a                	je     f0101446 <page_check+0xcf>
f010143c:	8b 55 ec             	mov    -0x14(%ebp),%edx
f010143f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101442:	39 c2                	cmp    %eax,%edx
f0101444:	75 19                	jne    f010145f <page_check+0xe8>
f0101446:	68 05 56 10 f0       	push   $0xf0105605
f010144b:	68 6e 54 10 f0       	push   $0xf010546e
f0101450:	68 a2 00 00 00       	push   $0xa2
f0101455:	68 fd 53 10 f0       	push   $0xf01053fd
f010145a:	e8 cf ec ff ff       	call   f010012e <_panic>
	assert(pp2 && pp2 != pp1 && pp2 != pp0);
f010145f:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101462:	85 c0                	test   %eax,%eax
f0101464:	74 14                	je     f010147a <page_check+0x103>
f0101466:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0101469:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010146c:	39 c2                	cmp    %eax,%edx
f010146e:	74 0a                	je     f010147a <page_check+0x103>
f0101470:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0101473:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101476:	39 c2                	cmp    %eax,%edx
f0101478:	75 19                	jne    f0101493 <page_check+0x11c>
f010147a:	68 18 56 10 f0       	push   $0xf0105618
f010147f:	68 6e 54 10 f0       	push   $0xf010546e
f0101484:	68 a3 00 00 00       	push   $0xa3
f0101489:	68 fd 53 10 f0       	push   $0xf01053fd
f010148e:	e8 9b ec ff ff       	call   f010012e <_panic>

	// temporarily steal the rest of the free frames_info
	fl = free_frame_list;
f0101493:	a1 b8 73 14 f0       	mov    0xf01473b8,%eax
f0101498:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	LIST_INIT(&free_frame_list);
f010149b:	c7 05 b8 73 14 f0 00 	movl   $0x0,0xf01473b8
f01014a2:	00 00 00 

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f01014a5:	83 ec 0c             	sub    $0xc,%esp
f01014a8:	8d 45 f4             	lea    -0xc(%ebp),%eax
f01014ab:	50                   	push   %eax
f01014ac:	e8 ba 0f 00 00       	call   f010246b <allocate_frame>
f01014b1:	83 c4 10             	add    $0x10,%esp
f01014b4:	83 f8 fc             	cmp    $0xfffffffc,%eax
f01014b7:	74 19                	je     f01014d2 <page_check+0x15b>
f01014b9:	68 38 56 10 f0       	push   $0xf0105638
f01014be:	68 6e 54 10 f0       	push   $0xf010546e
f01014c3:	68 aa 00 00 00       	push   $0xaa
f01014c8:	68 fd 53 10 f0       	push   $0xf01053fd
f01014cd:	e8 5c ec ff ff       	call   f010012e <_panic>

	// there is no free memory, so we can't allocate a page table 
	assert(map_frame(ptr_page_directory, pp1, 0x0, 0) < 0);
f01014d2:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01014d5:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01014da:	6a 00                	push   $0x0
f01014dc:	6a 00                	push   $0x0
f01014de:	52                   	push   %edx
f01014df:	50                   	push   %eax
f01014e0:	e8 97 11 00 00       	call   f010267c <map_frame>
f01014e5:	83 c4 10             	add    $0x10,%esp
f01014e8:	85 c0                	test   %eax,%eax
f01014ea:	78 19                	js     f0101505 <page_check+0x18e>
f01014ec:	68 58 56 10 f0       	push   $0xf0105658
f01014f1:	68 6e 54 10 f0       	push   $0xf010546e
f01014f6:	68 ad 00 00 00       	push   $0xad
f01014fb:	68 fd 53 10 f0       	push   $0xf01053fd
f0101500:	e8 29 ec ff ff       	call   f010012e <_panic>

	// free pp0 and try again: pp0 should be used for page table
	free_frame(pp0);
f0101505:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101508:	83 ec 0c             	sub    $0xc,%esp
f010150b:	50                   	push   %eax
f010150c:	e8 c1 0f 00 00       	call   f01024d2 <free_frame>
f0101511:	83 c4 10             	add    $0x10,%esp
	assert(map_frame(ptr_page_directory, pp1, 0x0, 0) == 0);
f0101514:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0101517:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010151c:	6a 00                	push   $0x0
f010151e:	6a 00                	push   $0x0
f0101520:	52                   	push   %edx
f0101521:	50                   	push   %eax
f0101522:	e8 55 11 00 00       	call   f010267c <map_frame>
f0101527:	83 c4 10             	add    $0x10,%esp
f010152a:	85 c0                	test   %eax,%eax
f010152c:	74 19                	je     f0101547 <page_check+0x1d0>
f010152e:	68 88 56 10 f0       	push   $0xf0105688
f0101533:	68 6e 54 10 f0       	push   $0xf010546e
f0101538:	68 b1 00 00 00       	push   $0xb1
f010153d:	68 fd 53 10 f0       	push   $0xf01053fd
f0101542:	e8 e7 eb ff ff       	call   f010012e <_panic>
	assert(EXTRACT_ADDRESS(ptr_page_directory[0]) == to_physical_address(pp0));
f0101547:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010154c:	8b 00                	mov    (%eax),%eax
f010154e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0101553:	89 c3                	mov    %eax,%ebx
f0101555:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101558:	83 ec 0c             	sub    $0xc,%esp
f010155b:	50                   	push   %eax
f010155c:	e8 fe f9 ff ff       	call   f0100f5f <to_physical_address>
f0101561:	83 c4 10             	add    $0x10,%esp
f0101564:	39 c3                	cmp    %eax,%ebx
f0101566:	74 19                	je     f0101581 <page_check+0x20a>
f0101568:	68 b8 56 10 f0       	push   $0xf01056b8
f010156d:	68 6e 54 10 f0       	push   $0xf010546e
f0101572:	68 b2 00 00 00       	push   $0xb2
f0101577:	68 fd 53 10 f0       	push   $0xf01053fd
f010157c:	e8 ad eb ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, 0x0) == to_physical_address(pp1));
f0101581:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101586:	83 ec 08             	sub    $0x8,%esp
f0101589:	6a 00                	push   $0x0
f010158b:	50                   	push   %eax
f010158c:	e8 20 fd ff ff       	call   f01012b1 <check_va2pa>
f0101591:	83 c4 10             	add    $0x10,%esp
f0101594:	89 c3                	mov    %eax,%ebx
f0101596:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101599:	83 ec 0c             	sub    $0xc,%esp
f010159c:	50                   	push   %eax
f010159d:	e8 bd f9 ff ff       	call   f0100f5f <to_physical_address>
f01015a2:	83 c4 10             	add    $0x10,%esp
f01015a5:	39 c3                	cmp    %eax,%ebx
f01015a7:	74 19                	je     f01015c2 <page_check+0x24b>
f01015a9:	68 fc 56 10 f0       	push   $0xf01056fc
f01015ae:	68 6e 54 10 f0       	push   $0xf010546e
f01015b3:	68 b3 00 00 00       	push   $0xb3
f01015b8:	68 fd 53 10 f0       	push   $0xf01053fd
f01015bd:	e8 6c eb ff ff       	call   f010012e <_panic>
	assert(pp1->references == 1);
f01015c2:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01015c5:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01015c9:	66 83 f8 01          	cmp    $0x1,%ax
f01015cd:	74 19                	je     f01015e8 <page_check+0x271>
f01015cf:	68 3d 57 10 f0       	push   $0xf010573d
f01015d4:	68 6e 54 10 f0       	push   $0xf010546e
f01015d9:	68 b4 00 00 00       	push   $0xb4
f01015de:	68 fd 53 10 f0       	push   $0xf01053fd
f01015e3:	e8 46 eb ff ff       	call   f010012e <_panic>
	assert(pp0->references == 1);
f01015e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01015eb:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01015ef:	66 83 f8 01          	cmp    $0x1,%ax
f01015f3:	74 19                	je     f010160e <page_check+0x297>
f01015f5:	68 52 57 10 f0       	push   $0xf0105752
f01015fa:	68 6e 54 10 f0       	push   $0xf010546e
f01015ff:	68 b5 00 00 00       	push   $0xb5
f0101604:	68 fd 53 10 f0       	push   $0xf01053fd
f0101609:	e8 20 eb ff ff       	call   f010012e <_panic>

	// should be able to map pp2 at PAGE_SIZE because pp0 is already allocated for page table
	assert(map_frame(ptr_page_directory, pp2, (void*) PAGE_SIZE, 0) == 0);
f010160e:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0101611:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101616:	6a 00                	push   $0x0
f0101618:	68 00 10 00 00       	push   $0x1000
f010161d:	52                   	push   %edx
f010161e:	50                   	push   %eax
f010161f:	e8 58 10 00 00       	call   f010267c <map_frame>
f0101624:	83 c4 10             	add    $0x10,%esp
f0101627:	85 c0                	test   %eax,%eax
f0101629:	74 19                	je     f0101644 <page_check+0x2cd>
f010162b:	68 68 57 10 f0       	push   $0xf0105768
f0101630:	68 6e 54 10 f0       	push   $0xf010546e
f0101635:	68 b8 00 00 00       	push   $0xb8
f010163a:	68 fd 53 10 f0       	push   $0xf01053fd
f010163f:	e8 ea ea ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp2));
f0101644:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101649:	83 ec 08             	sub    $0x8,%esp
f010164c:	68 00 10 00 00       	push   $0x1000
f0101651:	50                   	push   %eax
f0101652:	e8 5a fc ff ff       	call   f01012b1 <check_va2pa>
f0101657:	83 c4 10             	add    $0x10,%esp
f010165a:	89 c3                	mov    %eax,%ebx
f010165c:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010165f:	83 ec 0c             	sub    $0xc,%esp
f0101662:	50                   	push   %eax
f0101663:	e8 f7 f8 ff ff       	call   f0100f5f <to_physical_address>
f0101668:	83 c4 10             	add    $0x10,%esp
f010166b:	39 c3                	cmp    %eax,%ebx
f010166d:	74 19                	je     f0101688 <page_check+0x311>
f010166f:	68 a8 57 10 f0       	push   $0xf01057a8
f0101674:	68 6e 54 10 f0       	push   $0xf010546e
f0101679:	68 b9 00 00 00       	push   $0xb9
f010167e:	68 fd 53 10 f0       	push   $0xf01053fd
f0101683:	e8 a6 ea ff ff       	call   f010012e <_panic>
	assert(pp2->references == 1);
f0101688:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010168b:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f010168f:	66 83 f8 01          	cmp    $0x1,%ax
f0101693:	74 19                	je     f01016ae <page_check+0x337>
f0101695:	68 ef 57 10 f0       	push   $0xf01057ef
f010169a:	68 6e 54 10 f0       	push   $0xf010546e
f010169f:	68 ba 00 00 00       	push   $0xba
f01016a4:	68 fd 53 10 f0       	push   $0xf01053fd
f01016a9:	e8 80 ea ff ff       	call   f010012e <_panic>

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f01016ae:	83 ec 0c             	sub    $0xc,%esp
f01016b1:	8d 45 f4             	lea    -0xc(%ebp),%eax
f01016b4:	50                   	push   %eax
f01016b5:	e8 b1 0d 00 00       	call   f010246b <allocate_frame>
f01016ba:	83 c4 10             	add    $0x10,%esp
f01016bd:	83 f8 fc             	cmp    $0xfffffffc,%eax
f01016c0:	74 19                	je     f01016db <page_check+0x364>
f01016c2:	68 38 56 10 f0       	push   $0xf0105638
f01016c7:	68 6e 54 10 f0       	push   $0xf010546e
f01016cc:	68 bd 00 00 00       	push   $0xbd
f01016d1:	68 fd 53 10 f0       	push   $0xf01053fd
f01016d6:	e8 53 ea ff ff       	call   f010012e <_panic>

	// should be able to map pp2 at PAGE_SIZE because it's already there
	assert(map_frame(ptr_page_directory, pp2, (void*) PAGE_SIZE, 0) == 0);
f01016db:	8b 55 e8             	mov    -0x18(%ebp),%edx
f01016de:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01016e3:	6a 00                	push   $0x0
f01016e5:	68 00 10 00 00       	push   $0x1000
f01016ea:	52                   	push   %edx
f01016eb:	50                   	push   %eax
f01016ec:	e8 8b 0f 00 00       	call   f010267c <map_frame>
f01016f1:	83 c4 10             	add    $0x10,%esp
f01016f4:	85 c0                	test   %eax,%eax
f01016f6:	74 19                	je     f0101711 <page_check+0x39a>
f01016f8:	68 68 57 10 f0       	push   $0xf0105768
f01016fd:	68 6e 54 10 f0       	push   $0xf010546e
f0101702:	68 c0 00 00 00       	push   $0xc0
f0101707:	68 fd 53 10 f0       	push   $0xf01053fd
f010170c:	e8 1d ea ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp2));
f0101711:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101716:	83 ec 08             	sub    $0x8,%esp
f0101719:	68 00 10 00 00       	push   $0x1000
f010171e:	50                   	push   %eax
f010171f:	e8 8d fb ff ff       	call   f01012b1 <check_va2pa>
f0101724:	83 c4 10             	add    $0x10,%esp
f0101727:	89 c3                	mov    %eax,%ebx
f0101729:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010172c:	83 ec 0c             	sub    $0xc,%esp
f010172f:	50                   	push   %eax
f0101730:	e8 2a f8 ff ff       	call   f0100f5f <to_physical_address>
f0101735:	83 c4 10             	add    $0x10,%esp
f0101738:	39 c3                	cmp    %eax,%ebx
f010173a:	74 19                	je     f0101755 <page_check+0x3de>
f010173c:	68 a8 57 10 f0       	push   $0xf01057a8
f0101741:	68 6e 54 10 f0       	push   $0xf010546e
f0101746:	68 c1 00 00 00       	push   $0xc1
f010174b:	68 fd 53 10 f0       	push   $0xf01053fd
f0101750:	e8 d9 e9 ff ff       	call   f010012e <_panic>
	assert(pp2->references == 1);
f0101755:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101758:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f010175c:	66 83 f8 01          	cmp    $0x1,%ax
f0101760:	74 19                	je     f010177b <page_check+0x404>
f0101762:	68 ef 57 10 f0       	push   $0xf01057ef
f0101767:	68 6e 54 10 f0       	push   $0xf010546e
f010176c:	68 c2 00 00 00       	push   $0xc2
f0101771:	68 fd 53 10 f0       	push   $0xf01053fd
f0101776:	e8 b3 e9 ff ff       	call   f010012e <_panic>

	// pp2 should NOT be on the free list
	// could happen in ref counts are handled sloppily in map_frame
	assert(allocate_frame(&pp) == E_NO_MEM);
f010177b:	83 ec 0c             	sub    $0xc,%esp
f010177e:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0101781:	50                   	push   %eax
f0101782:	e8 e4 0c 00 00       	call   f010246b <allocate_frame>
f0101787:	83 c4 10             	add    $0x10,%esp
f010178a:	83 f8 fc             	cmp    $0xfffffffc,%eax
f010178d:	74 19                	je     f01017a8 <page_check+0x431>
f010178f:	68 38 56 10 f0       	push   $0xf0105638
f0101794:	68 6e 54 10 f0       	push   $0xf010546e
f0101799:	68 c6 00 00 00       	push   $0xc6
f010179e:	68 fd 53 10 f0       	push   $0xf01053fd
f01017a3:	e8 86 e9 ff ff       	call   f010012e <_panic>

	// should not be able to map at PTSIZE because need free frame for page table
	assert(map_frame(ptr_page_directory, pp0, (void*) PTSIZE, 0) < 0);
f01017a8:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01017ab:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01017b0:	6a 00                	push   $0x0
f01017b2:	68 00 00 40 00       	push   $0x400000
f01017b7:	52                   	push   %edx
f01017b8:	50                   	push   %eax
f01017b9:	e8 be 0e 00 00       	call   f010267c <map_frame>
f01017be:	83 c4 10             	add    $0x10,%esp
f01017c1:	85 c0                	test   %eax,%eax
f01017c3:	78 19                	js     f01017de <page_check+0x467>
f01017c5:	68 04 58 10 f0       	push   $0xf0105804
f01017ca:	68 6e 54 10 f0       	push   $0xf010546e
f01017cf:	68 c9 00 00 00       	push   $0xc9
f01017d4:	68 fd 53 10 f0       	push   $0xf01053fd
f01017d9:	e8 50 e9 ff ff       	call   f010012e <_panic>

	// insert pp1 at PAGE_SIZE (replacing pp2)
	assert(map_frame(ptr_page_directory, pp1, (void*) PAGE_SIZE, 0) == 0);
f01017de:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01017e1:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01017e6:	6a 00                	push   $0x0
f01017e8:	68 00 10 00 00       	push   $0x1000
f01017ed:	52                   	push   %edx
f01017ee:	50                   	push   %eax
f01017ef:	e8 88 0e 00 00       	call   f010267c <map_frame>
f01017f4:	83 c4 10             	add    $0x10,%esp
f01017f7:	85 c0                	test   %eax,%eax
f01017f9:	74 19                	je     f0101814 <page_check+0x49d>
f01017fb:	68 40 58 10 f0       	push   $0xf0105840
f0101800:	68 6e 54 10 f0       	push   $0xf010546e
f0101805:	68 cc 00 00 00       	push   $0xcc
f010180a:	68 fd 53 10 f0       	push   $0xf01053fd
f010180f:	e8 1a e9 ff ff       	call   f010012e <_panic>

	// should have pp1 at both 0 and PAGE_SIZE, pp2 nowhere, ...
	assert(check_va2pa(ptr_page_directory, 0) == to_physical_address(pp1));
f0101814:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101819:	83 ec 08             	sub    $0x8,%esp
f010181c:	6a 00                	push   $0x0
f010181e:	50                   	push   %eax
f010181f:	e8 8d fa ff ff       	call   f01012b1 <check_va2pa>
f0101824:	83 c4 10             	add    $0x10,%esp
f0101827:	89 c3                	mov    %eax,%ebx
f0101829:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010182c:	83 ec 0c             	sub    $0xc,%esp
f010182f:	50                   	push   %eax
f0101830:	e8 2a f7 ff ff       	call   f0100f5f <to_physical_address>
f0101835:	83 c4 10             	add    $0x10,%esp
f0101838:	39 c3                	cmp    %eax,%ebx
f010183a:	74 19                	je     f0101855 <page_check+0x4de>
f010183c:	68 80 58 10 f0       	push   $0xf0105880
f0101841:	68 6e 54 10 f0       	push   $0xf010546e
f0101846:	68 cf 00 00 00       	push   $0xcf
f010184b:	68 fd 53 10 f0       	push   $0xf01053fd
f0101850:	e8 d9 e8 ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp1));
f0101855:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010185a:	83 ec 08             	sub    $0x8,%esp
f010185d:	68 00 10 00 00       	push   $0x1000
f0101862:	50                   	push   %eax
f0101863:	e8 49 fa ff ff       	call   f01012b1 <check_va2pa>
f0101868:	83 c4 10             	add    $0x10,%esp
f010186b:	89 c3                	mov    %eax,%ebx
f010186d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101870:	83 ec 0c             	sub    $0xc,%esp
f0101873:	50                   	push   %eax
f0101874:	e8 e6 f6 ff ff       	call   f0100f5f <to_physical_address>
f0101879:	83 c4 10             	add    $0x10,%esp
f010187c:	39 c3                	cmp    %eax,%ebx
f010187e:	74 19                	je     f0101899 <page_check+0x522>
f0101880:	68 c0 58 10 f0       	push   $0xf01058c0
f0101885:	68 6e 54 10 f0       	push   $0xf010546e
f010188a:	68 d0 00 00 00       	push   $0xd0
f010188f:	68 fd 53 10 f0       	push   $0xf01053fd
f0101894:	e8 95 e8 ff ff       	call   f010012e <_panic>
	// ... and ref counts should reflect this
	assert(pp1->references == 2);
f0101899:	8b 45 ec             	mov    -0x14(%ebp),%eax
f010189c:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01018a0:	66 83 f8 02          	cmp    $0x2,%ax
f01018a4:	74 19                	je     f01018bf <page_check+0x548>
f01018a6:	68 07 59 10 f0       	push   $0xf0105907
f01018ab:	68 6e 54 10 f0       	push   $0xf010546e
f01018b0:	68 d2 00 00 00       	push   $0xd2
f01018b5:	68 fd 53 10 f0       	push   $0xf01053fd
f01018ba:	e8 6f e8 ff ff       	call   f010012e <_panic>
	assert(pp2->references == 0);
f01018bf:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01018c2:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01018c6:	66 85 c0             	test   %ax,%ax
f01018c9:	74 19                	je     f01018e4 <page_check+0x56d>
f01018cb:	68 1c 59 10 f0       	push   $0xf010591c
f01018d0:	68 6e 54 10 f0       	push   $0xf010546e
f01018d5:	68 d3 00 00 00       	push   $0xd3
f01018da:	68 fd 53 10 f0       	push   $0xf01053fd
f01018df:	e8 4a e8 ff ff       	call   f010012e <_panic>

	// pp2 should be returned by allocate_frame
	assert(allocate_frame(&pp) == 0 && pp == pp2);
f01018e4:	83 ec 0c             	sub    $0xc,%esp
f01018e7:	8d 45 f4             	lea    -0xc(%ebp),%eax
f01018ea:	50                   	push   %eax
f01018eb:	e8 7b 0b 00 00       	call   f010246b <allocate_frame>
f01018f0:	83 c4 10             	add    $0x10,%esp
f01018f3:	85 c0                	test   %eax,%eax
f01018f5:	75 0a                	jne    f0101901 <page_check+0x58a>
f01018f7:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01018fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01018fd:	39 c2                	cmp    %eax,%edx
f01018ff:	74 19                	je     f010191a <page_check+0x5a3>
f0101901:	68 34 59 10 f0       	push   $0xf0105934
f0101906:	68 6e 54 10 f0       	push   $0xf010546e
f010190b:	68 d6 00 00 00       	push   $0xd6
f0101910:	68 fd 53 10 f0       	push   $0xf01053fd
f0101915:	e8 14 e8 ff ff       	call   f010012e <_panic>

	// unmapping pp1 at 0 should keep pp1 at PAGE_SIZE
	unmap_frame(ptr_page_directory, 0x0);
f010191a:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f010191f:	83 ec 08             	sub    $0x8,%esp
f0101922:	6a 00                	push   $0x0
f0101924:	50                   	push   %eax
f0101925:	e8 68 0e 00 00       	call   f0102792 <unmap_frame>
f010192a:	83 c4 10             	add    $0x10,%esp
	assert(check_va2pa(ptr_page_directory, 0x0) == ~0);
f010192d:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101932:	83 ec 08             	sub    $0x8,%esp
f0101935:	6a 00                	push   $0x0
f0101937:	50                   	push   %eax
f0101938:	e8 74 f9 ff ff       	call   f01012b1 <check_va2pa>
f010193d:	83 c4 10             	add    $0x10,%esp
f0101940:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101943:	74 19                	je     f010195e <page_check+0x5e7>
f0101945:	68 5c 59 10 f0       	push   $0xf010595c
f010194a:	68 6e 54 10 f0       	push   $0xf010546e
f010194f:	68 da 00 00 00       	push   $0xda
f0101954:	68 fd 53 10 f0       	push   $0xf01053fd
f0101959:	e8 d0 e7 ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == to_physical_address(pp1));
f010195e:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101963:	83 ec 08             	sub    $0x8,%esp
f0101966:	68 00 10 00 00       	push   $0x1000
f010196b:	50                   	push   %eax
f010196c:	e8 40 f9 ff ff       	call   f01012b1 <check_va2pa>
f0101971:	83 c4 10             	add    $0x10,%esp
f0101974:	89 c3                	mov    %eax,%ebx
f0101976:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101979:	83 ec 0c             	sub    $0xc,%esp
f010197c:	50                   	push   %eax
f010197d:	e8 dd f5 ff ff       	call   f0100f5f <to_physical_address>
f0101982:	83 c4 10             	add    $0x10,%esp
f0101985:	39 c3                	cmp    %eax,%ebx
f0101987:	74 19                	je     f01019a2 <page_check+0x62b>
f0101989:	68 c0 58 10 f0       	push   $0xf01058c0
f010198e:	68 6e 54 10 f0       	push   $0xf010546e
f0101993:	68 db 00 00 00       	push   $0xdb
f0101998:	68 fd 53 10 f0       	push   $0xf01053fd
f010199d:	e8 8c e7 ff ff       	call   f010012e <_panic>
	assert(pp1->references == 1);
f01019a2:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01019a5:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01019a9:	66 83 f8 01          	cmp    $0x1,%ax
f01019ad:	74 19                	je     f01019c8 <page_check+0x651>
f01019af:	68 3d 57 10 f0       	push   $0xf010573d
f01019b4:	68 6e 54 10 f0       	push   $0xf010546e
f01019b9:	68 dc 00 00 00       	push   $0xdc
f01019be:	68 fd 53 10 f0       	push   $0xf01053fd
f01019c3:	e8 66 e7 ff ff       	call   f010012e <_panic>
	assert(pp2->references == 0);
f01019c8:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01019cb:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01019cf:	66 85 c0             	test   %ax,%ax
f01019d2:	74 19                	je     f01019ed <page_check+0x676>
f01019d4:	68 1c 59 10 f0       	push   $0xf010591c
f01019d9:	68 6e 54 10 f0       	push   $0xf010546e
f01019de:	68 dd 00 00 00       	push   $0xdd
f01019e3:	68 fd 53 10 f0       	push   $0xf01053fd
f01019e8:	e8 41 e7 ff ff       	call   f010012e <_panic>

	// unmapping pp1 at PAGE_SIZE should free it
	unmap_frame(ptr_page_directory, (void*) PAGE_SIZE);
f01019ed:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f01019f2:	83 ec 08             	sub    $0x8,%esp
f01019f5:	68 00 10 00 00       	push   $0x1000
f01019fa:	50                   	push   %eax
f01019fb:	e8 92 0d 00 00       	call   f0102792 <unmap_frame>
f0101a00:	83 c4 10             	add    $0x10,%esp
	assert(check_va2pa(ptr_page_directory, 0x0) == ~0);
f0101a03:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101a08:	83 ec 08             	sub    $0x8,%esp
f0101a0b:	6a 00                	push   $0x0
f0101a0d:	50                   	push   %eax
f0101a0e:	e8 9e f8 ff ff       	call   f01012b1 <check_va2pa>
f0101a13:	83 c4 10             	add    $0x10,%esp
f0101a16:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101a19:	74 19                	je     f0101a34 <page_check+0x6bd>
f0101a1b:	68 5c 59 10 f0       	push   $0xf010595c
f0101a20:	68 6e 54 10 f0       	push   $0xf010546e
f0101a25:	68 e1 00 00 00       	push   $0xe1
f0101a2a:	68 fd 53 10 f0       	push   $0xf01053fd
f0101a2f:	e8 fa e6 ff ff       	call   f010012e <_panic>
	assert(check_va2pa(ptr_page_directory, PAGE_SIZE) == ~0);
f0101a34:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101a39:	83 ec 08             	sub    $0x8,%esp
f0101a3c:	68 00 10 00 00       	push   $0x1000
f0101a41:	50                   	push   %eax
f0101a42:	e8 6a f8 ff ff       	call   f01012b1 <check_va2pa>
f0101a47:	83 c4 10             	add    $0x10,%esp
f0101a4a:	83 f8 ff             	cmp    $0xffffffff,%eax
f0101a4d:	74 19                	je     f0101a68 <page_check+0x6f1>
f0101a4f:	68 88 59 10 f0       	push   $0xf0105988
f0101a54:	68 6e 54 10 f0       	push   $0xf010546e
f0101a59:	68 e2 00 00 00       	push   $0xe2
f0101a5e:	68 fd 53 10 f0       	push   $0xf01053fd
f0101a63:	e8 c6 e6 ff ff       	call   f010012e <_panic>
	assert(pp1->references == 0);
f0101a68:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101a6b:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101a6f:	66 85 c0             	test   %ax,%ax
f0101a72:	74 19                	je     f0101a8d <page_check+0x716>
f0101a74:	68 b9 59 10 f0       	push   $0xf01059b9
f0101a79:	68 6e 54 10 f0       	push   $0xf010546e
f0101a7e:	68 e3 00 00 00       	push   $0xe3
f0101a83:	68 fd 53 10 f0       	push   $0xf01053fd
f0101a88:	e8 a1 e6 ff ff       	call   f010012e <_panic>
	assert(pp2->references == 0);
f0101a8d:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101a90:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101a94:	66 85 c0             	test   %ax,%ax
f0101a97:	74 19                	je     f0101ab2 <page_check+0x73b>
f0101a99:	68 1c 59 10 f0       	push   $0xf010591c
f0101a9e:	68 6e 54 10 f0       	push   $0xf010546e
f0101aa3:	68 e4 00 00 00       	push   $0xe4
f0101aa8:	68 fd 53 10 f0       	push   $0xf01053fd
f0101aad:	e8 7c e6 ff ff       	call   f010012e <_panic>

	// so it should be returned by allocate_frame
	assert(allocate_frame(&pp) == 0 && pp == pp1);
f0101ab2:	83 ec 0c             	sub    $0xc,%esp
f0101ab5:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0101ab8:	50                   	push   %eax
f0101ab9:	e8 ad 09 00 00       	call   f010246b <allocate_frame>
f0101abe:	83 c4 10             	add    $0x10,%esp
f0101ac1:	85 c0                	test   %eax,%eax
f0101ac3:	75 0a                	jne    f0101acf <page_check+0x758>
f0101ac5:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0101ac8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101acb:	39 c2                	cmp    %eax,%edx
f0101acd:	74 19                	je     f0101ae8 <page_check+0x771>
f0101acf:	68 d0 59 10 f0       	push   $0xf01059d0
f0101ad4:	68 6e 54 10 f0       	push   $0xf010546e
f0101ad9:	68 e7 00 00 00       	push   $0xe7
f0101ade:	68 fd 53 10 f0       	push   $0xf01053fd
f0101ae3:	e8 46 e6 ff ff       	call   f010012e <_panic>

	// should be no free memory
	assert(allocate_frame(&pp) == E_NO_MEM);
f0101ae8:	83 ec 0c             	sub    $0xc,%esp
f0101aeb:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0101aee:	50                   	push   %eax
f0101aef:	e8 77 09 00 00       	call   f010246b <allocate_frame>
f0101af4:	83 c4 10             	add    $0x10,%esp
f0101af7:	83 f8 fc             	cmp    $0xfffffffc,%eax
f0101afa:	74 19                	je     f0101b15 <page_check+0x79e>
f0101afc:	68 38 56 10 f0       	push   $0xf0105638
f0101b01:	68 6e 54 10 f0       	push   $0xf010546e
f0101b06:	68 ea 00 00 00       	push   $0xea
f0101b0b:	68 fd 53 10 f0       	push   $0xf01053fd
f0101b10:	e8 19 e6 ff ff       	call   f010012e <_panic>

	// forcibly take pp0 back
	assert(EXTRACT_ADDRESS(ptr_page_directory[0]) == to_physical_address(pp0));
f0101b15:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101b1a:	8b 00                	mov    (%eax),%eax
f0101b1c:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0101b21:	89 c3                	mov    %eax,%ebx
f0101b23:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101b26:	83 ec 0c             	sub    $0xc,%esp
f0101b29:	50                   	push   %eax
f0101b2a:	e8 30 f4 ff ff       	call   f0100f5f <to_physical_address>
f0101b2f:	83 c4 10             	add    $0x10,%esp
f0101b32:	39 c3                	cmp    %eax,%ebx
f0101b34:	74 19                	je     f0101b4f <page_check+0x7d8>
f0101b36:	68 b8 56 10 f0       	push   $0xf01056b8
f0101b3b:	68 6e 54 10 f0       	push   $0xf010546e
f0101b40:	68 ed 00 00 00       	push   $0xed
f0101b45:	68 fd 53 10 f0       	push   $0xf01053fd
f0101b4a:	e8 df e5 ff ff       	call   f010012e <_panic>
	ptr_page_directory[0] = 0;
f0101b4f:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101b54:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	assert(pp0->references == 1);
f0101b5a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101b5d:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0101b61:	66 83 f8 01          	cmp    $0x1,%ax
f0101b65:	74 19                	je     f0101b80 <page_check+0x809>
f0101b67:	68 52 57 10 f0       	push   $0xf0105752
f0101b6c:	68 6e 54 10 f0       	push   $0xf010546e
f0101b71:	68 ef 00 00 00       	push   $0xef
f0101b76:	68 fd 53 10 f0       	push   $0xf01053fd
f0101b7b:	e8 ae e5 ff ff       	call   f010012e <_panic>
	pp0->references = 0;
f0101b80:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101b83:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)

	// give free list back
	free_frame_list = fl;
f0101b89:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101b8c:	a3 b8 73 14 f0       	mov    %eax,0xf01473b8

	// free the frames_info we took
	free_frame(pp0);
f0101b91:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101b94:	83 ec 0c             	sub    $0xc,%esp
f0101b97:	50                   	push   %eax
f0101b98:	e8 35 09 00 00       	call   f01024d2 <free_frame>
f0101b9d:	83 c4 10             	add    $0x10,%esp
	free_frame(pp1);
f0101ba0:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101ba3:	83 ec 0c             	sub    $0xc,%esp
f0101ba6:	50                   	push   %eax
f0101ba7:	e8 26 09 00 00       	call   f01024d2 <free_frame>
f0101bac:	83 c4 10             	add    $0x10,%esp
	free_frame(pp2);
f0101baf:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101bb2:	83 ec 0c             	sub    $0xc,%esp
f0101bb5:	50                   	push   %eax
f0101bb6:	e8 17 09 00 00       	call   f01024d2 <free_frame>
f0101bbb:	83 c4 10             	add    $0x10,%esp

	cprintf("page_check() succeeded!\n");
f0101bbe:	83 ec 0c             	sub    $0xc,%esp
f0101bc1:	68 f6 59 10 f0       	push   $0xf01059f6
f0101bc6:	e8 93 14 00 00       	call   f010305e <cprintf>
f0101bcb:	83 c4 10             	add    $0x10,%esp
}
f0101bce:	90                   	nop
f0101bcf:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101bd2:	c9                   	leave  
f0101bd3:	c3                   	ret    

f0101bd4 <turn_on_paging>:

void turn_on_paging()
{
f0101bd4:	55                   	push   %ebp
f0101bd5:	89 e5                	mov    %esp,%ebp
f0101bd7:	83 ec 20             	sub    $0x20,%esp
	// mapping, even though we are turning on paging and reconfiguring
	// segmentation.

	// Map VA 0:4MB same as VA (KERNEL_BASE), i.e. to PA 0:4MB.
	// (Limits our kernel to <4MB)
	ptr_page_directory[0] = ptr_page_directory[PDX(KERNEL_BASE)];
f0101bda:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101bdf:	8b 15 c4 73 14 f0    	mov    0xf01473c4,%edx
f0101be5:	8b 92 00 0f 00 00    	mov    0xf00(%edx),%edx
f0101beb:	89 10                	mov    %edx,(%eax)

	// Install page table.
	lcr3(phys_page_directory);
f0101bed:	a1 c8 73 14 f0       	mov    0xf01473c8,%eax
f0101bf2:	89 45 fc             	mov    %eax,-0x4(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0101bf5:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101bf8:	0f 22 d8             	mov    %eax,%cr3

static __inline uint32
rcr0(void)
{
	uint32 val;
	__asm __volatile("movl %%cr0,%0" : "=r" (val));
f0101bfb:	0f 20 c0             	mov    %cr0,%eax
f0101bfe:	89 45 f4             	mov    %eax,-0xc(%ebp)
	return val;
f0101c01:	8b 45 f4             	mov    -0xc(%ebp),%eax

	// Turn on paging.
	uint32 cr0;
	cr0 = rcr0();
f0101c04:	89 45 f8             	mov    %eax,-0x8(%ebp)
	cr0 |= CR0_PE|CR0_PG|CR0_AM|CR0_WP|CR0_NE|CR0_TS|CR0_EM|CR0_MP;
f0101c07:	81 4d f8 2f 00 05 80 	orl    $0x8005002f,-0x8(%ebp)
	cr0 &= ~(CR0_TS|CR0_EM);
f0101c0e:	83 65 f8 f3          	andl   $0xfffffff3,-0x8(%ebp)
f0101c12:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0101c15:	89 45 f0             	mov    %eax,-0x10(%ebp)
}

static __inline void
lcr0(uint32 val)
{
	__asm __volatile("movl %0,%%cr0" : : "r" (val));
f0101c18:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101c1b:	0f 22 c0             	mov    %eax,%cr0

	// Current mapping: KERNEL_BASE+x => x => x.
	// (x < 4MB so uses paging ptr_page_directory[0])

	// Reload all segment registers.
	asm volatile("lgdt gdt_pd");
f0101c1e:	0f 01 15 f0 b5 11 f0 	lgdtl  0xf011b5f0
	asm volatile("movw %%ax,%%gs" :: "a" (GD_UD|3));
f0101c25:	b8 23 00 00 00       	mov    $0x23,%eax
f0101c2a:	8e e8                	mov    %eax,%gs
	asm volatile("movw %%ax,%%fs" :: "a" (GD_UD|3));
f0101c2c:	b8 23 00 00 00       	mov    $0x23,%eax
f0101c31:	8e e0                	mov    %eax,%fs
	asm volatile("movw %%ax,%%es" :: "a" (GD_KD));
f0101c33:	b8 10 00 00 00       	mov    $0x10,%eax
f0101c38:	8e c0                	mov    %eax,%es
	asm volatile("movw %%ax,%%ds" :: "a" (GD_KD));
f0101c3a:	b8 10 00 00 00       	mov    $0x10,%eax
f0101c3f:	8e d8                	mov    %eax,%ds
	asm volatile("movw %%ax,%%ss" :: "a" (GD_KD));
f0101c41:	b8 10 00 00 00       	mov    $0x10,%eax
f0101c46:	8e d0                	mov    %eax,%ss
	asm volatile("ljmp %0,$1f\n 1:\n" :: "i" (GD_KT));  // reload cs
f0101c48:	ea 4f 1c 10 f0 08 00 	ljmp   $0x8,$0xf0101c4f
	asm volatile("lldt %%ax" :: "a" (0));
f0101c4f:	b8 00 00 00 00       	mov    $0x0,%eax
f0101c54:	0f 00 d0             	lldt   %ax

	// Final mapping: KERNEL_BASE + x => KERNEL_BASE + x => x.

	// This mapping was only used after paging was turned on but
	// before the segment registers were reloaded.
	ptr_page_directory[0] = 0;
f0101c57:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101c5c:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

	// Flush the TLB for good measure, to kill the ptr_page_directory[0] mapping.
	lcr3(phys_page_directory);
f0101c62:	a1 c8 73 14 f0       	mov    0xf01473c8,%eax
f0101c67:	89 45 ec             	mov    %eax,-0x14(%ebp)
}

static __inline void
lcr3(uint32 val)
{
	__asm __volatile("movl %0,%%cr3" : : "r" (val));
f0101c6a:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101c6d:	0f 22 d8             	mov    %eax,%cr3
}
f0101c70:	90                   	nop
f0101c71:	c9                   	leave  
f0101c72:	c3                   	ret    

f0101c73 <setup_listing_to_all_page_tables_entries>:

void setup_listing_to_all_page_tables_entries()
{
f0101c73:	55                   	push   %ebp
f0101c74:	89 e5                	mov    %esp,%ebp
f0101c76:	83 ec 18             	sub    $0x18,%esp
	//////////////////////////////////////////////////////////////////////
	// Recursively insert PD in itself as a page table, to form
	// a virtual page table at virtual address VPT.

	// Permissions: kernel RW, user NONE
	uint32 phys_frame_address = K_PHYSICAL_ADDRESS(ptr_page_directory);
f0101c79:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101c7e:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0101c81:	81 7d f4 ff ff ff ef 	cmpl   $0xefffffff,-0xc(%ebp)
f0101c88:	77 17                	ja     f0101ca1 <setup_listing_to_all_page_tables_entries+0x2e>
f0101c8a:	ff 75 f4             	pushl  -0xc(%ebp)
f0101c8d:	68 cc 53 10 f0       	push   $0xf01053cc
f0101c92:	68 39 01 00 00       	push   $0x139
f0101c97:	68 fd 53 10 f0       	push   $0xf01053fd
f0101c9c:	e8 8d e4 ff ff       	call   f010012e <_panic>
f0101ca1:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101ca4:	05 00 00 00 10       	add    $0x10000000,%eax
f0101ca9:	89 45 f0             	mov    %eax,-0x10(%ebp)
	ptr_page_directory[PDX(VPT)] = CONSTRUCT_ENTRY(phys_frame_address , PERM_PRESENT | PERM_WRITEABLE);
f0101cac:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101cb1:	05 fc 0e 00 00       	add    $0xefc,%eax
f0101cb6:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0101cb9:	83 ca 03             	or     $0x3,%edx
f0101cbc:	89 10                	mov    %edx,(%eax)

	// same for UVPT
	//Permissions: kernel R, user R
	ptr_page_directory[PDX(UVPT)] = K_PHYSICAL_ADDRESS(ptr_page_directory)|PERM_USER|PERM_PRESENT;
f0101cbe:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101cc3:	8d 90 f4 0e 00 00    	lea    0xef4(%eax),%edx
f0101cc9:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101cce:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101cd1:	81 7d ec ff ff ff ef 	cmpl   $0xefffffff,-0x14(%ebp)
f0101cd8:	77 17                	ja     f0101cf1 <setup_listing_to_all_page_tables_entries+0x7e>
f0101cda:	ff 75 ec             	pushl  -0x14(%ebp)
f0101cdd:	68 cc 53 10 f0       	push   $0xf01053cc
f0101ce2:	68 3e 01 00 00       	push   $0x13e
f0101ce7:	68 fd 53 10 f0       	push   $0xf01053fd
f0101cec:	e8 3d e4 ff ff       	call   f010012e <_panic>
f0101cf1:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0101cf4:	05 00 00 00 10       	add    $0x10000000,%eax
f0101cf9:	83 c8 05             	or     $0x5,%eax
f0101cfc:	89 02                	mov    %eax,(%edx)

}
f0101cfe:	90                   	nop
f0101cff:	c9                   	leave  
f0101d00:	c3                   	ret    

f0101d01 <envid2env>:
//   0 on success, -E_BAD_ENV on error.
//   On success, sets *penv to the environment.
//   On error, sets *penv to NULL.
//
int envid2env(int32  envid, struct Env **env_store, bool checkperm)
{
f0101d01:	55                   	push   %ebp
f0101d02:	89 e5                	mov    %esp,%ebp
f0101d04:	83 ec 10             	sub    $0x10,%esp
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
f0101d07:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0101d0b:	75 12                	jne    f0101d1f <envid2env+0x1e>
		*env_store = curenv;
f0101d0d:	8b 15 30 6b 14 f0    	mov    0xf0146b30,%edx
f0101d13:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101d16:	89 10                	mov    %edx,(%eax)
		return 0;
f0101d18:	b8 00 00 00 00       	mov    $0x0,%eax
f0101d1d:	eb 7a                	jmp    f0101d99 <envid2env+0x98>
	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
f0101d1f:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f0101d25:	8b 45 08             	mov    0x8(%ebp),%eax
f0101d28:	25 ff 03 00 00       	and    $0x3ff,%eax
f0101d2d:	6b c0 64             	imul   $0x64,%eax,%eax
f0101d30:	01 d0                	add    %edx,%eax
f0101d32:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (e->env_status == ENV_FREE || e->env_id != envid) {
f0101d35:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101d38:	8b 40 54             	mov    0x54(%eax),%eax
f0101d3b:	85 c0                	test   %eax,%eax
f0101d3d:	74 0b                	je     f0101d4a <envid2env+0x49>
f0101d3f:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101d42:	8b 40 4c             	mov    0x4c(%eax),%eax
f0101d45:	3b 45 08             	cmp    0x8(%ebp),%eax
f0101d48:	74 10                	je     f0101d5a <envid2env+0x59>
		*env_store = 0;
f0101d4a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101d4d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0101d53:	b8 02 00 00 00       	mov    $0x2,%eax
f0101d58:	eb 3f                	jmp    f0101d99 <envid2env+0x98>
	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
f0101d5a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0101d5e:	74 2c                	je     f0101d8c <envid2env+0x8b>
f0101d60:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0101d65:	39 45 fc             	cmp    %eax,-0x4(%ebp)
f0101d68:	74 22                	je     f0101d8c <envid2env+0x8b>
f0101d6a:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0101d6d:	8b 50 50             	mov    0x50(%eax),%edx
f0101d70:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0101d75:	8b 40 4c             	mov    0x4c(%eax),%eax
f0101d78:	39 c2                	cmp    %eax,%edx
f0101d7a:	74 10                	je     f0101d8c <envid2env+0x8b>
		*env_store = 0;
f0101d7c:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101d7f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		return -E_BAD_ENV;
f0101d85:	b8 02 00 00 00       	mov    $0x2,%eax
f0101d8a:	eb 0d                	jmp    f0101d99 <envid2env+0x98>
	}

	*env_store = e;
f0101d8c:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101d8f:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0101d92:	89 10                	mov    %edx,(%eax)
	return 0;
f0101d94:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101d99:	c9                   	leave  
f0101d9a:	c3                   	ret    

f0101d9b <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f0101d9b:	55                   	push   %ebp
f0101d9c:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0101d9e:	8b 45 08             	mov    0x8(%ebp),%eax
f0101da1:	8b 15 bc 73 14 f0    	mov    0xf01473bc,%edx
f0101da7:	29 d0                	sub    %edx,%eax
f0101da9:	c1 f8 02             	sar    $0x2,%eax
f0101dac:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f0101db2:	5d                   	pop    %ebp
f0101db3:	c3                   	ret    

f0101db4 <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f0101db4:	55                   	push   %ebp
f0101db5:	89 e5                	mov    %esp,%ebp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f0101db7:	ff 75 08             	pushl  0x8(%ebp)
f0101dba:	e8 dc ff ff ff       	call   f0101d9b <to_frame_number>
f0101dbf:	83 c4 04             	add    $0x4,%esp
f0101dc2:	c1 e0 0c             	shl    $0xc,%eax
}
f0101dc5:	c9                   	leave  
f0101dc6:	c3                   	ret    

f0101dc7 <to_frame_info>:

static inline struct Frame_Info* to_frame_info(uint32 physical_address)
{
f0101dc7:	55                   	push   %ebp
f0101dc8:	89 e5                	mov    %esp,%ebp
f0101dca:	83 ec 08             	sub    $0x8,%esp
	if (PPN(physical_address) >= number_of_frames)
f0101dcd:	8b 45 08             	mov    0x8(%ebp),%eax
f0101dd0:	c1 e8 0c             	shr    $0xc,%eax
f0101dd3:	89 c2                	mov    %eax,%edx
f0101dd5:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f0101dda:	39 c2                	cmp    %eax,%edx
f0101ddc:	72 14                	jb     f0101df2 <to_frame_info+0x2b>
		panic("to_frame_info called with invalid pa");
f0101dde:	83 ec 04             	sub    $0x4,%esp
f0101de1:	68 10 5a 10 f0       	push   $0xf0105a10
f0101de6:	6a 39                	push   $0x39
f0101de8:	68 35 5a 10 f0       	push   $0xf0105a35
f0101ded:	e8 3c e3 ff ff       	call   f010012e <_panic>
	return &frames_info[PPN(physical_address)];
f0101df2:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0101df8:	8b 45 08             	mov    0x8(%ebp),%eax
f0101dfb:	c1 e8 0c             	shr    $0xc,%eax
f0101dfe:	89 c2                	mov    %eax,%edx
f0101e00:	89 d0                	mov    %edx,%eax
f0101e02:	01 c0                	add    %eax,%eax
f0101e04:	01 d0                	add    %edx,%eax
f0101e06:	c1 e0 02             	shl    $0x2,%eax
f0101e09:	01 c8                	add    %ecx,%eax
}
f0101e0b:	c9                   	leave  
f0101e0c:	c3                   	ret    

f0101e0d <initialize_kernel_VM>:
//
// From USER_TOP to USER_LIMIT, the user is allowed to read but not write.
// Above USER_LIMIT the user cannot read (or write).

void initialize_kernel_VM()
{
f0101e0d:	55                   	push   %ebp
f0101e0e:	89 e5                	mov    %esp,%ebp
f0101e10:	83 ec 28             	sub    $0x28,%esp
	//panic("initialize_kernel_VM: This function is not finished\n");

	//////////////////////////////////////////////////////////////////////
	// create initial page directory.

	ptr_page_directory = boot_allocate_space(PAGE_SIZE, PAGE_SIZE);
f0101e13:	83 ec 08             	sub    $0x8,%esp
f0101e16:	68 00 10 00 00       	push   $0x1000
f0101e1b:	68 00 10 00 00       	push   $0x1000
f0101e20:	e8 ca 01 00 00       	call   f0101fef <boot_allocate_space>
f0101e25:	83 c4 10             	add    $0x10,%esp
f0101e28:	a3 c4 73 14 f0       	mov    %eax,0xf01473c4
	memset(ptr_page_directory, 0, PAGE_SIZE);
f0101e2d:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101e32:	83 ec 04             	sub    $0x4,%esp
f0101e35:	68 00 10 00 00       	push   $0x1000
f0101e3a:	6a 00                	push   $0x0
f0101e3c:	50                   	push   %eax
f0101e3d:	e8 3c 29 00 00       	call   f010477e <memset>
f0101e42:	83 c4 10             	add    $0x10,%esp
	phys_page_directory = K_PHYSICAL_ADDRESS(ptr_page_directory);
f0101e45:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101e4a:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0101e4d:	81 7d f4 ff ff ff ef 	cmpl   $0xefffffff,-0xc(%ebp)
f0101e54:	77 14                	ja     f0101e6a <initialize_kernel_VM+0x5d>
f0101e56:	ff 75 f4             	pushl  -0xc(%ebp)
f0101e59:	68 50 5a 10 f0       	push   $0xf0105a50
f0101e5e:	6a 3c                	push   $0x3c
f0101e60:	68 81 5a 10 f0       	push   $0xf0105a81
f0101e65:	e8 c4 e2 ff ff       	call   f010012e <_panic>
f0101e6a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0101e6d:	05 00 00 00 10       	add    $0x10000000,%eax
f0101e72:	a3 c8 73 14 f0       	mov    %eax,0xf01473c8
	// Map the kernel stack with VA range :
	//  [KERNEL_STACK_TOP-KERNEL_STACK_SIZE, KERNEL_STACK_TOP), 
	// to physical address : "phys_stack_bottom".
	//     Permissions: kernel RW, user NONE
	// Your code goes here:
	boot_map_range(ptr_page_directory, KERNEL_STACK_TOP - KERNEL_STACK_SIZE, KERNEL_STACK_SIZE, K_PHYSICAL_ADDRESS(ptr_stack_bottom), PERM_WRITEABLE) ;
f0101e77:	c7 45 f0 00 30 11 f0 	movl   $0xf0113000,-0x10(%ebp)
f0101e7e:	81 7d f0 ff ff ff ef 	cmpl   $0xefffffff,-0x10(%ebp)
f0101e85:	77 14                	ja     f0101e9b <initialize_kernel_VM+0x8e>
f0101e87:	ff 75 f0             	pushl  -0x10(%ebp)
f0101e8a:	68 50 5a 10 f0       	push   $0xf0105a50
f0101e8f:	6a 44                	push   $0x44
f0101e91:	68 81 5a 10 f0       	push   $0xf0105a81
f0101e96:	e8 93 e2 ff ff       	call   f010012e <_panic>
f0101e9b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0101e9e:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101ea4:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101ea9:	83 ec 0c             	sub    $0xc,%esp
f0101eac:	6a 02                	push   $0x2
f0101eae:	52                   	push   %edx
f0101eaf:	68 00 80 00 00       	push   $0x8000
f0101eb4:	68 00 80 bf ef       	push   $0xefbf8000
f0101eb9:	50                   	push   %eax
f0101eba:	e8 94 01 00 00       	call   f0102053 <boot_map_range>
f0101ebf:	83 c4 20             	add    $0x20,%esp
	//      the PA range [0, 2^32 - KERNEL_BASE)
	// We might not have 2^32 - KERNEL_BASE bytes of physical memory, but
	// we just set up the mapping anyway.
	// Permissions: kernel RW, user NONE
	// Your code goes here: 
	boot_map_range(ptr_page_directory, KERNEL_BASE, 0xFFFFFFFF - KERNEL_BASE, 0, PERM_WRITEABLE) ;
f0101ec2:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101ec7:	83 ec 0c             	sub    $0xc,%esp
f0101eca:	6a 02                	push   $0x2
f0101ecc:	6a 00                	push   $0x0
f0101ece:	68 ff ff ff 0f       	push   $0xfffffff
f0101ed3:	68 00 00 00 f0       	push   $0xf0000000
f0101ed8:	50                   	push   %eax
f0101ed9:	e8 75 01 00 00       	call   f0102053 <boot_map_range>
f0101ede:	83 c4 20             	add    $0x20,%esp
	// Permissions:
	//    - frames_info -- kernel RW, user NONE
	//    - the image mapped at READ_ONLY_FRAMES_INFO  -- kernel R, user R
	// Your code goes here:
	uint32 array_size;
	array_size = number_of_frames * sizeof(struct Frame_Info) ;
f0101ee1:	8b 15 a8 73 14 f0    	mov    0xf01473a8,%edx
f0101ee7:	89 d0                	mov    %edx,%eax
f0101ee9:	01 c0                	add    %eax,%eax
f0101eeb:	01 d0                	add    %edx,%eax
f0101eed:	c1 e0 02             	shl    $0x2,%eax
f0101ef0:	89 45 ec             	mov    %eax,-0x14(%ebp)
	frames_info = boot_allocate_space(array_size, PAGE_SIZE);
f0101ef3:	83 ec 08             	sub    $0x8,%esp
f0101ef6:	68 00 10 00 00       	push   $0x1000
f0101efb:	ff 75 ec             	pushl  -0x14(%ebp)
f0101efe:	e8 ec 00 00 00       	call   f0101fef <boot_allocate_space>
f0101f03:	83 c4 10             	add    $0x10,%esp
f0101f06:	a3 bc 73 14 f0       	mov    %eax,0xf01473bc
	boot_map_range(ptr_page_directory, READ_ONLY_FRAMES_INFO, array_size, K_PHYSICAL_ADDRESS(frames_info), PERM_USER) ;
f0101f0b:	a1 bc 73 14 f0       	mov    0xf01473bc,%eax
f0101f10:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0101f13:	81 7d e8 ff ff ff ef 	cmpl   $0xefffffff,-0x18(%ebp)
f0101f1a:	77 14                	ja     f0101f30 <initialize_kernel_VM+0x123>
f0101f1c:	ff 75 e8             	pushl  -0x18(%ebp)
f0101f1f:	68 50 5a 10 f0       	push   $0xf0105a50
f0101f24:	6a 5f                	push   $0x5f
f0101f26:	68 81 5a 10 f0       	push   $0xf0105a81
f0101f2b:	e8 fe e1 ff ff       	call   f010012e <_panic>
f0101f30:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0101f33:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0101f39:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101f3e:	83 ec 0c             	sub    $0xc,%esp
f0101f41:	6a 04                	push   $0x4
f0101f43:	52                   	push   %edx
f0101f44:	ff 75 ec             	pushl  -0x14(%ebp)
f0101f47:	68 00 00 00 ef       	push   $0xef000000
f0101f4c:	50                   	push   %eax
f0101f4d:	e8 01 01 00 00       	call   f0102053 <boot_map_range>
f0101f52:	83 c4 20             	add    $0x20,%esp


	// This allows the kernel & user to access any page table entry using a
	// specified VA for each: VPT for kernel and UVPT for User.
	setup_listing_to_all_page_tables_entries();
f0101f55:	e8 19 fd ff ff       	call   f0101c73 <setup_listing_to_all_page_tables_entries>
	// Permissions:
	//    - envs itself -- kernel RW, user NONE
	//    - the image of envs mapped at UENVS  -- kernel R, user R
	
	// LAB 3: Your code here.
	int envs_size = NENV * sizeof(struct Env) ;
f0101f5a:	c7 45 e4 00 90 01 00 	movl   $0x19000,-0x1c(%ebp)

	//allocate space for "envs" array aligned on 4KB boundary
	envs = boot_allocate_space(envs_size, PAGE_SIZE);
f0101f61:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0101f64:	83 ec 08             	sub    $0x8,%esp
f0101f67:	68 00 10 00 00       	push   $0x1000
f0101f6c:	50                   	push   %eax
f0101f6d:	e8 7d 00 00 00       	call   f0101fef <boot_allocate_space>
f0101f72:	83 c4 10             	add    $0x10,%esp
f0101f75:	a3 2c 6b 14 f0       	mov    %eax,0xf0146b2c

	//make the user to access this array by mapping it to UPAGES linear address (UPAGES is in User/Kernel space)
	boot_map_range(ptr_page_directory, UENVS, envs_size, K_PHYSICAL_ADDRESS(envs), PERM_USER) ;
f0101f7a:	a1 2c 6b 14 f0       	mov    0xf0146b2c,%eax
f0101f7f:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0101f82:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0101f89:	77 14                	ja     f0101f9f <initialize_kernel_VM+0x192>
f0101f8b:	ff 75 e0             	pushl  -0x20(%ebp)
f0101f8e:	68 50 5a 10 f0       	push   $0xf0105a50
f0101f93:	6a 75                	push   $0x75
f0101f95:	68 81 5a 10 f0       	push   $0xf0105a81
f0101f9a:	e8 8f e1 ff ff       	call   f010012e <_panic>
f0101f9f:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0101fa2:	8d 88 00 00 00 10    	lea    0x10000000(%eax),%ecx
f0101fa8:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0101fab:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101fb0:	83 ec 0c             	sub    $0xc,%esp
f0101fb3:	6a 04                	push   $0x4
f0101fb5:	51                   	push   %ecx
f0101fb6:	52                   	push   %edx
f0101fb7:	68 00 00 c0 ee       	push   $0xeec00000
f0101fbc:	50                   	push   %eax
f0101fbd:	e8 91 00 00 00       	call   f0102053 <boot_map_range>
f0101fc2:	83 c4 20             	add    $0x20,%esp

	//update permissions of the corresponding entry in page directory to make it USER with PERMISSION read only
	ptr_page_directory[PDX(UENVS)] = ptr_page_directory[PDX(UENVS)]|(PERM_USER|(PERM_PRESENT & (~PERM_WRITEABLE)));
f0101fc5:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0101fca:	05 ec 0e 00 00       	add    $0xeec,%eax
f0101fcf:	8b 15 c4 73 14 f0    	mov    0xf01473c4,%edx
f0101fd5:	81 c2 ec 0e 00 00    	add    $0xeec,%edx
f0101fdb:	8b 12                	mov    (%edx),%edx
f0101fdd:	83 ca 05             	or     $0x5,%edx
f0101fe0:	89 10                	mov    %edx,(%eax)


	// Check that the initial page directory has been set up correctly.
	check_boot_pgdir();
f0101fe2:	e8 76 f0 ff ff       	call   f010105d <check_boot_pgdir>
	
	// NOW: Turn off the segmentation by setting the segments' base to 0, and
	// turn on the paging by setting the corresponding flags in control register 0 (cr0)
	turn_on_paging() ;
f0101fe7:	e8 e8 fb ff ff       	call   f0101bd4 <turn_on_paging>
}
f0101fec:	90                   	nop
f0101fed:	c9                   	leave  
f0101fee:	c3                   	ret    

f0101fef <boot_allocate_space>:
// It's too early to run out of memory.
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
// 
void* boot_allocate_space(uint32 size, uint32 align)
{
f0101fef:	55                   	push   %ebp
f0101ff0:	89 e5                	mov    %esp,%ebp
f0101ff2:	83 ec 10             	sub    $0x10,%esp
	// Initialize ptr_free_mem if this is the first time.
	// 'end_of_kernel' is a symbol automatically generated by the linker,
	// which points to the end of the kernel-
	// i.e., the first virtual address that the linker
	// did not assign to any kernel code or global variables.
	if (ptr_free_mem == 0)
f0101ff5:	a1 c0 73 14 f0       	mov    0xf01473c0,%eax
f0101ffa:	85 c0                	test   %eax,%eax
f0101ffc:	75 0a                	jne    f0102008 <boot_allocate_space+0x19>
		ptr_free_mem = end_of_kernel;
f0101ffe:	c7 05 c0 73 14 f0 cc 	movl   $0xf01473cc,0xf01473c0
f0102005:	73 14 f0 

	// Your code here:
	//	Step 1: round ptr_free_mem up to be aligned properly
	ptr_free_mem = ROUNDUP(ptr_free_mem, PAGE_SIZE) ;
f0102008:	c7 45 fc 00 10 00 00 	movl   $0x1000,-0x4(%ebp)
f010200f:	a1 c0 73 14 f0       	mov    0xf01473c0,%eax
f0102014:	89 c2                	mov    %eax,%edx
f0102016:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0102019:	01 d0                	add    %edx,%eax
f010201b:	83 e8 01             	sub    $0x1,%eax
f010201e:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0102021:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0102024:	ba 00 00 00 00       	mov    $0x0,%edx
f0102029:	f7 75 fc             	divl   -0x4(%ebp)
f010202c:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010202f:	29 d0                	sub    %edx,%eax
f0102031:	a3 c0 73 14 f0       	mov    %eax,0xf01473c0
	
	//	Step 2: save current value of ptr_free_mem as allocated space
	void *ptr_allocated_mem;
	ptr_allocated_mem = ptr_free_mem ;
f0102036:	a1 c0 73 14 f0       	mov    0xf01473c0,%eax
f010203b:	89 45 f4             	mov    %eax,-0xc(%ebp)

	//	Step 3: increase ptr_free_mem to record allocation
	ptr_free_mem += size ;
f010203e:	8b 15 c0 73 14 f0    	mov    0xf01473c0,%edx
f0102044:	8b 45 08             	mov    0x8(%ebp),%eax
f0102047:	01 d0                	add    %edx,%eax
f0102049:	a3 c0 73 14 f0       	mov    %eax,0xf01473c0

	//	Step 4: return allocated space
	return ptr_allocated_mem ;
f010204e:	8b 45 f4             	mov    -0xc(%ebp),%eax

}
f0102051:	c9                   	leave  
f0102052:	c3                   	ret    

f0102053 <boot_map_range>:
//
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
//
void boot_map_range(uint32 *ptr_page_directory, uint32 virtual_address, uint32 size, uint32 physical_address, int perm)
{
f0102053:	55                   	push   %ebp
f0102054:	89 e5                	mov    %esp,%ebp
f0102056:	83 ec 28             	sub    $0x28,%esp
	int i = 0 ;
f0102059:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	physical_address = ROUNDUP(physical_address, PAGE_SIZE) ;
f0102060:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f0102067:	8b 55 14             	mov    0x14(%ebp),%edx
f010206a:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010206d:	01 d0                	add    %edx,%eax
f010206f:	83 e8 01             	sub    $0x1,%eax
f0102072:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102075:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102078:	ba 00 00 00 00       	mov    $0x0,%edx
f010207d:	f7 75 f0             	divl   -0x10(%ebp)
f0102080:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102083:	29 d0                	sub    %edx,%eax
f0102085:	89 45 14             	mov    %eax,0x14(%ebp)
	for (i = 0 ; i < size ; i += PAGE_SIZE)
f0102088:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f010208f:	eb 53                	jmp    f01020e4 <boot_map_range+0x91>
	{
		uint32 *ptr_page_table = boot_get_page_table(ptr_page_directory, virtual_address, 1) ;
f0102091:	83 ec 04             	sub    $0x4,%esp
f0102094:	6a 01                	push   $0x1
f0102096:	ff 75 0c             	pushl  0xc(%ebp)
f0102099:	ff 75 08             	pushl  0x8(%ebp)
f010209c:	e8 4e 00 00 00       	call   f01020ef <boot_get_page_table>
f01020a1:	83 c4 10             	add    $0x10,%esp
f01020a4:	89 45 e8             	mov    %eax,-0x18(%ebp)
		uint32 index_page_table = PTX(virtual_address);
f01020a7:	8b 45 0c             	mov    0xc(%ebp),%eax
f01020aa:	c1 e8 0c             	shr    $0xc,%eax
f01020ad:	25 ff 03 00 00       	and    $0x3ff,%eax
f01020b2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		ptr_page_table[index_page_table] = CONSTRUCT_ENTRY(physical_address, perm | PERM_PRESENT) ;
f01020b5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01020b8:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f01020bf:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01020c2:	01 c2                	add    %eax,%edx
f01020c4:	8b 45 18             	mov    0x18(%ebp),%eax
f01020c7:	0b 45 14             	or     0x14(%ebp),%eax
f01020ca:	83 c8 01             	or     $0x1,%eax
f01020cd:	89 02                	mov    %eax,(%edx)
		physical_address += PAGE_SIZE ;
f01020cf:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
		virtual_address += PAGE_SIZE ;
f01020d6:	81 45 0c 00 10 00 00 	addl   $0x1000,0xc(%ebp)
//
void boot_map_range(uint32 *ptr_page_directory, uint32 virtual_address, uint32 size, uint32 physical_address, int perm)
{
	int i = 0 ;
	physical_address = ROUNDUP(physical_address, PAGE_SIZE) ;
	for (i = 0 ; i < size ; i += PAGE_SIZE)
f01020dd:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
f01020e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01020e7:	3b 45 10             	cmp    0x10(%ebp),%eax
f01020ea:	72 a5                	jb     f0102091 <boot_map_range+0x3e>
		uint32 index_page_table = PTX(virtual_address);
		ptr_page_table[index_page_table] = CONSTRUCT_ENTRY(physical_address, perm | PERM_PRESENT) ;
		physical_address += PAGE_SIZE ;
		virtual_address += PAGE_SIZE ;
	}
}
f01020ec:	90                   	nop
f01020ed:	c9                   	leave  
f01020ee:	c3                   	ret    

f01020ef <boot_get_page_table>:
// boot_get_page_table cannot fail.  It's too early to fail.
// This function may ONLY be used during boot time,
// before the free_frame_list has been set up.
//
uint32* boot_get_page_table(uint32 *ptr_page_directory, uint32 virtual_address, int create)
{
f01020ef:	55                   	push   %ebp
f01020f0:	89 e5                	mov    %esp,%ebp
f01020f2:	83 ec 28             	sub    $0x28,%esp
	uint32 index_page_directory = PDX(virtual_address);
f01020f5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01020f8:	c1 e8 16             	shr    $0x16,%eax
f01020fb:	89 45 f4             	mov    %eax,-0xc(%ebp)
	uint32 page_directory_entry = ptr_page_directory[index_page_directory];
f01020fe:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102101:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0102108:	8b 45 08             	mov    0x8(%ebp),%eax
f010210b:	01 d0                	add    %edx,%eax
f010210d:	8b 00                	mov    (%eax),%eax
f010210f:	89 45 f0             	mov    %eax,-0x10(%ebp)
	
	uint32 phys_page_table = EXTRACT_ADDRESS(page_directory_entry);
f0102112:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102115:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f010211a:	89 45 ec             	mov    %eax,-0x14(%ebp)
	uint32 *ptr_page_table = K_VIRTUAL_ADDRESS(phys_page_table);
f010211d:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102120:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0102123:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102126:	c1 e8 0c             	shr    $0xc,%eax
f0102129:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f010212c:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f0102131:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f0102134:	72 17                	jb     f010214d <boot_get_page_table+0x5e>
f0102136:	ff 75 e8             	pushl  -0x18(%ebp)
f0102139:	68 98 5a 10 f0       	push   $0xf0105a98
f010213e:	68 db 00 00 00       	push   $0xdb
f0102143:	68 81 5a 10 f0       	push   $0xf0105a81
f0102148:	e8 e1 df ff ff       	call   f010012e <_panic>
f010214d:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102150:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102155:	89 45 e0             	mov    %eax,-0x20(%ebp)
	if (phys_page_table == 0)
f0102158:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f010215c:	75 72                	jne    f01021d0 <boot_get_page_table+0xe1>
	{
		if (create)
f010215e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0102162:	74 65                	je     f01021c9 <boot_get_page_table+0xda>
		{
			ptr_page_table = boot_allocate_space(PAGE_SIZE, PAGE_SIZE) ;
f0102164:	83 ec 08             	sub    $0x8,%esp
f0102167:	68 00 10 00 00       	push   $0x1000
f010216c:	68 00 10 00 00       	push   $0x1000
f0102171:	e8 79 fe ff ff       	call   f0101fef <boot_allocate_space>
f0102176:	83 c4 10             	add    $0x10,%esp
f0102179:	89 45 e0             	mov    %eax,-0x20(%ebp)
			phys_page_table = K_PHYSICAL_ADDRESS(ptr_page_table);
f010217c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010217f:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0102182:	81 7d dc ff ff ff ef 	cmpl   $0xefffffff,-0x24(%ebp)
f0102189:	77 17                	ja     f01021a2 <boot_get_page_table+0xb3>
f010218b:	ff 75 dc             	pushl  -0x24(%ebp)
f010218e:	68 50 5a 10 f0       	push   $0xf0105a50
f0102193:	68 e1 00 00 00       	push   $0xe1
f0102198:	68 81 5a 10 f0       	push   $0xf0105a81
f010219d:	e8 8c df ff ff       	call   f010012e <_panic>
f01021a2:	8b 45 dc             	mov    -0x24(%ebp),%eax
f01021a5:	05 00 00 00 10       	add    $0x10000000,%eax
f01021aa:	89 45 ec             	mov    %eax,-0x14(%ebp)
			ptr_page_directory[index_page_directory] = CONSTRUCT_ENTRY(phys_page_table, PERM_PRESENT | PERM_WRITEABLE);
f01021ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01021b0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f01021b7:	8b 45 08             	mov    0x8(%ebp),%eax
f01021ba:	01 d0                	add    %edx,%eax
f01021bc:	8b 55 ec             	mov    -0x14(%ebp),%edx
f01021bf:	83 ca 03             	or     $0x3,%edx
f01021c2:	89 10                	mov    %edx,(%eax)
			return ptr_page_table ;
f01021c4:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01021c7:	eb 0a                	jmp    f01021d3 <boot_get_page_table+0xe4>
		}
		else
			return 0 ;
f01021c9:	b8 00 00 00 00       	mov    $0x0,%eax
f01021ce:	eb 03                	jmp    f01021d3 <boot_get_page_table+0xe4>
	}
	return ptr_page_table ;
f01021d0:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
f01021d3:	c9                   	leave  
f01021d4:	c3                   	ret    

f01021d5 <initialize_paging>:
// After this point, ONLY use the functions below
// to allocate and deallocate physical memory via the free_frame_list,
// and NEVER use boot_allocate_space() or the related boot-time functions above.
//
void initialize_paging()
{
f01021d5:	55                   	push   %ebp
f01021d6:	89 e5                	mov    %esp,%ebp
f01021d8:	53                   	push   %ebx
f01021d9:	83 ec 24             	sub    $0x24,%esp
	//     Some of it is in use, some is free. Where is the kernel?
	//     Which frames are used for page tables and other data structures?
	//
	// Change the code to reflect this.
	int i;
	LIST_INIT(&free_frame_list);
f01021dc:	c7 05 b8 73 14 f0 00 	movl   $0x0,0xf01473b8
f01021e3:	00 00 00 
	
	frames_info[0].references = 1;
f01021e6:	a1 bc 73 14 f0       	mov    0xf01473bc,%eax
f01021eb:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
	
	int range_end = ROUNDUP(PHYS_IO_MEM,PAGE_SIZE);
f01021f1:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
f01021f8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01021fb:	05 ff ff 09 00       	add    $0x9ffff,%eax
f0102200:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102203:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102206:	ba 00 00 00 00       	mov    $0x0,%edx
f010220b:	f7 75 f0             	divl   -0x10(%ebp)
f010220e:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102211:	29 d0                	sub    %edx,%eax
f0102213:	89 45 e8             	mov    %eax,-0x18(%ebp)
			
	for (i = 1; i < range_end/PAGE_SIZE; i++)
f0102216:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
f010221d:	e9 91 00 00 00       	jmp    f01022b3 <initialize_paging+0xde>
	{
		frames_info[i].references = 0;
f0102222:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102228:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010222b:	89 d0                	mov    %edx,%eax
f010222d:	01 c0                	add    %eax,%eax
f010222f:	01 d0                	add    %edx,%eax
f0102231:	c1 e0 02             	shl    $0x2,%eax
f0102234:	01 c8                	add    %ecx,%eax
f0102236:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
f010223c:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102242:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102245:	89 d0                	mov    %edx,%eax
f0102247:	01 c0                	add    %eax,%eax
f0102249:	01 d0                	add    %edx,%eax
f010224b:	c1 e0 02             	shl    $0x2,%eax
f010224e:	01 c8                	add    %ecx,%eax
f0102250:	8b 15 b8 73 14 f0    	mov    0xf01473b8,%edx
f0102256:	89 10                	mov    %edx,(%eax)
f0102258:	8b 00                	mov    (%eax),%eax
f010225a:	85 c0                	test   %eax,%eax
f010225c:	74 1d                	je     f010227b <initialize_paging+0xa6>
f010225e:	8b 0d b8 73 14 f0    	mov    0xf01473b8,%ecx
f0102264:	8b 1d bc 73 14 f0    	mov    0xf01473bc,%ebx
f010226a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010226d:	89 d0                	mov    %edx,%eax
f010226f:	01 c0                	add    %eax,%eax
f0102271:	01 d0                	add    %edx,%eax
f0102273:	c1 e0 02             	shl    $0x2,%eax
f0102276:	01 d8                	add    %ebx,%eax
f0102278:	89 41 04             	mov    %eax,0x4(%ecx)
f010227b:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102281:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102284:	89 d0                	mov    %edx,%eax
f0102286:	01 c0                	add    %eax,%eax
f0102288:	01 d0                	add    %edx,%eax
f010228a:	c1 e0 02             	shl    $0x2,%eax
f010228d:	01 c8                	add    %ecx,%eax
f010228f:	a3 b8 73 14 f0       	mov    %eax,0xf01473b8
f0102294:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f010229a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010229d:	89 d0                	mov    %edx,%eax
f010229f:	01 c0                	add    %eax,%eax
f01022a1:	01 d0                	add    %edx,%eax
f01022a3:	c1 e0 02             	shl    $0x2,%eax
f01022a6:	01 c8                	add    %ecx,%eax
f01022a8:	c7 40 04 b8 73 14 f0 	movl   $0xf01473b8,0x4(%eax)
	
	frames_info[0].references = 1;
	
	int range_end = ROUNDUP(PHYS_IO_MEM,PAGE_SIZE);
			
	for (i = 1; i < range_end/PAGE_SIZE; i++)
f01022af:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f01022b3:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01022b6:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f01022bc:	85 c0                	test   %eax,%eax
f01022be:	0f 48 c2             	cmovs  %edx,%eax
f01022c1:	c1 f8 0c             	sar    $0xc,%eax
f01022c4:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f01022c7:	0f 8f 55 ff ff ff    	jg     f0102222 <initialize_paging+0x4d>
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
	
	for (i = PHYS_IO_MEM/PAGE_SIZE ; i < PHYS_EXTENDED_MEM/PAGE_SIZE; i++)
f01022cd:	c7 45 f4 a0 00 00 00 	movl   $0xa0,-0xc(%ebp)
f01022d4:	eb 1e                	jmp    f01022f4 <initialize_paging+0x11f>
	{
		frames_info[i].references = 1;
f01022d6:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f01022dc:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01022df:	89 d0                	mov    %edx,%eax
f01022e1:	01 c0                	add    %eax,%eax
f01022e3:	01 d0                	add    %edx,%eax
f01022e5:	c1 e0 02             	shl    $0x2,%eax
f01022e8:	01 c8                	add    %ecx,%eax
f01022ea:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
	
	for (i = PHYS_IO_MEM/PAGE_SIZE ; i < PHYS_EXTENDED_MEM/PAGE_SIZE; i++)
f01022f0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f01022f4:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
f01022fb:	7e d9                	jle    f01022d6 <initialize_paging+0x101>
	{
		frames_info[i].references = 1;
	}
		
	range_end = ROUNDUP(K_PHYSICAL_ADDRESS(ptr_free_mem), PAGE_SIZE);
f01022fd:	c7 45 e4 00 10 00 00 	movl   $0x1000,-0x1c(%ebp)
f0102304:	a1 c0 73 14 f0       	mov    0xf01473c0,%eax
f0102309:	89 45 e0             	mov    %eax,-0x20(%ebp)
f010230c:	81 7d e0 ff ff ff ef 	cmpl   $0xefffffff,-0x20(%ebp)
f0102313:	77 17                	ja     f010232c <initialize_paging+0x157>
f0102315:	ff 75 e0             	pushl  -0x20(%ebp)
f0102318:	68 50 5a 10 f0       	push   $0xf0105a50
f010231d:	68 1e 01 00 00       	push   $0x11e
f0102322:	68 81 5a 10 f0       	push   $0xf0105a81
f0102327:	e8 02 de ff ff       	call   f010012e <_panic>
f010232c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010232f:	8d 90 00 00 00 10    	lea    0x10000000(%eax),%edx
f0102335:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0102338:	01 d0                	add    %edx,%eax
f010233a:	83 e8 01             	sub    $0x1,%eax
f010233d:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0102340:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0102343:	ba 00 00 00 00       	mov    $0x0,%edx
f0102348:	f7 75 e4             	divl   -0x1c(%ebp)
f010234b:	8b 45 dc             	mov    -0x24(%ebp),%eax
f010234e:	29 d0                	sub    %edx,%eax
f0102350:	89 45 e8             	mov    %eax,-0x18(%ebp)
	
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
f0102353:	c7 45 f4 00 01 00 00 	movl   $0x100,-0xc(%ebp)
f010235a:	eb 1e                	jmp    f010237a <initialize_paging+0x1a5>
	{
		frames_info[i].references = 1;
f010235c:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102362:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102365:	89 d0                	mov    %edx,%eax
f0102367:	01 c0                	add    %eax,%eax
f0102369:	01 d0                	add    %edx,%eax
f010236b:	c1 e0 02             	shl    $0x2,%eax
f010236e:	01 c8                	add    %ecx,%eax
f0102370:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
		frames_info[i].references = 1;
	}
		
	range_end = ROUNDUP(K_PHYSICAL_ADDRESS(ptr_free_mem), PAGE_SIZE);
	
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
f0102376:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f010237a:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010237d:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f0102383:	85 c0                	test   %eax,%eax
f0102385:	0f 48 c2             	cmovs  %edx,%eax
f0102388:	c1 f8 0c             	sar    $0xc,%eax
f010238b:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f010238e:	7f cc                	jg     f010235c <initialize_paging+0x187>
	{
		frames_info[i].references = 1;
	}
	
	for (i = range_end/PAGE_SIZE ; i < number_of_frames; i++)
f0102390:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102393:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
f0102399:	85 c0                	test   %eax,%eax
f010239b:	0f 48 c2             	cmovs  %edx,%eax
f010239e:	c1 f8 0c             	sar    $0xc,%eax
f01023a1:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01023a4:	e9 91 00 00 00       	jmp    f010243a <initialize_paging+0x265>
	{
		frames_info[i].references = 0;
f01023a9:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f01023af:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01023b2:	89 d0                	mov    %edx,%eax
f01023b4:	01 c0                	add    %eax,%eax
f01023b6:	01 d0                	add    %edx,%eax
f01023b8:	c1 e0 02             	shl    $0x2,%eax
f01023bb:	01 c8                	add    %ecx,%eax
f01023bd:	66 c7 40 08 00 00    	movw   $0x0,0x8(%eax)
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
f01023c3:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f01023c9:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01023cc:	89 d0                	mov    %edx,%eax
f01023ce:	01 c0                	add    %eax,%eax
f01023d0:	01 d0                	add    %edx,%eax
f01023d2:	c1 e0 02             	shl    $0x2,%eax
f01023d5:	01 c8                	add    %ecx,%eax
f01023d7:	8b 15 b8 73 14 f0    	mov    0xf01473b8,%edx
f01023dd:	89 10                	mov    %edx,(%eax)
f01023df:	8b 00                	mov    (%eax),%eax
f01023e1:	85 c0                	test   %eax,%eax
f01023e3:	74 1d                	je     f0102402 <initialize_paging+0x22d>
f01023e5:	8b 0d b8 73 14 f0    	mov    0xf01473b8,%ecx
f01023eb:	8b 1d bc 73 14 f0    	mov    0xf01473bc,%ebx
f01023f1:	8b 55 f4             	mov    -0xc(%ebp),%edx
f01023f4:	89 d0                	mov    %edx,%eax
f01023f6:	01 c0                	add    %eax,%eax
f01023f8:	01 d0                	add    %edx,%eax
f01023fa:	c1 e0 02             	shl    $0x2,%eax
f01023fd:	01 d8                	add    %ebx,%eax
f01023ff:	89 41 04             	mov    %eax,0x4(%ecx)
f0102402:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102408:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010240b:	89 d0                	mov    %edx,%eax
f010240d:	01 c0                	add    %eax,%eax
f010240f:	01 d0                	add    %edx,%eax
f0102411:	c1 e0 02             	shl    $0x2,%eax
f0102414:	01 c8                	add    %ecx,%eax
f0102416:	a3 b8 73 14 f0       	mov    %eax,0xf01473b8
f010241b:	8b 0d bc 73 14 f0    	mov    0xf01473bc,%ecx
f0102421:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0102424:	89 d0                	mov    %edx,%eax
f0102426:	01 c0                	add    %eax,%eax
f0102428:	01 d0                	add    %edx,%eax
f010242a:	c1 e0 02             	shl    $0x2,%eax
f010242d:	01 c8                	add    %ecx,%eax
f010242f:	c7 40 04 b8 73 14 f0 	movl   $0xf01473b8,0x4(%eax)
	for (i = PHYS_EXTENDED_MEM/PAGE_SIZE ; i < range_end/PAGE_SIZE; i++)
	{
		frames_info[i].references = 1;
	}
	
	for (i = range_end/PAGE_SIZE ; i < number_of_frames; i++)
f0102436:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f010243a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010243d:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f0102442:	39 c2                	cmp    %eax,%edx
f0102444:	0f 82 5f ff ff ff    	jb     f01023a9 <initialize_paging+0x1d4>
	{
		frames_info[i].references = 0;
		LIST_INSERT_HEAD(&free_frame_list, &frames_info[i]);
	}
}
f010244a:	90                   	nop
f010244b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f010244e:	c9                   	leave  
f010244f:	c3                   	ret    

f0102450 <initialize_frame_info>:
// Initialize a Frame_Info structure.
// The result has null links and 0 references.
// Note that the corresponding physical frame is NOT initialized!
//
void initialize_frame_info(struct Frame_Info *ptr_frame_info)
{
f0102450:	55                   	push   %ebp
f0102451:	89 e5                	mov    %esp,%ebp
f0102453:	83 ec 08             	sub    $0x8,%esp
	memset(ptr_frame_info, 0, sizeof(*ptr_frame_info));
f0102456:	83 ec 04             	sub    $0x4,%esp
f0102459:	6a 0c                	push   $0xc
f010245b:	6a 00                	push   $0x0
f010245d:	ff 75 08             	pushl  0x8(%ebp)
f0102460:	e8 19 23 00 00       	call   f010477e <memset>
f0102465:	83 c4 10             	add    $0x10,%esp
}
f0102468:	90                   	nop
f0102469:	c9                   	leave  
f010246a:	c3                   	ret    

f010246b <allocate_frame>:
//   E_NO_MEM -- otherwise
//
// Hint: use LIST_FIRST, LIST_REMOVE, and initialize_frame_info
// Hint: references should not be incremented
int allocate_frame(struct Frame_Info **ptr_frame_info)
{
f010246b:	55                   	push   %ebp
f010246c:	89 e5                	mov    %esp,%ebp
f010246e:	83 ec 08             	sub    $0x8,%esp
	// Fill this function in	
	*ptr_frame_info = LIST_FIRST(&free_frame_list);
f0102471:	8b 15 b8 73 14 f0    	mov    0xf01473b8,%edx
f0102477:	8b 45 08             	mov    0x8(%ebp),%eax
f010247a:	89 10                	mov    %edx,(%eax)
	if(*ptr_frame_info == NULL)
f010247c:	8b 45 08             	mov    0x8(%ebp),%eax
f010247f:	8b 00                	mov    (%eax),%eax
f0102481:	85 c0                	test   %eax,%eax
f0102483:	75 07                	jne    f010248c <allocate_frame+0x21>
		return E_NO_MEM;
f0102485:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
f010248a:	eb 44                	jmp    f01024d0 <allocate_frame+0x65>
	
	LIST_REMOVE(*ptr_frame_info);
f010248c:	8b 45 08             	mov    0x8(%ebp),%eax
f010248f:	8b 00                	mov    (%eax),%eax
f0102491:	8b 00                	mov    (%eax),%eax
f0102493:	85 c0                	test   %eax,%eax
f0102495:	74 12                	je     f01024a9 <allocate_frame+0x3e>
f0102497:	8b 45 08             	mov    0x8(%ebp),%eax
f010249a:	8b 00                	mov    (%eax),%eax
f010249c:	8b 00                	mov    (%eax),%eax
f010249e:	8b 55 08             	mov    0x8(%ebp),%edx
f01024a1:	8b 12                	mov    (%edx),%edx
f01024a3:	8b 52 04             	mov    0x4(%edx),%edx
f01024a6:	89 50 04             	mov    %edx,0x4(%eax)
f01024a9:	8b 45 08             	mov    0x8(%ebp),%eax
f01024ac:	8b 00                	mov    (%eax),%eax
f01024ae:	8b 40 04             	mov    0x4(%eax),%eax
f01024b1:	8b 55 08             	mov    0x8(%ebp),%edx
f01024b4:	8b 12                	mov    (%edx),%edx
f01024b6:	8b 12                	mov    (%edx),%edx
f01024b8:	89 10                	mov    %edx,(%eax)
	initialize_frame_info(*ptr_frame_info);
f01024ba:	8b 45 08             	mov    0x8(%ebp),%eax
f01024bd:	8b 00                	mov    (%eax),%eax
f01024bf:	83 ec 0c             	sub    $0xc,%esp
f01024c2:	50                   	push   %eax
f01024c3:	e8 88 ff ff ff       	call   f0102450 <initialize_frame_info>
f01024c8:	83 c4 10             	add    $0x10,%esp
	return 0;
f01024cb:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01024d0:	c9                   	leave  
f01024d1:	c3                   	ret    

f01024d2 <free_frame>:
//
// Return a frame to the free_frame_list.
// (This function should only be called when ptr_frame_info->references reaches 0.)
//
void free_frame(struct Frame_Info *ptr_frame_info)
{
f01024d2:	55                   	push   %ebp
f01024d3:	89 e5                	mov    %esp,%ebp
	// Fill this function in
	LIST_INSERT_HEAD(&free_frame_list, ptr_frame_info);
f01024d5:	8b 15 b8 73 14 f0    	mov    0xf01473b8,%edx
f01024db:	8b 45 08             	mov    0x8(%ebp),%eax
f01024de:	89 10                	mov    %edx,(%eax)
f01024e0:	8b 45 08             	mov    0x8(%ebp),%eax
f01024e3:	8b 00                	mov    (%eax),%eax
f01024e5:	85 c0                	test   %eax,%eax
f01024e7:	74 0b                	je     f01024f4 <free_frame+0x22>
f01024e9:	a1 b8 73 14 f0       	mov    0xf01473b8,%eax
f01024ee:	8b 55 08             	mov    0x8(%ebp),%edx
f01024f1:	89 50 04             	mov    %edx,0x4(%eax)
f01024f4:	8b 45 08             	mov    0x8(%ebp),%eax
f01024f7:	a3 b8 73 14 f0       	mov    %eax,0xf01473b8
f01024fc:	8b 45 08             	mov    0x8(%ebp),%eax
f01024ff:	c7 40 04 b8 73 14 f0 	movl   $0xf01473b8,0x4(%eax)
}
f0102506:	90                   	nop
f0102507:	5d                   	pop    %ebp
f0102508:	c3                   	ret    

f0102509 <decrement_references>:
//
// Decrement the reference count on a frame
// freeing it if there are no more references.
//
void decrement_references(struct Frame_Info* ptr_frame_info)
{
f0102509:	55                   	push   %ebp
f010250a:	89 e5                	mov    %esp,%ebp
	if (--(ptr_frame_info->references) == 0)
f010250c:	8b 45 08             	mov    0x8(%ebp),%eax
f010250f:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0102513:	8d 50 ff             	lea    -0x1(%eax),%edx
f0102516:	8b 45 08             	mov    0x8(%ebp),%eax
f0102519:	66 89 50 08          	mov    %dx,0x8(%eax)
f010251d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102520:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f0102524:	66 85 c0             	test   %ax,%ax
f0102527:	75 0b                	jne    f0102534 <decrement_references+0x2b>
		free_frame(ptr_frame_info);
f0102529:	ff 75 08             	pushl  0x8(%ebp)
f010252c:	e8 a1 ff ff ff       	call   f01024d2 <free_frame>
f0102531:	83 c4 04             	add    $0x4,%esp
}
f0102534:	90                   	nop
f0102535:	c9                   	leave  
f0102536:	c3                   	ret    

f0102537 <get_page_table>:
//
// Hint: you can use "to_physical_address()" to turn a Frame_Info*
// into the physical address of the frame it refers to. 

int get_page_table(uint32 *ptr_page_directory, const void *virtual_address, int create, uint32 **ptr_page_table)
{
f0102537:	55                   	push   %ebp
f0102538:	89 e5                	mov    %esp,%ebp
f010253a:	83 ec 28             	sub    $0x28,%esp
	// Fill this function in
	uint32 page_directory_entry = ptr_page_directory[PDX(virtual_address)];
f010253d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102540:	c1 e8 16             	shr    $0x16,%eax
f0102543:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f010254a:	8b 45 08             	mov    0x8(%ebp),%eax
f010254d:	01 d0                	add    %edx,%eax
f010254f:	8b 00                	mov    (%eax),%eax
f0102551:	89 45 f4             	mov    %eax,-0xc(%ebp)

	*ptr_page_table = K_VIRTUAL_ADDRESS(EXTRACT_ADDRESS(page_directory_entry)) ;
f0102554:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102557:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f010255c:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010255f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102562:	c1 e8 0c             	shr    $0xc,%eax
f0102565:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102568:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f010256d:	39 45 ec             	cmp    %eax,-0x14(%ebp)
f0102570:	72 17                	jb     f0102589 <get_page_table+0x52>
f0102572:	ff 75 f0             	pushl  -0x10(%ebp)
f0102575:	68 98 5a 10 f0       	push   $0xf0105a98
f010257a:	68 79 01 00 00       	push   $0x179
f010257f:	68 81 5a 10 f0       	push   $0xf0105a81
f0102584:	e8 a5 db ff ff       	call   f010012e <_panic>
f0102589:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010258c:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102591:	89 c2                	mov    %eax,%edx
f0102593:	8b 45 14             	mov    0x14(%ebp),%eax
f0102596:	89 10                	mov    %edx,(%eax)
	
	if (page_directory_entry == 0)
f0102598:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f010259c:	0f 85 d3 00 00 00    	jne    f0102675 <get_page_table+0x13e>
	{
		if (create)
f01025a2:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01025a6:	0f 84 b9 00 00 00    	je     f0102665 <get_page_table+0x12e>
		{
			struct Frame_Info* ptr_frame_info;
			int err = allocate_frame(&ptr_frame_info) ;
f01025ac:	83 ec 0c             	sub    $0xc,%esp
f01025af:	8d 45 d8             	lea    -0x28(%ebp),%eax
f01025b2:	50                   	push   %eax
f01025b3:	e8 b3 fe ff ff       	call   f010246b <allocate_frame>
f01025b8:	83 c4 10             	add    $0x10,%esp
f01025bb:	89 45 e8             	mov    %eax,-0x18(%ebp)
			if(err == E_NO_MEM)
f01025be:	83 7d e8 fc          	cmpl   $0xfffffffc,-0x18(%ebp)
f01025c2:	75 13                	jne    f01025d7 <get_page_table+0xa0>
			{
				*ptr_page_table = 0;
f01025c4:	8b 45 14             	mov    0x14(%ebp),%eax
f01025c7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
				return E_NO_MEM;
f01025cd:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
f01025d2:	e9 a3 00 00 00       	jmp    f010267a <get_page_table+0x143>
			}

			uint32 phys_page_table = to_physical_address(ptr_frame_info);
f01025d7:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01025da:	83 ec 0c             	sub    $0xc,%esp
f01025dd:	50                   	push   %eax
f01025de:	e8 d1 f7 ff ff       	call   f0101db4 <to_physical_address>
f01025e3:	83 c4 10             	add    $0x10,%esp
f01025e6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
			*ptr_page_table = K_VIRTUAL_ADDRESS(phys_page_table) ;
f01025e9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f01025ec:	89 45 e0             	mov    %eax,-0x20(%ebp)
f01025ef:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01025f2:	c1 e8 0c             	shr    $0xc,%eax
f01025f5:	89 45 dc             	mov    %eax,-0x24(%ebp)
f01025f8:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f01025fd:	39 45 dc             	cmp    %eax,-0x24(%ebp)
f0102600:	72 17                	jb     f0102619 <get_page_table+0xe2>
f0102602:	ff 75 e0             	pushl  -0x20(%ebp)
f0102605:	68 98 5a 10 f0       	push   $0xf0105a98
f010260a:	68 88 01 00 00       	push   $0x188
f010260f:	68 81 5a 10 f0       	push   $0xf0105a81
f0102614:	e8 15 db ff ff       	call   f010012e <_panic>
f0102619:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010261c:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0102621:	89 c2                	mov    %eax,%edx
f0102623:	8b 45 14             	mov    0x14(%ebp),%eax
f0102626:	89 10                	mov    %edx,(%eax)
			
			//initialize new page table by 0's
			memset(*ptr_page_table , 0, PAGE_SIZE);
f0102628:	8b 45 14             	mov    0x14(%ebp),%eax
f010262b:	8b 00                	mov    (%eax),%eax
f010262d:	83 ec 04             	sub    $0x4,%esp
f0102630:	68 00 10 00 00       	push   $0x1000
f0102635:	6a 00                	push   $0x0
f0102637:	50                   	push   %eax
f0102638:	e8 41 21 00 00       	call   f010477e <memset>
f010263d:	83 c4 10             	add    $0x10,%esp

			ptr_frame_info->references = 1;
f0102640:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0102643:	66 c7 40 08 01 00    	movw   $0x1,0x8(%eax)
			ptr_page_directory[PDX(virtual_address)] = CONSTRUCT_ENTRY(phys_page_table, PERM_PRESENT | PERM_USER | PERM_WRITEABLE);
f0102649:	8b 45 0c             	mov    0xc(%ebp),%eax
f010264c:	c1 e8 16             	shr    $0x16,%eax
f010264f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0102656:	8b 45 08             	mov    0x8(%ebp),%eax
f0102659:	01 d0                	add    %edx,%eax
f010265b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f010265e:	83 ca 07             	or     $0x7,%edx
f0102661:	89 10                	mov    %edx,(%eax)
f0102663:	eb 10                	jmp    f0102675 <get_page_table+0x13e>
		}
		else
		{
			*ptr_page_table = 0;
f0102665:	8b 45 14             	mov    0x14(%ebp),%eax
f0102668:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
			return 0;
f010266e:	b8 00 00 00 00       	mov    $0x0,%eax
f0102673:	eb 05                	jmp    f010267a <get_page_table+0x143>
		}
	}	
	return 0;
f0102675:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010267a:	c9                   	leave  
f010267b:	c3                   	ret    

f010267c <map_frame>:
//   E_NO_MEM, if page table couldn't be allocated
//
// Hint: implement using get_page_table() and unmap_frame().
//
int map_frame(uint32 *ptr_page_directory, struct Frame_Info *ptr_frame_info, void *virtual_address, int perm)
{
f010267c:	55                   	push   %ebp
f010267d:	89 e5                	mov    %esp,%ebp
f010267f:	83 ec 18             	sub    $0x18,%esp
	// Fill this function in
	uint32 physical_address = to_physical_address(ptr_frame_info);
f0102682:	ff 75 0c             	pushl  0xc(%ebp)
f0102685:	e8 2a f7 ff ff       	call   f0101db4 <to_physical_address>
f010268a:	83 c4 04             	add    $0x4,%esp
f010268d:	89 45 f4             	mov    %eax,-0xc(%ebp)
	uint32 *ptr_page_table;
	if( get_page_table(ptr_page_directory, virtual_address, 1, &ptr_page_table) == 0)
f0102690:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0102693:	50                   	push   %eax
f0102694:	6a 01                	push   $0x1
f0102696:	ff 75 10             	pushl  0x10(%ebp)
f0102699:	ff 75 08             	pushl  0x8(%ebp)
f010269c:	e8 96 fe ff ff       	call   f0102537 <get_page_table>
f01026a1:	83 c4 10             	add    $0x10,%esp
f01026a4:	85 c0                	test   %eax,%eax
f01026a6:	75 74                	jne    f010271c <map_frame+0xa0>
	{
		uint32 page_table_entry = ptr_page_table[PTX(virtual_address)];
f01026a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01026ab:	8b 55 10             	mov    0x10(%ebp),%edx
f01026ae:	c1 ea 0c             	shr    $0xc,%edx
f01026b1:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f01026b7:	c1 e2 02             	shl    $0x2,%edx
f01026ba:	01 d0                	add    %edx,%eax
f01026bc:	8b 00                	mov    (%eax),%eax
f01026be:	89 45 f0             	mov    %eax,-0x10(%ebp)
		
		
		if( EXTRACT_ADDRESS(page_table_entry) != physical_address)
f01026c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01026c4:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f01026c9:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f01026cc:	74 47                	je     f0102715 <map_frame+0x99>
		{
			if( page_table_entry != 0)
f01026ce:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f01026d2:	74 11                	je     f01026e5 <map_frame+0x69>
			{				
				unmap_frame(ptr_page_directory , virtual_address);
f01026d4:	83 ec 08             	sub    $0x8,%esp
f01026d7:	ff 75 10             	pushl  0x10(%ebp)
f01026da:	ff 75 08             	pushl  0x8(%ebp)
f01026dd:	e8 b0 00 00 00       	call   f0102792 <unmap_frame>
f01026e2:	83 c4 10             	add    $0x10,%esp
			}
			ptr_frame_info->references++;
f01026e5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01026e8:	0f b7 40 08          	movzwl 0x8(%eax),%eax
f01026ec:	8d 50 01             	lea    0x1(%eax),%edx
f01026ef:	8b 45 0c             	mov    0xc(%ebp),%eax
f01026f2:	66 89 50 08          	mov    %dx,0x8(%eax)
			ptr_page_table[PTX(virtual_address)] = CONSTRUCT_ENTRY(physical_address , perm | PERM_PRESENT);
f01026f6:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01026f9:	8b 55 10             	mov    0x10(%ebp),%edx
f01026fc:	c1 ea 0c             	shr    $0xc,%edx
f01026ff:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f0102705:	c1 e2 02             	shl    $0x2,%edx
f0102708:	01 c2                	add    %eax,%edx
f010270a:	8b 45 14             	mov    0x14(%ebp),%eax
f010270d:	0b 45 f4             	or     -0xc(%ebp),%eax
f0102710:	83 c8 01             	or     $0x1,%eax
f0102713:	89 02                	mov    %eax,(%edx)
			
		}		
		return 0;
f0102715:	b8 00 00 00 00       	mov    $0x0,%eax
f010271a:	eb 05                	jmp    f0102721 <map_frame+0xa5>
	}	
	return E_NO_MEM;
f010271c:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
}
f0102721:	c9                   	leave  
f0102722:	c3                   	ret    

f0102723 <get_frame_info>:
// Return 0 if there is no frame mapped at virtual_address.
//
// Hint: implement using get_page_table() and get_frame_info().
//
struct Frame_Info * get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table)
{
f0102723:	55                   	push   %ebp
f0102724:	89 e5                	mov    %esp,%ebp
f0102726:	83 ec 18             	sub    $0x18,%esp
	// Fill this function in	
	uint32 ret =  get_page_table(ptr_page_directory, virtual_address, 0, ptr_page_table) ;
f0102729:	ff 75 10             	pushl  0x10(%ebp)
f010272c:	6a 00                	push   $0x0
f010272e:	ff 75 0c             	pushl  0xc(%ebp)
f0102731:	ff 75 08             	pushl  0x8(%ebp)
f0102734:	e8 fe fd ff ff       	call   f0102537 <get_page_table>
f0102739:	83 c4 10             	add    $0x10,%esp
f010273c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if((*ptr_page_table) != 0)
f010273f:	8b 45 10             	mov    0x10(%ebp),%eax
f0102742:	8b 00                	mov    (%eax),%eax
f0102744:	85 c0                	test   %eax,%eax
f0102746:	74 43                	je     f010278b <get_frame_info+0x68>
	{	
		uint32 index_page_table = PTX(virtual_address);
f0102748:	8b 45 0c             	mov    0xc(%ebp),%eax
f010274b:	c1 e8 0c             	shr    $0xc,%eax
f010274e:	25 ff 03 00 00       	and    $0x3ff,%eax
f0102753:	89 45 f0             	mov    %eax,-0x10(%ebp)
		uint32 page_table_entry = (*ptr_page_table)[index_page_table];
f0102756:	8b 45 10             	mov    0x10(%ebp),%eax
f0102759:	8b 00                	mov    (%eax),%eax
f010275b:	8b 55 f0             	mov    -0x10(%ebp),%edx
f010275e:	c1 e2 02             	shl    $0x2,%edx
f0102761:	01 d0                	add    %edx,%eax
f0102763:	8b 00                	mov    (%eax),%eax
f0102765:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if( page_table_entry != 0)	
f0102768:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f010276c:	74 16                	je     f0102784 <get_frame_info+0x61>
			return to_frame_info( EXTRACT_ADDRESS ( page_table_entry ) );
f010276e:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102771:	25 00 f0 ff ff       	and    $0xfffff000,%eax
f0102776:	83 ec 0c             	sub    $0xc,%esp
f0102779:	50                   	push   %eax
f010277a:	e8 48 f6 ff ff       	call   f0101dc7 <to_frame_info>
f010277f:	83 c4 10             	add    $0x10,%esp
f0102782:	eb 0c                	jmp    f0102790 <get_frame_info+0x6d>
		return 0;
f0102784:	b8 00 00 00 00       	mov    $0x0,%eax
f0102789:	eb 05                	jmp    f0102790 <get_frame_info+0x6d>
	}
	return 0;
f010278b:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0102790:	c9                   	leave  
f0102791:	c3                   	ret    

f0102792 <unmap_frame>:
//
// Hint: implement using get_frame_info(),
// 	tlb_invalidate(), and decrement_references().
//
void unmap_frame(uint32 *ptr_page_directory, void *virtual_address)
{
f0102792:	55                   	push   %ebp
f0102793:	89 e5                	mov    %esp,%ebp
f0102795:	83 ec 18             	sub    $0x18,%esp
	// Fill this function in
	uint32 *ptr_page_table;
	struct Frame_Info* ptr_frame_info = get_frame_info(ptr_page_directory, virtual_address, &ptr_page_table);
f0102798:	83 ec 04             	sub    $0x4,%esp
f010279b:	8d 45 f0             	lea    -0x10(%ebp),%eax
f010279e:	50                   	push   %eax
f010279f:	ff 75 0c             	pushl  0xc(%ebp)
f01027a2:	ff 75 08             	pushl  0x8(%ebp)
f01027a5:	e8 79 ff ff ff       	call   f0102723 <get_frame_info>
f01027aa:	83 c4 10             	add    $0x10,%esp
f01027ad:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if( ptr_frame_info != 0 )
f01027b0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01027b4:	74 39                	je     f01027ef <unmap_frame+0x5d>
	{
		decrement_references(ptr_frame_info);
f01027b6:	83 ec 0c             	sub    $0xc,%esp
f01027b9:	ff 75 f4             	pushl  -0xc(%ebp)
f01027bc:	e8 48 fd ff ff       	call   f0102509 <decrement_references>
f01027c1:	83 c4 10             	add    $0x10,%esp
		ptr_page_table[PTX(virtual_address)] = 0;
f01027c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01027c7:	8b 55 0c             	mov    0xc(%ebp),%edx
f01027ca:	c1 ea 0c             	shr    $0xc,%edx
f01027cd:	81 e2 ff 03 00 00    	and    $0x3ff,%edx
f01027d3:	c1 e2 02             	shl    $0x2,%edx
f01027d6:	01 d0                	add    %edx,%eax
f01027d8:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
		tlb_invalidate(ptr_page_directory, virtual_address);
f01027de:	83 ec 08             	sub    $0x8,%esp
f01027e1:	ff 75 0c             	pushl  0xc(%ebp)
f01027e4:	ff 75 08             	pushl  0x8(%ebp)
f01027e7:	e8 76 eb ff ff       	call   f0101362 <tlb_invalidate>
f01027ec:	83 c4 10             	add    $0x10,%esp
	}	
}
f01027ef:	90                   	nop
f01027f0:	c9                   	leave  
f01027f1:	c3                   	ret    

f01027f2 <get_page>:
//		or to allocate any necessary page tables.
// 	HINT: 	remember to free the allocated frame if there is no space 
//		for the necessary page tables

int get_page(uint32* ptr_page_directory, void *virtual_address, int perm)
{
f01027f2:	55                   	push   %ebp
f01027f3:	89 e5                	mov    %esp,%ebp
f01027f5:	83 ec 08             	sub    $0x8,%esp
	// PROJECT 2008: Your code here.
	panic("get_page function is not completed yet") ;
f01027f8:	83 ec 04             	sub    $0x4,%esp
f01027fb:	68 c8 5a 10 f0       	push   $0xf0105ac8
f0102800:	68 12 02 00 00       	push   $0x212
f0102805:	68 81 5a 10 f0       	push   $0xf0105a81
f010280a:	e8 1f d9 ff ff       	call   f010012e <_panic>

f010280f <calculate_required_frames>:
	return 0 ;
}

//[2] calculate_required_frames: 
uint32 calculate_required_frames(uint32* ptr_page_directory, uint32 start_virtual_address, uint32 size)
{
f010280f:	55                   	push   %ebp
f0102810:	89 e5                	mov    %esp,%ebp
f0102812:	83 ec 08             	sub    $0x8,%esp
	// PROJECT 2008: Your code here.
	panic("calculate_required_frames function is not completed yet") ;
f0102815:	83 ec 04             	sub    $0x4,%esp
f0102818:	68 f0 5a 10 f0       	push   $0xf0105af0
f010281d:	68 29 02 00 00       	push   $0x229
f0102822:	68 81 5a 10 f0       	push   $0xf0105a81
f0102827:	e8 02 d9 ff ff       	call   f010012e <_panic>

f010282c <calculate_free_frames>:


//[3] calculate_free_frames:

uint32 calculate_free_frames()
{
f010282c:	55                   	push   %ebp
f010282d:	89 e5                	mov    %esp,%ebp
f010282f:	83 ec 10             	sub    $0x10,%esp
	// PROJECT 2008: Your code here.
	//panic("calculate_free_frames function is not completed yet") ;
	
	//calculate the free frames from the free frame list
	struct Frame_Info *ptr;
	uint32 cnt = 0 ; 
f0102832:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
	LIST_FOREACH(ptr, &free_frame_list)
f0102839:	a1 b8 73 14 f0       	mov    0xf01473b8,%eax
f010283e:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0102841:	eb 0c                	jmp    f010284f <calculate_free_frames+0x23>
	{
		cnt++ ;
f0102843:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
	//panic("calculate_free_frames function is not completed yet") ;
	
	//calculate the free frames from the free frame list
	struct Frame_Info *ptr;
	uint32 cnt = 0 ; 
	LIST_FOREACH(ptr, &free_frame_list)
f0102847:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010284a:	8b 00                	mov    (%eax),%eax
f010284c:	89 45 fc             	mov    %eax,-0x4(%ebp)
f010284f:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f0102853:	75 ee                	jne    f0102843 <calculate_free_frames+0x17>
	{
		cnt++ ;
	}
	return cnt;
f0102855:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f0102858:	c9                   	leave  
f0102859:	c3                   	ret    

f010285a <freeMem>:
//	Steps:
//		1) Unmap all mapped pages in the range [virtual_address, virtual_address + size ]
//		2) Free all mapped page tables in this range

void freeMem(uint32* ptr_page_directory, void *virtual_address, uint32 size)
{
f010285a:	55                   	push   %ebp
f010285b:	89 e5                	mov    %esp,%ebp
f010285d:	83 ec 08             	sub    $0x8,%esp
	// PROJECT 2008: Your code here.
	panic("freeMem function is not completed yet") ;
f0102860:	83 ec 04             	sub    $0x4,%esp
f0102863:	68 28 5b 10 f0       	push   $0xf0105b28
f0102868:	68 50 02 00 00       	push   $0x250
f010286d:	68 81 5a 10 f0       	push   $0xf0105a81
f0102872:	e8 b7 d8 ff ff       	call   f010012e <_panic>

f0102877 <allocate_environment>:
//
// Returns 0 on success, < 0 on failure.  Errors include:
//	E_NO_FREE_ENV if all NENVS environments are allocated
//
int allocate_environment(struct Env** e)
{	
f0102877:	55                   	push   %ebp
f0102878:	89 e5                	mov    %esp,%ebp
	if (!(*e = LIST_FIRST(&env_free_list)))
f010287a:	8b 15 34 6b 14 f0    	mov    0xf0146b34,%edx
f0102880:	8b 45 08             	mov    0x8(%ebp),%eax
f0102883:	89 10                	mov    %edx,(%eax)
f0102885:	8b 45 08             	mov    0x8(%ebp),%eax
f0102888:	8b 00                	mov    (%eax),%eax
f010288a:	85 c0                	test   %eax,%eax
f010288c:	75 07                	jne    f0102895 <allocate_environment+0x1e>
		return E_NO_FREE_ENV;
f010288e:	b8 fb ff ff ff       	mov    $0xfffffffb,%eax
f0102893:	eb 05                	jmp    f010289a <allocate_environment+0x23>
	return 0;
f0102895:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010289a:	5d                   	pop    %ebp
f010289b:	c3                   	ret    

f010289c <free_environment>:

// Free the given environment "e", simply by adding it to the free environment list.
void free_environment(struct Env* e)
{
f010289c:	55                   	push   %ebp
f010289d:	89 e5                	mov    %esp,%ebp
	curenv = NULL;	
f010289f:	c7 05 30 6b 14 f0 00 	movl   $0x0,0xf0146b30
f01028a6:	00 00 00 
	// return the environment to the free list
	e->env_status = ENV_FREE;
f01028a9:	8b 45 08             	mov    0x8(%ebp),%eax
f01028ac:	c7 40 54 00 00 00 00 	movl   $0x0,0x54(%eax)
	LIST_INSERT_HEAD(&env_free_list, e);
f01028b3:	8b 15 34 6b 14 f0    	mov    0xf0146b34,%edx
f01028b9:	8b 45 08             	mov    0x8(%ebp),%eax
f01028bc:	89 50 44             	mov    %edx,0x44(%eax)
f01028bf:	8b 45 08             	mov    0x8(%ebp),%eax
f01028c2:	8b 40 44             	mov    0x44(%eax),%eax
f01028c5:	85 c0                	test   %eax,%eax
f01028c7:	74 0e                	je     f01028d7 <free_environment+0x3b>
f01028c9:	a1 34 6b 14 f0       	mov    0xf0146b34,%eax
f01028ce:	8b 55 08             	mov    0x8(%ebp),%edx
f01028d1:	83 c2 44             	add    $0x44,%edx
f01028d4:	89 50 48             	mov    %edx,0x48(%eax)
f01028d7:	8b 45 08             	mov    0x8(%ebp),%eax
f01028da:	a3 34 6b 14 f0       	mov    %eax,0xf0146b34
f01028df:	8b 45 08             	mov    0x8(%ebp),%eax
f01028e2:	c7 40 48 34 6b 14 f0 	movl   $0xf0146b34,0x48(%eax)
}
f01028e9:	90                   	nop
f01028ea:	5d                   	pop    %ebp
f01028eb:	c3                   	ret    

f01028ec <initialize_environment>:
// and initialize the kernel portion of the new environment's address space.
// Do NOT (yet) map anything into the user portion
// of the environment's virtual address space.
//
void initialize_environment(struct Env* e, uint32* ptr_user_page_directory)
{	
f01028ec:	55                   	push   %ebp
f01028ed:	89 e5                	mov    %esp,%ebp
f01028ef:	83 ec 08             	sub    $0x8,%esp
	// PROJECT 2008: Your code here.
	panic("initialize_environment function is not completed yet") ;
f01028f2:	83 ec 04             	sub    $0x4,%esp
f01028f5:	68 ac 5b 10 f0       	push   $0xf0105bac
f01028fa:	6a 77                	push   $0x77
f01028fc:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102901:	e8 28 d8 ff ff       	call   f010012e <_panic>

f0102906 <program_segment_alloc_map>:
//
// if the allocation failed, return E_NO_MEM 
// otherwise return 0
//
static int program_segment_alloc_map(struct Env *e, void *va, uint32 length)
{
f0102906:	55                   	push   %ebp
f0102907:	89 e5                	mov    %esp,%ebp
f0102909:	83 ec 08             	sub    $0x8,%esp
	//PROJECT 2008: Your code here.	
	panic("program_segment_alloc_map function is not completed yet") ;
f010290c:	83 ec 04             	sub    $0x4,%esp
f010290f:	68 fc 5b 10 f0       	push   $0xf0105bfc
f0102914:	68 8e 00 00 00       	push   $0x8e
f0102919:	68 e1 5b 10 f0       	push   $0xf0105be1
f010291e:	e8 0b d8 ff ff       	call   f010012e <_panic>

f0102923 <env_create>:
}

//
// Allocates a new env and loads the named user program into it.
struct UserProgramInfo* env_create(char* user_program_name)
{
f0102923:	55                   	push   %ebp
f0102924:	89 e5                	mov    %esp,%ebp
f0102926:	83 ec 28             	sub    $0x28,%esp
	// PROJECT 2008: Your code here.
	panic("env_create function is not completed yet") ;
f0102929:	83 ec 04             	sub    $0x4,%esp
f010292c:	68 34 5c 10 f0       	push   $0xf0105c34
f0102931:	68 99 00 00 00       	push   $0x99
f0102936:	68 e1 5b 10 f0       	push   $0xf0105be1
f010293b:	e8 ee d7 ff ff       	call   f010012e <_panic>

f0102940 <env_run>:
// Used to run the given environment "e", simply by 
// context switch from curenv to env e.
//  (This function does not return.)
//
void env_run(struct Env *e)
{
f0102940:	55                   	push   %ebp
f0102941:	89 e5                	mov    %esp,%ebp
f0102943:	83 ec 18             	sub    $0x18,%esp
	if(curenv != e)
f0102946:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010294b:	3b 45 08             	cmp    0x8(%ebp),%eax
f010294e:	74 27                	je     f0102977 <env_run+0x37>
	{		
		curenv = e ;
f0102950:	8b 45 08             	mov    0x8(%ebp),%eax
f0102953:	a3 30 6b 14 f0       	mov    %eax,0xf0146b30
		curenv->env_runs++ ;
f0102958:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010295d:	8b 50 58             	mov    0x58(%eax),%edx
f0102960:	83 c2 01             	add    $0x1,%edx
f0102963:	89 50 58             	mov    %edx,0x58(%eax)
		lcr3(curenv->env_cr3) ;	
f0102966:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010296b:	8b 40 60             	mov    0x60(%eax),%eax
f010296e:	89 45 f4             	mov    %eax,-0xc(%ebp)
f0102971:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102974:	0f 22 d8             	mov    %eax,%cr3
	}	
	env_pop_tf(&(curenv->env_tf));
f0102977:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010297c:	83 ec 0c             	sub    $0xc,%esp
f010297f:	50                   	push   %eax
f0102980:	e8 f9 05 00 00       	call   f0102f7e <env_pop_tf>

f0102985 <env_free>:

//
// Frees environment "e" and all memory it uses.
// 
void env_free(struct Env *e)
{
f0102985:	55                   	push   %ebp
f0102986:	89 e5                	mov    %esp,%ebp
f0102988:	83 ec 08             	sub    $0x8,%esp
	// PROJECT 2008: Your code here.
	panic("env_free function is not completed yet") ;
f010298b:	83 ec 04             	sub    $0x4,%esp
f010298e:	68 60 5c 10 f0       	push   $0xf0105c60
f0102993:	68 ef 00 00 00       	push   $0xef
f0102998:	68 e1 5b 10 f0       	push   $0xf0105be1
f010299d:	e8 8c d7 ff ff       	call   f010012e <_panic>

f01029a2 <env_init>:
// Insert in reverse order, so that the first call to allocate_environment()
// returns envs[0].
//
void
env_init(void)
{	
f01029a2:	55                   	push   %ebp
f01029a3:	89 e5                	mov    %esp,%ebp
f01029a5:	83 ec 10             	sub    $0x10,%esp
	int iEnv = NENV-1;
f01029a8:	c7 45 fc ff 03 00 00 	movl   $0x3ff,-0x4(%ebp)
	for(; iEnv >= 0; iEnv--)
f01029af:	e9 8d 00 00 00       	jmp    f0102a41 <env_init+0x9f>
	{
		envs[iEnv].env_status = ENV_FREE;
f01029b4:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f01029ba:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01029bd:	6b c0 64             	imul   $0x64,%eax,%eax
f01029c0:	01 d0                	add    %edx,%eax
f01029c2:	c7 40 54 00 00 00 00 	movl   $0x0,0x54(%eax)
		envs[iEnv].env_id = 0;
f01029c9:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f01029cf:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01029d2:	6b c0 64             	imul   $0x64,%eax,%eax
f01029d5:	01 d0                	add    %edx,%eax
f01029d7:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
		LIST_INSERT_HEAD(&env_free_list, &envs[iEnv]);	
f01029de:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f01029e4:	8b 45 fc             	mov    -0x4(%ebp),%eax
f01029e7:	6b c0 64             	imul   $0x64,%eax,%eax
f01029ea:	01 d0                	add    %edx,%eax
f01029ec:	8b 15 34 6b 14 f0    	mov    0xf0146b34,%edx
f01029f2:	89 50 44             	mov    %edx,0x44(%eax)
f01029f5:	8b 40 44             	mov    0x44(%eax),%eax
f01029f8:	85 c0                	test   %eax,%eax
f01029fa:	74 19                	je     f0102a15 <env_init+0x73>
f01029fc:	a1 34 6b 14 f0       	mov    0xf0146b34,%eax
f0102a01:	8b 0d 2c 6b 14 f0    	mov    0xf0146b2c,%ecx
f0102a07:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102a0a:	6b d2 64             	imul   $0x64,%edx,%edx
f0102a0d:	01 ca                	add    %ecx,%edx
f0102a0f:	83 c2 44             	add    $0x44,%edx
f0102a12:	89 50 48             	mov    %edx,0x48(%eax)
f0102a15:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f0102a1b:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0102a1e:	6b c0 64             	imul   $0x64,%eax,%eax
f0102a21:	01 d0                	add    %edx,%eax
f0102a23:	a3 34 6b 14 f0       	mov    %eax,0xf0146b34
f0102a28:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f0102a2e:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0102a31:	6b c0 64             	imul   $0x64,%eax,%eax
f0102a34:	01 d0                	add    %edx,%eax
f0102a36:	c7 40 48 34 6b 14 f0 	movl   $0xf0146b34,0x48(%eax)
//
void
env_init(void)
{	
	int iEnv = NENV-1;
	for(; iEnv >= 0; iEnv--)
f0102a3d:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
f0102a41:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f0102a45:	0f 89 69 ff ff ff    	jns    f01029b4 <env_init+0x12>
	{
		envs[iEnv].env_status = ENV_FREE;
		envs[iEnv].env_id = 0;
		LIST_INSERT_HEAD(&env_free_list, &envs[iEnv]);	
	}
}
f0102a4b:	90                   	nop
f0102a4c:	c9                   	leave  
f0102a4d:	c3                   	ret    

f0102a4e <complete_environment_initialization>:

void complete_environment_initialization(struct Env* e)
{	
f0102a4e:	55                   	push   %ebp
f0102a4f:	89 e5                	mov    %esp,%ebp
f0102a51:	83 ec 18             	sub    $0x18,%esp
	//VPT and UVPT map the env's own page table, with
	//different permissions.
	e->env_pgdir[PDX(VPT)]  = e->env_cr3 | PERM_PRESENT | PERM_WRITEABLE;
f0102a54:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a57:	8b 40 5c             	mov    0x5c(%eax),%eax
f0102a5a:	8d 90 fc 0e 00 00    	lea    0xefc(%eax),%edx
f0102a60:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a63:	8b 40 60             	mov    0x60(%eax),%eax
f0102a66:	83 c8 03             	or     $0x3,%eax
f0102a69:	89 02                	mov    %eax,(%edx)
	e->env_pgdir[PDX(UVPT)] = e->env_cr3 | PERM_PRESENT | PERM_USER;
f0102a6b:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a6e:	8b 40 5c             	mov    0x5c(%eax),%eax
f0102a71:	8d 90 f4 0e 00 00    	lea    0xef4(%eax),%edx
f0102a77:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a7a:	8b 40 60             	mov    0x60(%eax),%eax
f0102a7d:	83 c8 05             	or     $0x5,%eax
f0102a80:	89 02                	mov    %eax,(%edx)
	
	int32 generation;	
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1);
f0102a82:	8b 45 08             	mov    0x8(%ebp),%eax
f0102a85:	8b 40 4c             	mov    0x4c(%eax),%eax
f0102a88:	05 00 10 00 00       	add    $0x1000,%eax
f0102a8d:	25 00 fc ff ff       	and    $0xfffffc00,%eax
f0102a92:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (generation <= 0)	// Don't create a negative env_id.
f0102a95:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0102a99:	7f 07                	jg     f0102aa2 <complete_environment_initialization+0x54>
		generation = 1 << ENVGENSHIFT;
f0102a9b:	c7 45 f4 00 10 00 00 	movl   $0x1000,-0xc(%ebp)
	e->env_id = generation | (e - envs);
f0102aa2:	8b 45 08             	mov    0x8(%ebp),%eax
f0102aa5:	8b 15 2c 6b 14 f0    	mov    0xf0146b2c,%edx
f0102aab:	29 d0                	sub    %edx,%eax
f0102aad:	c1 f8 02             	sar    $0x2,%eax
f0102ab0:	69 c0 29 5c 8f c2    	imul   $0xc28f5c29,%eax,%eax
f0102ab6:	0b 45 f4             	or     -0xc(%ebp),%eax
f0102ab9:	89 c2                	mov    %eax,%edx
f0102abb:	8b 45 08             	mov    0x8(%ebp),%eax
f0102abe:	89 50 4c             	mov    %edx,0x4c(%eax)
	
	// Set the basic status variables.
	e->env_parent_id = 0;//parent_id;
f0102ac1:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ac4:	c7 40 50 00 00 00 00 	movl   $0x0,0x50(%eax)
	e->env_status = ENV_RUNNABLE;
f0102acb:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ace:	c7 40 54 01 00 00 00 	movl   $0x1,0x54(%eax)
	e->env_runs = 0;
f0102ad5:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ad8:	c7 40 58 00 00 00 00 	movl   $0x0,0x58(%eax)

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf));
f0102adf:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ae2:	83 ec 04             	sub    $0x4,%esp
f0102ae5:	6a 44                	push   $0x44
f0102ae7:	6a 00                	push   $0x0
f0102ae9:	50                   	push   %eax
f0102aea:	e8 8f 1c 00 00       	call   f010477e <memset>
f0102aef:	83 c4 10             	add    $0x10,%esp
	// GD_UD is the user data segment selector in the GDT, and 
	// GD_UT is the user text segment selector (see inc/memlayout.h).
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.
	
	e->env_tf.tf_ds = GD_UD | 3;
f0102af2:	8b 45 08             	mov    0x8(%ebp),%eax
f0102af5:	66 c7 40 24 23 00    	movw   $0x23,0x24(%eax)
	e->env_tf.tf_es = GD_UD | 3;
f0102afb:	8b 45 08             	mov    0x8(%ebp),%eax
f0102afe:	66 c7 40 20 23 00    	movw   $0x23,0x20(%eax)
	e->env_tf.tf_ss = GD_UD | 3;
f0102b04:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b07:	66 c7 40 40 23 00    	movw   $0x23,0x40(%eax)
	e->env_tf.tf_esp = (uint32*)USTACKTOP;
f0102b0d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b10:	c7 40 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%eax)
	e->env_tf.tf_cs = GD_UT | 3;
f0102b17:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b1a:	66 c7 40 34 1b 00    	movw   $0x1b,0x34(%eax)
	// You will set e->env_tf.tf_eip later.

	// commit the allocation
	LIST_REMOVE(e);	
f0102b20:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b23:	8b 40 44             	mov    0x44(%eax),%eax
f0102b26:	85 c0                	test   %eax,%eax
f0102b28:	74 0f                	je     f0102b39 <complete_environment_initialization+0xeb>
f0102b2a:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b2d:	8b 40 44             	mov    0x44(%eax),%eax
f0102b30:	8b 55 08             	mov    0x8(%ebp),%edx
f0102b33:	8b 52 48             	mov    0x48(%edx),%edx
f0102b36:	89 50 48             	mov    %edx,0x48(%eax)
f0102b39:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b3c:	8b 40 48             	mov    0x48(%eax),%eax
f0102b3f:	8b 55 08             	mov    0x8(%ebp),%edx
f0102b42:	8b 52 44             	mov    0x44(%edx),%edx
f0102b45:	89 10                	mov    %edx,(%eax)
	return ;
f0102b47:	90                   	nop
}
f0102b48:	c9                   	leave  
f0102b49:	c3                   	ret    

f0102b4a <PROGRAM_SEGMENT_NEXT>:

struct ProgramSegment* PROGRAM_SEGMENT_NEXT(struct ProgramSegment* seg, uint8* ptr_program_start)
{
f0102b4a:	55                   	push   %ebp
f0102b4b:	89 e5                	mov    %esp,%ebp
f0102b4d:	83 ec 18             	sub    $0x18,%esp
	int index = (*seg).segment_id++;
f0102b50:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b53:	8b 40 10             	mov    0x10(%eax),%eax
f0102b56:	8d 48 01             	lea    0x1(%eax),%ecx
f0102b59:	8b 55 08             	mov    0x8(%ebp),%edx
f0102b5c:	89 4a 10             	mov    %ecx,0x10(%edx)
f0102b5f:	89 45 f4             	mov    %eax,-0xc(%ebp)

	struct Proghdr *ph, *eph; 
	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102b62:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102b65:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102b68:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102b6b:	8b 00                	mov    (%eax),%eax
f0102b6d:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102b72:	74 17                	je     f0102b8b <PROGRAM_SEGMENT_NEXT+0x41>
		panic("Matafa2nash 3ala Keda"); 
f0102b74:	83 ec 04             	sub    $0x4,%esp
f0102b77:	68 87 5c 10 f0       	push   $0xf0105c87
f0102b7c:	68 48 01 00 00       	push   $0x148
f0102b81:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102b86:	e8 a3 d5 ff ff       	call   f010012e <_panic>
	ph = (struct Proghdr *) ( ((uint8 *) ptr_program_start) + pELFHDR->e_phoff);
f0102b8b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102b8e:	8b 50 1c             	mov    0x1c(%eax),%edx
f0102b91:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102b94:	01 d0                	add    %edx,%eax
f0102b96:	89 45 ec             	mov    %eax,-0x14(%ebp)
	
	while (ph[(*seg).segment_id].p_type != ELF_PROG_LOAD && ((*seg).segment_id < pELFHDR->e_phnum)) (*seg).segment_id++;	
f0102b99:	eb 0f                	jmp    f0102baa <PROGRAM_SEGMENT_NEXT+0x60>
f0102b9b:	8b 45 08             	mov    0x8(%ebp),%eax
f0102b9e:	8b 40 10             	mov    0x10(%eax),%eax
f0102ba1:	8d 50 01             	lea    0x1(%eax),%edx
f0102ba4:	8b 45 08             	mov    0x8(%ebp),%eax
f0102ba7:	89 50 10             	mov    %edx,0x10(%eax)
f0102baa:	8b 45 08             	mov    0x8(%ebp),%eax
f0102bad:	8b 40 10             	mov    0x10(%eax),%eax
f0102bb0:	c1 e0 05             	shl    $0x5,%eax
f0102bb3:	89 c2                	mov    %eax,%edx
f0102bb5:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102bb8:	01 d0                	add    %edx,%eax
f0102bba:	8b 00                	mov    (%eax),%eax
f0102bbc:	83 f8 01             	cmp    $0x1,%eax
f0102bbf:	74 14                	je     f0102bd5 <PROGRAM_SEGMENT_NEXT+0x8b>
f0102bc1:	8b 45 08             	mov    0x8(%ebp),%eax
f0102bc4:	8b 50 10             	mov    0x10(%eax),%edx
f0102bc7:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102bca:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102bce:	0f b7 c0             	movzwl %ax,%eax
f0102bd1:	39 c2                	cmp    %eax,%edx
f0102bd3:	72 c6                	jb     f0102b9b <PROGRAM_SEGMENT_NEXT+0x51>
	index = (*seg).segment_id;
f0102bd5:	8b 45 08             	mov    0x8(%ebp),%eax
f0102bd8:	8b 40 10             	mov    0x10(%eax),%eax
f0102bdb:	89 45 f4             	mov    %eax,-0xc(%ebp)

	if(index < pELFHDR->e_phnum)
f0102bde:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102be1:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102be5:	0f b7 c0             	movzwl %ax,%eax
f0102be8:	3b 45 f4             	cmp    -0xc(%ebp),%eax
f0102beb:	7e 63                	jle    f0102c50 <PROGRAM_SEGMENT_NEXT+0x106>
	{
		(*seg).ptr_start = (uint8 *) ptr_program_start + ph[index].p_offset;
f0102bed:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102bf0:	c1 e0 05             	shl    $0x5,%eax
f0102bf3:	89 c2                	mov    %eax,%edx
f0102bf5:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102bf8:	01 d0                	add    %edx,%eax
f0102bfa:	8b 50 04             	mov    0x4(%eax),%edx
f0102bfd:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c00:	01 c2                	add    %eax,%edx
f0102c02:	8b 45 08             	mov    0x8(%ebp),%eax
f0102c05:	89 10                	mov    %edx,(%eax)
		(*seg).size_in_memory =  ph[index].p_memsz;
f0102c07:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c0a:	c1 e0 05             	shl    $0x5,%eax
f0102c0d:	89 c2                	mov    %eax,%edx
f0102c0f:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102c12:	01 d0                	add    %edx,%eax
f0102c14:	8b 50 14             	mov    0x14(%eax),%edx
f0102c17:	8b 45 08             	mov    0x8(%ebp),%eax
f0102c1a:	89 50 08             	mov    %edx,0x8(%eax)
		(*seg).size_in_file = ph[index].p_filesz;
f0102c1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c20:	c1 e0 05             	shl    $0x5,%eax
f0102c23:	89 c2                	mov    %eax,%edx
f0102c25:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102c28:	01 d0                	add    %edx,%eax
f0102c2a:	8b 50 10             	mov    0x10(%eax),%edx
f0102c2d:	8b 45 08             	mov    0x8(%ebp),%eax
f0102c30:	89 50 04             	mov    %edx,0x4(%eax)
		(*seg).virtual_address = (uint8*)ph[index].p_va;
f0102c33:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c36:	c1 e0 05             	shl    $0x5,%eax
f0102c39:	89 c2                	mov    %eax,%edx
f0102c3b:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102c3e:	01 d0                	add    %edx,%eax
f0102c40:	8b 40 08             	mov    0x8(%eax),%eax
f0102c43:	89 c2                	mov    %eax,%edx
f0102c45:	8b 45 08             	mov    0x8(%ebp),%eax
f0102c48:	89 50 0c             	mov    %edx,0xc(%eax)
		return seg;
f0102c4b:	8b 45 08             	mov    0x8(%ebp),%eax
f0102c4e:	eb 05                	jmp    f0102c55 <PROGRAM_SEGMENT_NEXT+0x10b>
	}
	return 0;
f0102c50:	b8 00 00 00 00       	mov    $0x0,%eax
}	
f0102c55:	c9                   	leave  
f0102c56:	c3                   	ret    

f0102c57 <PROGRAM_SEGMENT_FIRST>:

struct ProgramSegment PROGRAM_SEGMENT_FIRST( uint8* ptr_program_start)
{
f0102c57:	55                   	push   %ebp
f0102c58:	89 e5                	mov    %esp,%ebp
f0102c5a:	83 ec 28             	sub    $0x28,%esp
	struct ProgramSegment seg;
	seg.segment_id = 0;
f0102c5d:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	struct Proghdr *ph, *eph; 
	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102c64:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c67:	89 45 f4             	mov    %eax,-0xc(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102c6a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c6d:	8b 00                	mov    (%eax),%eax
f0102c6f:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102c74:	74 17                	je     f0102c8d <PROGRAM_SEGMENT_FIRST+0x36>
		panic("Matafa2nash 3ala Keda"); 
f0102c76:	83 ec 04             	sub    $0x4,%esp
f0102c79:	68 87 5c 10 f0       	push   $0xf0105c87
f0102c7e:	68 61 01 00 00       	push   $0x161
f0102c83:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102c88:	e8 a1 d4 ff ff       	call   f010012e <_panic>
	ph = (struct Proghdr *) ( ((uint8 *) ptr_program_start) + pELFHDR->e_phoff);
f0102c8d:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102c90:	8b 50 1c             	mov    0x1c(%eax),%edx
f0102c93:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102c96:	01 d0                	add    %edx,%eax
f0102c98:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (ph[(seg).segment_id].p_type != ELF_PROG_LOAD && ((seg).segment_id < pELFHDR->e_phnum)) (seg).segment_id++;
f0102c9b:	eb 09                	jmp    f0102ca6 <PROGRAM_SEGMENT_FIRST+0x4f>
f0102c9d:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102ca0:	83 c0 01             	add    $0x1,%eax
f0102ca3:	89 45 e8             	mov    %eax,-0x18(%ebp)
f0102ca6:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102ca9:	c1 e0 05             	shl    $0x5,%eax
f0102cac:	89 c2                	mov    %eax,%edx
f0102cae:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102cb1:	01 d0                	add    %edx,%eax
f0102cb3:	8b 00                	mov    (%eax),%eax
f0102cb5:	83 f8 01             	cmp    $0x1,%eax
f0102cb8:	74 11                	je     f0102ccb <PROGRAM_SEGMENT_FIRST+0x74>
f0102cba:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102cbd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102cc0:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102cc4:	0f b7 c0             	movzwl %ax,%eax
f0102cc7:	39 c2                	cmp    %eax,%edx
f0102cc9:	72 d2                	jb     f0102c9d <PROGRAM_SEGMENT_FIRST+0x46>
	int index = (seg).segment_id;
f0102ccb:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0102cce:	89 45 ec             	mov    %eax,-0x14(%ebp)

	if(index < pELFHDR->e_phnum)
f0102cd1:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102cd4:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
f0102cd8:	0f b7 c0             	movzwl %ax,%eax
f0102cdb:	3b 45 ec             	cmp    -0x14(%ebp),%eax
f0102cde:	7e 73                	jle    f0102d53 <PROGRAM_SEGMENT_FIRST+0xfc>
	{	
		(seg).ptr_start = (uint8 *) ptr_program_start + ph[index].p_offset;
f0102ce0:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102ce3:	c1 e0 05             	shl    $0x5,%eax
f0102ce6:	89 c2                	mov    %eax,%edx
f0102ce8:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102ceb:	01 d0                	add    %edx,%eax
f0102ced:	8b 50 04             	mov    0x4(%eax),%edx
f0102cf0:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102cf3:	01 d0                	add    %edx,%eax
f0102cf5:	89 45 d8             	mov    %eax,-0x28(%ebp)
		(seg).size_in_memory =  ph[index].p_memsz;
f0102cf8:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102cfb:	c1 e0 05             	shl    $0x5,%eax
f0102cfe:	89 c2                	mov    %eax,%edx
f0102d00:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102d03:	01 d0                	add    %edx,%eax
f0102d05:	8b 40 14             	mov    0x14(%eax),%eax
f0102d08:	89 45 e0             	mov    %eax,-0x20(%ebp)
		(seg).size_in_file = ph[index].p_filesz;
f0102d0b:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102d0e:	c1 e0 05             	shl    $0x5,%eax
f0102d11:	89 c2                	mov    %eax,%edx
f0102d13:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102d16:	01 d0                	add    %edx,%eax
f0102d18:	8b 40 10             	mov    0x10(%eax),%eax
f0102d1b:	89 45 dc             	mov    %eax,-0x24(%ebp)
		(seg).virtual_address = (uint8*)ph[index].p_va;
f0102d1e:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102d21:	c1 e0 05             	shl    $0x5,%eax
f0102d24:	89 c2                	mov    %eax,%edx
f0102d26:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102d29:	01 d0                	add    %edx,%eax
f0102d2b:	8b 40 08             	mov    0x8(%eax),%eax
f0102d2e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		return seg;
f0102d31:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d34:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0102d37:	89 10                	mov    %edx,(%eax)
f0102d39:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0102d3c:	89 50 04             	mov    %edx,0x4(%eax)
f0102d3f:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0102d42:	89 50 08             	mov    %edx,0x8(%eax)
f0102d45:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0102d48:	89 50 0c             	mov    %edx,0xc(%eax)
f0102d4b:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102d4e:	89 50 10             	mov    %edx,0x10(%eax)
f0102d51:	eb 27                	jmp    f0102d7a <PROGRAM_SEGMENT_FIRST+0x123>
	}
	seg.segment_id = -1;
f0102d53:	c7 45 e8 ff ff ff ff 	movl   $0xffffffff,-0x18(%ebp)
	return seg;
f0102d5a:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d5d:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0102d60:	89 10                	mov    %edx,(%eax)
f0102d62:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0102d65:	89 50 04             	mov    %edx,0x4(%eax)
f0102d68:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0102d6b:	89 50 08             	mov    %edx,0x8(%eax)
f0102d6e:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0102d71:	89 50 0c             	mov    %edx,0xc(%eax)
f0102d74:	8b 55 e8             	mov    -0x18(%ebp),%edx
f0102d77:	89 50 10             	mov    %edx,0x10(%eax)
}
f0102d7a:	8b 45 08             	mov    0x8(%ebp),%eax
f0102d7d:	c9                   	leave  
f0102d7e:	c2 04 00             	ret    $0x4

f0102d81 <get_user_program_info>:

struct UserProgramInfo* get_user_program_info(char* user_program_name)
{
f0102d81:	55                   	push   %ebp
f0102d82:	89 e5                	mov    %esp,%ebp
f0102d84:	83 ec 18             	sub    $0x18,%esp
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102d87:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0102d8e:	eb 24                	jmp    f0102db4 <get_user_program_info+0x33>
		if (strcmp(user_program_name, userPrograms[i].name) == 0)
f0102d90:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102d93:	c1 e0 04             	shl    $0x4,%eax
f0102d96:	05 00 b6 11 f0       	add    $0xf011b600,%eax
f0102d9b:	8b 00                	mov    (%eax),%eax
f0102d9d:	83 ec 08             	sub    $0x8,%esp
f0102da0:	50                   	push   %eax
f0102da1:	ff 75 08             	pushl  0x8(%ebp)
f0102da4:	e8 de 18 00 00       	call   f0104687 <strcmp>
f0102da9:	83 c4 10             	add    $0x10,%esp
f0102dac:	85 c0                	test   %eax,%eax
f0102dae:	74 10                	je     f0102dc0 <get_user_program_info+0x3f>
}

struct UserProgramInfo* get_user_program_info(char* user_program_name)
{
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102db0:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0102db4:	a1 54 b6 11 f0       	mov    0xf011b654,%eax
f0102db9:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102dbc:	7c d2                	jl     f0102d90 <get_user_program_info+0xf>
f0102dbe:	eb 01                	jmp    f0102dc1 <get_user_program_info+0x40>
		if (strcmp(user_program_name, userPrograms[i].name) == 0)
			break;
f0102dc0:	90                   	nop
	}
	if(i==NUM_USER_PROGS) 
f0102dc1:	a1 54 b6 11 f0       	mov    0xf011b654,%eax
f0102dc6:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102dc9:	75 1a                	jne    f0102de5 <get_user_program_info+0x64>
	{
		cprintf("Unknown user program '%s'\n", user_program_name);
f0102dcb:	83 ec 08             	sub    $0x8,%esp
f0102dce:	ff 75 08             	pushl  0x8(%ebp)
f0102dd1:	68 9d 5c 10 f0       	push   $0xf0105c9d
f0102dd6:	e8 83 02 00 00       	call   f010305e <cprintf>
f0102ddb:	83 c4 10             	add    $0x10,%esp
		return 0;
f0102dde:	b8 00 00 00 00       	mov    $0x0,%eax
f0102de3:	eb 0b                	jmp    f0102df0 <get_user_program_info+0x6f>
	}

	return &userPrograms[i];
f0102de5:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102de8:	c1 e0 04             	shl    $0x4,%eax
f0102deb:	05 00 b6 11 f0       	add    $0xf011b600,%eax
}
f0102df0:	c9                   	leave  
f0102df1:	c3                   	ret    

f0102df2 <get_user_program_info_by_env>:

struct UserProgramInfo* get_user_program_info_by_env(struct Env* e)
{
f0102df2:	55                   	push   %ebp
f0102df3:	89 e5                	mov    %esp,%ebp
f0102df5:	83 ec 18             	sub    $0x18,%esp
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102df8:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
f0102dff:	eb 16                	jmp    f0102e17 <get_user_program_info_by_env+0x25>
		if (e== userPrograms[i].environment)
f0102e01:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102e04:	c1 e0 04             	shl    $0x4,%eax
f0102e07:	05 0c b6 11 f0       	add    $0xf011b60c,%eax
f0102e0c:	8b 00                	mov    (%eax),%eax
f0102e0e:	3b 45 08             	cmp    0x8(%ebp),%eax
f0102e11:	74 10                	je     f0102e23 <get_user_program_info_by_env+0x31>
}

struct UserProgramInfo* get_user_program_info_by_env(struct Env* e)
{
	int i;
	for (i = 0; i < NUM_USER_PROGS; i++) {
f0102e13:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
f0102e17:	a1 54 b6 11 f0       	mov    0xf011b654,%eax
f0102e1c:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102e1f:	7c e0                	jl     f0102e01 <get_user_program_info_by_env+0xf>
f0102e21:	eb 01                	jmp    f0102e24 <get_user_program_info_by_env+0x32>
		if (e== userPrograms[i].environment)
			break;
f0102e23:	90                   	nop
	}
	if(i==NUM_USER_PROGS) 
f0102e24:	a1 54 b6 11 f0       	mov    0xf011b654,%eax
f0102e29:	39 45 f4             	cmp    %eax,-0xc(%ebp)
f0102e2c:	75 17                	jne    f0102e45 <get_user_program_info_by_env+0x53>
	{
		cprintf("Unknown user program \n");
f0102e2e:	83 ec 0c             	sub    $0xc,%esp
f0102e31:	68 b8 5c 10 f0       	push   $0xf0105cb8
f0102e36:	e8 23 02 00 00       	call   f010305e <cprintf>
f0102e3b:	83 c4 10             	add    $0x10,%esp
		return 0;
f0102e3e:	b8 00 00 00 00       	mov    $0x0,%eax
f0102e43:	eb 0b                	jmp    f0102e50 <get_user_program_info_by_env+0x5e>
	}

	return &userPrograms[i];
f0102e45:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102e48:	c1 e0 04             	shl    $0x4,%eax
f0102e4b:	05 00 b6 11 f0       	add    $0xf011b600,%eax
}
f0102e50:	c9                   	leave  
f0102e51:	c3                   	ret    

f0102e52 <set_environment_entry_point>:

void set_environment_entry_point(struct UserProgramInfo* ptr_user_program)
{
f0102e52:	55                   	push   %ebp
f0102e53:	89 e5                	mov    %esp,%ebp
f0102e55:	83 ec 18             	sub    $0x18,%esp
	uint8* ptr_program_start=ptr_user_program->ptr_start;
f0102e58:	8b 45 08             	mov    0x8(%ebp),%eax
f0102e5b:	8b 40 08             	mov    0x8(%eax),%eax
f0102e5e:	89 45 f4             	mov    %eax,-0xc(%ebp)
	struct Env* e = ptr_user_program->environment;
f0102e61:	8b 45 08             	mov    0x8(%ebp),%eax
f0102e64:	8b 40 0c             	mov    0xc(%eax),%eax
f0102e67:	89 45 f0             	mov    %eax,-0x10(%ebp)

	struct Elf * pELFHDR = (struct Elf *)ptr_program_start ; 
f0102e6a:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0102e6d:	89 45 ec             	mov    %eax,-0x14(%ebp)
	if (pELFHDR->e_magic != ELF_MAGIC) 
f0102e70:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102e73:	8b 00                	mov    (%eax),%eax
f0102e75:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
f0102e7a:	74 17                	je     f0102e93 <set_environment_entry_point+0x41>
		panic("Matafa2nash 3ala Keda"); 
f0102e7c:	83 ec 04             	sub    $0x4,%esp
f0102e7f:	68 87 5c 10 f0       	push   $0xf0105c87
f0102e84:	68 99 01 00 00       	push   $0x199
f0102e89:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102e8e:	e8 9b d2 ff ff       	call   f010012e <_panic>
	e->env_tf.tf_eip = (uint32*)pELFHDR->e_entry ;
f0102e93:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102e96:	8b 40 18             	mov    0x18(%eax),%eax
f0102e99:	89 c2                	mov    %eax,%edx
f0102e9b:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102e9e:	89 50 30             	mov    %edx,0x30(%eax)
}
f0102ea1:	90                   	nop
f0102ea2:	c9                   	leave  
f0102ea3:	c3                   	ret    

f0102ea4 <env_destroy>:
// If e was the current env, then runs a new environment (and does not return
// to the caller).
//
void
env_destroy(struct Env *e) 
{
f0102ea4:	55                   	push   %ebp
f0102ea5:	89 e5                	mov    %esp,%ebp
f0102ea7:	83 ec 08             	sub    $0x8,%esp
	env_free(e);
f0102eaa:	83 ec 0c             	sub    $0xc,%esp
f0102ead:	ff 75 08             	pushl  0x8(%ebp)
f0102eb0:	e8 d0 fa ff ff       	call   f0102985 <env_free>
f0102eb5:	83 c4 10             	add    $0x10,%esp

	//cprintf("Destroyed the only environment - nothing more to do!\n");
	while (1)
		run_command_prompt();
f0102eb8:	e8 d9 da ff ff       	call   f0100996 <run_command_prompt>
f0102ebd:	eb f9                	jmp    f0102eb8 <env_destroy+0x14>

f0102ebf <env_run_cmd_prmpt>:
}

void env_run_cmd_prmpt()
{
f0102ebf:	55                   	push   %ebp
f0102ec0:	89 e5                	mov    %esp,%ebp
f0102ec2:	83 ec 18             	sub    $0x18,%esp
	struct UserProgramInfo* upi= get_user_program_info_by_env(curenv);	
f0102ec5:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102eca:	83 ec 0c             	sub    $0xc,%esp
f0102ecd:	50                   	push   %eax
f0102ece:	e8 1f ff ff ff       	call   f0102df2 <get_user_program_info_by_env>
f0102ed3:	83 c4 10             	add    $0x10,%esp
f0102ed6:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&curenv->env_tf, 0, sizeof(curenv->env_tf));
f0102ed9:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102ede:	83 ec 04             	sub    $0x4,%esp
f0102ee1:	6a 44                	push   $0x44
f0102ee3:	6a 00                	push   $0x0
f0102ee5:	50                   	push   %eax
f0102ee6:	e8 93 18 00 00       	call   f010477e <memset>
f0102eeb:	83 c4 10             	add    $0x10,%esp
	// GD_UD is the user data segment selector in the GDT, and 
	// GD_UT is the user text segment selector (see inc/memlayout.h).
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.
	
	curenv->env_tf.tf_ds = GD_UD | 3;
f0102eee:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102ef3:	66 c7 40 24 23 00    	movw   $0x23,0x24(%eax)
	curenv->env_tf.tf_es = GD_UD | 3;
f0102ef9:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102efe:	66 c7 40 20 23 00    	movw   $0x23,0x20(%eax)
	curenv->env_tf.tf_ss = GD_UD | 3;
f0102f04:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102f09:	66 c7 40 40 23 00    	movw   $0x23,0x40(%eax)
	curenv->env_tf.tf_esp = (uint32*)USTACKTOP;
f0102f0f:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102f14:	c7 40 3c 00 e0 bf ee 	movl   $0xeebfe000,0x3c(%eax)
	curenv->env_tf.tf_cs = GD_UT | 3;
f0102f1b:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0102f20:	66 c7 40 34 1b 00    	movw   $0x1b,0x34(%eax)
	set_environment_entry_point(upi);
f0102f26:	83 ec 0c             	sub    $0xc,%esp
f0102f29:	ff 75 f4             	pushl  -0xc(%ebp)
f0102f2c:	e8 21 ff ff ff       	call   f0102e52 <set_environment_entry_point>
f0102f31:	83 c4 10             	add    $0x10,%esp
	
	lcr3(K_PHYSICAL_ADDRESS(ptr_page_directory));
f0102f34:	a1 c4 73 14 f0       	mov    0xf01473c4,%eax
f0102f39:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0102f3c:	81 7d f0 ff ff ff ef 	cmpl   $0xefffffff,-0x10(%ebp)
f0102f43:	77 17                	ja     f0102f5c <env_run_cmd_prmpt+0x9d>
f0102f45:	ff 75 f0             	pushl  -0x10(%ebp)
f0102f48:	68 d0 5c 10 f0       	push   $0xf0105cd0
f0102f4d:	68 c4 01 00 00       	push   $0x1c4
f0102f52:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102f57:	e8 d2 d1 ff ff       	call   f010012e <_panic>
f0102f5c:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0102f5f:	05 00 00 00 10       	add    $0x10000000,%eax
f0102f64:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0102f67:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0102f6a:	0f 22 d8             	mov    %eax,%cr3
	
	curenv = NULL;
f0102f6d:	c7 05 30 6b 14 f0 00 	movl   $0x0,0xf0146b30
f0102f74:	00 00 00 
	
	while (1)
		run_command_prompt();
f0102f77:	e8 1a da ff ff       	call   f0100996 <run_command_prompt>
f0102f7c:	eb f9                	jmp    f0102f77 <env_run_cmd_prmpt+0xb8>

f0102f7e <env_pop_tf>:
// This exits the kernel and starts executing some environment's code.
// This function does not return.
//
void
env_pop_tf(struct Trapframe *tf)
{
f0102f7e:	55                   	push   %ebp
f0102f7f:	89 e5                	mov    %esp,%ebp
f0102f81:	83 ec 08             	sub    $0x8,%esp
	__asm __volatile("movl %0,%%esp\n"
f0102f84:	8b 65 08             	mov    0x8(%ebp),%esp
f0102f87:	61                   	popa   
f0102f88:	07                   	pop    %es
f0102f89:	1f                   	pop    %ds
f0102f8a:	83 c4 08             	add    $0x8,%esp
f0102f8d:	cf                   	iret   
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
f0102f8e:	83 ec 04             	sub    $0x4,%esp
f0102f91:	68 01 5d 10 f0       	push   $0xf0105d01
f0102f96:	68 db 01 00 00       	push   $0x1db
f0102f9b:	68 e1 5b 10 f0       	push   $0xf0105be1
f0102fa0:	e8 89 d1 ff ff       	call   f010012e <_panic>

f0102fa5 <mc146818_read>:
#include <kern/kclock.h>


unsigned
mc146818_read(unsigned reg)
{
f0102fa5:	55                   	push   %ebp
f0102fa6:	89 e5                	mov    %esp,%ebp
f0102fa8:	83 ec 10             	sub    $0x10,%esp
	outb(IO_RTC, reg);
f0102fab:	8b 45 08             	mov    0x8(%ebp),%eax
f0102fae:	0f b6 c0             	movzbl %al,%eax
f0102fb1:	c7 45 fc 70 00 00 00 	movl   $0x70,-0x4(%ebp)
f0102fb8:	88 45 f6             	mov    %al,-0xa(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0102fbb:	0f b6 45 f6          	movzbl -0xa(%ebp),%eax
f0102fbf:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102fc2:	ee                   	out    %al,(%dx)
f0102fc3:	c7 45 f8 71 00 00 00 	movl   $0x71,-0x8(%ebp)

static __inline uint8
inb(int port)
{
	uint8 data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0102fca:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0102fcd:	89 c2                	mov    %eax,%edx
f0102fcf:	ec                   	in     (%dx),%al
f0102fd0:	88 45 f7             	mov    %al,-0x9(%ebp)
	return data;
f0102fd3:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
	return inb(IO_RTC+1);
f0102fd7:	0f b6 c0             	movzbl %al,%eax
}
f0102fda:	c9                   	leave  
f0102fdb:	c3                   	ret    

f0102fdc <mc146818_write>:

void
mc146818_write(unsigned reg, unsigned datum)
{
f0102fdc:	55                   	push   %ebp
f0102fdd:	89 e5                	mov    %esp,%ebp
f0102fdf:	83 ec 10             	sub    $0x10,%esp
	outb(IO_RTC, reg);
f0102fe2:	8b 45 08             	mov    0x8(%ebp),%eax
f0102fe5:	0f b6 c0             	movzbl %al,%eax
f0102fe8:	c7 45 fc 70 00 00 00 	movl   $0x70,-0x4(%ebp)
f0102fef:	88 45 f6             	mov    %al,-0xa(%ebp)
}

static __inline void
outb(int port, uint8 data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0102ff2:	0f b6 45 f6          	movzbl -0xa(%ebp),%eax
f0102ff6:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0102ff9:	ee                   	out    %al,(%dx)
	outb(IO_RTC+1, datum);
f0102ffa:	8b 45 0c             	mov    0xc(%ebp),%eax
f0102ffd:	0f b6 c0             	movzbl %al,%eax
f0103000:	c7 45 f8 71 00 00 00 	movl   $0x71,-0x8(%ebp)
f0103007:	88 45 f7             	mov    %al,-0x9(%ebp)
f010300a:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
f010300e:	8b 55 f8             	mov    -0x8(%ebp),%edx
f0103011:	ee                   	out    %al,(%dx)
}
f0103012:	90                   	nop
f0103013:	c9                   	leave  
f0103014:	c3                   	ret    

f0103015 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0103015:	55                   	push   %ebp
f0103016:	89 e5                	mov    %esp,%ebp
f0103018:	83 ec 08             	sub    $0x8,%esp
	cputchar(ch);
f010301b:	83 ec 0c             	sub    $0xc,%esp
f010301e:	ff 75 08             	pushl  0x8(%ebp)
f0103021:	e8 36 d9 ff ff       	call   f010095c <cputchar>
f0103026:	83 c4 10             	add    $0x10,%esp
	*cnt++;
f0103029:	8b 45 0c             	mov    0xc(%ebp),%eax
f010302c:	83 c0 04             	add    $0x4,%eax
f010302f:	89 45 0c             	mov    %eax,0xc(%ebp)
}
f0103032:	90                   	nop
f0103033:	c9                   	leave  
f0103034:	c3                   	ret    

f0103035 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f0103035:	55                   	push   %ebp
f0103036:	89 e5                	mov    %esp,%ebp
f0103038:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f010303b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0103042:	ff 75 0c             	pushl  0xc(%ebp)
f0103045:	ff 75 08             	pushl  0x8(%ebp)
f0103048:	8d 45 f4             	lea    -0xc(%ebp),%eax
f010304b:	50                   	push   %eax
f010304c:	68 15 30 10 f0       	push   $0xf0103015
f0103051:	e8 5f 0f 00 00       	call   f0103fb5 <vprintfmt>
f0103056:	83 c4 10             	add    $0x10,%esp
	return cnt;
f0103059:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f010305c:	c9                   	leave  
f010305d:	c3                   	ret    

f010305e <cprintf>:

int
cprintf(const char *fmt, ...)
{
f010305e:	55                   	push   %ebp
f010305f:	89 e5                	mov    %esp,%ebp
f0103061:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0103064:	8d 45 0c             	lea    0xc(%ebp),%eax
f0103067:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
f010306a:	8b 45 08             	mov    0x8(%ebp),%eax
f010306d:	83 ec 08             	sub    $0x8,%esp
f0103070:	ff 75 f4             	pushl  -0xc(%ebp)
f0103073:	50                   	push   %eax
f0103074:	e8 bc ff ff ff       	call   f0103035 <vcprintf>
f0103079:	83 c4 10             	add    $0x10,%esp
f010307c:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
f010307f:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
f0103082:	c9                   	leave  
f0103083:	c3                   	ret    

f0103084 <trapname>:
};
extern  void (*PAGE_FAULT)();
extern  void (*SYSCALL_HANDLER)();

static const char *trapname(int trapno)
{
f0103084:	55                   	push   %ebp
f0103085:	89 e5                	mov    %esp,%ebp
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < sizeof(excnames)/sizeof(excnames[0]))
f0103087:	8b 45 08             	mov    0x8(%ebp),%eax
f010308a:	83 f8 13             	cmp    $0x13,%eax
f010308d:	77 0c                	ja     f010309b <trapname+0x17>
		return excnames[trapno];
f010308f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103092:	8b 04 85 40 60 10 f0 	mov    -0xfef9fc0(,%eax,4),%eax
f0103099:	eb 12                	jmp    f01030ad <trapname+0x29>
	if (trapno == T_SYSCALL)
f010309b:	83 7d 08 30          	cmpl   $0x30,0x8(%ebp)
f010309f:	75 07                	jne    f01030a8 <trapname+0x24>
		return "System call";
f01030a1:	b8 20 5d 10 f0       	mov    $0xf0105d20,%eax
f01030a6:	eb 05                	jmp    f01030ad <trapname+0x29>
	return "(unknown trap)";
f01030a8:	b8 2c 5d 10 f0       	mov    $0xf0105d2c,%eax
}
f01030ad:	5d                   	pop    %ebp
f01030ae:	c3                   	ret    

f01030af <idt_init>:


void
idt_init(void)
{
f01030af:	55                   	push   %ebp
f01030b0:	89 e5                	mov    %esp,%ebp
f01030b2:	83 ec 10             	sub    $0x10,%esp
	extern struct Segdesc gdt[];
	
	// LAB 3: Your code here.
	//initialize idt
	SETGATE(idt[T_PGFLT], 0, GD_KT , &PAGE_FAULT, 0) ;
f01030b5:	b8 40 36 10 f0       	mov    $0xf0103640,%eax
f01030ba:	66 a3 b0 6b 14 f0    	mov    %ax,0xf0146bb0
f01030c0:	66 c7 05 b2 6b 14 f0 	movw   $0x8,0xf0146bb2
f01030c7:	08 00 
f01030c9:	0f b6 05 b4 6b 14 f0 	movzbl 0xf0146bb4,%eax
f01030d0:	83 e0 e0             	and    $0xffffffe0,%eax
f01030d3:	a2 b4 6b 14 f0       	mov    %al,0xf0146bb4
f01030d8:	0f b6 05 b4 6b 14 f0 	movzbl 0xf0146bb4,%eax
f01030df:	83 e0 1f             	and    $0x1f,%eax
f01030e2:	a2 b4 6b 14 f0       	mov    %al,0xf0146bb4
f01030e7:	0f b6 05 b5 6b 14 f0 	movzbl 0xf0146bb5,%eax
f01030ee:	83 e0 f0             	and    $0xfffffff0,%eax
f01030f1:	83 c8 0e             	or     $0xe,%eax
f01030f4:	a2 b5 6b 14 f0       	mov    %al,0xf0146bb5
f01030f9:	0f b6 05 b5 6b 14 f0 	movzbl 0xf0146bb5,%eax
f0103100:	83 e0 ef             	and    $0xffffffef,%eax
f0103103:	a2 b5 6b 14 f0       	mov    %al,0xf0146bb5
f0103108:	0f b6 05 b5 6b 14 f0 	movzbl 0xf0146bb5,%eax
f010310f:	83 e0 9f             	and    $0xffffff9f,%eax
f0103112:	a2 b5 6b 14 f0       	mov    %al,0xf0146bb5
f0103117:	0f b6 05 b5 6b 14 f0 	movzbl 0xf0146bb5,%eax
f010311e:	83 c8 80             	or     $0xffffff80,%eax
f0103121:	a2 b5 6b 14 f0       	mov    %al,0xf0146bb5
f0103126:	b8 40 36 10 f0       	mov    $0xf0103640,%eax
f010312b:	c1 e8 10             	shr    $0x10,%eax
f010312e:	66 a3 b6 6b 14 f0    	mov    %ax,0xf0146bb6
	SETGATE(idt[T_SYSCALL], 0, GD_KT , &SYSCALL_HANDLER, 3) ;
f0103134:	b8 44 36 10 f0       	mov    $0xf0103644,%eax
f0103139:	66 a3 c0 6c 14 f0    	mov    %ax,0xf0146cc0
f010313f:	66 c7 05 c2 6c 14 f0 	movw   $0x8,0xf0146cc2
f0103146:	08 00 
f0103148:	0f b6 05 c4 6c 14 f0 	movzbl 0xf0146cc4,%eax
f010314f:	83 e0 e0             	and    $0xffffffe0,%eax
f0103152:	a2 c4 6c 14 f0       	mov    %al,0xf0146cc4
f0103157:	0f b6 05 c4 6c 14 f0 	movzbl 0xf0146cc4,%eax
f010315e:	83 e0 1f             	and    $0x1f,%eax
f0103161:	a2 c4 6c 14 f0       	mov    %al,0xf0146cc4
f0103166:	0f b6 05 c5 6c 14 f0 	movzbl 0xf0146cc5,%eax
f010316d:	83 e0 f0             	and    $0xfffffff0,%eax
f0103170:	83 c8 0e             	or     $0xe,%eax
f0103173:	a2 c5 6c 14 f0       	mov    %al,0xf0146cc5
f0103178:	0f b6 05 c5 6c 14 f0 	movzbl 0xf0146cc5,%eax
f010317f:	83 e0 ef             	and    $0xffffffef,%eax
f0103182:	a2 c5 6c 14 f0       	mov    %al,0xf0146cc5
f0103187:	0f b6 05 c5 6c 14 f0 	movzbl 0xf0146cc5,%eax
f010318e:	83 c8 60             	or     $0x60,%eax
f0103191:	a2 c5 6c 14 f0       	mov    %al,0xf0146cc5
f0103196:	0f b6 05 c5 6c 14 f0 	movzbl 0xf0146cc5,%eax
f010319d:	83 c8 80             	or     $0xffffff80,%eax
f01031a0:	a2 c5 6c 14 f0       	mov    %al,0xf0146cc5
f01031a5:	b8 44 36 10 f0       	mov    $0xf0103644,%eax
f01031aa:	c1 e8 10             	shr    $0x10,%eax
f01031ad:	66 a3 c6 6c 14 f0    	mov    %ax,0xf0146cc6

	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	ts.ts_esp0 = KERNEL_STACK_TOP;
f01031b3:	c7 05 44 73 14 f0 00 	movl   $0xefc00000,0xf0147344
f01031ba:	00 c0 ef 
	ts.ts_ss0 = GD_KD;
f01031bd:	66 c7 05 48 73 14 f0 	movw   $0x10,0xf0147348
f01031c4:	10 00 

	// Initialize the TSS field of the gdt.
	gdt[GD_TSS >> 3] = SEG16(STS_T32A, (uint32) (&ts),
f01031c6:	66 c7 05 e8 b5 11 f0 	movw   $0x68,0xf011b5e8
f01031cd:	68 00 
f01031cf:	b8 40 73 14 f0       	mov    $0xf0147340,%eax
f01031d4:	66 a3 ea b5 11 f0    	mov    %ax,0xf011b5ea
f01031da:	b8 40 73 14 f0       	mov    $0xf0147340,%eax
f01031df:	c1 e8 10             	shr    $0x10,%eax
f01031e2:	a2 ec b5 11 f0       	mov    %al,0xf011b5ec
f01031e7:	0f b6 05 ed b5 11 f0 	movzbl 0xf011b5ed,%eax
f01031ee:	83 e0 f0             	and    $0xfffffff0,%eax
f01031f1:	83 c8 09             	or     $0x9,%eax
f01031f4:	a2 ed b5 11 f0       	mov    %al,0xf011b5ed
f01031f9:	0f b6 05 ed b5 11 f0 	movzbl 0xf011b5ed,%eax
f0103200:	83 c8 10             	or     $0x10,%eax
f0103203:	a2 ed b5 11 f0       	mov    %al,0xf011b5ed
f0103208:	0f b6 05 ed b5 11 f0 	movzbl 0xf011b5ed,%eax
f010320f:	83 e0 9f             	and    $0xffffff9f,%eax
f0103212:	a2 ed b5 11 f0       	mov    %al,0xf011b5ed
f0103217:	0f b6 05 ed b5 11 f0 	movzbl 0xf011b5ed,%eax
f010321e:	83 c8 80             	or     $0xffffff80,%eax
f0103221:	a2 ed b5 11 f0       	mov    %al,0xf011b5ed
f0103226:	0f b6 05 ee b5 11 f0 	movzbl 0xf011b5ee,%eax
f010322d:	83 e0 f0             	and    $0xfffffff0,%eax
f0103230:	a2 ee b5 11 f0       	mov    %al,0xf011b5ee
f0103235:	0f b6 05 ee b5 11 f0 	movzbl 0xf011b5ee,%eax
f010323c:	83 e0 ef             	and    $0xffffffef,%eax
f010323f:	a2 ee b5 11 f0       	mov    %al,0xf011b5ee
f0103244:	0f b6 05 ee b5 11 f0 	movzbl 0xf011b5ee,%eax
f010324b:	83 e0 df             	and    $0xffffffdf,%eax
f010324e:	a2 ee b5 11 f0       	mov    %al,0xf011b5ee
f0103253:	0f b6 05 ee b5 11 f0 	movzbl 0xf011b5ee,%eax
f010325a:	83 c8 40             	or     $0x40,%eax
f010325d:	a2 ee b5 11 f0       	mov    %al,0xf011b5ee
f0103262:	0f b6 05 ee b5 11 f0 	movzbl 0xf011b5ee,%eax
f0103269:	83 e0 7f             	and    $0x7f,%eax
f010326c:	a2 ee b5 11 f0       	mov    %al,0xf011b5ee
f0103271:	b8 40 73 14 f0       	mov    $0xf0147340,%eax
f0103276:	c1 e8 18             	shr    $0x18,%eax
f0103279:	a2 ef b5 11 f0       	mov    %al,0xf011b5ef
					sizeof(struct Taskstate), 0);
	gdt[GD_TSS >> 3].sd_s = 0;
f010327e:	0f b6 05 ed b5 11 f0 	movzbl 0xf011b5ed,%eax
f0103285:	83 e0 ef             	and    $0xffffffef,%eax
f0103288:	a2 ed b5 11 f0       	mov    %al,0xf011b5ed
f010328d:	66 c7 45 fe 28 00    	movw   $0x28,-0x2(%ebp)
}

static __inline void
ltr(uint16 sel)
{
	__asm __volatile("ltr %0" : : "r" (sel));
f0103293:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
f0103297:	0f 00 d8             	ltr    %ax

	// Load the TSS
	ltr(GD_TSS);

	// Load the IDT
	asm volatile("lidt idt_pd");
f010329a:	0f 01 1d 58 b6 11 f0 	lidtl  0xf011b658
}
f01032a1:	90                   	nop
f01032a2:	c9                   	leave  
f01032a3:	c3                   	ret    

f01032a4 <print_trapframe>:

void
print_trapframe(struct Trapframe *tf)
{
f01032a4:	55                   	push   %ebp
f01032a5:	89 e5                	mov    %esp,%ebp
f01032a7:	83 ec 08             	sub    $0x8,%esp
	cprintf("TRAP frame at %p\n", tf);
f01032aa:	83 ec 08             	sub    $0x8,%esp
f01032ad:	ff 75 08             	pushl  0x8(%ebp)
f01032b0:	68 3b 5d 10 f0       	push   $0xf0105d3b
f01032b5:	e8 a4 fd ff ff       	call   f010305e <cprintf>
f01032ba:	83 c4 10             	add    $0x10,%esp
	print_regs(&tf->tf_regs);
f01032bd:	8b 45 08             	mov    0x8(%ebp),%eax
f01032c0:	83 ec 0c             	sub    $0xc,%esp
f01032c3:	50                   	push   %eax
f01032c4:	e8 fa 00 00 00       	call   f01033c3 <print_regs>
f01032c9:	83 c4 10             	add    $0x10,%esp
	cprintf("  es   0x----%04x\n", tf->tf_es);
f01032cc:	8b 45 08             	mov    0x8(%ebp),%eax
f01032cf:	0f b7 40 20          	movzwl 0x20(%eax),%eax
f01032d3:	0f b7 c0             	movzwl %ax,%eax
f01032d6:	83 ec 08             	sub    $0x8,%esp
f01032d9:	50                   	push   %eax
f01032da:	68 4d 5d 10 f0       	push   $0xf0105d4d
f01032df:	e8 7a fd ff ff       	call   f010305e <cprintf>
f01032e4:	83 c4 10             	add    $0x10,%esp
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
f01032e7:	8b 45 08             	mov    0x8(%ebp),%eax
f01032ea:	0f b7 40 24          	movzwl 0x24(%eax),%eax
f01032ee:	0f b7 c0             	movzwl %ax,%eax
f01032f1:	83 ec 08             	sub    $0x8,%esp
f01032f4:	50                   	push   %eax
f01032f5:	68 60 5d 10 f0       	push   $0xf0105d60
f01032fa:	e8 5f fd ff ff       	call   f010305e <cprintf>
f01032ff:	83 c4 10             	add    $0x10,%esp
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
f0103302:	8b 45 08             	mov    0x8(%ebp),%eax
f0103305:	8b 40 28             	mov    0x28(%eax),%eax
f0103308:	83 ec 0c             	sub    $0xc,%esp
f010330b:	50                   	push   %eax
f010330c:	e8 73 fd ff ff       	call   f0103084 <trapname>
f0103311:	83 c4 10             	add    $0x10,%esp
f0103314:	89 c2                	mov    %eax,%edx
f0103316:	8b 45 08             	mov    0x8(%ebp),%eax
f0103319:	8b 40 28             	mov    0x28(%eax),%eax
f010331c:	83 ec 04             	sub    $0x4,%esp
f010331f:	52                   	push   %edx
f0103320:	50                   	push   %eax
f0103321:	68 73 5d 10 f0       	push   $0xf0105d73
f0103326:	e8 33 fd ff ff       	call   f010305e <cprintf>
f010332b:	83 c4 10             	add    $0x10,%esp
	cprintf("  err  0x%08x\n", tf->tf_err);
f010332e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103331:	8b 40 2c             	mov    0x2c(%eax),%eax
f0103334:	83 ec 08             	sub    $0x8,%esp
f0103337:	50                   	push   %eax
f0103338:	68 85 5d 10 f0       	push   $0xf0105d85
f010333d:	e8 1c fd ff ff       	call   f010305e <cprintf>
f0103342:	83 c4 10             	add    $0x10,%esp
	cprintf("  eip  0x%08x\n", tf->tf_eip);
f0103345:	8b 45 08             	mov    0x8(%ebp),%eax
f0103348:	8b 40 30             	mov    0x30(%eax),%eax
f010334b:	83 ec 08             	sub    $0x8,%esp
f010334e:	50                   	push   %eax
f010334f:	68 94 5d 10 f0       	push   $0xf0105d94
f0103354:	e8 05 fd ff ff       	call   f010305e <cprintf>
f0103359:	83 c4 10             	add    $0x10,%esp
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
f010335c:	8b 45 08             	mov    0x8(%ebp),%eax
f010335f:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f0103363:	0f b7 c0             	movzwl %ax,%eax
f0103366:	83 ec 08             	sub    $0x8,%esp
f0103369:	50                   	push   %eax
f010336a:	68 a3 5d 10 f0       	push   $0xf0105da3
f010336f:	e8 ea fc ff ff       	call   f010305e <cprintf>
f0103374:	83 c4 10             	add    $0x10,%esp
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
f0103377:	8b 45 08             	mov    0x8(%ebp),%eax
f010337a:	8b 40 38             	mov    0x38(%eax),%eax
f010337d:	83 ec 08             	sub    $0x8,%esp
f0103380:	50                   	push   %eax
f0103381:	68 b6 5d 10 f0       	push   $0xf0105db6
f0103386:	e8 d3 fc ff ff       	call   f010305e <cprintf>
f010338b:	83 c4 10             	add    $0x10,%esp
	cprintf("  esp  0x%08x\n", tf->tf_esp);
f010338e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103391:	8b 40 3c             	mov    0x3c(%eax),%eax
f0103394:	83 ec 08             	sub    $0x8,%esp
f0103397:	50                   	push   %eax
f0103398:	68 c5 5d 10 f0       	push   $0xf0105dc5
f010339d:	e8 bc fc ff ff       	call   f010305e <cprintf>
f01033a2:	83 c4 10             	add    $0x10,%esp
	cprintf("  ss   0x----%04x\n", tf->tf_ss);
f01033a5:	8b 45 08             	mov    0x8(%ebp),%eax
f01033a8:	0f b7 40 40          	movzwl 0x40(%eax),%eax
f01033ac:	0f b7 c0             	movzwl %ax,%eax
f01033af:	83 ec 08             	sub    $0x8,%esp
f01033b2:	50                   	push   %eax
f01033b3:	68 d4 5d 10 f0       	push   $0xf0105dd4
f01033b8:	e8 a1 fc ff ff       	call   f010305e <cprintf>
f01033bd:	83 c4 10             	add    $0x10,%esp
}
f01033c0:	90                   	nop
f01033c1:	c9                   	leave  
f01033c2:	c3                   	ret    

f01033c3 <print_regs>:

void
print_regs(struct PushRegs *regs)
{
f01033c3:	55                   	push   %ebp
f01033c4:	89 e5                	mov    %esp,%ebp
f01033c6:	83 ec 08             	sub    $0x8,%esp
	cprintf("  edi  0x%08x\n", regs->reg_edi);
f01033c9:	8b 45 08             	mov    0x8(%ebp),%eax
f01033cc:	8b 00                	mov    (%eax),%eax
f01033ce:	83 ec 08             	sub    $0x8,%esp
f01033d1:	50                   	push   %eax
f01033d2:	68 e7 5d 10 f0       	push   $0xf0105de7
f01033d7:	e8 82 fc ff ff       	call   f010305e <cprintf>
f01033dc:	83 c4 10             	add    $0x10,%esp
	cprintf("  esi  0x%08x\n", regs->reg_esi);
f01033df:	8b 45 08             	mov    0x8(%ebp),%eax
f01033e2:	8b 40 04             	mov    0x4(%eax),%eax
f01033e5:	83 ec 08             	sub    $0x8,%esp
f01033e8:	50                   	push   %eax
f01033e9:	68 f6 5d 10 f0       	push   $0xf0105df6
f01033ee:	e8 6b fc ff ff       	call   f010305e <cprintf>
f01033f3:	83 c4 10             	add    $0x10,%esp
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
f01033f6:	8b 45 08             	mov    0x8(%ebp),%eax
f01033f9:	8b 40 08             	mov    0x8(%eax),%eax
f01033fc:	83 ec 08             	sub    $0x8,%esp
f01033ff:	50                   	push   %eax
f0103400:	68 05 5e 10 f0       	push   $0xf0105e05
f0103405:	e8 54 fc ff ff       	call   f010305e <cprintf>
f010340a:	83 c4 10             	add    $0x10,%esp
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
f010340d:	8b 45 08             	mov    0x8(%ebp),%eax
f0103410:	8b 40 0c             	mov    0xc(%eax),%eax
f0103413:	83 ec 08             	sub    $0x8,%esp
f0103416:	50                   	push   %eax
f0103417:	68 14 5e 10 f0       	push   $0xf0105e14
f010341c:	e8 3d fc ff ff       	call   f010305e <cprintf>
f0103421:	83 c4 10             	add    $0x10,%esp
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
f0103424:	8b 45 08             	mov    0x8(%ebp),%eax
f0103427:	8b 40 10             	mov    0x10(%eax),%eax
f010342a:	83 ec 08             	sub    $0x8,%esp
f010342d:	50                   	push   %eax
f010342e:	68 23 5e 10 f0       	push   $0xf0105e23
f0103433:	e8 26 fc ff ff       	call   f010305e <cprintf>
f0103438:	83 c4 10             	add    $0x10,%esp
	cprintf("  edx  0x%08x\n", regs->reg_edx);
f010343b:	8b 45 08             	mov    0x8(%ebp),%eax
f010343e:	8b 40 14             	mov    0x14(%eax),%eax
f0103441:	83 ec 08             	sub    $0x8,%esp
f0103444:	50                   	push   %eax
f0103445:	68 32 5e 10 f0       	push   $0xf0105e32
f010344a:	e8 0f fc ff ff       	call   f010305e <cprintf>
f010344f:	83 c4 10             	add    $0x10,%esp
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
f0103452:	8b 45 08             	mov    0x8(%ebp),%eax
f0103455:	8b 40 18             	mov    0x18(%eax),%eax
f0103458:	83 ec 08             	sub    $0x8,%esp
f010345b:	50                   	push   %eax
f010345c:	68 41 5e 10 f0       	push   $0xf0105e41
f0103461:	e8 f8 fb ff ff       	call   f010305e <cprintf>
f0103466:	83 c4 10             	add    $0x10,%esp
	cprintf("  eax  0x%08x\n", regs->reg_eax);
f0103469:	8b 45 08             	mov    0x8(%ebp),%eax
f010346c:	8b 40 1c             	mov    0x1c(%eax),%eax
f010346f:	83 ec 08             	sub    $0x8,%esp
f0103472:	50                   	push   %eax
f0103473:	68 50 5e 10 f0       	push   $0xf0105e50
f0103478:	e8 e1 fb ff ff       	call   f010305e <cprintf>
f010347d:	83 c4 10             	add    $0x10,%esp
}
f0103480:	90                   	nop
f0103481:	c9                   	leave  
f0103482:	c3                   	ret    

f0103483 <trap_dispatch>:

static void
trap_dispatch(struct Trapframe *tf)
{
f0103483:	55                   	push   %ebp
f0103484:	89 e5                	mov    %esp,%ebp
f0103486:	57                   	push   %edi
f0103487:	56                   	push   %esi
f0103488:	53                   	push   %ebx
f0103489:	83 ec 1c             	sub    $0x1c,%esp
	// Handle processor exceptions.
	// LAB 3: Your code here.
	
	if(tf->tf_trapno == T_PGFLT)
f010348c:	8b 45 08             	mov    0x8(%ebp),%eax
f010348f:	8b 40 28             	mov    0x28(%eax),%eax
f0103492:	83 f8 0e             	cmp    $0xe,%eax
f0103495:	75 13                	jne    f01034aa <trap_dispatch+0x27>
	{
		page_fault_handler(tf);
f0103497:	83 ec 0c             	sub    $0xc,%esp
f010349a:	ff 75 08             	pushl  0x8(%ebp)
f010349d:	e8 49 01 00 00       	call   f01035eb <page_fault_handler>
f01034a2:	83 c4 10             	add    $0x10,%esp
		else {
			env_destroy(curenv);
			return;	
		}
	}
	return;
f01034a5:	e9 91 00 00 00       	jmp    f010353b <trap_dispatch+0xb8>
	
	if(tf->tf_trapno == T_PGFLT)
	{
		page_fault_handler(tf);
	}
	else if (tf->tf_trapno == T_SYSCALL)
f01034aa:	8b 45 08             	mov    0x8(%ebp),%eax
f01034ad:	8b 40 28             	mov    0x28(%eax),%eax
f01034b0:	83 f8 30             	cmp    $0x30,%eax
f01034b3:	75 42                	jne    f01034f7 <trap_dispatch+0x74>
	{
		uint32 ret = syscall(tf->tf_regs.reg_eax
f01034b5:	8b 45 08             	mov    0x8(%ebp),%eax
f01034b8:	8b 78 04             	mov    0x4(%eax),%edi
f01034bb:	8b 45 08             	mov    0x8(%ebp),%eax
f01034be:	8b 30                	mov    (%eax),%esi
f01034c0:	8b 45 08             	mov    0x8(%ebp),%eax
f01034c3:	8b 58 10             	mov    0x10(%eax),%ebx
f01034c6:	8b 45 08             	mov    0x8(%ebp),%eax
f01034c9:	8b 48 18             	mov    0x18(%eax),%ecx
f01034cc:	8b 45 08             	mov    0x8(%ebp),%eax
f01034cf:	8b 50 14             	mov    0x14(%eax),%edx
f01034d2:	8b 45 08             	mov    0x8(%ebp),%eax
f01034d5:	8b 40 1c             	mov    0x1c(%eax),%eax
f01034d8:	83 ec 08             	sub    $0x8,%esp
f01034db:	57                   	push   %edi
f01034dc:	56                   	push   %esi
f01034dd:	53                   	push   %ebx
f01034de:	51                   	push   %ecx
f01034df:	52                   	push   %edx
f01034e0:	50                   	push   %eax
f01034e1:	e8 2b 04 00 00       	call   f0103911 <syscall>
f01034e6:	83 c4 20             	add    $0x20,%esp
f01034e9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
			,tf->tf_regs.reg_edx
			,tf->tf_regs.reg_ecx
			,tf->tf_regs.reg_ebx
			,tf->tf_regs.reg_edi
					,tf->tf_regs.reg_esi);
		tf->tf_regs.reg_eax = ret;
f01034ec:	8b 45 08             	mov    0x8(%ebp),%eax
f01034ef:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f01034f2:	89 50 1c             	mov    %edx,0x1c(%eax)
		else {
			env_destroy(curenv);
			return;	
		}
	}
	return;
f01034f5:	eb 44                	jmp    f010353b <trap_dispatch+0xb8>
		tf->tf_regs.reg_eax = ret;
	}
	else
	{
		// Unexpected trap: The user process or the kernel has a bug.
		print_trapframe(tf);
f01034f7:	83 ec 0c             	sub    $0xc,%esp
f01034fa:	ff 75 08             	pushl  0x8(%ebp)
f01034fd:	e8 a2 fd ff ff       	call   f01032a4 <print_trapframe>
f0103502:	83 c4 10             	add    $0x10,%esp
		if (tf->tf_cs == GD_KT)
f0103505:	8b 45 08             	mov    0x8(%ebp),%eax
f0103508:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f010350c:	66 83 f8 08          	cmp    $0x8,%ax
f0103510:	75 17                	jne    f0103529 <trap_dispatch+0xa6>
			panic("unhandled trap in kernel");
f0103512:	83 ec 04             	sub    $0x4,%esp
f0103515:	68 5f 5e 10 f0       	push   $0xf0105e5f
f010351a:	68 8a 00 00 00       	push   $0x8a
f010351f:	68 78 5e 10 f0       	push   $0xf0105e78
f0103524:	e8 05 cc ff ff       	call   f010012e <_panic>
		else {
			env_destroy(curenv);
f0103529:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010352e:	83 ec 0c             	sub    $0xc,%esp
f0103531:	50                   	push   %eax
f0103532:	e8 6d f9 ff ff       	call   f0102ea4 <env_destroy>
f0103537:	83 c4 10             	add    $0x10,%esp
			return;	
f010353a:	90                   	nop
		}
	}
	return;
}
f010353b:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010353e:	5b                   	pop    %ebx
f010353f:	5e                   	pop    %esi
f0103540:	5f                   	pop    %edi
f0103541:	5d                   	pop    %ebp
f0103542:	c3                   	ret    

f0103543 <trap>:

void
trap(struct Trapframe *tf)
{
f0103543:	55                   	push   %ebp
f0103544:	89 e5                	mov    %esp,%ebp
f0103546:	57                   	push   %edi
f0103547:	56                   	push   %esi
f0103548:	53                   	push   %ebx
f0103549:	83 ec 0c             	sub    $0xc,%esp
	//cprintf("Incoming TRAP frame at %p\n", tf);

	if ((tf->tf_cs & 3) == 3) {
f010354c:	8b 45 08             	mov    0x8(%ebp),%eax
f010354f:	0f b7 40 34          	movzwl 0x34(%eax),%eax
f0103553:	0f b7 c0             	movzwl %ax,%eax
f0103556:	83 e0 03             	and    $0x3,%eax
f0103559:	83 f8 03             	cmp    $0x3,%eax
f010355c:	75 42                	jne    f01035a0 <trap+0x5d>
		// Trapped from user mode.
		// Copy trap frame (which is currently on the stack)
		// into 'curenv->env_tf', so that running the environment
		// will restart at the trap point.
		assert(curenv);
f010355e:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0103563:	85 c0                	test   %eax,%eax
f0103565:	75 19                	jne    f0103580 <trap+0x3d>
f0103567:	68 84 5e 10 f0       	push   $0xf0105e84
f010356c:	68 8b 5e 10 f0       	push   $0xf0105e8b
f0103571:	68 9d 00 00 00       	push   $0x9d
f0103576:	68 78 5e 10 f0       	push   $0xf0105e78
f010357b:	e8 ae cb ff ff       	call   f010012e <_panic>
		curenv->env_tf = *tf;
f0103580:	8b 15 30 6b 14 f0    	mov    0xf0146b30,%edx
f0103586:	8b 45 08             	mov    0x8(%ebp),%eax
f0103589:	89 c3                	mov    %eax,%ebx
f010358b:	b8 11 00 00 00       	mov    $0x11,%eax
f0103590:	89 d7                	mov    %edx,%edi
f0103592:	89 de                	mov    %ebx,%esi
f0103594:	89 c1                	mov    %eax,%ecx
f0103596:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
		// The trapframe on the stack should be ignored from here on.
		tf = &curenv->env_tf;
f0103598:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010359d:	89 45 08             	mov    %eax,0x8(%ebp)
	}
	
	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);
f01035a0:	83 ec 0c             	sub    $0xc,%esp
f01035a3:	ff 75 08             	pushl  0x8(%ebp)
f01035a6:	e8 d8 fe ff ff       	call   f0103483 <trap_dispatch>
f01035ab:	83 c4 10             	add    $0x10,%esp

        // Return to the current environment, which should be runnable.
        assert(curenv && curenv->env_status == ENV_RUNNABLE);
f01035ae:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01035b3:	85 c0                	test   %eax,%eax
f01035b5:	74 0d                	je     f01035c4 <trap+0x81>
f01035b7:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01035bc:	8b 40 54             	mov    0x54(%eax),%eax
f01035bf:	83 f8 01             	cmp    $0x1,%eax
f01035c2:	74 19                	je     f01035dd <trap+0x9a>
f01035c4:	68 a0 5e 10 f0       	push   $0xf0105ea0
f01035c9:	68 8b 5e 10 f0       	push   $0xf0105e8b
f01035ce:	68 a7 00 00 00       	push   $0xa7
f01035d3:	68 78 5e 10 f0       	push   $0xf0105e78
f01035d8:	e8 51 cb ff ff       	call   f010012e <_panic>
        env_run(curenv);
f01035dd:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01035e2:	83 ec 0c             	sub    $0xc,%esp
f01035e5:	50                   	push   %eax
f01035e6:	e8 55 f3 ff ff       	call   f0102940 <env_run>

f01035eb <page_fault_handler>:
}


void
page_fault_handler(struct Trapframe *tf)
{
f01035eb:	55                   	push   %ebp
f01035ec:	89 e5                	mov    %esp,%ebp
f01035ee:	83 ec 18             	sub    $0x18,%esp

static __inline uint32
rcr2(void)
{
	uint32 val;
	__asm __volatile("movl %%cr2,%0" : "=r" (val));
f01035f1:	0f 20 d0             	mov    %cr2,%eax
f01035f4:	89 45 f0             	mov    %eax,-0x10(%ebp)
	return val;
f01035f7:	8b 45 f0             	mov    -0x10(%ebp),%eax
	uint32 fault_va;

	// Read processor's CR2 register to find the faulting address
	fault_va = rcr2();
f01035fa:	89 45 f4             	mov    %eax,-0xc(%ebp)
	//   (the 'tf' variable points at 'curenv->env_tf').
	
	// LAB 4: Your code here.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f01035fd:	8b 45 08             	mov    0x8(%ebp),%eax
f0103600:	8b 50 30             	mov    0x30(%eax),%edx
		curenv->env_id, fault_va, tf->tf_eip);
f0103603:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
	//   (the 'tf' variable points at 'curenv->env_tf').
	
	// LAB 4: Your code here.

	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
f0103608:	8b 40 4c             	mov    0x4c(%eax),%eax
f010360b:	52                   	push   %edx
f010360c:	ff 75 f4             	pushl  -0xc(%ebp)
f010360f:	50                   	push   %eax
f0103610:	68 d0 5e 10 f0       	push   $0xf0105ed0
f0103615:	e8 44 fa ff ff       	call   f010305e <cprintf>
f010361a:	83 c4 10             	add    $0x10,%esp
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
f010361d:	83 ec 0c             	sub    $0xc,%esp
f0103620:	ff 75 08             	pushl  0x8(%ebp)
f0103623:	e8 7c fc ff ff       	call   f01032a4 <print_trapframe>
f0103628:	83 c4 10             	add    $0x10,%esp
	env_destroy(curenv);
f010362b:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0103630:	83 ec 0c             	sub    $0xc,%esp
f0103633:	50                   	push   %eax
f0103634:	e8 6b f8 ff ff       	call   f0102ea4 <env_destroy>
f0103639:	83 c4 10             	add    $0x10,%esp
	
}
f010363c:	90                   	nop
f010363d:	c9                   	leave  
f010363e:	c3                   	ret    
f010363f:	90                   	nop

f0103640 <PAGE_FAULT>:

/*
 * Lab 3: Your code here for generating entry points for the different traps.
 */

TRAPHANDLER(PAGE_FAULT, T_PGFLT)		
f0103640:	6a 0e                	push   $0xe
f0103642:	eb 06                	jmp    f010364a <_alltraps>

f0103644 <SYSCALL_HANDLER>:

TRAPHANDLER_NOEC(SYSCALL_HANDLER, T_SYSCALL)
f0103644:	6a 00                	push   $0x0
f0103646:	6a 30                	push   $0x30
f0103648:	eb 00                	jmp    f010364a <_alltraps>

f010364a <_alltraps>:
/*
 * Lab 3: Your code here for _alltraps
 */
_alltraps:

push %ds 
f010364a:	1e                   	push   %ds
push %es 
f010364b:	06                   	push   %es
pushal 	
f010364c:	60                   	pusha  

mov $(GD_KD), %ax 
f010364d:	66 b8 10 00          	mov    $0x10,%ax
mov %ax,%ds
f0103651:	8e d8                	mov    %eax,%ds
mov %ax,%es
f0103653:	8e c0                	mov    %eax,%es

push %esp
f0103655:	54                   	push   %esp

call trap
f0103656:	e8 e8 fe ff ff       	call   f0103543 <trap>

pop %ecx /* poping the pointer to the tf from the stack so that the stack top is at the values of the registers posuhed by pusha*/
f010365b:	59                   	pop    %ecx
popal 	
f010365c:	61                   	popa   
pop %es 
f010365d:	07                   	pop    %es
pop %ds    
f010365e:	1f                   	pop    %ds

/*skipping the trap_no and the error code so that the stack top is at the old eip value*/
add $(8),%esp
f010365f:	83 c4 08             	add    $0x8,%esp

iret
f0103662:	cf                   	iret   

f0103663 <to_frame_number>:
void	unmap_frame(uint32 *pgdir, void *va);
struct Frame_Info *get_frame_info(uint32 *ptr_page_directory, void *virtual_address, uint32 **ptr_page_table);
void decrement_references(struct Frame_Info* ptr_frame_info);

static inline uint32 to_frame_number(struct Frame_Info *ptr_frame_info)
{
f0103663:	55                   	push   %ebp
f0103664:	89 e5                	mov    %esp,%ebp
	return ptr_frame_info - frames_info;
f0103666:	8b 45 08             	mov    0x8(%ebp),%eax
f0103669:	8b 15 bc 73 14 f0    	mov    0xf01473bc,%edx
f010366f:	29 d0                	sub    %edx,%eax
f0103671:	c1 f8 02             	sar    $0x2,%eax
f0103674:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
}
f010367a:	5d                   	pop    %ebp
f010367b:	c3                   	ret    

f010367c <to_physical_address>:

static inline uint32 to_physical_address(struct Frame_Info *ptr_frame_info)
{
f010367c:	55                   	push   %ebp
f010367d:	89 e5                	mov    %esp,%ebp
	return to_frame_number(ptr_frame_info) << PGSHIFT;
f010367f:	ff 75 08             	pushl  0x8(%ebp)
f0103682:	e8 dc ff ff ff       	call   f0103663 <to_frame_number>
f0103687:	83 c4 04             	add    $0x4,%esp
f010368a:	c1 e0 0c             	shl    $0xc,%eax
}
f010368d:	c9                   	leave  
f010368e:	c3                   	ret    

f010368f <sys_cputs>:

// Print a string to the system console.
// The string is exactly 'len' characters long.
// Destroys the environment on memory errors.
static void sys_cputs(const char *s, uint32 len)
{
f010368f:	55                   	push   %ebp
f0103690:	89 e5                	mov    %esp,%ebp
f0103692:	83 ec 08             	sub    $0x8,%esp
	// Destroy the environment if not.
	
	// LAB 3: Your code here.

	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
f0103695:	83 ec 04             	sub    $0x4,%esp
f0103698:	ff 75 08             	pushl  0x8(%ebp)
f010369b:	ff 75 0c             	pushl  0xc(%ebp)
f010369e:	68 90 60 10 f0       	push   $0xf0106090
f01036a3:	e8 b6 f9 ff ff       	call   f010305e <cprintf>
f01036a8:	83 c4 10             	add    $0x10,%esp
}
f01036ab:	90                   	nop
f01036ac:	c9                   	leave  
f01036ad:	c3                   	ret    

f01036ae <sys_cgetc>:

// Read a character from the system console.
// Returns the character.
static int
sys_cgetc(void)
{
f01036ae:	55                   	push   %ebp
f01036af:	89 e5                	mov    %esp,%ebp
f01036b1:	83 ec 18             	sub    $0x18,%esp
	int c;

	// The cons_getc() primitive doesn't wait for a character,
	// but the sys_cgetc() system call does.
	while ((c = cons_getc()) == 0)
f01036b4:	e8 f4 d1 ff ff       	call   f01008ad <cons_getc>
f01036b9:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01036bc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01036c0:	74 f2                	je     f01036b4 <sys_cgetc+0x6>
		/* do nothing */;

	return c;
f01036c2:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f01036c5:	c9                   	leave  
f01036c6:	c3                   	ret    

f01036c7 <sys_getenvid>:

// Returns the current environment's envid.
static int32 sys_getenvid(void)
{
f01036c7:	55                   	push   %ebp
f01036c8:	89 e5                	mov    %esp,%ebp
	return curenv->env_id;
f01036ca:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01036cf:	8b 40 4c             	mov    0x4c(%eax),%eax
}
f01036d2:	5d                   	pop    %ebp
f01036d3:	c3                   	ret    

f01036d4 <sys_env_destroy>:
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
static int sys_env_destroy(int32  envid)
{
f01036d4:	55                   	push   %ebp
f01036d5:	89 e5                	mov    %esp,%ebp
f01036d7:	83 ec 18             	sub    $0x18,%esp
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
f01036da:	83 ec 04             	sub    $0x4,%esp
f01036dd:	6a 01                	push   $0x1
f01036df:	8d 45 f0             	lea    -0x10(%ebp),%eax
f01036e2:	50                   	push   %eax
f01036e3:	ff 75 08             	pushl  0x8(%ebp)
f01036e6:	e8 16 e6 ff ff       	call   f0101d01 <envid2env>
f01036eb:	83 c4 10             	add    $0x10,%esp
f01036ee:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01036f1:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f01036f5:	79 05                	jns    f01036fc <sys_env_destroy+0x28>
		return r;
f01036f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01036fa:	eb 5b                	jmp    f0103757 <sys_env_destroy+0x83>
	if (e == curenv)
f01036fc:	8b 55 f0             	mov    -0x10(%ebp),%edx
f01036ff:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0103704:	39 c2                	cmp    %eax,%edx
f0103706:	75 1b                	jne    f0103723 <sys_env_destroy+0x4f>
		cprintf("[%08x] exiting gracefully\n", curenv->env_id);
f0103708:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010370d:	8b 40 4c             	mov    0x4c(%eax),%eax
f0103710:	83 ec 08             	sub    $0x8,%esp
f0103713:	50                   	push   %eax
f0103714:	68 95 60 10 f0       	push   $0xf0106095
f0103719:	e8 40 f9 ff ff       	call   f010305e <cprintf>
f010371e:	83 c4 10             	add    $0x10,%esp
f0103721:	eb 20                	jmp    f0103743 <sys_env_destroy+0x6f>
	else
		cprintf("[%08x] destroying %08x\n", curenv->env_id, e->env_id);
f0103723:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103726:	8b 50 4c             	mov    0x4c(%eax),%edx
f0103729:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010372e:	8b 40 4c             	mov    0x4c(%eax),%eax
f0103731:	83 ec 04             	sub    $0x4,%esp
f0103734:	52                   	push   %edx
f0103735:	50                   	push   %eax
f0103736:	68 b0 60 10 f0       	push   $0xf01060b0
f010373b:	e8 1e f9 ff ff       	call   f010305e <cprintf>
f0103740:	83 c4 10             	add    $0x10,%esp
	env_destroy(e);
f0103743:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103746:	83 ec 0c             	sub    $0xc,%esp
f0103749:	50                   	push   %eax
f010374a:	e8 55 f7 ff ff       	call   f0102ea4 <env_destroy>
f010374f:	83 c4 10             	add    $0x10,%esp
	return 0;
f0103752:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0103757:	c9                   	leave  
f0103758:	c3                   	ret    

f0103759 <sys_env_sleep>:

static void sys_env_sleep()
{
f0103759:	55                   	push   %ebp
f010375a:	89 e5                	mov    %esp,%ebp
f010375c:	83 ec 08             	sub    $0x8,%esp
	env_run_cmd_prmpt();
f010375f:	e8 5b f7 ff ff       	call   f0102ebf <env_run_cmd_prmpt>
}
f0103764:	90                   	nop
f0103765:	c9                   	leave  
f0103766:	c3                   	ret    

f0103767 <sys_allocate_page>:
//	E_INVAL if va >= UTOP, or va is not page-aligned.
//	E_INVAL if perm is inappropriate (see above).
//	E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_allocate_page(void *va, int perm)
{
f0103767:	55                   	push   %ebp
f0103768:	89 e5                	mov    %esp,%ebp
f010376a:	83 ec 28             	sub    $0x28,%esp
	//   parameters for correctness.
	//   If page_insert() fails, remember to free the page you
	//   allocated!
	
	int r;
	struct Env *e = curenv;
f010376d:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f0103772:	89 45 f4             	mov    %eax,-0xc(%ebp)

	//if ((r = envid2env(envid, &e, 1)) < 0)
		//return r;
	
	struct Frame_Info *ptr_frame_info ;
	r = allocate_frame(&ptr_frame_info) ;
f0103775:	83 ec 0c             	sub    $0xc,%esp
f0103778:	8d 45 e0             	lea    -0x20(%ebp),%eax
f010377b:	50                   	push   %eax
f010377c:	e8 ea ec ff ff       	call   f010246b <allocate_frame>
f0103781:	83 c4 10             	add    $0x10,%esp
f0103784:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (r == E_NO_MEM)
f0103787:	83 7d f0 fc          	cmpl   $0xfffffffc,-0x10(%ebp)
f010378b:	75 08                	jne    f0103795 <sys_allocate_page+0x2e>
		return r ;
f010378d:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103790:	e9 cc 00 00 00       	jmp    f0103861 <sys_allocate_page+0xfa>
	
	//check virtual address to be paged_aligned and < USER_TOP
	if ((uint32)va >= USER_TOP || (uint32)va % PAGE_SIZE != 0)
f0103795:	8b 45 08             	mov    0x8(%ebp),%eax
f0103798:	3d ff ff bf ee       	cmp    $0xeebfffff,%eax
f010379d:	77 0c                	ja     f01037ab <sys_allocate_page+0x44>
f010379f:	8b 45 08             	mov    0x8(%ebp),%eax
f01037a2:	25 ff 0f 00 00       	and    $0xfff,%eax
f01037a7:	85 c0                	test   %eax,%eax
f01037a9:	74 0a                	je     f01037b5 <sys_allocate_page+0x4e>
		return E_INVAL;
f01037ab:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f01037b0:	e9 ac 00 00 00       	jmp    f0103861 <sys_allocate_page+0xfa>
	
	//check permissions to be appropriatess
	if ((perm & (~PERM_AVAILABLE & ~PERM_WRITEABLE)) != (PERM_USER))
f01037b5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01037b8:	25 fd f1 ff ff       	and    $0xfffff1fd,%eax
f01037bd:	83 f8 04             	cmp    $0x4,%eax
f01037c0:	74 0a                	je     f01037cc <sys_allocate_page+0x65>
		return E_INVAL;
f01037c2:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
f01037c7:	e9 95 00 00 00       	jmp    f0103861 <sys_allocate_page+0xfa>
	
			
	uint32 physical_address = to_physical_address(ptr_frame_info) ;
f01037cc:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01037cf:	83 ec 0c             	sub    $0xc,%esp
f01037d2:	50                   	push   %eax
f01037d3:	e8 a4 fe ff ff       	call   f010367c <to_physical_address>
f01037d8:	83 c4 10             	add    $0x10,%esp
f01037db:	89 45 ec             	mov    %eax,-0x14(%ebp)
	
	memset(K_VIRTUAL_ADDRESS(physical_address), 0, PAGE_SIZE);
f01037de:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01037e1:	89 45 e8             	mov    %eax,-0x18(%ebp)
f01037e4:	8b 45 e8             	mov    -0x18(%ebp),%eax
f01037e7:	c1 e8 0c             	shr    $0xc,%eax
f01037ea:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01037ed:	a1 a8 73 14 f0       	mov    0xf01473a8,%eax
f01037f2:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f01037f5:	72 14                	jb     f010380b <sys_allocate_page+0xa4>
f01037f7:	ff 75 e8             	pushl  -0x18(%ebp)
f01037fa:	68 c8 60 10 f0       	push   $0xf01060c8
f01037ff:	6a 7a                	push   $0x7a
f0103801:	68 f7 60 10 f0       	push   $0xf01060f7
f0103806:	e8 23 c9 ff ff       	call   f010012e <_panic>
f010380b:	8b 45 e8             	mov    -0x18(%ebp),%eax
f010380e:	2d 00 00 00 10       	sub    $0x10000000,%eax
f0103813:	83 ec 04             	sub    $0x4,%esp
f0103816:	68 00 10 00 00       	push   $0x1000
f010381b:	6a 00                	push   $0x0
f010381d:	50                   	push   %eax
f010381e:	e8 5b 0f 00 00       	call   f010477e <memset>
f0103823:	83 c4 10             	add    $0x10,%esp
		
	r = map_frame(e->env_pgdir, ptr_frame_info, va, perm) ;
f0103826:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0103829:	8b 45 f4             	mov    -0xc(%ebp),%eax
f010382c:	8b 40 5c             	mov    0x5c(%eax),%eax
f010382f:	ff 75 0c             	pushl  0xc(%ebp)
f0103832:	ff 75 08             	pushl  0x8(%ebp)
f0103835:	52                   	push   %edx
f0103836:	50                   	push   %eax
f0103837:	e8 40 ee ff ff       	call   f010267c <map_frame>
f010383c:	83 c4 10             	add    $0x10,%esp
f010383f:	89 45 f0             	mov    %eax,-0x10(%ebp)
	if (r == E_NO_MEM)
f0103842:	83 7d f0 fc          	cmpl   $0xfffffffc,-0x10(%ebp)
f0103846:	75 14                	jne    f010385c <sys_allocate_page+0xf5>
	{
		decrement_references(ptr_frame_info);
f0103848:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010384b:	83 ec 0c             	sub    $0xc,%esp
f010384e:	50                   	push   %eax
f010384f:	e8 b5 ec ff ff       	call   f0102509 <decrement_references>
f0103854:	83 c4 10             	add    $0x10,%esp
		return r;
f0103857:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010385a:	eb 05                	jmp    f0103861 <sys_allocate_page+0xfa>
	}
	return 0 ;
f010385c:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0103861:	c9                   	leave  
f0103862:	c3                   	ret    

f0103863 <sys_get_page>:
//	E_INVAL if va >= UTOP, or va is not page-aligned.
//	E_INVAL if perm is inappropriate (see above).
//	E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_get_page(void *va, int perm)
{
f0103863:	55                   	push   %ebp
f0103864:	89 e5                	mov    %esp,%ebp
f0103866:	83 ec 08             	sub    $0x8,%esp
	return get_page(curenv->env_pgdir, va, perm) ;
f0103869:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f010386e:	8b 40 5c             	mov    0x5c(%eax),%eax
f0103871:	83 ec 04             	sub    $0x4,%esp
f0103874:	ff 75 0c             	pushl  0xc(%ebp)
f0103877:	ff 75 08             	pushl  0x8(%ebp)
f010387a:	50                   	push   %eax
f010387b:	e8 72 ef ff ff       	call   f01027f2 <get_page>
f0103880:	83 c4 10             	add    $0x10,%esp
}
f0103883:	c9                   	leave  
f0103884:	c3                   	ret    

f0103885 <sys_map_frame>:
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
static int sys_map_frame(int32 srcenvid, void *srcva, int32 dstenvid, void *dstva, int perm)
{
f0103885:	55                   	push   %ebp
f0103886:	89 e5                	mov    %esp,%ebp
f0103888:	83 ec 08             	sub    $0x8,%esp
	//   parameters for correctness.
	//   Use the third argument to page_lookup() to
	//   check the current permissions on the page.

	// LAB 4: Your code here.
	panic("sys_map_frame not implemented");
f010388b:	83 ec 04             	sub    $0x4,%esp
f010388e:	68 06 61 10 f0       	push   $0xf0106106
f0103893:	68 b1 00 00 00       	push   $0xb1
f0103898:	68 f7 60 10 f0       	push   $0xf01060f7
f010389d:	e8 8c c8 ff ff       	call   f010012e <_panic>

f01038a2 <sys_unmap_frame>:
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
static int sys_unmap_frame(int32 envid, void *va)
{
f01038a2:	55                   	push   %ebp
f01038a3:	89 e5                	mov    %esp,%ebp
f01038a5:	83 ec 08             	sub    $0x8,%esp
	// Hint: This function is a wrapper around page_remove().
	
	// LAB 4: Your code here.
	panic("sys_page_unmap not implemented");
f01038a8:	83 ec 04             	sub    $0x4,%esp
f01038ab:	68 24 61 10 f0       	push   $0xf0106124
f01038b0:	68 c0 00 00 00       	push   $0xc0
f01038b5:	68 f7 60 10 f0       	push   $0xf01060f7
f01038ba:	e8 6f c8 ff ff       	call   f010012e <_panic>

f01038bf <sys_calculate_required_frames>:
}

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
f01038bf:	55                   	push   %ebp
f01038c0:	89 e5                	mov    %esp,%ebp
f01038c2:	83 ec 08             	sub    $0x8,%esp
	return calculate_required_frames(curenv->env_pgdir, start_virtual_address, size); 
f01038c5:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01038ca:	8b 40 5c             	mov    0x5c(%eax),%eax
f01038cd:	83 ec 04             	sub    $0x4,%esp
f01038d0:	ff 75 0c             	pushl  0xc(%ebp)
f01038d3:	ff 75 08             	pushl  0x8(%ebp)
f01038d6:	50                   	push   %eax
f01038d7:	e8 33 ef ff ff       	call   f010280f <calculate_required_frames>
f01038dc:	83 c4 10             	add    $0x10,%esp
}
f01038df:	c9                   	leave  
f01038e0:	c3                   	ret    

f01038e1 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
f01038e1:	55                   	push   %ebp
f01038e2:	89 e5                	mov    %esp,%ebp
f01038e4:	83 ec 08             	sub    $0x8,%esp
	return calculate_free_frames();
f01038e7:	e8 40 ef ff ff       	call   f010282c <calculate_free_frames>
}
f01038ec:	c9                   	leave  
f01038ed:	c3                   	ret    

f01038ee <sys_freeMem>:
void sys_freeMem(void* start_virtual_address, uint32 size)
{
f01038ee:	55                   	push   %ebp
f01038ef:	89 e5                	mov    %esp,%ebp
f01038f1:	83 ec 08             	sub    $0x8,%esp
	freeMem((uint32*)curenv->env_pgdir, (void*)start_virtual_address, size);
f01038f4:	a1 30 6b 14 f0       	mov    0xf0146b30,%eax
f01038f9:	8b 40 5c             	mov    0x5c(%eax),%eax
f01038fc:	83 ec 04             	sub    $0x4,%esp
f01038ff:	ff 75 0c             	pushl  0xc(%ebp)
f0103902:	ff 75 08             	pushl  0x8(%ebp)
f0103905:	50                   	push   %eax
f0103906:	e8 4f ef ff ff       	call   f010285a <freeMem>
f010390b:	83 c4 10             	add    $0x10,%esp
	return;
f010390e:	90                   	nop
}
f010390f:	c9                   	leave  
f0103910:	c3                   	ret    

f0103911 <syscall>:
// Dispatches to the correct kernel function, passing the arguments.
uint32
syscall(uint32 syscallno, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
f0103911:	55                   	push   %ebp
f0103912:	89 e5                	mov    %esp,%ebp
f0103914:	56                   	push   %esi
f0103915:	53                   	push   %ebx
	// Call the function corresponding to the 'syscallno' parameter.
	// Return any appropriate return value.
	// LAB 3: Your code here.
	switch(syscallno)
f0103916:	83 7d 08 0c          	cmpl   $0xc,0x8(%ebp)
f010391a:	0f 87 19 01 00 00    	ja     f0103a39 <syscall+0x128>
f0103920:	8b 45 08             	mov    0x8(%ebp),%eax
f0103923:	c1 e0 02             	shl    $0x2,%eax
f0103926:	05 44 61 10 f0       	add    $0xf0106144,%eax
f010392b:	8b 00                	mov    (%eax),%eax
f010392d:	ff e0                	jmp    *%eax
	{
		case SYS_cputs:
			sys_cputs((const char*)a1,a2);
f010392f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103932:	83 ec 08             	sub    $0x8,%esp
f0103935:	ff 75 10             	pushl  0x10(%ebp)
f0103938:	50                   	push   %eax
f0103939:	e8 51 fd ff ff       	call   f010368f <sys_cputs>
f010393e:	83 c4 10             	add    $0x10,%esp
			return 0;
f0103941:	b8 00 00 00 00       	mov    $0x0,%eax
f0103946:	e9 f3 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_cgetc:
			return sys_cgetc();
f010394b:	e8 5e fd ff ff       	call   f01036ae <sys_cgetc>
f0103950:	e9 e9 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_getenvid:
			return sys_getenvid();
f0103955:	e8 6d fd ff ff       	call   f01036c7 <sys_getenvid>
f010395a:	e9 df 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_env_destroy:
			return sys_env_destroy(a1);
f010395f:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103962:	83 ec 0c             	sub    $0xc,%esp
f0103965:	50                   	push   %eax
f0103966:	e8 69 fd ff ff       	call   f01036d4 <sys_env_destroy>
f010396b:	83 c4 10             	add    $0x10,%esp
f010396e:	e9 cb 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_env_sleep:
			sys_env_sleep();
f0103973:	e8 e1 fd ff ff       	call   f0103759 <sys_env_sleep>
			return 0;
f0103978:	b8 00 00 00 00       	mov    $0x0,%eax
f010397d:	e9 bc 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_calc_req_frames:
			return sys_calculate_required_frames(a1, a2);			
f0103982:	83 ec 08             	sub    $0x8,%esp
f0103985:	ff 75 10             	pushl  0x10(%ebp)
f0103988:	ff 75 0c             	pushl  0xc(%ebp)
f010398b:	e8 2f ff ff ff       	call   f01038bf <sys_calculate_required_frames>
f0103990:	83 c4 10             	add    $0x10,%esp
f0103993:	e9 a6 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_calc_free_frames:
			return sys_calculate_free_frames();			
f0103998:	e8 44 ff ff ff       	call   f01038e1 <sys_calculate_free_frames>
f010399d:	e9 9c 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_freeMem:
			sys_freeMem((void*)a1, a2);
f01039a2:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039a5:	83 ec 08             	sub    $0x8,%esp
f01039a8:	ff 75 10             	pushl  0x10(%ebp)
f01039ab:	50                   	push   %eax
f01039ac:	e8 3d ff ff ff       	call   f01038ee <sys_freeMem>
f01039b1:	83 c4 10             	add    $0x10,%esp
			return 0;			
f01039b4:	b8 00 00 00 00       	mov    $0x0,%eax
f01039b9:	e9 80 00 00 00       	jmp    f0103a3e <syscall+0x12d>
			break;
		//======================
		
		case SYS_allocate_page:
			sys_allocate_page((void*)a1, a2);
f01039be:	8b 55 10             	mov    0x10(%ebp),%edx
f01039c1:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039c4:	83 ec 08             	sub    $0x8,%esp
f01039c7:	52                   	push   %edx
f01039c8:	50                   	push   %eax
f01039c9:	e8 99 fd ff ff       	call   f0103767 <sys_allocate_page>
f01039ce:	83 c4 10             	add    $0x10,%esp
			return 0;
f01039d1:	b8 00 00 00 00       	mov    $0x0,%eax
f01039d6:	eb 66                	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_get_page:
			sys_get_page((void*)a1, a2);
f01039d8:	8b 55 10             	mov    0x10(%ebp),%edx
f01039db:	8b 45 0c             	mov    0xc(%ebp),%eax
f01039de:	83 ec 08             	sub    $0x8,%esp
f01039e1:	52                   	push   %edx
f01039e2:	50                   	push   %eax
f01039e3:	e8 7b fe ff ff       	call   f0103863 <sys_get_page>
f01039e8:	83 c4 10             	add    $0x10,%esp
			return 0;
f01039eb:	b8 00 00 00 00       	mov    $0x0,%eax
f01039f0:	eb 4c                	jmp    f0103a3e <syscall+0x12d>
		break;case SYS_map_frame:
			sys_map_frame(a1, (void*)a2, a3, (void*)a4, a5);
f01039f2:	8b 75 1c             	mov    0x1c(%ebp),%esi
f01039f5:	8b 5d 18             	mov    0x18(%ebp),%ebx
f01039f8:	8b 4d 14             	mov    0x14(%ebp),%ecx
f01039fb:	8b 55 10             	mov    0x10(%ebp),%edx
f01039fe:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103a01:	83 ec 0c             	sub    $0xc,%esp
f0103a04:	56                   	push   %esi
f0103a05:	53                   	push   %ebx
f0103a06:	51                   	push   %ecx
f0103a07:	52                   	push   %edx
f0103a08:	50                   	push   %eax
f0103a09:	e8 77 fe ff ff       	call   f0103885 <sys_map_frame>
f0103a0e:	83 c4 20             	add    $0x20,%esp
			return 0;
f0103a11:	b8 00 00 00 00       	mov    $0x0,%eax
f0103a16:	eb 26                	jmp    f0103a3e <syscall+0x12d>
			break;
		case SYS_unmap_frame:
			sys_unmap_frame(a1, (void*)a2);
f0103a18:	8b 55 10             	mov    0x10(%ebp),%edx
f0103a1b:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103a1e:	83 ec 08             	sub    $0x8,%esp
f0103a21:	52                   	push   %edx
f0103a22:	50                   	push   %eax
f0103a23:	e8 7a fe ff ff       	call   f01038a2 <sys_unmap_frame>
f0103a28:	83 c4 10             	add    $0x10,%esp
			return 0;
f0103a2b:	b8 00 00 00 00       	mov    $0x0,%eax
f0103a30:	eb 0c                	jmp    f0103a3e <syscall+0x12d>
			break;
		case NSYSCALLS:	
			return 	-E_INVAL;
f0103a32:	b8 03 00 00 00       	mov    $0x3,%eax
f0103a37:	eb 05                	jmp    f0103a3e <syscall+0x12d>
			break;
	}
	//panic("syscall not implemented");
	return -E_INVAL;
f0103a39:	b8 03 00 00 00       	mov    $0x3,%eax
}
f0103a3e:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0103a41:	5b                   	pop    %ebx
f0103a42:	5e                   	pop    %esi
f0103a43:	5d                   	pop    %ebp
f0103a44:	c3                   	ret    

f0103a45 <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uint32*  addr)
{
f0103a45:	55                   	push   %ebp
f0103a46:	89 e5                	mov    %esp,%ebp
f0103a48:	83 ec 20             	sub    $0x20,%esp
	int l = *region_left, r = *region_right, any_matches = 0;
f0103a4b:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103a4e:	8b 00                	mov    (%eax),%eax
f0103a50:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103a53:	8b 45 10             	mov    0x10(%ebp),%eax
f0103a56:	8b 00                	mov    (%eax),%eax
f0103a58:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0103a5b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	
	while (l <= r) {
f0103a62:	e9 d2 00 00 00       	jmp    f0103b39 <stab_binsearch+0xf4>
		int true_m = (l + r) / 2, m = true_m;
f0103a67:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103a6a:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0103a6d:	01 d0                	add    %edx,%eax
f0103a6f:	89 c2                	mov    %eax,%edx
f0103a71:	c1 ea 1f             	shr    $0x1f,%edx
f0103a74:	01 d0                	add    %edx,%eax
f0103a76:	d1 f8                	sar    %eax
f0103a78:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0103a7b:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103a7e:	89 45 f0             	mov    %eax,-0x10(%ebp)
		
		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103a81:	eb 04                	jmp    f0103a87 <stab_binsearch+0x42>
			m--;
f0103a83:	83 6d f0 01          	subl   $0x1,-0x10(%ebp)
	
	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;
		
		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f0103a87:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103a8a:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103a8d:	7c 1f                	jl     f0103aae <stab_binsearch+0x69>
f0103a8f:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103a92:	89 d0                	mov    %edx,%eax
f0103a94:	01 c0                	add    %eax,%eax
f0103a96:	01 d0                	add    %edx,%eax
f0103a98:	c1 e0 02             	shl    $0x2,%eax
f0103a9b:	89 c2                	mov    %eax,%edx
f0103a9d:	8b 45 08             	mov    0x8(%ebp),%eax
f0103aa0:	01 d0                	add    %edx,%eax
f0103aa2:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103aa6:	0f b6 c0             	movzbl %al,%eax
f0103aa9:	3b 45 14             	cmp    0x14(%ebp),%eax
f0103aac:	75 d5                	jne    f0103a83 <stab_binsearch+0x3e>
			m--;
		if (m < l) {	// no match in [l, m]
f0103aae:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103ab1:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103ab4:	7d 0b                	jge    f0103ac1 <stab_binsearch+0x7c>
			l = true_m + 1;
f0103ab6:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103ab9:	83 c0 01             	add    $0x1,%eax
f0103abc:	89 45 fc             	mov    %eax,-0x4(%ebp)
			continue;
f0103abf:	eb 78                	jmp    f0103b39 <stab_binsearch+0xf4>
		}

		// actual binary search
		any_matches = 1;
f0103ac1:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
		if (stabs[m].n_value < addr) {
f0103ac8:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103acb:	89 d0                	mov    %edx,%eax
f0103acd:	01 c0                	add    %eax,%eax
f0103acf:	01 d0                	add    %edx,%eax
f0103ad1:	c1 e0 02             	shl    $0x2,%eax
f0103ad4:	89 c2                	mov    %eax,%edx
f0103ad6:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ad9:	01 d0                	add    %edx,%eax
f0103adb:	8b 40 08             	mov    0x8(%eax),%eax
f0103ade:	3b 45 18             	cmp    0x18(%ebp),%eax
f0103ae1:	73 13                	jae    f0103af6 <stab_binsearch+0xb1>
			*region_left = m;
f0103ae3:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ae6:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103ae9:	89 10                	mov    %edx,(%eax)
			l = true_m + 1;
f0103aeb:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103aee:	83 c0 01             	add    $0x1,%eax
f0103af1:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103af4:	eb 43                	jmp    f0103b39 <stab_binsearch+0xf4>
		} else if (stabs[m].n_value > addr) {
f0103af6:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103af9:	89 d0                	mov    %edx,%eax
f0103afb:	01 c0                	add    %eax,%eax
f0103afd:	01 d0                	add    %edx,%eax
f0103aff:	c1 e0 02             	shl    $0x2,%eax
f0103b02:	89 c2                	mov    %eax,%edx
f0103b04:	8b 45 08             	mov    0x8(%ebp),%eax
f0103b07:	01 d0                	add    %edx,%eax
f0103b09:	8b 40 08             	mov    0x8(%eax),%eax
f0103b0c:	3b 45 18             	cmp    0x18(%ebp),%eax
f0103b0f:	76 16                	jbe    f0103b27 <stab_binsearch+0xe2>
			*region_right = m - 1;
f0103b11:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103b14:	8d 50 ff             	lea    -0x1(%eax),%edx
f0103b17:	8b 45 10             	mov    0x10(%ebp),%eax
f0103b1a:	89 10                	mov    %edx,(%eax)
			r = m - 1;
f0103b1c:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103b1f:	83 e8 01             	sub    $0x1,%eax
f0103b22:	89 45 f8             	mov    %eax,-0x8(%ebp)
f0103b25:	eb 12                	jmp    f0103b39 <stab_binsearch+0xf4>
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0103b27:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b2a:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103b2d:	89 10                	mov    %edx,(%eax)
			l = m;
f0103b2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103b32:	89 45 fc             	mov    %eax,-0x4(%ebp)
			addr++;
f0103b35:	83 45 18 04          	addl   $0x4,0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uint32*  addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;
	
	while (l <= r) {
f0103b39:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0103b3c:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0103b3f:	0f 8e 22 ff ff ff    	jle    f0103a67 <stab_binsearch+0x22>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0103b45:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0103b49:	75 0f                	jne    f0103b5a <stab_binsearch+0x115>
		*region_right = *region_left - 1;
f0103b4b:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b4e:	8b 00                	mov    (%eax),%eax
f0103b50:	8d 50 ff             	lea    -0x1(%eax),%edx
f0103b53:	8b 45 10             	mov    0x10(%ebp),%eax
f0103b56:	89 10                	mov    %edx,(%eax)
		     l > *region_left && stabs[l].n_type != type;
		     l--)
			/* do nothing */;
		*region_left = l;
	}
}
f0103b58:	eb 3f                	jmp    f0103b99 <stab_binsearch+0x154>

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103b5a:	8b 45 10             	mov    0x10(%ebp),%eax
f0103b5d:	8b 00                	mov    (%eax),%eax
f0103b5f:	89 45 fc             	mov    %eax,-0x4(%ebp)
f0103b62:	eb 04                	jmp    f0103b68 <stab_binsearch+0x123>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0103b64:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
		     l > *region_left && stabs[l].n_type != type;
f0103b68:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b6b:	8b 00                	mov    (%eax),%eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0103b6d:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f0103b70:	7d 1f                	jge    f0103b91 <stab_binsearch+0x14c>
		     l > *region_left && stabs[l].n_type != type;
f0103b72:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103b75:	89 d0                	mov    %edx,%eax
f0103b77:	01 c0                	add    %eax,%eax
f0103b79:	01 d0                	add    %edx,%eax
f0103b7b:	c1 e0 02             	shl    $0x2,%eax
f0103b7e:	89 c2                	mov    %eax,%edx
f0103b80:	8b 45 08             	mov    0x8(%ebp),%eax
f0103b83:	01 d0                	add    %edx,%eax
f0103b85:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103b89:	0f b6 c0             	movzbl %al,%eax
f0103b8c:	3b 45 14             	cmp    0x14(%ebp),%eax
f0103b8f:	75 d3                	jne    f0103b64 <stab_binsearch+0x11f>
		     l--)
			/* do nothing */;
		*region_left = l;
f0103b91:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103b94:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0103b97:	89 10                	mov    %edx,(%eax)
	}
}
f0103b99:	90                   	nop
f0103b9a:	c9                   	leave  
f0103b9b:	c3                   	ret    

f0103b9c <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uint32*  addr, struct Eipdebuginfo *info)
{
f0103b9c:	55                   	push   %ebp
f0103b9d:	89 e5                	mov    %esp,%ebp
f0103b9f:	83 ec 38             	sub    $0x38,%esp
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0103ba2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103ba5:	c7 00 78 61 10 f0    	movl   $0xf0106178,(%eax)
	info->eip_line = 0;
f0103bab:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bae:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
	info->eip_fn_name = "<unknown>";
f0103bb5:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bb8:	c7 40 08 78 61 10 f0 	movl   $0xf0106178,0x8(%eax)
	info->eip_fn_namelen = 9;
f0103bbf:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bc2:	c7 40 0c 09 00 00 00 	movl   $0x9,0xc(%eax)
	info->eip_fn_addr = addr;
f0103bc9:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bcc:	8b 55 08             	mov    0x8(%ebp),%edx
f0103bcf:	89 50 10             	mov    %edx,0x10(%eax)
	info->eip_fn_narg = 0;
f0103bd2:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103bd5:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)

	// Find the relevant set of stabs
	if ((uint32)addr >= USER_LIMIT) {
f0103bdc:	8b 45 08             	mov    0x8(%ebp),%eax
f0103bdf:	3d ff ff 7f ef       	cmp    $0xef7fffff,%eax
f0103be4:	76 1e                	jbe    f0103c04 <debuginfo_eip+0x68>
		stabs = __STAB_BEGIN__;
f0103be6:	c7 45 f4 d0 63 10 f0 	movl   $0xf01063d0,-0xc(%ebp)
		stab_end = __STAB_END__;
f0103bed:	c7 45 f0 f0 ee 10 f0 	movl   $0xf010eef0,-0x10(%ebp)
		stabstr = __STABSTR_BEGIN__;
f0103bf4:	c7 45 ec f1 ee 10 f0 	movl   $0xf010eef1,-0x14(%ebp)
		stabstr_end = __STABSTR_END__;
f0103bfb:	c7 45 e8 6c 27 11 f0 	movl   $0xf011276c,-0x18(%ebp)
f0103c02:	eb 2a                	jmp    f0103c2e <debuginfo_eip+0x92>
		// The user-application linker script, user/user.ld,
		// puts information about the application's stabs (equivalent
		// to __STAB_BEGIN__, __STAB_END__, __STABSTR_BEGIN__, and
		// __STABSTR_END__) in a structure located at virtual address
		// USTABDATA.
		const struct UserStabData *usd = (const struct UserStabData *) USTABDATA;
f0103c04:	c7 45 e0 00 00 20 00 	movl   $0x200000,-0x20(%ebp)

		// Make sure this memory is valid.
		// Return -1 if it is not.  Hint: Call user_mem_check.
		// LAB 3: Your code here.
		
		stabs = usd->stabs;
f0103c0b:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c0e:	8b 00                	mov    (%eax),%eax
f0103c10:	89 45 f4             	mov    %eax,-0xc(%ebp)
		stab_end = usd->stab_end;
f0103c13:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c16:	8b 40 04             	mov    0x4(%eax),%eax
f0103c19:	89 45 f0             	mov    %eax,-0x10(%ebp)
		stabstr = usd->stabstr;
f0103c1c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c1f:	8b 40 08             	mov    0x8(%eax),%eax
f0103c22:	89 45 ec             	mov    %eax,-0x14(%ebp)
		stabstr_end = usd->stabstr_end;
f0103c25:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0103c28:	8b 40 0c             	mov    0xc(%eax),%eax
f0103c2b:	89 45 e8             	mov    %eax,-0x18(%ebp)
		// Make sure the STABS and string table memory is valid.
		// LAB 3: Your code here.
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0103c2e:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0103c31:	3b 45 ec             	cmp    -0x14(%ebp),%eax
f0103c34:	76 0d                	jbe    f0103c43 <debuginfo_eip+0xa7>
f0103c36:	8b 45 e8             	mov    -0x18(%ebp),%eax
f0103c39:	83 e8 01             	sub    $0x1,%eax
f0103c3c:	0f b6 00             	movzbl (%eax),%eax
f0103c3f:	84 c0                	test   %al,%al
f0103c41:	74 0a                	je     f0103c4d <debuginfo_eip+0xb1>
		return -1;
f0103c43:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0103c48:	e9 e7 01 00 00       	jmp    f0103e34 <debuginfo_eip+0x298>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.
	
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0103c4d:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
	rfile = (stab_end - stabs) - 1;
f0103c54:	8b 55 f0             	mov    -0x10(%ebp),%edx
f0103c57:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103c5a:	29 c2                	sub    %eax,%edx
f0103c5c:	89 d0                	mov    %edx,%eax
f0103c5e:	c1 f8 02             	sar    $0x2,%eax
f0103c61:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
f0103c67:	83 e8 01             	sub    $0x1,%eax
f0103c6a:	89 45 d4             	mov    %eax,-0x2c(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f0103c6d:	ff 75 08             	pushl  0x8(%ebp)
f0103c70:	6a 64                	push   $0x64
f0103c72:	8d 45 d4             	lea    -0x2c(%ebp),%eax
f0103c75:	50                   	push   %eax
f0103c76:	8d 45 d8             	lea    -0x28(%ebp),%eax
f0103c79:	50                   	push   %eax
f0103c7a:	ff 75 f4             	pushl  -0xc(%ebp)
f0103c7d:	e8 c3 fd ff ff       	call   f0103a45 <stab_binsearch>
f0103c82:	83 c4 14             	add    $0x14,%esp
	if (lfile == 0)
f0103c85:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103c88:	85 c0                	test   %eax,%eax
f0103c8a:	75 0a                	jne    f0103c96 <debuginfo_eip+0xfa>
		return -1;
f0103c8c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0103c91:	e9 9e 01 00 00       	jmp    f0103e34 <debuginfo_eip+0x298>

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0103c96:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103c99:	89 45 d0             	mov    %eax,-0x30(%ebp)
	rfun = rfile;
f0103c9c:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103c9f:	89 45 cc             	mov    %eax,-0x34(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f0103ca2:	ff 75 08             	pushl  0x8(%ebp)
f0103ca5:	6a 24                	push   $0x24
f0103ca7:	8d 45 cc             	lea    -0x34(%ebp),%eax
f0103caa:	50                   	push   %eax
f0103cab:	8d 45 d0             	lea    -0x30(%ebp),%eax
f0103cae:	50                   	push   %eax
f0103caf:	ff 75 f4             	pushl  -0xc(%ebp)
f0103cb2:	e8 8e fd ff ff       	call   f0103a45 <stab_binsearch>
f0103cb7:	83 c4 14             	add    $0x14,%esp

	if (lfun <= rfun) {
f0103cba:	8b 55 d0             	mov    -0x30(%ebp),%edx
f0103cbd:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0103cc0:	39 c2                	cmp    %eax,%edx
f0103cc2:	0f 8f 86 00 00 00    	jg     f0103d4e <debuginfo_eip+0x1b2>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0103cc8:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103ccb:	89 c2                	mov    %eax,%edx
f0103ccd:	89 d0                	mov    %edx,%eax
f0103ccf:	01 c0                	add    %eax,%eax
f0103cd1:	01 d0                	add    %edx,%eax
f0103cd3:	c1 e0 02             	shl    $0x2,%eax
f0103cd6:	89 c2                	mov    %eax,%edx
f0103cd8:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103cdb:	01 d0                	add    %edx,%eax
f0103cdd:	8b 00                	mov    (%eax),%eax
f0103cdf:	8b 4d e8             	mov    -0x18(%ebp),%ecx
f0103ce2:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0103ce5:	29 d1                	sub    %edx,%ecx
f0103ce7:	89 ca                	mov    %ecx,%edx
f0103ce9:	39 d0                	cmp    %edx,%eax
f0103ceb:	73 22                	jae    f0103d0f <debuginfo_eip+0x173>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0103ced:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103cf0:	89 c2                	mov    %eax,%edx
f0103cf2:	89 d0                	mov    %edx,%eax
f0103cf4:	01 c0                	add    %eax,%eax
f0103cf6:	01 d0                	add    %edx,%eax
f0103cf8:	c1 e0 02             	shl    $0x2,%eax
f0103cfb:	89 c2                	mov    %eax,%edx
f0103cfd:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103d00:	01 d0                	add    %edx,%eax
f0103d02:	8b 10                	mov    (%eax),%edx
f0103d04:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103d07:	01 c2                	add    %eax,%edx
f0103d09:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d0c:	89 50 08             	mov    %edx,0x8(%eax)
		info->eip_fn_addr = (uint32*) stabs[lfun].n_value;
f0103d0f:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103d12:	89 c2                	mov    %eax,%edx
f0103d14:	89 d0                	mov    %edx,%eax
f0103d16:	01 c0                	add    %eax,%eax
f0103d18:	01 d0                	add    %edx,%eax
f0103d1a:	c1 e0 02             	shl    $0x2,%eax
f0103d1d:	89 c2                	mov    %eax,%edx
f0103d1f:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103d22:	01 d0                	add    %edx,%eax
f0103d24:	8b 50 08             	mov    0x8(%eax),%edx
f0103d27:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d2a:	89 50 10             	mov    %edx,0x10(%eax)
		addr = (uint32*)(addr - (info->eip_fn_addr));
f0103d2d:	8b 55 08             	mov    0x8(%ebp),%edx
f0103d30:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d33:	8b 40 10             	mov    0x10(%eax),%eax
f0103d36:	29 c2                	sub    %eax,%edx
f0103d38:	89 d0                	mov    %edx,%eax
f0103d3a:	c1 f8 02             	sar    $0x2,%eax
f0103d3d:	89 45 08             	mov    %eax,0x8(%ebp)
		// Search within the function definition for the line number.
		lline = lfun;
f0103d40:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0103d43:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		rline = rfun;
f0103d46:	8b 45 cc             	mov    -0x34(%ebp),%eax
f0103d49:	89 45 dc             	mov    %eax,-0x24(%ebp)
f0103d4c:	eb 15                	jmp    f0103d63 <debuginfo_eip+0x1c7>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0103d4e:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d51:	8b 55 08             	mov    0x8(%ebp),%edx
f0103d54:	89 50 10             	mov    %edx,0x10(%eax)
		lline = lfile;
f0103d57:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103d5a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
		rline = rfile;
f0103d5d:	8b 45 d4             	mov    -0x2c(%ebp),%eax
f0103d60:	89 45 dc             	mov    %eax,-0x24(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0103d63:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d66:	8b 40 08             	mov    0x8(%eax),%eax
f0103d69:	83 ec 08             	sub    $0x8,%esp
f0103d6c:	6a 3a                	push   $0x3a
f0103d6e:	50                   	push   %eax
f0103d6f:	e8 db 09 00 00       	call   f010474f <strfind>
f0103d74:	83 c4 10             	add    $0x10,%esp
f0103d77:	89 c2                	mov    %eax,%edx
f0103d79:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d7c:	8b 40 08             	mov    0x8(%eax),%eax
f0103d7f:	29 c2                	sub    %eax,%edx
f0103d81:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103d84:	89 50 0c             	mov    %edx,0xc(%eax)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0103d87:	eb 04                	jmp    f0103d8d <debuginfo_eip+0x1f1>
	       && stabs[lline].n_type != N_SOL
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
		lline--;
f0103d89:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0103d8d:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103d90:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f0103d93:	7c 50                	jl     f0103de5 <debuginfo_eip+0x249>
	       && stabs[lline].n_type != N_SOL
f0103d95:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103d98:	89 d0                	mov    %edx,%eax
f0103d9a:	01 c0                	add    %eax,%eax
f0103d9c:	01 d0                	add    %edx,%eax
f0103d9e:	c1 e0 02             	shl    $0x2,%eax
f0103da1:	89 c2                	mov    %eax,%edx
f0103da3:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103da6:	01 d0                	add    %edx,%eax
f0103da8:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103dac:	3c 84                	cmp    $0x84,%al
f0103dae:	74 35                	je     f0103de5 <debuginfo_eip+0x249>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f0103db0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103db3:	89 d0                	mov    %edx,%eax
f0103db5:	01 c0                	add    %eax,%eax
f0103db7:	01 d0                	add    %edx,%eax
f0103db9:	c1 e0 02             	shl    $0x2,%eax
f0103dbc:	89 c2                	mov    %eax,%edx
f0103dbe:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103dc1:	01 d0                	add    %edx,%eax
f0103dc3:	0f b6 40 04          	movzbl 0x4(%eax),%eax
f0103dc7:	3c 64                	cmp    $0x64,%al
f0103dc9:	75 be                	jne    f0103d89 <debuginfo_eip+0x1ed>
f0103dcb:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103dce:	89 d0                	mov    %edx,%eax
f0103dd0:	01 c0                	add    %eax,%eax
f0103dd2:	01 d0                	add    %edx,%eax
f0103dd4:	c1 e0 02             	shl    $0x2,%eax
f0103dd7:	89 c2                	mov    %eax,%edx
f0103dd9:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103ddc:	01 d0                	add    %edx,%eax
f0103dde:	8b 40 08             	mov    0x8(%eax),%eax
f0103de1:	85 c0                	test   %eax,%eax
f0103de3:	74 a4                	je     f0103d89 <debuginfo_eip+0x1ed>
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f0103de5:	8b 45 d8             	mov    -0x28(%ebp),%eax
f0103de8:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
f0103deb:	7c 42                	jl     f0103e2f <debuginfo_eip+0x293>
f0103ded:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103df0:	89 d0                	mov    %edx,%eax
f0103df2:	01 c0                	add    %eax,%eax
f0103df4:	01 d0                	add    %edx,%eax
f0103df6:	c1 e0 02             	shl    $0x2,%eax
f0103df9:	89 c2                	mov    %eax,%edx
f0103dfb:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103dfe:	01 d0                	add    %edx,%eax
f0103e00:	8b 00                	mov    (%eax),%eax
f0103e02:	8b 4d e8             	mov    -0x18(%ebp),%ecx
f0103e05:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0103e08:	29 d1                	sub    %edx,%ecx
f0103e0a:	89 ca                	mov    %ecx,%edx
f0103e0c:	39 d0                	cmp    %edx,%eax
f0103e0e:	73 1f                	jae    f0103e2f <debuginfo_eip+0x293>
		info->eip_file = stabstr + stabs[lline].n_strx;
f0103e10:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0103e13:	89 d0                	mov    %edx,%eax
f0103e15:	01 c0                	add    %eax,%eax
f0103e17:	01 d0                	add    %edx,%eax
f0103e19:	c1 e0 02             	shl    $0x2,%eax
f0103e1c:	89 c2                	mov    %eax,%edx
f0103e1e:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0103e21:	01 d0                	add    %edx,%eax
f0103e23:	8b 10                	mov    (%eax),%edx
f0103e25:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0103e28:	01 c2                	add    %eax,%edx
f0103e2a:	8b 45 0c             	mov    0xc(%ebp),%eax
f0103e2d:	89 10                	mov    %edx,(%eax)
	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	// Your code here.

	
	return 0;
f0103e2f:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0103e34:	c9                   	leave  
f0103e35:	c3                   	ret    

f0103e36 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f0103e36:	55                   	push   %ebp
f0103e37:	89 e5                	mov    %esp,%ebp
f0103e39:	53                   	push   %ebx
f0103e3a:	83 ec 14             	sub    $0x14,%esp
f0103e3d:	8b 45 10             	mov    0x10(%ebp),%eax
f0103e40:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0103e43:	8b 45 14             	mov    0x14(%ebp),%eax
f0103e46:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f0103e49:	8b 45 18             	mov    0x18(%ebp),%eax
f0103e4c:	ba 00 00 00 00       	mov    $0x0,%edx
f0103e51:	3b 55 f4             	cmp    -0xc(%ebp),%edx
f0103e54:	77 55                	ja     f0103eab <printnum+0x75>
f0103e56:	3b 55 f4             	cmp    -0xc(%ebp),%edx
f0103e59:	72 05                	jb     f0103e60 <printnum+0x2a>
f0103e5b:	3b 45 f0             	cmp    -0x10(%ebp),%eax
f0103e5e:	77 4b                	ja     f0103eab <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f0103e60:	8b 45 1c             	mov    0x1c(%ebp),%eax
f0103e63:	8d 58 ff             	lea    -0x1(%eax),%ebx
f0103e66:	8b 45 18             	mov    0x18(%ebp),%eax
f0103e69:	ba 00 00 00 00       	mov    $0x0,%edx
f0103e6e:	52                   	push   %edx
f0103e6f:	50                   	push   %eax
f0103e70:	ff 75 f4             	pushl  -0xc(%ebp)
f0103e73:	ff 75 f0             	pushl  -0x10(%ebp)
f0103e76:	e8 c5 0c 00 00       	call   f0104b40 <__udivdi3>
f0103e7b:	83 c4 10             	add    $0x10,%esp
f0103e7e:	83 ec 04             	sub    $0x4,%esp
f0103e81:	ff 75 20             	pushl  0x20(%ebp)
f0103e84:	53                   	push   %ebx
f0103e85:	ff 75 18             	pushl  0x18(%ebp)
f0103e88:	52                   	push   %edx
f0103e89:	50                   	push   %eax
f0103e8a:	ff 75 0c             	pushl  0xc(%ebp)
f0103e8d:	ff 75 08             	pushl  0x8(%ebp)
f0103e90:	e8 a1 ff ff ff       	call   f0103e36 <printnum>
f0103e95:	83 c4 20             	add    $0x20,%esp
f0103e98:	eb 1b                	jmp    f0103eb5 <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0103e9a:	83 ec 08             	sub    $0x8,%esp
f0103e9d:	ff 75 0c             	pushl  0xc(%ebp)
f0103ea0:	ff 75 20             	pushl  0x20(%ebp)
f0103ea3:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ea6:	ff d0                	call   *%eax
f0103ea8:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0103eab:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
f0103eaf:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
f0103eb3:	7f e5                	jg     f0103e9a <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0103eb5:	8b 4d 18             	mov    0x18(%ebp),%ecx
f0103eb8:	bb 00 00 00 00       	mov    $0x0,%ebx
f0103ebd:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0103ec0:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0103ec3:	53                   	push   %ebx
f0103ec4:	51                   	push   %ecx
f0103ec5:	52                   	push   %edx
f0103ec6:	50                   	push   %eax
f0103ec7:	e8 a4 0d 00 00       	call   f0104c70 <__umoddi3>
f0103ecc:	83 c4 10             	add    $0x10,%esp
f0103ecf:	05 40 62 10 f0       	add    $0xf0106240,%eax
f0103ed4:	0f b6 00             	movzbl (%eax),%eax
f0103ed7:	0f be c0             	movsbl %al,%eax
f0103eda:	83 ec 08             	sub    $0x8,%esp
f0103edd:	ff 75 0c             	pushl  0xc(%ebp)
f0103ee0:	50                   	push   %eax
f0103ee1:	8b 45 08             	mov    0x8(%ebp),%eax
f0103ee4:	ff d0                	call   *%eax
f0103ee6:	83 c4 10             	add    $0x10,%esp
}
f0103ee9:	90                   	nop
f0103eea:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0103eed:	c9                   	leave  
f0103eee:	c3                   	ret    

f0103eef <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0103eef:	55                   	push   %ebp
f0103ef0:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0103ef2:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
f0103ef6:	7e 1c                	jle    f0103f14 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
f0103ef8:	8b 45 08             	mov    0x8(%ebp),%eax
f0103efb:	8b 00                	mov    (%eax),%eax
f0103efd:	8d 50 08             	lea    0x8(%eax),%edx
f0103f00:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f03:	89 10                	mov    %edx,(%eax)
f0103f05:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f08:	8b 00                	mov    (%eax),%eax
f0103f0a:	83 e8 08             	sub    $0x8,%eax
f0103f0d:	8b 50 04             	mov    0x4(%eax),%edx
f0103f10:	8b 00                	mov    (%eax),%eax
f0103f12:	eb 40                	jmp    f0103f54 <getuint+0x65>
	else if (lflag)
f0103f14:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0103f18:	74 1e                	je     f0103f38 <getuint+0x49>
		return va_arg(*ap, unsigned long);
f0103f1a:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f1d:	8b 00                	mov    (%eax),%eax
f0103f1f:	8d 50 04             	lea    0x4(%eax),%edx
f0103f22:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f25:	89 10                	mov    %edx,(%eax)
f0103f27:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f2a:	8b 00                	mov    (%eax),%eax
f0103f2c:	83 e8 04             	sub    $0x4,%eax
f0103f2f:	8b 00                	mov    (%eax),%eax
f0103f31:	ba 00 00 00 00       	mov    $0x0,%edx
f0103f36:	eb 1c                	jmp    f0103f54 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
f0103f38:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f3b:	8b 00                	mov    (%eax),%eax
f0103f3d:	8d 50 04             	lea    0x4(%eax),%edx
f0103f40:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f43:	89 10                	mov    %edx,(%eax)
f0103f45:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f48:	8b 00                	mov    (%eax),%eax
f0103f4a:	83 e8 04             	sub    $0x4,%eax
f0103f4d:	8b 00                	mov    (%eax),%eax
f0103f4f:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0103f54:	5d                   	pop    %ebp
f0103f55:	c3                   	ret    

f0103f56 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
f0103f56:	55                   	push   %ebp
f0103f57:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0103f59:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
f0103f5d:	7e 1c                	jle    f0103f7b <getint+0x25>
		return va_arg(*ap, long long);
f0103f5f:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f62:	8b 00                	mov    (%eax),%eax
f0103f64:	8d 50 08             	lea    0x8(%eax),%edx
f0103f67:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f6a:	89 10                	mov    %edx,(%eax)
f0103f6c:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f6f:	8b 00                	mov    (%eax),%eax
f0103f71:	83 e8 08             	sub    $0x8,%eax
f0103f74:	8b 50 04             	mov    0x4(%eax),%edx
f0103f77:	8b 00                	mov    (%eax),%eax
f0103f79:	eb 38                	jmp    f0103fb3 <getint+0x5d>
	else if (lflag)
f0103f7b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0103f7f:	74 1a                	je     f0103f9b <getint+0x45>
		return va_arg(*ap, long);
f0103f81:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f84:	8b 00                	mov    (%eax),%eax
f0103f86:	8d 50 04             	lea    0x4(%eax),%edx
f0103f89:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f8c:	89 10                	mov    %edx,(%eax)
f0103f8e:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f91:	8b 00                	mov    (%eax),%eax
f0103f93:	83 e8 04             	sub    $0x4,%eax
f0103f96:	8b 00                	mov    (%eax),%eax
f0103f98:	99                   	cltd   
f0103f99:	eb 18                	jmp    f0103fb3 <getint+0x5d>
	else
		return va_arg(*ap, int);
f0103f9b:	8b 45 08             	mov    0x8(%ebp),%eax
f0103f9e:	8b 00                	mov    (%eax),%eax
f0103fa0:	8d 50 04             	lea    0x4(%eax),%edx
f0103fa3:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fa6:	89 10                	mov    %edx,(%eax)
f0103fa8:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fab:	8b 00                	mov    (%eax),%eax
f0103fad:	83 e8 04             	sub    $0x4,%eax
f0103fb0:	8b 00                	mov    (%eax),%eax
f0103fb2:	99                   	cltd   
}
f0103fb3:	5d                   	pop    %ebp
f0103fb4:	c3                   	ret    

f0103fb5 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f0103fb5:	55                   	push   %ebp
f0103fb6:	89 e5                	mov    %esp,%ebp
f0103fb8:	56                   	push   %esi
f0103fb9:	53                   	push   %ebx
f0103fba:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0103fbd:	eb 17                	jmp    f0103fd6 <vprintfmt+0x21>
			if (ch == '\0')
f0103fbf:	85 db                	test   %ebx,%ebx
f0103fc1:	0f 84 be 03 00 00    	je     f0104385 <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
f0103fc7:	83 ec 08             	sub    $0x8,%esp
f0103fca:	ff 75 0c             	pushl  0xc(%ebp)
f0103fcd:	53                   	push   %ebx
f0103fce:	8b 45 08             	mov    0x8(%ebp),%eax
f0103fd1:	ff d0                	call   *%eax
f0103fd3:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0103fd6:	8b 45 10             	mov    0x10(%ebp),%eax
f0103fd9:	8d 50 01             	lea    0x1(%eax),%edx
f0103fdc:	89 55 10             	mov    %edx,0x10(%ebp)
f0103fdf:	0f b6 00             	movzbl (%eax),%eax
f0103fe2:	0f b6 d8             	movzbl %al,%ebx
f0103fe5:	83 fb 25             	cmp    $0x25,%ebx
f0103fe8:	75 d5                	jne    f0103fbf <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
f0103fea:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
f0103fee:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
f0103ff5:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
f0103ffc:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
f0104003:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010400a:	8b 45 10             	mov    0x10(%ebp),%eax
f010400d:	8d 50 01             	lea    0x1(%eax),%edx
f0104010:	89 55 10             	mov    %edx,0x10(%ebp)
f0104013:	0f b6 00             	movzbl (%eax),%eax
f0104016:	0f b6 d8             	movzbl %al,%ebx
f0104019:	8d 43 dd             	lea    -0x23(%ebx),%eax
f010401c:	83 f8 55             	cmp    $0x55,%eax
f010401f:	0f 87 33 03 00 00    	ja     f0104358 <vprintfmt+0x3a3>
f0104025:	8b 04 85 64 62 10 f0 	mov    -0xfef9d9c(,%eax,4),%eax
f010402c:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
f010402e:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
f0104032:	eb d6                	jmp    f010400a <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0104034:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
f0104038:	eb d0                	jmp    f010400a <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f010403a:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
f0104041:	8b 55 e0             	mov    -0x20(%ebp),%edx
f0104044:	89 d0                	mov    %edx,%eax
f0104046:	c1 e0 02             	shl    $0x2,%eax
f0104049:	01 d0                	add    %edx,%eax
f010404b:	01 c0                	add    %eax,%eax
f010404d:	01 d8                	add    %ebx,%eax
f010404f:	83 e8 30             	sub    $0x30,%eax
f0104052:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
f0104055:	8b 45 10             	mov    0x10(%ebp),%eax
f0104058:	0f b6 00             	movzbl (%eax),%eax
f010405b:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
f010405e:	83 fb 2f             	cmp    $0x2f,%ebx
f0104061:	7e 3f                	jle    f01040a2 <vprintfmt+0xed>
f0104063:	83 fb 39             	cmp    $0x39,%ebx
f0104066:	7f 3a                	jg     f01040a2 <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0104068:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f010406c:	eb d3                	jmp    f0104041 <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f010406e:	8b 45 14             	mov    0x14(%ebp),%eax
f0104071:	83 c0 04             	add    $0x4,%eax
f0104074:	89 45 14             	mov    %eax,0x14(%ebp)
f0104077:	8b 45 14             	mov    0x14(%ebp),%eax
f010407a:	83 e8 04             	sub    $0x4,%eax
f010407d:	8b 00                	mov    (%eax),%eax
f010407f:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
f0104082:	eb 1f                	jmp    f01040a3 <vprintfmt+0xee>

		case '.':
			if (width < 0)
f0104084:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104088:	79 80                	jns    f010400a <vprintfmt+0x55>
				width = 0;
f010408a:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
f0104091:	e9 74 ff ff ff       	jmp    f010400a <vprintfmt+0x55>

		case '#':
			altflag = 1;
f0104096:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
f010409d:	e9 68 ff ff ff       	jmp    f010400a <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
f01040a2:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
f01040a3:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f01040a7:	0f 89 5d ff ff ff    	jns    f010400a <vprintfmt+0x55>
				width = precision, precision = -1;
f01040ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
f01040b0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f01040b3:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
f01040ba:	e9 4b ff ff ff       	jmp    f010400a <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f01040bf:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
f01040c3:	e9 42 ff ff ff       	jmp    f010400a <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f01040c8:	8b 45 14             	mov    0x14(%ebp),%eax
f01040cb:	83 c0 04             	add    $0x4,%eax
f01040ce:	89 45 14             	mov    %eax,0x14(%ebp)
f01040d1:	8b 45 14             	mov    0x14(%ebp),%eax
f01040d4:	83 e8 04             	sub    $0x4,%eax
f01040d7:	8b 00                	mov    (%eax),%eax
f01040d9:	83 ec 08             	sub    $0x8,%esp
f01040dc:	ff 75 0c             	pushl  0xc(%ebp)
f01040df:	50                   	push   %eax
f01040e0:	8b 45 08             	mov    0x8(%ebp),%eax
f01040e3:	ff d0                	call   *%eax
f01040e5:	83 c4 10             	add    $0x10,%esp
			break;
f01040e8:	e9 93 02 00 00       	jmp    f0104380 <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
f01040ed:	8b 45 14             	mov    0x14(%ebp),%eax
f01040f0:	83 c0 04             	add    $0x4,%eax
f01040f3:	89 45 14             	mov    %eax,0x14(%ebp)
f01040f6:	8b 45 14             	mov    0x14(%ebp),%eax
f01040f9:	83 e8 04             	sub    $0x4,%eax
f01040fc:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
f01040fe:	85 db                	test   %ebx,%ebx
f0104100:	79 02                	jns    f0104104 <vprintfmt+0x14f>
				err = -err;
f0104102:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
f0104104:	83 fb 07             	cmp    $0x7,%ebx
f0104107:	7f 0b                	jg     f0104114 <vprintfmt+0x15f>
f0104109:	8b 34 9d 20 62 10 f0 	mov    -0xfef9de0(,%ebx,4),%esi
f0104110:	85 f6                	test   %esi,%esi
f0104112:	75 19                	jne    f010412d <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
f0104114:	53                   	push   %ebx
f0104115:	68 51 62 10 f0       	push   $0xf0106251
f010411a:	ff 75 0c             	pushl  0xc(%ebp)
f010411d:	ff 75 08             	pushl  0x8(%ebp)
f0104120:	e8 68 02 00 00       	call   f010438d <printfmt>
f0104125:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
f0104128:	e9 53 02 00 00       	jmp    f0104380 <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
f010412d:	56                   	push   %esi
f010412e:	68 5a 62 10 f0       	push   $0xf010625a
f0104133:	ff 75 0c             	pushl  0xc(%ebp)
f0104136:	ff 75 08             	pushl  0x8(%ebp)
f0104139:	e8 4f 02 00 00       	call   f010438d <printfmt>
f010413e:	83 c4 10             	add    $0x10,%esp
			break;
f0104141:	e9 3a 02 00 00       	jmp    f0104380 <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0104146:	8b 45 14             	mov    0x14(%ebp),%eax
f0104149:	83 c0 04             	add    $0x4,%eax
f010414c:	89 45 14             	mov    %eax,0x14(%ebp)
f010414f:	8b 45 14             	mov    0x14(%ebp),%eax
f0104152:	83 e8 04             	sub    $0x4,%eax
f0104155:	8b 30                	mov    (%eax),%esi
f0104157:	85 f6                	test   %esi,%esi
f0104159:	75 05                	jne    f0104160 <vprintfmt+0x1ab>
				p = "(null)";
f010415b:	be 5d 62 10 f0       	mov    $0xf010625d,%esi
			if (width > 0 && padc != '-')
f0104160:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f0104164:	7e 6f                	jle    f01041d5 <vprintfmt+0x220>
f0104166:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
f010416a:	74 69                	je     f01041d5 <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
f010416c:	8b 45 e0             	mov    -0x20(%ebp),%eax
f010416f:	83 ec 08             	sub    $0x8,%esp
f0104172:	50                   	push   %eax
f0104173:	56                   	push   %esi
f0104174:	e8 18 04 00 00       	call   f0104591 <strnlen>
f0104179:	83 c4 10             	add    $0x10,%esp
f010417c:	29 45 e4             	sub    %eax,-0x1c(%ebp)
f010417f:	eb 17                	jmp    f0104198 <vprintfmt+0x1e3>
					putch(padc, putdat);
f0104181:	0f be 45 db          	movsbl -0x25(%ebp),%eax
f0104185:	83 ec 08             	sub    $0x8,%esp
f0104188:	ff 75 0c             	pushl  0xc(%ebp)
f010418b:	50                   	push   %eax
f010418c:	8b 45 08             	mov    0x8(%ebp),%eax
f010418f:	ff d0                	call   *%eax
f0104191:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0104194:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f0104198:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f010419c:	7f e3                	jg     f0104181 <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f010419e:	eb 35                	jmp    f01041d5 <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
f01041a0:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f01041a4:	74 1c                	je     f01041c2 <vprintfmt+0x20d>
f01041a6:	83 fb 1f             	cmp    $0x1f,%ebx
f01041a9:	7e 05                	jle    f01041b0 <vprintfmt+0x1fb>
f01041ab:	83 fb 7e             	cmp    $0x7e,%ebx
f01041ae:	7e 12                	jle    f01041c2 <vprintfmt+0x20d>
					putch('?', putdat);
f01041b0:	83 ec 08             	sub    $0x8,%esp
f01041b3:	ff 75 0c             	pushl  0xc(%ebp)
f01041b6:	6a 3f                	push   $0x3f
f01041b8:	8b 45 08             	mov    0x8(%ebp),%eax
f01041bb:	ff d0                	call   *%eax
f01041bd:	83 c4 10             	add    $0x10,%esp
f01041c0:	eb 0f                	jmp    f01041d1 <vprintfmt+0x21c>
				else
					putch(ch, putdat);
f01041c2:	83 ec 08             	sub    $0x8,%esp
f01041c5:	ff 75 0c             	pushl  0xc(%ebp)
f01041c8:	53                   	push   %ebx
f01041c9:	8b 45 08             	mov    0x8(%ebp),%eax
f01041cc:	ff d0                	call   *%eax
f01041ce:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f01041d1:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f01041d5:	89 f0                	mov    %esi,%eax
f01041d7:	8d 70 01             	lea    0x1(%eax),%esi
f01041da:	0f b6 00             	movzbl (%eax),%eax
f01041dd:	0f be d8             	movsbl %al,%ebx
f01041e0:	85 db                	test   %ebx,%ebx
f01041e2:	74 26                	je     f010420a <vprintfmt+0x255>
f01041e4:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f01041e8:	78 b6                	js     f01041a0 <vprintfmt+0x1eb>
f01041ea:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
f01041ee:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f01041f2:	79 ac                	jns    f01041a0 <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f01041f4:	eb 14                	jmp    f010420a <vprintfmt+0x255>
				putch(' ', putdat);
f01041f6:	83 ec 08             	sub    $0x8,%esp
f01041f9:	ff 75 0c             	pushl  0xc(%ebp)
f01041fc:	6a 20                	push   $0x20
f01041fe:	8b 45 08             	mov    0x8(%ebp),%eax
f0104201:	ff d0                	call   *%eax
f0104203:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0104206:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
f010420a:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
f010420e:	7f e6                	jg     f01041f6 <vprintfmt+0x241>
				putch(' ', putdat);
			break;
f0104210:	e9 6b 01 00 00       	jmp    f0104380 <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f0104215:	83 ec 08             	sub    $0x8,%esp
f0104218:	ff 75 e8             	pushl  -0x18(%ebp)
f010421b:	8d 45 14             	lea    0x14(%ebp),%eax
f010421e:	50                   	push   %eax
f010421f:	e8 32 fd ff ff       	call   f0103f56 <getint>
f0104224:	83 c4 10             	add    $0x10,%esp
f0104227:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010422a:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
f010422d:	8b 45 f0             	mov    -0x10(%ebp),%eax
f0104230:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0104233:	85 d2                	test   %edx,%edx
f0104235:	79 23                	jns    f010425a <vprintfmt+0x2a5>
				putch('-', putdat);
f0104237:	83 ec 08             	sub    $0x8,%esp
f010423a:	ff 75 0c             	pushl  0xc(%ebp)
f010423d:	6a 2d                	push   $0x2d
f010423f:	8b 45 08             	mov    0x8(%ebp),%eax
f0104242:	ff d0                	call   *%eax
f0104244:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
f0104247:	8b 45 f0             	mov    -0x10(%ebp),%eax
f010424a:	8b 55 f4             	mov    -0xc(%ebp),%edx
f010424d:	f7 d8                	neg    %eax
f010424f:	83 d2 00             	adc    $0x0,%edx
f0104252:	f7 da                	neg    %edx
f0104254:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104257:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
f010425a:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
f0104261:	e9 bc 00 00 00       	jmp    f0104322 <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f0104266:	83 ec 08             	sub    $0x8,%esp
f0104269:	ff 75 e8             	pushl  -0x18(%ebp)
f010426c:	8d 45 14             	lea    0x14(%ebp),%eax
f010426f:	50                   	push   %eax
f0104270:	e8 7a fc ff ff       	call   f0103eef <getuint>
f0104275:	83 c4 10             	add    $0x10,%esp
f0104278:	89 45 f0             	mov    %eax,-0x10(%ebp)
f010427b:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
f010427e:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
f0104285:	e9 98 00 00 00       	jmp    f0104322 <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
f010428a:	83 ec 08             	sub    $0x8,%esp
f010428d:	ff 75 0c             	pushl  0xc(%ebp)
f0104290:	6a 58                	push   $0x58
f0104292:	8b 45 08             	mov    0x8(%ebp),%eax
f0104295:	ff d0                	call   *%eax
f0104297:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
f010429a:	83 ec 08             	sub    $0x8,%esp
f010429d:	ff 75 0c             	pushl  0xc(%ebp)
f01042a0:	6a 58                	push   $0x58
f01042a2:	8b 45 08             	mov    0x8(%ebp),%eax
f01042a5:	ff d0                	call   *%eax
f01042a7:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
f01042aa:	83 ec 08             	sub    $0x8,%esp
f01042ad:	ff 75 0c             	pushl  0xc(%ebp)
f01042b0:	6a 58                	push   $0x58
f01042b2:	8b 45 08             	mov    0x8(%ebp),%eax
f01042b5:	ff d0                	call   *%eax
f01042b7:	83 c4 10             	add    $0x10,%esp
			break;
f01042ba:	e9 c1 00 00 00       	jmp    f0104380 <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
f01042bf:	83 ec 08             	sub    $0x8,%esp
f01042c2:	ff 75 0c             	pushl  0xc(%ebp)
f01042c5:	6a 30                	push   $0x30
f01042c7:	8b 45 08             	mov    0x8(%ebp),%eax
f01042ca:	ff d0                	call   *%eax
f01042cc:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
f01042cf:	83 ec 08             	sub    $0x8,%esp
f01042d2:	ff 75 0c             	pushl  0xc(%ebp)
f01042d5:	6a 78                	push   $0x78
f01042d7:	8b 45 08             	mov    0x8(%ebp),%eax
f01042da:	ff d0                	call   *%eax
f01042dc:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
f01042df:	8b 45 14             	mov    0x14(%ebp),%eax
f01042e2:	83 c0 04             	add    $0x4,%eax
f01042e5:	89 45 14             	mov    %eax,0x14(%ebp)
f01042e8:	8b 45 14             	mov    0x14(%ebp),%eax
f01042eb:	83 e8 04             	sub    $0x4,%eax
f01042ee:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f01042f0:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01042f3:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
f01042fa:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
f0104301:	eb 1f                	jmp    f0104322 <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0104303:	83 ec 08             	sub    $0x8,%esp
f0104306:	ff 75 e8             	pushl  -0x18(%ebp)
f0104309:	8d 45 14             	lea    0x14(%ebp),%eax
f010430c:	50                   	push   %eax
f010430d:	e8 dd fb ff ff       	call   f0103eef <getuint>
f0104312:	83 c4 10             	add    $0x10,%esp
f0104315:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104318:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
f010431b:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
f0104322:	0f be 55 db          	movsbl -0x25(%ebp),%edx
f0104326:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104329:	83 ec 04             	sub    $0x4,%esp
f010432c:	52                   	push   %edx
f010432d:	ff 75 e4             	pushl  -0x1c(%ebp)
f0104330:	50                   	push   %eax
f0104331:	ff 75 f4             	pushl  -0xc(%ebp)
f0104334:	ff 75 f0             	pushl  -0x10(%ebp)
f0104337:	ff 75 0c             	pushl  0xc(%ebp)
f010433a:	ff 75 08             	pushl  0x8(%ebp)
f010433d:	e8 f4 fa ff ff       	call   f0103e36 <printnum>
f0104342:	83 c4 20             	add    $0x20,%esp
			break;
f0104345:	eb 39                	jmp    f0104380 <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0104347:	83 ec 08             	sub    $0x8,%esp
f010434a:	ff 75 0c             	pushl  0xc(%ebp)
f010434d:	53                   	push   %ebx
f010434e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104351:	ff d0                	call   *%eax
f0104353:	83 c4 10             	add    $0x10,%esp
			break;
f0104356:	eb 28                	jmp    f0104380 <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0104358:	83 ec 08             	sub    $0x8,%esp
f010435b:	ff 75 0c             	pushl  0xc(%ebp)
f010435e:	6a 25                	push   $0x25
f0104360:	8b 45 08             	mov    0x8(%ebp),%eax
f0104363:	ff d0                	call   *%eax
f0104365:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
f0104368:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f010436c:	eb 04                	jmp    f0104372 <vprintfmt+0x3bd>
f010436e:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f0104372:	8b 45 10             	mov    0x10(%ebp),%eax
f0104375:	83 e8 01             	sub    $0x1,%eax
f0104378:	0f b6 00             	movzbl (%eax),%eax
f010437b:	3c 25                	cmp    $0x25,%al
f010437d:	75 ef                	jne    f010436e <vprintfmt+0x3b9>
				/* do nothing */;
			break;
f010437f:	90                   	nop
		}
	}
f0104380:	e9 38 fc ff ff       	jmp    f0103fbd <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
f0104385:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
f0104386:	8d 65 f8             	lea    -0x8(%ebp),%esp
f0104389:	5b                   	pop    %ebx
f010438a:	5e                   	pop    %esi
f010438b:	5d                   	pop    %ebp
f010438c:	c3                   	ret    

f010438d <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f010438d:	55                   	push   %ebp
f010438e:	89 e5                	mov    %esp,%ebp
f0104390:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
f0104393:	8d 45 10             	lea    0x10(%ebp),%eax
f0104396:	83 c0 04             	add    $0x4,%eax
f0104399:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
f010439c:	8b 45 10             	mov    0x10(%ebp),%eax
f010439f:	ff 75 f4             	pushl  -0xc(%ebp)
f01043a2:	50                   	push   %eax
f01043a3:	ff 75 0c             	pushl  0xc(%ebp)
f01043a6:	ff 75 08             	pushl  0x8(%ebp)
f01043a9:	e8 07 fc ff ff       	call   f0103fb5 <vprintfmt>
f01043ae:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
f01043b1:	90                   	nop
f01043b2:	c9                   	leave  
f01043b3:	c3                   	ret    

f01043b4 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f01043b4:	55                   	push   %ebp
f01043b5:	89 e5                	mov    %esp,%ebp
	b->cnt++;
f01043b7:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043ba:	8b 40 08             	mov    0x8(%eax),%eax
f01043bd:	8d 50 01             	lea    0x1(%eax),%edx
f01043c0:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043c3:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
f01043c6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043c9:	8b 10                	mov    (%eax),%edx
f01043cb:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043ce:	8b 40 04             	mov    0x4(%eax),%eax
f01043d1:	39 c2                	cmp    %eax,%edx
f01043d3:	73 12                	jae    f01043e7 <sprintputch+0x33>
		*b->buf++ = ch;
f01043d5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043d8:	8b 00                	mov    (%eax),%eax
f01043da:	8d 48 01             	lea    0x1(%eax),%ecx
f01043dd:	8b 55 0c             	mov    0xc(%ebp),%edx
f01043e0:	89 0a                	mov    %ecx,(%edx)
f01043e2:	8b 55 08             	mov    0x8(%ebp),%edx
f01043e5:	88 10                	mov    %dl,(%eax)
}
f01043e7:	90                   	nop
f01043e8:	5d                   	pop    %ebp
f01043e9:	c3                   	ret    

f01043ea <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f01043ea:	55                   	push   %ebp
f01043eb:	89 e5                	mov    %esp,%ebp
f01043ed:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
f01043f0:	8b 45 08             	mov    0x8(%ebp),%eax
f01043f3:	89 45 ec             	mov    %eax,-0x14(%ebp)
f01043f6:	8b 45 0c             	mov    0xc(%ebp),%eax
f01043f9:	8d 50 ff             	lea    -0x1(%eax),%edx
f01043fc:	8b 45 08             	mov    0x8(%ebp),%eax
f01043ff:	01 d0                	add    %edx,%eax
f0104401:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0104404:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f010440b:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f010440f:	74 06                	je     f0104417 <vsnprintf+0x2d>
f0104411:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104415:	7f 07                	jg     f010441e <vsnprintf+0x34>
		return -E_INVAL;
f0104417:	b8 03 00 00 00       	mov    $0x3,%eax
f010441c:	eb 20                	jmp    f010443e <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f010441e:	ff 75 14             	pushl  0x14(%ebp)
f0104421:	ff 75 10             	pushl  0x10(%ebp)
f0104424:	8d 45 ec             	lea    -0x14(%ebp),%eax
f0104427:	50                   	push   %eax
f0104428:	68 b4 43 10 f0       	push   $0xf01043b4
f010442d:	e8 83 fb ff ff       	call   f0103fb5 <vprintfmt>
f0104432:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
f0104435:	8b 45 ec             	mov    -0x14(%ebp),%eax
f0104438:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f010443b:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
f010443e:	c9                   	leave  
f010443f:	c3                   	ret    

f0104440 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f0104440:	55                   	push   %ebp
f0104441:	89 e5                	mov    %esp,%ebp
f0104443:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f0104446:	8d 45 10             	lea    0x10(%ebp),%eax
f0104449:	83 c0 04             	add    $0x4,%eax
f010444c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
f010444f:	8b 45 10             	mov    0x10(%ebp),%eax
f0104452:	ff 75 f4             	pushl  -0xc(%ebp)
f0104455:	50                   	push   %eax
f0104456:	ff 75 0c             	pushl  0xc(%ebp)
f0104459:	ff 75 08             	pushl  0x8(%ebp)
f010445c:	e8 89 ff ff ff       	call   f01043ea <vsnprintf>
f0104461:	83 c4 10             	add    $0x10,%esp
f0104464:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
f0104467:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
f010446a:	c9                   	leave  
f010446b:	c3                   	ret    

f010446c <readline>:

#define BUFLEN 1024
//static char buf[BUFLEN];

void readline(const char *prompt, char* buf)
{
f010446c:	55                   	push   %ebp
f010446d:	89 e5                	mov    %esp,%ebp
f010446f:	83 ec 18             	sub    $0x18,%esp
	int i, c, echoing;
	
	if (prompt != NULL)
f0104472:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
f0104476:	74 13                	je     f010448b <readline+0x1f>
		cprintf("%s", prompt);
f0104478:	83 ec 08             	sub    $0x8,%esp
f010447b:	ff 75 08             	pushl  0x8(%ebp)
f010447e:	68 bc 63 10 f0       	push   $0xf01063bc
f0104483:	e8 d6 eb ff ff       	call   f010305e <cprintf>
f0104488:	83 c4 10             	add    $0x10,%esp

	
	i = 0;
f010448b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	echoing = iscons(0);	
f0104492:	83 ec 0c             	sub    $0xc,%esp
f0104495:	6a 00                	push   $0x0
f0104497:	e8 f0 c4 ff ff       	call   f010098c <iscons>
f010449c:	83 c4 10             	add    $0x10,%esp
f010449f:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (1) {
		c = getchar();
f01044a2:	e8 cc c4 ff ff       	call   f0100973 <getchar>
f01044a7:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if (c < 0) {
f01044aa:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
f01044ae:	79 22                	jns    f01044d2 <readline+0x66>
			if (c != -E_EOF)
f01044b0:	83 7d ec 07          	cmpl   $0x7,-0x14(%ebp)
f01044b4:	0f 84 ae 00 00 00    	je     f0104568 <readline+0xfc>
				cprintf("read error: %e\n", c);			
f01044ba:	83 ec 08             	sub    $0x8,%esp
f01044bd:	ff 75 ec             	pushl  -0x14(%ebp)
f01044c0:	68 bf 63 10 f0       	push   $0xf01063bf
f01044c5:	e8 94 eb ff ff       	call   f010305e <cprintf>
f01044ca:	83 c4 10             	add    $0x10,%esp
			return;
f01044cd:	e9 96 00 00 00       	jmp    f0104568 <readline+0xfc>
		} else if (c >= ' ' && i < BUFLEN-1) {
f01044d2:	83 7d ec 1f          	cmpl   $0x1f,-0x14(%ebp)
f01044d6:	7e 34                	jle    f010450c <readline+0xa0>
f01044d8:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
f01044df:	7f 2b                	jg     f010450c <readline+0xa0>
			if (echoing)
f01044e1:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f01044e5:	74 0e                	je     f01044f5 <readline+0x89>
				cputchar(c);
f01044e7:	83 ec 0c             	sub    $0xc,%esp
f01044ea:	ff 75 ec             	pushl  -0x14(%ebp)
f01044ed:	e8 6a c4 ff ff       	call   f010095c <cputchar>
f01044f2:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f01044f5:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01044f8:	8d 50 01             	lea    0x1(%eax),%edx
f01044fb:	89 55 f4             	mov    %edx,-0xc(%ebp)
f01044fe:	89 c2                	mov    %eax,%edx
f0104500:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104503:	01 d0                	add    %edx,%eax
f0104505:	8b 55 ec             	mov    -0x14(%ebp),%edx
f0104508:	88 10                	mov    %dl,(%eax)
f010450a:	eb 57                	jmp    f0104563 <readline+0xf7>
		} else if (c == '\b' && i > 0) {
f010450c:	83 7d ec 08          	cmpl   $0x8,-0x14(%ebp)
f0104510:	75 20                	jne    f0104532 <readline+0xc6>
f0104512:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
f0104516:	7e 1a                	jle    f0104532 <readline+0xc6>
			if (echoing)
f0104518:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f010451c:	74 0e                	je     f010452c <readline+0xc0>
				cputchar(c);
f010451e:	83 ec 0c             	sub    $0xc,%esp
f0104521:	ff 75 ec             	pushl  -0x14(%ebp)
f0104524:	e8 33 c4 ff ff       	call   f010095c <cputchar>
f0104529:	83 c4 10             	add    $0x10,%esp
			i--;
f010452c:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
f0104530:	eb 31                	jmp    f0104563 <readline+0xf7>
		} else if (c == '\n' || c == '\r') {
f0104532:	83 7d ec 0a          	cmpl   $0xa,-0x14(%ebp)
f0104536:	74 0a                	je     f0104542 <readline+0xd6>
f0104538:	83 7d ec 0d          	cmpl   $0xd,-0x14(%ebp)
f010453c:	0f 85 60 ff ff ff    	jne    f01044a2 <readline+0x36>
			if (echoing)
f0104542:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
f0104546:	74 0e                	je     f0104556 <readline+0xea>
				cputchar(c);
f0104548:	83 ec 0c             	sub    $0xc,%esp
f010454b:	ff 75 ec             	pushl  -0x14(%ebp)
f010454e:	e8 09 c4 ff ff       	call   f010095c <cputchar>
f0104553:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;	
f0104556:	8b 55 f4             	mov    -0xc(%ebp),%edx
f0104559:	8b 45 0c             	mov    0xc(%ebp),%eax
f010455c:	01 d0                	add    %edx,%eax
f010455e:	c6 00 00             	movb   $0x0,(%eax)
			return;		
f0104561:	eb 06                	jmp    f0104569 <readline+0xfd>
		}
	}
f0104563:	e9 3a ff ff ff       	jmp    f01044a2 <readline+0x36>
	while (1) {
		c = getchar();
		if (c < 0) {
			if (c != -E_EOF)
				cprintf("read error: %e\n", c);			
			return;
f0104568:	90                   	nop
				cputchar(c);
			buf[i] = 0;	
			return;		
		}
	}
}
f0104569:	c9                   	leave  
f010456a:	c3                   	ret    

f010456b <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
f010456b:	55                   	push   %ebp
f010456c:	89 e5                	mov    %esp,%ebp
f010456e:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
f0104571:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f0104578:	eb 08                	jmp    f0104582 <strlen+0x17>
		n++;
f010457a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f010457e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104582:	8b 45 08             	mov    0x8(%ebp),%eax
f0104585:	0f b6 00             	movzbl (%eax),%eax
f0104588:	84 c0                	test   %al,%al
f010458a:	75 ee                	jne    f010457a <strlen+0xf>
		n++;
	return n;
f010458c:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f010458f:	c9                   	leave  
f0104590:	c3                   	ret    

f0104591 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
f0104591:	55                   	push   %ebp
f0104592:	89 e5                	mov    %esp,%ebp
f0104594:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f0104597:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f010459e:	eb 0c                	jmp    f01045ac <strnlen+0x1b>
		n++;
f01045a0:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f01045a4:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01045a8:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
f01045ac:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f01045b0:	74 0a                	je     f01045bc <strnlen+0x2b>
f01045b2:	8b 45 08             	mov    0x8(%ebp),%eax
f01045b5:	0f b6 00             	movzbl (%eax),%eax
f01045b8:	84 c0                	test   %al,%al
f01045ba:	75 e4                	jne    f01045a0 <strnlen+0xf>
		n++;
	return n;
f01045bc:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f01045bf:	c9                   	leave  
f01045c0:	c3                   	ret    

f01045c1 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f01045c1:	55                   	push   %ebp
f01045c2:	89 e5                	mov    %esp,%ebp
f01045c4:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
f01045c7:	8b 45 08             	mov    0x8(%ebp),%eax
f01045ca:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
f01045cd:	90                   	nop
f01045ce:	8b 45 08             	mov    0x8(%ebp),%eax
f01045d1:	8d 50 01             	lea    0x1(%eax),%edx
f01045d4:	89 55 08             	mov    %edx,0x8(%ebp)
f01045d7:	8b 55 0c             	mov    0xc(%ebp),%edx
f01045da:	8d 4a 01             	lea    0x1(%edx),%ecx
f01045dd:	89 4d 0c             	mov    %ecx,0xc(%ebp)
f01045e0:	0f b6 12             	movzbl (%edx),%edx
f01045e3:	88 10                	mov    %dl,(%eax)
f01045e5:	0f b6 00             	movzbl (%eax),%eax
f01045e8:	84 c0                	test   %al,%al
f01045ea:	75 e2                	jne    f01045ce <strcpy+0xd>
		/* do nothing */;
	return ret;
f01045ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
f01045ef:	c9                   	leave  
f01045f0:	c3                   	ret    

f01045f1 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
f01045f1:	55                   	push   %ebp
f01045f2:	89 e5                	mov    %esp,%ebp
f01045f4:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
f01045f7:	8b 45 08             	mov    0x8(%ebp),%eax
f01045fa:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
f01045fd:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
f0104604:	eb 23                	jmp    f0104629 <strncpy+0x38>
		*dst++ = *src;
f0104606:	8b 45 08             	mov    0x8(%ebp),%eax
f0104609:	8d 50 01             	lea    0x1(%eax),%edx
f010460c:	89 55 08             	mov    %edx,0x8(%ebp)
f010460f:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104612:	0f b6 12             	movzbl (%edx),%edx
f0104615:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
f0104617:	8b 45 0c             	mov    0xc(%ebp),%eax
f010461a:	0f b6 00             	movzbl (%eax),%eax
f010461d:	84 c0                	test   %al,%al
f010461f:	74 04                	je     f0104625 <strncpy+0x34>
			src++;
f0104621:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0104625:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f0104629:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010462c:	3b 45 10             	cmp    0x10(%ebp),%eax
f010462f:	72 d5                	jb     f0104606 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
f0104631:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f0104634:	c9                   	leave  
f0104635:	c3                   	ret    

f0104636 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
f0104636:	55                   	push   %ebp
f0104637:	89 e5                	mov    %esp,%ebp
f0104639:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
f010463c:	8b 45 08             	mov    0x8(%ebp),%eax
f010463f:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
f0104642:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104646:	74 33                	je     f010467b <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
f0104648:	eb 17                	jmp    f0104661 <strlcpy+0x2b>
			*dst++ = *src++;
f010464a:	8b 45 08             	mov    0x8(%ebp),%eax
f010464d:	8d 50 01             	lea    0x1(%eax),%edx
f0104650:	89 55 08             	mov    %edx,0x8(%ebp)
f0104653:	8b 55 0c             	mov    0xc(%ebp),%edx
f0104656:	8d 4a 01             	lea    0x1(%edx),%ecx
f0104659:	89 4d 0c             	mov    %ecx,0xc(%ebp)
f010465c:	0f b6 12             	movzbl (%edx),%edx
f010465f:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0104661:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f0104665:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104669:	74 0a                	je     f0104675 <strlcpy+0x3f>
f010466b:	8b 45 0c             	mov    0xc(%ebp),%eax
f010466e:	0f b6 00             	movzbl (%eax),%eax
f0104671:	84 c0                	test   %al,%al
f0104673:	75 d5                	jne    f010464a <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
f0104675:	8b 45 08             	mov    0x8(%ebp),%eax
f0104678:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
f010467b:	8b 55 08             	mov    0x8(%ebp),%edx
f010467e:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104681:	29 c2                	sub    %eax,%edx
f0104683:	89 d0                	mov    %edx,%eax
}
f0104685:	c9                   	leave  
f0104686:	c3                   	ret    

f0104687 <strcmp>:

int
strcmp(const char *p, const char *q)
{
f0104687:	55                   	push   %ebp
f0104688:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
f010468a:	eb 08                	jmp    f0104694 <strcmp+0xd>
		p++, q++;
f010468c:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104690:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f0104694:	8b 45 08             	mov    0x8(%ebp),%eax
f0104697:	0f b6 00             	movzbl (%eax),%eax
f010469a:	84 c0                	test   %al,%al
f010469c:	74 10                	je     f01046ae <strcmp+0x27>
f010469e:	8b 45 08             	mov    0x8(%ebp),%eax
f01046a1:	0f b6 10             	movzbl (%eax),%edx
f01046a4:	8b 45 0c             	mov    0xc(%ebp),%eax
f01046a7:	0f b6 00             	movzbl (%eax),%eax
f01046aa:	38 c2                	cmp    %al,%dl
f01046ac:	74 de                	je     f010468c <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f01046ae:	8b 45 08             	mov    0x8(%ebp),%eax
f01046b1:	0f b6 00             	movzbl (%eax),%eax
f01046b4:	0f b6 d0             	movzbl %al,%edx
f01046b7:	8b 45 0c             	mov    0xc(%ebp),%eax
f01046ba:	0f b6 00             	movzbl (%eax),%eax
f01046bd:	0f b6 c0             	movzbl %al,%eax
f01046c0:	29 c2                	sub    %eax,%edx
f01046c2:	89 d0                	mov    %edx,%eax
}
f01046c4:	5d                   	pop    %ebp
f01046c5:	c3                   	ret    

f01046c6 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
f01046c6:	55                   	push   %ebp
f01046c7:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
f01046c9:	eb 0c                	jmp    f01046d7 <strncmp+0x11>
		n--, p++, q++;
f01046cb:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
f01046cf:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01046d3:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
f01046d7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01046db:	74 1a                	je     f01046f7 <strncmp+0x31>
f01046dd:	8b 45 08             	mov    0x8(%ebp),%eax
f01046e0:	0f b6 00             	movzbl (%eax),%eax
f01046e3:	84 c0                	test   %al,%al
f01046e5:	74 10                	je     f01046f7 <strncmp+0x31>
f01046e7:	8b 45 08             	mov    0x8(%ebp),%eax
f01046ea:	0f b6 10             	movzbl (%eax),%edx
f01046ed:	8b 45 0c             	mov    0xc(%ebp),%eax
f01046f0:	0f b6 00             	movzbl (%eax),%eax
f01046f3:	38 c2                	cmp    %al,%dl
f01046f5:	74 d4                	je     f01046cb <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
f01046f7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01046fb:	75 07                	jne    f0104704 <strncmp+0x3e>
		return 0;
f01046fd:	b8 00 00 00 00       	mov    $0x0,%eax
f0104702:	eb 16                	jmp    f010471a <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f0104704:	8b 45 08             	mov    0x8(%ebp),%eax
f0104707:	0f b6 00             	movzbl (%eax),%eax
f010470a:	0f b6 d0             	movzbl %al,%edx
f010470d:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104710:	0f b6 00             	movzbl (%eax),%eax
f0104713:	0f b6 c0             	movzbl %al,%eax
f0104716:	29 c2                	sub    %eax,%edx
f0104718:	89 d0                	mov    %edx,%eax
}
f010471a:	5d                   	pop    %ebp
f010471b:	c3                   	ret    

f010471c <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f010471c:	55                   	push   %ebp
f010471d:	89 e5                	mov    %esp,%ebp
f010471f:	83 ec 04             	sub    $0x4,%esp
f0104722:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104725:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
f0104728:	eb 14                	jmp    f010473e <strchr+0x22>
		if (*s == c)
f010472a:	8b 45 08             	mov    0x8(%ebp),%eax
f010472d:	0f b6 00             	movzbl (%eax),%eax
f0104730:	3a 45 fc             	cmp    -0x4(%ebp),%al
f0104733:	75 05                	jne    f010473a <strchr+0x1e>
			return (char *) s;
f0104735:	8b 45 08             	mov    0x8(%ebp),%eax
f0104738:	eb 13                	jmp    f010474d <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f010473a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f010473e:	8b 45 08             	mov    0x8(%ebp),%eax
f0104741:	0f b6 00             	movzbl (%eax),%eax
f0104744:	84 c0                	test   %al,%al
f0104746:	75 e2                	jne    f010472a <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
f0104748:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010474d:	c9                   	leave  
f010474e:	c3                   	ret    

f010474f <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f010474f:	55                   	push   %ebp
f0104750:	89 e5                	mov    %esp,%ebp
f0104752:	83 ec 04             	sub    $0x4,%esp
f0104755:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104758:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
f010475b:	eb 0f                	jmp    f010476c <strfind+0x1d>
		if (*s == c)
f010475d:	8b 45 08             	mov    0x8(%ebp),%eax
f0104760:	0f b6 00             	movzbl (%eax),%eax
f0104763:	3a 45 fc             	cmp    -0x4(%ebp),%al
f0104766:	74 10                	je     f0104778 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
f0104768:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f010476c:	8b 45 08             	mov    0x8(%ebp),%eax
f010476f:	0f b6 00             	movzbl (%eax),%eax
f0104772:	84 c0                	test   %al,%al
f0104774:	75 e7                	jne    f010475d <strfind+0xe>
f0104776:	eb 01                	jmp    f0104779 <strfind+0x2a>
		if (*s == c)
			break;
f0104778:	90                   	nop
	return (char *) s;
f0104779:	8b 45 08             	mov    0x8(%ebp),%eax
}
f010477c:	c9                   	leave  
f010477d:	c3                   	ret    

f010477e <memset>:


void *
memset(void *v, int c, uint32 n)
{
f010477e:	55                   	push   %ebp
f010477f:	89 e5                	mov    %esp,%ebp
f0104781:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
f0104784:	8b 45 08             	mov    0x8(%ebp),%eax
f0104787:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
f010478a:	8b 45 10             	mov    0x10(%ebp),%eax
f010478d:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
f0104790:	eb 0e                	jmp    f01047a0 <memset+0x22>
		*p++ = c;
f0104792:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104795:	8d 50 01             	lea    0x1(%eax),%edx
f0104798:	89 55 fc             	mov    %edx,-0x4(%ebp)
f010479b:	8b 55 0c             	mov    0xc(%ebp),%edx
f010479e:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
f01047a0:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
f01047a4:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
f01047a8:	79 e8                	jns    f0104792 <memset+0x14>
		*p++ = c;

	return v;
f01047aa:	8b 45 08             	mov    0x8(%ebp),%eax
}
f01047ad:	c9                   	leave  
f01047ae:	c3                   	ret    

f01047af <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
f01047af:	55                   	push   %ebp
f01047b0:	89 e5                	mov    %esp,%ebp
f01047b2:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
f01047b5:	8b 45 0c             	mov    0xc(%ebp),%eax
f01047b8:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
f01047bb:	8b 45 08             	mov    0x8(%ebp),%eax
f01047be:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
f01047c1:	eb 17                	jmp    f01047da <memcpy+0x2b>
		*d++ = *s++;
f01047c3:	8b 45 f8             	mov    -0x8(%ebp),%eax
f01047c6:	8d 50 01             	lea    0x1(%eax),%edx
f01047c9:	89 55 f8             	mov    %edx,-0x8(%ebp)
f01047cc:	8b 55 fc             	mov    -0x4(%ebp),%edx
f01047cf:	8d 4a 01             	lea    0x1(%edx),%ecx
f01047d2:	89 4d fc             	mov    %ecx,-0x4(%ebp)
f01047d5:	0f b6 12             	movzbl (%edx),%edx
f01047d8:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
f01047da:	8b 45 10             	mov    0x10(%ebp),%eax
f01047dd:	8d 50 ff             	lea    -0x1(%eax),%edx
f01047e0:	89 55 10             	mov    %edx,0x10(%ebp)
f01047e3:	85 c0                	test   %eax,%eax
f01047e5:	75 dc                	jne    f01047c3 <memcpy+0x14>
		*d++ = *s++;

	return dst;
f01047e7:	8b 45 08             	mov    0x8(%ebp),%eax
}
f01047ea:	c9                   	leave  
f01047eb:	c3                   	ret    

f01047ec <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
f01047ec:	55                   	push   %ebp
f01047ed:	89 e5                	mov    %esp,%ebp
f01047ef:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
f01047f2:	8b 45 0c             	mov    0xc(%ebp),%eax
f01047f5:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
f01047f8:	8b 45 08             	mov    0x8(%ebp),%eax
f01047fb:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
f01047fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104801:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0104804:	73 54                	jae    f010485a <memmove+0x6e>
f0104806:	8b 55 fc             	mov    -0x4(%ebp),%edx
f0104809:	8b 45 10             	mov    0x10(%ebp),%eax
f010480c:	01 d0                	add    %edx,%eax
f010480e:	3b 45 f8             	cmp    -0x8(%ebp),%eax
f0104811:	76 47                	jbe    f010485a <memmove+0x6e>
		s += n;
f0104813:	8b 45 10             	mov    0x10(%ebp),%eax
f0104816:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
f0104819:	8b 45 10             	mov    0x10(%ebp),%eax
f010481c:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
f010481f:	eb 13                	jmp    f0104834 <memmove+0x48>
			*--d = *--s;
f0104821:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
f0104825:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
f0104829:	8b 45 fc             	mov    -0x4(%ebp),%eax
f010482c:	0f b6 10             	movzbl (%eax),%edx
f010482f:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104832:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
f0104834:	8b 45 10             	mov    0x10(%ebp),%eax
f0104837:	8d 50 ff             	lea    -0x1(%eax),%edx
f010483a:	89 55 10             	mov    %edx,0x10(%ebp)
f010483d:	85 c0                	test   %eax,%eax
f010483f:	75 e0                	jne    f0104821 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
f0104841:	eb 24                	jmp    f0104867 <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
f0104843:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104846:	8d 50 01             	lea    0x1(%eax),%edx
f0104849:	89 55 f8             	mov    %edx,-0x8(%ebp)
f010484c:	8b 55 fc             	mov    -0x4(%ebp),%edx
f010484f:	8d 4a 01             	lea    0x1(%edx),%ecx
f0104852:	89 4d fc             	mov    %ecx,-0x4(%ebp)
f0104855:	0f b6 12             	movzbl (%edx),%edx
f0104858:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
f010485a:	8b 45 10             	mov    0x10(%ebp),%eax
f010485d:	8d 50 ff             	lea    -0x1(%eax),%edx
f0104860:	89 55 10             	mov    %edx,0x10(%ebp)
f0104863:	85 c0                	test   %eax,%eax
f0104865:	75 dc                	jne    f0104843 <memmove+0x57>
			*d++ = *s++;

	return dst;
f0104867:	8b 45 08             	mov    0x8(%ebp),%eax
}
f010486a:	c9                   	leave  
f010486b:	c3                   	ret    

f010486c <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
f010486c:	55                   	push   %ebp
f010486d:	89 e5                	mov    %esp,%ebp
f010486f:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
f0104872:	8b 45 08             	mov    0x8(%ebp),%eax
f0104875:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
f0104878:	8b 45 0c             	mov    0xc(%ebp),%eax
f010487b:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
f010487e:	eb 30                	jmp    f01048b0 <memcmp+0x44>
		if (*s1 != *s2)
f0104880:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104883:	0f b6 10             	movzbl (%eax),%edx
f0104886:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104889:	0f b6 00             	movzbl (%eax),%eax
f010488c:	38 c2                	cmp    %al,%dl
f010488e:	74 18                	je     f01048a8 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
f0104890:	8b 45 fc             	mov    -0x4(%ebp),%eax
f0104893:	0f b6 00             	movzbl (%eax),%eax
f0104896:	0f b6 d0             	movzbl %al,%edx
f0104899:	8b 45 f8             	mov    -0x8(%ebp),%eax
f010489c:	0f b6 00             	movzbl (%eax),%eax
f010489f:	0f b6 c0             	movzbl %al,%eax
f01048a2:	29 c2                	sub    %eax,%edx
f01048a4:	89 d0                	mov    %edx,%eax
f01048a6:	eb 1a                	jmp    f01048c2 <memcmp+0x56>
		s1++, s2++;
f01048a8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
f01048ac:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
f01048b0:	8b 45 10             	mov    0x10(%ebp),%eax
f01048b3:	8d 50 ff             	lea    -0x1(%eax),%edx
f01048b6:	89 55 10             	mov    %edx,0x10(%ebp)
f01048b9:	85 c0                	test   %eax,%eax
f01048bb:	75 c3                	jne    f0104880 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f01048bd:	b8 00 00 00 00       	mov    $0x0,%eax
}
f01048c2:	c9                   	leave  
f01048c3:	c3                   	ret    

f01048c4 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
f01048c4:	55                   	push   %ebp
f01048c5:	89 e5                	mov    %esp,%ebp
f01048c7:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
f01048ca:	8b 55 08             	mov    0x8(%ebp),%edx
f01048cd:	8b 45 10             	mov    0x10(%ebp),%eax
f01048d0:	01 d0                	add    %edx,%eax
f01048d2:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
f01048d5:	eb 17                	jmp    f01048ee <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
f01048d7:	8b 45 08             	mov    0x8(%ebp),%eax
f01048da:	0f b6 00             	movzbl (%eax),%eax
f01048dd:	0f b6 d0             	movzbl %al,%edx
f01048e0:	8b 45 0c             	mov    0xc(%ebp),%eax
f01048e3:	0f b6 c0             	movzbl %al,%eax
f01048e6:	39 c2                	cmp    %eax,%edx
f01048e8:	74 0e                	je     f01048f8 <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f01048ea:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f01048ee:	8b 45 08             	mov    0x8(%ebp),%eax
f01048f1:	3b 45 fc             	cmp    -0x4(%ebp),%eax
f01048f4:	72 e1                	jb     f01048d7 <memfind+0x13>
f01048f6:	eb 01                	jmp    f01048f9 <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
f01048f8:	90                   	nop
	return (void *) s;
f01048f9:	8b 45 08             	mov    0x8(%ebp),%eax
}
f01048fc:	c9                   	leave  
f01048fd:	c3                   	ret    

f01048fe <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f01048fe:	55                   	push   %ebp
f01048ff:	89 e5                	mov    %esp,%ebp
f0104901:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
f0104904:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
f010490b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104912:	eb 04                	jmp    f0104918 <strtol+0x1a>
		s++;
f0104914:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0104918:	8b 45 08             	mov    0x8(%ebp),%eax
f010491b:	0f b6 00             	movzbl (%eax),%eax
f010491e:	3c 20                	cmp    $0x20,%al
f0104920:	74 f2                	je     f0104914 <strtol+0x16>
f0104922:	8b 45 08             	mov    0x8(%ebp),%eax
f0104925:	0f b6 00             	movzbl (%eax),%eax
f0104928:	3c 09                	cmp    $0x9,%al
f010492a:	74 e8                	je     f0104914 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
f010492c:	8b 45 08             	mov    0x8(%ebp),%eax
f010492f:	0f b6 00             	movzbl (%eax),%eax
f0104932:	3c 2b                	cmp    $0x2b,%al
f0104934:	75 06                	jne    f010493c <strtol+0x3e>
		s++;
f0104936:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f010493a:	eb 15                	jmp    f0104951 <strtol+0x53>
	else if (*s == '-')
f010493c:	8b 45 08             	mov    0x8(%ebp),%eax
f010493f:	0f b6 00             	movzbl (%eax),%eax
f0104942:	3c 2d                	cmp    $0x2d,%al
f0104944:	75 0b                	jne    f0104951 <strtol+0x53>
		s++, neg = 1;
f0104946:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f010494a:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0104951:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104955:	74 06                	je     f010495d <strtol+0x5f>
f0104957:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
f010495b:	75 24                	jne    f0104981 <strtol+0x83>
f010495d:	8b 45 08             	mov    0x8(%ebp),%eax
f0104960:	0f b6 00             	movzbl (%eax),%eax
f0104963:	3c 30                	cmp    $0x30,%al
f0104965:	75 1a                	jne    f0104981 <strtol+0x83>
f0104967:	8b 45 08             	mov    0x8(%ebp),%eax
f010496a:	83 c0 01             	add    $0x1,%eax
f010496d:	0f b6 00             	movzbl (%eax),%eax
f0104970:	3c 78                	cmp    $0x78,%al
f0104972:	75 0d                	jne    f0104981 <strtol+0x83>
		s += 2, base = 16;
f0104974:	83 45 08 02          	addl   $0x2,0x8(%ebp)
f0104978:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
f010497f:	eb 2a                	jmp    f01049ab <strtol+0xad>
	else if (base == 0 && s[0] == '0')
f0104981:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f0104985:	75 17                	jne    f010499e <strtol+0xa0>
f0104987:	8b 45 08             	mov    0x8(%ebp),%eax
f010498a:	0f b6 00             	movzbl (%eax),%eax
f010498d:	3c 30                	cmp    $0x30,%al
f010498f:	75 0d                	jne    f010499e <strtol+0xa0>
		s++, base = 8;
f0104991:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104995:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
f010499c:	eb 0d                	jmp    f01049ab <strtol+0xad>
	else if (base == 0)
f010499e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
f01049a2:	75 07                	jne    f01049ab <strtol+0xad>
		base = 10;
f01049a4:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f01049ab:	8b 45 08             	mov    0x8(%ebp),%eax
f01049ae:	0f b6 00             	movzbl (%eax),%eax
f01049b1:	3c 2f                	cmp    $0x2f,%al
f01049b3:	7e 1b                	jle    f01049d0 <strtol+0xd2>
f01049b5:	8b 45 08             	mov    0x8(%ebp),%eax
f01049b8:	0f b6 00             	movzbl (%eax),%eax
f01049bb:	3c 39                	cmp    $0x39,%al
f01049bd:	7f 11                	jg     f01049d0 <strtol+0xd2>
			dig = *s - '0';
f01049bf:	8b 45 08             	mov    0x8(%ebp),%eax
f01049c2:	0f b6 00             	movzbl (%eax),%eax
f01049c5:	0f be c0             	movsbl %al,%eax
f01049c8:	83 e8 30             	sub    $0x30,%eax
f01049cb:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01049ce:	eb 48                	jmp    f0104a18 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
f01049d0:	8b 45 08             	mov    0x8(%ebp),%eax
f01049d3:	0f b6 00             	movzbl (%eax),%eax
f01049d6:	3c 60                	cmp    $0x60,%al
f01049d8:	7e 1b                	jle    f01049f5 <strtol+0xf7>
f01049da:	8b 45 08             	mov    0x8(%ebp),%eax
f01049dd:	0f b6 00             	movzbl (%eax),%eax
f01049e0:	3c 7a                	cmp    $0x7a,%al
f01049e2:	7f 11                	jg     f01049f5 <strtol+0xf7>
			dig = *s - 'a' + 10;
f01049e4:	8b 45 08             	mov    0x8(%ebp),%eax
f01049e7:	0f b6 00             	movzbl (%eax),%eax
f01049ea:	0f be c0             	movsbl %al,%eax
f01049ed:	83 e8 57             	sub    $0x57,%eax
f01049f0:	89 45 f4             	mov    %eax,-0xc(%ebp)
f01049f3:	eb 23                	jmp    f0104a18 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
f01049f5:	8b 45 08             	mov    0x8(%ebp),%eax
f01049f8:	0f b6 00             	movzbl (%eax),%eax
f01049fb:	3c 40                	cmp    $0x40,%al
f01049fd:	7e 3c                	jle    f0104a3b <strtol+0x13d>
f01049ff:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a02:	0f b6 00             	movzbl (%eax),%eax
f0104a05:	3c 5a                	cmp    $0x5a,%al
f0104a07:	7f 32                	jg     f0104a3b <strtol+0x13d>
			dig = *s - 'A' + 10;
f0104a09:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a0c:	0f b6 00             	movzbl (%eax),%eax
f0104a0f:	0f be c0             	movsbl %al,%eax
f0104a12:	83 e8 37             	sub    $0x37,%eax
f0104a15:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
f0104a18:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0104a1b:	3b 45 10             	cmp    0x10(%ebp),%eax
f0104a1e:	7d 1a                	jge    f0104a3a <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
f0104a20:	83 45 08 01          	addl   $0x1,0x8(%ebp)
f0104a24:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104a27:	0f af 45 10          	imul   0x10(%ebp),%eax
f0104a2b:	89 c2                	mov    %eax,%edx
f0104a2d:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0104a30:	01 d0                	add    %edx,%eax
f0104a32:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
f0104a35:	e9 71 ff ff ff       	jmp    f01049ab <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
f0104a3a:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
f0104a3b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0104a3f:	74 08                	je     f0104a49 <strtol+0x14b>
		*endptr = (char *) s;
f0104a41:	8b 45 0c             	mov    0xc(%ebp),%eax
f0104a44:	8b 55 08             	mov    0x8(%ebp),%edx
f0104a47:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
f0104a49:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
f0104a4d:	74 07                	je     f0104a56 <strtol+0x158>
f0104a4f:	8b 45 f8             	mov    -0x8(%ebp),%eax
f0104a52:	f7 d8                	neg    %eax
f0104a54:	eb 03                	jmp    f0104a59 <strtol+0x15b>
f0104a56:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
f0104a59:	c9                   	leave  
f0104a5a:	c3                   	ret    

f0104a5b <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
f0104a5b:	55                   	push   %ebp
f0104a5c:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
f0104a5e:	8b 45 14             	mov    0x14(%ebp),%eax
f0104a61:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
f0104a67:	8b 45 14             	mov    0x14(%ebp),%eax
f0104a6a:	8b 00                	mov    (%eax),%eax
f0104a6c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104a73:	8b 45 10             	mov    0x10(%ebp),%eax
f0104a76:	01 d0                	add    %edx,%eax
f0104a78:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
f0104a7e:	eb 0c                	jmp    f0104a8c <strsplit+0x31>
			*string++ = 0;
f0104a80:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a83:	8d 50 01             	lea    0x1(%eax),%edx
f0104a86:	89 55 08             	mov    %edx,0x8(%ebp)
f0104a89:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
f0104a8c:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a8f:	0f b6 00             	movzbl (%eax),%eax
f0104a92:	84 c0                	test   %al,%al
f0104a94:	74 19                	je     f0104aaf <strsplit+0x54>
f0104a96:	8b 45 08             	mov    0x8(%ebp),%eax
f0104a99:	0f b6 00             	movzbl (%eax),%eax
f0104a9c:	0f be c0             	movsbl %al,%eax
f0104a9f:	50                   	push   %eax
f0104aa0:	ff 75 0c             	pushl  0xc(%ebp)
f0104aa3:	e8 74 fc ff ff       	call   f010471c <strchr>
f0104aa8:	83 c4 08             	add    $0x8,%esp
f0104aab:	85 c0                	test   %eax,%eax
f0104aad:	75 d1                	jne    f0104a80 <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
f0104aaf:	8b 45 08             	mov    0x8(%ebp),%eax
f0104ab2:	0f b6 00             	movzbl (%eax),%eax
f0104ab5:	84 c0                	test   %al,%al
f0104ab7:	74 5d                	je     f0104b16 <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
f0104ab9:	8b 45 14             	mov    0x14(%ebp),%eax
f0104abc:	8b 00                	mov    (%eax),%eax
f0104abe:	83 f8 0f             	cmp    $0xf,%eax
f0104ac1:	75 07                	jne    f0104aca <strsplit+0x6f>
		{
			return 0;
f0104ac3:	b8 00 00 00 00       	mov    $0x0,%eax
f0104ac8:	eb 69                	jmp    f0104b33 <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
f0104aca:	8b 45 14             	mov    0x14(%ebp),%eax
f0104acd:	8b 00                	mov    (%eax),%eax
f0104acf:	8d 48 01             	lea    0x1(%eax),%ecx
f0104ad2:	8b 55 14             	mov    0x14(%ebp),%edx
f0104ad5:	89 0a                	mov    %ecx,(%edx)
f0104ad7:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104ade:	8b 45 10             	mov    0x10(%ebp),%eax
f0104ae1:	01 c2                	add    %eax,%edx
f0104ae3:	8b 45 08             	mov    0x8(%ebp),%eax
f0104ae6:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
f0104ae8:	eb 04                	jmp    f0104aee <strsplit+0x93>
			string++;
f0104aea:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
f0104aee:	8b 45 08             	mov    0x8(%ebp),%eax
f0104af1:	0f b6 00             	movzbl (%eax),%eax
f0104af4:	84 c0                	test   %al,%al
f0104af6:	74 86                	je     f0104a7e <strsplit+0x23>
f0104af8:	8b 45 08             	mov    0x8(%ebp),%eax
f0104afb:	0f b6 00             	movzbl (%eax),%eax
f0104afe:	0f be c0             	movsbl %al,%eax
f0104b01:	50                   	push   %eax
f0104b02:	ff 75 0c             	pushl  0xc(%ebp)
f0104b05:	e8 12 fc ff ff       	call   f010471c <strchr>
f0104b0a:	83 c4 08             	add    $0x8,%esp
f0104b0d:	85 c0                	test   %eax,%eax
f0104b0f:	74 d9                	je     f0104aea <strsplit+0x8f>
			string++;
	}
f0104b11:	e9 68 ff ff ff       	jmp    f0104a7e <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
f0104b16:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
f0104b17:	8b 45 14             	mov    0x14(%ebp),%eax
f0104b1a:	8b 00                	mov    (%eax),%eax
f0104b1c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
f0104b23:	8b 45 10             	mov    0x10(%ebp),%eax
f0104b26:	01 d0                	add    %edx,%eax
f0104b28:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
f0104b2e:	b8 01 00 00 00       	mov    $0x1,%eax
}
f0104b33:	c9                   	leave  
f0104b34:	c3                   	ret    
f0104b35:	66 90                	xchg   %ax,%ax
f0104b37:	66 90                	xchg   %ax,%ax
f0104b39:	66 90                	xchg   %ax,%ax
f0104b3b:	66 90                	xchg   %ax,%ax
f0104b3d:	66 90                	xchg   %ax,%ax
f0104b3f:	90                   	nop

f0104b40 <__udivdi3>:
f0104b40:	55                   	push   %ebp
f0104b41:	57                   	push   %edi
f0104b42:	56                   	push   %esi
f0104b43:	53                   	push   %ebx
f0104b44:	83 ec 1c             	sub    $0x1c,%esp
f0104b47:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f0104b4b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f0104b4f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0104b53:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104b57:	85 f6                	test   %esi,%esi
f0104b59:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0104b5d:	89 ca                	mov    %ecx,%edx
f0104b5f:	89 f8                	mov    %edi,%eax
f0104b61:	75 3d                	jne    f0104ba0 <__udivdi3+0x60>
f0104b63:	39 cf                	cmp    %ecx,%edi
f0104b65:	0f 87 c5 00 00 00    	ja     f0104c30 <__udivdi3+0xf0>
f0104b6b:	85 ff                	test   %edi,%edi
f0104b6d:	89 fd                	mov    %edi,%ebp
f0104b6f:	75 0b                	jne    f0104b7c <__udivdi3+0x3c>
f0104b71:	b8 01 00 00 00       	mov    $0x1,%eax
f0104b76:	31 d2                	xor    %edx,%edx
f0104b78:	f7 f7                	div    %edi
f0104b7a:	89 c5                	mov    %eax,%ebp
f0104b7c:	89 c8                	mov    %ecx,%eax
f0104b7e:	31 d2                	xor    %edx,%edx
f0104b80:	f7 f5                	div    %ebp
f0104b82:	89 c1                	mov    %eax,%ecx
f0104b84:	89 d8                	mov    %ebx,%eax
f0104b86:	89 cf                	mov    %ecx,%edi
f0104b88:	f7 f5                	div    %ebp
f0104b8a:	89 c3                	mov    %eax,%ebx
f0104b8c:	89 d8                	mov    %ebx,%eax
f0104b8e:	89 fa                	mov    %edi,%edx
f0104b90:	83 c4 1c             	add    $0x1c,%esp
f0104b93:	5b                   	pop    %ebx
f0104b94:	5e                   	pop    %esi
f0104b95:	5f                   	pop    %edi
f0104b96:	5d                   	pop    %ebp
f0104b97:	c3                   	ret    
f0104b98:	90                   	nop
f0104b99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0104ba0:	39 ce                	cmp    %ecx,%esi
f0104ba2:	77 74                	ja     f0104c18 <__udivdi3+0xd8>
f0104ba4:	0f bd fe             	bsr    %esi,%edi
f0104ba7:	83 f7 1f             	xor    $0x1f,%edi
f0104baa:	0f 84 98 00 00 00    	je     f0104c48 <__udivdi3+0x108>
f0104bb0:	bb 20 00 00 00       	mov    $0x20,%ebx
f0104bb5:	89 f9                	mov    %edi,%ecx
f0104bb7:	89 c5                	mov    %eax,%ebp
f0104bb9:	29 fb                	sub    %edi,%ebx
f0104bbb:	d3 e6                	shl    %cl,%esi
f0104bbd:	89 d9                	mov    %ebx,%ecx
f0104bbf:	d3 ed                	shr    %cl,%ebp
f0104bc1:	89 f9                	mov    %edi,%ecx
f0104bc3:	d3 e0                	shl    %cl,%eax
f0104bc5:	09 ee                	or     %ebp,%esi
f0104bc7:	89 d9                	mov    %ebx,%ecx
f0104bc9:	89 44 24 0c          	mov    %eax,0xc(%esp)
f0104bcd:	89 d5                	mov    %edx,%ebp
f0104bcf:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104bd3:	d3 ed                	shr    %cl,%ebp
f0104bd5:	89 f9                	mov    %edi,%ecx
f0104bd7:	d3 e2                	shl    %cl,%edx
f0104bd9:	89 d9                	mov    %ebx,%ecx
f0104bdb:	d3 e8                	shr    %cl,%eax
f0104bdd:	09 c2                	or     %eax,%edx
f0104bdf:	89 d0                	mov    %edx,%eax
f0104be1:	89 ea                	mov    %ebp,%edx
f0104be3:	f7 f6                	div    %esi
f0104be5:	89 d5                	mov    %edx,%ebp
f0104be7:	89 c3                	mov    %eax,%ebx
f0104be9:	f7 64 24 0c          	mull   0xc(%esp)
f0104bed:	39 d5                	cmp    %edx,%ebp
f0104bef:	72 10                	jb     f0104c01 <__udivdi3+0xc1>
f0104bf1:	8b 74 24 08          	mov    0x8(%esp),%esi
f0104bf5:	89 f9                	mov    %edi,%ecx
f0104bf7:	d3 e6                	shl    %cl,%esi
f0104bf9:	39 c6                	cmp    %eax,%esi
f0104bfb:	73 07                	jae    f0104c04 <__udivdi3+0xc4>
f0104bfd:	39 d5                	cmp    %edx,%ebp
f0104bff:	75 03                	jne    f0104c04 <__udivdi3+0xc4>
f0104c01:	83 eb 01             	sub    $0x1,%ebx
f0104c04:	31 ff                	xor    %edi,%edi
f0104c06:	89 d8                	mov    %ebx,%eax
f0104c08:	89 fa                	mov    %edi,%edx
f0104c0a:	83 c4 1c             	add    $0x1c,%esp
f0104c0d:	5b                   	pop    %ebx
f0104c0e:	5e                   	pop    %esi
f0104c0f:	5f                   	pop    %edi
f0104c10:	5d                   	pop    %ebp
f0104c11:	c3                   	ret    
f0104c12:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f0104c18:	31 ff                	xor    %edi,%edi
f0104c1a:	31 db                	xor    %ebx,%ebx
f0104c1c:	89 d8                	mov    %ebx,%eax
f0104c1e:	89 fa                	mov    %edi,%edx
f0104c20:	83 c4 1c             	add    $0x1c,%esp
f0104c23:	5b                   	pop    %ebx
f0104c24:	5e                   	pop    %esi
f0104c25:	5f                   	pop    %edi
f0104c26:	5d                   	pop    %ebp
f0104c27:	c3                   	ret    
f0104c28:	90                   	nop
f0104c29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0104c30:	89 d8                	mov    %ebx,%eax
f0104c32:	f7 f7                	div    %edi
f0104c34:	31 ff                	xor    %edi,%edi
f0104c36:	89 c3                	mov    %eax,%ebx
f0104c38:	89 d8                	mov    %ebx,%eax
f0104c3a:	89 fa                	mov    %edi,%edx
f0104c3c:	83 c4 1c             	add    $0x1c,%esp
f0104c3f:	5b                   	pop    %ebx
f0104c40:	5e                   	pop    %esi
f0104c41:	5f                   	pop    %edi
f0104c42:	5d                   	pop    %ebp
f0104c43:	c3                   	ret    
f0104c44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104c48:	39 ce                	cmp    %ecx,%esi
f0104c4a:	72 0c                	jb     f0104c58 <__udivdi3+0x118>
f0104c4c:	31 db                	xor    %ebx,%ebx
f0104c4e:	3b 44 24 08          	cmp    0x8(%esp),%eax
f0104c52:	0f 87 34 ff ff ff    	ja     f0104b8c <__udivdi3+0x4c>
f0104c58:	bb 01 00 00 00       	mov    $0x1,%ebx
f0104c5d:	e9 2a ff ff ff       	jmp    f0104b8c <__udivdi3+0x4c>
f0104c62:	66 90                	xchg   %ax,%ax
f0104c64:	66 90                	xchg   %ax,%ax
f0104c66:	66 90                	xchg   %ax,%ax
f0104c68:	66 90                	xchg   %ax,%ax
f0104c6a:	66 90                	xchg   %ax,%ax
f0104c6c:	66 90                	xchg   %ax,%ax
f0104c6e:	66 90                	xchg   %ax,%ax

f0104c70 <__umoddi3>:
f0104c70:	55                   	push   %ebp
f0104c71:	57                   	push   %edi
f0104c72:	56                   	push   %esi
f0104c73:	53                   	push   %ebx
f0104c74:	83 ec 1c             	sub    $0x1c,%esp
f0104c77:	8b 54 24 3c          	mov    0x3c(%esp),%edx
f0104c7b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f0104c7f:	8b 74 24 34          	mov    0x34(%esp),%esi
f0104c83:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0104c87:	85 d2                	test   %edx,%edx
f0104c89:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f0104c8d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104c91:	89 f3                	mov    %esi,%ebx
f0104c93:	89 3c 24             	mov    %edi,(%esp)
f0104c96:	89 74 24 04          	mov    %esi,0x4(%esp)
f0104c9a:	75 1c                	jne    f0104cb8 <__umoddi3+0x48>
f0104c9c:	39 f7                	cmp    %esi,%edi
f0104c9e:	76 50                	jbe    f0104cf0 <__umoddi3+0x80>
f0104ca0:	89 c8                	mov    %ecx,%eax
f0104ca2:	89 f2                	mov    %esi,%edx
f0104ca4:	f7 f7                	div    %edi
f0104ca6:	89 d0                	mov    %edx,%eax
f0104ca8:	31 d2                	xor    %edx,%edx
f0104caa:	83 c4 1c             	add    $0x1c,%esp
f0104cad:	5b                   	pop    %ebx
f0104cae:	5e                   	pop    %esi
f0104caf:	5f                   	pop    %edi
f0104cb0:	5d                   	pop    %ebp
f0104cb1:	c3                   	ret    
f0104cb2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f0104cb8:	39 f2                	cmp    %esi,%edx
f0104cba:	89 d0                	mov    %edx,%eax
f0104cbc:	77 52                	ja     f0104d10 <__umoddi3+0xa0>
f0104cbe:	0f bd ea             	bsr    %edx,%ebp
f0104cc1:	83 f5 1f             	xor    $0x1f,%ebp
f0104cc4:	75 5a                	jne    f0104d20 <__umoddi3+0xb0>
f0104cc6:	3b 54 24 04          	cmp    0x4(%esp),%edx
f0104cca:	0f 82 e0 00 00 00    	jb     f0104db0 <__umoddi3+0x140>
f0104cd0:	39 0c 24             	cmp    %ecx,(%esp)
f0104cd3:	0f 86 d7 00 00 00    	jbe    f0104db0 <__umoddi3+0x140>
f0104cd9:	8b 44 24 08          	mov    0x8(%esp),%eax
f0104cdd:	8b 54 24 04          	mov    0x4(%esp),%edx
f0104ce1:	83 c4 1c             	add    $0x1c,%esp
f0104ce4:	5b                   	pop    %ebx
f0104ce5:	5e                   	pop    %esi
f0104ce6:	5f                   	pop    %edi
f0104ce7:	5d                   	pop    %ebp
f0104ce8:	c3                   	ret    
f0104ce9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0104cf0:	85 ff                	test   %edi,%edi
f0104cf2:	89 fd                	mov    %edi,%ebp
f0104cf4:	75 0b                	jne    f0104d01 <__umoddi3+0x91>
f0104cf6:	b8 01 00 00 00       	mov    $0x1,%eax
f0104cfb:	31 d2                	xor    %edx,%edx
f0104cfd:	f7 f7                	div    %edi
f0104cff:	89 c5                	mov    %eax,%ebp
f0104d01:	89 f0                	mov    %esi,%eax
f0104d03:	31 d2                	xor    %edx,%edx
f0104d05:	f7 f5                	div    %ebp
f0104d07:	89 c8                	mov    %ecx,%eax
f0104d09:	f7 f5                	div    %ebp
f0104d0b:	89 d0                	mov    %edx,%eax
f0104d0d:	eb 99                	jmp    f0104ca8 <__umoddi3+0x38>
f0104d0f:	90                   	nop
f0104d10:	89 c8                	mov    %ecx,%eax
f0104d12:	89 f2                	mov    %esi,%edx
f0104d14:	83 c4 1c             	add    $0x1c,%esp
f0104d17:	5b                   	pop    %ebx
f0104d18:	5e                   	pop    %esi
f0104d19:	5f                   	pop    %edi
f0104d1a:	5d                   	pop    %ebp
f0104d1b:	c3                   	ret    
f0104d1c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0104d20:	8b 34 24             	mov    (%esp),%esi
f0104d23:	bf 20 00 00 00       	mov    $0x20,%edi
f0104d28:	89 e9                	mov    %ebp,%ecx
f0104d2a:	29 ef                	sub    %ebp,%edi
f0104d2c:	d3 e0                	shl    %cl,%eax
f0104d2e:	89 f9                	mov    %edi,%ecx
f0104d30:	89 f2                	mov    %esi,%edx
f0104d32:	d3 ea                	shr    %cl,%edx
f0104d34:	89 e9                	mov    %ebp,%ecx
f0104d36:	09 c2                	or     %eax,%edx
f0104d38:	89 d8                	mov    %ebx,%eax
f0104d3a:	89 14 24             	mov    %edx,(%esp)
f0104d3d:	89 f2                	mov    %esi,%edx
f0104d3f:	d3 e2                	shl    %cl,%edx
f0104d41:	89 f9                	mov    %edi,%ecx
f0104d43:	89 54 24 04          	mov    %edx,0x4(%esp)
f0104d47:	8b 54 24 0c          	mov    0xc(%esp),%edx
f0104d4b:	d3 e8                	shr    %cl,%eax
f0104d4d:	89 e9                	mov    %ebp,%ecx
f0104d4f:	89 c6                	mov    %eax,%esi
f0104d51:	d3 e3                	shl    %cl,%ebx
f0104d53:	89 f9                	mov    %edi,%ecx
f0104d55:	89 d0                	mov    %edx,%eax
f0104d57:	d3 e8                	shr    %cl,%eax
f0104d59:	89 e9                	mov    %ebp,%ecx
f0104d5b:	09 d8                	or     %ebx,%eax
f0104d5d:	89 d3                	mov    %edx,%ebx
f0104d5f:	89 f2                	mov    %esi,%edx
f0104d61:	f7 34 24             	divl   (%esp)
f0104d64:	89 d6                	mov    %edx,%esi
f0104d66:	d3 e3                	shl    %cl,%ebx
f0104d68:	f7 64 24 04          	mull   0x4(%esp)
f0104d6c:	39 d6                	cmp    %edx,%esi
f0104d6e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0104d72:	89 d1                	mov    %edx,%ecx
f0104d74:	89 c3                	mov    %eax,%ebx
f0104d76:	72 08                	jb     f0104d80 <__umoddi3+0x110>
f0104d78:	75 11                	jne    f0104d8b <__umoddi3+0x11b>
f0104d7a:	39 44 24 08          	cmp    %eax,0x8(%esp)
f0104d7e:	73 0b                	jae    f0104d8b <__umoddi3+0x11b>
f0104d80:	2b 44 24 04          	sub    0x4(%esp),%eax
f0104d84:	1b 14 24             	sbb    (%esp),%edx
f0104d87:	89 d1                	mov    %edx,%ecx
f0104d89:	89 c3                	mov    %eax,%ebx
f0104d8b:	8b 54 24 08          	mov    0x8(%esp),%edx
f0104d8f:	29 da                	sub    %ebx,%edx
f0104d91:	19 ce                	sbb    %ecx,%esi
f0104d93:	89 f9                	mov    %edi,%ecx
f0104d95:	89 f0                	mov    %esi,%eax
f0104d97:	d3 e0                	shl    %cl,%eax
f0104d99:	89 e9                	mov    %ebp,%ecx
f0104d9b:	d3 ea                	shr    %cl,%edx
f0104d9d:	89 e9                	mov    %ebp,%ecx
f0104d9f:	d3 ee                	shr    %cl,%esi
f0104da1:	09 d0                	or     %edx,%eax
f0104da3:	89 f2                	mov    %esi,%edx
f0104da5:	83 c4 1c             	add    $0x1c,%esp
f0104da8:	5b                   	pop    %ebx
f0104da9:	5e                   	pop    %esi
f0104daa:	5f                   	pop    %edi
f0104dab:	5d                   	pop    %ebp
f0104dac:	c3                   	ret    
f0104dad:	8d 76 00             	lea    0x0(%esi),%esi
f0104db0:	29 f9                	sub    %edi,%ecx
f0104db2:	19 d6                	sbb    %edx,%esi
f0104db4:	89 74 24 04          	mov    %esi,0x4(%esp)
f0104db8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0104dbc:	e9 18 ff ff ff       	jmp    f0104cd9 <__umoddi3+0x69>
