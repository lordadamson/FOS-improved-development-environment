
obj/user/fos_alloc:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 c6 01 00 00       	call   8001fc <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:

#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	53                   	push   %ebx
  80003c:	83 ec 24             	sub    $0x24,%esp
	int size = 10 ;
  80003f:	c7 45 f0 0a 00 00 00 	movl   $0xa,-0x10(%ebp)
	int *x = malloc(sizeof(int)*size) ;
  800046:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800049:	c1 e0 02             	shl    $0x2,%eax
  80004c:	83 ec 0c             	sub    $0xc,%esp
  80004f:	50                   	push   %eax
  800050:	e8 e5 0e 00 00       	call   800f3a <malloc>
  800055:	83 c4 10             	add    $0x10,%esp
  800058:	89 45 ec             	mov    %eax,-0x14(%ebp)
	int *y = malloc(sizeof(int)*size) ;
  80005b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80005e:	c1 e0 02             	shl    $0x2,%eax
  800061:	83 ec 0c             	sub    $0xc,%esp
  800064:	50                   	push   %eax
  800065:	e8 d0 0e 00 00       	call   800f3a <malloc>
  80006a:	83 c4 10             	add    $0x10,%esp
  80006d:	89 45 e8             	mov    %eax,-0x18(%ebp)
	int *z = malloc(sizeof(int)*size) ;
  800070:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800073:	c1 e0 02             	shl    $0x2,%eax
  800076:	83 ec 0c             	sub    $0xc,%esp
  800079:	50                   	push   %eax
  80007a:	e8 bb 0e 00 00       	call   800f3a <malloc>
  80007f:	83 c4 10             	add    $0x10,%esp
  800082:	89 45 e4             	mov    %eax,-0x1c(%ebp)

	int i ;
	for (i = 0 ; i < size ; i++)
  800085:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  80008c:	eb 63                	jmp    8000f1 <_main+0xb9>
	{
		x[i] = i ;
  80008e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800091:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800098:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80009b:	01 c2                	add    %eax,%edx
  80009d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000a0:	89 02                	mov    %eax,(%edx)
		y[i] = 10 ;
  8000a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000a5:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000ac:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8000af:	01 d0                	add    %edx,%eax
  8000b1:	c7 00 0a 00 00 00    	movl   $0xa,(%eax)
		z[i] = (int)x[i]  * y[i]  ;
  8000b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000ba:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8000c1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8000c4:	01 c2                	add    %eax,%edx
  8000c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000c9:	8d 0c 85 00 00 00 00 	lea    0x0(,%eax,4),%ecx
  8000d0:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8000d3:	01 c8                	add    %ecx,%eax
  8000d5:	8b 08                	mov    (%eax),%ecx
  8000d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000da:	8d 1c 85 00 00 00 00 	lea    0x0(,%eax,4),%ebx
  8000e1:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8000e4:	01 d8                	add    %ebx,%eax
  8000e6:	8b 00                	mov    (%eax),%eax
  8000e8:	0f af c1             	imul   %ecx,%eax
  8000eb:	89 02                	mov    %eax,(%edx)
	int *x = malloc(sizeof(int)*size) ;
	int *y = malloc(sizeof(int)*size) ;
	int *z = malloc(sizeof(int)*size) ;

	int i ;
	for (i = 0 ; i < size ; i++)
  8000ed:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8000f1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8000f4:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8000f7:	7c 95                	jl     80008e <_main+0x56>
		x[i] = i ;
		y[i] = 10 ;
		z[i] = (int)x[i]  * y[i]  ;
	}
	
	for (i = 0 ; i < size ; i++)
  8000f9:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  800100:	eb 47                	jmp    800149 <_main+0x111>
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
  800102:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800105:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80010c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  80010f:	01 d0                	add    %edx,%eax
  800111:	8b 08                	mov    (%eax),%ecx
  800113:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800116:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80011d:	8b 45 e8             	mov    -0x18(%ebp),%eax
  800120:	01 d0                	add    %edx,%eax
  800122:	8b 10                	mov    (%eax),%edx
  800124:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800127:	8d 1c 85 00 00 00 00 	lea    0x0(,%eax,4),%ebx
  80012e:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800131:	01 d8                	add    %ebx,%eax
  800133:	8b 00                	mov    (%eax),%eax
  800135:	51                   	push   %ecx
  800136:	52                   	push   %edx
  800137:	50                   	push   %eax
  800138:	68 00 14 80 00       	push   $0x801400
  80013d:	e8 d2 01 00 00       	call   800314 <cprintf>
  800142:	83 c4 10             	add    $0x10,%esp
		x[i] = i ;
		y[i] = 10 ;
		z[i] = (int)x[i]  * y[i]  ;
	}
	
	for (i = 0 ; i < size ; i++)
  800145:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  800149:	8b 45 f4             	mov    -0xc(%ebp),%eax
  80014c:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  80014f:	7c b1                	jl     800102 <_main+0xca>
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
	
	freeHeap();
  800151:	e8 fe 0d 00 00       	call   800f54 <freeHeap>
	cprintf("the heap is freed successfully\n");
  800156:	83 ec 0c             	sub    $0xc,%esp
  800159:	68 10 14 80 00       	push   $0x801410
  80015e:	e8 b1 01 00 00       	call   800314 <cprintf>
  800163:	83 c4 10             	add    $0x10,%esp
	z = malloc(sizeof(int)*size) ;
  800166:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800169:	c1 e0 02             	shl    $0x2,%eax
  80016c:	83 ec 0c             	sub    $0xc,%esp
  80016f:	50                   	push   %eax
  800170:	e8 c5 0d 00 00       	call   800f3a <malloc>
  800175:	83 c4 10             	add    $0x10,%esp
  800178:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	for (i = 0 ; i < size ; i++)
  80017b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  800182:	eb 6a                	jmp    8001ee <_main+0x1b6>
	{
		cprintf("x[i] = %d\t",x[i]);
  800184:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800187:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  80018e:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800191:	01 d0                	add    %edx,%eax
  800193:	8b 00                	mov    (%eax),%eax
  800195:	83 ec 08             	sub    $0x8,%esp
  800198:	50                   	push   %eax
  800199:	68 30 14 80 00       	push   $0x801430
  80019e:	e8 71 01 00 00       	call   800314 <cprintf>
  8001a3:	83 c4 10             	add    $0x10,%esp
		cprintf("y[i] = %d\t",y[i]);
  8001a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8001a9:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001b0:	8b 45 e8             	mov    -0x18(%ebp),%eax
  8001b3:	01 d0                	add    %edx,%eax
  8001b5:	8b 00                	mov    (%eax),%eax
  8001b7:	83 ec 08             	sub    $0x8,%esp
  8001ba:	50                   	push   %eax
  8001bb:	68 3b 14 80 00       	push   $0x80143b
  8001c0:	e8 4f 01 00 00       	call   800314 <cprintf>
  8001c5:	83 c4 10             	add    $0x10,%esp
		cprintf("z[i] = %d\n",z[i]);
  8001c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8001cb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  8001d2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  8001d5:	01 d0                	add    %edx,%eax
  8001d7:	8b 00                	mov    (%eax),%eax
  8001d9:	83 ec 08             	sub    $0x8,%esp
  8001dc:	50                   	push   %eax
  8001dd:	68 46 14 80 00       	push   $0x801446
  8001e2:	e8 2d 01 00 00       	call   800314 <cprintf>
  8001e7:	83 c4 10             	add    $0x10,%esp
		cprintf("%d * %d = %d\n",x[i], y[i], z[i]);
	
	freeHeap();
	cprintf("the heap is freed successfully\n");
	z = malloc(sizeof(int)*size) ;
	for (i = 0 ; i < size ; i++)
  8001ea:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8001ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8001f1:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8001f4:	7c 8e                	jl     800184 <_main+0x14c>
		cprintf("y[i] = %d\t",y[i]);
		cprintf("z[i] = %d\n",z[i]);
	
	}

	return;	
  8001f6:	90                   	nop
}
  8001f7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8001fa:	c9                   	leave  
  8001fb:	c3                   	ret    

008001fc <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8001fc:	55                   	push   %ebp
  8001fd:	89 e5                	mov    %esp,%ebp
  8001ff:	83 ec 08             	sub    $0x8,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  800202:	c7 05 08 20 80 00 00 	movl   $0xeec00000,0x802008
  800209:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  80020c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800210:	7e 0a                	jle    80021c <libmain+0x20>
		binaryname = argv[0];
  800212:	8b 45 0c             	mov    0xc(%ebp),%eax
  800215:	8b 00                	mov    (%eax),%eax
  800217:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  80021c:	83 ec 08             	sub    $0x8,%esp
  80021f:	ff 75 0c             	pushl  0xc(%ebp)
  800222:	ff 75 08             	pushl  0x8(%ebp)
  800225:	e8 0e fe ff ff       	call   800038 <_main>
  80022a:	83 c4 10             	add    $0x10,%esp

	// exit gracefully
	//exit();
	sleep();
  80022d:	e8 19 00 00 00       	call   80024b <sleep>
}
  800232:	90                   	nop
  800233:	c9                   	leave  
  800234:	c3                   	ret    

00800235 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800235:	55                   	push   %ebp
  800236:	89 e5                	mov    %esp,%ebp
  800238:	83 ec 08             	sub    $0x8,%esp
	sys_env_destroy(0);	
  80023b:	83 ec 0c             	sub    $0xc,%esp
  80023e:	6a 00                	push   $0x0
  800240:	e8 8a 0d 00 00       	call   800fcf <sys_env_destroy>
  800245:	83 c4 10             	add    $0x10,%esp
}
  800248:	90                   	nop
  800249:	c9                   	leave  
  80024a:	c3                   	ret    

0080024b <sleep>:

void
sleep(void)
{	
  80024b:	55                   	push   %ebp
  80024c:	89 e5                	mov    %esp,%ebp
  80024e:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  800251:	e8 ad 0d 00 00       	call   801003 <sys_env_sleep>
}
  800256:	90                   	nop
  800257:	c9                   	leave  
  800258:	c3                   	ret    

00800259 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800259:	55                   	push   %ebp
  80025a:	89 e5                	mov    %esp,%ebp
  80025c:	83 ec 08             	sub    $0x8,%esp
	b->buf[b->idx++] = ch;
  80025f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800262:	8b 00                	mov    (%eax),%eax
  800264:	8d 48 01             	lea    0x1(%eax),%ecx
  800267:	8b 55 0c             	mov    0xc(%ebp),%edx
  80026a:	89 0a                	mov    %ecx,(%edx)
  80026c:	8b 55 08             	mov    0x8(%ebp),%edx
  80026f:	89 d1                	mov    %edx,%ecx
  800271:	8b 55 0c             	mov    0xc(%ebp),%edx
  800274:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800278:	8b 45 0c             	mov    0xc(%ebp),%eax
  80027b:	8b 00                	mov    (%eax),%eax
  80027d:	3d ff 00 00 00       	cmp    $0xff,%eax
  800282:	75 23                	jne    8002a7 <putch+0x4e>
		sys_cputs(b->buf, b->idx);
  800284:	8b 45 0c             	mov    0xc(%ebp),%eax
  800287:	8b 00                	mov    (%eax),%eax
  800289:	89 c2                	mov    %eax,%edx
  80028b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80028e:	83 c0 08             	add    $0x8,%eax
  800291:	83 ec 08             	sub    $0x8,%esp
  800294:	52                   	push   %edx
  800295:	50                   	push   %eax
  800296:	e8 fe 0c 00 00       	call   800f99 <sys_cputs>
  80029b:	83 c4 10             	add    $0x10,%esp
		b->idx = 0;
  80029e:	8b 45 0c             	mov    0xc(%ebp),%eax
  8002a1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  8002a7:	8b 45 0c             	mov    0xc(%ebp),%eax
  8002aa:	8b 40 04             	mov    0x4(%eax),%eax
  8002ad:	8d 50 01             	lea    0x1(%eax),%edx
  8002b0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8002b3:	89 50 04             	mov    %edx,0x4(%eax)
}
  8002b6:	90                   	nop
  8002b7:	c9                   	leave  
  8002b8:	c3                   	ret    

008002b9 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  8002b9:	55                   	push   %ebp
  8002ba:	89 e5                	mov    %esp,%ebp
  8002bc:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  8002c2:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  8002c9:	00 00 00 
	b.cnt = 0;
  8002cc:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8002d3:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8002d6:	ff 75 0c             	pushl  0xc(%ebp)
  8002d9:	ff 75 08             	pushl  0x8(%ebp)
  8002dc:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8002e2:	50                   	push   %eax
  8002e3:	68 59 02 80 00       	push   $0x800259
  8002e8:	e8 cc 01 00 00       	call   8004b9 <vprintfmt>
  8002ed:	83 c4 10             	add    $0x10,%esp
	sys_cputs(b.buf, b.idx);
  8002f0:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8002f6:	83 ec 08             	sub    $0x8,%esp
  8002f9:	50                   	push   %eax
  8002fa:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800300:	83 c0 08             	add    $0x8,%eax
  800303:	50                   	push   %eax
  800304:	e8 90 0c 00 00       	call   800f99 <sys_cputs>
  800309:	83 c4 10             	add    $0x10,%esp

	return b.cnt;
  80030c:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  800312:	c9                   	leave  
  800313:	c3                   	ret    

00800314 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800314:	55                   	push   %ebp
  800315:	89 e5                	mov    %esp,%ebp
  800317:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80031a:	8d 45 0c             	lea    0xc(%ebp),%eax
  80031d:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  800320:	8b 45 08             	mov    0x8(%ebp),%eax
  800323:	83 ec 08             	sub    $0x8,%esp
  800326:	ff 75 f4             	pushl  -0xc(%ebp)
  800329:	50                   	push   %eax
  80032a:	e8 8a ff ff ff       	call   8002b9 <vcprintf>
  80032f:	83 c4 10             	add    $0x10,%esp
  800332:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  800335:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800338:	c9                   	leave  
  800339:	c3                   	ret    

0080033a <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80033a:	55                   	push   %ebp
  80033b:	89 e5                	mov    %esp,%ebp
  80033d:	53                   	push   %ebx
  80033e:	83 ec 14             	sub    $0x14,%esp
  800341:	8b 45 10             	mov    0x10(%ebp),%eax
  800344:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800347:	8b 45 14             	mov    0x14(%ebp),%eax
  80034a:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80034d:	8b 45 18             	mov    0x18(%ebp),%eax
  800350:	ba 00 00 00 00       	mov    $0x0,%edx
  800355:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800358:	77 55                	ja     8003af <printnum+0x75>
  80035a:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80035d:	72 05                	jb     800364 <printnum+0x2a>
  80035f:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800362:	77 4b                	ja     8003af <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800364:	8b 45 1c             	mov    0x1c(%ebp),%eax
  800367:	8d 58 ff             	lea    -0x1(%eax),%ebx
  80036a:	8b 45 18             	mov    0x18(%ebp),%eax
  80036d:	ba 00 00 00 00       	mov    $0x0,%edx
  800372:	52                   	push   %edx
  800373:	50                   	push   %eax
  800374:	ff 75 f4             	pushl  -0xc(%ebp)
  800377:	ff 75 f0             	pushl  -0x10(%ebp)
  80037a:	e8 e1 0d 00 00       	call   801160 <__udivdi3>
  80037f:	83 c4 10             	add    $0x10,%esp
  800382:	83 ec 04             	sub    $0x4,%esp
  800385:	ff 75 20             	pushl  0x20(%ebp)
  800388:	53                   	push   %ebx
  800389:	ff 75 18             	pushl  0x18(%ebp)
  80038c:	52                   	push   %edx
  80038d:	50                   	push   %eax
  80038e:	ff 75 0c             	pushl  0xc(%ebp)
  800391:	ff 75 08             	pushl  0x8(%ebp)
  800394:	e8 a1 ff ff ff       	call   80033a <printnum>
  800399:	83 c4 20             	add    $0x20,%esp
  80039c:	eb 1b                	jmp    8003b9 <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80039e:	83 ec 08             	sub    $0x8,%esp
  8003a1:	ff 75 0c             	pushl  0xc(%ebp)
  8003a4:	ff 75 20             	pushl  0x20(%ebp)
  8003a7:	8b 45 08             	mov    0x8(%ebp),%eax
  8003aa:	ff d0                	call   *%eax
  8003ac:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  8003af:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  8003b3:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  8003b7:	7f e5                	jg     80039e <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  8003b9:	8b 4d 18             	mov    0x18(%ebp),%ecx
  8003bc:	bb 00 00 00 00       	mov    $0x0,%ebx
  8003c1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8003c4:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8003c7:	53                   	push   %ebx
  8003c8:	51                   	push   %ecx
  8003c9:	52                   	push   %edx
  8003ca:	50                   	push   %eax
  8003cb:	e8 c0 0e 00 00       	call   801290 <__umoddi3>
  8003d0:	83 c4 10             	add    $0x10,%esp
  8003d3:	05 20 15 80 00       	add    $0x801520,%eax
  8003d8:	0f b6 00             	movzbl (%eax),%eax
  8003db:	0f be c0             	movsbl %al,%eax
  8003de:	83 ec 08             	sub    $0x8,%esp
  8003e1:	ff 75 0c             	pushl  0xc(%ebp)
  8003e4:	50                   	push   %eax
  8003e5:	8b 45 08             	mov    0x8(%ebp),%eax
  8003e8:	ff d0                	call   *%eax
  8003ea:	83 c4 10             	add    $0x10,%esp
}
  8003ed:	90                   	nop
  8003ee:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8003f1:	c9                   	leave  
  8003f2:	c3                   	ret    

008003f3 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8003f3:	55                   	push   %ebp
  8003f4:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8003f6:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8003fa:	7e 1c                	jle    800418 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8003fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8003ff:	8b 00                	mov    (%eax),%eax
  800401:	8d 50 08             	lea    0x8(%eax),%edx
  800404:	8b 45 08             	mov    0x8(%ebp),%eax
  800407:	89 10                	mov    %edx,(%eax)
  800409:	8b 45 08             	mov    0x8(%ebp),%eax
  80040c:	8b 00                	mov    (%eax),%eax
  80040e:	83 e8 08             	sub    $0x8,%eax
  800411:	8b 50 04             	mov    0x4(%eax),%edx
  800414:	8b 00                	mov    (%eax),%eax
  800416:	eb 40                	jmp    800458 <getuint+0x65>
	else if (lflag)
  800418:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80041c:	74 1e                	je     80043c <getuint+0x49>
		return va_arg(*ap, unsigned long);
  80041e:	8b 45 08             	mov    0x8(%ebp),%eax
  800421:	8b 00                	mov    (%eax),%eax
  800423:	8d 50 04             	lea    0x4(%eax),%edx
  800426:	8b 45 08             	mov    0x8(%ebp),%eax
  800429:	89 10                	mov    %edx,(%eax)
  80042b:	8b 45 08             	mov    0x8(%ebp),%eax
  80042e:	8b 00                	mov    (%eax),%eax
  800430:	83 e8 04             	sub    $0x4,%eax
  800433:	8b 00                	mov    (%eax),%eax
  800435:	ba 00 00 00 00       	mov    $0x0,%edx
  80043a:	eb 1c                	jmp    800458 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  80043c:	8b 45 08             	mov    0x8(%ebp),%eax
  80043f:	8b 00                	mov    (%eax),%eax
  800441:	8d 50 04             	lea    0x4(%eax),%edx
  800444:	8b 45 08             	mov    0x8(%ebp),%eax
  800447:	89 10                	mov    %edx,(%eax)
  800449:	8b 45 08             	mov    0x8(%ebp),%eax
  80044c:	8b 00                	mov    (%eax),%eax
  80044e:	83 e8 04             	sub    $0x4,%eax
  800451:	8b 00                	mov    (%eax),%eax
  800453:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800458:	5d                   	pop    %ebp
  800459:	c3                   	ret    

0080045a <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  80045a:	55                   	push   %ebp
  80045b:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80045d:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800461:	7e 1c                	jle    80047f <getint+0x25>
		return va_arg(*ap, long long);
  800463:	8b 45 08             	mov    0x8(%ebp),%eax
  800466:	8b 00                	mov    (%eax),%eax
  800468:	8d 50 08             	lea    0x8(%eax),%edx
  80046b:	8b 45 08             	mov    0x8(%ebp),%eax
  80046e:	89 10                	mov    %edx,(%eax)
  800470:	8b 45 08             	mov    0x8(%ebp),%eax
  800473:	8b 00                	mov    (%eax),%eax
  800475:	83 e8 08             	sub    $0x8,%eax
  800478:	8b 50 04             	mov    0x4(%eax),%edx
  80047b:	8b 00                	mov    (%eax),%eax
  80047d:	eb 38                	jmp    8004b7 <getint+0x5d>
	else if (lflag)
  80047f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800483:	74 1a                	je     80049f <getint+0x45>
		return va_arg(*ap, long);
  800485:	8b 45 08             	mov    0x8(%ebp),%eax
  800488:	8b 00                	mov    (%eax),%eax
  80048a:	8d 50 04             	lea    0x4(%eax),%edx
  80048d:	8b 45 08             	mov    0x8(%ebp),%eax
  800490:	89 10                	mov    %edx,(%eax)
  800492:	8b 45 08             	mov    0x8(%ebp),%eax
  800495:	8b 00                	mov    (%eax),%eax
  800497:	83 e8 04             	sub    $0x4,%eax
  80049a:	8b 00                	mov    (%eax),%eax
  80049c:	99                   	cltd   
  80049d:	eb 18                	jmp    8004b7 <getint+0x5d>
	else
		return va_arg(*ap, int);
  80049f:	8b 45 08             	mov    0x8(%ebp),%eax
  8004a2:	8b 00                	mov    (%eax),%eax
  8004a4:	8d 50 04             	lea    0x4(%eax),%edx
  8004a7:	8b 45 08             	mov    0x8(%ebp),%eax
  8004aa:	89 10                	mov    %edx,(%eax)
  8004ac:	8b 45 08             	mov    0x8(%ebp),%eax
  8004af:	8b 00                	mov    (%eax),%eax
  8004b1:	83 e8 04             	sub    $0x4,%eax
  8004b4:	8b 00                	mov    (%eax),%eax
  8004b6:	99                   	cltd   
}
  8004b7:	5d                   	pop    %ebp
  8004b8:	c3                   	ret    

008004b9 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  8004b9:	55                   	push   %ebp
  8004ba:	89 e5                	mov    %esp,%ebp
  8004bc:	56                   	push   %esi
  8004bd:	53                   	push   %ebx
  8004be:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004c1:	eb 17                	jmp    8004da <vprintfmt+0x21>
			if (ch == '\0')
  8004c3:	85 db                	test   %ebx,%ebx
  8004c5:	0f 84 be 03 00 00    	je     800889 <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
  8004cb:	83 ec 08             	sub    $0x8,%esp
  8004ce:	ff 75 0c             	pushl  0xc(%ebp)
  8004d1:	53                   	push   %ebx
  8004d2:	8b 45 08             	mov    0x8(%ebp),%eax
  8004d5:	ff d0                	call   *%eax
  8004d7:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8004da:	8b 45 10             	mov    0x10(%ebp),%eax
  8004dd:	8d 50 01             	lea    0x1(%eax),%edx
  8004e0:	89 55 10             	mov    %edx,0x10(%ebp)
  8004e3:	0f b6 00             	movzbl (%eax),%eax
  8004e6:	0f b6 d8             	movzbl %al,%ebx
  8004e9:	83 fb 25             	cmp    $0x25,%ebx
  8004ec:	75 d5                	jne    8004c3 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8004ee:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8004f2:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8004f9:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  800500:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  800507:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  80050e:	8b 45 10             	mov    0x10(%ebp),%eax
  800511:	8d 50 01             	lea    0x1(%eax),%edx
  800514:	89 55 10             	mov    %edx,0x10(%ebp)
  800517:	0f b6 00             	movzbl (%eax),%eax
  80051a:	0f b6 d8             	movzbl %al,%ebx
  80051d:	8d 43 dd             	lea    -0x23(%ebx),%eax
  800520:	83 f8 55             	cmp    $0x55,%eax
  800523:	0f 87 33 03 00 00    	ja     80085c <vprintfmt+0x3a3>
  800529:	8b 04 85 44 15 80 00 	mov    0x801544(,%eax,4),%eax
  800530:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  800532:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  800536:	eb d6                	jmp    80050e <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800538:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  80053c:	eb d0                	jmp    80050e <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80053e:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  800545:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800548:	89 d0                	mov    %edx,%eax
  80054a:	c1 e0 02             	shl    $0x2,%eax
  80054d:	01 d0                	add    %edx,%eax
  80054f:	01 c0                	add    %eax,%eax
  800551:	01 d8                	add    %ebx,%eax
  800553:	83 e8 30             	sub    $0x30,%eax
  800556:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  800559:	8b 45 10             	mov    0x10(%ebp),%eax
  80055c:	0f b6 00             	movzbl (%eax),%eax
  80055f:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  800562:	83 fb 2f             	cmp    $0x2f,%ebx
  800565:	7e 3f                	jle    8005a6 <vprintfmt+0xed>
  800567:	83 fb 39             	cmp    $0x39,%ebx
  80056a:	7f 3a                	jg     8005a6 <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80056c:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800570:	eb d3                	jmp    800545 <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800572:	8b 45 14             	mov    0x14(%ebp),%eax
  800575:	83 c0 04             	add    $0x4,%eax
  800578:	89 45 14             	mov    %eax,0x14(%ebp)
  80057b:	8b 45 14             	mov    0x14(%ebp),%eax
  80057e:	83 e8 04             	sub    $0x4,%eax
  800581:	8b 00                	mov    (%eax),%eax
  800583:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800586:	eb 1f                	jmp    8005a7 <vprintfmt+0xee>

		case '.':
			if (width < 0)
  800588:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80058c:	79 80                	jns    80050e <vprintfmt+0x55>
				width = 0;
  80058e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  800595:	e9 74 ff ff ff       	jmp    80050e <vprintfmt+0x55>

		case '#':
			altflag = 1;
  80059a:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  8005a1:	e9 68 ff ff ff       	jmp    80050e <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
  8005a6:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
  8005a7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ab:	0f 89 5d ff ff ff    	jns    80050e <vprintfmt+0x55>
				width = precision, precision = -1;
  8005b1:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8005b4:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  8005b7:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  8005be:	e9 4b ff ff ff       	jmp    80050e <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  8005c3:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  8005c7:	e9 42 ff ff ff       	jmp    80050e <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  8005cc:	8b 45 14             	mov    0x14(%ebp),%eax
  8005cf:	83 c0 04             	add    $0x4,%eax
  8005d2:	89 45 14             	mov    %eax,0x14(%ebp)
  8005d5:	8b 45 14             	mov    0x14(%ebp),%eax
  8005d8:	83 e8 04             	sub    $0x4,%eax
  8005db:	8b 00                	mov    (%eax),%eax
  8005dd:	83 ec 08             	sub    $0x8,%esp
  8005e0:	ff 75 0c             	pushl  0xc(%ebp)
  8005e3:	50                   	push   %eax
  8005e4:	8b 45 08             	mov    0x8(%ebp),%eax
  8005e7:	ff d0                	call   *%eax
  8005e9:	83 c4 10             	add    $0x10,%esp
			break;
  8005ec:	e9 93 02 00 00       	jmp    800884 <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8005f1:	8b 45 14             	mov    0x14(%ebp),%eax
  8005f4:	83 c0 04             	add    $0x4,%eax
  8005f7:	89 45 14             	mov    %eax,0x14(%ebp)
  8005fa:	8b 45 14             	mov    0x14(%ebp),%eax
  8005fd:	83 e8 04             	sub    $0x4,%eax
  800600:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  800602:	85 db                	test   %ebx,%ebx
  800604:	79 02                	jns    800608 <vprintfmt+0x14f>
				err = -err;
  800606:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  800608:	83 fb 07             	cmp    $0x7,%ebx
  80060b:	7f 0b                	jg     800618 <vprintfmt+0x15f>
  80060d:	8b 34 9d 00 15 80 00 	mov    0x801500(,%ebx,4),%esi
  800614:	85 f6                	test   %esi,%esi
  800616:	75 19                	jne    800631 <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
  800618:	53                   	push   %ebx
  800619:	68 31 15 80 00       	push   $0x801531
  80061e:	ff 75 0c             	pushl  0xc(%ebp)
  800621:	ff 75 08             	pushl  0x8(%ebp)
  800624:	e8 68 02 00 00       	call   800891 <printfmt>
  800629:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
  80062c:	e9 53 02 00 00       	jmp    800884 <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  800631:	56                   	push   %esi
  800632:	68 3a 15 80 00       	push   $0x80153a
  800637:	ff 75 0c             	pushl  0xc(%ebp)
  80063a:	ff 75 08             	pushl  0x8(%ebp)
  80063d:	e8 4f 02 00 00       	call   800891 <printfmt>
  800642:	83 c4 10             	add    $0x10,%esp
			break;
  800645:	e9 3a 02 00 00       	jmp    800884 <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80064a:	8b 45 14             	mov    0x14(%ebp),%eax
  80064d:	83 c0 04             	add    $0x4,%eax
  800650:	89 45 14             	mov    %eax,0x14(%ebp)
  800653:	8b 45 14             	mov    0x14(%ebp),%eax
  800656:	83 e8 04             	sub    $0x4,%eax
  800659:	8b 30                	mov    (%eax),%esi
  80065b:	85 f6                	test   %esi,%esi
  80065d:	75 05                	jne    800664 <vprintfmt+0x1ab>
				p = "(null)";
  80065f:	be 3d 15 80 00       	mov    $0x80153d,%esi
			if (width > 0 && padc != '-')
  800664:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800668:	7e 6f                	jle    8006d9 <vprintfmt+0x220>
  80066a:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  80066e:	74 69                	je     8006d9 <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
  800670:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800673:	83 ec 08             	sub    $0x8,%esp
  800676:	50                   	push   %eax
  800677:	56                   	push   %esi
  800678:	e8 19 03 00 00       	call   800996 <strnlen>
  80067d:	83 c4 10             	add    $0x10,%esp
  800680:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  800683:	eb 17                	jmp    80069c <vprintfmt+0x1e3>
					putch(padc, putdat);
  800685:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  800689:	83 ec 08             	sub    $0x8,%esp
  80068c:	ff 75 0c             	pushl  0xc(%ebp)
  80068f:	50                   	push   %eax
  800690:	8b 45 08             	mov    0x8(%ebp),%eax
  800693:	ff d0                	call   *%eax
  800695:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800698:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80069c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8006a0:	7f e3                	jg     800685 <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8006a2:	eb 35                	jmp    8006d9 <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
  8006a4:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8006a8:	74 1c                	je     8006c6 <vprintfmt+0x20d>
  8006aa:	83 fb 1f             	cmp    $0x1f,%ebx
  8006ad:	7e 05                	jle    8006b4 <vprintfmt+0x1fb>
  8006af:	83 fb 7e             	cmp    $0x7e,%ebx
  8006b2:	7e 12                	jle    8006c6 <vprintfmt+0x20d>
					putch('?', putdat);
  8006b4:	83 ec 08             	sub    $0x8,%esp
  8006b7:	ff 75 0c             	pushl  0xc(%ebp)
  8006ba:	6a 3f                	push   $0x3f
  8006bc:	8b 45 08             	mov    0x8(%ebp),%eax
  8006bf:	ff d0                	call   *%eax
  8006c1:	83 c4 10             	add    $0x10,%esp
  8006c4:	eb 0f                	jmp    8006d5 <vprintfmt+0x21c>
				else
					putch(ch, putdat);
  8006c6:	83 ec 08             	sub    $0x8,%esp
  8006c9:	ff 75 0c             	pushl  0xc(%ebp)
  8006cc:	53                   	push   %ebx
  8006cd:	8b 45 08             	mov    0x8(%ebp),%eax
  8006d0:	ff d0                	call   *%eax
  8006d2:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8006d5:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8006d9:	89 f0                	mov    %esi,%eax
  8006db:	8d 70 01             	lea    0x1(%eax),%esi
  8006de:	0f b6 00             	movzbl (%eax),%eax
  8006e1:	0f be d8             	movsbl %al,%ebx
  8006e4:	85 db                	test   %ebx,%ebx
  8006e6:	74 26                	je     80070e <vprintfmt+0x255>
  8006e8:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8006ec:	78 b6                	js     8006a4 <vprintfmt+0x1eb>
  8006ee:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  8006f2:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8006f6:	79 ac                	jns    8006a4 <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8006f8:	eb 14                	jmp    80070e <vprintfmt+0x255>
				putch(' ', putdat);
  8006fa:	83 ec 08             	sub    $0x8,%esp
  8006fd:	ff 75 0c             	pushl  0xc(%ebp)
  800700:	6a 20                	push   $0x20
  800702:	8b 45 08             	mov    0x8(%ebp),%eax
  800705:	ff d0                	call   *%eax
  800707:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80070a:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80070e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800712:	7f e6                	jg     8006fa <vprintfmt+0x241>
				putch(' ', putdat);
			break;
  800714:	e9 6b 01 00 00       	jmp    800884 <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  800719:	83 ec 08             	sub    $0x8,%esp
  80071c:	ff 75 e8             	pushl  -0x18(%ebp)
  80071f:	8d 45 14             	lea    0x14(%ebp),%eax
  800722:	50                   	push   %eax
  800723:	e8 32 fd ff ff       	call   80045a <getint>
  800728:	83 c4 10             	add    $0x10,%esp
  80072b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80072e:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  800731:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800734:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800737:	85 d2                	test   %edx,%edx
  800739:	79 23                	jns    80075e <vprintfmt+0x2a5>
				putch('-', putdat);
  80073b:	83 ec 08             	sub    $0x8,%esp
  80073e:	ff 75 0c             	pushl  0xc(%ebp)
  800741:	6a 2d                	push   $0x2d
  800743:	8b 45 08             	mov    0x8(%ebp),%eax
  800746:	ff d0                	call   *%eax
  800748:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
  80074b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80074e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800751:	f7 d8                	neg    %eax
  800753:	83 d2 00             	adc    $0x0,%edx
  800756:	f7 da                	neg    %edx
  800758:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80075b:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  80075e:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800765:	e9 bc 00 00 00       	jmp    800826 <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  80076a:	83 ec 08             	sub    $0x8,%esp
  80076d:	ff 75 e8             	pushl  -0x18(%ebp)
  800770:	8d 45 14             	lea    0x14(%ebp),%eax
  800773:	50                   	push   %eax
  800774:	e8 7a fc ff ff       	call   8003f3 <getuint>
  800779:	83 c4 10             	add    $0x10,%esp
  80077c:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80077f:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  800782:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800789:	e9 98 00 00 00       	jmp    800826 <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  80078e:	83 ec 08             	sub    $0x8,%esp
  800791:	ff 75 0c             	pushl  0xc(%ebp)
  800794:	6a 58                	push   $0x58
  800796:	8b 45 08             	mov    0x8(%ebp),%eax
  800799:	ff d0                	call   *%eax
  80079b:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  80079e:	83 ec 08             	sub    $0x8,%esp
  8007a1:	ff 75 0c             	pushl  0xc(%ebp)
  8007a4:	6a 58                	push   $0x58
  8007a6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007a9:	ff d0                	call   *%eax
  8007ab:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  8007ae:	83 ec 08             	sub    $0x8,%esp
  8007b1:	ff 75 0c             	pushl  0xc(%ebp)
  8007b4:	6a 58                	push   $0x58
  8007b6:	8b 45 08             	mov    0x8(%ebp),%eax
  8007b9:	ff d0                	call   *%eax
  8007bb:	83 c4 10             	add    $0x10,%esp
			break;
  8007be:	e9 c1 00 00 00       	jmp    800884 <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
  8007c3:	83 ec 08             	sub    $0x8,%esp
  8007c6:	ff 75 0c             	pushl  0xc(%ebp)
  8007c9:	6a 30                	push   $0x30
  8007cb:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ce:	ff d0                	call   *%eax
  8007d0:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
  8007d3:	83 ec 08             	sub    $0x8,%esp
  8007d6:	ff 75 0c             	pushl  0xc(%ebp)
  8007d9:	6a 78                	push   $0x78
  8007db:	8b 45 08             	mov    0x8(%ebp),%eax
  8007de:	ff d0                	call   *%eax
  8007e0:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  8007e3:	8b 45 14             	mov    0x14(%ebp),%eax
  8007e6:	83 c0 04             	add    $0x4,%eax
  8007e9:	89 45 14             	mov    %eax,0x14(%ebp)
  8007ec:	8b 45 14             	mov    0x14(%ebp),%eax
  8007ef:	83 e8 04             	sub    $0x4,%eax
  8007f2:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8007f4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007f7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  8007fe:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  800805:	eb 1f                	jmp    800826 <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  800807:	83 ec 08             	sub    $0x8,%esp
  80080a:	ff 75 e8             	pushl  -0x18(%ebp)
  80080d:	8d 45 14             	lea    0x14(%ebp),%eax
  800810:	50                   	push   %eax
  800811:	e8 dd fb ff ff       	call   8003f3 <getuint>
  800816:	83 c4 10             	add    $0x10,%esp
  800819:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80081c:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  80081f:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  800826:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  80082a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80082d:	83 ec 04             	sub    $0x4,%esp
  800830:	52                   	push   %edx
  800831:	ff 75 e4             	pushl  -0x1c(%ebp)
  800834:	50                   	push   %eax
  800835:	ff 75 f4             	pushl  -0xc(%ebp)
  800838:	ff 75 f0             	pushl  -0x10(%ebp)
  80083b:	ff 75 0c             	pushl  0xc(%ebp)
  80083e:	ff 75 08             	pushl  0x8(%ebp)
  800841:	e8 f4 fa ff ff       	call   80033a <printnum>
  800846:	83 c4 20             	add    $0x20,%esp
			break;
  800849:	eb 39                	jmp    800884 <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80084b:	83 ec 08             	sub    $0x8,%esp
  80084e:	ff 75 0c             	pushl  0xc(%ebp)
  800851:	53                   	push   %ebx
  800852:	8b 45 08             	mov    0x8(%ebp),%eax
  800855:	ff d0                	call   *%eax
  800857:	83 c4 10             	add    $0x10,%esp
			break;
  80085a:	eb 28                	jmp    800884 <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80085c:	83 ec 08             	sub    $0x8,%esp
  80085f:	ff 75 0c             	pushl  0xc(%ebp)
  800862:	6a 25                	push   $0x25
  800864:	8b 45 08             	mov    0x8(%ebp),%eax
  800867:	ff d0                	call   *%eax
  800869:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
  80086c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800870:	eb 04                	jmp    800876 <vprintfmt+0x3bd>
  800872:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800876:	8b 45 10             	mov    0x10(%ebp),%eax
  800879:	83 e8 01             	sub    $0x1,%eax
  80087c:	0f b6 00             	movzbl (%eax),%eax
  80087f:	3c 25                	cmp    $0x25,%al
  800881:	75 ef                	jne    800872 <vprintfmt+0x3b9>
				/* do nothing */;
			break;
  800883:	90                   	nop
		}
	}
  800884:	e9 38 fc ff ff       	jmp    8004c1 <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
  800889:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  80088a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80088d:	5b                   	pop    %ebx
  80088e:	5e                   	pop    %esi
  80088f:	5d                   	pop    %ebp
  800890:	c3                   	ret    

00800891 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800891:	55                   	push   %ebp
  800892:	89 e5                	mov    %esp,%ebp
  800894:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  800897:	8d 45 10             	lea    0x10(%ebp),%eax
  80089a:	83 c0 04             	add    $0x4,%eax
  80089d:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  8008a0:	8b 45 10             	mov    0x10(%ebp),%eax
  8008a3:	ff 75 f4             	pushl  -0xc(%ebp)
  8008a6:	50                   	push   %eax
  8008a7:	ff 75 0c             	pushl  0xc(%ebp)
  8008aa:	ff 75 08             	pushl  0x8(%ebp)
  8008ad:	e8 07 fc ff ff       	call   8004b9 <vprintfmt>
  8008b2:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
  8008b5:	90                   	nop
  8008b6:	c9                   	leave  
  8008b7:	c3                   	ret    

008008b8 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  8008b8:	55                   	push   %ebp
  8008b9:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  8008bb:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008be:	8b 40 08             	mov    0x8(%eax),%eax
  8008c1:	8d 50 01             	lea    0x1(%eax),%edx
  8008c4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008c7:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  8008ca:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008cd:	8b 10                	mov    (%eax),%edx
  8008cf:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008d2:	8b 40 04             	mov    0x4(%eax),%eax
  8008d5:	39 c2                	cmp    %eax,%edx
  8008d7:	73 12                	jae    8008eb <sprintputch+0x33>
		*b->buf++ = ch;
  8008d9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008dc:	8b 00                	mov    (%eax),%eax
  8008de:	8d 48 01             	lea    0x1(%eax),%ecx
  8008e1:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008e4:	89 0a                	mov    %ecx,(%edx)
  8008e6:	8b 55 08             	mov    0x8(%ebp),%edx
  8008e9:	88 10                	mov    %dl,(%eax)
}
  8008eb:	90                   	nop
  8008ec:	5d                   	pop    %ebp
  8008ed:	c3                   	ret    

008008ee <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8008ee:	55                   	push   %ebp
  8008ef:	89 e5                	mov    %esp,%ebp
  8008f1:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  8008f4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f7:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8008fa:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008fd:	8d 50 ff             	lea    -0x1(%eax),%edx
  800900:	8b 45 08             	mov    0x8(%ebp),%eax
  800903:	01 d0                	add    %edx,%eax
  800905:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800908:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  80090f:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800913:	74 06                	je     80091b <vsnprintf+0x2d>
  800915:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800919:	7f 07                	jg     800922 <vsnprintf+0x34>
		return -E_INVAL;
  80091b:	b8 03 00 00 00       	mov    $0x3,%eax
  800920:	eb 20                	jmp    800942 <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800922:	ff 75 14             	pushl  0x14(%ebp)
  800925:	ff 75 10             	pushl  0x10(%ebp)
  800928:	8d 45 ec             	lea    -0x14(%ebp),%eax
  80092b:	50                   	push   %eax
  80092c:	68 b8 08 80 00       	push   $0x8008b8
  800931:	e8 83 fb ff ff       	call   8004b9 <vprintfmt>
  800936:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
  800939:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80093c:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80093f:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  800942:	c9                   	leave  
  800943:	c3                   	ret    

00800944 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800944:	55                   	push   %ebp
  800945:	89 e5                	mov    %esp,%ebp
  800947:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80094a:	8d 45 10             	lea    0x10(%ebp),%eax
  80094d:	83 c0 04             	add    $0x4,%eax
  800950:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  800953:	8b 45 10             	mov    0x10(%ebp),%eax
  800956:	ff 75 f4             	pushl  -0xc(%ebp)
  800959:	50                   	push   %eax
  80095a:	ff 75 0c             	pushl  0xc(%ebp)
  80095d:	ff 75 08             	pushl  0x8(%ebp)
  800960:	e8 89 ff ff ff       	call   8008ee <vsnprintf>
  800965:	83 c4 10             	add    $0x10,%esp
  800968:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  80096b:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80096e:	c9                   	leave  
  80096f:	c3                   	ret    

00800970 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  800970:	55                   	push   %ebp
  800971:	89 e5                	mov    %esp,%ebp
  800973:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  800976:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80097d:	eb 08                	jmp    800987 <strlen+0x17>
		n++;
  80097f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800983:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800987:	8b 45 08             	mov    0x8(%ebp),%eax
  80098a:	0f b6 00             	movzbl (%eax),%eax
  80098d:	84 c0                	test   %al,%al
  80098f:	75 ee                	jne    80097f <strlen+0xf>
		n++;
	return n;
  800991:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800994:	c9                   	leave  
  800995:	c3                   	ret    

00800996 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  800996:	55                   	push   %ebp
  800997:	89 e5                	mov    %esp,%ebp
  800999:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80099c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8009a3:	eb 0c                	jmp    8009b1 <strnlen+0x1b>
		n++;
  8009a5:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8009a9:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009ad:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  8009b1:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8009b5:	74 0a                	je     8009c1 <strnlen+0x2b>
  8009b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ba:	0f b6 00             	movzbl (%eax),%eax
  8009bd:	84 c0                	test   %al,%al
  8009bf:	75 e4                	jne    8009a5 <strnlen+0xf>
		n++;
	return n;
  8009c1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8009c4:	c9                   	leave  
  8009c5:	c3                   	ret    

008009c6 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  8009c6:	55                   	push   %ebp
  8009c7:	89 e5                	mov    %esp,%ebp
  8009c9:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  8009cc:	8b 45 08             	mov    0x8(%ebp),%eax
  8009cf:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  8009d2:	90                   	nop
  8009d3:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d6:	8d 50 01             	lea    0x1(%eax),%edx
  8009d9:	89 55 08             	mov    %edx,0x8(%ebp)
  8009dc:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009df:	8d 4a 01             	lea    0x1(%edx),%ecx
  8009e2:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8009e5:	0f b6 12             	movzbl (%edx),%edx
  8009e8:	88 10                	mov    %dl,(%eax)
  8009ea:	0f b6 00             	movzbl (%eax),%eax
  8009ed:	84 c0                	test   %al,%al
  8009ef:	75 e2                	jne    8009d3 <strcpy+0xd>
		/* do nothing */;
	return ret;
  8009f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8009f4:	c9                   	leave  
  8009f5:	c3                   	ret    

008009f6 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  8009f6:	55                   	push   %ebp
  8009f7:	89 e5                	mov    %esp,%ebp
  8009f9:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  8009fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ff:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800a02:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800a09:	eb 23                	jmp    800a2e <strncpy+0x38>
		*dst++ = *src;
  800a0b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a0e:	8d 50 01             	lea    0x1(%eax),%edx
  800a11:	89 55 08             	mov    %edx,0x8(%ebp)
  800a14:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a17:	0f b6 12             	movzbl (%edx),%edx
  800a1a:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  800a1c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a1f:	0f b6 00             	movzbl (%eax),%eax
  800a22:	84 c0                	test   %al,%al
  800a24:	74 04                	je     800a2a <strncpy+0x34>
			src++;
  800a26:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  800a2a:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800a2e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a31:	3b 45 10             	cmp    0x10(%ebp),%eax
  800a34:	72 d5                	jb     800a0b <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800a36:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800a39:	c9                   	leave  
  800a3a:	c3                   	ret    

00800a3b <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800a3b:	55                   	push   %ebp
  800a3c:	89 e5                	mov    %esp,%ebp
  800a3e:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800a41:	8b 45 08             	mov    0x8(%ebp),%eax
  800a44:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800a47:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a4b:	74 33                	je     800a80 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800a4d:	eb 17                	jmp    800a66 <strlcpy+0x2b>
			*dst++ = *src++;
  800a4f:	8b 45 08             	mov    0x8(%ebp),%eax
  800a52:	8d 50 01             	lea    0x1(%eax),%edx
  800a55:	89 55 08             	mov    %edx,0x8(%ebp)
  800a58:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a5b:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a5e:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800a61:	0f b6 12             	movzbl (%edx),%edx
  800a64:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a66:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800a6a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a6e:	74 0a                	je     800a7a <strlcpy+0x3f>
  800a70:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a73:	0f b6 00             	movzbl (%eax),%eax
  800a76:	84 c0                	test   %al,%al
  800a78:	75 d5                	jne    800a4f <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800a7a:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7d:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  800a80:	8b 55 08             	mov    0x8(%ebp),%edx
  800a83:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a86:	29 c2                	sub    %eax,%edx
  800a88:	89 d0                	mov    %edx,%eax
}
  800a8a:	c9                   	leave  
  800a8b:	c3                   	ret    

00800a8c <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a8c:	55                   	push   %ebp
  800a8d:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800a8f:	eb 08                	jmp    800a99 <strcmp+0xd>
		p++, q++;
  800a91:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a95:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a99:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9c:	0f b6 00             	movzbl (%eax),%eax
  800a9f:	84 c0                	test   %al,%al
  800aa1:	74 10                	je     800ab3 <strcmp+0x27>
  800aa3:	8b 45 08             	mov    0x8(%ebp),%eax
  800aa6:	0f b6 10             	movzbl (%eax),%edx
  800aa9:	8b 45 0c             	mov    0xc(%ebp),%eax
  800aac:	0f b6 00             	movzbl (%eax),%eax
  800aaf:	38 c2                	cmp    %al,%dl
  800ab1:	74 de                	je     800a91 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800ab3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab6:	0f b6 00             	movzbl (%eax),%eax
  800ab9:	0f b6 d0             	movzbl %al,%edx
  800abc:	8b 45 0c             	mov    0xc(%ebp),%eax
  800abf:	0f b6 00             	movzbl (%eax),%eax
  800ac2:	0f b6 c0             	movzbl %al,%eax
  800ac5:	29 c2                	sub    %eax,%edx
  800ac7:	89 d0                	mov    %edx,%eax
}
  800ac9:	5d                   	pop    %ebp
  800aca:	c3                   	ret    

00800acb <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800acb:	55                   	push   %ebp
  800acc:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800ace:	eb 0c                	jmp    800adc <strncmp+0x11>
		n--, p++, q++;
  800ad0:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800ad4:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ad8:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800adc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ae0:	74 1a                	je     800afc <strncmp+0x31>
  800ae2:	8b 45 08             	mov    0x8(%ebp),%eax
  800ae5:	0f b6 00             	movzbl (%eax),%eax
  800ae8:	84 c0                	test   %al,%al
  800aea:	74 10                	je     800afc <strncmp+0x31>
  800aec:	8b 45 08             	mov    0x8(%ebp),%eax
  800aef:	0f b6 10             	movzbl (%eax),%edx
  800af2:	8b 45 0c             	mov    0xc(%ebp),%eax
  800af5:	0f b6 00             	movzbl (%eax),%eax
  800af8:	38 c2                	cmp    %al,%dl
  800afa:	74 d4                	je     800ad0 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800afc:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800b00:	75 07                	jne    800b09 <strncmp+0x3e>
		return 0;
  800b02:	b8 00 00 00 00       	mov    $0x0,%eax
  800b07:	eb 16                	jmp    800b1f <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800b09:	8b 45 08             	mov    0x8(%ebp),%eax
  800b0c:	0f b6 00             	movzbl (%eax),%eax
  800b0f:	0f b6 d0             	movzbl %al,%edx
  800b12:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b15:	0f b6 00             	movzbl (%eax),%eax
  800b18:	0f b6 c0             	movzbl %al,%eax
  800b1b:	29 c2                	sub    %eax,%edx
  800b1d:	89 d0                	mov    %edx,%eax
}
  800b1f:	5d                   	pop    %ebp
  800b20:	c3                   	ret    

00800b21 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800b21:	55                   	push   %ebp
  800b22:	89 e5                	mov    %esp,%ebp
  800b24:	83 ec 04             	sub    $0x4,%esp
  800b27:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b2a:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800b2d:	eb 14                	jmp    800b43 <strchr+0x22>
		if (*s == c)
  800b2f:	8b 45 08             	mov    0x8(%ebp),%eax
  800b32:	0f b6 00             	movzbl (%eax),%eax
  800b35:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800b38:	75 05                	jne    800b3f <strchr+0x1e>
			return (char *) s;
  800b3a:	8b 45 08             	mov    0x8(%ebp),%eax
  800b3d:	eb 13                	jmp    800b52 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800b3f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b43:	8b 45 08             	mov    0x8(%ebp),%eax
  800b46:	0f b6 00             	movzbl (%eax),%eax
  800b49:	84 c0                	test   %al,%al
  800b4b:	75 e2                	jne    800b2f <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800b4d:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b52:	c9                   	leave  
  800b53:	c3                   	ret    

00800b54 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800b54:	55                   	push   %ebp
  800b55:	89 e5                	mov    %esp,%ebp
  800b57:	83 ec 04             	sub    $0x4,%esp
  800b5a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b5d:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800b60:	eb 0f                	jmp    800b71 <strfind+0x1d>
		if (*s == c)
  800b62:	8b 45 08             	mov    0x8(%ebp),%eax
  800b65:	0f b6 00             	movzbl (%eax),%eax
  800b68:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800b6b:	74 10                	je     800b7d <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800b6d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b71:	8b 45 08             	mov    0x8(%ebp),%eax
  800b74:	0f b6 00             	movzbl (%eax),%eax
  800b77:	84 c0                	test   %al,%al
  800b79:	75 e7                	jne    800b62 <strfind+0xe>
  800b7b:	eb 01                	jmp    800b7e <strfind+0x2a>
		if (*s == c)
			break;
  800b7d:	90                   	nop
	return (char *) s;
  800b7e:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b81:	c9                   	leave  
  800b82:	c3                   	ret    

00800b83 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800b83:	55                   	push   %ebp
  800b84:	89 e5                	mov    %esp,%ebp
  800b86:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800b89:	8b 45 08             	mov    0x8(%ebp),%eax
  800b8c:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800b8f:	8b 45 10             	mov    0x10(%ebp),%eax
  800b92:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800b95:	eb 0e                	jmp    800ba5 <memset+0x22>
		*p++ = c;
  800b97:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b9a:	8d 50 01             	lea    0x1(%eax),%edx
  800b9d:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800ba0:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ba3:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800ba5:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800ba9:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800bad:	79 e8                	jns    800b97 <memset+0x14>
		*p++ = c;

	return v;
  800baf:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bb2:	c9                   	leave  
  800bb3:	c3                   	ret    

00800bb4 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800bb4:	55                   	push   %ebp
  800bb5:	89 e5                	mov    %esp,%ebp
  800bb7:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800bba:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bbd:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800bc0:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc3:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800bc6:	eb 17                	jmp    800bdf <memcpy+0x2b>
		*d++ = *s++;
  800bc8:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800bcb:	8d 50 01             	lea    0x1(%eax),%edx
  800bce:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800bd1:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800bd4:	8d 4a 01             	lea    0x1(%edx),%ecx
  800bd7:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800bda:	0f b6 12             	movzbl (%edx),%edx
  800bdd:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800bdf:	8b 45 10             	mov    0x10(%ebp),%eax
  800be2:	8d 50 ff             	lea    -0x1(%eax),%edx
  800be5:	89 55 10             	mov    %edx,0x10(%ebp)
  800be8:	85 c0                	test   %eax,%eax
  800bea:	75 dc                	jne    800bc8 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800bec:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bef:	c9                   	leave  
  800bf0:	c3                   	ret    

00800bf1 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800bf1:	55                   	push   %ebp
  800bf2:	89 e5                	mov    %esp,%ebp
  800bf4:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800bf7:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bfa:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800bfd:	8b 45 08             	mov    0x8(%ebp),%eax
  800c00:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800c03:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c06:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c09:	73 54                	jae    800c5f <memmove+0x6e>
  800c0b:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c0e:	8b 45 10             	mov    0x10(%ebp),%eax
  800c11:	01 d0                	add    %edx,%eax
  800c13:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800c16:	76 47                	jbe    800c5f <memmove+0x6e>
		s += n;
  800c18:	8b 45 10             	mov    0x10(%ebp),%eax
  800c1b:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800c1e:	8b 45 10             	mov    0x10(%ebp),%eax
  800c21:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800c24:	eb 13                	jmp    800c39 <memmove+0x48>
			*--d = *--s;
  800c26:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800c2a:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800c2e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c31:	0f b6 10             	movzbl (%eax),%edx
  800c34:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c37:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800c39:	8b 45 10             	mov    0x10(%ebp),%eax
  800c3c:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c3f:	89 55 10             	mov    %edx,0x10(%ebp)
  800c42:	85 c0                	test   %eax,%eax
  800c44:	75 e0                	jne    800c26 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800c46:	eb 24                	jmp    800c6c <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
  800c48:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c4b:	8d 50 01             	lea    0x1(%eax),%edx
  800c4e:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800c51:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c54:	8d 4a 01             	lea    0x1(%edx),%ecx
  800c57:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800c5a:	0f b6 12             	movzbl (%edx),%edx
  800c5d:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800c5f:	8b 45 10             	mov    0x10(%ebp),%eax
  800c62:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c65:	89 55 10             	mov    %edx,0x10(%ebp)
  800c68:	85 c0                	test   %eax,%eax
  800c6a:	75 dc                	jne    800c48 <memmove+0x57>
			*d++ = *s++;

	return dst;
  800c6c:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c6f:	c9                   	leave  
  800c70:	c3                   	ret    

00800c71 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800c71:	55                   	push   %ebp
  800c72:	89 e5                	mov    %esp,%ebp
  800c74:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800c77:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7a:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800c7d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c80:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800c83:	eb 30                	jmp    800cb5 <memcmp+0x44>
		if (*s1 != *s2)
  800c85:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c88:	0f b6 10             	movzbl (%eax),%edx
  800c8b:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c8e:	0f b6 00             	movzbl (%eax),%eax
  800c91:	38 c2                	cmp    %al,%dl
  800c93:	74 18                	je     800cad <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800c95:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c98:	0f b6 00             	movzbl (%eax),%eax
  800c9b:	0f b6 d0             	movzbl %al,%edx
  800c9e:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ca1:	0f b6 00             	movzbl (%eax),%eax
  800ca4:	0f b6 c0             	movzbl %al,%eax
  800ca7:	29 c2                	sub    %eax,%edx
  800ca9:	89 d0                	mov    %edx,%eax
  800cab:	eb 1a                	jmp    800cc7 <memcmp+0x56>
		s1++, s2++;
  800cad:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800cb1:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800cb5:	8b 45 10             	mov    0x10(%ebp),%eax
  800cb8:	8d 50 ff             	lea    -0x1(%eax),%edx
  800cbb:	89 55 10             	mov    %edx,0x10(%ebp)
  800cbe:	85 c0                	test   %eax,%eax
  800cc0:	75 c3                	jne    800c85 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800cc2:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800cc7:	c9                   	leave  
  800cc8:	c3                   	ret    

00800cc9 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800cc9:	55                   	push   %ebp
  800cca:	89 e5                	mov    %esp,%ebp
  800ccc:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800ccf:	8b 55 08             	mov    0x8(%ebp),%edx
  800cd2:	8b 45 10             	mov    0x10(%ebp),%eax
  800cd5:	01 d0                	add    %edx,%eax
  800cd7:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800cda:	eb 17                	jmp    800cf3 <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800cdc:	8b 45 08             	mov    0x8(%ebp),%eax
  800cdf:	0f b6 00             	movzbl (%eax),%eax
  800ce2:	0f b6 d0             	movzbl %al,%edx
  800ce5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ce8:	0f b6 c0             	movzbl %al,%eax
  800ceb:	39 c2                	cmp    %eax,%edx
  800ced:	74 0e                	je     800cfd <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800cef:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800cf3:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf6:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800cf9:	72 e1                	jb     800cdc <memfind+0x13>
  800cfb:	eb 01                	jmp    800cfe <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
  800cfd:	90                   	nop
	return (void *) s;
  800cfe:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800d01:	c9                   	leave  
  800d02:	c3                   	ret    

00800d03 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800d03:	55                   	push   %ebp
  800d04:	89 e5                	mov    %esp,%ebp
  800d06:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800d09:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800d10:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d17:	eb 04                	jmp    800d1d <strtol+0x1a>
		s++;
  800d19:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800d1d:	8b 45 08             	mov    0x8(%ebp),%eax
  800d20:	0f b6 00             	movzbl (%eax),%eax
  800d23:	3c 20                	cmp    $0x20,%al
  800d25:	74 f2                	je     800d19 <strtol+0x16>
  800d27:	8b 45 08             	mov    0x8(%ebp),%eax
  800d2a:	0f b6 00             	movzbl (%eax),%eax
  800d2d:	3c 09                	cmp    $0x9,%al
  800d2f:	74 e8                	je     800d19 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800d31:	8b 45 08             	mov    0x8(%ebp),%eax
  800d34:	0f b6 00             	movzbl (%eax),%eax
  800d37:	3c 2b                	cmp    $0x2b,%al
  800d39:	75 06                	jne    800d41 <strtol+0x3e>
		s++;
  800d3b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d3f:	eb 15                	jmp    800d56 <strtol+0x53>
	else if (*s == '-')
  800d41:	8b 45 08             	mov    0x8(%ebp),%eax
  800d44:	0f b6 00             	movzbl (%eax),%eax
  800d47:	3c 2d                	cmp    $0x2d,%al
  800d49:	75 0b                	jne    800d56 <strtol+0x53>
		s++, neg = 1;
  800d4b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d4f:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d56:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d5a:	74 06                	je     800d62 <strtol+0x5f>
  800d5c:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800d60:	75 24                	jne    800d86 <strtol+0x83>
  800d62:	8b 45 08             	mov    0x8(%ebp),%eax
  800d65:	0f b6 00             	movzbl (%eax),%eax
  800d68:	3c 30                	cmp    $0x30,%al
  800d6a:	75 1a                	jne    800d86 <strtol+0x83>
  800d6c:	8b 45 08             	mov    0x8(%ebp),%eax
  800d6f:	83 c0 01             	add    $0x1,%eax
  800d72:	0f b6 00             	movzbl (%eax),%eax
  800d75:	3c 78                	cmp    $0x78,%al
  800d77:	75 0d                	jne    800d86 <strtol+0x83>
		s += 2, base = 16;
  800d79:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800d7d:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800d84:	eb 2a                	jmp    800db0 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800d86:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d8a:	75 17                	jne    800da3 <strtol+0xa0>
  800d8c:	8b 45 08             	mov    0x8(%ebp),%eax
  800d8f:	0f b6 00             	movzbl (%eax),%eax
  800d92:	3c 30                	cmp    $0x30,%al
  800d94:	75 0d                	jne    800da3 <strtol+0xa0>
		s++, base = 8;
  800d96:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d9a:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800da1:	eb 0d                	jmp    800db0 <strtol+0xad>
	else if (base == 0)
  800da3:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800da7:	75 07                	jne    800db0 <strtol+0xad>
		base = 10;
  800da9:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800db0:	8b 45 08             	mov    0x8(%ebp),%eax
  800db3:	0f b6 00             	movzbl (%eax),%eax
  800db6:	3c 2f                	cmp    $0x2f,%al
  800db8:	7e 1b                	jle    800dd5 <strtol+0xd2>
  800dba:	8b 45 08             	mov    0x8(%ebp),%eax
  800dbd:	0f b6 00             	movzbl (%eax),%eax
  800dc0:	3c 39                	cmp    $0x39,%al
  800dc2:	7f 11                	jg     800dd5 <strtol+0xd2>
			dig = *s - '0';
  800dc4:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc7:	0f b6 00             	movzbl (%eax),%eax
  800dca:	0f be c0             	movsbl %al,%eax
  800dcd:	83 e8 30             	sub    $0x30,%eax
  800dd0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800dd3:	eb 48                	jmp    800e1d <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800dd5:	8b 45 08             	mov    0x8(%ebp),%eax
  800dd8:	0f b6 00             	movzbl (%eax),%eax
  800ddb:	3c 60                	cmp    $0x60,%al
  800ddd:	7e 1b                	jle    800dfa <strtol+0xf7>
  800ddf:	8b 45 08             	mov    0x8(%ebp),%eax
  800de2:	0f b6 00             	movzbl (%eax),%eax
  800de5:	3c 7a                	cmp    $0x7a,%al
  800de7:	7f 11                	jg     800dfa <strtol+0xf7>
			dig = *s - 'a' + 10;
  800de9:	8b 45 08             	mov    0x8(%ebp),%eax
  800dec:	0f b6 00             	movzbl (%eax),%eax
  800def:	0f be c0             	movsbl %al,%eax
  800df2:	83 e8 57             	sub    $0x57,%eax
  800df5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800df8:	eb 23                	jmp    800e1d <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800dfa:	8b 45 08             	mov    0x8(%ebp),%eax
  800dfd:	0f b6 00             	movzbl (%eax),%eax
  800e00:	3c 40                	cmp    $0x40,%al
  800e02:	7e 3c                	jle    800e40 <strtol+0x13d>
  800e04:	8b 45 08             	mov    0x8(%ebp),%eax
  800e07:	0f b6 00             	movzbl (%eax),%eax
  800e0a:	3c 5a                	cmp    $0x5a,%al
  800e0c:	7f 32                	jg     800e40 <strtol+0x13d>
			dig = *s - 'A' + 10;
  800e0e:	8b 45 08             	mov    0x8(%ebp),%eax
  800e11:	0f b6 00             	movzbl (%eax),%eax
  800e14:	0f be c0             	movsbl %al,%eax
  800e17:	83 e8 37             	sub    $0x37,%eax
  800e1a:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800e1d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e20:	3b 45 10             	cmp    0x10(%ebp),%eax
  800e23:	7d 1a                	jge    800e3f <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
  800e25:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800e29:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800e2c:	0f af 45 10          	imul   0x10(%ebp),%eax
  800e30:	89 c2                	mov    %eax,%edx
  800e32:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e35:	01 d0                	add    %edx,%eax
  800e37:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800e3a:	e9 71 ff ff ff       	jmp    800db0 <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
  800e3f:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
  800e40:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800e44:	74 08                	je     800e4e <strtol+0x14b>
		*endptr = (char *) s;
  800e46:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e49:	8b 55 08             	mov    0x8(%ebp),%edx
  800e4c:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800e4e:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800e52:	74 07                	je     800e5b <strtol+0x158>
  800e54:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800e57:	f7 d8                	neg    %eax
  800e59:	eb 03                	jmp    800e5e <strtol+0x15b>
  800e5b:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800e5e:	c9                   	leave  
  800e5f:	c3                   	ret    

00800e60 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800e60:	55                   	push   %ebp
  800e61:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800e63:	8b 45 14             	mov    0x14(%ebp),%eax
  800e66:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800e6c:	8b 45 14             	mov    0x14(%ebp),%eax
  800e6f:	8b 00                	mov    (%eax),%eax
  800e71:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e78:	8b 45 10             	mov    0x10(%ebp),%eax
  800e7b:	01 d0                	add    %edx,%eax
  800e7d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e83:	eb 0c                	jmp    800e91 <strsplit+0x31>
			*string++ = 0;
  800e85:	8b 45 08             	mov    0x8(%ebp),%eax
  800e88:	8d 50 01             	lea    0x1(%eax),%edx
  800e8b:	89 55 08             	mov    %edx,0x8(%ebp)
  800e8e:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e91:	8b 45 08             	mov    0x8(%ebp),%eax
  800e94:	0f b6 00             	movzbl (%eax),%eax
  800e97:	84 c0                	test   %al,%al
  800e99:	74 19                	je     800eb4 <strsplit+0x54>
  800e9b:	8b 45 08             	mov    0x8(%ebp),%eax
  800e9e:	0f b6 00             	movzbl (%eax),%eax
  800ea1:	0f be c0             	movsbl %al,%eax
  800ea4:	50                   	push   %eax
  800ea5:	ff 75 0c             	pushl  0xc(%ebp)
  800ea8:	e8 74 fc ff ff       	call   800b21 <strchr>
  800ead:	83 c4 08             	add    $0x8,%esp
  800eb0:	85 c0                	test   %eax,%eax
  800eb2:	75 d1                	jne    800e85 <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800eb4:	8b 45 08             	mov    0x8(%ebp),%eax
  800eb7:	0f b6 00             	movzbl (%eax),%eax
  800eba:	84 c0                	test   %al,%al
  800ebc:	74 5d                	je     800f1b <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800ebe:	8b 45 14             	mov    0x14(%ebp),%eax
  800ec1:	8b 00                	mov    (%eax),%eax
  800ec3:	83 f8 0f             	cmp    $0xf,%eax
  800ec6:	75 07                	jne    800ecf <strsplit+0x6f>
		{
			return 0;
  800ec8:	b8 00 00 00 00       	mov    $0x0,%eax
  800ecd:	eb 69                	jmp    800f38 <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800ecf:	8b 45 14             	mov    0x14(%ebp),%eax
  800ed2:	8b 00                	mov    (%eax),%eax
  800ed4:	8d 48 01             	lea    0x1(%eax),%ecx
  800ed7:	8b 55 14             	mov    0x14(%ebp),%edx
  800eda:	89 0a                	mov    %ecx,(%edx)
  800edc:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ee3:	8b 45 10             	mov    0x10(%ebp),%eax
  800ee6:	01 c2                	add    %eax,%edx
  800ee8:	8b 45 08             	mov    0x8(%ebp),%eax
  800eeb:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800eed:	eb 04                	jmp    800ef3 <strsplit+0x93>
			string++;
  800eef:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800ef3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ef6:	0f b6 00             	movzbl (%eax),%eax
  800ef9:	84 c0                	test   %al,%al
  800efb:	74 86                	je     800e83 <strsplit+0x23>
  800efd:	8b 45 08             	mov    0x8(%ebp),%eax
  800f00:	0f b6 00             	movzbl (%eax),%eax
  800f03:	0f be c0             	movsbl %al,%eax
  800f06:	50                   	push   %eax
  800f07:	ff 75 0c             	pushl  0xc(%ebp)
  800f0a:	e8 12 fc ff ff       	call   800b21 <strchr>
  800f0f:	83 c4 08             	add    $0x8,%esp
  800f12:	85 c0                	test   %eax,%eax
  800f14:	74 d9                	je     800eef <strsplit+0x8f>
			string++;
	}
  800f16:	e9 68 ff ff ff       	jmp    800e83 <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
  800f1b:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800f1c:	8b 45 14             	mov    0x14(%ebp),%eax
  800f1f:	8b 00                	mov    (%eax),%eax
  800f21:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800f28:	8b 45 10             	mov    0x10(%ebp),%eax
  800f2b:	01 d0                	add    %edx,%eax
  800f2d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800f33:	b8 01 00 00 00       	mov    $0x1,%eax
}
  800f38:	c9                   	leave  
  800f39:	c3                   	ret    

00800f3a <malloc>:

 
static uint8 *ptr_user_free_mem  = (uint8*) USER_HEAP_START;

void* malloc(uint32 size)
{	
  800f3a:	55                   	push   %ebp
  800f3b:	89 e5                	mov    %esp,%ebp
  800f3d:	83 ec 08             	sub    $0x8,%esp
	//PROJECT 2008: your code here
	//	

	panic("malloc is not implemented yet");
  800f40:	83 ec 04             	sub    $0x4,%esp
  800f43:	68 9c 16 80 00       	push   $0x80169c
  800f48:	6a 2b                	push   $0x2b
  800f4a:	68 ba 16 80 00       	push   $0x8016ba
  800f4f:	e8 9b 01 00 00       	call   8010ef <_panic>

00800f54 <freeHeap>:
//	freeMem(uint32* ptr_page_directory, void* start_virtual_address, uint32 size) in 
//	"memory_manager.c" then switch back to user mode, the later function is empty, 
//	please go fill it.

void freeHeap()
{
  800f54:	55                   	push   %ebp
  800f55:	89 e5                	mov    %esp,%ebp
  800f57:	83 ec 08             	sub    $0x8,%esp
	//PROJECT 2008: your code here
	//	

	panic("freeHeap is not implemented yet");
  800f5a:	83 ec 04             	sub    $0x4,%esp
  800f5d:	68 c8 16 80 00       	push   $0x8016c8
  800f62:	6a 6a                	push   $0x6a
  800f64:	68 ba 16 80 00       	push   $0x8016ba
  800f69:	e8 81 01 00 00       	call   8010ef <_panic>

00800f6e <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800f6e:	55                   	push   %ebp
  800f6f:	89 e5                	mov    %esp,%ebp
  800f71:	57                   	push   %edi
  800f72:	56                   	push   %esi
  800f73:	53                   	push   %ebx
  800f74:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f77:	8b 45 08             	mov    0x8(%ebp),%eax
  800f7a:	8b 55 0c             	mov    0xc(%ebp),%edx
  800f7d:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800f80:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800f83:	8b 7d 18             	mov    0x18(%ebp),%edi
  800f86:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800f89:	cd 30                	int    $0x30
  800f8b:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800f8e:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800f91:	83 c4 10             	add    $0x10,%esp
  800f94:	5b                   	pop    %ebx
  800f95:	5e                   	pop    %esi
  800f96:	5f                   	pop    %edi
  800f97:	5d                   	pop    %ebp
  800f98:	c3                   	ret    

00800f99 <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800f99:	55                   	push   %ebp
  800f9a:	89 e5                	mov    %esp,%ebp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800f9c:	8b 45 08             	mov    0x8(%ebp),%eax
  800f9f:	6a 00                	push   $0x0
  800fa1:	6a 00                	push   $0x0
  800fa3:	6a 00                	push   $0x0
  800fa5:	ff 75 0c             	pushl  0xc(%ebp)
  800fa8:	50                   	push   %eax
  800fa9:	6a 00                	push   $0x0
  800fab:	e8 be ff ff ff       	call   800f6e <syscall>
  800fb0:	83 c4 18             	add    $0x18,%esp
}
  800fb3:	90                   	nop
  800fb4:	c9                   	leave  
  800fb5:	c3                   	ret    

00800fb6 <sys_cgetc>:

int
sys_cgetc(void)
{
  800fb6:	55                   	push   %ebp
  800fb7:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800fb9:	6a 00                	push   $0x0
  800fbb:	6a 00                	push   $0x0
  800fbd:	6a 00                	push   $0x0
  800fbf:	6a 00                	push   $0x0
  800fc1:	6a 00                	push   $0x0
  800fc3:	6a 01                	push   $0x1
  800fc5:	e8 a4 ff ff ff       	call   800f6e <syscall>
  800fca:	83 c4 18             	add    $0x18,%esp
}
  800fcd:	c9                   	leave  
  800fce:	c3                   	ret    

00800fcf <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800fcf:	55                   	push   %ebp
  800fd0:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800fd2:	8b 45 08             	mov    0x8(%ebp),%eax
  800fd5:	6a 00                	push   $0x0
  800fd7:	6a 00                	push   $0x0
  800fd9:	6a 00                	push   $0x0
  800fdb:	6a 00                	push   $0x0
  800fdd:	50                   	push   %eax
  800fde:	6a 03                	push   $0x3
  800fe0:	e8 89 ff ff ff       	call   800f6e <syscall>
  800fe5:	83 c4 18             	add    $0x18,%esp
}
  800fe8:	c9                   	leave  
  800fe9:	c3                   	ret    

00800fea <sys_getenvid>:

int32 sys_getenvid(void)
{
  800fea:	55                   	push   %ebp
  800feb:	89 e5                	mov    %esp,%ebp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800fed:	6a 00                	push   $0x0
  800fef:	6a 00                	push   $0x0
  800ff1:	6a 00                	push   $0x0
  800ff3:	6a 00                	push   $0x0
  800ff5:	6a 00                	push   $0x0
  800ff7:	6a 02                	push   $0x2
  800ff9:	e8 70 ff ff ff       	call   800f6e <syscall>
  800ffe:	83 c4 18             	add    $0x18,%esp
}
  801001:	c9                   	leave  
  801002:	c3                   	ret    

00801003 <sys_env_sleep>:

void sys_env_sleep(void)
{
  801003:	55                   	push   %ebp
  801004:	89 e5                	mov    %esp,%ebp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  801006:	6a 00                	push   $0x0
  801008:	6a 00                	push   $0x0
  80100a:	6a 00                	push   $0x0
  80100c:	6a 00                	push   $0x0
  80100e:	6a 00                	push   $0x0
  801010:	6a 04                	push   $0x4
  801012:	e8 57 ff ff ff       	call   800f6e <syscall>
  801017:	83 c4 18             	add    $0x18,%esp
}
  80101a:	90                   	nop
  80101b:	c9                   	leave  
  80101c:	c3                   	ret    

0080101d <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  80101d:	55                   	push   %ebp
  80101e:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  801020:	8b 55 0c             	mov    0xc(%ebp),%edx
  801023:	8b 45 08             	mov    0x8(%ebp),%eax
  801026:	6a 00                	push   $0x0
  801028:	6a 00                	push   $0x0
  80102a:	6a 00                	push   $0x0
  80102c:	52                   	push   %edx
  80102d:	50                   	push   %eax
  80102e:	6a 05                	push   $0x5
  801030:	e8 39 ff ff ff       	call   800f6e <syscall>
  801035:	83 c4 18             	add    $0x18,%esp
}
  801038:	c9                   	leave  
  801039:	c3                   	ret    

0080103a <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  80103a:	55                   	push   %ebp
  80103b:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  80103d:	8b 55 0c             	mov    0xc(%ebp),%edx
  801040:	8b 45 08             	mov    0x8(%ebp),%eax
  801043:	6a 00                	push   $0x0
  801045:	6a 00                	push   $0x0
  801047:	6a 00                	push   $0x0
  801049:	52                   	push   %edx
  80104a:	50                   	push   %eax
  80104b:	6a 06                	push   $0x6
  80104d:	e8 1c ff ff ff       	call   800f6e <syscall>
  801052:	83 c4 18             	add    $0x18,%esp
}
  801055:	c9                   	leave  
  801056:	c3                   	ret    

00801057 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  801057:	55                   	push   %ebp
  801058:	89 e5                	mov    %esp,%ebp
  80105a:	56                   	push   %esi
  80105b:	53                   	push   %ebx
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  80105c:	8b 75 18             	mov    0x18(%ebp),%esi
  80105f:	8b 5d 14             	mov    0x14(%ebp),%ebx
  801062:	8b 4d 10             	mov    0x10(%ebp),%ecx
  801065:	8b 55 0c             	mov    0xc(%ebp),%edx
  801068:	8b 45 08             	mov    0x8(%ebp),%eax
  80106b:	56                   	push   %esi
  80106c:	53                   	push   %ebx
  80106d:	51                   	push   %ecx
  80106e:	52                   	push   %edx
  80106f:	50                   	push   %eax
  801070:	6a 07                	push   $0x7
  801072:	e8 f7 fe ff ff       	call   800f6e <syscall>
  801077:	83 c4 18             	add    $0x18,%esp
}
  80107a:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80107d:	5b                   	pop    %ebx
  80107e:	5e                   	pop    %esi
  80107f:	5d                   	pop    %ebp
  801080:	c3                   	ret    

00801081 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  801081:	55                   	push   %ebp
  801082:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  801084:	8b 55 0c             	mov    0xc(%ebp),%edx
  801087:	8b 45 08             	mov    0x8(%ebp),%eax
  80108a:	6a 00                	push   $0x0
  80108c:	6a 00                	push   $0x0
  80108e:	6a 00                	push   $0x0
  801090:	52                   	push   %edx
  801091:	50                   	push   %eax
  801092:	6a 08                	push   $0x8
  801094:	e8 d5 fe ff ff       	call   800f6e <syscall>
  801099:	83 c4 18             	add    $0x18,%esp
}
  80109c:	c9                   	leave  
  80109d:	c3                   	ret    

0080109e <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  80109e:	55                   	push   %ebp
  80109f:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  8010a1:	6a 00                	push   $0x0
  8010a3:	6a 00                	push   $0x0
  8010a5:	6a 00                	push   $0x0
  8010a7:	ff 75 0c             	pushl  0xc(%ebp)
  8010aa:	ff 75 08             	pushl  0x8(%ebp)
  8010ad:	6a 09                	push   $0x9
  8010af:	e8 ba fe ff ff       	call   800f6e <syscall>
  8010b4:	83 c4 18             	add    $0x18,%esp
}
  8010b7:	c9                   	leave  
  8010b8:	c3                   	ret    

008010b9 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  8010b9:	55                   	push   %ebp
  8010ba:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  8010bc:	6a 00                	push   $0x0
  8010be:	6a 00                	push   $0x0
  8010c0:	6a 00                	push   $0x0
  8010c2:	6a 00                	push   $0x0
  8010c4:	6a 00                	push   $0x0
  8010c6:	6a 0a                	push   $0xa
  8010c8:	e8 a1 fe ff ff       	call   800f6e <syscall>
  8010cd:	83 c4 18             	add    $0x18,%esp
}
  8010d0:	c9                   	leave  
  8010d1:	c3                   	ret    

008010d2 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  8010d2:	55                   	push   %ebp
  8010d3:	89 e5                	mov    %esp,%ebp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  8010d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8010d8:	6a 00                	push   $0x0
  8010da:	6a 00                	push   $0x0
  8010dc:	6a 00                	push   $0x0
  8010de:	ff 75 0c             	pushl  0xc(%ebp)
  8010e1:	50                   	push   %eax
  8010e2:	6a 0b                	push   $0xb
  8010e4:	e8 85 fe ff ff       	call   800f6e <syscall>
  8010e9:	83 c4 18             	add    $0x18,%esp
	return;
  8010ec:	90                   	nop
}
  8010ed:	c9                   	leave  
  8010ee:	c3                   	ret    

008010ef <_panic>:
 * It prints "panic: <message>", then causes a breakpoint exception,
 * which causes FOS to enter the FOS kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
  8010ef:	55                   	push   %ebp
  8010f0:	89 e5                	mov    %esp,%ebp
  8010f2:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  8010f5:	8d 45 10             	lea    0x10(%ebp),%eax
  8010f8:	83 c0 04             	add    $0x4,%eax
  8010fb:	89 45 f4             	mov    %eax,-0xc(%ebp)

	// Print the panic message
	if (argv0)
  8010fe:	a1 0c 20 80 00       	mov    0x80200c,%eax
  801103:	85 c0                	test   %eax,%eax
  801105:	74 16                	je     80111d <_panic+0x2e>
		cprintf("%s: ", argv0);
  801107:	a1 0c 20 80 00       	mov    0x80200c,%eax
  80110c:	83 ec 08             	sub    $0x8,%esp
  80110f:	50                   	push   %eax
  801110:	68 e8 16 80 00       	push   $0x8016e8
  801115:	e8 fa f1 ff ff       	call   800314 <cprintf>
  80111a:	83 c4 10             	add    $0x10,%esp
	cprintf("user panic in %s at %s:%d: ", binaryname, file, line);
  80111d:	a1 00 20 80 00       	mov    0x802000,%eax
  801122:	ff 75 0c             	pushl  0xc(%ebp)
  801125:	ff 75 08             	pushl  0x8(%ebp)
  801128:	50                   	push   %eax
  801129:	68 ed 16 80 00       	push   $0x8016ed
  80112e:	e8 e1 f1 ff ff       	call   800314 <cprintf>
  801133:	83 c4 10             	add    $0x10,%esp
	vcprintf(fmt, ap);
  801136:	8b 45 10             	mov    0x10(%ebp),%eax
  801139:	83 ec 08             	sub    $0x8,%esp
  80113c:	ff 75 f4             	pushl  -0xc(%ebp)
  80113f:	50                   	push   %eax
  801140:	e8 74 f1 ff ff       	call   8002b9 <vcprintf>
  801145:	83 c4 10             	add    $0x10,%esp
	cprintf("\n");
  801148:	83 ec 0c             	sub    $0xc,%esp
  80114b:	68 09 17 80 00       	push   $0x801709
  801150:	e8 bf f1 ff ff       	call   800314 <cprintf>
  801155:	83 c4 10             	add    $0x10,%esp

	// Cause a breakpoint exception
	while (1)
		asm volatile("int3");
  801158:	cc                   	int3   
  801159:	eb fd                	jmp    801158 <_panic+0x69>
  80115b:	66 90                	xchg   %ax,%ax
  80115d:	66 90                	xchg   %ax,%ax
  80115f:	90                   	nop

00801160 <__udivdi3>:
  801160:	55                   	push   %ebp
  801161:	57                   	push   %edi
  801162:	56                   	push   %esi
  801163:	53                   	push   %ebx
  801164:	83 ec 1c             	sub    $0x1c,%esp
  801167:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  80116b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  80116f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  801173:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801177:	85 f6                	test   %esi,%esi
  801179:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  80117d:	89 ca                	mov    %ecx,%edx
  80117f:	89 f8                	mov    %edi,%eax
  801181:	75 3d                	jne    8011c0 <__udivdi3+0x60>
  801183:	39 cf                	cmp    %ecx,%edi
  801185:	0f 87 c5 00 00 00    	ja     801250 <__udivdi3+0xf0>
  80118b:	85 ff                	test   %edi,%edi
  80118d:	89 fd                	mov    %edi,%ebp
  80118f:	75 0b                	jne    80119c <__udivdi3+0x3c>
  801191:	b8 01 00 00 00       	mov    $0x1,%eax
  801196:	31 d2                	xor    %edx,%edx
  801198:	f7 f7                	div    %edi
  80119a:	89 c5                	mov    %eax,%ebp
  80119c:	89 c8                	mov    %ecx,%eax
  80119e:	31 d2                	xor    %edx,%edx
  8011a0:	f7 f5                	div    %ebp
  8011a2:	89 c1                	mov    %eax,%ecx
  8011a4:	89 d8                	mov    %ebx,%eax
  8011a6:	89 cf                	mov    %ecx,%edi
  8011a8:	f7 f5                	div    %ebp
  8011aa:	89 c3                	mov    %eax,%ebx
  8011ac:	89 d8                	mov    %ebx,%eax
  8011ae:	89 fa                	mov    %edi,%edx
  8011b0:	83 c4 1c             	add    $0x1c,%esp
  8011b3:	5b                   	pop    %ebx
  8011b4:	5e                   	pop    %esi
  8011b5:	5f                   	pop    %edi
  8011b6:	5d                   	pop    %ebp
  8011b7:	c3                   	ret    
  8011b8:	90                   	nop
  8011b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8011c0:	39 ce                	cmp    %ecx,%esi
  8011c2:	77 74                	ja     801238 <__udivdi3+0xd8>
  8011c4:	0f bd fe             	bsr    %esi,%edi
  8011c7:	83 f7 1f             	xor    $0x1f,%edi
  8011ca:	0f 84 98 00 00 00    	je     801268 <__udivdi3+0x108>
  8011d0:	bb 20 00 00 00       	mov    $0x20,%ebx
  8011d5:	89 f9                	mov    %edi,%ecx
  8011d7:	89 c5                	mov    %eax,%ebp
  8011d9:	29 fb                	sub    %edi,%ebx
  8011db:	d3 e6                	shl    %cl,%esi
  8011dd:	89 d9                	mov    %ebx,%ecx
  8011df:	d3 ed                	shr    %cl,%ebp
  8011e1:	89 f9                	mov    %edi,%ecx
  8011e3:	d3 e0                	shl    %cl,%eax
  8011e5:	09 ee                	or     %ebp,%esi
  8011e7:	89 d9                	mov    %ebx,%ecx
  8011e9:	89 44 24 0c          	mov    %eax,0xc(%esp)
  8011ed:	89 d5                	mov    %edx,%ebp
  8011ef:	8b 44 24 08          	mov    0x8(%esp),%eax
  8011f3:	d3 ed                	shr    %cl,%ebp
  8011f5:	89 f9                	mov    %edi,%ecx
  8011f7:	d3 e2                	shl    %cl,%edx
  8011f9:	89 d9                	mov    %ebx,%ecx
  8011fb:	d3 e8                	shr    %cl,%eax
  8011fd:	09 c2                	or     %eax,%edx
  8011ff:	89 d0                	mov    %edx,%eax
  801201:	89 ea                	mov    %ebp,%edx
  801203:	f7 f6                	div    %esi
  801205:	89 d5                	mov    %edx,%ebp
  801207:	89 c3                	mov    %eax,%ebx
  801209:	f7 64 24 0c          	mull   0xc(%esp)
  80120d:	39 d5                	cmp    %edx,%ebp
  80120f:	72 10                	jb     801221 <__udivdi3+0xc1>
  801211:	8b 74 24 08          	mov    0x8(%esp),%esi
  801215:	89 f9                	mov    %edi,%ecx
  801217:	d3 e6                	shl    %cl,%esi
  801219:	39 c6                	cmp    %eax,%esi
  80121b:	73 07                	jae    801224 <__udivdi3+0xc4>
  80121d:	39 d5                	cmp    %edx,%ebp
  80121f:	75 03                	jne    801224 <__udivdi3+0xc4>
  801221:	83 eb 01             	sub    $0x1,%ebx
  801224:	31 ff                	xor    %edi,%edi
  801226:	89 d8                	mov    %ebx,%eax
  801228:	89 fa                	mov    %edi,%edx
  80122a:	83 c4 1c             	add    $0x1c,%esp
  80122d:	5b                   	pop    %ebx
  80122e:	5e                   	pop    %esi
  80122f:	5f                   	pop    %edi
  801230:	5d                   	pop    %ebp
  801231:	c3                   	ret    
  801232:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801238:	31 ff                	xor    %edi,%edi
  80123a:	31 db                	xor    %ebx,%ebx
  80123c:	89 d8                	mov    %ebx,%eax
  80123e:	89 fa                	mov    %edi,%edx
  801240:	83 c4 1c             	add    $0x1c,%esp
  801243:	5b                   	pop    %ebx
  801244:	5e                   	pop    %esi
  801245:	5f                   	pop    %edi
  801246:	5d                   	pop    %ebp
  801247:	c3                   	ret    
  801248:	90                   	nop
  801249:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801250:	89 d8                	mov    %ebx,%eax
  801252:	f7 f7                	div    %edi
  801254:	31 ff                	xor    %edi,%edi
  801256:	89 c3                	mov    %eax,%ebx
  801258:	89 d8                	mov    %ebx,%eax
  80125a:	89 fa                	mov    %edi,%edx
  80125c:	83 c4 1c             	add    $0x1c,%esp
  80125f:	5b                   	pop    %ebx
  801260:	5e                   	pop    %esi
  801261:	5f                   	pop    %edi
  801262:	5d                   	pop    %ebp
  801263:	c3                   	ret    
  801264:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801268:	39 ce                	cmp    %ecx,%esi
  80126a:	72 0c                	jb     801278 <__udivdi3+0x118>
  80126c:	31 db                	xor    %ebx,%ebx
  80126e:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801272:	0f 87 34 ff ff ff    	ja     8011ac <__udivdi3+0x4c>
  801278:	bb 01 00 00 00       	mov    $0x1,%ebx
  80127d:	e9 2a ff ff ff       	jmp    8011ac <__udivdi3+0x4c>
  801282:	66 90                	xchg   %ax,%ax
  801284:	66 90                	xchg   %ax,%ax
  801286:	66 90                	xchg   %ax,%ax
  801288:	66 90                	xchg   %ax,%ax
  80128a:	66 90                	xchg   %ax,%ax
  80128c:	66 90                	xchg   %ax,%ax
  80128e:	66 90                	xchg   %ax,%ax

00801290 <__umoddi3>:
  801290:	55                   	push   %ebp
  801291:	57                   	push   %edi
  801292:	56                   	push   %esi
  801293:	53                   	push   %ebx
  801294:	83 ec 1c             	sub    $0x1c,%esp
  801297:	8b 54 24 3c          	mov    0x3c(%esp),%edx
  80129b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  80129f:	8b 74 24 34          	mov    0x34(%esp),%esi
  8012a3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8012a7:	85 d2                	test   %edx,%edx
  8012a9:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8012ad:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8012b1:	89 f3                	mov    %esi,%ebx
  8012b3:	89 3c 24             	mov    %edi,(%esp)
  8012b6:	89 74 24 04          	mov    %esi,0x4(%esp)
  8012ba:	75 1c                	jne    8012d8 <__umoddi3+0x48>
  8012bc:	39 f7                	cmp    %esi,%edi
  8012be:	76 50                	jbe    801310 <__umoddi3+0x80>
  8012c0:	89 c8                	mov    %ecx,%eax
  8012c2:	89 f2                	mov    %esi,%edx
  8012c4:	f7 f7                	div    %edi
  8012c6:	89 d0                	mov    %edx,%eax
  8012c8:	31 d2                	xor    %edx,%edx
  8012ca:	83 c4 1c             	add    $0x1c,%esp
  8012cd:	5b                   	pop    %ebx
  8012ce:	5e                   	pop    %esi
  8012cf:	5f                   	pop    %edi
  8012d0:	5d                   	pop    %ebp
  8012d1:	c3                   	ret    
  8012d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  8012d8:	39 f2                	cmp    %esi,%edx
  8012da:	89 d0                	mov    %edx,%eax
  8012dc:	77 52                	ja     801330 <__umoddi3+0xa0>
  8012de:	0f bd ea             	bsr    %edx,%ebp
  8012e1:	83 f5 1f             	xor    $0x1f,%ebp
  8012e4:	75 5a                	jne    801340 <__umoddi3+0xb0>
  8012e6:	3b 54 24 04          	cmp    0x4(%esp),%edx
  8012ea:	0f 82 e0 00 00 00    	jb     8013d0 <__umoddi3+0x140>
  8012f0:	39 0c 24             	cmp    %ecx,(%esp)
  8012f3:	0f 86 d7 00 00 00    	jbe    8013d0 <__umoddi3+0x140>
  8012f9:	8b 44 24 08          	mov    0x8(%esp),%eax
  8012fd:	8b 54 24 04          	mov    0x4(%esp),%edx
  801301:	83 c4 1c             	add    $0x1c,%esp
  801304:	5b                   	pop    %ebx
  801305:	5e                   	pop    %esi
  801306:	5f                   	pop    %edi
  801307:	5d                   	pop    %ebp
  801308:	c3                   	ret    
  801309:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801310:	85 ff                	test   %edi,%edi
  801312:	89 fd                	mov    %edi,%ebp
  801314:	75 0b                	jne    801321 <__umoddi3+0x91>
  801316:	b8 01 00 00 00       	mov    $0x1,%eax
  80131b:	31 d2                	xor    %edx,%edx
  80131d:	f7 f7                	div    %edi
  80131f:	89 c5                	mov    %eax,%ebp
  801321:	89 f0                	mov    %esi,%eax
  801323:	31 d2                	xor    %edx,%edx
  801325:	f7 f5                	div    %ebp
  801327:	89 c8                	mov    %ecx,%eax
  801329:	f7 f5                	div    %ebp
  80132b:	89 d0                	mov    %edx,%eax
  80132d:	eb 99                	jmp    8012c8 <__umoddi3+0x38>
  80132f:	90                   	nop
  801330:	89 c8                	mov    %ecx,%eax
  801332:	89 f2                	mov    %esi,%edx
  801334:	83 c4 1c             	add    $0x1c,%esp
  801337:	5b                   	pop    %ebx
  801338:	5e                   	pop    %esi
  801339:	5f                   	pop    %edi
  80133a:	5d                   	pop    %ebp
  80133b:	c3                   	ret    
  80133c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801340:	8b 34 24             	mov    (%esp),%esi
  801343:	bf 20 00 00 00       	mov    $0x20,%edi
  801348:	89 e9                	mov    %ebp,%ecx
  80134a:	29 ef                	sub    %ebp,%edi
  80134c:	d3 e0                	shl    %cl,%eax
  80134e:	89 f9                	mov    %edi,%ecx
  801350:	89 f2                	mov    %esi,%edx
  801352:	d3 ea                	shr    %cl,%edx
  801354:	89 e9                	mov    %ebp,%ecx
  801356:	09 c2                	or     %eax,%edx
  801358:	89 d8                	mov    %ebx,%eax
  80135a:	89 14 24             	mov    %edx,(%esp)
  80135d:	89 f2                	mov    %esi,%edx
  80135f:	d3 e2                	shl    %cl,%edx
  801361:	89 f9                	mov    %edi,%ecx
  801363:	89 54 24 04          	mov    %edx,0x4(%esp)
  801367:	8b 54 24 0c          	mov    0xc(%esp),%edx
  80136b:	d3 e8                	shr    %cl,%eax
  80136d:	89 e9                	mov    %ebp,%ecx
  80136f:	89 c6                	mov    %eax,%esi
  801371:	d3 e3                	shl    %cl,%ebx
  801373:	89 f9                	mov    %edi,%ecx
  801375:	89 d0                	mov    %edx,%eax
  801377:	d3 e8                	shr    %cl,%eax
  801379:	89 e9                	mov    %ebp,%ecx
  80137b:	09 d8                	or     %ebx,%eax
  80137d:	89 d3                	mov    %edx,%ebx
  80137f:	89 f2                	mov    %esi,%edx
  801381:	f7 34 24             	divl   (%esp)
  801384:	89 d6                	mov    %edx,%esi
  801386:	d3 e3                	shl    %cl,%ebx
  801388:	f7 64 24 04          	mull   0x4(%esp)
  80138c:	39 d6                	cmp    %edx,%esi
  80138e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801392:	89 d1                	mov    %edx,%ecx
  801394:	89 c3                	mov    %eax,%ebx
  801396:	72 08                	jb     8013a0 <__umoddi3+0x110>
  801398:	75 11                	jne    8013ab <__umoddi3+0x11b>
  80139a:	39 44 24 08          	cmp    %eax,0x8(%esp)
  80139e:	73 0b                	jae    8013ab <__umoddi3+0x11b>
  8013a0:	2b 44 24 04          	sub    0x4(%esp),%eax
  8013a4:	1b 14 24             	sbb    (%esp),%edx
  8013a7:	89 d1                	mov    %edx,%ecx
  8013a9:	89 c3                	mov    %eax,%ebx
  8013ab:	8b 54 24 08          	mov    0x8(%esp),%edx
  8013af:	29 da                	sub    %ebx,%edx
  8013b1:	19 ce                	sbb    %ecx,%esi
  8013b3:	89 f9                	mov    %edi,%ecx
  8013b5:	89 f0                	mov    %esi,%eax
  8013b7:	d3 e0                	shl    %cl,%eax
  8013b9:	89 e9                	mov    %ebp,%ecx
  8013bb:	d3 ea                	shr    %cl,%edx
  8013bd:	89 e9                	mov    %ebp,%ecx
  8013bf:	d3 ee                	shr    %cl,%esi
  8013c1:	09 d0                	or     %edx,%eax
  8013c3:	89 f2                	mov    %esi,%edx
  8013c5:	83 c4 1c             	add    $0x1c,%esp
  8013c8:	5b                   	pop    %ebx
  8013c9:	5e                   	pop    %esi
  8013ca:	5f                   	pop    %edi
  8013cb:	5d                   	pop    %ebp
  8013cc:	c3                   	ret    
  8013cd:	8d 76 00             	lea    0x0(%esi),%esi
  8013d0:	29 f9                	sub    %edi,%ecx
  8013d2:	19 d6                	sbb    %edx,%esi
  8013d4:	89 74 24 04          	mov    %esi,0x4(%esp)
  8013d8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8013dc:	e9 18 ff ff ff       	jmp    8012f9 <__umoddi3+0x69>
