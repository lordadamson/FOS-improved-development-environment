
obj/user/game:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 7d 00 00 00       	call   8000b3 <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
#include <inc/lib.h>
	
void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 18             	sub    $0x18,%esp
	int i=28;
  80003e:	c7 45 f4 1c 00 00 00 	movl   $0x1c,-0xc(%ebp)
	for(;i<128; i++)
  800045:	eb 63                	jmp    8000aa <_main+0x72>
	{
		int c=0;
  800047:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
		for(;c<10; c++)
  80004e:	eb 17                	jmp    800067 <_main+0x2f>
		{
			cprintf("%c",i);
  800050:	83 ec 08             	sub    $0x8,%esp
  800053:	ff 75 f4             	pushl  -0xc(%ebp)
  800056:	68 20 12 80 00       	push   $0x801220
  80005b:	e8 6b 01 00 00       	call   8001cb <cprintf>
  800060:	83 c4 10             	add    $0x10,%esp
{	
	int i=28;
	for(;i<128; i++)
	{
		int c=0;
		for(;c<10; c++)
  800063:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  800067:	83 7d f0 09          	cmpl   $0x9,-0x10(%ebp)
  80006b:	7e e3                	jle    800050 <_main+0x18>
		{
			cprintf("%c",i);
		}
		int d=0;
  80006d:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
		for(; d< 500000; d++);	
  800074:	eb 04                	jmp    80007a <_main+0x42>
  800076:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
  80007a:	81 7d ec 1f a1 07 00 	cmpl   $0x7a11f,-0x14(%ebp)
  800081:	7e f3                	jle    800076 <_main+0x3e>
		c=0;
  800083:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
		for(;c<10; c++)
  80008a:	eb 14                	jmp    8000a0 <_main+0x68>
		{
			cprintf("\b");
  80008c:	83 ec 0c             	sub    $0xc,%esp
  80008f:	68 23 12 80 00       	push   $0x801223
  800094:	e8 32 01 00 00       	call   8001cb <cprintf>
  800099:	83 c4 10             	add    $0x10,%esp
			cprintf("%c",i);
		}
		int d=0;
		for(; d< 500000; d++);	
		c=0;
		for(;c<10; c++)
  80009c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
  8000a0:	83 7d f0 09          	cmpl   $0x9,-0x10(%ebp)
  8000a4:	7e e6                	jle    80008c <_main+0x54>
	
void
_main(void)
{	
	int i=28;
	for(;i<128; i++)
  8000a6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  8000aa:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
  8000ae:	7e 97                	jle    800047 <_main+0xf>
		{
			cprintf("\b");
		}		
	}
	
	return;	
  8000b0:	90                   	nop
}
  8000b1:	c9                   	leave  
  8000b2:	c3                   	ret    

008000b3 <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8000b3:	55                   	push   %ebp
  8000b4:	89 e5                	mov    %esp,%ebp
  8000b6:	83 ec 08             	sub    $0x8,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8000b9:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000c0:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000c3:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000c7:	7e 0a                	jle    8000d3 <libmain+0x20>
		binaryname = argv[0];
  8000c9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000cc:	8b 00                	mov    (%eax),%eax
  8000ce:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000d3:	83 ec 08             	sub    $0x8,%esp
  8000d6:	ff 75 0c             	pushl  0xc(%ebp)
  8000d9:	ff 75 08             	pushl  0x8(%ebp)
  8000dc:	e8 57 ff ff ff       	call   800038 <_main>
  8000e1:	83 c4 10             	add    $0x10,%esp

	// exit gracefully
	//exit();
	sleep();
  8000e4:	e8 19 00 00 00       	call   800102 <sleep>
}
  8000e9:	90                   	nop
  8000ea:	c9                   	leave  
  8000eb:	c3                   	ret    

008000ec <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000ec:	55                   	push   %ebp
  8000ed:	89 e5                	mov    %esp,%ebp
  8000ef:	83 ec 08             	sub    $0x8,%esp
	sys_env_destroy(0);	
  8000f2:	83 ec 0c             	sub    $0xc,%esp
  8000f5:	6a 00                	push   $0x0
  8000f7:	e8 56 0d 00 00       	call   800e52 <sys_env_destroy>
  8000fc:	83 c4 10             	add    $0x10,%esp
}
  8000ff:	90                   	nop
  800100:	c9                   	leave  
  800101:	c3                   	ret    

00800102 <sleep>:

void
sleep(void)
{	
  800102:	55                   	push   %ebp
  800103:	89 e5                	mov    %esp,%ebp
  800105:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  800108:	e8 79 0d 00 00       	call   800e86 <sys_env_sleep>
}
  80010d:	90                   	nop
  80010e:	c9                   	leave  
  80010f:	c3                   	ret    

00800110 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800110:	55                   	push   %ebp
  800111:	89 e5                	mov    %esp,%ebp
  800113:	83 ec 08             	sub    $0x8,%esp
	b->buf[b->idx++] = ch;
  800116:	8b 45 0c             	mov    0xc(%ebp),%eax
  800119:	8b 00                	mov    (%eax),%eax
  80011b:	8d 48 01             	lea    0x1(%eax),%ecx
  80011e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800121:	89 0a                	mov    %ecx,(%edx)
  800123:	8b 55 08             	mov    0x8(%ebp),%edx
  800126:	89 d1                	mov    %edx,%ecx
  800128:	8b 55 0c             	mov    0xc(%ebp),%edx
  80012b:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  80012f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800132:	8b 00                	mov    (%eax),%eax
  800134:	3d ff 00 00 00       	cmp    $0xff,%eax
  800139:	75 23                	jne    80015e <putch+0x4e>
		sys_cputs(b->buf, b->idx);
  80013b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80013e:	8b 00                	mov    (%eax),%eax
  800140:	89 c2                	mov    %eax,%edx
  800142:	8b 45 0c             	mov    0xc(%ebp),%eax
  800145:	83 c0 08             	add    $0x8,%eax
  800148:	83 ec 08             	sub    $0x8,%esp
  80014b:	52                   	push   %edx
  80014c:	50                   	push   %eax
  80014d:	e8 ca 0c 00 00       	call   800e1c <sys_cputs>
  800152:	83 c4 10             	add    $0x10,%esp
		b->idx = 0;
  800155:	8b 45 0c             	mov    0xc(%ebp),%eax
  800158:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  80015e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800161:	8b 40 04             	mov    0x4(%eax),%eax
  800164:	8d 50 01             	lea    0x1(%eax),%edx
  800167:	8b 45 0c             	mov    0xc(%ebp),%eax
  80016a:	89 50 04             	mov    %edx,0x4(%eax)
}
  80016d:	90                   	nop
  80016e:	c9                   	leave  
  80016f:	c3                   	ret    

00800170 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800170:	55                   	push   %ebp
  800171:	89 e5                	mov    %esp,%ebp
  800173:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800179:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800180:	00 00 00 
	b.cnt = 0;
  800183:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80018a:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80018d:	ff 75 0c             	pushl  0xc(%ebp)
  800190:	ff 75 08             	pushl  0x8(%ebp)
  800193:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800199:	50                   	push   %eax
  80019a:	68 10 01 80 00       	push   $0x800110
  80019f:	e8 cc 01 00 00       	call   800370 <vprintfmt>
  8001a4:	83 c4 10             	add    $0x10,%esp
	sys_cputs(b.buf, b.idx);
  8001a7:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8001ad:	83 ec 08             	sub    $0x8,%esp
  8001b0:	50                   	push   %eax
  8001b1:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001b7:	83 c0 08             	add    $0x8,%eax
  8001ba:	50                   	push   %eax
  8001bb:	e8 5c 0c 00 00       	call   800e1c <sys_cputs>
  8001c0:	83 c4 10             	add    $0x10,%esp

	return b.cnt;
  8001c3:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001c9:	c9                   	leave  
  8001ca:	c3                   	ret    

008001cb <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001cb:	55                   	push   %ebp
  8001cc:	89 e5                	mov    %esp,%ebp
  8001ce:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001d1:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001d4:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001d7:	8b 45 08             	mov    0x8(%ebp),%eax
  8001da:	83 ec 08             	sub    $0x8,%esp
  8001dd:	ff 75 f4             	pushl  -0xc(%ebp)
  8001e0:	50                   	push   %eax
  8001e1:	e8 8a ff ff ff       	call   800170 <vcprintf>
  8001e6:	83 c4 10             	add    $0x10,%esp
  8001e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  8001ec:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8001ef:	c9                   	leave  
  8001f0:	c3                   	ret    

008001f1 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001f1:	55                   	push   %ebp
  8001f2:	89 e5                	mov    %esp,%ebp
  8001f4:	53                   	push   %ebx
  8001f5:	83 ec 14             	sub    $0x14,%esp
  8001f8:	8b 45 10             	mov    0x10(%ebp),%eax
  8001fb:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8001fe:	8b 45 14             	mov    0x14(%ebp),%eax
  800201:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  800204:	8b 45 18             	mov    0x18(%ebp),%eax
  800207:	ba 00 00 00 00       	mov    $0x0,%edx
  80020c:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80020f:	77 55                	ja     800266 <printnum+0x75>
  800211:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800214:	72 05                	jb     80021b <printnum+0x2a>
  800216:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800219:	77 4b                	ja     800266 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  80021b:	8b 45 1c             	mov    0x1c(%ebp),%eax
  80021e:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800221:	8b 45 18             	mov    0x18(%ebp),%eax
  800224:	ba 00 00 00 00       	mov    $0x0,%edx
  800229:	52                   	push   %edx
  80022a:	50                   	push   %eax
  80022b:	ff 75 f4             	pushl  -0xc(%ebp)
  80022e:	ff 75 f0             	pushl  -0x10(%ebp)
  800231:	e8 4a 0d 00 00       	call   800f80 <__udivdi3>
  800236:	83 c4 10             	add    $0x10,%esp
  800239:	83 ec 04             	sub    $0x4,%esp
  80023c:	ff 75 20             	pushl  0x20(%ebp)
  80023f:	53                   	push   %ebx
  800240:	ff 75 18             	pushl  0x18(%ebp)
  800243:	52                   	push   %edx
  800244:	50                   	push   %eax
  800245:	ff 75 0c             	pushl  0xc(%ebp)
  800248:	ff 75 08             	pushl  0x8(%ebp)
  80024b:	e8 a1 ff ff ff       	call   8001f1 <printnum>
  800250:	83 c4 20             	add    $0x20,%esp
  800253:	eb 1b                	jmp    800270 <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800255:	83 ec 08             	sub    $0x8,%esp
  800258:	ff 75 0c             	pushl  0xc(%ebp)
  80025b:	ff 75 20             	pushl  0x20(%ebp)
  80025e:	8b 45 08             	mov    0x8(%ebp),%eax
  800261:	ff d0                	call   *%eax
  800263:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800266:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  80026a:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  80026e:	7f e5                	jg     800255 <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800270:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800273:	bb 00 00 00 00       	mov    $0x0,%ebx
  800278:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80027b:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80027e:	53                   	push   %ebx
  80027f:	51                   	push   %ecx
  800280:	52                   	push   %edx
  800281:	50                   	push   %eax
  800282:	e8 29 0e 00 00       	call   8010b0 <__umoddi3>
  800287:	83 c4 10             	add    $0x10,%esp
  80028a:	05 e0 12 80 00       	add    $0x8012e0,%eax
  80028f:	0f b6 00             	movzbl (%eax),%eax
  800292:	0f be c0             	movsbl %al,%eax
  800295:	83 ec 08             	sub    $0x8,%esp
  800298:	ff 75 0c             	pushl  0xc(%ebp)
  80029b:	50                   	push   %eax
  80029c:	8b 45 08             	mov    0x8(%ebp),%eax
  80029f:	ff d0                	call   *%eax
  8002a1:	83 c4 10             	add    $0x10,%esp
}
  8002a4:	90                   	nop
  8002a5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8002a8:	c9                   	leave  
  8002a9:	c3                   	ret    

008002aa <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002aa:	55                   	push   %ebp
  8002ab:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002ad:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002b1:	7e 1c                	jle    8002cf <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8002b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002b6:	8b 00                	mov    (%eax),%eax
  8002b8:	8d 50 08             	lea    0x8(%eax),%edx
  8002bb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002be:	89 10                	mov    %edx,(%eax)
  8002c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c3:	8b 00                	mov    (%eax),%eax
  8002c5:	83 e8 08             	sub    $0x8,%eax
  8002c8:	8b 50 04             	mov    0x4(%eax),%edx
  8002cb:	8b 00                	mov    (%eax),%eax
  8002cd:	eb 40                	jmp    80030f <getuint+0x65>
	else if (lflag)
  8002cf:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002d3:	74 1e                	je     8002f3 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  8002d5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d8:	8b 00                	mov    (%eax),%eax
  8002da:	8d 50 04             	lea    0x4(%eax),%edx
  8002dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e0:	89 10                	mov    %edx,(%eax)
  8002e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e5:	8b 00                	mov    (%eax),%eax
  8002e7:	83 e8 04             	sub    $0x4,%eax
  8002ea:	8b 00                	mov    (%eax),%eax
  8002ec:	ba 00 00 00 00       	mov    $0x0,%edx
  8002f1:	eb 1c                	jmp    80030f <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  8002f3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f6:	8b 00                	mov    (%eax),%eax
  8002f8:	8d 50 04             	lea    0x4(%eax),%edx
  8002fb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002fe:	89 10                	mov    %edx,(%eax)
  800300:	8b 45 08             	mov    0x8(%ebp),%eax
  800303:	8b 00                	mov    (%eax),%eax
  800305:	83 e8 04             	sub    $0x4,%eax
  800308:	8b 00                	mov    (%eax),%eax
  80030a:	ba 00 00 00 00       	mov    $0x0,%edx
}
  80030f:	5d                   	pop    %ebp
  800310:	c3                   	ret    

00800311 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  800311:	55                   	push   %ebp
  800312:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800314:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800318:	7e 1c                	jle    800336 <getint+0x25>
		return va_arg(*ap, long long);
  80031a:	8b 45 08             	mov    0x8(%ebp),%eax
  80031d:	8b 00                	mov    (%eax),%eax
  80031f:	8d 50 08             	lea    0x8(%eax),%edx
  800322:	8b 45 08             	mov    0x8(%ebp),%eax
  800325:	89 10                	mov    %edx,(%eax)
  800327:	8b 45 08             	mov    0x8(%ebp),%eax
  80032a:	8b 00                	mov    (%eax),%eax
  80032c:	83 e8 08             	sub    $0x8,%eax
  80032f:	8b 50 04             	mov    0x4(%eax),%edx
  800332:	8b 00                	mov    (%eax),%eax
  800334:	eb 38                	jmp    80036e <getint+0x5d>
	else if (lflag)
  800336:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80033a:	74 1a                	je     800356 <getint+0x45>
		return va_arg(*ap, long);
  80033c:	8b 45 08             	mov    0x8(%ebp),%eax
  80033f:	8b 00                	mov    (%eax),%eax
  800341:	8d 50 04             	lea    0x4(%eax),%edx
  800344:	8b 45 08             	mov    0x8(%ebp),%eax
  800347:	89 10                	mov    %edx,(%eax)
  800349:	8b 45 08             	mov    0x8(%ebp),%eax
  80034c:	8b 00                	mov    (%eax),%eax
  80034e:	83 e8 04             	sub    $0x4,%eax
  800351:	8b 00                	mov    (%eax),%eax
  800353:	99                   	cltd   
  800354:	eb 18                	jmp    80036e <getint+0x5d>
	else
		return va_arg(*ap, int);
  800356:	8b 45 08             	mov    0x8(%ebp),%eax
  800359:	8b 00                	mov    (%eax),%eax
  80035b:	8d 50 04             	lea    0x4(%eax),%edx
  80035e:	8b 45 08             	mov    0x8(%ebp),%eax
  800361:	89 10                	mov    %edx,(%eax)
  800363:	8b 45 08             	mov    0x8(%ebp),%eax
  800366:	8b 00                	mov    (%eax),%eax
  800368:	83 e8 04             	sub    $0x4,%eax
  80036b:	8b 00                	mov    (%eax),%eax
  80036d:	99                   	cltd   
}
  80036e:	5d                   	pop    %ebp
  80036f:	c3                   	ret    

00800370 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800370:	55                   	push   %ebp
  800371:	89 e5                	mov    %esp,%ebp
  800373:	56                   	push   %esi
  800374:	53                   	push   %ebx
  800375:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800378:	eb 17                	jmp    800391 <vprintfmt+0x21>
			if (ch == '\0')
  80037a:	85 db                	test   %ebx,%ebx
  80037c:	0f 84 be 03 00 00    	je     800740 <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
  800382:	83 ec 08             	sub    $0x8,%esp
  800385:	ff 75 0c             	pushl  0xc(%ebp)
  800388:	53                   	push   %ebx
  800389:	8b 45 08             	mov    0x8(%ebp),%eax
  80038c:	ff d0                	call   *%eax
  80038e:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800391:	8b 45 10             	mov    0x10(%ebp),%eax
  800394:	8d 50 01             	lea    0x1(%eax),%edx
  800397:	89 55 10             	mov    %edx,0x10(%ebp)
  80039a:	0f b6 00             	movzbl (%eax),%eax
  80039d:	0f b6 d8             	movzbl %al,%ebx
  8003a0:	83 fb 25             	cmp    $0x25,%ebx
  8003a3:	75 d5                	jne    80037a <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8003a5:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8003a9:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8003b0:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  8003b7:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  8003be:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003c5:	8b 45 10             	mov    0x10(%ebp),%eax
  8003c8:	8d 50 01             	lea    0x1(%eax),%edx
  8003cb:	89 55 10             	mov    %edx,0x10(%ebp)
  8003ce:	0f b6 00             	movzbl (%eax),%eax
  8003d1:	0f b6 d8             	movzbl %al,%ebx
  8003d4:	8d 43 dd             	lea    -0x23(%ebx),%eax
  8003d7:	83 f8 55             	cmp    $0x55,%eax
  8003da:	0f 87 33 03 00 00    	ja     800713 <vprintfmt+0x3a3>
  8003e0:	8b 04 85 04 13 80 00 	mov    0x801304(,%eax,4),%eax
  8003e7:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  8003e9:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  8003ed:	eb d6                	jmp    8003c5 <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003ef:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  8003f3:	eb d0                	jmp    8003c5 <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003f5:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  8003fc:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003ff:	89 d0                	mov    %edx,%eax
  800401:	c1 e0 02             	shl    $0x2,%eax
  800404:	01 d0                	add    %edx,%eax
  800406:	01 c0                	add    %eax,%eax
  800408:	01 d8                	add    %ebx,%eax
  80040a:	83 e8 30             	sub    $0x30,%eax
  80040d:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  800410:	8b 45 10             	mov    0x10(%ebp),%eax
  800413:	0f b6 00             	movzbl (%eax),%eax
  800416:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  800419:	83 fb 2f             	cmp    $0x2f,%ebx
  80041c:	7e 3f                	jle    80045d <vprintfmt+0xed>
  80041e:	83 fb 39             	cmp    $0x39,%ebx
  800421:	7f 3a                	jg     80045d <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800423:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  800427:	eb d3                	jmp    8003fc <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800429:	8b 45 14             	mov    0x14(%ebp),%eax
  80042c:	83 c0 04             	add    $0x4,%eax
  80042f:	89 45 14             	mov    %eax,0x14(%ebp)
  800432:	8b 45 14             	mov    0x14(%ebp),%eax
  800435:	83 e8 04             	sub    $0x4,%eax
  800438:	8b 00                	mov    (%eax),%eax
  80043a:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  80043d:	eb 1f                	jmp    80045e <vprintfmt+0xee>

		case '.':
			if (width < 0)
  80043f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800443:	79 80                	jns    8003c5 <vprintfmt+0x55>
				width = 0;
  800445:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  80044c:	e9 74 ff ff ff       	jmp    8003c5 <vprintfmt+0x55>

		case '#':
			altflag = 1;
  800451:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  800458:	e9 68 ff ff ff       	jmp    8003c5 <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
  80045d:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
  80045e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800462:	0f 89 5d ff ff ff    	jns    8003c5 <vprintfmt+0x55>
				width = precision, precision = -1;
  800468:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80046b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80046e:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  800475:	e9 4b ff ff ff       	jmp    8003c5 <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80047a:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  80047e:	e9 42 ff ff ff       	jmp    8003c5 <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800483:	8b 45 14             	mov    0x14(%ebp),%eax
  800486:	83 c0 04             	add    $0x4,%eax
  800489:	89 45 14             	mov    %eax,0x14(%ebp)
  80048c:	8b 45 14             	mov    0x14(%ebp),%eax
  80048f:	83 e8 04             	sub    $0x4,%eax
  800492:	8b 00                	mov    (%eax),%eax
  800494:	83 ec 08             	sub    $0x8,%esp
  800497:	ff 75 0c             	pushl  0xc(%ebp)
  80049a:	50                   	push   %eax
  80049b:	8b 45 08             	mov    0x8(%ebp),%eax
  80049e:	ff d0                	call   *%eax
  8004a0:	83 c4 10             	add    $0x10,%esp
			break;
  8004a3:	e9 93 02 00 00       	jmp    80073b <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004a8:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ab:	83 c0 04             	add    $0x4,%eax
  8004ae:	89 45 14             	mov    %eax,0x14(%ebp)
  8004b1:	8b 45 14             	mov    0x14(%ebp),%eax
  8004b4:	83 e8 04             	sub    $0x4,%eax
  8004b7:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  8004b9:	85 db                	test   %ebx,%ebx
  8004bb:	79 02                	jns    8004bf <vprintfmt+0x14f>
				err = -err;
  8004bd:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  8004bf:	83 fb 07             	cmp    $0x7,%ebx
  8004c2:	7f 0b                	jg     8004cf <vprintfmt+0x15f>
  8004c4:	8b 34 9d c0 12 80 00 	mov    0x8012c0(,%ebx,4),%esi
  8004cb:	85 f6                	test   %esi,%esi
  8004cd:	75 19                	jne    8004e8 <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
  8004cf:	53                   	push   %ebx
  8004d0:	68 f1 12 80 00       	push   $0x8012f1
  8004d5:	ff 75 0c             	pushl  0xc(%ebp)
  8004d8:	ff 75 08             	pushl  0x8(%ebp)
  8004db:	e8 68 02 00 00       	call   800748 <printfmt>
  8004e0:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
  8004e3:	e9 53 02 00 00       	jmp    80073b <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  8004e8:	56                   	push   %esi
  8004e9:	68 fa 12 80 00       	push   $0x8012fa
  8004ee:	ff 75 0c             	pushl  0xc(%ebp)
  8004f1:	ff 75 08             	pushl  0x8(%ebp)
  8004f4:	e8 4f 02 00 00       	call   800748 <printfmt>
  8004f9:	83 c4 10             	add    $0x10,%esp
			break;
  8004fc:	e9 3a 02 00 00       	jmp    80073b <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800501:	8b 45 14             	mov    0x14(%ebp),%eax
  800504:	83 c0 04             	add    $0x4,%eax
  800507:	89 45 14             	mov    %eax,0x14(%ebp)
  80050a:	8b 45 14             	mov    0x14(%ebp),%eax
  80050d:	83 e8 04             	sub    $0x4,%eax
  800510:	8b 30                	mov    (%eax),%esi
  800512:	85 f6                	test   %esi,%esi
  800514:	75 05                	jne    80051b <vprintfmt+0x1ab>
				p = "(null)";
  800516:	be fd 12 80 00       	mov    $0x8012fd,%esi
			if (width > 0 && padc != '-')
  80051b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80051f:	7e 6f                	jle    800590 <vprintfmt+0x220>
  800521:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  800525:	74 69                	je     800590 <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
  800527:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80052a:	83 ec 08             	sub    $0x8,%esp
  80052d:	50                   	push   %eax
  80052e:	56                   	push   %esi
  80052f:	e8 19 03 00 00       	call   80084d <strnlen>
  800534:	83 c4 10             	add    $0x10,%esp
  800537:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  80053a:	eb 17                	jmp    800553 <vprintfmt+0x1e3>
					putch(padc, putdat);
  80053c:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  800540:	83 ec 08             	sub    $0x8,%esp
  800543:	ff 75 0c             	pushl  0xc(%ebp)
  800546:	50                   	push   %eax
  800547:	8b 45 08             	mov    0x8(%ebp),%eax
  80054a:	ff d0                	call   *%eax
  80054c:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  80054f:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800553:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800557:	7f e3                	jg     80053c <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800559:	eb 35                	jmp    800590 <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
  80055b:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  80055f:	74 1c                	je     80057d <vprintfmt+0x20d>
  800561:	83 fb 1f             	cmp    $0x1f,%ebx
  800564:	7e 05                	jle    80056b <vprintfmt+0x1fb>
  800566:	83 fb 7e             	cmp    $0x7e,%ebx
  800569:	7e 12                	jle    80057d <vprintfmt+0x20d>
					putch('?', putdat);
  80056b:	83 ec 08             	sub    $0x8,%esp
  80056e:	ff 75 0c             	pushl  0xc(%ebp)
  800571:	6a 3f                	push   $0x3f
  800573:	8b 45 08             	mov    0x8(%ebp),%eax
  800576:	ff d0                	call   *%eax
  800578:	83 c4 10             	add    $0x10,%esp
  80057b:	eb 0f                	jmp    80058c <vprintfmt+0x21c>
				else
					putch(ch, putdat);
  80057d:	83 ec 08             	sub    $0x8,%esp
  800580:	ff 75 0c             	pushl  0xc(%ebp)
  800583:	53                   	push   %ebx
  800584:	8b 45 08             	mov    0x8(%ebp),%eax
  800587:	ff d0                	call   *%eax
  800589:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80058c:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800590:	89 f0                	mov    %esi,%eax
  800592:	8d 70 01             	lea    0x1(%eax),%esi
  800595:	0f b6 00             	movzbl (%eax),%eax
  800598:	0f be d8             	movsbl %al,%ebx
  80059b:	85 db                	test   %ebx,%ebx
  80059d:	74 26                	je     8005c5 <vprintfmt+0x255>
  80059f:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005a3:	78 b6                	js     80055b <vprintfmt+0x1eb>
  8005a5:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  8005a9:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005ad:	79 ac                	jns    80055b <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005af:	eb 14                	jmp    8005c5 <vprintfmt+0x255>
				putch(' ', putdat);
  8005b1:	83 ec 08             	sub    $0x8,%esp
  8005b4:	ff 75 0c             	pushl  0xc(%ebp)
  8005b7:	6a 20                	push   $0x20
  8005b9:	8b 45 08             	mov    0x8(%ebp),%eax
  8005bc:	ff d0                	call   *%eax
  8005be:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005c1:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005c5:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005c9:	7f e6                	jg     8005b1 <vprintfmt+0x241>
				putch(' ', putdat);
			break;
  8005cb:	e9 6b 01 00 00       	jmp    80073b <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005d0:	83 ec 08             	sub    $0x8,%esp
  8005d3:	ff 75 e8             	pushl  -0x18(%ebp)
  8005d6:	8d 45 14             	lea    0x14(%ebp),%eax
  8005d9:	50                   	push   %eax
  8005da:	e8 32 fd ff ff       	call   800311 <getint>
  8005df:	83 c4 10             	add    $0x10,%esp
  8005e2:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005e5:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  8005e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005eb:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005ee:	85 d2                	test   %edx,%edx
  8005f0:	79 23                	jns    800615 <vprintfmt+0x2a5>
				putch('-', putdat);
  8005f2:	83 ec 08             	sub    $0x8,%esp
  8005f5:	ff 75 0c             	pushl  0xc(%ebp)
  8005f8:	6a 2d                	push   $0x2d
  8005fa:	8b 45 08             	mov    0x8(%ebp),%eax
  8005fd:	ff d0                	call   *%eax
  8005ff:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
  800602:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800605:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800608:	f7 d8                	neg    %eax
  80060a:	83 d2 00             	adc    $0x0,%edx
  80060d:	f7 da                	neg    %edx
  80060f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800612:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  800615:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  80061c:	e9 bc 00 00 00       	jmp    8006dd <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800621:	83 ec 08             	sub    $0x8,%esp
  800624:	ff 75 e8             	pushl  -0x18(%ebp)
  800627:	8d 45 14             	lea    0x14(%ebp),%eax
  80062a:	50                   	push   %eax
  80062b:	e8 7a fc ff ff       	call   8002aa <getuint>
  800630:	83 c4 10             	add    $0x10,%esp
  800633:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800636:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  800639:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800640:	e9 98 00 00 00       	jmp    8006dd <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  800645:	83 ec 08             	sub    $0x8,%esp
  800648:	ff 75 0c             	pushl  0xc(%ebp)
  80064b:	6a 58                	push   $0x58
  80064d:	8b 45 08             	mov    0x8(%ebp),%eax
  800650:	ff d0                	call   *%eax
  800652:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  800655:	83 ec 08             	sub    $0x8,%esp
  800658:	ff 75 0c             	pushl  0xc(%ebp)
  80065b:	6a 58                	push   $0x58
  80065d:	8b 45 08             	mov    0x8(%ebp),%eax
  800660:	ff d0                	call   *%eax
  800662:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  800665:	83 ec 08             	sub    $0x8,%esp
  800668:	ff 75 0c             	pushl  0xc(%ebp)
  80066b:	6a 58                	push   $0x58
  80066d:	8b 45 08             	mov    0x8(%ebp),%eax
  800670:	ff d0                	call   *%eax
  800672:	83 c4 10             	add    $0x10,%esp
			break;
  800675:	e9 c1 00 00 00       	jmp    80073b <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
  80067a:	83 ec 08             	sub    $0x8,%esp
  80067d:	ff 75 0c             	pushl  0xc(%ebp)
  800680:	6a 30                	push   $0x30
  800682:	8b 45 08             	mov    0x8(%ebp),%eax
  800685:	ff d0                	call   *%eax
  800687:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
  80068a:	83 ec 08             	sub    $0x8,%esp
  80068d:	ff 75 0c             	pushl  0xc(%ebp)
  800690:	6a 78                	push   $0x78
  800692:	8b 45 08             	mov    0x8(%ebp),%eax
  800695:	ff d0                	call   *%eax
  800697:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  80069a:	8b 45 14             	mov    0x14(%ebp),%eax
  80069d:	83 c0 04             	add    $0x4,%eax
  8006a0:	89 45 14             	mov    %eax,0x14(%ebp)
  8006a3:	8b 45 14             	mov    0x14(%ebp),%eax
  8006a6:	83 e8 04             	sub    $0x4,%eax
  8006a9:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006ab:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006ae:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  8006b5:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  8006bc:	eb 1f                	jmp    8006dd <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006be:	83 ec 08             	sub    $0x8,%esp
  8006c1:	ff 75 e8             	pushl  -0x18(%ebp)
  8006c4:	8d 45 14             	lea    0x14(%ebp),%eax
  8006c7:	50                   	push   %eax
  8006c8:	e8 dd fb ff ff       	call   8002aa <getuint>
  8006cd:	83 c4 10             	add    $0x10,%esp
  8006d0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006d3:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  8006d6:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006dd:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  8006e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006e4:	83 ec 04             	sub    $0x4,%esp
  8006e7:	52                   	push   %edx
  8006e8:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006eb:	50                   	push   %eax
  8006ec:	ff 75 f4             	pushl  -0xc(%ebp)
  8006ef:	ff 75 f0             	pushl  -0x10(%ebp)
  8006f2:	ff 75 0c             	pushl  0xc(%ebp)
  8006f5:	ff 75 08             	pushl  0x8(%ebp)
  8006f8:	e8 f4 fa ff ff       	call   8001f1 <printnum>
  8006fd:	83 c4 20             	add    $0x20,%esp
			break;
  800700:	eb 39                	jmp    80073b <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  800702:	83 ec 08             	sub    $0x8,%esp
  800705:	ff 75 0c             	pushl  0xc(%ebp)
  800708:	53                   	push   %ebx
  800709:	8b 45 08             	mov    0x8(%ebp),%eax
  80070c:	ff d0                	call   *%eax
  80070e:	83 c4 10             	add    $0x10,%esp
			break;
  800711:	eb 28                	jmp    80073b <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  800713:	83 ec 08             	sub    $0x8,%esp
  800716:	ff 75 0c             	pushl  0xc(%ebp)
  800719:	6a 25                	push   $0x25
  80071b:	8b 45 08             	mov    0x8(%ebp),%eax
  80071e:	ff d0                	call   *%eax
  800720:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
  800723:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800727:	eb 04                	jmp    80072d <vprintfmt+0x3bd>
  800729:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80072d:	8b 45 10             	mov    0x10(%ebp),%eax
  800730:	83 e8 01             	sub    $0x1,%eax
  800733:	0f b6 00             	movzbl (%eax),%eax
  800736:	3c 25                	cmp    $0x25,%al
  800738:	75 ef                	jne    800729 <vprintfmt+0x3b9>
				/* do nothing */;
			break;
  80073a:	90                   	nop
		}
	}
  80073b:	e9 38 fc ff ff       	jmp    800378 <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
  800740:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  800741:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800744:	5b                   	pop    %ebx
  800745:	5e                   	pop    %esi
  800746:	5d                   	pop    %ebp
  800747:	c3                   	ret    

00800748 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800748:	55                   	push   %ebp
  800749:	89 e5                	mov    %esp,%ebp
  80074b:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  80074e:	8d 45 10             	lea    0x10(%ebp),%eax
  800751:	83 c0 04             	add    $0x4,%eax
  800754:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  800757:	8b 45 10             	mov    0x10(%ebp),%eax
  80075a:	ff 75 f4             	pushl  -0xc(%ebp)
  80075d:	50                   	push   %eax
  80075e:	ff 75 0c             	pushl  0xc(%ebp)
  800761:	ff 75 08             	pushl  0x8(%ebp)
  800764:	e8 07 fc ff ff       	call   800370 <vprintfmt>
  800769:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
  80076c:	90                   	nop
  80076d:	c9                   	leave  
  80076e:	c3                   	ret    

0080076f <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80076f:	55                   	push   %ebp
  800770:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800772:	8b 45 0c             	mov    0xc(%ebp),%eax
  800775:	8b 40 08             	mov    0x8(%eax),%eax
  800778:	8d 50 01             	lea    0x1(%eax),%edx
  80077b:	8b 45 0c             	mov    0xc(%ebp),%eax
  80077e:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800781:	8b 45 0c             	mov    0xc(%ebp),%eax
  800784:	8b 10                	mov    (%eax),%edx
  800786:	8b 45 0c             	mov    0xc(%ebp),%eax
  800789:	8b 40 04             	mov    0x4(%eax),%eax
  80078c:	39 c2                	cmp    %eax,%edx
  80078e:	73 12                	jae    8007a2 <sprintputch+0x33>
		*b->buf++ = ch;
  800790:	8b 45 0c             	mov    0xc(%ebp),%eax
  800793:	8b 00                	mov    (%eax),%eax
  800795:	8d 48 01             	lea    0x1(%eax),%ecx
  800798:	8b 55 0c             	mov    0xc(%ebp),%edx
  80079b:	89 0a                	mov    %ecx,(%edx)
  80079d:	8b 55 08             	mov    0x8(%ebp),%edx
  8007a0:	88 10                	mov    %dl,(%eax)
}
  8007a2:	90                   	nop
  8007a3:	5d                   	pop    %ebp
  8007a4:	c3                   	ret    

008007a5 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007a5:	55                   	push   %ebp
  8007a6:	89 e5                	mov    %esp,%ebp
  8007a8:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007ab:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ae:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007b1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007b4:	8d 50 ff             	lea    -0x1(%eax),%edx
  8007b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8007ba:	01 d0                	add    %edx,%eax
  8007bc:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007bf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007c6:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8007ca:	74 06                	je     8007d2 <vsnprintf+0x2d>
  8007cc:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8007d0:	7f 07                	jg     8007d9 <vsnprintf+0x34>
		return -E_INVAL;
  8007d2:	b8 03 00 00 00       	mov    $0x3,%eax
  8007d7:	eb 20                	jmp    8007f9 <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007d9:	ff 75 14             	pushl  0x14(%ebp)
  8007dc:	ff 75 10             	pushl  0x10(%ebp)
  8007df:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007e2:	50                   	push   %eax
  8007e3:	68 6f 07 80 00       	push   $0x80076f
  8007e8:	e8 83 fb ff ff       	call   800370 <vprintfmt>
  8007ed:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
  8007f0:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007f3:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007f6:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  8007f9:	c9                   	leave  
  8007fa:	c3                   	ret    

008007fb <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8007fb:	55                   	push   %ebp
  8007fc:	89 e5                	mov    %esp,%ebp
  8007fe:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800801:	8d 45 10             	lea    0x10(%ebp),%eax
  800804:	83 c0 04             	add    $0x4,%eax
  800807:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  80080a:	8b 45 10             	mov    0x10(%ebp),%eax
  80080d:	ff 75 f4             	pushl  -0xc(%ebp)
  800810:	50                   	push   %eax
  800811:	ff 75 0c             	pushl  0xc(%ebp)
  800814:	ff 75 08             	pushl  0x8(%ebp)
  800817:	e8 89 ff ff ff       	call   8007a5 <vsnprintf>
  80081c:	83 c4 10             	add    $0x10,%esp
  80081f:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  800822:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800825:	c9                   	leave  
  800826:	c3                   	ret    

00800827 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  800827:	55                   	push   %ebp
  800828:	89 e5                	mov    %esp,%ebp
  80082a:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  80082d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800834:	eb 08                	jmp    80083e <strlen+0x17>
		n++;
  800836:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80083a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80083e:	8b 45 08             	mov    0x8(%ebp),%eax
  800841:	0f b6 00             	movzbl (%eax),%eax
  800844:	84 c0                	test   %al,%al
  800846:	75 ee                	jne    800836 <strlen+0xf>
		n++;
	return n;
  800848:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80084b:	c9                   	leave  
  80084c:	c3                   	ret    

0080084d <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  80084d:	55                   	push   %ebp
  80084e:	89 e5                	mov    %esp,%ebp
  800850:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800853:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80085a:	eb 0c                	jmp    800868 <strnlen+0x1b>
		n++;
  80085c:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800860:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800864:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  800868:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80086c:	74 0a                	je     800878 <strnlen+0x2b>
  80086e:	8b 45 08             	mov    0x8(%ebp),%eax
  800871:	0f b6 00             	movzbl (%eax),%eax
  800874:	84 c0                	test   %al,%al
  800876:	75 e4                	jne    80085c <strnlen+0xf>
		n++;
	return n;
  800878:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80087b:	c9                   	leave  
  80087c:	c3                   	ret    

0080087d <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80087d:	55                   	push   %ebp
  80087e:	89 e5                	mov    %esp,%ebp
  800880:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800883:	8b 45 08             	mov    0x8(%ebp),%eax
  800886:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800889:	90                   	nop
  80088a:	8b 45 08             	mov    0x8(%ebp),%eax
  80088d:	8d 50 01             	lea    0x1(%eax),%edx
  800890:	89 55 08             	mov    %edx,0x8(%ebp)
  800893:	8b 55 0c             	mov    0xc(%ebp),%edx
  800896:	8d 4a 01             	lea    0x1(%edx),%ecx
  800899:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  80089c:	0f b6 12             	movzbl (%edx),%edx
  80089f:	88 10                	mov    %dl,(%eax)
  8008a1:	0f b6 00             	movzbl (%eax),%eax
  8008a4:	84 c0                	test   %al,%al
  8008a6:	75 e2                	jne    80088a <strcpy+0xd>
		/* do nothing */;
	return ret;
  8008a8:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8008ab:	c9                   	leave  
  8008ac:	c3                   	ret    

008008ad <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  8008ad:	55                   	push   %ebp
  8008ae:	89 e5                	mov    %esp,%ebp
  8008b0:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  8008b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8008b6:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  8008b9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008c0:	eb 23                	jmp    8008e5 <strncpy+0x38>
		*dst++ = *src;
  8008c2:	8b 45 08             	mov    0x8(%ebp),%eax
  8008c5:	8d 50 01             	lea    0x1(%eax),%edx
  8008c8:	89 55 08             	mov    %edx,0x8(%ebp)
  8008cb:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008ce:	0f b6 12             	movzbl (%edx),%edx
  8008d1:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  8008d3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008d6:	0f b6 00             	movzbl (%eax),%eax
  8008d9:	84 c0                	test   %al,%al
  8008db:	74 04                	je     8008e1 <strncpy+0x34>
			src++;
  8008dd:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008e1:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  8008e5:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8008e8:	3b 45 10             	cmp    0x10(%ebp),%eax
  8008eb:	72 d5                	jb     8008c2 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  8008ed:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  8008f0:	c9                   	leave  
  8008f1:	c3                   	ret    

008008f2 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  8008f2:	55                   	push   %ebp
  8008f3:	89 e5                	mov    %esp,%ebp
  8008f5:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  8008f8:	8b 45 08             	mov    0x8(%ebp),%eax
  8008fb:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  8008fe:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800902:	74 33                	je     800937 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800904:	eb 17                	jmp    80091d <strlcpy+0x2b>
			*dst++ = *src++;
  800906:	8b 45 08             	mov    0x8(%ebp),%eax
  800909:	8d 50 01             	lea    0x1(%eax),%edx
  80090c:	89 55 08             	mov    %edx,0x8(%ebp)
  80090f:	8b 55 0c             	mov    0xc(%ebp),%edx
  800912:	8d 4a 01             	lea    0x1(%edx),%ecx
  800915:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800918:	0f b6 12             	movzbl (%edx),%edx
  80091b:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  80091d:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800921:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800925:	74 0a                	je     800931 <strlcpy+0x3f>
  800927:	8b 45 0c             	mov    0xc(%ebp),%eax
  80092a:	0f b6 00             	movzbl (%eax),%eax
  80092d:	84 c0                	test   %al,%al
  80092f:	75 d5                	jne    800906 <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800931:	8b 45 08             	mov    0x8(%ebp),%eax
  800934:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  800937:	8b 55 08             	mov    0x8(%ebp),%edx
  80093a:	8b 45 fc             	mov    -0x4(%ebp),%eax
  80093d:	29 c2                	sub    %eax,%edx
  80093f:	89 d0                	mov    %edx,%eax
}
  800941:	c9                   	leave  
  800942:	c3                   	ret    

00800943 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800943:	55                   	push   %ebp
  800944:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800946:	eb 08                	jmp    800950 <strcmp+0xd>
		p++, q++;
  800948:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80094c:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800950:	8b 45 08             	mov    0x8(%ebp),%eax
  800953:	0f b6 00             	movzbl (%eax),%eax
  800956:	84 c0                	test   %al,%al
  800958:	74 10                	je     80096a <strcmp+0x27>
  80095a:	8b 45 08             	mov    0x8(%ebp),%eax
  80095d:	0f b6 10             	movzbl (%eax),%edx
  800960:	8b 45 0c             	mov    0xc(%ebp),%eax
  800963:	0f b6 00             	movzbl (%eax),%eax
  800966:	38 c2                	cmp    %al,%dl
  800968:	74 de                	je     800948 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80096a:	8b 45 08             	mov    0x8(%ebp),%eax
  80096d:	0f b6 00             	movzbl (%eax),%eax
  800970:	0f b6 d0             	movzbl %al,%edx
  800973:	8b 45 0c             	mov    0xc(%ebp),%eax
  800976:	0f b6 00             	movzbl (%eax),%eax
  800979:	0f b6 c0             	movzbl %al,%eax
  80097c:	29 c2                	sub    %eax,%edx
  80097e:	89 d0                	mov    %edx,%eax
}
  800980:	5d                   	pop    %ebp
  800981:	c3                   	ret    

00800982 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800982:	55                   	push   %ebp
  800983:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800985:	eb 0c                	jmp    800993 <strncmp+0x11>
		n--, p++, q++;
  800987:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80098b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80098f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800993:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800997:	74 1a                	je     8009b3 <strncmp+0x31>
  800999:	8b 45 08             	mov    0x8(%ebp),%eax
  80099c:	0f b6 00             	movzbl (%eax),%eax
  80099f:	84 c0                	test   %al,%al
  8009a1:	74 10                	je     8009b3 <strncmp+0x31>
  8009a3:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a6:	0f b6 10             	movzbl (%eax),%edx
  8009a9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009ac:	0f b6 00             	movzbl (%eax),%eax
  8009af:	38 c2                	cmp    %al,%dl
  8009b1:	74 d4                	je     800987 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  8009b3:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8009b7:	75 07                	jne    8009c0 <strncmp+0x3e>
		return 0;
  8009b9:	b8 00 00 00 00       	mov    $0x0,%eax
  8009be:	eb 16                	jmp    8009d6 <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8009c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c3:	0f b6 00             	movzbl (%eax),%eax
  8009c6:	0f b6 d0             	movzbl %al,%edx
  8009c9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009cc:	0f b6 00             	movzbl (%eax),%eax
  8009cf:	0f b6 c0             	movzbl %al,%eax
  8009d2:	29 c2                	sub    %eax,%edx
  8009d4:	89 d0                	mov    %edx,%eax
}
  8009d6:	5d                   	pop    %ebp
  8009d7:	c3                   	ret    

008009d8 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8009d8:	55                   	push   %ebp
  8009d9:	89 e5                	mov    %esp,%ebp
  8009db:	83 ec 04             	sub    $0x4,%esp
  8009de:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009e1:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  8009e4:	eb 14                	jmp    8009fa <strchr+0x22>
		if (*s == c)
  8009e6:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e9:	0f b6 00             	movzbl (%eax),%eax
  8009ec:	3a 45 fc             	cmp    -0x4(%ebp),%al
  8009ef:	75 05                	jne    8009f6 <strchr+0x1e>
			return (char *) s;
  8009f1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009f4:	eb 13                	jmp    800a09 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8009f6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009fa:	8b 45 08             	mov    0x8(%ebp),%eax
  8009fd:	0f b6 00             	movzbl (%eax),%eax
  800a00:	84 c0                	test   %al,%al
  800a02:	75 e2                	jne    8009e6 <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800a04:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800a09:	c9                   	leave  
  800a0a:	c3                   	ret    

00800a0b <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800a0b:	55                   	push   %ebp
  800a0c:	89 e5                	mov    %esp,%ebp
  800a0e:	83 ec 04             	sub    $0x4,%esp
  800a11:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a14:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800a17:	eb 0f                	jmp    800a28 <strfind+0x1d>
		if (*s == c)
  800a19:	8b 45 08             	mov    0x8(%ebp),%eax
  800a1c:	0f b6 00             	movzbl (%eax),%eax
  800a1f:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a22:	74 10                	je     800a34 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a24:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a28:	8b 45 08             	mov    0x8(%ebp),%eax
  800a2b:	0f b6 00             	movzbl (%eax),%eax
  800a2e:	84 c0                	test   %al,%al
  800a30:	75 e7                	jne    800a19 <strfind+0xe>
  800a32:	eb 01                	jmp    800a35 <strfind+0x2a>
		if (*s == c)
			break;
  800a34:	90                   	nop
	return (char *) s;
  800a35:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a38:	c9                   	leave  
  800a39:	c3                   	ret    

00800a3a <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800a3a:	55                   	push   %ebp
  800a3b:	89 e5                	mov    %esp,%ebp
  800a3d:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800a40:	8b 45 08             	mov    0x8(%ebp),%eax
  800a43:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800a46:	8b 45 10             	mov    0x10(%ebp),%eax
  800a49:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800a4c:	eb 0e                	jmp    800a5c <memset+0x22>
		*p++ = c;
  800a4e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a51:	8d 50 01             	lea    0x1(%eax),%edx
  800a54:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800a57:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a5a:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800a5c:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800a60:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800a64:	79 e8                	jns    800a4e <memset+0x14>
		*p++ = c;

	return v;
  800a66:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a69:	c9                   	leave  
  800a6a:	c3                   	ret    

00800a6b <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800a6b:	55                   	push   %ebp
  800a6c:	89 e5                	mov    %esp,%ebp
  800a6e:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800a71:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a74:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a77:	8b 45 08             	mov    0x8(%ebp),%eax
  800a7a:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800a7d:	eb 17                	jmp    800a96 <memcpy+0x2b>
		*d++ = *s++;
  800a7f:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800a82:	8d 50 01             	lea    0x1(%eax),%edx
  800a85:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800a88:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800a8b:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a8e:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800a91:	0f b6 12             	movzbl (%edx),%edx
  800a94:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800a96:	8b 45 10             	mov    0x10(%ebp),%eax
  800a99:	8d 50 ff             	lea    -0x1(%eax),%edx
  800a9c:	89 55 10             	mov    %edx,0x10(%ebp)
  800a9f:	85 c0                	test   %eax,%eax
  800aa1:	75 dc                	jne    800a7f <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800aa3:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800aa6:	c9                   	leave  
  800aa7:	c3                   	ret    

00800aa8 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800aa8:	55                   	push   %ebp
  800aa9:	89 e5                	mov    %esp,%ebp
  800aab:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800aae:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ab1:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800ab4:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab7:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800aba:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800abd:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800ac0:	73 54                	jae    800b16 <memmove+0x6e>
  800ac2:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800ac5:	8b 45 10             	mov    0x10(%ebp),%eax
  800ac8:	01 d0                	add    %edx,%eax
  800aca:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800acd:	76 47                	jbe    800b16 <memmove+0x6e>
		s += n;
  800acf:	8b 45 10             	mov    0x10(%ebp),%eax
  800ad2:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800ad5:	8b 45 10             	mov    0x10(%ebp),%eax
  800ad8:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800adb:	eb 13                	jmp    800af0 <memmove+0x48>
			*--d = *--s;
  800add:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800ae1:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800ae5:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800ae8:	0f b6 10             	movzbl (%eax),%edx
  800aeb:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800aee:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800af0:	8b 45 10             	mov    0x10(%ebp),%eax
  800af3:	8d 50 ff             	lea    -0x1(%eax),%edx
  800af6:	89 55 10             	mov    %edx,0x10(%ebp)
  800af9:	85 c0                	test   %eax,%eax
  800afb:	75 e0                	jne    800add <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800afd:	eb 24                	jmp    800b23 <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
  800aff:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b02:	8d 50 01             	lea    0x1(%eax),%edx
  800b05:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b08:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800b0b:	8d 4a 01             	lea    0x1(%edx),%ecx
  800b0e:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800b11:	0f b6 12             	movzbl (%edx),%edx
  800b14:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800b16:	8b 45 10             	mov    0x10(%ebp),%eax
  800b19:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b1c:	89 55 10             	mov    %edx,0x10(%ebp)
  800b1f:	85 c0                	test   %eax,%eax
  800b21:	75 dc                	jne    800aff <memmove+0x57>
			*d++ = *s++;

	return dst;
  800b23:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b26:	c9                   	leave  
  800b27:	c3                   	ret    

00800b28 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800b28:	55                   	push   %ebp
  800b29:	89 e5                	mov    %esp,%ebp
  800b2b:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800b2e:	8b 45 08             	mov    0x8(%ebp),%eax
  800b31:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800b34:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b37:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800b3a:	eb 30                	jmp    800b6c <memcmp+0x44>
		if (*s1 != *s2)
  800b3c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b3f:	0f b6 10             	movzbl (%eax),%edx
  800b42:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b45:	0f b6 00             	movzbl (%eax),%eax
  800b48:	38 c2                	cmp    %al,%dl
  800b4a:	74 18                	je     800b64 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800b4c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b4f:	0f b6 00             	movzbl (%eax),%eax
  800b52:	0f b6 d0             	movzbl %al,%edx
  800b55:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b58:	0f b6 00             	movzbl (%eax),%eax
  800b5b:	0f b6 c0             	movzbl %al,%eax
  800b5e:	29 c2                	sub    %eax,%edx
  800b60:	89 d0                	mov    %edx,%eax
  800b62:	eb 1a                	jmp    800b7e <memcmp+0x56>
		s1++, s2++;
  800b64:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800b68:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800b6c:	8b 45 10             	mov    0x10(%ebp),%eax
  800b6f:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b72:	89 55 10             	mov    %edx,0x10(%ebp)
  800b75:	85 c0                	test   %eax,%eax
  800b77:	75 c3                	jne    800b3c <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b79:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b7e:	c9                   	leave  
  800b7f:	c3                   	ret    

00800b80 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800b80:	55                   	push   %ebp
  800b81:	89 e5                	mov    %esp,%ebp
  800b83:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800b86:	8b 55 08             	mov    0x8(%ebp),%edx
  800b89:	8b 45 10             	mov    0x10(%ebp),%eax
  800b8c:	01 d0                	add    %edx,%eax
  800b8e:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800b91:	eb 17                	jmp    800baa <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b93:	8b 45 08             	mov    0x8(%ebp),%eax
  800b96:	0f b6 00             	movzbl (%eax),%eax
  800b99:	0f b6 d0             	movzbl %al,%edx
  800b9c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b9f:	0f b6 c0             	movzbl %al,%eax
  800ba2:	39 c2                	cmp    %eax,%edx
  800ba4:	74 0e                	je     800bb4 <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800ba6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800baa:	8b 45 08             	mov    0x8(%ebp),%eax
  800bad:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800bb0:	72 e1                	jb     800b93 <memfind+0x13>
  800bb2:	eb 01                	jmp    800bb5 <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
  800bb4:	90                   	nop
	return (void *) s;
  800bb5:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bb8:	c9                   	leave  
  800bb9:	c3                   	ret    

00800bba <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800bba:	55                   	push   %ebp
  800bbb:	89 e5                	mov    %esp,%ebp
  800bbd:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800bc0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800bc7:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bce:	eb 04                	jmp    800bd4 <strtol+0x1a>
		s++;
  800bd0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bd4:	8b 45 08             	mov    0x8(%ebp),%eax
  800bd7:	0f b6 00             	movzbl (%eax),%eax
  800bda:	3c 20                	cmp    $0x20,%al
  800bdc:	74 f2                	je     800bd0 <strtol+0x16>
  800bde:	8b 45 08             	mov    0x8(%ebp),%eax
  800be1:	0f b6 00             	movzbl (%eax),%eax
  800be4:	3c 09                	cmp    $0x9,%al
  800be6:	74 e8                	je     800bd0 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800be8:	8b 45 08             	mov    0x8(%ebp),%eax
  800beb:	0f b6 00             	movzbl (%eax),%eax
  800bee:	3c 2b                	cmp    $0x2b,%al
  800bf0:	75 06                	jne    800bf8 <strtol+0x3e>
		s++;
  800bf2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bf6:	eb 15                	jmp    800c0d <strtol+0x53>
	else if (*s == '-')
  800bf8:	8b 45 08             	mov    0x8(%ebp),%eax
  800bfb:	0f b6 00             	movzbl (%eax),%eax
  800bfe:	3c 2d                	cmp    $0x2d,%al
  800c00:	75 0b                	jne    800c0d <strtol+0x53>
		s++, neg = 1;
  800c02:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c06:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800c0d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c11:	74 06                	je     800c19 <strtol+0x5f>
  800c13:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800c17:	75 24                	jne    800c3d <strtol+0x83>
  800c19:	8b 45 08             	mov    0x8(%ebp),%eax
  800c1c:	0f b6 00             	movzbl (%eax),%eax
  800c1f:	3c 30                	cmp    $0x30,%al
  800c21:	75 1a                	jne    800c3d <strtol+0x83>
  800c23:	8b 45 08             	mov    0x8(%ebp),%eax
  800c26:	83 c0 01             	add    $0x1,%eax
  800c29:	0f b6 00             	movzbl (%eax),%eax
  800c2c:	3c 78                	cmp    $0x78,%al
  800c2e:	75 0d                	jne    800c3d <strtol+0x83>
		s += 2, base = 16;
  800c30:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800c34:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c3b:	eb 2a                	jmp    800c67 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800c3d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c41:	75 17                	jne    800c5a <strtol+0xa0>
  800c43:	8b 45 08             	mov    0x8(%ebp),%eax
  800c46:	0f b6 00             	movzbl (%eax),%eax
  800c49:	3c 30                	cmp    $0x30,%al
  800c4b:	75 0d                	jne    800c5a <strtol+0xa0>
		s++, base = 8;
  800c4d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c51:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c58:	eb 0d                	jmp    800c67 <strtol+0xad>
	else if (base == 0)
  800c5a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c5e:	75 07                	jne    800c67 <strtol+0xad>
		base = 10;
  800c60:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c67:	8b 45 08             	mov    0x8(%ebp),%eax
  800c6a:	0f b6 00             	movzbl (%eax),%eax
  800c6d:	3c 2f                	cmp    $0x2f,%al
  800c6f:	7e 1b                	jle    800c8c <strtol+0xd2>
  800c71:	8b 45 08             	mov    0x8(%ebp),%eax
  800c74:	0f b6 00             	movzbl (%eax),%eax
  800c77:	3c 39                	cmp    $0x39,%al
  800c79:	7f 11                	jg     800c8c <strtol+0xd2>
			dig = *s - '0';
  800c7b:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7e:	0f b6 00             	movzbl (%eax),%eax
  800c81:	0f be c0             	movsbl %al,%eax
  800c84:	83 e8 30             	sub    $0x30,%eax
  800c87:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800c8a:	eb 48                	jmp    800cd4 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800c8c:	8b 45 08             	mov    0x8(%ebp),%eax
  800c8f:	0f b6 00             	movzbl (%eax),%eax
  800c92:	3c 60                	cmp    $0x60,%al
  800c94:	7e 1b                	jle    800cb1 <strtol+0xf7>
  800c96:	8b 45 08             	mov    0x8(%ebp),%eax
  800c99:	0f b6 00             	movzbl (%eax),%eax
  800c9c:	3c 7a                	cmp    $0x7a,%al
  800c9e:	7f 11                	jg     800cb1 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800ca0:	8b 45 08             	mov    0x8(%ebp),%eax
  800ca3:	0f b6 00             	movzbl (%eax),%eax
  800ca6:	0f be c0             	movsbl %al,%eax
  800ca9:	83 e8 57             	sub    $0x57,%eax
  800cac:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800caf:	eb 23                	jmp    800cd4 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800cb1:	8b 45 08             	mov    0x8(%ebp),%eax
  800cb4:	0f b6 00             	movzbl (%eax),%eax
  800cb7:	3c 40                	cmp    $0x40,%al
  800cb9:	7e 3c                	jle    800cf7 <strtol+0x13d>
  800cbb:	8b 45 08             	mov    0x8(%ebp),%eax
  800cbe:	0f b6 00             	movzbl (%eax),%eax
  800cc1:	3c 5a                	cmp    $0x5a,%al
  800cc3:	7f 32                	jg     800cf7 <strtol+0x13d>
			dig = *s - 'A' + 10;
  800cc5:	8b 45 08             	mov    0x8(%ebp),%eax
  800cc8:	0f b6 00             	movzbl (%eax),%eax
  800ccb:	0f be c0             	movsbl %al,%eax
  800cce:	83 e8 37             	sub    $0x37,%eax
  800cd1:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800cd4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800cd7:	3b 45 10             	cmp    0x10(%ebp),%eax
  800cda:	7d 1a                	jge    800cf6 <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
  800cdc:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ce0:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ce3:	0f af 45 10          	imul   0x10(%ebp),%eax
  800ce7:	89 c2                	mov    %eax,%edx
  800ce9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800cec:	01 d0                	add    %edx,%eax
  800cee:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800cf1:	e9 71 ff ff ff       	jmp    800c67 <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
  800cf6:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
  800cf7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cfb:	74 08                	je     800d05 <strtol+0x14b>
		*endptr = (char *) s;
  800cfd:	8b 45 0c             	mov    0xc(%ebp),%eax
  800d00:	8b 55 08             	mov    0x8(%ebp),%edx
  800d03:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800d05:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800d09:	74 07                	je     800d12 <strtol+0x158>
  800d0b:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800d0e:	f7 d8                	neg    %eax
  800d10:	eb 03                	jmp    800d15 <strtol+0x15b>
  800d12:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800d15:	c9                   	leave  
  800d16:	c3                   	ret    

00800d17 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800d17:	55                   	push   %ebp
  800d18:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800d1a:	8b 45 14             	mov    0x14(%ebp),%eax
  800d1d:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800d23:	8b 45 14             	mov    0x14(%ebp),%eax
  800d26:	8b 00                	mov    (%eax),%eax
  800d28:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d2f:	8b 45 10             	mov    0x10(%ebp),%eax
  800d32:	01 d0                	add    %edx,%eax
  800d34:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d3a:	eb 0c                	jmp    800d48 <strsplit+0x31>
			*string++ = 0;
  800d3c:	8b 45 08             	mov    0x8(%ebp),%eax
  800d3f:	8d 50 01             	lea    0x1(%eax),%edx
  800d42:	89 55 08             	mov    %edx,0x8(%ebp)
  800d45:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d48:	8b 45 08             	mov    0x8(%ebp),%eax
  800d4b:	0f b6 00             	movzbl (%eax),%eax
  800d4e:	84 c0                	test   %al,%al
  800d50:	74 19                	je     800d6b <strsplit+0x54>
  800d52:	8b 45 08             	mov    0x8(%ebp),%eax
  800d55:	0f b6 00             	movzbl (%eax),%eax
  800d58:	0f be c0             	movsbl %al,%eax
  800d5b:	50                   	push   %eax
  800d5c:	ff 75 0c             	pushl  0xc(%ebp)
  800d5f:	e8 74 fc ff ff       	call   8009d8 <strchr>
  800d64:	83 c4 08             	add    $0x8,%esp
  800d67:	85 c0                	test   %eax,%eax
  800d69:	75 d1                	jne    800d3c <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800d6b:	8b 45 08             	mov    0x8(%ebp),%eax
  800d6e:	0f b6 00             	movzbl (%eax),%eax
  800d71:	84 c0                	test   %al,%al
  800d73:	74 5d                	je     800dd2 <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800d75:	8b 45 14             	mov    0x14(%ebp),%eax
  800d78:	8b 00                	mov    (%eax),%eax
  800d7a:	83 f8 0f             	cmp    $0xf,%eax
  800d7d:	75 07                	jne    800d86 <strsplit+0x6f>
		{
			return 0;
  800d7f:	b8 00 00 00 00       	mov    $0x0,%eax
  800d84:	eb 69                	jmp    800def <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800d86:	8b 45 14             	mov    0x14(%ebp),%eax
  800d89:	8b 00                	mov    (%eax),%eax
  800d8b:	8d 48 01             	lea    0x1(%eax),%ecx
  800d8e:	8b 55 14             	mov    0x14(%ebp),%edx
  800d91:	89 0a                	mov    %ecx,(%edx)
  800d93:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d9a:	8b 45 10             	mov    0x10(%ebp),%eax
  800d9d:	01 c2                	add    %eax,%edx
  800d9f:	8b 45 08             	mov    0x8(%ebp),%eax
  800da2:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800da4:	eb 04                	jmp    800daa <strsplit+0x93>
			string++;
  800da6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800daa:	8b 45 08             	mov    0x8(%ebp),%eax
  800dad:	0f b6 00             	movzbl (%eax),%eax
  800db0:	84 c0                	test   %al,%al
  800db2:	74 86                	je     800d3a <strsplit+0x23>
  800db4:	8b 45 08             	mov    0x8(%ebp),%eax
  800db7:	0f b6 00             	movzbl (%eax),%eax
  800dba:	0f be c0             	movsbl %al,%eax
  800dbd:	50                   	push   %eax
  800dbe:	ff 75 0c             	pushl  0xc(%ebp)
  800dc1:	e8 12 fc ff ff       	call   8009d8 <strchr>
  800dc6:	83 c4 08             	add    $0x8,%esp
  800dc9:	85 c0                	test   %eax,%eax
  800dcb:	74 d9                	je     800da6 <strsplit+0x8f>
			string++;
	}
  800dcd:	e9 68 ff ff ff       	jmp    800d3a <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
  800dd2:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800dd3:	8b 45 14             	mov    0x14(%ebp),%eax
  800dd6:	8b 00                	mov    (%eax),%eax
  800dd8:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ddf:	8b 45 10             	mov    0x10(%ebp),%eax
  800de2:	01 d0                	add    %edx,%eax
  800de4:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800dea:	b8 01 00 00 00       	mov    $0x1,%eax
}
  800def:	c9                   	leave  
  800df0:	c3                   	ret    

00800df1 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800df1:	55                   	push   %ebp
  800df2:	89 e5                	mov    %esp,%ebp
  800df4:	57                   	push   %edi
  800df5:	56                   	push   %esi
  800df6:	53                   	push   %ebx
  800df7:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800dfa:	8b 45 08             	mov    0x8(%ebp),%eax
  800dfd:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e00:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800e03:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800e06:	8b 7d 18             	mov    0x18(%ebp),%edi
  800e09:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800e0c:	cd 30                	int    $0x30
  800e0e:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800e11:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800e14:	83 c4 10             	add    $0x10,%esp
  800e17:	5b                   	pop    %ebx
  800e18:	5e                   	pop    %esi
  800e19:	5f                   	pop    %edi
  800e1a:	5d                   	pop    %ebp
  800e1b:	c3                   	ret    

00800e1c <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800e1c:	55                   	push   %ebp
  800e1d:	89 e5                	mov    %esp,%ebp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800e1f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e22:	6a 00                	push   $0x0
  800e24:	6a 00                	push   $0x0
  800e26:	6a 00                	push   $0x0
  800e28:	ff 75 0c             	pushl  0xc(%ebp)
  800e2b:	50                   	push   %eax
  800e2c:	6a 00                	push   $0x0
  800e2e:	e8 be ff ff ff       	call   800df1 <syscall>
  800e33:	83 c4 18             	add    $0x18,%esp
}
  800e36:	90                   	nop
  800e37:	c9                   	leave  
  800e38:	c3                   	ret    

00800e39 <sys_cgetc>:

int
sys_cgetc(void)
{
  800e39:	55                   	push   %ebp
  800e3a:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800e3c:	6a 00                	push   $0x0
  800e3e:	6a 00                	push   $0x0
  800e40:	6a 00                	push   $0x0
  800e42:	6a 00                	push   $0x0
  800e44:	6a 00                	push   $0x0
  800e46:	6a 01                	push   $0x1
  800e48:	e8 a4 ff ff ff       	call   800df1 <syscall>
  800e4d:	83 c4 18             	add    $0x18,%esp
}
  800e50:	c9                   	leave  
  800e51:	c3                   	ret    

00800e52 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800e52:	55                   	push   %ebp
  800e53:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800e55:	8b 45 08             	mov    0x8(%ebp),%eax
  800e58:	6a 00                	push   $0x0
  800e5a:	6a 00                	push   $0x0
  800e5c:	6a 00                	push   $0x0
  800e5e:	6a 00                	push   $0x0
  800e60:	50                   	push   %eax
  800e61:	6a 03                	push   $0x3
  800e63:	e8 89 ff ff ff       	call   800df1 <syscall>
  800e68:	83 c4 18             	add    $0x18,%esp
}
  800e6b:	c9                   	leave  
  800e6c:	c3                   	ret    

00800e6d <sys_getenvid>:

int32 sys_getenvid(void)
{
  800e6d:	55                   	push   %ebp
  800e6e:	89 e5                	mov    %esp,%ebp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800e70:	6a 00                	push   $0x0
  800e72:	6a 00                	push   $0x0
  800e74:	6a 00                	push   $0x0
  800e76:	6a 00                	push   $0x0
  800e78:	6a 00                	push   $0x0
  800e7a:	6a 02                	push   $0x2
  800e7c:	e8 70 ff ff ff       	call   800df1 <syscall>
  800e81:	83 c4 18             	add    $0x18,%esp
}
  800e84:	c9                   	leave  
  800e85:	c3                   	ret    

00800e86 <sys_env_sleep>:

void sys_env_sleep(void)
{
  800e86:	55                   	push   %ebp
  800e87:	89 e5                	mov    %esp,%ebp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800e89:	6a 00                	push   $0x0
  800e8b:	6a 00                	push   $0x0
  800e8d:	6a 00                	push   $0x0
  800e8f:	6a 00                	push   $0x0
  800e91:	6a 00                	push   $0x0
  800e93:	6a 04                	push   $0x4
  800e95:	e8 57 ff ff ff       	call   800df1 <syscall>
  800e9a:	83 c4 18             	add    $0x18,%esp
}
  800e9d:	90                   	nop
  800e9e:	c9                   	leave  
  800e9f:	c3                   	ret    

00800ea0 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800ea0:	55                   	push   %ebp
  800ea1:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800ea3:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ea6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ea9:	6a 00                	push   $0x0
  800eab:	6a 00                	push   $0x0
  800ead:	6a 00                	push   $0x0
  800eaf:	52                   	push   %edx
  800eb0:	50                   	push   %eax
  800eb1:	6a 05                	push   $0x5
  800eb3:	e8 39 ff ff ff       	call   800df1 <syscall>
  800eb8:	83 c4 18             	add    $0x18,%esp
}
  800ebb:	c9                   	leave  
  800ebc:	c3                   	ret    

00800ebd <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  800ebd:	55                   	push   %ebp
  800ebe:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  800ec0:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ec3:	8b 45 08             	mov    0x8(%ebp),%eax
  800ec6:	6a 00                	push   $0x0
  800ec8:	6a 00                	push   $0x0
  800eca:	6a 00                	push   $0x0
  800ecc:	52                   	push   %edx
  800ecd:	50                   	push   %eax
  800ece:	6a 06                	push   $0x6
  800ed0:	e8 1c ff ff ff       	call   800df1 <syscall>
  800ed5:	83 c4 18             	add    $0x18,%esp
}
  800ed8:	c9                   	leave  
  800ed9:	c3                   	ret    

00800eda <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  800eda:	55                   	push   %ebp
  800edb:	89 e5                	mov    %esp,%ebp
  800edd:	56                   	push   %esi
  800ede:	53                   	push   %ebx
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  800edf:	8b 75 18             	mov    0x18(%ebp),%esi
  800ee2:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800ee5:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800ee8:	8b 55 0c             	mov    0xc(%ebp),%edx
  800eeb:	8b 45 08             	mov    0x8(%ebp),%eax
  800eee:	56                   	push   %esi
  800eef:	53                   	push   %ebx
  800ef0:	51                   	push   %ecx
  800ef1:	52                   	push   %edx
  800ef2:	50                   	push   %eax
  800ef3:	6a 07                	push   $0x7
  800ef5:	e8 f7 fe ff ff       	call   800df1 <syscall>
  800efa:	83 c4 18             	add    $0x18,%esp
}
  800efd:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800f00:	5b                   	pop    %ebx
  800f01:	5e                   	pop    %esi
  800f02:	5d                   	pop    %ebp
  800f03:	c3                   	ret    

00800f04 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  800f04:	55                   	push   %ebp
  800f05:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  800f07:	8b 55 0c             	mov    0xc(%ebp),%edx
  800f0a:	8b 45 08             	mov    0x8(%ebp),%eax
  800f0d:	6a 00                	push   $0x0
  800f0f:	6a 00                	push   $0x0
  800f11:	6a 00                	push   $0x0
  800f13:	52                   	push   %edx
  800f14:	50                   	push   %eax
  800f15:	6a 08                	push   $0x8
  800f17:	e8 d5 fe ff ff       	call   800df1 <syscall>
  800f1c:	83 c4 18             	add    $0x18,%esp
}
  800f1f:	c9                   	leave  
  800f20:	c3                   	ret    

00800f21 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  800f21:	55                   	push   %ebp
  800f22:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  800f24:	6a 00                	push   $0x0
  800f26:	6a 00                	push   $0x0
  800f28:	6a 00                	push   $0x0
  800f2a:	ff 75 0c             	pushl  0xc(%ebp)
  800f2d:	ff 75 08             	pushl  0x8(%ebp)
  800f30:	6a 09                	push   $0x9
  800f32:	e8 ba fe ff ff       	call   800df1 <syscall>
  800f37:	83 c4 18             	add    $0x18,%esp
}
  800f3a:	c9                   	leave  
  800f3b:	c3                   	ret    

00800f3c <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  800f3c:	55                   	push   %ebp
  800f3d:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  800f3f:	6a 00                	push   $0x0
  800f41:	6a 00                	push   $0x0
  800f43:	6a 00                	push   $0x0
  800f45:	6a 00                	push   $0x0
  800f47:	6a 00                	push   $0x0
  800f49:	6a 0a                	push   $0xa
  800f4b:	e8 a1 fe ff ff       	call   800df1 <syscall>
  800f50:	83 c4 18             	add    $0x18,%esp
}
  800f53:	c9                   	leave  
  800f54:	c3                   	ret    

00800f55 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  800f55:	55                   	push   %ebp
  800f56:	89 e5                	mov    %esp,%ebp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  800f58:	8b 45 08             	mov    0x8(%ebp),%eax
  800f5b:	6a 00                	push   $0x0
  800f5d:	6a 00                	push   $0x0
  800f5f:	6a 00                	push   $0x0
  800f61:	ff 75 0c             	pushl  0xc(%ebp)
  800f64:	50                   	push   %eax
  800f65:	6a 0b                	push   $0xb
  800f67:	e8 85 fe ff ff       	call   800df1 <syscall>
  800f6c:	83 c4 18             	add    $0x18,%esp
	return;
  800f6f:	90                   	nop
}
  800f70:	c9                   	leave  
  800f71:	c3                   	ret    
  800f72:	66 90                	xchg   %ax,%ax
  800f74:	66 90                	xchg   %ax,%ax
  800f76:	66 90                	xchg   %ax,%ax
  800f78:	66 90                	xchg   %ax,%ax
  800f7a:	66 90                	xchg   %ax,%ax
  800f7c:	66 90                	xchg   %ax,%ax
  800f7e:	66 90                	xchg   %ax,%ax

00800f80 <__udivdi3>:
  800f80:	55                   	push   %ebp
  800f81:	57                   	push   %edi
  800f82:	56                   	push   %esi
  800f83:	53                   	push   %ebx
  800f84:	83 ec 1c             	sub    $0x1c,%esp
  800f87:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800f8b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800f8f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800f93:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800f97:	85 f6                	test   %esi,%esi
  800f99:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800f9d:	89 ca                	mov    %ecx,%edx
  800f9f:	89 f8                	mov    %edi,%eax
  800fa1:	75 3d                	jne    800fe0 <__udivdi3+0x60>
  800fa3:	39 cf                	cmp    %ecx,%edi
  800fa5:	0f 87 c5 00 00 00    	ja     801070 <__udivdi3+0xf0>
  800fab:	85 ff                	test   %edi,%edi
  800fad:	89 fd                	mov    %edi,%ebp
  800faf:	75 0b                	jne    800fbc <__udivdi3+0x3c>
  800fb1:	b8 01 00 00 00       	mov    $0x1,%eax
  800fb6:	31 d2                	xor    %edx,%edx
  800fb8:	f7 f7                	div    %edi
  800fba:	89 c5                	mov    %eax,%ebp
  800fbc:	89 c8                	mov    %ecx,%eax
  800fbe:	31 d2                	xor    %edx,%edx
  800fc0:	f7 f5                	div    %ebp
  800fc2:	89 c1                	mov    %eax,%ecx
  800fc4:	89 d8                	mov    %ebx,%eax
  800fc6:	89 cf                	mov    %ecx,%edi
  800fc8:	f7 f5                	div    %ebp
  800fca:	89 c3                	mov    %eax,%ebx
  800fcc:	89 d8                	mov    %ebx,%eax
  800fce:	89 fa                	mov    %edi,%edx
  800fd0:	83 c4 1c             	add    $0x1c,%esp
  800fd3:	5b                   	pop    %ebx
  800fd4:	5e                   	pop    %esi
  800fd5:	5f                   	pop    %edi
  800fd6:	5d                   	pop    %ebp
  800fd7:	c3                   	ret    
  800fd8:	90                   	nop
  800fd9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  800fe0:	39 ce                	cmp    %ecx,%esi
  800fe2:	77 74                	ja     801058 <__udivdi3+0xd8>
  800fe4:	0f bd fe             	bsr    %esi,%edi
  800fe7:	83 f7 1f             	xor    $0x1f,%edi
  800fea:	0f 84 98 00 00 00    	je     801088 <__udivdi3+0x108>
  800ff0:	bb 20 00 00 00       	mov    $0x20,%ebx
  800ff5:	89 f9                	mov    %edi,%ecx
  800ff7:	89 c5                	mov    %eax,%ebp
  800ff9:	29 fb                	sub    %edi,%ebx
  800ffb:	d3 e6                	shl    %cl,%esi
  800ffd:	89 d9                	mov    %ebx,%ecx
  800fff:	d3 ed                	shr    %cl,%ebp
  801001:	89 f9                	mov    %edi,%ecx
  801003:	d3 e0                	shl    %cl,%eax
  801005:	09 ee                	or     %ebp,%esi
  801007:	89 d9                	mov    %ebx,%ecx
  801009:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80100d:	89 d5                	mov    %edx,%ebp
  80100f:	8b 44 24 08          	mov    0x8(%esp),%eax
  801013:	d3 ed                	shr    %cl,%ebp
  801015:	89 f9                	mov    %edi,%ecx
  801017:	d3 e2                	shl    %cl,%edx
  801019:	89 d9                	mov    %ebx,%ecx
  80101b:	d3 e8                	shr    %cl,%eax
  80101d:	09 c2                	or     %eax,%edx
  80101f:	89 d0                	mov    %edx,%eax
  801021:	89 ea                	mov    %ebp,%edx
  801023:	f7 f6                	div    %esi
  801025:	89 d5                	mov    %edx,%ebp
  801027:	89 c3                	mov    %eax,%ebx
  801029:	f7 64 24 0c          	mull   0xc(%esp)
  80102d:	39 d5                	cmp    %edx,%ebp
  80102f:	72 10                	jb     801041 <__udivdi3+0xc1>
  801031:	8b 74 24 08          	mov    0x8(%esp),%esi
  801035:	89 f9                	mov    %edi,%ecx
  801037:	d3 e6                	shl    %cl,%esi
  801039:	39 c6                	cmp    %eax,%esi
  80103b:	73 07                	jae    801044 <__udivdi3+0xc4>
  80103d:	39 d5                	cmp    %edx,%ebp
  80103f:	75 03                	jne    801044 <__udivdi3+0xc4>
  801041:	83 eb 01             	sub    $0x1,%ebx
  801044:	31 ff                	xor    %edi,%edi
  801046:	89 d8                	mov    %ebx,%eax
  801048:	89 fa                	mov    %edi,%edx
  80104a:	83 c4 1c             	add    $0x1c,%esp
  80104d:	5b                   	pop    %ebx
  80104e:	5e                   	pop    %esi
  80104f:	5f                   	pop    %edi
  801050:	5d                   	pop    %ebp
  801051:	c3                   	ret    
  801052:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801058:	31 ff                	xor    %edi,%edi
  80105a:	31 db                	xor    %ebx,%ebx
  80105c:	89 d8                	mov    %ebx,%eax
  80105e:	89 fa                	mov    %edi,%edx
  801060:	83 c4 1c             	add    $0x1c,%esp
  801063:	5b                   	pop    %ebx
  801064:	5e                   	pop    %esi
  801065:	5f                   	pop    %edi
  801066:	5d                   	pop    %ebp
  801067:	c3                   	ret    
  801068:	90                   	nop
  801069:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801070:	89 d8                	mov    %ebx,%eax
  801072:	f7 f7                	div    %edi
  801074:	31 ff                	xor    %edi,%edi
  801076:	89 c3                	mov    %eax,%ebx
  801078:	89 d8                	mov    %ebx,%eax
  80107a:	89 fa                	mov    %edi,%edx
  80107c:	83 c4 1c             	add    $0x1c,%esp
  80107f:	5b                   	pop    %ebx
  801080:	5e                   	pop    %esi
  801081:	5f                   	pop    %edi
  801082:	5d                   	pop    %ebp
  801083:	c3                   	ret    
  801084:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801088:	39 ce                	cmp    %ecx,%esi
  80108a:	72 0c                	jb     801098 <__udivdi3+0x118>
  80108c:	31 db                	xor    %ebx,%ebx
  80108e:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801092:	0f 87 34 ff ff ff    	ja     800fcc <__udivdi3+0x4c>
  801098:	bb 01 00 00 00       	mov    $0x1,%ebx
  80109d:	e9 2a ff ff ff       	jmp    800fcc <__udivdi3+0x4c>
  8010a2:	66 90                	xchg   %ax,%ax
  8010a4:	66 90                	xchg   %ax,%ax
  8010a6:	66 90                	xchg   %ax,%ax
  8010a8:	66 90                	xchg   %ax,%ax
  8010aa:	66 90                	xchg   %ax,%ax
  8010ac:	66 90                	xchg   %ax,%ax
  8010ae:	66 90                	xchg   %ax,%ax

008010b0 <__umoddi3>:
  8010b0:	55                   	push   %ebp
  8010b1:	57                   	push   %edi
  8010b2:	56                   	push   %esi
  8010b3:	53                   	push   %ebx
  8010b4:	83 ec 1c             	sub    $0x1c,%esp
  8010b7:	8b 54 24 3c          	mov    0x3c(%esp),%edx
  8010bb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8010bf:	8b 74 24 34          	mov    0x34(%esp),%esi
  8010c3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8010c7:	85 d2                	test   %edx,%edx
  8010c9:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8010cd:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8010d1:	89 f3                	mov    %esi,%ebx
  8010d3:	89 3c 24             	mov    %edi,(%esp)
  8010d6:	89 74 24 04          	mov    %esi,0x4(%esp)
  8010da:	75 1c                	jne    8010f8 <__umoddi3+0x48>
  8010dc:	39 f7                	cmp    %esi,%edi
  8010de:	76 50                	jbe    801130 <__umoddi3+0x80>
  8010e0:	89 c8                	mov    %ecx,%eax
  8010e2:	89 f2                	mov    %esi,%edx
  8010e4:	f7 f7                	div    %edi
  8010e6:	89 d0                	mov    %edx,%eax
  8010e8:	31 d2                	xor    %edx,%edx
  8010ea:	83 c4 1c             	add    $0x1c,%esp
  8010ed:	5b                   	pop    %ebx
  8010ee:	5e                   	pop    %esi
  8010ef:	5f                   	pop    %edi
  8010f0:	5d                   	pop    %ebp
  8010f1:	c3                   	ret    
  8010f2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  8010f8:	39 f2                	cmp    %esi,%edx
  8010fa:	89 d0                	mov    %edx,%eax
  8010fc:	77 52                	ja     801150 <__umoddi3+0xa0>
  8010fe:	0f bd ea             	bsr    %edx,%ebp
  801101:	83 f5 1f             	xor    $0x1f,%ebp
  801104:	75 5a                	jne    801160 <__umoddi3+0xb0>
  801106:	3b 54 24 04          	cmp    0x4(%esp),%edx
  80110a:	0f 82 e0 00 00 00    	jb     8011f0 <__umoddi3+0x140>
  801110:	39 0c 24             	cmp    %ecx,(%esp)
  801113:	0f 86 d7 00 00 00    	jbe    8011f0 <__umoddi3+0x140>
  801119:	8b 44 24 08          	mov    0x8(%esp),%eax
  80111d:	8b 54 24 04          	mov    0x4(%esp),%edx
  801121:	83 c4 1c             	add    $0x1c,%esp
  801124:	5b                   	pop    %ebx
  801125:	5e                   	pop    %esi
  801126:	5f                   	pop    %edi
  801127:	5d                   	pop    %ebp
  801128:	c3                   	ret    
  801129:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801130:	85 ff                	test   %edi,%edi
  801132:	89 fd                	mov    %edi,%ebp
  801134:	75 0b                	jne    801141 <__umoddi3+0x91>
  801136:	b8 01 00 00 00       	mov    $0x1,%eax
  80113b:	31 d2                	xor    %edx,%edx
  80113d:	f7 f7                	div    %edi
  80113f:	89 c5                	mov    %eax,%ebp
  801141:	89 f0                	mov    %esi,%eax
  801143:	31 d2                	xor    %edx,%edx
  801145:	f7 f5                	div    %ebp
  801147:	89 c8                	mov    %ecx,%eax
  801149:	f7 f5                	div    %ebp
  80114b:	89 d0                	mov    %edx,%eax
  80114d:	eb 99                	jmp    8010e8 <__umoddi3+0x38>
  80114f:	90                   	nop
  801150:	89 c8                	mov    %ecx,%eax
  801152:	89 f2                	mov    %esi,%edx
  801154:	83 c4 1c             	add    $0x1c,%esp
  801157:	5b                   	pop    %ebx
  801158:	5e                   	pop    %esi
  801159:	5f                   	pop    %edi
  80115a:	5d                   	pop    %ebp
  80115b:	c3                   	ret    
  80115c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801160:	8b 34 24             	mov    (%esp),%esi
  801163:	bf 20 00 00 00       	mov    $0x20,%edi
  801168:	89 e9                	mov    %ebp,%ecx
  80116a:	29 ef                	sub    %ebp,%edi
  80116c:	d3 e0                	shl    %cl,%eax
  80116e:	89 f9                	mov    %edi,%ecx
  801170:	89 f2                	mov    %esi,%edx
  801172:	d3 ea                	shr    %cl,%edx
  801174:	89 e9                	mov    %ebp,%ecx
  801176:	09 c2                	or     %eax,%edx
  801178:	89 d8                	mov    %ebx,%eax
  80117a:	89 14 24             	mov    %edx,(%esp)
  80117d:	89 f2                	mov    %esi,%edx
  80117f:	d3 e2                	shl    %cl,%edx
  801181:	89 f9                	mov    %edi,%ecx
  801183:	89 54 24 04          	mov    %edx,0x4(%esp)
  801187:	8b 54 24 0c          	mov    0xc(%esp),%edx
  80118b:	d3 e8                	shr    %cl,%eax
  80118d:	89 e9                	mov    %ebp,%ecx
  80118f:	89 c6                	mov    %eax,%esi
  801191:	d3 e3                	shl    %cl,%ebx
  801193:	89 f9                	mov    %edi,%ecx
  801195:	89 d0                	mov    %edx,%eax
  801197:	d3 e8                	shr    %cl,%eax
  801199:	89 e9                	mov    %ebp,%ecx
  80119b:	09 d8                	or     %ebx,%eax
  80119d:	89 d3                	mov    %edx,%ebx
  80119f:	89 f2                	mov    %esi,%edx
  8011a1:	f7 34 24             	divl   (%esp)
  8011a4:	89 d6                	mov    %edx,%esi
  8011a6:	d3 e3                	shl    %cl,%ebx
  8011a8:	f7 64 24 04          	mull   0x4(%esp)
  8011ac:	39 d6                	cmp    %edx,%esi
  8011ae:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8011b2:	89 d1                	mov    %edx,%ecx
  8011b4:	89 c3                	mov    %eax,%ebx
  8011b6:	72 08                	jb     8011c0 <__umoddi3+0x110>
  8011b8:	75 11                	jne    8011cb <__umoddi3+0x11b>
  8011ba:	39 44 24 08          	cmp    %eax,0x8(%esp)
  8011be:	73 0b                	jae    8011cb <__umoddi3+0x11b>
  8011c0:	2b 44 24 04          	sub    0x4(%esp),%eax
  8011c4:	1b 14 24             	sbb    (%esp),%edx
  8011c7:	89 d1                	mov    %edx,%ecx
  8011c9:	89 c3                	mov    %eax,%ebx
  8011cb:	8b 54 24 08          	mov    0x8(%esp),%edx
  8011cf:	29 da                	sub    %ebx,%edx
  8011d1:	19 ce                	sbb    %ecx,%esi
  8011d3:	89 f9                	mov    %edi,%ecx
  8011d5:	89 f0                	mov    %esi,%eax
  8011d7:	d3 e0                	shl    %cl,%eax
  8011d9:	89 e9                	mov    %ebp,%ecx
  8011db:	d3 ea                	shr    %cl,%edx
  8011dd:	89 e9                	mov    %ebp,%ecx
  8011df:	d3 ee                	shr    %cl,%esi
  8011e1:	09 d0                	or     %edx,%eax
  8011e3:	89 f2                	mov    %esi,%edx
  8011e5:	83 c4 1c             	add    $0x1c,%esp
  8011e8:	5b                   	pop    %ebx
  8011e9:	5e                   	pop    %esi
  8011ea:	5f                   	pop    %edi
  8011eb:	5d                   	pop    %ebp
  8011ec:	c3                   	ret    
  8011ed:	8d 76 00             	lea    0x0(%esi),%esi
  8011f0:	29 f9                	sub    %edi,%ecx
  8011f2:	19 d6                	sbb    %edx,%esi
  8011f4:	89 74 24 04          	mov    %esi,0x4(%esp)
  8011f8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8011fc:	e9 18 ff ff ff       	jmp    801119 <__umoddi3+0x69>
