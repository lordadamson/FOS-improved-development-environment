
obj/user/fos_input:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 95 00 00 00       	call   8000cb <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:

#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	81 ec 18 02 00 00    	sub    $0x218,%esp
	int i1=0;
  800041:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i2=0;
  800048:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
	char buff1[256];
	char buff2[256];	
	
	readline("Please enter first number :", buff1);	
  80004f:	83 ec 08             	sub    $0x8,%esp
  800052:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800058:	50                   	push   %eax
  800059:	68 60 13 80 00       	push   $0x801360
  80005e:	e8 dc 07 00 00       	call   80083f <readline>
  800063:	83 c4 10             	add    $0x10,%esp
	i1 = strtol(buff1, NULL, 10);
  800066:	83 ec 04             	sub    $0x4,%esp
  800069:	6a 0a                	push   $0xa
  80006b:	6a 00                	push   $0x0
  80006d:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800073:	50                   	push   %eax
  800074:	e8 58 0c 00 00       	call   800cd1 <strtol>
  800079:	83 c4 10             	add    $0x10,%esp
  80007c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	readline("Please enter second number :", buff2);
  80007f:	83 ec 08             	sub    $0x8,%esp
  800082:	8d 85 f0 fd ff ff    	lea    -0x210(%ebp),%eax
  800088:	50                   	push   %eax
  800089:	68 7c 13 80 00       	push   $0x80137c
  80008e:	e8 ac 07 00 00       	call   80083f <readline>
  800093:	83 c4 10             	add    $0x10,%esp
	
	i2 = strtol(buff2, NULL, 10);
  800096:	83 ec 04             	sub    $0x4,%esp
  800099:	6a 0a                	push   $0xa
  80009b:	6a 00                	push   $0x0
  80009d:	8d 85 f0 fd ff ff    	lea    -0x210(%ebp),%eax
  8000a3:	50                   	push   %eax
  8000a4:	e8 28 0c 00 00       	call   800cd1 <strtol>
  8000a9:	83 c4 10             	add    $0x10,%esp
  8000ac:	89 45 f0             	mov    %eax,-0x10(%ebp)

	cprintf("number 1 + number 2 = %d\n",i1+i2);
  8000af:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8000b2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8000b5:	01 d0                	add    %edx,%eax
  8000b7:	83 ec 08             	sub    $0x8,%esp
  8000ba:	50                   	push   %eax
  8000bb:	68 99 13 80 00       	push   $0x801399
  8000c0:	e8 1e 01 00 00       	call   8001e3 <cprintf>
  8000c5:	83 c4 10             	add    $0x10,%esp
	return;	
  8000c8:	90                   	nop
}
  8000c9:	c9                   	leave  
  8000ca:	c3                   	ret    

008000cb <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  8000cb:	55                   	push   %ebp
  8000cc:	89 e5                	mov    %esp,%ebp
  8000ce:	83 ec 08             	sub    $0x8,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  8000d1:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000d8:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000db:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000df:	7e 0a                	jle    8000eb <libmain+0x20>
		binaryname = argv[0];
  8000e1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000e4:	8b 00                	mov    (%eax),%eax
  8000e6:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000eb:	83 ec 08             	sub    $0x8,%esp
  8000ee:	ff 75 0c             	pushl  0xc(%ebp)
  8000f1:	ff 75 08             	pushl  0x8(%ebp)
  8000f4:	e8 3f ff ff ff       	call   800038 <_main>
  8000f9:	83 c4 10             	add    $0x10,%esp

	// exit gracefully
	//exit();
	sleep();
  8000fc:	e8 19 00 00 00       	call   80011a <sleep>
}
  800101:	90                   	nop
  800102:	c9                   	leave  
  800103:	c3                   	ret    

00800104 <exit>:

#include <inc/lib.h>

void
exit(void)
{
  800104:	55                   	push   %ebp
  800105:	89 e5                	mov    %esp,%ebp
  800107:	83 ec 08             	sub    $0x8,%esp
	sys_env_destroy(0);	
  80010a:	83 ec 0c             	sub    $0xc,%esp
  80010d:	6a 00                	push   $0x0
  80010f:	e8 55 0e 00 00       	call   800f69 <sys_env_destroy>
  800114:	83 c4 10             	add    $0x10,%esp
}
  800117:	90                   	nop
  800118:	c9                   	leave  
  800119:	c3                   	ret    

0080011a <sleep>:

void
sleep(void)
{	
  80011a:	55                   	push   %ebp
  80011b:	89 e5                	mov    %esp,%ebp
  80011d:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  800120:	e8 78 0e 00 00       	call   800f9d <sys_env_sleep>
}
  800125:	90                   	nop
  800126:	c9                   	leave  
  800127:	c3                   	ret    

00800128 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  800128:	55                   	push   %ebp
  800129:	89 e5                	mov    %esp,%ebp
  80012b:	83 ec 08             	sub    $0x8,%esp
	b->buf[b->idx++] = ch;
  80012e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800131:	8b 00                	mov    (%eax),%eax
  800133:	8d 48 01             	lea    0x1(%eax),%ecx
  800136:	8b 55 0c             	mov    0xc(%ebp),%edx
  800139:	89 0a                	mov    %ecx,(%edx)
  80013b:	8b 55 08             	mov    0x8(%ebp),%edx
  80013e:	89 d1                	mov    %edx,%ecx
  800140:	8b 55 0c             	mov    0xc(%ebp),%edx
  800143:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800147:	8b 45 0c             	mov    0xc(%ebp),%eax
  80014a:	8b 00                	mov    (%eax),%eax
  80014c:	3d ff 00 00 00       	cmp    $0xff,%eax
  800151:	75 23                	jne    800176 <putch+0x4e>
		sys_cputs(b->buf, b->idx);
  800153:	8b 45 0c             	mov    0xc(%ebp),%eax
  800156:	8b 00                	mov    (%eax),%eax
  800158:	89 c2                	mov    %eax,%edx
  80015a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80015d:	83 c0 08             	add    $0x8,%eax
  800160:	83 ec 08             	sub    $0x8,%esp
  800163:	52                   	push   %edx
  800164:	50                   	push   %eax
  800165:	e8 c9 0d 00 00       	call   800f33 <sys_cputs>
  80016a:	83 c4 10             	add    $0x10,%esp
		b->idx = 0;
  80016d:	8b 45 0c             	mov    0xc(%ebp),%eax
  800170:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  800176:	8b 45 0c             	mov    0xc(%ebp),%eax
  800179:	8b 40 04             	mov    0x4(%eax),%eax
  80017c:	8d 50 01             	lea    0x1(%eax),%edx
  80017f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800182:	89 50 04             	mov    %edx,0x4(%eax)
}
  800185:	90                   	nop
  800186:	c9                   	leave  
  800187:	c3                   	ret    

00800188 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800188:	55                   	push   %ebp
  800189:	89 e5                	mov    %esp,%ebp
  80018b:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800191:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800198:	00 00 00 
	b.cnt = 0;
  80019b:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  8001a2:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  8001a5:	ff 75 0c             	pushl  0xc(%ebp)
  8001a8:	ff 75 08             	pushl  0x8(%ebp)
  8001ab:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001b1:	50                   	push   %eax
  8001b2:	68 28 01 80 00       	push   $0x800128
  8001b7:	e8 cc 01 00 00       	call   800388 <vprintfmt>
  8001bc:	83 c4 10             	add    $0x10,%esp
	sys_cputs(b.buf, b.idx);
  8001bf:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  8001c5:	83 ec 08             	sub    $0x8,%esp
  8001c8:	50                   	push   %eax
  8001c9:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  8001cf:	83 c0 08             	add    $0x8,%eax
  8001d2:	50                   	push   %eax
  8001d3:	e8 5b 0d 00 00       	call   800f33 <sys_cputs>
  8001d8:	83 c4 10             	add    $0x10,%esp

	return b.cnt;
  8001db:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001e1:	c9                   	leave  
  8001e2:	c3                   	ret    

008001e3 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001e3:	55                   	push   %ebp
  8001e4:	89 e5                	mov    %esp,%ebp
  8001e6:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001e9:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001ec:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001ef:	8b 45 08             	mov    0x8(%ebp),%eax
  8001f2:	83 ec 08             	sub    $0x8,%esp
  8001f5:	ff 75 f4             	pushl  -0xc(%ebp)
  8001f8:	50                   	push   %eax
  8001f9:	e8 8a ff ff ff       	call   800188 <vcprintf>
  8001fe:	83 c4 10             	add    $0x10,%esp
  800201:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  800204:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800207:	c9                   	leave  
  800208:	c3                   	ret    

00800209 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  800209:	55                   	push   %ebp
  80020a:	89 e5                	mov    %esp,%ebp
  80020c:	53                   	push   %ebx
  80020d:	83 ec 14             	sub    $0x14,%esp
  800210:	8b 45 10             	mov    0x10(%ebp),%eax
  800213:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800216:	8b 45 14             	mov    0x14(%ebp),%eax
  800219:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  80021c:	8b 45 18             	mov    0x18(%ebp),%eax
  80021f:	ba 00 00 00 00       	mov    $0x0,%edx
  800224:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  800227:	77 55                	ja     80027e <printnum+0x75>
  800229:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  80022c:	72 05                	jb     800233 <printnum+0x2a>
  80022e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  800231:	77 4b                	ja     80027e <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  800233:	8b 45 1c             	mov    0x1c(%ebp),%eax
  800236:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800239:	8b 45 18             	mov    0x18(%ebp),%eax
  80023c:	ba 00 00 00 00       	mov    $0x0,%edx
  800241:	52                   	push   %edx
  800242:	50                   	push   %eax
  800243:	ff 75 f4             	pushl  -0xc(%ebp)
  800246:	ff 75 f0             	pushl  -0x10(%ebp)
  800249:	e8 72 0e 00 00       	call   8010c0 <__udivdi3>
  80024e:	83 c4 10             	add    $0x10,%esp
  800251:	83 ec 04             	sub    $0x4,%esp
  800254:	ff 75 20             	pushl  0x20(%ebp)
  800257:	53                   	push   %ebx
  800258:	ff 75 18             	pushl  0x18(%ebp)
  80025b:	52                   	push   %edx
  80025c:	50                   	push   %eax
  80025d:	ff 75 0c             	pushl  0xc(%ebp)
  800260:	ff 75 08             	pushl  0x8(%ebp)
  800263:	e8 a1 ff ff ff       	call   800209 <printnum>
  800268:	83 c4 20             	add    $0x20,%esp
  80026b:	eb 1b                	jmp    800288 <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  80026d:	83 ec 08             	sub    $0x8,%esp
  800270:	ff 75 0c             	pushl  0xc(%ebp)
  800273:	ff 75 20             	pushl  0x20(%ebp)
  800276:	8b 45 08             	mov    0x8(%ebp),%eax
  800279:	ff d0                	call   *%eax
  80027b:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  80027e:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  800282:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  800286:	7f e5                	jg     80026d <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800288:	8b 4d 18             	mov    0x18(%ebp),%ecx
  80028b:	bb 00 00 00 00       	mov    $0x0,%ebx
  800290:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800293:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800296:	53                   	push   %ebx
  800297:	51                   	push   %ecx
  800298:	52                   	push   %edx
  800299:	50                   	push   %eax
  80029a:	e8 51 0f 00 00       	call   8011f0 <__umoddi3>
  80029f:	83 c4 10             	add    $0x10,%esp
  8002a2:	05 80 14 80 00       	add    $0x801480,%eax
  8002a7:	0f b6 00             	movzbl (%eax),%eax
  8002aa:	0f be c0             	movsbl %al,%eax
  8002ad:	83 ec 08             	sub    $0x8,%esp
  8002b0:	ff 75 0c             	pushl  0xc(%ebp)
  8002b3:	50                   	push   %eax
  8002b4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002b7:	ff d0                	call   *%eax
  8002b9:	83 c4 10             	add    $0x10,%esp
}
  8002bc:	90                   	nop
  8002bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  8002c0:	c9                   	leave  
  8002c1:	c3                   	ret    

008002c2 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  8002c2:	55                   	push   %ebp
  8002c3:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002c5:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002c9:	7e 1c                	jle    8002e7 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  8002cb:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ce:	8b 00                	mov    (%eax),%eax
  8002d0:	8d 50 08             	lea    0x8(%eax),%edx
  8002d3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d6:	89 10                	mov    %edx,(%eax)
  8002d8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002db:	8b 00                	mov    (%eax),%eax
  8002dd:	83 e8 08             	sub    $0x8,%eax
  8002e0:	8b 50 04             	mov    0x4(%eax),%edx
  8002e3:	8b 00                	mov    (%eax),%eax
  8002e5:	eb 40                	jmp    800327 <getuint+0x65>
	else if (lflag)
  8002e7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002eb:	74 1e                	je     80030b <getuint+0x49>
		return va_arg(*ap, unsigned long);
  8002ed:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f0:	8b 00                	mov    (%eax),%eax
  8002f2:	8d 50 04             	lea    0x4(%eax),%edx
  8002f5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f8:	89 10                	mov    %edx,(%eax)
  8002fa:	8b 45 08             	mov    0x8(%ebp),%eax
  8002fd:	8b 00                	mov    (%eax),%eax
  8002ff:	83 e8 04             	sub    $0x4,%eax
  800302:	8b 00                	mov    (%eax),%eax
  800304:	ba 00 00 00 00       	mov    $0x0,%edx
  800309:	eb 1c                	jmp    800327 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  80030b:	8b 45 08             	mov    0x8(%ebp),%eax
  80030e:	8b 00                	mov    (%eax),%eax
  800310:	8d 50 04             	lea    0x4(%eax),%edx
  800313:	8b 45 08             	mov    0x8(%ebp),%eax
  800316:	89 10                	mov    %edx,(%eax)
  800318:	8b 45 08             	mov    0x8(%ebp),%eax
  80031b:	8b 00                	mov    (%eax),%eax
  80031d:	83 e8 04             	sub    $0x4,%eax
  800320:	8b 00                	mov    (%eax),%eax
  800322:	ba 00 00 00 00       	mov    $0x0,%edx
}
  800327:	5d                   	pop    %ebp
  800328:	c3                   	ret    

00800329 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  800329:	55                   	push   %ebp
  80032a:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80032c:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800330:	7e 1c                	jle    80034e <getint+0x25>
		return va_arg(*ap, long long);
  800332:	8b 45 08             	mov    0x8(%ebp),%eax
  800335:	8b 00                	mov    (%eax),%eax
  800337:	8d 50 08             	lea    0x8(%eax),%edx
  80033a:	8b 45 08             	mov    0x8(%ebp),%eax
  80033d:	89 10                	mov    %edx,(%eax)
  80033f:	8b 45 08             	mov    0x8(%ebp),%eax
  800342:	8b 00                	mov    (%eax),%eax
  800344:	83 e8 08             	sub    $0x8,%eax
  800347:	8b 50 04             	mov    0x4(%eax),%edx
  80034a:	8b 00                	mov    (%eax),%eax
  80034c:	eb 38                	jmp    800386 <getint+0x5d>
	else if (lflag)
  80034e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800352:	74 1a                	je     80036e <getint+0x45>
		return va_arg(*ap, long);
  800354:	8b 45 08             	mov    0x8(%ebp),%eax
  800357:	8b 00                	mov    (%eax),%eax
  800359:	8d 50 04             	lea    0x4(%eax),%edx
  80035c:	8b 45 08             	mov    0x8(%ebp),%eax
  80035f:	89 10                	mov    %edx,(%eax)
  800361:	8b 45 08             	mov    0x8(%ebp),%eax
  800364:	8b 00                	mov    (%eax),%eax
  800366:	83 e8 04             	sub    $0x4,%eax
  800369:	8b 00                	mov    (%eax),%eax
  80036b:	99                   	cltd   
  80036c:	eb 18                	jmp    800386 <getint+0x5d>
	else
		return va_arg(*ap, int);
  80036e:	8b 45 08             	mov    0x8(%ebp),%eax
  800371:	8b 00                	mov    (%eax),%eax
  800373:	8d 50 04             	lea    0x4(%eax),%edx
  800376:	8b 45 08             	mov    0x8(%ebp),%eax
  800379:	89 10                	mov    %edx,(%eax)
  80037b:	8b 45 08             	mov    0x8(%ebp),%eax
  80037e:	8b 00                	mov    (%eax),%eax
  800380:	83 e8 04             	sub    $0x4,%eax
  800383:	8b 00                	mov    (%eax),%eax
  800385:	99                   	cltd   
}
  800386:	5d                   	pop    %ebp
  800387:	c3                   	ret    

00800388 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800388:	55                   	push   %ebp
  800389:	89 e5                	mov    %esp,%ebp
  80038b:	56                   	push   %esi
  80038c:	53                   	push   %ebx
  80038d:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800390:	eb 17                	jmp    8003a9 <vprintfmt+0x21>
			if (ch == '\0')
  800392:	85 db                	test   %ebx,%ebx
  800394:	0f 84 be 03 00 00    	je     800758 <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
  80039a:	83 ec 08             	sub    $0x8,%esp
  80039d:	ff 75 0c             	pushl  0xc(%ebp)
  8003a0:	53                   	push   %ebx
  8003a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8003a4:	ff d0                	call   *%eax
  8003a6:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  8003a9:	8b 45 10             	mov    0x10(%ebp),%eax
  8003ac:	8d 50 01             	lea    0x1(%eax),%edx
  8003af:	89 55 10             	mov    %edx,0x10(%ebp)
  8003b2:	0f b6 00             	movzbl (%eax),%eax
  8003b5:	0f b6 d8             	movzbl %al,%ebx
  8003b8:	83 fb 25             	cmp    $0x25,%ebx
  8003bb:	75 d5                	jne    800392 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  8003bd:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  8003c1:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  8003c8:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  8003cf:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  8003d6:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003dd:	8b 45 10             	mov    0x10(%ebp),%eax
  8003e0:	8d 50 01             	lea    0x1(%eax),%edx
  8003e3:	89 55 10             	mov    %edx,0x10(%ebp)
  8003e6:	0f b6 00             	movzbl (%eax),%eax
  8003e9:	0f b6 d8             	movzbl %al,%ebx
  8003ec:	8d 43 dd             	lea    -0x23(%ebx),%eax
  8003ef:	83 f8 55             	cmp    $0x55,%eax
  8003f2:	0f 87 33 03 00 00    	ja     80072b <vprintfmt+0x3a3>
  8003f8:	8b 04 85 a4 14 80 00 	mov    0x8014a4(,%eax,4),%eax
  8003ff:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  800401:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  800405:	eb d6                	jmp    8003dd <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  800407:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  80040b:	eb d0                	jmp    8003dd <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80040d:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  800414:	8b 55 e0             	mov    -0x20(%ebp),%edx
  800417:	89 d0                	mov    %edx,%eax
  800419:	c1 e0 02             	shl    $0x2,%eax
  80041c:	01 d0                	add    %edx,%eax
  80041e:	01 c0                	add    %eax,%eax
  800420:	01 d8                	add    %ebx,%eax
  800422:	83 e8 30             	sub    $0x30,%eax
  800425:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  800428:	8b 45 10             	mov    0x10(%ebp),%eax
  80042b:	0f b6 00             	movzbl (%eax),%eax
  80042e:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  800431:	83 fb 2f             	cmp    $0x2f,%ebx
  800434:	7e 3f                	jle    800475 <vprintfmt+0xed>
  800436:	83 fb 39             	cmp    $0x39,%ebx
  800439:	7f 3a                	jg     800475 <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  80043b:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80043f:	eb d3                	jmp    800414 <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  800441:	8b 45 14             	mov    0x14(%ebp),%eax
  800444:	83 c0 04             	add    $0x4,%eax
  800447:	89 45 14             	mov    %eax,0x14(%ebp)
  80044a:	8b 45 14             	mov    0x14(%ebp),%eax
  80044d:	83 e8 04             	sub    $0x4,%eax
  800450:	8b 00                	mov    (%eax),%eax
  800452:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800455:	eb 1f                	jmp    800476 <vprintfmt+0xee>

		case '.':
			if (width < 0)
  800457:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80045b:	79 80                	jns    8003dd <vprintfmt+0x55>
				width = 0;
  80045d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  800464:	e9 74 ff ff ff       	jmp    8003dd <vprintfmt+0x55>

		case '#':
			altflag = 1;
  800469:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  800470:	e9 68 ff ff ff       	jmp    8003dd <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
  800475:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
  800476:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80047a:	0f 89 5d ff ff ff    	jns    8003dd <vprintfmt+0x55>
				width = precision, precision = -1;
  800480:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800483:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800486:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  80048d:	e9 4b ff ff ff       	jmp    8003dd <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800492:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  800496:	e9 42 ff ff ff       	jmp    8003dd <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  80049b:	8b 45 14             	mov    0x14(%ebp),%eax
  80049e:	83 c0 04             	add    $0x4,%eax
  8004a1:	89 45 14             	mov    %eax,0x14(%ebp)
  8004a4:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a7:	83 e8 04             	sub    $0x4,%eax
  8004aa:	8b 00                	mov    (%eax),%eax
  8004ac:	83 ec 08             	sub    $0x8,%esp
  8004af:	ff 75 0c             	pushl  0xc(%ebp)
  8004b2:	50                   	push   %eax
  8004b3:	8b 45 08             	mov    0x8(%ebp),%eax
  8004b6:	ff d0                	call   *%eax
  8004b8:	83 c4 10             	add    $0x10,%esp
			break;
  8004bb:	e9 93 02 00 00       	jmp    800753 <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
  8004c0:	8b 45 14             	mov    0x14(%ebp),%eax
  8004c3:	83 c0 04             	add    $0x4,%eax
  8004c6:	89 45 14             	mov    %eax,0x14(%ebp)
  8004c9:	8b 45 14             	mov    0x14(%ebp),%eax
  8004cc:	83 e8 04             	sub    $0x4,%eax
  8004cf:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  8004d1:	85 db                	test   %ebx,%ebx
  8004d3:	79 02                	jns    8004d7 <vprintfmt+0x14f>
				err = -err;
  8004d5:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  8004d7:	83 fb 07             	cmp    $0x7,%ebx
  8004da:	7f 0b                	jg     8004e7 <vprintfmt+0x15f>
  8004dc:	8b 34 9d 60 14 80 00 	mov    0x801460(,%ebx,4),%esi
  8004e3:	85 f6                	test   %esi,%esi
  8004e5:	75 19                	jne    800500 <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
  8004e7:	53                   	push   %ebx
  8004e8:	68 91 14 80 00       	push   $0x801491
  8004ed:	ff 75 0c             	pushl  0xc(%ebp)
  8004f0:	ff 75 08             	pushl  0x8(%ebp)
  8004f3:	e8 68 02 00 00       	call   800760 <printfmt>
  8004f8:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
  8004fb:	e9 53 02 00 00       	jmp    800753 <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  800500:	56                   	push   %esi
  800501:	68 9a 14 80 00       	push   $0x80149a
  800506:	ff 75 0c             	pushl  0xc(%ebp)
  800509:	ff 75 08             	pushl  0x8(%ebp)
  80050c:	e8 4f 02 00 00       	call   800760 <printfmt>
  800511:	83 c4 10             	add    $0x10,%esp
			break;
  800514:	e9 3a 02 00 00       	jmp    800753 <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  800519:	8b 45 14             	mov    0x14(%ebp),%eax
  80051c:	83 c0 04             	add    $0x4,%eax
  80051f:	89 45 14             	mov    %eax,0x14(%ebp)
  800522:	8b 45 14             	mov    0x14(%ebp),%eax
  800525:	83 e8 04             	sub    $0x4,%eax
  800528:	8b 30                	mov    (%eax),%esi
  80052a:	85 f6                	test   %esi,%esi
  80052c:	75 05                	jne    800533 <vprintfmt+0x1ab>
				p = "(null)";
  80052e:	be 9d 14 80 00       	mov    $0x80149d,%esi
			if (width > 0 && padc != '-')
  800533:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800537:	7e 6f                	jle    8005a8 <vprintfmt+0x220>
  800539:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  80053d:	74 69                	je     8005a8 <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
  80053f:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800542:	83 ec 08             	sub    $0x8,%esp
  800545:	50                   	push   %eax
  800546:	56                   	push   %esi
  800547:	e8 18 04 00 00       	call   800964 <strnlen>
  80054c:	83 c4 10             	add    $0x10,%esp
  80054f:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  800552:	eb 17                	jmp    80056b <vprintfmt+0x1e3>
					putch(padc, putdat);
  800554:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  800558:	83 ec 08             	sub    $0x8,%esp
  80055b:	ff 75 0c             	pushl  0xc(%ebp)
  80055e:	50                   	push   %eax
  80055f:	8b 45 08             	mov    0x8(%ebp),%eax
  800562:	ff d0                	call   *%eax
  800564:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800567:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80056b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80056f:	7f e3                	jg     800554 <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  800571:	eb 35                	jmp    8005a8 <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
  800573:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800577:	74 1c                	je     800595 <vprintfmt+0x20d>
  800579:	83 fb 1f             	cmp    $0x1f,%ebx
  80057c:	7e 05                	jle    800583 <vprintfmt+0x1fb>
  80057e:	83 fb 7e             	cmp    $0x7e,%ebx
  800581:	7e 12                	jle    800595 <vprintfmt+0x20d>
					putch('?', putdat);
  800583:	83 ec 08             	sub    $0x8,%esp
  800586:	ff 75 0c             	pushl  0xc(%ebp)
  800589:	6a 3f                	push   $0x3f
  80058b:	8b 45 08             	mov    0x8(%ebp),%eax
  80058e:	ff d0                	call   *%eax
  800590:	83 c4 10             	add    $0x10,%esp
  800593:	eb 0f                	jmp    8005a4 <vprintfmt+0x21c>
				else
					putch(ch, putdat);
  800595:	83 ec 08             	sub    $0x8,%esp
  800598:	ff 75 0c             	pushl  0xc(%ebp)
  80059b:	53                   	push   %ebx
  80059c:	8b 45 08             	mov    0x8(%ebp),%eax
  80059f:	ff d0                	call   *%eax
  8005a1:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8005a4:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005a8:	89 f0                	mov    %esi,%eax
  8005aa:	8d 70 01             	lea    0x1(%eax),%esi
  8005ad:	0f b6 00             	movzbl (%eax),%eax
  8005b0:	0f be d8             	movsbl %al,%ebx
  8005b3:	85 db                	test   %ebx,%ebx
  8005b5:	74 26                	je     8005dd <vprintfmt+0x255>
  8005b7:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005bb:	78 b6                	js     800573 <vprintfmt+0x1eb>
  8005bd:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  8005c1:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  8005c5:	79 ac                	jns    800573 <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005c7:	eb 14                	jmp    8005dd <vprintfmt+0x255>
				putch(' ', putdat);
  8005c9:	83 ec 08             	sub    $0x8,%esp
  8005cc:	ff 75 0c             	pushl  0xc(%ebp)
  8005cf:	6a 20                	push   $0x20
  8005d1:	8b 45 08             	mov    0x8(%ebp),%eax
  8005d4:	ff d0                	call   *%eax
  8005d6:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005d9:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005dd:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005e1:	7f e6                	jg     8005c9 <vprintfmt+0x241>
				putch(' ', putdat);
			break;
  8005e3:	e9 6b 01 00 00       	jmp    800753 <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005e8:	83 ec 08             	sub    $0x8,%esp
  8005eb:	ff 75 e8             	pushl  -0x18(%ebp)
  8005ee:	8d 45 14             	lea    0x14(%ebp),%eax
  8005f1:	50                   	push   %eax
  8005f2:	e8 32 fd ff ff       	call   800329 <getint>
  8005f7:	83 c4 10             	add    $0x10,%esp
  8005fa:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005fd:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  800600:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800603:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800606:	85 d2                	test   %edx,%edx
  800608:	79 23                	jns    80062d <vprintfmt+0x2a5>
				putch('-', putdat);
  80060a:	83 ec 08             	sub    $0x8,%esp
  80060d:	ff 75 0c             	pushl  0xc(%ebp)
  800610:	6a 2d                	push   $0x2d
  800612:	8b 45 08             	mov    0x8(%ebp),%eax
  800615:	ff d0                	call   *%eax
  800617:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
  80061a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80061d:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800620:	f7 d8                	neg    %eax
  800622:	83 d2 00             	adc    $0x0,%edx
  800625:	f7 da                	neg    %edx
  800627:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80062a:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  80062d:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800634:	e9 bc 00 00 00       	jmp    8006f5 <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800639:	83 ec 08             	sub    $0x8,%esp
  80063c:	ff 75 e8             	pushl  -0x18(%ebp)
  80063f:	8d 45 14             	lea    0x14(%ebp),%eax
  800642:	50                   	push   %eax
  800643:	e8 7a fc ff ff       	call   8002c2 <getuint>
  800648:	83 c4 10             	add    $0x10,%esp
  80064b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80064e:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  800651:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800658:	e9 98 00 00 00       	jmp    8006f5 <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  80065d:	83 ec 08             	sub    $0x8,%esp
  800660:	ff 75 0c             	pushl  0xc(%ebp)
  800663:	6a 58                	push   $0x58
  800665:	8b 45 08             	mov    0x8(%ebp),%eax
  800668:	ff d0                	call   *%eax
  80066a:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  80066d:	83 ec 08             	sub    $0x8,%esp
  800670:	ff 75 0c             	pushl  0xc(%ebp)
  800673:	6a 58                	push   $0x58
  800675:	8b 45 08             	mov    0x8(%ebp),%eax
  800678:	ff d0                	call   *%eax
  80067a:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  80067d:	83 ec 08             	sub    $0x8,%esp
  800680:	ff 75 0c             	pushl  0xc(%ebp)
  800683:	6a 58                	push   $0x58
  800685:	8b 45 08             	mov    0x8(%ebp),%eax
  800688:	ff d0                	call   *%eax
  80068a:	83 c4 10             	add    $0x10,%esp
			break;
  80068d:	e9 c1 00 00 00       	jmp    800753 <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
  800692:	83 ec 08             	sub    $0x8,%esp
  800695:	ff 75 0c             	pushl  0xc(%ebp)
  800698:	6a 30                	push   $0x30
  80069a:	8b 45 08             	mov    0x8(%ebp),%eax
  80069d:	ff d0                	call   *%eax
  80069f:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
  8006a2:	83 ec 08             	sub    $0x8,%esp
  8006a5:	ff 75 0c             	pushl  0xc(%ebp)
  8006a8:	6a 78                	push   $0x78
  8006aa:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ad:	ff d0                	call   *%eax
  8006af:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  8006b2:	8b 45 14             	mov    0x14(%ebp),%eax
  8006b5:	83 c0 04             	add    $0x4,%eax
  8006b8:	89 45 14             	mov    %eax,0x14(%ebp)
  8006bb:	8b 45 14             	mov    0x14(%ebp),%eax
  8006be:	83 e8 04             	sub    $0x4,%eax
  8006c1:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  8006c3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006c6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  8006cd:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  8006d4:	eb 1f                	jmp    8006f5 <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006d6:	83 ec 08             	sub    $0x8,%esp
  8006d9:	ff 75 e8             	pushl  -0x18(%ebp)
  8006dc:	8d 45 14             	lea    0x14(%ebp),%eax
  8006df:	50                   	push   %eax
  8006e0:	e8 dd fb ff ff       	call   8002c2 <getuint>
  8006e5:	83 c4 10             	add    $0x10,%esp
  8006e8:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006eb:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  8006ee:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006f5:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  8006f9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006fc:	83 ec 04             	sub    $0x4,%esp
  8006ff:	52                   	push   %edx
  800700:	ff 75 e4             	pushl  -0x1c(%ebp)
  800703:	50                   	push   %eax
  800704:	ff 75 f4             	pushl  -0xc(%ebp)
  800707:	ff 75 f0             	pushl  -0x10(%ebp)
  80070a:	ff 75 0c             	pushl  0xc(%ebp)
  80070d:	ff 75 08             	pushl  0x8(%ebp)
  800710:	e8 f4 fa ff ff       	call   800209 <printnum>
  800715:	83 c4 20             	add    $0x20,%esp
			break;
  800718:	eb 39                	jmp    800753 <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  80071a:	83 ec 08             	sub    $0x8,%esp
  80071d:	ff 75 0c             	pushl  0xc(%ebp)
  800720:	53                   	push   %ebx
  800721:	8b 45 08             	mov    0x8(%ebp),%eax
  800724:	ff d0                	call   *%eax
  800726:	83 c4 10             	add    $0x10,%esp
			break;
  800729:	eb 28                	jmp    800753 <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  80072b:	83 ec 08             	sub    $0x8,%esp
  80072e:	ff 75 0c             	pushl  0xc(%ebp)
  800731:	6a 25                	push   $0x25
  800733:	8b 45 08             	mov    0x8(%ebp),%eax
  800736:	ff d0                	call   *%eax
  800738:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
  80073b:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80073f:	eb 04                	jmp    800745 <vprintfmt+0x3bd>
  800741:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800745:	8b 45 10             	mov    0x10(%ebp),%eax
  800748:	83 e8 01             	sub    $0x1,%eax
  80074b:	0f b6 00             	movzbl (%eax),%eax
  80074e:	3c 25                	cmp    $0x25,%al
  800750:	75 ef                	jne    800741 <vprintfmt+0x3b9>
				/* do nothing */;
			break;
  800752:	90                   	nop
		}
	}
  800753:	e9 38 fc ff ff       	jmp    800390 <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
  800758:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  800759:	8d 65 f8             	lea    -0x8(%ebp),%esp
  80075c:	5b                   	pop    %ebx
  80075d:	5e                   	pop    %esi
  80075e:	5d                   	pop    %ebp
  80075f:	c3                   	ret    

00800760 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  800760:	55                   	push   %ebp
  800761:	89 e5                	mov    %esp,%ebp
  800763:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  800766:	8d 45 10             	lea    0x10(%ebp),%eax
  800769:	83 c0 04             	add    $0x4,%eax
  80076c:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  80076f:	8b 45 10             	mov    0x10(%ebp),%eax
  800772:	ff 75 f4             	pushl  -0xc(%ebp)
  800775:	50                   	push   %eax
  800776:	ff 75 0c             	pushl  0xc(%ebp)
  800779:	ff 75 08             	pushl  0x8(%ebp)
  80077c:	e8 07 fc ff ff       	call   800388 <vprintfmt>
  800781:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
  800784:	90                   	nop
  800785:	c9                   	leave  
  800786:	c3                   	ret    

00800787 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800787:	55                   	push   %ebp
  800788:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  80078a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80078d:	8b 40 08             	mov    0x8(%eax),%eax
  800790:	8d 50 01             	lea    0x1(%eax),%edx
  800793:	8b 45 0c             	mov    0xc(%ebp),%eax
  800796:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800799:	8b 45 0c             	mov    0xc(%ebp),%eax
  80079c:	8b 10                	mov    (%eax),%edx
  80079e:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007a1:	8b 40 04             	mov    0x4(%eax),%eax
  8007a4:	39 c2                	cmp    %eax,%edx
  8007a6:	73 12                	jae    8007ba <sprintputch+0x33>
		*b->buf++ = ch;
  8007a8:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007ab:	8b 00                	mov    (%eax),%eax
  8007ad:	8d 48 01             	lea    0x1(%eax),%ecx
  8007b0:	8b 55 0c             	mov    0xc(%ebp),%edx
  8007b3:	89 0a                	mov    %ecx,(%edx)
  8007b5:	8b 55 08             	mov    0x8(%ebp),%edx
  8007b8:	88 10                	mov    %dl,(%eax)
}
  8007ba:	90                   	nop
  8007bb:	5d                   	pop    %ebp
  8007bc:	c3                   	ret    

008007bd <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  8007bd:	55                   	push   %ebp
  8007be:	89 e5                	mov    %esp,%ebp
  8007c0:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  8007c3:	8b 45 08             	mov    0x8(%ebp),%eax
  8007c6:	89 45 ec             	mov    %eax,-0x14(%ebp)
  8007c9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8007cc:	8d 50 ff             	lea    -0x1(%eax),%edx
  8007cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8007d2:	01 d0                	add    %edx,%eax
  8007d4:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007d7:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007de:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8007e2:	74 06                	je     8007ea <vsnprintf+0x2d>
  8007e4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8007e8:	7f 07                	jg     8007f1 <vsnprintf+0x34>
		return -E_INVAL;
  8007ea:	b8 03 00 00 00       	mov    $0x3,%eax
  8007ef:	eb 20                	jmp    800811 <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007f1:	ff 75 14             	pushl  0x14(%ebp)
  8007f4:	ff 75 10             	pushl  0x10(%ebp)
  8007f7:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007fa:	50                   	push   %eax
  8007fb:	68 87 07 80 00       	push   $0x800787
  800800:	e8 83 fb ff ff       	call   800388 <vprintfmt>
  800805:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
  800808:	8b 45 ec             	mov    -0x14(%ebp),%eax
  80080b:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  80080e:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  800811:	c9                   	leave  
  800812:	c3                   	ret    

00800813 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800813:	55                   	push   %ebp
  800814:	89 e5                	mov    %esp,%ebp
  800816:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  800819:	8d 45 10             	lea    0x10(%ebp),%eax
  80081c:	83 c0 04             	add    $0x4,%eax
  80081f:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  800822:	8b 45 10             	mov    0x10(%ebp),%eax
  800825:	ff 75 f4             	pushl  -0xc(%ebp)
  800828:	50                   	push   %eax
  800829:	ff 75 0c             	pushl  0xc(%ebp)
  80082c:	ff 75 08             	pushl  0x8(%ebp)
  80082f:	e8 89 ff ff ff       	call   8007bd <vsnprintf>
  800834:	83 c4 10             	add    $0x10,%esp
  800837:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  80083a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80083d:	c9                   	leave  
  80083e:	c3                   	ret    

0080083f <readline>:

#define BUFLEN 1024
//static char buf[BUFLEN];

void readline(const char *prompt, char* buf)
{
  80083f:	55                   	push   %ebp
  800840:	89 e5                	mov    %esp,%ebp
  800842:	83 ec 18             	sub    $0x18,%esp
	int i, c, echoing;
	
	if (prompt != NULL)
  800845:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800849:	74 13                	je     80085e <readline+0x1f>
		cprintf("%s", prompt);
  80084b:	83 ec 08             	sub    $0x8,%esp
  80084e:	ff 75 08             	pushl  0x8(%ebp)
  800851:	68 fc 15 80 00       	push   $0x8015fc
  800856:	e8 88 f9 ff ff       	call   8001e3 <cprintf>
  80085b:	83 c4 10             	add    $0x10,%esp

	
	i = 0;
  80085e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	echoing = iscons(0);	
  800865:	83 ec 0c             	sub    $0xc,%esp
  800868:	6a 00                	push   $0x0
  80086a:	e8 47 08 00 00       	call   8010b6 <iscons>
  80086f:	83 c4 10             	add    $0x10,%esp
  800872:	89 45 f0             	mov    %eax,-0x10(%ebp)
	while (1) {
		c = getchar();
  800875:	e8 2f 08 00 00       	call   8010a9 <getchar>
  80087a:	89 45 ec             	mov    %eax,-0x14(%ebp)
		if (c < 0) {
  80087d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  800881:	79 22                	jns    8008a5 <readline+0x66>
			if (c != -E_EOF)
  800883:	83 7d ec 07          	cmpl   $0x7,-0x14(%ebp)
  800887:	0f 84 ae 00 00 00    	je     80093b <readline+0xfc>
				cprintf("read error: %e\n", c);			
  80088d:	83 ec 08             	sub    $0x8,%esp
  800890:	ff 75 ec             	pushl  -0x14(%ebp)
  800893:	68 ff 15 80 00       	push   $0x8015ff
  800898:	e8 46 f9 ff ff       	call   8001e3 <cprintf>
  80089d:	83 c4 10             	add    $0x10,%esp
			return;
  8008a0:	e9 96 00 00 00       	jmp    80093b <readline+0xfc>
		} else if (c >= ' ' && i < BUFLEN-1) {
  8008a5:	83 7d ec 1f          	cmpl   $0x1f,-0x14(%ebp)
  8008a9:	7e 34                	jle    8008df <readline+0xa0>
  8008ab:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
  8008b2:	7f 2b                	jg     8008df <readline+0xa0>
			if (echoing)
  8008b4:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  8008b8:	74 0e                	je     8008c8 <readline+0x89>
				cputchar(c);
  8008ba:	83 ec 0c             	sub    $0xc,%esp
  8008bd:	ff 75 ec             	pushl  -0x14(%ebp)
  8008c0:	e8 c4 07 00 00       	call   801089 <cputchar>
  8008c5:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
  8008c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  8008cb:	8d 50 01             	lea    0x1(%eax),%edx
  8008ce:	89 55 f4             	mov    %edx,-0xc(%ebp)
  8008d1:	89 c2                	mov    %eax,%edx
  8008d3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008d6:	01 d0                	add    %edx,%eax
  8008d8:	8b 55 ec             	mov    -0x14(%ebp),%edx
  8008db:	88 10                	mov    %dl,(%eax)
  8008dd:	eb 57                	jmp    800936 <readline+0xf7>
		} else if (c == '\b' && i > 0) {
  8008df:	83 7d ec 08          	cmpl   $0x8,-0x14(%ebp)
  8008e3:	75 20                	jne    800905 <readline+0xc6>
  8008e5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  8008e9:	7e 1a                	jle    800905 <readline+0xc6>
			if (echoing)
  8008eb:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  8008ef:	74 0e                	je     8008ff <readline+0xc0>
				cputchar(c);
  8008f1:	83 ec 0c             	sub    $0xc,%esp
  8008f4:	ff 75 ec             	pushl  -0x14(%ebp)
  8008f7:	e8 8d 07 00 00       	call   801089 <cputchar>
  8008fc:	83 c4 10             	add    $0x10,%esp
			i--;
  8008ff:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
  800903:	eb 31                	jmp    800936 <readline+0xf7>
		} else if (c == '\n' || c == '\r') {
  800905:	83 7d ec 0a          	cmpl   $0xa,-0x14(%ebp)
  800909:	74 0a                	je     800915 <readline+0xd6>
  80090b:	83 7d ec 0d          	cmpl   $0xd,-0x14(%ebp)
  80090f:	0f 85 60 ff ff ff    	jne    800875 <readline+0x36>
			if (echoing)
  800915:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  800919:	74 0e                	je     800929 <readline+0xea>
				cputchar(c);
  80091b:	83 ec 0c             	sub    $0xc,%esp
  80091e:	ff 75 ec             	pushl  -0x14(%ebp)
  800921:	e8 63 07 00 00       	call   801089 <cputchar>
  800926:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;	
  800929:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80092c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80092f:	01 d0                	add    %edx,%eax
  800931:	c6 00 00             	movb   $0x0,(%eax)
			return;		
  800934:	eb 06                	jmp    80093c <readline+0xfd>
		}
	}
  800936:	e9 3a ff ff ff       	jmp    800875 <readline+0x36>
	while (1) {
		c = getchar();
		if (c < 0) {
			if (c != -E_EOF)
				cprintf("read error: %e\n", c);			
			return;
  80093b:	90                   	nop
				cputchar(c);
			buf[i] = 0;	
			return;		
		}
	}
}
  80093c:	c9                   	leave  
  80093d:	c3                   	ret    

0080093e <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  80093e:	55                   	push   %ebp
  80093f:	89 e5                	mov    %esp,%ebp
  800941:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  800944:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80094b:	eb 08                	jmp    800955 <strlen+0x17>
		n++;
  80094d:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  800951:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800955:	8b 45 08             	mov    0x8(%ebp),%eax
  800958:	0f b6 00             	movzbl (%eax),%eax
  80095b:	84 c0                	test   %al,%al
  80095d:	75 ee                	jne    80094d <strlen+0xf>
		n++;
	return n;
  80095f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800962:	c9                   	leave  
  800963:	c3                   	ret    

00800964 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  800964:	55                   	push   %ebp
  800965:	89 e5                	mov    %esp,%ebp
  800967:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  80096a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800971:	eb 0c                	jmp    80097f <strnlen+0x1b>
		n++;
  800973:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800977:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80097b:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  80097f:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800983:	74 0a                	je     80098f <strnlen+0x2b>
  800985:	8b 45 08             	mov    0x8(%ebp),%eax
  800988:	0f b6 00             	movzbl (%eax),%eax
  80098b:	84 c0                	test   %al,%al
  80098d:	75 e4                	jne    800973 <strnlen+0xf>
		n++;
	return n;
  80098f:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800992:	c9                   	leave  
  800993:	c3                   	ret    

00800994 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800994:	55                   	push   %ebp
  800995:	89 e5                	mov    %esp,%ebp
  800997:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  80099a:	8b 45 08             	mov    0x8(%ebp),%eax
  80099d:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  8009a0:	90                   	nop
  8009a1:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a4:	8d 50 01             	lea    0x1(%eax),%edx
  8009a7:	89 55 08             	mov    %edx,0x8(%ebp)
  8009aa:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009ad:	8d 4a 01             	lea    0x1(%edx),%ecx
  8009b0:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8009b3:	0f b6 12             	movzbl (%edx),%edx
  8009b6:	88 10                	mov    %dl,(%eax)
  8009b8:	0f b6 00             	movzbl (%eax),%eax
  8009bb:	84 c0                	test   %al,%al
  8009bd:	75 e2                	jne    8009a1 <strcpy+0xd>
		/* do nothing */;
	return ret;
  8009bf:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8009c2:	c9                   	leave  
  8009c3:	c3                   	ret    

008009c4 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  8009c4:	55                   	push   %ebp
  8009c5:	89 e5                	mov    %esp,%ebp
  8009c7:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  8009ca:	8b 45 08             	mov    0x8(%ebp),%eax
  8009cd:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  8009d0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8009d7:	eb 23                	jmp    8009fc <strncpy+0x38>
		*dst++ = *src;
  8009d9:	8b 45 08             	mov    0x8(%ebp),%eax
  8009dc:	8d 50 01             	lea    0x1(%eax),%edx
  8009df:	89 55 08             	mov    %edx,0x8(%ebp)
  8009e2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009e5:	0f b6 12             	movzbl (%edx),%edx
  8009e8:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  8009ea:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009ed:	0f b6 00             	movzbl (%eax),%eax
  8009f0:	84 c0                	test   %al,%al
  8009f2:	74 04                	je     8009f8 <strncpy+0x34>
			src++;
  8009f4:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8009f8:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  8009fc:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8009ff:	3b 45 10             	cmp    0x10(%ebp),%eax
  800a02:	72 d5                	jb     8009d9 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  800a04:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800a07:	c9                   	leave  
  800a08:	c3                   	ret    

00800a09 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800a09:	55                   	push   %ebp
  800a0a:	89 e5                	mov    %esp,%ebp
  800a0c:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800a0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800a12:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  800a15:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a19:	74 33                	je     800a4e <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  800a1b:	eb 17                	jmp    800a34 <strlcpy+0x2b>
			*dst++ = *src++;
  800a1d:	8b 45 08             	mov    0x8(%ebp),%eax
  800a20:	8d 50 01             	lea    0x1(%eax),%edx
  800a23:	89 55 08             	mov    %edx,0x8(%ebp)
  800a26:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a29:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a2c:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  800a2f:	0f b6 12             	movzbl (%edx),%edx
  800a32:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800a34:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800a38:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800a3c:	74 0a                	je     800a48 <strlcpy+0x3f>
  800a3e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a41:	0f b6 00             	movzbl (%eax),%eax
  800a44:	84 c0                	test   %al,%al
  800a46:	75 d5                	jne    800a1d <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800a48:	8b 45 08             	mov    0x8(%ebp),%eax
  800a4b:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  800a4e:	8b 55 08             	mov    0x8(%ebp),%edx
  800a51:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a54:	29 c2                	sub    %eax,%edx
  800a56:	89 d0                	mov    %edx,%eax
}
  800a58:	c9                   	leave  
  800a59:	c3                   	ret    

00800a5a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800a5a:	55                   	push   %ebp
  800a5b:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800a5d:	eb 08                	jmp    800a67 <strcmp+0xd>
		p++, q++;
  800a5f:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a63:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800a67:	8b 45 08             	mov    0x8(%ebp),%eax
  800a6a:	0f b6 00             	movzbl (%eax),%eax
  800a6d:	84 c0                	test   %al,%al
  800a6f:	74 10                	je     800a81 <strcmp+0x27>
  800a71:	8b 45 08             	mov    0x8(%ebp),%eax
  800a74:	0f b6 10             	movzbl (%eax),%edx
  800a77:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a7a:	0f b6 00             	movzbl (%eax),%eax
  800a7d:	38 c2                	cmp    %al,%dl
  800a7f:	74 de                	je     800a5f <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800a81:	8b 45 08             	mov    0x8(%ebp),%eax
  800a84:	0f b6 00             	movzbl (%eax),%eax
  800a87:	0f b6 d0             	movzbl %al,%edx
  800a8a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a8d:	0f b6 00             	movzbl (%eax),%eax
  800a90:	0f b6 c0             	movzbl %al,%eax
  800a93:	29 c2                	sub    %eax,%edx
  800a95:	89 d0                	mov    %edx,%eax
}
  800a97:	5d                   	pop    %ebp
  800a98:	c3                   	ret    

00800a99 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800a99:	55                   	push   %ebp
  800a9a:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800a9c:	eb 0c                	jmp    800aaa <strncmp+0x11>
		n--, p++, q++;
  800a9e:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800aa2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800aa6:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800aaa:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800aae:	74 1a                	je     800aca <strncmp+0x31>
  800ab0:	8b 45 08             	mov    0x8(%ebp),%eax
  800ab3:	0f b6 00             	movzbl (%eax),%eax
  800ab6:	84 c0                	test   %al,%al
  800ab8:	74 10                	je     800aca <strncmp+0x31>
  800aba:	8b 45 08             	mov    0x8(%ebp),%eax
  800abd:	0f b6 10             	movzbl (%eax),%edx
  800ac0:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ac3:	0f b6 00             	movzbl (%eax),%eax
  800ac6:	38 c2                	cmp    %al,%dl
  800ac8:	74 d4                	je     800a9e <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800aca:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800ace:	75 07                	jne    800ad7 <strncmp+0x3e>
		return 0;
  800ad0:	b8 00 00 00 00       	mov    $0x0,%eax
  800ad5:	eb 16                	jmp    800aed <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  800ad7:	8b 45 08             	mov    0x8(%ebp),%eax
  800ada:	0f b6 00             	movzbl (%eax),%eax
  800add:	0f b6 d0             	movzbl %al,%edx
  800ae0:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ae3:	0f b6 00             	movzbl (%eax),%eax
  800ae6:	0f b6 c0             	movzbl %al,%eax
  800ae9:	29 c2                	sub    %eax,%edx
  800aeb:	89 d0                	mov    %edx,%eax
}
  800aed:	5d                   	pop    %ebp
  800aee:	c3                   	ret    

00800aef <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800aef:	55                   	push   %ebp
  800af0:	89 e5                	mov    %esp,%ebp
  800af2:	83 ec 04             	sub    $0x4,%esp
  800af5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800af8:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800afb:	eb 14                	jmp    800b11 <strchr+0x22>
		if (*s == c)
  800afd:	8b 45 08             	mov    0x8(%ebp),%eax
  800b00:	0f b6 00             	movzbl (%eax),%eax
  800b03:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800b06:	75 05                	jne    800b0d <strchr+0x1e>
			return (char *) s;
  800b08:	8b 45 08             	mov    0x8(%ebp),%eax
  800b0b:	eb 13                	jmp    800b20 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800b0d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b11:	8b 45 08             	mov    0x8(%ebp),%eax
  800b14:	0f b6 00             	movzbl (%eax),%eax
  800b17:	84 c0                	test   %al,%al
  800b19:	75 e2                	jne    800afd <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  800b1b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b20:	c9                   	leave  
  800b21:	c3                   	ret    

00800b22 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  800b22:	55                   	push   %ebp
  800b23:	89 e5                	mov    %esp,%ebp
  800b25:	83 ec 04             	sub    $0x4,%esp
  800b28:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b2b:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800b2e:	eb 0f                	jmp    800b3f <strfind+0x1d>
		if (*s == c)
  800b30:	8b 45 08             	mov    0x8(%ebp),%eax
  800b33:	0f b6 00             	movzbl (%eax),%eax
  800b36:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800b39:	74 10                	je     800b4b <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800b3b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b3f:	8b 45 08             	mov    0x8(%ebp),%eax
  800b42:	0f b6 00             	movzbl (%eax),%eax
  800b45:	84 c0                	test   %al,%al
  800b47:	75 e7                	jne    800b30 <strfind+0xe>
  800b49:	eb 01                	jmp    800b4c <strfind+0x2a>
		if (*s == c)
			break;
  800b4b:	90                   	nop
	return (char *) s;
  800b4c:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b4f:	c9                   	leave  
  800b50:	c3                   	ret    

00800b51 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800b51:	55                   	push   %ebp
  800b52:	89 e5                	mov    %esp,%ebp
  800b54:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800b57:	8b 45 08             	mov    0x8(%ebp),%eax
  800b5a:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800b5d:	8b 45 10             	mov    0x10(%ebp),%eax
  800b60:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800b63:	eb 0e                	jmp    800b73 <memset+0x22>
		*p++ = c;
  800b65:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b68:	8d 50 01             	lea    0x1(%eax),%edx
  800b6b:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800b6e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800b71:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800b73:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800b77:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800b7b:	79 e8                	jns    800b65 <memset+0x14>
		*p++ = c;

	return v;
  800b7d:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b80:	c9                   	leave  
  800b81:	c3                   	ret    

00800b82 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800b82:	55                   	push   %ebp
  800b83:	89 e5                	mov    %esp,%ebp
  800b85:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800b88:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b8b:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800b8e:	8b 45 08             	mov    0x8(%ebp),%eax
  800b91:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800b94:	eb 17                	jmp    800bad <memcpy+0x2b>
		*d++ = *s++;
  800b96:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b99:	8d 50 01             	lea    0x1(%eax),%edx
  800b9c:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800b9f:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800ba2:	8d 4a 01             	lea    0x1(%edx),%ecx
  800ba5:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800ba8:	0f b6 12             	movzbl (%edx),%edx
  800bab:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800bad:	8b 45 10             	mov    0x10(%ebp),%eax
  800bb0:	8d 50 ff             	lea    -0x1(%eax),%edx
  800bb3:	89 55 10             	mov    %edx,0x10(%ebp)
  800bb6:	85 c0                	test   %eax,%eax
  800bb8:	75 dc                	jne    800b96 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800bba:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800bbd:	c9                   	leave  
  800bbe:	c3                   	ret    

00800bbf <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800bbf:	55                   	push   %ebp
  800bc0:	89 e5                	mov    %esp,%ebp
  800bc2:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800bc5:	8b 45 0c             	mov    0xc(%ebp),%eax
  800bc8:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800bcb:	8b 45 08             	mov    0x8(%ebp),%eax
  800bce:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800bd1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bd4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800bd7:	73 54                	jae    800c2d <memmove+0x6e>
  800bd9:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800bdc:	8b 45 10             	mov    0x10(%ebp),%eax
  800bdf:	01 d0                	add    %edx,%eax
  800be1:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800be4:	76 47                	jbe    800c2d <memmove+0x6e>
		s += n;
  800be6:	8b 45 10             	mov    0x10(%ebp),%eax
  800be9:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800bec:	8b 45 10             	mov    0x10(%ebp),%eax
  800bef:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800bf2:	eb 13                	jmp    800c07 <memmove+0x48>
			*--d = *--s;
  800bf4:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800bf8:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800bfc:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800bff:	0f b6 10             	movzbl (%eax),%edx
  800c02:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c05:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800c07:	8b 45 10             	mov    0x10(%ebp),%eax
  800c0a:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c0d:	89 55 10             	mov    %edx,0x10(%ebp)
  800c10:	85 c0                	test   %eax,%eax
  800c12:	75 e0                	jne    800bf4 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800c14:	eb 24                	jmp    800c3a <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
  800c16:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c19:	8d 50 01             	lea    0x1(%eax),%edx
  800c1c:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800c1f:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800c22:	8d 4a 01             	lea    0x1(%edx),%ecx
  800c25:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800c28:	0f b6 12             	movzbl (%edx),%edx
  800c2b:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800c2d:	8b 45 10             	mov    0x10(%ebp),%eax
  800c30:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c33:	89 55 10             	mov    %edx,0x10(%ebp)
  800c36:	85 c0                	test   %eax,%eax
  800c38:	75 dc                	jne    800c16 <memmove+0x57>
			*d++ = *s++;

	return dst;
  800c3a:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800c3d:	c9                   	leave  
  800c3e:	c3                   	ret    

00800c3f <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800c3f:	55                   	push   %ebp
  800c40:	89 e5                	mov    %esp,%ebp
  800c42:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800c45:	8b 45 08             	mov    0x8(%ebp),%eax
  800c48:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800c4b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c4e:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800c51:	eb 30                	jmp    800c83 <memcmp+0x44>
		if (*s1 != *s2)
  800c53:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c56:	0f b6 10             	movzbl (%eax),%edx
  800c59:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c5c:	0f b6 00             	movzbl (%eax),%eax
  800c5f:	38 c2                	cmp    %al,%dl
  800c61:	74 18                	je     800c7b <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800c63:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800c66:	0f b6 00             	movzbl (%eax),%eax
  800c69:	0f b6 d0             	movzbl %al,%edx
  800c6c:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c6f:	0f b6 00             	movzbl (%eax),%eax
  800c72:	0f b6 c0             	movzbl %al,%eax
  800c75:	29 c2                	sub    %eax,%edx
  800c77:	89 d0                	mov    %edx,%eax
  800c79:	eb 1a                	jmp    800c95 <memcmp+0x56>
		s1++, s2++;
  800c7b:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800c7f:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800c83:	8b 45 10             	mov    0x10(%ebp),%eax
  800c86:	8d 50 ff             	lea    -0x1(%eax),%edx
  800c89:	89 55 10             	mov    %edx,0x10(%ebp)
  800c8c:	85 c0                	test   %eax,%eax
  800c8e:	75 c3                	jne    800c53 <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800c90:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800c95:	c9                   	leave  
  800c96:	c3                   	ret    

00800c97 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800c97:	55                   	push   %ebp
  800c98:	89 e5                	mov    %esp,%ebp
  800c9a:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800c9d:	8b 55 08             	mov    0x8(%ebp),%edx
  800ca0:	8b 45 10             	mov    0x10(%ebp),%eax
  800ca3:	01 d0                	add    %edx,%eax
  800ca5:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800ca8:	eb 17                	jmp    800cc1 <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800caa:	8b 45 08             	mov    0x8(%ebp),%eax
  800cad:	0f b6 00             	movzbl (%eax),%eax
  800cb0:	0f b6 d0             	movzbl %al,%edx
  800cb3:	8b 45 0c             	mov    0xc(%ebp),%eax
  800cb6:	0f b6 c0             	movzbl %al,%eax
  800cb9:	39 c2                	cmp    %eax,%edx
  800cbb:	74 0e                	je     800ccb <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800cbd:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800cc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800cc4:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800cc7:	72 e1                	jb     800caa <memfind+0x13>
  800cc9:	eb 01                	jmp    800ccc <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
  800ccb:	90                   	nop
	return (void *) s;
  800ccc:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ccf:	c9                   	leave  
  800cd0:	c3                   	ret    

00800cd1 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800cd1:	55                   	push   %ebp
  800cd2:	89 e5                	mov    %esp,%ebp
  800cd4:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800cd7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800cde:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800ce5:	eb 04                	jmp    800ceb <strtol+0x1a>
		s++;
  800ce7:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800ceb:	8b 45 08             	mov    0x8(%ebp),%eax
  800cee:	0f b6 00             	movzbl (%eax),%eax
  800cf1:	3c 20                	cmp    $0x20,%al
  800cf3:	74 f2                	je     800ce7 <strtol+0x16>
  800cf5:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf8:	0f b6 00             	movzbl (%eax),%eax
  800cfb:	3c 09                	cmp    $0x9,%al
  800cfd:	74 e8                	je     800ce7 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800cff:	8b 45 08             	mov    0x8(%ebp),%eax
  800d02:	0f b6 00             	movzbl (%eax),%eax
  800d05:	3c 2b                	cmp    $0x2b,%al
  800d07:	75 06                	jne    800d0f <strtol+0x3e>
		s++;
  800d09:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d0d:	eb 15                	jmp    800d24 <strtol+0x53>
	else if (*s == '-')
  800d0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800d12:	0f b6 00             	movzbl (%eax),%eax
  800d15:	3c 2d                	cmp    $0x2d,%al
  800d17:	75 0b                	jne    800d24 <strtol+0x53>
		s++, neg = 1;
  800d19:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d1d:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800d24:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d28:	74 06                	je     800d30 <strtol+0x5f>
  800d2a:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800d2e:	75 24                	jne    800d54 <strtol+0x83>
  800d30:	8b 45 08             	mov    0x8(%ebp),%eax
  800d33:	0f b6 00             	movzbl (%eax),%eax
  800d36:	3c 30                	cmp    $0x30,%al
  800d38:	75 1a                	jne    800d54 <strtol+0x83>
  800d3a:	8b 45 08             	mov    0x8(%ebp),%eax
  800d3d:	83 c0 01             	add    $0x1,%eax
  800d40:	0f b6 00             	movzbl (%eax),%eax
  800d43:	3c 78                	cmp    $0x78,%al
  800d45:	75 0d                	jne    800d54 <strtol+0x83>
		s += 2, base = 16;
  800d47:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800d4b:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800d52:	eb 2a                	jmp    800d7e <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800d54:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d58:	75 17                	jne    800d71 <strtol+0xa0>
  800d5a:	8b 45 08             	mov    0x8(%ebp),%eax
  800d5d:	0f b6 00             	movzbl (%eax),%eax
  800d60:	3c 30                	cmp    $0x30,%al
  800d62:	75 0d                	jne    800d71 <strtol+0xa0>
		s++, base = 8;
  800d64:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800d68:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800d6f:	eb 0d                	jmp    800d7e <strtol+0xad>
	else if (base == 0)
  800d71:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800d75:	75 07                	jne    800d7e <strtol+0xad>
		base = 10;
  800d77:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800d7e:	8b 45 08             	mov    0x8(%ebp),%eax
  800d81:	0f b6 00             	movzbl (%eax),%eax
  800d84:	3c 2f                	cmp    $0x2f,%al
  800d86:	7e 1b                	jle    800da3 <strtol+0xd2>
  800d88:	8b 45 08             	mov    0x8(%ebp),%eax
  800d8b:	0f b6 00             	movzbl (%eax),%eax
  800d8e:	3c 39                	cmp    $0x39,%al
  800d90:	7f 11                	jg     800da3 <strtol+0xd2>
			dig = *s - '0';
  800d92:	8b 45 08             	mov    0x8(%ebp),%eax
  800d95:	0f b6 00             	movzbl (%eax),%eax
  800d98:	0f be c0             	movsbl %al,%eax
  800d9b:	83 e8 30             	sub    $0x30,%eax
  800d9e:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800da1:	eb 48                	jmp    800deb <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800da3:	8b 45 08             	mov    0x8(%ebp),%eax
  800da6:	0f b6 00             	movzbl (%eax),%eax
  800da9:	3c 60                	cmp    $0x60,%al
  800dab:	7e 1b                	jle    800dc8 <strtol+0xf7>
  800dad:	8b 45 08             	mov    0x8(%ebp),%eax
  800db0:	0f b6 00             	movzbl (%eax),%eax
  800db3:	3c 7a                	cmp    $0x7a,%al
  800db5:	7f 11                	jg     800dc8 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800db7:	8b 45 08             	mov    0x8(%ebp),%eax
  800dba:	0f b6 00             	movzbl (%eax),%eax
  800dbd:	0f be c0             	movsbl %al,%eax
  800dc0:	83 e8 57             	sub    $0x57,%eax
  800dc3:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800dc6:	eb 23                	jmp    800deb <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800dc8:	8b 45 08             	mov    0x8(%ebp),%eax
  800dcb:	0f b6 00             	movzbl (%eax),%eax
  800dce:	3c 40                	cmp    $0x40,%al
  800dd0:	7e 3c                	jle    800e0e <strtol+0x13d>
  800dd2:	8b 45 08             	mov    0x8(%ebp),%eax
  800dd5:	0f b6 00             	movzbl (%eax),%eax
  800dd8:	3c 5a                	cmp    $0x5a,%al
  800dda:	7f 32                	jg     800e0e <strtol+0x13d>
			dig = *s - 'A' + 10;
  800ddc:	8b 45 08             	mov    0x8(%ebp),%eax
  800ddf:	0f b6 00             	movzbl (%eax),%eax
  800de2:	0f be c0             	movsbl %al,%eax
  800de5:	83 e8 37             	sub    $0x37,%eax
  800de8:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800deb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800dee:	3b 45 10             	cmp    0x10(%ebp),%eax
  800df1:	7d 1a                	jge    800e0d <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
  800df3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800df7:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800dfa:	0f af 45 10          	imul   0x10(%ebp),%eax
  800dfe:	89 c2                	mov    %eax,%edx
  800e00:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800e03:	01 d0                	add    %edx,%eax
  800e05:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800e08:	e9 71 ff ff ff       	jmp    800d7e <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
  800e0d:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
  800e0e:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800e12:	74 08                	je     800e1c <strtol+0x14b>
		*endptr = (char *) s;
  800e14:	8b 45 0c             	mov    0xc(%ebp),%eax
  800e17:	8b 55 08             	mov    0x8(%ebp),%edx
  800e1a:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800e1c:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800e20:	74 07                	je     800e29 <strtol+0x158>
  800e22:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800e25:	f7 d8                	neg    %eax
  800e27:	eb 03                	jmp    800e2c <strtol+0x15b>
  800e29:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800e2c:	c9                   	leave  
  800e2d:	c3                   	ret    

00800e2e <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800e2e:	55                   	push   %ebp
  800e2f:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800e31:	8b 45 14             	mov    0x14(%ebp),%eax
  800e34:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800e3a:	8b 45 14             	mov    0x14(%ebp),%eax
  800e3d:	8b 00                	mov    (%eax),%eax
  800e3f:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800e46:	8b 45 10             	mov    0x10(%ebp),%eax
  800e49:	01 d0                	add    %edx,%eax
  800e4b:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e51:	eb 0c                	jmp    800e5f <strsplit+0x31>
			*string++ = 0;
  800e53:	8b 45 08             	mov    0x8(%ebp),%eax
  800e56:	8d 50 01             	lea    0x1(%eax),%edx
  800e59:	89 55 08             	mov    %edx,0x8(%ebp)
  800e5c:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800e5f:	8b 45 08             	mov    0x8(%ebp),%eax
  800e62:	0f b6 00             	movzbl (%eax),%eax
  800e65:	84 c0                	test   %al,%al
  800e67:	74 19                	je     800e82 <strsplit+0x54>
  800e69:	8b 45 08             	mov    0x8(%ebp),%eax
  800e6c:	0f b6 00             	movzbl (%eax),%eax
  800e6f:	0f be c0             	movsbl %al,%eax
  800e72:	50                   	push   %eax
  800e73:	ff 75 0c             	pushl  0xc(%ebp)
  800e76:	e8 74 fc ff ff       	call   800aef <strchr>
  800e7b:	83 c4 08             	add    $0x8,%esp
  800e7e:	85 c0                	test   %eax,%eax
  800e80:	75 d1                	jne    800e53 <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800e82:	8b 45 08             	mov    0x8(%ebp),%eax
  800e85:	0f b6 00             	movzbl (%eax),%eax
  800e88:	84 c0                	test   %al,%al
  800e8a:	74 5d                	je     800ee9 <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800e8c:	8b 45 14             	mov    0x14(%ebp),%eax
  800e8f:	8b 00                	mov    (%eax),%eax
  800e91:	83 f8 0f             	cmp    $0xf,%eax
  800e94:	75 07                	jne    800e9d <strsplit+0x6f>
		{
			return 0;
  800e96:	b8 00 00 00 00       	mov    $0x0,%eax
  800e9b:	eb 69                	jmp    800f06 <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800e9d:	8b 45 14             	mov    0x14(%ebp),%eax
  800ea0:	8b 00                	mov    (%eax),%eax
  800ea2:	8d 48 01             	lea    0x1(%eax),%ecx
  800ea5:	8b 55 14             	mov    0x14(%ebp),%edx
  800ea8:	89 0a                	mov    %ecx,(%edx)
  800eaa:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800eb1:	8b 45 10             	mov    0x10(%ebp),%eax
  800eb4:	01 c2                	add    %eax,%edx
  800eb6:	8b 45 08             	mov    0x8(%ebp),%eax
  800eb9:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800ebb:	eb 04                	jmp    800ec1 <strsplit+0x93>
			string++;
  800ebd:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800ec1:	8b 45 08             	mov    0x8(%ebp),%eax
  800ec4:	0f b6 00             	movzbl (%eax),%eax
  800ec7:	84 c0                	test   %al,%al
  800ec9:	74 86                	je     800e51 <strsplit+0x23>
  800ecb:	8b 45 08             	mov    0x8(%ebp),%eax
  800ece:	0f b6 00             	movzbl (%eax),%eax
  800ed1:	0f be c0             	movsbl %al,%eax
  800ed4:	50                   	push   %eax
  800ed5:	ff 75 0c             	pushl  0xc(%ebp)
  800ed8:	e8 12 fc ff ff       	call   800aef <strchr>
  800edd:	83 c4 08             	add    $0x8,%esp
  800ee0:	85 c0                	test   %eax,%eax
  800ee2:	74 d9                	je     800ebd <strsplit+0x8f>
			string++;
	}
  800ee4:	e9 68 ff ff ff       	jmp    800e51 <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
  800ee9:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800eea:	8b 45 14             	mov    0x14(%ebp),%eax
  800eed:	8b 00                	mov    (%eax),%eax
  800eef:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ef6:	8b 45 10             	mov    0x10(%ebp),%eax
  800ef9:	01 d0                	add    %edx,%eax
  800efb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800f01:	b8 01 00 00 00       	mov    $0x1,%eax
}
  800f06:	c9                   	leave  
  800f07:	c3                   	ret    

00800f08 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800f08:	55                   	push   %ebp
  800f09:	89 e5                	mov    %esp,%ebp
  800f0b:	57                   	push   %edi
  800f0c:	56                   	push   %esi
  800f0d:	53                   	push   %ebx
  800f0e:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800f11:	8b 45 08             	mov    0x8(%ebp),%eax
  800f14:	8b 55 0c             	mov    0xc(%ebp),%edx
  800f17:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800f1a:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800f1d:	8b 7d 18             	mov    0x18(%ebp),%edi
  800f20:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800f23:	cd 30                	int    $0x30
  800f25:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800f28:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800f2b:	83 c4 10             	add    $0x10,%esp
  800f2e:	5b                   	pop    %ebx
  800f2f:	5e                   	pop    %esi
  800f30:	5f                   	pop    %edi
  800f31:	5d                   	pop    %ebp
  800f32:	c3                   	ret    

00800f33 <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800f33:	55                   	push   %ebp
  800f34:	89 e5                	mov    %esp,%ebp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800f36:	8b 45 08             	mov    0x8(%ebp),%eax
  800f39:	6a 00                	push   $0x0
  800f3b:	6a 00                	push   $0x0
  800f3d:	6a 00                	push   $0x0
  800f3f:	ff 75 0c             	pushl  0xc(%ebp)
  800f42:	50                   	push   %eax
  800f43:	6a 00                	push   $0x0
  800f45:	e8 be ff ff ff       	call   800f08 <syscall>
  800f4a:	83 c4 18             	add    $0x18,%esp
}
  800f4d:	90                   	nop
  800f4e:	c9                   	leave  
  800f4f:	c3                   	ret    

00800f50 <sys_cgetc>:

int
sys_cgetc(void)
{
  800f50:	55                   	push   %ebp
  800f51:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800f53:	6a 00                	push   $0x0
  800f55:	6a 00                	push   $0x0
  800f57:	6a 00                	push   $0x0
  800f59:	6a 00                	push   $0x0
  800f5b:	6a 00                	push   $0x0
  800f5d:	6a 01                	push   $0x1
  800f5f:	e8 a4 ff ff ff       	call   800f08 <syscall>
  800f64:	83 c4 18             	add    $0x18,%esp
}
  800f67:	c9                   	leave  
  800f68:	c3                   	ret    

00800f69 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800f69:	55                   	push   %ebp
  800f6a:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800f6c:	8b 45 08             	mov    0x8(%ebp),%eax
  800f6f:	6a 00                	push   $0x0
  800f71:	6a 00                	push   $0x0
  800f73:	6a 00                	push   $0x0
  800f75:	6a 00                	push   $0x0
  800f77:	50                   	push   %eax
  800f78:	6a 03                	push   $0x3
  800f7a:	e8 89 ff ff ff       	call   800f08 <syscall>
  800f7f:	83 c4 18             	add    $0x18,%esp
}
  800f82:	c9                   	leave  
  800f83:	c3                   	ret    

00800f84 <sys_getenvid>:

int32 sys_getenvid(void)
{
  800f84:	55                   	push   %ebp
  800f85:	89 e5                	mov    %esp,%ebp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800f87:	6a 00                	push   $0x0
  800f89:	6a 00                	push   $0x0
  800f8b:	6a 00                	push   $0x0
  800f8d:	6a 00                	push   $0x0
  800f8f:	6a 00                	push   $0x0
  800f91:	6a 02                	push   $0x2
  800f93:	e8 70 ff ff ff       	call   800f08 <syscall>
  800f98:	83 c4 18             	add    $0x18,%esp
}
  800f9b:	c9                   	leave  
  800f9c:	c3                   	ret    

00800f9d <sys_env_sleep>:

void sys_env_sleep(void)
{
  800f9d:	55                   	push   %ebp
  800f9e:	89 e5                	mov    %esp,%ebp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800fa0:	6a 00                	push   $0x0
  800fa2:	6a 00                	push   $0x0
  800fa4:	6a 00                	push   $0x0
  800fa6:	6a 00                	push   $0x0
  800fa8:	6a 00                	push   $0x0
  800faa:	6a 04                	push   $0x4
  800fac:	e8 57 ff ff ff       	call   800f08 <syscall>
  800fb1:	83 c4 18             	add    $0x18,%esp
}
  800fb4:	90                   	nop
  800fb5:	c9                   	leave  
  800fb6:	c3                   	ret    

00800fb7 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800fb7:	55                   	push   %ebp
  800fb8:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800fba:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fbd:	8b 45 08             	mov    0x8(%ebp),%eax
  800fc0:	6a 00                	push   $0x0
  800fc2:	6a 00                	push   $0x0
  800fc4:	6a 00                	push   $0x0
  800fc6:	52                   	push   %edx
  800fc7:	50                   	push   %eax
  800fc8:	6a 05                	push   $0x5
  800fca:	e8 39 ff ff ff       	call   800f08 <syscall>
  800fcf:	83 c4 18             	add    $0x18,%esp
}
  800fd2:	c9                   	leave  
  800fd3:	c3                   	ret    

00800fd4 <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  800fd4:	55                   	push   %ebp
  800fd5:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  800fd7:	8b 55 0c             	mov    0xc(%ebp),%edx
  800fda:	8b 45 08             	mov    0x8(%ebp),%eax
  800fdd:	6a 00                	push   $0x0
  800fdf:	6a 00                	push   $0x0
  800fe1:	6a 00                	push   $0x0
  800fe3:	52                   	push   %edx
  800fe4:	50                   	push   %eax
  800fe5:	6a 06                	push   $0x6
  800fe7:	e8 1c ff ff ff       	call   800f08 <syscall>
  800fec:	83 c4 18             	add    $0x18,%esp
}
  800fef:	c9                   	leave  
  800ff0:	c3                   	ret    

00800ff1 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  800ff1:	55                   	push   %ebp
  800ff2:	89 e5                	mov    %esp,%ebp
  800ff4:	56                   	push   %esi
  800ff5:	53                   	push   %ebx
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  800ff6:	8b 75 18             	mov    0x18(%ebp),%esi
  800ff9:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800ffc:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800fff:	8b 55 0c             	mov    0xc(%ebp),%edx
  801002:	8b 45 08             	mov    0x8(%ebp),%eax
  801005:	56                   	push   %esi
  801006:	53                   	push   %ebx
  801007:	51                   	push   %ecx
  801008:	52                   	push   %edx
  801009:	50                   	push   %eax
  80100a:	6a 07                	push   $0x7
  80100c:	e8 f7 fe ff ff       	call   800f08 <syscall>
  801011:	83 c4 18             	add    $0x18,%esp
}
  801014:	8d 65 f8             	lea    -0x8(%ebp),%esp
  801017:	5b                   	pop    %ebx
  801018:	5e                   	pop    %esi
  801019:	5d                   	pop    %ebp
  80101a:	c3                   	ret    

0080101b <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  80101b:	55                   	push   %ebp
  80101c:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  80101e:	8b 55 0c             	mov    0xc(%ebp),%edx
  801021:	8b 45 08             	mov    0x8(%ebp),%eax
  801024:	6a 00                	push   $0x0
  801026:	6a 00                	push   $0x0
  801028:	6a 00                	push   $0x0
  80102a:	52                   	push   %edx
  80102b:	50                   	push   %eax
  80102c:	6a 08                	push   $0x8
  80102e:	e8 d5 fe ff ff       	call   800f08 <syscall>
  801033:	83 c4 18             	add    $0x18,%esp
}
  801036:	c9                   	leave  
  801037:	c3                   	ret    

00801038 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  801038:	55                   	push   %ebp
  801039:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  80103b:	6a 00                	push   $0x0
  80103d:	6a 00                	push   $0x0
  80103f:	6a 00                	push   $0x0
  801041:	ff 75 0c             	pushl  0xc(%ebp)
  801044:	ff 75 08             	pushl  0x8(%ebp)
  801047:	6a 09                	push   $0x9
  801049:	e8 ba fe ff ff       	call   800f08 <syscall>
  80104e:	83 c4 18             	add    $0x18,%esp
}
  801051:	c9                   	leave  
  801052:	c3                   	ret    

00801053 <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  801053:	55                   	push   %ebp
  801054:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  801056:	6a 00                	push   $0x0
  801058:	6a 00                	push   $0x0
  80105a:	6a 00                	push   $0x0
  80105c:	6a 00                	push   $0x0
  80105e:	6a 00                	push   $0x0
  801060:	6a 0a                	push   $0xa
  801062:	e8 a1 fe ff ff       	call   800f08 <syscall>
  801067:	83 c4 18             	add    $0x18,%esp
}
  80106a:	c9                   	leave  
  80106b:	c3                   	ret    

0080106c <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  80106c:	55                   	push   %ebp
  80106d:	89 e5                	mov    %esp,%ebp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  80106f:	8b 45 08             	mov    0x8(%ebp),%eax
  801072:	6a 00                	push   $0x0
  801074:	6a 00                	push   $0x0
  801076:	6a 00                	push   $0x0
  801078:	ff 75 0c             	pushl  0xc(%ebp)
  80107b:	50                   	push   %eax
  80107c:	6a 0b                	push   $0xb
  80107e:	e8 85 fe ff ff       	call   800f08 <syscall>
  801083:	83 c4 18             	add    $0x18,%esp
	return;
  801086:	90                   	nop
}
  801087:	c9                   	leave  
  801088:	c3                   	ret    

00801089 <cputchar>:
#include <inc/string.h>
#include <inc/lib.h>

void
cputchar(int ch)
{
  801089:	55                   	push   %ebp
  80108a:	89 e5                	mov    %esp,%ebp
  80108c:	83 ec 18             	sub    $0x18,%esp
	char c = ch;
  80108f:	8b 45 08             	mov    0x8(%ebp),%eax
  801092:	88 45 f7             	mov    %al,-0x9(%ebp)

	// Unlike standard Unix's putchar,
	// the cputchar function _always_ outputs to the system console.
	sys_cputs(&c, 1);
  801095:	83 ec 08             	sub    $0x8,%esp
  801098:	6a 01                	push   $0x1
  80109a:	8d 45 f7             	lea    -0x9(%ebp),%eax
  80109d:	50                   	push   %eax
  80109e:	e8 90 fe ff ff       	call   800f33 <sys_cputs>
  8010a3:	83 c4 10             	add    $0x10,%esp
}
  8010a6:	90                   	nop
  8010a7:	c9                   	leave  
  8010a8:	c3                   	ret    

008010a9 <getchar>:

int
getchar(void)
{
  8010a9:	55                   	push   %ebp
  8010aa:	89 e5                	mov    %esp,%ebp
  8010ac:	83 ec 08             	sub    $0x8,%esp
	return sys_cgetc();
  8010af:	e8 9c fe ff ff       	call   800f50 <sys_cgetc>
}
  8010b4:	c9                   	leave  
  8010b5:	c3                   	ret    

008010b6 <iscons>:


int iscons(int fdnum)
{
  8010b6:	55                   	push   %ebp
  8010b7:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
  8010b9:	b8 01 00 00 00       	mov    $0x1,%eax
}
  8010be:	5d                   	pop    %ebp
  8010bf:	c3                   	ret    

008010c0 <__udivdi3>:
  8010c0:	55                   	push   %ebp
  8010c1:	57                   	push   %edi
  8010c2:	56                   	push   %esi
  8010c3:	53                   	push   %ebx
  8010c4:	83 ec 1c             	sub    $0x1c,%esp
  8010c7:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  8010cb:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  8010cf:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  8010d3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8010d7:	85 f6                	test   %esi,%esi
  8010d9:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8010dd:	89 ca                	mov    %ecx,%edx
  8010df:	89 f8                	mov    %edi,%eax
  8010e1:	75 3d                	jne    801120 <__udivdi3+0x60>
  8010e3:	39 cf                	cmp    %ecx,%edi
  8010e5:	0f 87 c5 00 00 00    	ja     8011b0 <__udivdi3+0xf0>
  8010eb:	85 ff                	test   %edi,%edi
  8010ed:	89 fd                	mov    %edi,%ebp
  8010ef:	75 0b                	jne    8010fc <__udivdi3+0x3c>
  8010f1:	b8 01 00 00 00       	mov    $0x1,%eax
  8010f6:	31 d2                	xor    %edx,%edx
  8010f8:	f7 f7                	div    %edi
  8010fa:	89 c5                	mov    %eax,%ebp
  8010fc:	89 c8                	mov    %ecx,%eax
  8010fe:	31 d2                	xor    %edx,%edx
  801100:	f7 f5                	div    %ebp
  801102:	89 c1                	mov    %eax,%ecx
  801104:	89 d8                	mov    %ebx,%eax
  801106:	89 cf                	mov    %ecx,%edi
  801108:	f7 f5                	div    %ebp
  80110a:	89 c3                	mov    %eax,%ebx
  80110c:	89 d8                	mov    %ebx,%eax
  80110e:	89 fa                	mov    %edi,%edx
  801110:	83 c4 1c             	add    $0x1c,%esp
  801113:	5b                   	pop    %ebx
  801114:	5e                   	pop    %esi
  801115:	5f                   	pop    %edi
  801116:	5d                   	pop    %ebp
  801117:	c3                   	ret    
  801118:	90                   	nop
  801119:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801120:	39 ce                	cmp    %ecx,%esi
  801122:	77 74                	ja     801198 <__udivdi3+0xd8>
  801124:	0f bd fe             	bsr    %esi,%edi
  801127:	83 f7 1f             	xor    $0x1f,%edi
  80112a:	0f 84 98 00 00 00    	je     8011c8 <__udivdi3+0x108>
  801130:	bb 20 00 00 00       	mov    $0x20,%ebx
  801135:	89 f9                	mov    %edi,%ecx
  801137:	89 c5                	mov    %eax,%ebp
  801139:	29 fb                	sub    %edi,%ebx
  80113b:	d3 e6                	shl    %cl,%esi
  80113d:	89 d9                	mov    %ebx,%ecx
  80113f:	d3 ed                	shr    %cl,%ebp
  801141:	89 f9                	mov    %edi,%ecx
  801143:	d3 e0                	shl    %cl,%eax
  801145:	09 ee                	or     %ebp,%esi
  801147:	89 d9                	mov    %ebx,%ecx
  801149:	89 44 24 0c          	mov    %eax,0xc(%esp)
  80114d:	89 d5                	mov    %edx,%ebp
  80114f:	8b 44 24 08          	mov    0x8(%esp),%eax
  801153:	d3 ed                	shr    %cl,%ebp
  801155:	89 f9                	mov    %edi,%ecx
  801157:	d3 e2                	shl    %cl,%edx
  801159:	89 d9                	mov    %ebx,%ecx
  80115b:	d3 e8                	shr    %cl,%eax
  80115d:	09 c2                	or     %eax,%edx
  80115f:	89 d0                	mov    %edx,%eax
  801161:	89 ea                	mov    %ebp,%edx
  801163:	f7 f6                	div    %esi
  801165:	89 d5                	mov    %edx,%ebp
  801167:	89 c3                	mov    %eax,%ebx
  801169:	f7 64 24 0c          	mull   0xc(%esp)
  80116d:	39 d5                	cmp    %edx,%ebp
  80116f:	72 10                	jb     801181 <__udivdi3+0xc1>
  801171:	8b 74 24 08          	mov    0x8(%esp),%esi
  801175:	89 f9                	mov    %edi,%ecx
  801177:	d3 e6                	shl    %cl,%esi
  801179:	39 c6                	cmp    %eax,%esi
  80117b:	73 07                	jae    801184 <__udivdi3+0xc4>
  80117d:	39 d5                	cmp    %edx,%ebp
  80117f:	75 03                	jne    801184 <__udivdi3+0xc4>
  801181:	83 eb 01             	sub    $0x1,%ebx
  801184:	31 ff                	xor    %edi,%edi
  801186:	89 d8                	mov    %ebx,%eax
  801188:	89 fa                	mov    %edi,%edx
  80118a:	83 c4 1c             	add    $0x1c,%esp
  80118d:	5b                   	pop    %ebx
  80118e:	5e                   	pop    %esi
  80118f:	5f                   	pop    %edi
  801190:	5d                   	pop    %ebp
  801191:	c3                   	ret    
  801192:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801198:	31 ff                	xor    %edi,%edi
  80119a:	31 db                	xor    %ebx,%ebx
  80119c:	89 d8                	mov    %ebx,%eax
  80119e:	89 fa                	mov    %edi,%edx
  8011a0:	83 c4 1c             	add    $0x1c,%esp
  8011a3:	5b                   	pop    %ebx
  8011a4:	5e                   	pop    %esi
  8011a5:	5f                   	pop    %edi
  8011a6:	5d                   	pop    %ebp
  8011a7:	c3                   	ret    
  8011a8:	90                   	nop
  8011a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8011b0:	89 d8                	mov    %ebx,%eax
  8011b2:	f7 f7                	div    %edi
  8011b4:	31 ff                	xor    %edi,%edi
  8011b6:	89 c3                	mov    %eax,%ebx
  8011b8:	89 d8                	mov    %ebx,%eax
  8011ba:	89 fa                	mov    %edi,%edx
  8011bc:	83 c4 1c             	add    $0x1c,%esp
  8011bf:	5b                   	pop    %ebx
  8011c0:	5e                   	pop    %esi
  8011c1:	5f                   	pop    %edi
  8011c2:	5d                   	pop    %ebp
  8011c3:	c3                   	ret    
  8011c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8011c8:	39 ce                	cmp    %ecx,%esi
  8011ca:	72 0c                	jb     8011d8 <__udivdi3+0x118>
  8011cc:	31 db                	xor    %ebx,%ebx
  8011ce:	3b 44 24 08          	cmp    0x8(%esp),%eax
  8011d2:	0f 87 34 ff ff ff    	ja     80110c <__udivdi3+0x4c>
  8011d8:	bb 01 00 00 00       	mov    $0x1,%ebx
  8011dd:	e9 2a ff ff ff       	jmp    80110c <__udivdi3+0x4c>
  8011e2:	66 90                	xchg   %ax,%ax
  8011e4:	66 90                	xchg   %ax,%ax
  8011e6:	66 90                	xchg   %ax,%ax
  8011e8:	66 90                	xchg   %ax,%ax
  8011ea:	66 90                	xchg   %ax,%ax
  8011ec:	66 90                	xchg   %ax,%ax
  8011ee:	66 90                	xchg   %ax,%ax

008011f0 <__umoddi3>:
  8011f0:	55                   	push   %ebp
  8011f1:	57                   	push   %edi
  8011f2:	56                   	push   %esi
  8011f3:	53                   	push   %ebx
  8011f4:	83 ec 1c             	sub    $0x1c,%esp
  8011f7:	8b 54 24 3c          	mov    0x3c(%esp),%edx
  8011fb:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  8011ff:	8b 74 24 34          	mov    0x34(%esp),%esi
  801203:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801207:	85 d2                	test   %edx,%edx
  801209:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80120d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801211:	89 f3                	mov    %esi,%ebx
  801213:	89 3c 24             	mov    %edi,(%esp)
  801216:	89 74 24 04          	mov    %esi,0x4(%esp)
  80121a:	75 1c                	jne    801238 <__umoddi3+0x48>
  80121c:	39 f7                	cmp    %esi,%edi
  80121e:	76 50                	jbe    801270 <__umoddi3+0x80>
  801220:	89 c8                	mov    %ecx,%eax
  801222:	89 f2                	mov    %esi,%edx
  801224:	f7 f7                	div    %edi
  801226:	89 d0                	mov    %edx,%eax
  801228:	31 d2                	xor    %edx,%edx
  80122a:	83 c4 1c             	add    $0x1c,%esp
  80122d:	5b                   	pop    %ebx
  80122e:	5e                   	pop    %esi
  80122f:	5f                   	pop    %edi
  801230:	5d                   	pop    %ebp
  801231:	c3                   	ret    
  801232:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801238:	39 f2                	cmp    %esi,%edx
  80123a:	89 d0                	mov    %edx,%eax
  80123c:	77 52                	ja     801290 <__umoddi3+0xa0>
  80123e:	0f bd ea             	bsr    %edx,%ebp
  801241:	83 f5 1f             	xor    $0x1f,%ebp
  801244:	75 5a                	jne    8012a0 <__umoddi3+0xb0>
  801246:	3b 54 24 04          	cmp    0x4(%esp),%edx
  80124a:	0f 82 e0 00 00 00    	jb     801330 <__umoddi3+0x140>
  801250:	39 0c 24             	cmp    %ecx,(%esp)
  801253:	0f 86 d7 00 00 00    	jbe    801330 <__umoddi3+0x140>
  801259:	8b 44 24 08          	mov    0x8(%esp),%eax
  80125d:	8b 54 24 04          	mov    0x4(%esp),%edx
  801261:	83 c4 1c             	add    $0x1c,%esp
  801264:	5b                   	pop    %ebx
  801265:	5e                   	pop    %esi
  801266:	5f                   	pop    %edi
  801267:	5d                   	pop    %ebp
  801268:	c3                   	ret    
  801269:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801270:	85 ff                	test   %edi,%edi
  801272:	89 fd                	mov    %edi,%ebp
  801274:	75 0b                	jne    801281 <__umoddi3+0x91>
  801276:	b8 01 00 00 00       	mov    $0x1,%eax
  80127b:	31 d2                	xor    %edx,%edx
  80127d:	f7 f7                	div    %edi
  80127f:	89 c5                	mov    %eax,%ebp
  801281:	89 f0                	mov    %esi,%eax
  801283:	31 d2                	xor    %edx,%edx
  801285:	f7 f5                	div    %ebp
  801287:	89 c8                	mov    %ecx,%eax
  801289:	f7 f5                	div    %ebp
  80128b:	89 d0                	mov    %edx,%eax
  80128d:	eb 99                	jmp    801228 <__umoddi3+0x38>
  80128f:	90                   	nop
  801290:	89 c8                	mov    %ecx,%eax
  801292:	89 f2                	mov    %esi,%edx
  801294:	83 c4 1c             	add    $0x1c,%esp
  801297:	5b                   	pop    %ebx
  801298:	5e                   	pop    %esi
  801299:	5f                   	pop    %edi
  80129a:	5d                   	pop    %ebp
  80129b:	c3                   	ret    
  80129c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8012a0:	8b 34 24             	mov    (%esp),%esi
  8012a3:	bf 20 00 00 00       	mov    $0x20,%edi
  8012a8:	89 e9                	mov    %ebp,%ecx
  8012aa:	29 ef                	sub    %ebp,%edi
  8012ac:	d3 e0                	shl    %cl,%eax
  8012ae:	89 f9                	mov    %edi,%ecx
  8012b0:	89 f2                	mov    %esi,%edx
  8012b2:	d3 ea                	shr    %cl,%edx
  8012b4:	89 e9                	mov    %ebp,%ecx
  8012b6:	09 c2                	or     %eax,%edx
  8012b8:	89 d8                	mov    %ebx,%eax
  8012ba:	89 14 24             	mov    %edx,(%esp)
  8012bd:	89 f2                	mov    %esi,%edx
  8012bf:	d3 e2                	shl    %cl,%edx
  8012c1:	89 f9                	mov    %edi,%ecx
  8012c3:	89 54 24 04          	mov    %edx,0x4(%esp)
  8012c7:	8b 54 24 0c          	mov    0xc(%esp),%edx
  8012cb:	d3 e8                	shr    %cl,%eax
  8012cd:	89 e9                	mov    %ebp,%ecx
  8012cf:	89 c6                	mov    %eax,%esi
  8012d1:	d3 e3                	shl    %cl,%ebx
  8012d3:	89 f9                	mov    %edi,%ecx
  8012d5:	89 d0                	mov    %edx,%eax
  8012d7:	d3 e8                	shr    %cl,%eax
  8012d9:	89 e9                	mov    %ebp,%ecx
  8012db:	09 d8                	or     %ebx,%eax
  8012dd:	89 d3                	mov    %edx,%ebx
  8012df:	89 f2                	mov    %esi,%edx
  8012e1:	f7 34 24             	divl   (%esp)
  8012e4:	89 d6                	mov    %edx,%esi
  8012e6:	d3 e3                	shl    %cl,%ebx
  8012e8:	f7 64 24 04          	mull   0x4(%esp)
  8012ec:	39 d6                	cmp    %edx,%esi
  8012ee:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  8012f2:	89 d1                	mov    %edx,%ecx
  8012f4:	89 c3                	mov    %eax,%ebx
  8012f6:	72 08                	jb     801300 <__umoddi3+0x110>
  8012f8:	75 11                	jne    80130b <__umoddi3+0x11b>
  8012fa:	39 44 24 08          	cmp    %eax,0x8(%esp)
  8012fe:	73 0b                	jae    80130b <__umoddi3+0x11b>
  801300:	2b 44 24 04          	sub    0x4(%esp),%eax
  801304:	1b 14 24             	sbb    (%esp),%edx
  801307:	89 d1                	mov    %edx,%ecx
  801309:	89 c3                	mov    %eax,%ebx
  80130b:	8b 54 24 08          	mov    0x8(%esp),%edx
  80130f:	29 da                	sub    %ebx,%edx
  801311:	19 ce                	sbb    %ecx,%esi
  801313:	89 f9                	mov    %edi,%ecx
  801315:	89 f0                	mov    %esi,%eax
  801317:	d3 e0                	shl    %cl,%eax
  801319:	89 e9                	mov    %ebp,%ecx
  80131b:	d3 ea                	shr    %cl,%edx
  80131d:	89 e9                	mov    %ebp,%ecx
  80131f:	d3 ee                	shr    %cl,%esi
  801321:	09 d0                	or     %edx,%eax
  801323:	89 f2                	mov    %esi,%edx
  801325:	83 c4 1c             	add    $0x1c,%esp
  801328:	5b                   	pop    %ebx
  801329:	5e                   	pop    %esi
  80132a:	5f                   	pop    %edi
  80132b:	5d                   	pop    %ebp
  80132c:	c3                   	ret    
  80132d:	8d 76 00             	lea    0x0(%esi),%esi
  801330:	29 f9                	sub    %edi,%ecx
  801332:	19 d6                	sbb    %edx,%esi
  801334:	89 74 24 04          	mov    %esi,0x4(%esp)
  801338:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80133c:	e9 18 ff ff ff       	jmp    801259 <__umoddi3+0x69>
