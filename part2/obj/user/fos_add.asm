
obj/user/fos_add:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 60 00 00 00       	call   800096 <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
// hello, world
#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 18             	sub    $0x18,%esp
	int i1=0;
  80003e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
	int i2=0;
  800045:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)

	i1 = strtol("1", NULL, 10);
  80004c:	83 ec 04             	sub    $0x4,%esp
  80004f:	6a 0a                	push   $0xa
  800051:	6a 00                	push   $0x0
  800053:	68 00 12 80 00       	push   $0x801200
  800058:	e8 40 0b 00 00       	call   800b9d <strtol>
  80005d:	83 c4 10             	add    $0x10,%esp
  800060:	89 45 f4             	mov    %eax,-0xc(%ebp)
	i2 = strtol("2", NULL, 10);
  800063:	83 ec 04             	sub    $0x4,%esp
  800066:	6a 0a                	push   $0xa
  800068:	6a 00                	push   $0x0
  80006a:	68 02 12 80 00       	push   $0x801202
  80006f:	e8 29 0b 00 00       	call   800b9d <strtol>
  800074:	83 c4 10             	add    $0x10,%esp
  800077:	89 45 f0             	mov    %eax,-0x10(%ebp)

	cprintf("number 1 + number 2 = %d\n",i1+i2);
  80007a:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80007d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800080:	01 d0                	add    %edx,%eax
  800082:	83 ec 08             	sub    $0x8,%esp
  800085:	50                   	push   %eax
  800086:	68 04 12 80 00       	push   $0x801204
  80008b:	e8 1e 01 00 00       	call   8001ae <cprintf>
  800090:	83 c4 10             	add    $0x10,%esp
	return;	
  800093:	90                   	nop
}
  800094:	c9                   	leave  
  800095:	c3                   	ret    

00800096 <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  800096:	55                   	push   %ebp
  800097:	89 e5                	mov    %esp,%ebp
  800099:	83 ec 08             	sub    $0x8,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  80009c:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  8000a3:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  8000a6:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8000aa:	7e 0a                	jle    8000b6 <libmain+0x20>
		binaryname = argv[0];
  8000ac:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000af:	8b 00                	mov    (%eax),%eax
  8000b1:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  8000b6:	83 ec 08             	sub    $0x8,%esp
  8000b9:	ff 75 0c             	pushl  0xc(%ebp)
  8000bc:	ff 75 08             	pushl  0x8(%ebp)
  8000bf:	e8 74 ff ff ff       	call   800038 <_main>
  8000c4:	83 c4 10             	add    $0x10,%esp

	// exit gracefully
	//exit();
	sleep();
  8000c7:	e8 19 00 00 00       	call   8000e5 <sleep>
}
  8000cc:	90                   	nop
  8000cd:	c9                   	leave  
  8000ce:	c3                   	ret    

008000cf <exit>:

#include <inc/lib.h>

void
exit(void)
{
  8000cf:	55                   	push   %ebp
  8000d0:	89 e5                	mov    %esp,%ebp
  8000d2:	83 ec 08             	sub    $0x8,%esp
	sys_env_destroy(0);	
  8000d5:	83 ec 0c             	sub    $0xc,%esp
  8000d8:	6a 00                	push   $0x0
  8000da:	e8 56 0d 00 00       	call   800e35 <sys_env_destroy>
  8000df:	83 c4 10             	add    $0x10,%esp
}
  8000e2:	90                   	nop
  8000e3:	c9                   	leave  
  8000e4:	c3                   	ret    

008000e5 <sleep>:

void
sleep(void)
{	
  8000e5:	55                   	push   %ebp
  8000e6:	89 e5                	mov    %esp,%ebp
  8000e8:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  8000eb:	e8 79 0d 00 00       	call   800e69 <sys_env_sleep>
}
  8000f0:	90                   	nop
  8000f1:	c9                   	leave  
  8000f2:	c3                   	ret    

008000f3 <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000f3:	55                   	push   %ebp
  8000f4:	89 e5                	mov    %esp,%ebp
  8000f6:	83 ec 08             	sub    $0x8,%esp
	b->buf[b->idx++] = ch;
  8000f9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000fc:	8b 00                	mov    (%eax),%eax
  8000fe:	8d 48 01             	lea    0x1(%eax),%ecx
  800101:	8b 55 0c             	mov    0xc(%ebp),%edx
  800104:	89 0a                	mov    %ecx,(%edx)
  800106:	8b 55 08             	mov    0x8(%ebp),%edx
  800109:	89 d1                	mov    %edx,%ecx
  80010b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80010e:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  800112:	8b 45 0c             	mov    0xc(%ebp),%eax
  800115:	8b 00                	mov    (%eax),%eax
  800117:	3d ff 00 00 00       	cmp    $0xff,%eax
  80011c:	75 23                	jne    800141 <putch+0x4e>
		sys_cputs(b->buf, b->idx);
  80011e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800121:	8b 00                	mov    (%eax),%eax
  800123:	89 c2                	mov    %eax,%edx
  800125:	8b 45 0c             	mov    0xc(%ebp),%eax
  800128:	83 c0 08             	add    $0x8,%eax
  80012b:	83 ec 08             	sub    $0x8,%esp
  80012e:	52                   	push   %edx
  80012f:	50                   	push   %eax
  800130:	e8 ca 0c 00 00       	call   800dff <sys_cputs>
  800135:	83 c4 10             	add    $0x10,%esp
		b->idx = 0;
  800138:	8b 45 0c             	mov    0xc(%ebp),%eax
  80013b:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  800141:	8b 45 0c             	mov    0xc(%ebp),%eax
  800144:	8b 40 04             	mov    0x4(%eax),%eax
  800147:	8d 50 01             	lea    0x1(%eax),%edx
  80014a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80014d:	89 50 04             	mov    %edx,0x4(%eax)
}
  800150:	90                   	nop
  800151:	c9                   	leave  
  800152:	c3                   	ret    

00800153 <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  800153:	55                   	push   %ebp
  800154:	89 e5                	mov    %esp,%ebp
  800156:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  80015c:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  800163:	00 00 00 
	b.cnt = 0;
  800166:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  80016d:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  800170:	ff 75 0c             	pushl  0xc(%ebp)
  800173:	ff 75 08             	pushl  0x8(%ebp)
  800176:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80017c:	50                   	push   %eax
  80017d:	68 f3 00 80 00       	push   $0x8000f3
  800182:	e8 cc 01 00 00       	call   800353 <vprintfmt>
  800187:	83 c4 10             	add    $0x10,%esp
	sys_cputs(b.buf, b.idx);
  80018a:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  800190:	83 ec 08             	sub    $0x8,%esp
  800193:	50                   	push   %eax
  800194:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  80019a:	83 c0 08             	add    $0x8,%eax
  80019d:	50                   	push   %eax
  80019e:	e8 5c 0c 00 00       	call   800dff <sys_cputs>
  8001a3:	83 c4 10             	add    $0x10,%esp

	return b.cnt;
  8001a6:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  8001ac:	c9                   	leave  
  8001ad:	c3                   	ret    

008001ae <cprintf>:

int
cprintf(const char *fmt, ...)
{
  8001ae:	55                   	push   %ebp
  8001af:	89 e5                	mov    %esp,%ebp
  8001b1:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  8001b4:	8d 45 0c             	lea    0xc(%ebp),%eax
  8001b7:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  8001ba:	8b 45 08             	mov    0x8(%ebp),%eax
  8001bd:	83 ec 08             	sub    $0x8,%esp
  8001c0:	ff 75 f4             	pushl  -0xc(%ebp)
  8001c3:	50                   	push   %eax
  8001c4:	e8 8a ff ff ff       	call   800153 <vcprintf>
  8001c9:	83 c4 10             	add    $0x10,%esp
  8001cc:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  8001cf:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8001d2:	c9                   	leave  
  8001d3:	c3                   	ret    

008001d4 <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  8001d4:	55                   	push   %ebp
  8001d5:	89 e5                	mov    %esp,%ebp
  8001d7:	53                   	push   %ebx
  8001d8:	83 ec 14             	sub    $0x14,%esp
  8001db:	8b 45 10             	mov    0x10(%ebp),%eax
  8001de:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8001e1:	8b 45 14             	mov    0x14(%ebp),%eax
  8001e4:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001e7:	8b 45 18             	mov    0x18(%ebp),%eax
  8001ea:	ba 00 00 00 00       	mov    $0x0,%edx
  8001ef:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001f2:	77 55                	ja     800249 <printnum+0x75>
  8001f4:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001f7:	72 05                	jb     8001fe <printnum+0x2a>
  8001f9:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8001fc:	77 4b                	ja     800249 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001fe:	8b 45 1c             	mov    0x1c(%ebp),%eax
  800201:	8d 58 ff             	lea    -0x1(%eax),%ebx
  800204:	8b 45 18             	mov    0x18(%ebp),%eax
  800207:	ba 00 00 00 00       	mov    $0x0,%edx
  80020c:	52                   	push   %edx
  80020d:	50                   	push   %eax
  80020e:	ff 75 f4             	pushl  -0xc(%ebp)
  800211:	ff 75 f0             	pushl  -0x10(%ebp)
  800214:	e8 47 0d 00 00       	call   800f60 <__udivdi3>
  800219:	83 c4 10             	add    $0x10,%esp
  80021c:	83 ec 04             	sub    $0x4,%esp
  80021f:	ff 75 20             	pushl  0x20(%ebp)
  800222:	53                   	push   %ebx
  800223:	ff 75 18             	pushl  0x18(%ebp)
  800226:	52                   	push   %edx
  800227:	50                   	push   %eax
  800228:	ff 75 0c             	pushl  0xc(%ebp)
  80022b:	ff 75 08             	pushl  0x8(%ebp)
  80022e:	e8 a1 ff ff ff       	call   8001d4 <printnum>
  800233:	83 c4 20             	add    $0x20,%esp
  800236:	eb 1b                	jmp    800253 <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  800238:	83 ec 08             	sub    $0x8,%esp
  80023b:	ff 75 0c             	pushl  0xc(%ebp)
  80023e:	ff 75 20             	pushl  0x20(%ebp)
  800241:	8b 45 08             	mov    0x8(%ebp),%eax
  800244:	ff d0                	call   *%eax
  800246:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800249:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  80024d:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  800251:	7f e5                	jg     800238 <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  800253:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800256:	bb 00 00 00 00       	mov    $0x0,%ebx
  80025b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  80025e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  800261:	53                   	push   %ebx
  800262:	51                   	push   %ecx
  800263:	52                   	push   %edx
  800264:	50                   	push   %eax
  800265:	e8 26 0e 00 00       	call   801090 <__umoddi3>
  80026a:	83 c4 10             	add    $0x10,%esp
  80026d:	05 e0 12 80 00       	add    $0x8012e0,%eax
  800272:	0f b6 00             	movzbl (%eax),%eax
  800275:	0f be c0             	movsbl %al,%eax
  800278:	83 ec 08             	sub    $0x8,%esp
  80027b:	ff 75 0c             	pushl  0xc(%ebp)
  80027e:	50                   	push   %eax
  80027f:	8b 45 08             	mov    0x8(%ebp),%eax
  800282:	ff d0                	call   *%eax
  800284:	83 c4 10             	add    $0x10,%esp
}
  800287:	90                   	nop
  800288:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  80028b:	c9                   	leave  
  80028c:	c3                   	ret    

0080028d <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  80028d:	55                   	push   %ebp
  80028e:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  800290:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  800294:	7e 1c                	jle    8002b2 <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  800296:	8b 45 08             	mov    0x8(%ebp),%eax
  800299:	8b 00                	mov    (%eax),%eax
  80029b:	8d 50 08             	lea    0x8(%eax),%edx
  80029e:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a1:	89 10                	mov    %edx,(%eax)
  8002a3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a6:	8b 00                	mov    (%eax),%eax
  8002a8:	83 e8 08             	sub    $0x8,%eax
  8002ab:	8b 50 04             	mov    0x4(%eax),%edx
  8002ae:	8b 00                	mov    (%eax),%eax
  8002b0:	eb 40                	jmp    8002f2 <getuint+0x65>
	else if (lflag)
  8002b2:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002b6:	74 1e                	je     8002d6 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  8002b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002bb:	8b 00                	mov    (%eax),%eax
  8002bd:	8d 50 04             	lea    0x4(%eax),%edx
  8002c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c3:	89 10                	mov    %edx,(%eax)
  8002c5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c8:	8b 00                	mov    (%eax),%eax
  8002ca:	83 e8 04             	sub    $0x4,%eax
  8002cd:	8b 00                	mov    (%eax),%eax
  8002cf:	ba 00 00 00 00       	mov    $0x0,%edx
  8002d4:	eb 1c                	jmp    8002f2 <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  8002d6:	8b 45 08             	mov    0x8(%ebp),%eax
  8002d9:	8b 00                	mov    (%eax),%eax
  8002db:	8d 50 04             	lea    0x4(%eax),%edx
  8002de:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e1:	89 10                	mov    %edx,(%eax)
  8002e3:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e6:	8b 00                	mov    (%eax),%eax
  8002e8:	83 e8 04             	sub    $0x4,%eax
  8002eb:	8b 00                	mov    (%eax),%eax
  8002ed:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002f2:	5d                   	pop    %ebp
  8002f3:	c3                   	ret    

008002f4 <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  8002f4:	55                   	push   %ebp
  8002f5:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002f7:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002fb:	7e 1c                	jle    800319 <getint+0x25>
		return va_arg(*ap, long long);
  8002fd:	8b 45 08             	mov    0x8(%ebp),%eax
  800300:	8b 00                	mov    (%eax),%eax
  800302:	8d 50 08             	lea    0x8(%eax),%edx
  800305:	8b 45 08             	mov    0x8(%ebp),%eax
  800308:	89 10                	mov    %edx,(%eax)
  80030a:	8b 45 08             	mov    0x8(%ebp),%eax
  80030d:	8b 00                	mov    (%eax),%eax
  80030f:	83 e8 08             	sub    $0x8,%eax
  800312:	8b 50 04             	mov    0x4(%eax),%edx
  800315:	8b 00                	mov    (%eax),%eax
  800317:	eb 38                	jmp    800351 <getint+0x5d>
	else if (lflag)
  800319:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80031d:	74 1a                	je     800339 <getint+0x45>
		return va_arg(*ap, long);
  80031f:	8b 45 08             	mov    0x8(%ebp),%eax
  800322:	8b 00                	mov    (%eax),%eax
  800324:	8d 50 04             	lea    0x4(%eax),%edx
  800327:	8b 45 08             	mov    0x8(%ebp),%eax
  80032a:	89 10                	mov    %edx,(%eax)
  80032c:	8b 45 08             	mov    0x8(%ebp),%eax
  80032f:	8b 00                	mov    (%eax),%eax
  800331:	83 e8 04             	sub    $0x4,%eax
  800334:	8b 00                	mov    (%eax),%eax
  800336:	99                   	cltd   
  800337:	eb 18                	jmp    800351 <getint+0x5d>
	else
		return va_arg(*ap, int);
  800339:	8b 45 08             	mov    0x8(%ebp),%eax
  80033c:	8b 00                	mov    (%eax),%eax
  80033e:	8d 50 04             	lea    0x4(%eax),%edx
  800341:	8b 45 08             	mov    0x8(%ebp),%eax
  800344:	89 10                	mov    %edx,(%eax)
  800346:	8b 45 08             	mov    0x8(%ebp),%eax
  800349:	8b 00                	mov    (%eax),%eax
  80034b:	83 e8 04             	sub    $0x4,%eax
  80034e:	8b 00                	mov    (%eax),%eax
  800350:	99                   	cltd   
}
  800351:	5d                   	pop    %ebp
  800352:	c3                   	ret    

00800353 <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  800353:	55                   	push   %ebp
  800354:	89 e5                	mov    %esp,%ebp
  800356:	56                   	push   %esi
  800357:	53                   	push   %ebx
  800358:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80035b:	eb 17                	jmp    800374 <vprintfmt+0x21>
			if (ch == '\0')
  80035d:	85 db                	test   %ebx,%ebx
  80035f:	0f 84 be 03 00 00    	je     800723 <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
  800365:	83 ec 08             	sub    $0x8,%esp
  800368:	ff 75 0c             	pushl  0xc(%ebp)
  80036b:	53                   	push   %ebx
  80036c:	8b 45 08             	mov    0x8(%ebp),%eax
  80036f:	ff d0                	call   *%eax
  800371:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800374:	8b 45 10             	mov    0x10(%ebp),%eax
  800377:	8d 50 01             	lea    0x1(%eax),%edx
  80037a:	89 55 10             	mov    %edx,0x10(%ebp)
  80037d:	0f b6 00             	movzbl (%eax),%eax
  800380:	0f b6 d8             	movzbl %al,%ebx
  800383:	83 fb 25             	cmp    $0x25,%ebx
  800386:	75 d5                	jne    80035d <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  800388:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  80038c:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  800393:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  80039a:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  8003a1:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  8003a8:	8b 45 10             	mov    0x10(%ebp),%eax
  8003ab:	8d 50 01             	lea    0x1(%eax),%edx
  8003ae:	89 55 10             	mov    %edx,0x10(%ebp)
  8003b1:	0f b6 00             	movzbl (%eax),%eax
  8003b4:	0f b6 d8             	movzbl %al,%ebx
  8003b7:	8d 43 dd             	lea    -0x23(%ebx),%eax
  8003ba:	83 f8 55             	cmp    $0x55,%eax
  8003bd:	0f 87 33 03 00 00    	ja     8006f6 <vprintfmt+0x3a3>
  8003c3:	8b 04 85 04 13 80 00 	mov    0x801304(,%eax,4),%eax
  8003ca:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  8003cc:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  8003d0:	eb d6                	jmp    8003a8 <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  8003d2:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  8003d6:	eb d0                	jmp    8003a8 <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003d8:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  8003df:	8b 55 e0             	mov    -0x20(%ebp),%edx
  8003e2:	89 d0                	mov    %edx,%eax
  8003e4:	c1 e0 02             	shl    $0x2,%eax
  8003e7:	01 d0                	add    %edx,%eax
  8003e9:	01 c0                	add    %eax,%eax
  8003eb:	01 d8                	add    %ebx,%eax
  8003ed:	83 e8 30             	sub    $0x30,%eax
  8003f0:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  8003f3:	8b 45 10             	mov    0x10(%ebp),%eax
  8003f6:	0f b6 00             	movzbl (%eax),%eax
  8003f9:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  8003fc:	83 fb 2f             	cmp    $0x2f,%ebx
  8003ff:	7e 3f                	jle    800440 <vprintfmt+0xed>
  800401:	83 fb 39             	cmp    $0x39,%ebx
  800404:	7f 3a                	jg     800440 <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800406:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  80040a:	eb d3                	jmp    8003df <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  80040c:	8b 45 14             	mov    0x14(%ebp),%eax
  80040f:	83 c0 04             	add    $0x4,%eax
  800412:	89 45 14             	mov    %eax,0x14(%ebp)
  800415:	8b 45 14             	mov    0x14(%ebp),%eax
  800418:	83 e8 04             	sub    $0x4,%eax
  80041b:	8b 00                	mov    (%eax),%eax
  80041d:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  800420:	eb 1f                	jmp    800441 <vprintfmt+0xee>

		case '.':
			if (width < 0)
  800422:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800426:	79 80                	jns    8003a8 <vprintfmt+0x55>
				width = 0;
  800428:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  80042f:	e9 74 ff ff ff       	jmp    8003a8 <vprintfmt+0x55>

		case '#':
			altflag = 1;
  800434:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  80043b:	e9 68 ff ff ff       	jmp    8003a8 <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
  800440:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
  800441:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800445:	0f 89 5d ff ff ff    	jns    8003a8 <vprintfmt+0x55>
				width = precision, precision = -1;
  80044b:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80044e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  800451:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  800458:	e9 4b ff ff ff       	jmp    8003a8 <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  80045d:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  800461:	e9 42 ff ff ff       	jmp    8003a8 <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800466:	8b 45 14             	mov    0x14(%ebp),%eax
  800469:	83 c0 04             	add    $0x4,%eax
  80046c:	89 45 14             	mov    %eax,0x14(%ebp)
  80046f:	8b 45 14             	mov    0x14(%ebp),%eax
  800472:	83 e8 04             	sub    $0x4,%eax
  800475:	8b 00                	mov    (%eax),%eax
  800477:	83 ec 08             	sub    $0x8,%esp
  80047a:	ff 75 0c             	pushl  0xc(%ebp)
  80047d:	50                   	push   %eax
  80047e:	8b 45 08             	mov    0x8(%ebp),%eax
  800481:	ff d0                	call   *%eax
  800483:	83 c4 10             	add    $0x10,%esp
			break;
  800486:	e9 93 02 00 00       	jmp    80071e <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
  80048b:	8b 45 14             	mov    0x14(%ebp),%eax
  80048e:	83 c0 04             	add    $0x4,%eax
  800491:	89 45 14             	mov    %eax,0x14(%ebp)
  800494:	8b 45 14             	mov    0x14(%ebp),%eax
  800497:	83 e8 04             	sub    $0x4,%eax
  80049a:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  80049c:	85 db                	test   %ebx,%ebx
  80049e:	79 02                	jns    8004a2 <vprintfmt+0x14f>
				err = -err;
  8004a0:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  8004a2:	83 fb 07             	cmp    $0x7,%ebx
  8004a5:	7f 0b                	jg     8004b2 <vprintfmt+0x15f>
  8004a7:	8b 34 9d c0 12 80 00 	mov    0x8012c0(,%ebx,4),%esi
  8004ae:	85 f6                	test   %esi,%esi
  8004b0:	75 19                	jne    8004cb <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
  8004b2:	53                   	push   %ebx
  8004b3:	68 f1 12 80 00       	push   $0x8012f1
  8004b8:	ff 75 0c             	pushl  0xc(%ebp)
  8004bb:	ff 75 08             	pushl  0x8(%ebp)
  8004be:	e8 68 02 00 00       	call   80072b <printfmt>
  8004c3:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
  8004c6:	e9 53 02 00 00       	jmp    80071e <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  8004cb:	56                   	push   %esi
  8004cc:	68 fa 12 80 00       	push   $0x8012fa
  8004d1:	ff 75 0c             	pushl  0xc(%ebp)
  8004d4:	ff 75 08             	pushl  0x8(%ebp)
  8004d7:	e8 4f 02 00 00       	call   80072b <printfmt>
  8004dc:	83 c4 10             	add    $0x10,%esp
			break;
  8004df:	e9 3a 02 00 00       	jmp    80071e <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  8004e4:	8b 45 14             	mov    0x14(%ebp),%eax
  8004e7:	83 c0 04             	add    $0x4,%eax
  8004ea:	89 45 14             	mov    %eax,0x14(%ebp)
  8004ed:	8b 45 14             	mov    0x14(%ebp),%eax
  8004f0:	83 e8 04             	sub    $0x4,%eax
  8004f3:	8b 30                	mov    (%eax),%esi
  8004f5:	85 f6                	test   %esi,%esi
  8004f7:	75 05                	jne    8004fe <vprintfmt+0x1ab>
				p = "(null)";
  8004f9:	be fd 12 80 00       	mov    $0x8012fd,%esi
			if (width > 0 && padc != '-')
  8004fe:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800502:	7e 6f                	jle    800573 <vprintfmt+0x220>
  800504:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  800508:	74 69                	je     800573 <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
  80050a:	8b 45 e0             	mov    -0x20(%ebp),%eax
  80050d:	83 ec 08             	sub    $0x8,%esp
  800510:	50                   	push   %eax
  800511:	56                   	push   %esi
  800512:	e8 19 03 00 00       	call   800830 <strnlen>
  800517:	83 c4 10             	add    $0x10,%esp
  80051a:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  80051d:	eb 17                	jmp    800536 <vprintfmt+0x1e3>
					putch(padc, putdat);
  80051f:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  800523:	83 ec 08             	sub    $0x8,%esp
  800526:	ff 75 0c             	pushl  0xc(%ebp)
  800529:	50                   	push   %eax
  80052a:	8b 45 08             	mov    0x8(%ebp),%eax
  80052d:	ff d0                	call   *%eax
  80052f:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  800532:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800536:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  80053a:	7f e3                	jg     80051f <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80053c:	eb 35                	jmp    800573 <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
  80053e:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  800542:	74 1c                	je     800560 <vprintfmt+0x20d>
  800544:	83 fb 1f             	cmp    $0x1f,%ebx
  800547:	7e 05                	jle    80054e <vprintfmt+0x1fb>
  800549:	83 fb 7e             	cmp    $0x7e,%ebx
  80054c:	7e 12                	jle    800560 <vprintfmt+0x20d>
					putch('?', putdat);
  80054e:	83 ec 08             	sub    $0x8,%esp
  800551:	ff 75 0c             	pushl  0xc(%ebp)
  800554:	6a 3f                	push   $0x3f
  800556:	8b 45 08             	mov    0x8(%ebp),%eax
  800559:	ff d0                	call   *%eax
  80055b:	83 c4 10             	add    $0x10,%esp
  80055e:	eb 0f                	jmp    80056f <vprintfmt+0x21c>
				else
					putch(ch, putdat);
  800560:	83 ec 08             	sub    $0x8,%esp
  800563:	ff 75 0c             	pushl  0xc(%ebp)
  800566:	53                   	push   %ebx
  800567:	8b 45 08             	mov    0x8(%ebp),%eax
  80056a:	ff d0                	call   *%eax
  80056c:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80056f:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800573:	89 f0                	mov    %esi,%eax
  800575:	8d 70 01             	lea    0x1(%eax),%esi
  800578:	0f b6 00             	movzbl (%eax),%eax
  80057b:	0f be d8             	movsbl %al,%ebx
  80057e:	85 db                	test   %ebx,%ebx
  800580:	74 26                	je     8005a8 <vprintfmt+0x255>
  800582:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800586:	78 b6                	js     80053e <vprintfmt+0x1eb>
  800588:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  80058c:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800590:	79 ac                	jns    80053e <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  800592:	eb 14                	jmp    8005a8 <vprintfmt+0x255>
				putch(' ', putdat);
  800594:	83 ec 08             	sub    $0x8,%esp
  800597:	ff 75 0c             	pushl  0xc(%ebp)
  80059a:	6a 20                	push   $0x20
  80059c:	8b 45 08             	mov    0x8(%ebp),%eax
  80059f:	ff d0                	call   *%eax
  8005a1:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  8005a4:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8005a8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8005ac:	7f e6                	jg     800594 <vprintfmt+0x241>
				putch(' ', putdat);
			break;
  8005ae:	e9 6b 01 00 00       	jmp    80071e <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  8005b3:	83 ec 08             	sub    $0x8,%esp
  8005b6:	ff 75 e8             	pushl  -0x18(%ebp)
  8005b9:	8d 45 14             	lea    0x14(%ebp),%eax
  8005bc:	50                   	push   %eax
  8005bd:	e8 32 fd ff ff       	call   8002f4 <getint>
  8005c2:	83 c4 10             	add    $0x10,%esp
  8005c5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005c8:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  8005cb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005ce:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005d1:	85 d2                	test   %edx,%edx
  8005d3:	79 23                	jns    8005f8 <vprintfmt+0x2a5>
				putch('-', putdat);
  8005d5:	83 ec 08             	sub    $0x8,%esp
  8005d8:	ff 75 0c             	pushl  0xc(%ebp)
  8005db:	6a 2d                	push   $0x2d
  8005dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8005e0:	ff d0                	call   *%eax
  8005e2:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
  8005e5:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005e8:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005eb:	f7 d8                	neg    %eax
  8005ed:	83 d2 00             	adc    $0x0,%edx
  8005f0:	f7 da                	neg    %edx
  8005f2:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005f5:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  8005f8:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8005ff:	e9 bc 00 00 00       	jmp    8006c0 <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  800604:	83 ec 08             	sub    $0x8,%esp
  800607:	ff 75 e8             	pushl  -0x18(%ebp)
  80060a:	8d 45 14             	lea    0x14(%ebp),%eax
  80060d:	50                   	push   %eax
  80060e:	e8 7a fc ff ff       	call   80028d <getuint>
  800613:	83 c4 10             	add    $0x10,%esp
  800616:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800619:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  80061c:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  800623:	e9 98 00 00 00       	jmp    8006c0 <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  800628:	83 ec 08             	sub    $0x8,%esp
  80062b:	ff 75 0c             	pushl  0xc(%ebp)
  80062e:	6a 58                	push   $0x58
  800630:	8b 45 08             	mov    0x8(%ebp),%eax
  800633:	ff d0                	call   *%eax
  800635:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  800638:	83 ec 08             	sub    $0x8,%esp
  80063b:	ff 75 0c             	pushl  0xc(%ebp)
  80063e:	6a 58                	push   $0x58
  800640:	8b 45 08             	mov    0x8(%ebp),%eax
  800643:	ff d0                	call   *%eax
  800645:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  800648:	83 ec 08             	sub    $0x8,%esp
  80064b:	ff 75 0c             	pushl  0xc(%ebp)
  80064e:	6a 58                	push   $0x58
  800650:	8b 45 08             	mov    0x8(%ebp),%eax
  800653:	ff d0                	call   *%eax
  800655:	83 c4 10             	add    $0x10,%esp
			break;
  800658:	e9 c1 00 00 00       	jmp    80071e <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
  80065d:	83 ec 08             	sub    $0x8,%esp
  800660:	ff 75 0c             	pushl  0xc(%ebp)
  800663:	6a 30                	push   $0x30
  800665:	8b 45 08             	mov    0x8(%ebp),%eax
  800668:	ff d0                	call   *%eax
  80066a:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
  80066d:	83 ec 08             	sub    $0x8,%esp
  800670:	ff 75 0c             	pushl  0xc(%ebp)
  800673:	6a 78                	push   $0x78
  800675:	8b 45 08             	mov    0x8(%ebp),%eax
  800678:	ff d0                	call   *%eax
  80067a:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  80067d:	8b 45 14             	mov    0x14(%ebp),%eax
  800680:	83 c0 04             	add    $0x4,%eax
  800683:	89 45 14             	mov    %eax,0x14(%ebp)
  800686:	8b 45 14             	mov    0x14(%ebp),%eax
  800689:	83 e8 04             	sub    $0x4,%eax
  80068c:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  80068e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800691:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  800698:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  80069f:	eb 1f                	jmp    8006c0 <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  8006a1:	83 ec 08             	sub    $0x8,%esp
  8006a4:	ff 75 e8             	pushl  -0x18(%ebp)
  8006a7:	8d 45 14             	lea    0x14(%ebp),%eax
  8006aa:	50                   	push   %eax
  8006ab:	e8 dd fb ff ff       	call   80028d <getuint>
  8006b0:	83 c4 10             	add    $0x10,%esp
  8006b3:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8006b6:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  8006b9:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  8006c0:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  8006c4:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8006c7:	83 ec 04             	sub    $0x4,%esp
  8006ca:	52                   	push   %edx
  8006cb:	ff 75 e4             	pushl  -0x1c(%ebp)
  8006ce:	50                   	push   %eax
  8006cf:	ff 75 f4             	pushl  -0xc(%ebp)
  8006d2:	ff 75 f0             	pushl  -0x10(%ebp)
  8006d5:	ff 75 0c             	pushl  0xc(%ebp)
  8006d8:	ff 75 08             	pushl  0x8(%ebp)
  8006db:	e8 f4 fa ff ff       	call   8001d4 <printnum>
  8006e0:	83 c4 20             	add    $0x20,%esp
			break;
  8006e3:	eb 39                	jmp    80071e <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006e5:	83 ec 08             	sub    $0x8,%esp
  8006e8:	ff 75 0c             	pushl  0xc(%ebp)
  8006eb:	53                   	push   %ebx
  8006ec:	8b 45 08             	mov    0x8(%ebp),%eax
  8006ef:	ff d0                	call   *%eax
  8006f1:	83 c4 10             	add    $0x10,%esp
			break;
  8006f4:	eb 28                	jmp    80071e <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006f6:	83 ec 08             	sub    $0x8,%esp
  8006f9:	ff 75 0c             	pushl  0xc(%ebp)
  8006fc:	6a 25                	push   $0x25
  8006fe:	8b 45 08             	mov    0x8(%ebp),%eax
  800701:	ff d0                	call   *%eax
  800703:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
  800706:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80070a:	eb 04                	jmp    800710 <vprintfmt+0x3bd>
  80070c:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800710:	8b 45 10             	mov    0x10(%ebp),%eax
  800713:	83 e8 01             	sub    $0x1,%eax
  800716:	0f b6 00             	movzbl (%eax),%eax
  800719:	3c 25                	cmp    $0x25,%al
  80071b:	75 ef                	jne    80070c <vprintfmt+0x3b9>
				/* do nothing */;
			break;
  80071d:	90                   	nop
		}
	}
  80071e:	e9 38 fc ff ff       	jmp    80035b <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
  800723:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  800724:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800727:	5b                   	pop    %ebx
  800728:	5e                   	pop    %esi
  800729:	5d                   	pop    %ebp
  80072a:	c3                   	ret    

0080072b <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  80072b:	55                   	push   %ebp
  80072c:	89 e5                	mov    %esp,%ebp
  80072e:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  800731:	8d 45 10             	lea    0x10(%ebp),%eax
  800734:	83 c0 04             	add    $0x4,%eax
  800737:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  80073a:	8b 45 10             	mov    0x10(%ebp),%eax
  80073d:	ff 75 f4             	pushl  -0xc(%ebp)
  800740:	50                   	push   %eax
  800741:	ff 75 0c             	pushl  0xc(%ebp)
  800744:	ff 75 08             	pushl  0x8(%ebp)
  800747:	e8 07 fc ff ff       	call   800353 <vprintfmt>
  80074c:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
  80074f:	90                   	nop
  800750:	c9                   	leave  
  800751:	c3                   	ret    

00800752 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  800752:	55                   	push   %ebp
  800753:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800755:	8b 45 0c             	mov    0xc(%ebp),%eax
  800758:	8b 40 08             	mov    0x8(%eax),%eax
  80075b:	8d 50 01             	lea    0x1(%eax),%edx
  80075e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800761:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  800764:	8b 45 0c             	mov    0xc(%ebp),%eax
  800767:	8b 10                	mov    (%eax),%edx
  800769:	8b 45 0c             	mov    0xc(%ebp),%eax
  80076c:	8b 40 04             	mov    0x4(%eax),%eax
  80076f:	39 c2                	cmp    %eax,%edx
  800771:	73 12                	jae    800785 <sprintputch+0x33>
		*b->buf++ = ch;
  800773:	8b 45 0c             	mov    0xc(%ebp),%eax
  800776:	8b 00                	mov    (%eax),%eax
  800778:	8d 48 01             	lea    0x1(%eax),%ecx
  80077b:	8b 55 0c             	mov    0xc(%ebp),%edx
  80077e:	89 0a                	mov    %ecx,(%edx)
  800780:	8b 55 08             	mov    0x8(%ebp),%edx
  800783:	88 10                	mov    %dl,(%eax)
}
  800785:	90                   	nop
  800786:	5d                   	pop    %ebp
  800787:	c3                   	ret    

00800788 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800788:	55                   	push   %ebp
  800789:	89 e5                	mov    %esp,%ebp
  80078b:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  80078e:	8b 45 08             	mov    0x8(%ebp),%eax
  800791:	89 45 ec             	mov    %eax,-0x14(%ebp)
  800794:	8b 45 0c             	mov    0xc(%ebp),%eax
  800797:	8d 50 ff             	lea    -0x1(%eax),%edx
  80079a:	8b 45 08             	mov    0x8(%ebp),%eax
  80079d:	01 d0                	add    %edx,%eax
  80079f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8007a2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  8007a9:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  8007ad:	74 06                	je     8007b5 <vsnprintf+0x2d>
  8007af:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8007b3:	7f 07                	jg     8007bc <vsnprintf+0x34>
		return -E_INVAL;
  8007b5:	b8 03 00 00 00       	mov    $0x3,%eax
  8007ba:	eb 20                	jmp    8007dc <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  8007bc:	ff 75 14             	pushl  0x14(%ebp)
  8007bf:	ff 75 10             	pushl  0x10(%ebp)
  8007c2:	8d 45 ec             	lea    -0x14(%ebp),%eax
  8007c5:	50                   	push   %eax
  8007c6:	68 52 07 80 00       	push   $0x800752
  8007cb:	e8 83 fb ff ff       	call   800353 <vprintfmt>
  8007d0:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
  8007d3:	8b 45 ec             	mov    -0x14(%ebp),%eax
  8007d6:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  8007d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  8007dc:	c9                   	leave  
  8007dd:	c3                   	ret    

008007de <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  8007de:	55                   	push   %ebp
  8007df:	89 e5                	mov    %esp,%ebp
  8007e1:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  8007e4:	8d 45 10             	lea    0x10(%ebp),%eax
  8007e7:	83 c0 04             	add    $0x4,%eax
  8007ea:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  8007ed:	8b 45 10             	mov    0x10(%ebp),%eax
  8007f0:	ff 75 f4             	pushl  -0xc(%ebp)
  8007f3:	50                   	push   %eax
  8007f4:	ff 75 0c             	pushl  0xc(%ebp)
  8007f7:	ff 75 08             	pushl  0x8(%ebp)
  8007fa:	e8 89 ff ff ff       	call   800788 <vsnprintf>
  8007ff:	83 c4 10             	add    $0x10,%esp
  800802:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  800805:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800808:	c9                   	leave  
  800809:	c3                   	ret    

0080080a <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  80080a:	55                   	push   %ebp
  80080b:	89 e5                	mov    %esp,%ebp
  80080d:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  800810:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  800817:	eb 08                	jmp    800821 <strlen+0x17>
		n++;
  800819:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  80081d:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800821:	8b 45 08             	mov    0x8(%ebp),%eax
  800824:	0f b6 00             	movzbl (%eax),%eax
  800827:	84 c0                	test   %al,%al
  800829:	75 ee                	jne    800819 <strlen+0xf>
		n++;
	return n;
  80082b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80082e:	c9                   	leave  
  80082f:	c3                   	ret    

00800830 <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  800830:	55                   	push   %ebp
  800831:	89 e5                	mov    %esp,%ebp
  800833:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800836:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80083d:	eb 0c                	jmp    80084b <strnlen+0x1b>
		n++;
  80083f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  800843:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800847:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  80084b:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80084f:	74 0a                	je     80085b <strnlen+0x2b>
  800851:	8b 45 08             	mov    0x8(%ebp),%eax
  800854:	0f b6 00             	movzbl (%eax),%eax
  800857:	84 c0                	test   %al,%al
  800859:	75 e4                	jne    80083f <strnlen+0xf>
		n++;
	return n;
  80085b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80085e:	c9                   	leave  
  80085f:	c3                   	ret    

00800860 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  800860:	55                   	push   %ebp
  800861:	89 e5                	mov    %esp,%ebp
  800863:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800866:	8b 45 08             	mov    0x8(%ebp),%eax
  800869:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  80086c:	90                   	nop
  80086d:	8b 45 08             	mov    0x8(%ebp),%eax
  800870:	8d 50 01             	lea    0x1(%eax),%edx
  800873:	89 55 08             	mov    %edx,0x8(%ebp)
  800876:	8b 55 0c             	mov    0xc(%ebp),%edx
  800879:	8d 4a 01             	lea    0x1(%edx),%ecx
  80087c:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  80087f:	0f b6 12             	movzbl (%edx),%edx
  800882:	88 10                	mov    %dl,(%eax)
  800884:	0f b6 00             	movzbl (%eax),%eax
  800887:	84 c0                	test   %al,%al
  800889:	75 e2                	jne    80086d <strcpy+0xd>
		/* do nothing */;
	return ret;
  80088b:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  80088e:	c9                   	leave  
  80088f:	c3                   	ret    

00800890 <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  800890:	55                   	push   %ebp
  800891:	89 e5                	mov    %esp,%ebp
  800893:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  800896:	8b 45 08             	mov    0x8(%ebp),%eax
  800899:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  80089c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8008a3:	eb 23                	jmp    8008c8 <strncpy+0x38>
		*dst++ = *src;
  8008a5:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a8:	8d 50 01             	lea    0x1(%eax),%edx
  8008ab:	89 55 08             	mov    %edx,0x8(%ebp)
  8008ae:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008b1:	0f b6 12             	movzbl (%edx),%edx
  8008b4:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  8008b6:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008b9:	0f b6 00             	movzbl (%eax),%eax
  8008bc:	84 c0                	test   %al,%al
  8008be:	74 04                	je     8008c4 <strncpy+0x34>
			src++;
  8008c0:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  8008c4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  8008c8:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8008cb:	3b 45 10             	cmp    0x10(%ebp),%eax
  8008ce:	72 d5                	jb     8008a5 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  8008d0:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  8008d3:	c9                   	leave  
  8008d4:	c3                   	ret    

008008d5 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  8008d5:	55                   	push   %ebp
  8008d6:	89 e5                	mov    %esp,%ebp
  8008d8:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  8008db:	8b 45 08             	mov    0x8(%ebp),%eax
  8008de:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  8008e1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8008e5:	74 33                	je     80091a <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  8008e7:	eb 17                	jmp    800900 <strlcpy+0x2b>
			*dst++ = *src++;
  8008e9:	8b 45 08             	mov    0x8(%ebp),%eax
  8008ec:	8d 50 01             	lea    0x1(%eax),%edx
  8008ef:	89 55 08             	mov    %edx,0x8(%ebp)
  8008f2:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008f5:	8d 4a 01             	lea    0x1(%edx),%ecx
  8008f8:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8008fb:	0f b6 12             	movzbl (%edx),%edx
  8008fe:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  800900:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800904:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800908:	74 0a                	je     800914 <strlcpy+0x3f>
  80090a:	8b 45 0c             	mov    0xc(%ebp),%eax
  80090d:	0f b6 00             	movzbl (%eax),%eax
  800910:	84 c0                	test   %al,%al
  800912:	75 d5                	jne    8008e9 <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  800914:	8b 45 08             	mov    0x8(%ebp),%eax
  800917:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  80091a:	8b 55 08             	mov    0x8(%ebp),%edx
  80091d:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800920:	29 c2                	sub    %eax,%edx
  800922:	89 d0                	mov    %edx,%eax
}
  800924:	c9                   	leave  
  800925:	c3                   	ret    

00800926 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  800926:	55                   	push   %ebp
  800927:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  800929:	eb 08                	jmp    800933 <strcmp+0xd>
		p++, q++;
  80092b:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80092f:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  800933:	8b 45 08             	mov    0x8(%ebp),%eax
  800936:	0f b6 00             	movzbl (%eax),%eax
  800939:	84 c0                	test   %al,%al
  80093b:	74 10                	je     80094d <strcmp+0x27>
  80093d:	8b 45 08             	mov    0x8(%ebp),%eax
  800940:	0f b6 10             	movzbl (%eax),%edx
  800943:	8b 45 0c             	mov    0xc(%ebp),%eax
  800946:	0f b6 00             	movzbl (%eax),%eax
  800949:	38 c2                	cmp    %al,%dl
  80094b:	74 de                	je     80092b <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  80094d:	8b 45 08             	mov    0x8(%ebp),%eax
  800950:	0f b6 00             	movzbl (%eax),%eax
  800953:	0f b6 d0             	movzbl %al,%edx
  800956:	8b 45 0c             	mov    0xc(%ebp),%eax
  800959:	0f b6 00             	movzbl (%eax),%eax
  80095c:	0f b6 c0             	movzbl %al,%eax
  80095f:	29 c2                	sub    %eax,%edx
  800961:	89 d0                	mov    %edx,%eax
}
  800963:	5d                   	pop    %ebp
  800964:	c3                   	ret    

00800965 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800965:	55                   	push   %ebp
  800966:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800968:	eb 0c                	jmp    800976 <strncmp+0x11>
		n--, p++, q++;
  80096a:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  80096e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800972:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800976:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80097a:	74 1a                	je     800996 <strncmp+0x31>
  80097c:	8b 45 08             	mov    0x8(%ebp),%eax
  80097f:	0f b6 00             	movzbl (%eax),%eax
  800982:	84 c0                	test   %al,%al
  800984:	74 10                	je     800996 <strncmp+0x31>
  800986:	8b 45 08             	mov    0x8(%ebp),%eax
  800989:	0f b6 10             	movzbl (%eax),%edx
  80098c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80098f:	0f b6 00             	movzbl (%eax),%eax
  800992:	38 c2                	cmp    %al,%dl
  800994:	74 d4                	je     80096a <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800996:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  80099a:	75 07                	jne    8009a3 <strncmp+0x3e>
		return 0;
  80099c:	b8 00 00 00 00       	mov    $0x0,%eax
  8009a1:	eb 16                	jmp    8009b9 <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  8009a3:	8b 45 08             	mov    0x8(%ebp),%eax
  8009a6:	0f b6 00             	movzbl (%eax),%eax
  8009a9:	0f b6 d0             	movzbl %al,%edx
  8009ac:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009af:	0f b6 00             	movzbl (%eax),%eax
  8009b2:	0f b6 c0             	movzbl %al,%eax
  8009b5:	29 c2                	sub    %eax,%edx
  8009b7:	89 d0                	mov    %edx,%eax
}
  8009b9:	5d                   	pop    %ebp
  8009ba:	c3                   	ret    

008009bb <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  8009bb:	55                   	push   %ebp
  8009bc:	89 e5                	mov    %esp,%ebp
  8009be:	83 ec 04             	sub    $0x4,%esp
  8009c1:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009c4:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  8009c7:	eb 14                	jmp    8009dd <strchr+0x22>
		if (*s == c)
  8009c9:	8b 45 08             	mov    0x8(%ebp),%eax
  8009cc:	0f b6 00             	movzbl (%eax),%eax
  8009cf:	3a 45 fc             	cmp    -0x4(%ebp),%al
  8009d2:	75 05                	jne    8009d9 <strchr+0x1e>
			return (char *) s;
  8009d4:	8b 45 08             	mov    0x8(%ebp),%eax
  8009d7:	eb 13                	jmp    8009ec <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  8009d9:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009dd:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e0:	0f b6 00             	movzbl (%eax),%eax
  8009e3:	84 c0                	test   %al,%al
  8009e5:	75 e2                	jne    8009c9 <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  8009e7:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009ec:	c9                   	leave  
  8009ed:	c3                   	ret    

008009ee <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8009ee:	55                   	push   %ebp
  8009ef:	89 e5                	mov    %esp,%ebp
  8009f1:	83 ec 04             	sub    $0x4,%esp
  8009f4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009f7:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  8009fa:	eb 0f                	jmp    800a0b <strfind+0x1d>
		if (*s == c)
  8009fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ff:	0f b6 00             	movzbl (%eax),%eax
  800a02:	3a 45 fc             	cmp    -0x4(%ebp),%al
  800a05:	74 10                	je     800a17 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  800a07:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800a0b:	8b 45 08             	mov    0x8(%ebp),%eax
  800a0e:	0f b6 00             	movzbl (%eax),%eax
  800a11:	84 c0                	test   %al,%al
  800a13:	75 e7                	jne    8009fc <strfind+0xe>
  800a15:	eb 01                	jmp    800a18 <strfind+0x2a>
		if (*s == c)
			break;
  800a17:	90                   	nop
	return (char *) s;
  800a18:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a1b:	c9                   	leave  
  800a1c:	c3                   	ret    

00800a1d <memset>:


void *
memset(void *v, int c, uint32 n)
{
  800a1d:	55                   	push   %ebp
  800a1e:	89 e5                	mov    %esp,%ebp
  800a20:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  800a23:	8b 45 08             	mov    0x8(%ebp),%eax
  800a26:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  800a29:	8b 45 10             	mov    0x10(%ebp),%eax
  800a2c:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  800a2f:	eb 0e                	jmp    800a3f <memset+0x22>
		*p++ = c;
  800a31:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a34:	8d 50 01             	lea    0x1(%eax),%edx
  800a37:	89 55 fc             	mov    %edx,-0x4(%ebp)
  800a3a:	8b 55 0c             	mov    0xc(%ebp),%edx
  800a3d:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  800a3f:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800a43:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800a47:	79 e8                	jns    800a31 <memset+0x14>
		*p++ = c;

	return v;
  800a49:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a4c:	c9                   	leave  
  800a4d:	c3                   	ret    

00800a4e <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800a4e:	55                   	push   %ebp
  800a4f:	89 e5                	mov    %esp,%ebp
  800a51:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800a54:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a57:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a5a:	8b 45 08             	mov    0x8(%ebp),%eax
  800a5d:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800a60:	eb 17                	jmp    800a79 <memcpy+0x2b>
		*d++ = *s++;
  800a62:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800a65:	8d 50 01             	lea    0x1(%eax),%edx
  800a68:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800a6b:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800a6e:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a71:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800a74:	0f b6 12             	movzbl (%edx),%edx
  800a77:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800a79:	8b 45 10             	mov    0x10(%ebp),%eax
  800a7c:	8d 50 ff             	lea    -0x1(%eax),%edx
  800a7f:	89 55 10             	mov    %edx,0x10(%ebp)
  800a82:	85 c0                	test   %eax,%eax
  800a84:	75 dc                	jne    800a62 <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800a86:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a89:	c9                   	leave  
  800a8a:	c3                   	ret    

00800a8b <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800a8b:	55                   	push   %ebp
  800a8c:	89 e5                	mov    %esp,%ebp
  800a8e:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800a91:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a94:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a97:	8b 45 08             	mov    0x8(%ebp),%eax
  800a9a:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800a9d:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800aa0:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800aa3:	73 54                	jae    800af9 <memmove+0x6e>
  800aa5:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800aa8:	8b 45 10             	mov    0x10(%ebp),%eax
  800aab:	01 d0                	add    %edx,%eax
  800aad:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800ab0:	76 47                	jbe    800af9 <memmove+0x6e>
		s += n;
  800ab2:	8b 45 10             	mov    0x10(%ebp),%eax
  800ab5:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800ab8:	8b 45 10             	mov    0x10(%ebp),%eax
  800abb:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800abe:	eb 13                	jmp    800ad3 <memmove+0x48>
			*--d = *--s;
  800ac0:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800ac4:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800ac8:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800acb:	0f b6 10             	movzbl (%eax),%edx
  800ace:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ad1:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800ad3:	8b 45 10             	mov    0x10(%ebp),%eax
  800ad6:	8d 50 ff             	lea    -0x1(%eax),%edx
  800ad9:	89 55 10             	mov    %edx,0x10(%ebp)
  800adc:	85 c0                	test   %eax,%eax
  800ade:	75 e0                	jne    800ac0 <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800ae0:	eb 24                	jmp    800b06 <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
  800ae2:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ae5:	8d 50 01             	lea    0x1(%eax),%edx
  800ae8:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800aeb:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800aee:	8d 4a 01             	lea    0x1(%edx),%ecx
  800af1:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800af4:	0f b6 12             	movzbl (%edx),%edx
  800af7:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800af9:	8b 45 10             	mov    0x10(%ebp),%eax
  800afc:	8d 50 ff             	lea    -0x1(%eax),%edx
  800aff:	89 55 10             	mov    %edx,0x10(%ebp)
  800b02:	85 c0                	test   %eax,%eax
  800b04:	75 dc                	jne    800ae2 <memmove+0x57>
			*d++ = *s++;

	return dst;
  800b06:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b09:	c9                   	leave  
  800b0a:	c3                   	ret    

00800b0b <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800b0b:	55                   	push   %ebp
  800b0c:	89 e5                	mov    %esp,%ebp
  800b0e:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800b11:	8b 45 08             	mov    0x8(%ebp),%eax
  800b14:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800b17:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b1a:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800b1d:	eb 30                	jmp    800b4f <memcmp+0x44>
		if (*s1 != *s2)
  800b1f:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b22:	0f b6 10             	movzbl (%eax),%edx
  800b25:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b28:	0f b6 00             	movzbl (%eax),%eax
  800b2b:	38 c2                	cmp    %al,%dl
  800b2d:	74 18                	je     800b47 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800b2f:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800b32:	0f b6 00             	movzbl (%eax),%eax
  800b35:	0f b6 d0             	movzbl %al,%edx
  800b38:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800b3b:	0f b6 00             	movzbl (%eax),%eax
  800b3e:	0f b6 c0             	movzbl %al,%eax
  800b41:	29 c2                	sub    %eax,%edx
  800b43:	89 d0                	mov    %edx,%eax
  800b45:	eb 1a                	jmp    800b61 <memcmp+0x56>
		s1++, s2++;
  800b47:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800b4b:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800b4f:	8b 45 10             	mov    0x10(%ebp),%eax
  800b52:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b55:	89 55 10             	mov    %edx,0x10(%ebp)
  800b58:	85 c0                	test   %eax,%eax
  800b5a:	75 c3                	jne    800b1f <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b5c:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b61:	c9                   	leave  
  800b62:	c3                   	ret    

00800b63 <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800b63:	55                   	push   %ebp
  800b64:	89 e5                	mov    %esp,%ebp
  800b66:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800b69:	8b 55 08             	mov    0x8(%ebp),%edx
  800b6c:	8b 45 10             	mov    0x10(%ebp),%eax
  800b6f:	01 d0                	add    %edx,%eax
  800b71:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800b74:	eb 17                	jmp    800b8d <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b76:	8b 45 08             	mov    0x8(%ebp),%eax
  800b79:	0f b6 00             	movzbl (%eax),%eax
  800b7c:	0f b6 d0             	movzbl %al,%edx
  800b7f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b82:	0f b6 c0             	movzbl %al,%eax
  800b85:	39 c2                	cmp    %eax,%edx
  800b87:	74 0e                	je     800b97 <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b89:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b8d:	8b 45 08             	mov    0x8(%ebp),%eax
  800b90:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800b93:	72 e1                	jb     800b76 <memfind+0x13>
  800b95:	eb 01                	jmp    800b98 <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
  800b97:	90                   	nop
	return (void *) s;
  800b98:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b9b:	c9                   	leave  
  800b9c:	c3                   	ret    

00800b9d <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800b9d:	55                   	push   %ebp
  800b9e:	89 e5                	mov    %esp,%ebp
  800ba0:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800ba3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800baa:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb1:	eb 04                	jmp    800bb7 <strtol+0x1a>
		s++;
  800bb3:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800bb7:	8b 45 08             	mov    0x8(%ebp),%eax
  800bba:	0f b6 00             	movzbl (%eax),%eax
  800bbd:	3c 20                	cmp    $0x20,%al
  800bbf:	74 f2                	je     800bb3 <strtol+0x16>
  800bc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc4:	0f b6 00             	movzbl (%eax),%eax
  800bc7:	3c 09                	cmp    $0x9,%al
  800bc9:	74 e8                	je     800bb3 <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800bcb:	8b 45 08             	mov    0x8(%ebp),%eax
  800bce:	0f b6 00             	movzbl (%eax),%eax
  800bd1:	3c 2b                	cmp    $0x2b,%al
  800bd3:	75 06                	jne    800bdb <strtol+0x3e>
		s++;
  800bd5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bd9:	eb 15                	jmp    800bf0 <strtol+0x53>
	else if (*s == '-')
  800bdb:	8b 45 08             	mov    0x8(%ebp),%eax
  800bde:	0f b6 00             	movzbl (%eax),%eax
  800be1:	3c 2d                	cmp    $0x2d,%al
  800be3:	75 0b                	jne    800bf0 <strtol+0x53>
		s++, neg = 1;
  800be5:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800be9:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bf0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bf4:	74 06                	je     800bfc <strtol+0x5f>
  800bf6:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bfa:	75 24                	jne    800c20 <strtol+0x83>
  800bfc:	8b 45 08             	mov    0x8(%ebp),%eax
  800bff:	0f b6 00             	movzbl (%eax),%eax
  800c02:	3c 30                	cmp    $0x30,%al
  800c04:	75 1a                	jne    800c20 <strtol+0x83>
  800c06:	8b 45 08             	mov    0x8(%ebp),%eax
  800c09:	83 c0 01             	add    $0x1,%eax
  800c0c:	0f b6 00             	movzbl (%eax),%eax
  800c0f:	3c 78                	cmp    $0x78,%al
  800c11:	75 0d                	jne    800c20 <strtol+0x83>
		s += 2, base = 16;
  800c13:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800c17:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800c1e:	eb 2a                	jmp    800c4a <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800c20:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c24:	75 17                	jne    800c3d <strtol+0xa0>
  800c26:	8b 45 08             	mov    0x8(%ebp),%eax
  800c29:	0f b6 00             	movzbl (%eax),%eax
  800c2c:	3c 30                	cmp    $0x30,%al
  800c2e:	75 0d                	jne    800c3d <strtol+0xa0>
		s++, base = 8;
  800c30:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c34:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800c3b:	eb 0d                	jmp    800c4a <strtol+0xad>
	else if (base == 0)
  800c3d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800c41:	75 07                	jne    800c4a <strtol+0xad>
		base = 10;
  800c43:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c4a:	8b 45 08             	mov    0x8(%ebp),%eax
  800c4d:	0f b6 00             	movzbl (%eax),%eax
  800c50:	3c 2f                	cmp    $0x2f,%al
  800c52:	7e 1b                	jle    800c6f <strtol+0xd2>
  800c54:	8b 45 08             	mov    0x8(%ebp),%eax
  800c57:	0f b6 00             	movzbl (%eax),%eax
  800c5a:	3c 39                	cmp    $0x39,%al
  800c5c:	7f 11                	jg     800c6f <strtol+0xd2>
			dig = *s - '0';
  800c5e:	8b 45 08             	mov    0x8(%ebp),%eax
  800c61:	0f b6 00             	movzbl (%eax),%eax
  800c64:	0f be c0             	movsbl %al,%eax
  800c67:	83 e8 30             	sub    $0x30,%eax
  800c6a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800c6d:	eb 48                	jmp    800cb7 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800c6f:	8b 45 08             	mov    0x8(%ebp),%eax
  800c72:	0f b6 00             	movzbl (%eax),%eax
  800c75:	3c 60                	cmp    $0x60,%al
  800c77:	7e 1b                	jle    800c94 <strtol+0xf7>
  800c79:	8b 45 08             	mov    0x8(%ebp),%eax
  800c7c:	0f b6 00             	movzbl (%eax),%eax
  800c7f:	3c 7a                	cmp    $0x7a,%al
  800c81:	7f 11                	jg     800c94 <strtol+0xf7>
			dig = *s - 'a' + 10;
  800c83:	8b 45 08             	mov    0x8(%ebp),%eax
  800c86:	0f b6 00             	movzbl (%eax),%eax
  800c89:	0f be c0             	movsbl %al,%eax
  800c8c:	83 e8 57             	sub    $0x57,%eax
  800c8f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800c92:	eb 23                	jmp    800cb7 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800c94:	8b 45 08             	mov    0x8(%ebp),%eax
  800c97:	0f b6 00             	movzbl (%eax),%eax
  800c9a:	3c 40                	cmp    $0x40,%al
  800c9c:	7e 3c                	jle    800cda <strtol+0x13d>
  800c9e:	8b 45 08             	mov    0x8(%ebp),%eax
  800ca1:	0f b6 00             	movzbl (%eax),%eax
  800ca4:	3c 5a                	cmp    $0x5a,%al
  800ca6:	7f 32                	jg     800cda <strtol+0x13d>
			dig = *s - 'A' + 10;
  800ca8:	8b 45 08             	mov    0x8(%ebp),%eax
  800cab:	0f b6 00             	movzbl (%eax),%eax
  800cae:	0f be c0             	movsbl %al,%eax
  800cb1:	83 e8 37             	sub    $0x37,%eax
  800cb4:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800cb7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800cba:	3b 45 10             	cmp    0x10(%ebp),%eax
  800cbd:	7d 1a                	jge    800cd9 <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
  800cbf:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800cc3:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cc6:	0f af 45 10          	imul   0x10(%ebp),%eax
  800cca:	89 c2                	mov    %eax,%edx
  800ccc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800ccf:	01 d0                	add    %edx,%eax
  800cd1:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800cd4:	e9 71 ff ff ff       	jmp    800c4a <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
  800cd9:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
  800cda:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800cde:	74 08                	je     800ce8 <strtol+0x14b>
		*endptr = (char *) s;
  800ce0:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ce3:	8b 55 08             	mov    0x8(%ebp),%edx
  800ce6:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800ce8:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800cec:	74 07                	je     800cf5 <strtol+0x158>
  800cee:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cf1:	f7 d8                	neg    %eax
  800cf3:	eb 03                	jmp    800cf8 <strtol+0x15b>
  800cf5:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800cf8:	c9                   	leave  
  800cf9:	c3                   	ret    

00800cfa <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800cfa:	55                   	push   %ebp
  800cfb:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800cfd:	8b 45 14             	mov    0x14(%ebp),%eax
  800d00:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800d06:	8b 45 14             	mov    0x14(%ebp),%eax
  800d09:	8b 00                	mov    (%eax),%eax
  800d0b:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d12:	8b 45 10             	mov    0x10(%ebp),%eax
  800d15:	01 d0                	add    %edx,%eax
  800d17:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d1d:	eb 0c                	jmp    800d2b <strsplit+0x31>
			*string++ = 0;
  800d1f:	8b 45 08             	mov    0x8(%ebp),%eax
  800d22:	8d 50 01             	lea    0x1(%eax),%edx
  800d25:	89 55 08             	mov    %edx,0x8(%ebp)
  800d28:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800d2b:	8b 45 08             	mov    0x8(%ebp),%eax
  800d2e:	0f b6 00             	movzbl (%eax),%eax
  800d31:	84 c0                	test   %al,%al
  800d33:	74 19                	je     800d4e <strsplit+0x54>
  800d35:	8b 45 08             	mov    0x8(%ebp),%eax
  800d38:	0f b6 00             	movzbl (%eax),%eax
  800d3b:	0f be c0             	movsbl %al,%eax
  800d3e:	50                   	push   %eax
  800d3f:	ff 75 0c             	pushl  0xc(%ebp)
  800d42:	e8 74 fc ff ff       	call   8009bb <strchr>
  800d47:	83 c4 08             	add    $0x8,%esp
  800d4a:	85 c0                	test   %eax,%eax
  800d4c:	75 d1                	jne    800d1f <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800d4e:	8b 45 08             	mov    0x8(%ebp),%eax
  800d51:	0f b6 00             	movzbl (%eax),%eax
  800d54:	84 c0                	test   %al,%al
  800d56:	74 5d                	je     800db5 <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800d58:	8b 45 14             	mov    0x14(%ebp),%eax
  800d5b:	8b 00                	mov    (%eax),%eax
  800d5d:	83 f8 0f             	cmp    $0xf,%eax
  800d60:	75 07                	jne    800d69 <strsplit+0x6f>
		{
			return 0;
  800d62:	b8 00 00 00 00       	mov    $0x0,%eax
  800d67:	eb 69                	jmp    800dd2 <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800d69:	8b 45 14             	mov    0x14(%ebp),%eax
  800d6c:	8b 00                	mov    (%eax),%eax
  800d6e:	8d 48 01             	lea    0x1(%eax),%ecx
  800d71:	8b 55 14             	mov    0x14(%ebp),%edx
  800d74:	89 0a                	mov    %ecx,(%edx)
  800d76:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d7d:	8b 45 10             	mov    0x10(%ebp),%eax
  800d80:	01 c2                	add    %eax,%edx
  800d82:	8b 45 08             	mov    0x8(%ebp),%eax
  800d85:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800d87:	eb 04                	jmp    800d8d <strsplit+0x93>
			string++;
  800d89:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800d8d:	8b 45 08             	mov    0x8(%ebp),%eax
  800d90:	0f b6 00             	movzbl (%eax),%eax
  800d93:	84 c0                	test   %al,%al
  800d95:	74 86                	je     800d1d <strsplit+0x23>
  800d97:	8b 45 08             	mov    0x8(%ebp),%eax
  800d9a:	0f b6 00             	movzbl (%eax),%eax
  800d9d:	0f be c0             	movsbl %al,%eax
  800da0:	50                   	push   %eax
  800da1:	ff 75 0c             	pushl  0xc(%ebp)
  800da4:	e8 12 fc ff ff       	call   8009bb <strchr>
  800da9:	83 c4 08             	add    $0x8,%esp
  800dac:	85 c0                	test   %eax,%eax
  800dae:	74 d9                	je     800d89 <strsplit+0x8f>
			string++;
	}
  800db0:	e9 68 ff ff ff       	jmp    800d1d <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
  800db5:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800db6:	8b 45 14             	mov    0x14(%ebp),%eax
  800db9:	8b 00                	mov    (%eax),%eax
  800dbb:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800dc2:	8b 45 10             	mov    0x10(%ebp),%eax
  800dc5:	01 d0                	add    %edx,%eax
  800dc7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800dcd:	b8 01 00 00 00       	mov    $0x1,%eax
}
  800dd2:	c9                   	leave  
  800dd3:	c3                   	ret    

00800dd4 <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800dd4:	55                   	push   %ebp
  800dd5:	89 e5                	mov    %esp,%ebp
  800dd7:	57                   	push   %edi
  800dd8:	56                   	push   %esi
  800dd9:	53                   	push   %ebx
  800dda:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800ddd:	8b 45 08             	mov    0x8(%ebp),%eax
  800de0:	8b 55 0c             	mov    0xc(%ebp),%edx
  800de3:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800de6:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800de9:	8b 7d 18             	mov    0x18(%ebp),%edi
  800dec:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800def:	cd 30                	int    $0x30
  800df1:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800df4:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800df7:	83 c4 10             	add    $0x10,%esp
  800dfa:	5b                   	pop    %ebx
  800dfb:	5e                   	pop    %esi
  800dfc:	5f                   	pop    %edi
  800dfd:	5d                   	pop    %ebp
  800dfe:	c3                   	ret    

00800dff <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800dff:	55                   	push   %ebp
  800e00:	89 e5                	mov    %esp,%ebp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800e02:	8b 45 08             	mov    0x8(%ebp),%eax
  800e05:	6a 00                	push   $0x0
  800e07:	6a 00                	push   $0x0
  800e09:	6a 00                	push   $0x0
  800e0b:	ff 75 0c             	pushl  0xc(%ebp)
  800e0e:	50                   	push   %eax
  800e0f:	6a 00                	push   $0x0
  800e11:	e8 be ff ff ff       	call   800dd4 <syscall>
  800e16:	83 c4 18             	add    $0x18,%esp
}
  800e19:	90                   	nop
  800e1a:	c9                   	leave  
  800e1b:	c3                   	ret    

00800e1c <sys_cgetc>:

int
sys_cgetc(void)
{
  800e1c:	55                   	push   %ebp
  800e1d:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800e1f:	6a 00                	push   $0x0
  800e21:	6a 00                	push   $0x0
  800e23:	6a 00                	push   $0x0
  800e25:	6a 00                	push   $0x0
  800e27:	6a 00                	push   $0x0
  800e29:	6a 01                	push   $0x1
  800e2b:	e8 a4 ff ff ff       	call   800dd4 <syscall>
  800e30:	83 c4 18             	add    $0x18,%esp
}
  800e33:	c9                   	leave  
  800e34:	c3                   	ret    

00800e35 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800e35:	55                   	push   %ebp
  800e36:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800e38:	8b 45 08             	mov    0x8(%ebp),%eax
  800e3b:	6a 00                	push   $0x0
  800e3d:	6a 00                	push   $0x0
  800e3f:	6a 00                	push   $0x0
  800e41:	6a 00                	push   $0x0
  800e43:	50                   	push   %eax
  800e44:	6a 03                	push   $0x3
  800e46:	e8 89 ff ff ff       	call   800dd4 <syscall>
  800e4b:	83 c4 18             	add    $0x18,%esp
}
  800e4e:	c9                   	leave  
  800e4f:	c3                   	ret    

00800e50 <sys_getenvid>:

int32 sys_getenvid(void)
{
  800e50:	55                   	push   %ebp
  800e51:	89 e5                	mov    %esp,%ebp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800e53:	6a 00                	push   $0x0
  800e55:	6a 00                	push   $0x0
  800e57:	6a 00                	push   $0x0
  800e59:	6a 00                	push   $0x0
  800e5b:	6a 00                	push   $0x0
  800e5d:	6a 02                	push   $0x2
  800e5f:	e8 70 ff ff ff       	call   800dd4 <syscall>
  800e64:	83 c4 18             	add    $0x18,%esp
}
  800e67:	c9                   	leave  
  800e68:	c3                   	ret    

00800e69 <sys_env_sleep>:

void sys_env_sleep(void)
{
  800e69:	55                   	push   %ebp
  800e6a:	89 e5                	mov    %esp,%ebp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800e6c:	6a 00                	push   $0x0
  800e6e:	6a 00                	push   $0x0
  800e70:	6a 00                	push   $0x0
  800e72:	6a 00                	push   $0x0
  800e74:	6a 00                	push   $0x0
  800e76:	6a 04                	push   $0x4
  800e78:	e8 57 ff ff ff       	call   800dd4 <syscall>
  800e7d:	83 c4 18             	add    $0x18,%esp
}
  800e80:	90                   	nop
  800e81:	c9                   	leave  
  800e82:	c3                   	ret    

00800e83 <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800e83:	55                   	push   %ebp
  800e84:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800e86:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e89:	8b 45 08             	mov    0x8(%ebp),%eax
  800e8c:	6a 00                	push   $0x0
  800e8e:	6a 00                	push   $0x0
  800e90:	6a 00                	push   $0x0
  800e92:	52                   	push   %edx
  800e93:	50                   	push   %eax
  800e94:	6a 05                	push   $0x5
  800e96:	e8 39 ff ff ff       	call   800dd4 <syscall>
  800e9b:	83 c4 18             	add    $0x18,%esp
}
  800e9e:	c9                   	leave  
  800e9f:	c3                   	ret    

00800ea0 <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  800ea0:	55                   	push   %ebp
  800ea1:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  800ea3:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ea6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ea9:	6a 00                	push   $0x0
  800eab:	6a 00                	push   $0x0
  800ead:	6a 00                	push   $0x0
  800eaf:	52                   	push   %edx
  800eb0:	50                   	push   %eax
  800eb1:	6a 06                	push   $0x6
  800eb3:	e8 1c ff ff ff       	call   800dd4 <syscall>
  800eb8:	83 c4 18             	add    $0x18,%esp
}
  800ebb:	c9                   	leave  
  800ebc:	c3                   	ret    

00800ebd <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  800ebd:	55                   	push   %ebp
  800ebe:	89 e5                	mov    %esp,%ebp
  800ec0:	56                   	push   %esi
  800ec1:	53                   	push   %ebx
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  800ec2:	8b 75 18             	mov    0x18(%ebp),%esi
  800ec5:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800ec8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800ecb:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ece:	8b 45 08             	mov    0x8(%ebp),%eax
  800ed1:	56                   	push   %esi
  800ed2:	53                   	push   %ebx
  800ed3:	51                   	push   %ecx
  800ed4:	52                   	push   %edx
  800ed5:	50                   	push   %eax
  800ed6:	6a 07                	push   $0x7
  800ed8:	e8 f7 fe ff ff       	call   800dd4 <syscall>
  800edd:	83 c4 18             	add    $0x18,%esp
}
  800ee0:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800ee3:	5b                   	pop    %ebx
  800ee4:	5e                   	pop    %esi
  800ee5:	5d                   	pop    %ebp
  800ee6:	c3                   	ret    

00800ee7 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  800ee7:	55                   	push   %ebp
  800ee8:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  800eea:	8b 55 0c             	mov    0xc(%ebp),%edx
  800eed:	8b 45 08             	mov    0x8(%ebp),%eax
  800ef0:	6a 00                	push   $0x0
  800ef2:	6a 00                	push   $0x0
  800ef4:	6a 00                	push   $0x0
  800ef6:	52                   	push   %edx
  800ef7:	50                   	push   %eax
  800ef8:	6a 08                	push   $0x8
  800efa:	e8 d5 fe ff ff       	call   800dd4 <syscall>
  800eff:	83 c4 18             	add    $0x18,%esp
}
  800f02:	c9                   	leave  
  800f03:	c3                   	ret    

00800f04 <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  800f04:	55                   	push   %ebp
  800f05:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  800f07:	6a 00                	push   $0x0
  800f09:	6a 00                	push   $0x0
  800f0b:	6a 00                	push   $0x0
  800f0d:	ff 75 0c             	pushl  0xc(%ebp)
  800f10:	ff 75 08             	pushl  0x8(%ebp)
  800f13:	6a 09                	push   $0x9
  800f15:	e8 ba fe ff ff       	call   800dd4 <syscall>
  800f1a:	83 c4 18             	add    $0x18,%esp
}
  800f1d:	c9                   	leave  
  800f1e:	c3                   	ret    

00800f1f <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  800f1f:	55                   	push   %ebp
  800f20:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  800f22:	6a 00                	push   $0x0
  800f24:	6a 00                	push   $0x0
  800f26:	6a 00                	push   $0x0
  800f28:	6a 00                	push   $0x0
  800f2a:	6a 00                	push   $0x0
  800f2c:	6a 0a                	push   $0xa
  800f2e:	e8 a1 fe ff ff       	call   800dd4 <syscall>
  800f33:	83 c4 18             	add    $0x18,%esp
}
  800f36:	c9                   	leave  
  800f37:	c3                   	ret    

00800f38 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  800f38:	55                   	push   %ebp
  800f39:	89 e5                	mov    %esp,%ebp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  800f3b:	8b 45 08             	mov    0x8(%ebp),%eax
  800f3e:	6a 00                	push   $0x0
  800f40:	6a 00                	push   $0x0
  800f42:	6a 00                	push   $0x0
  800f44:	ff 75 0c             	pushl  0xc(%ebp)
  800f47:	50                   	push   %eax
  800f48:	6a 0b                	push   $0xb
  800f4a:	e8 85 fe ff ff       	call   800dd4 <syscall>
  800f4f:	83 c4 18             	add    $0x18,%esp
	return;
  800f52:	90                   	nop
}
  800f53:	c9                   	leave  
  800f54:	c3                   	ret    
  800f55:	66 90                	xchg   %ax,%ax
  800f57:	66 90                	xchg   %ax,%ax
  800f59:	66 90                	xchg   %ax,%ax
  800f5b:	66 90                	xchg   %ax,%ax
  800f5d:	66 90                	xchg   %ax,%ax
  800f5f:	90                   	nop

00800f60 <__udivdi3>:
  800f60:	55                   	push   %ebp
  800f61:	57                   	push   %edi
  800f62:	56                   	push   %esi
  800f63:	53                   	push   %ebx
  800f64:	83 ec 1c             	sub    $0x1c,%esp
  800f67:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800f6b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800f6f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800f73:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800f77:	85 f6                	test   %esi,%esi
  800f79:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800f7d:	89 ca                	mov    %ecx,%edx
  800f7f:	89 f8                	mov    %edi,%eax
  800f81:	75 3d                	jne    800fc0 <__udivdi3+0x60>
  800f83:	39 cf                	cmp    %ecx,%edi
  800f85:	0f 87 c5 00 00 00    	ja     801050 <__udivdi3+0xf0>
  800f8b:	85 ff                	test   %edi,%edi
  800f8d:	89 fd                	mov    %edi,%ebp
  800f8f:	75 0b                	jne    800f9c <__udivdi3+0x3c>
  800f91:	b8 01 00 00 00       	mov    $0x1,%eax
  800f96:	31 d2                	xor    %edx,%edx
  800f98:	f7 f7                	div    %edi
  800f9a:	89 c5                	mov    %eax,%ebp
  800f9c:	89 c8                	mov    %ecx,%eax
  800f9e:	31 d2                	xor    %edx,%edx
  800fa0:	f7 f5                	div    %ebp
  800fa2:	89 c1                	mov    %eax,%ecx
  800fa4:	89 d8                	mov    %ebx,%eax
  800fa6:	89 cf                	mov    %ecx,%edi
  800fa8:	f7 f5                	div    %ebp
  800faa:	89 c3                	mov    %eax,%ebx
  800fac:	89 d8                	mov    %ebx,%eax
  800fae:	89 fa                	mov    %edi,%edx
  800fb0:	83 c4 1c             	add    $0x1c,%esp
  800fb3:	5b                   	pop    %ebx
  800fb4:	5e                   	pop    %esi
  800fb5:	5f                   	pop    %edi
  800fb6:	5d                   	pop    %ebp
  800fb7:	c3                   	ret    
  800fb8:	90                   	nop
  800fb9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  800fc0:	39 ce                	cmp    %ecx,%esi
  800fc2:	77 74                	ja     801038 <__udivdi3+0xd8>
  800fc4:	0f bd fe             	bsr    %esi,%edi
  800fc7:	83 f7 1f             	xor    $0x1f,%edi
  800fca:	0f 84 98 00 00 00    	je     801068 <__udivdi3+0x108>
  800fd0:	bb 20 00 00 00       	mov    $0x20,%ebx
  800fd5:	89 f9                	mov    %edi,%ecx
  800fd7:	89 c5                	mov    %eax,%ebp
  800fd9:	29 fb                	sub    %edi,%ebx
  800fdb:	d3 e6                	shl    %cl,%esi
  800fdd:	89 d9                	mov    %ebx,%ecx
  800fdf:	d3 ed                	shr    %cl,%ebp
  800fe1:	89 f9                	mov    %edi,%ecx
  800fe3:	d3 e0                	shl    %cl,%eax
  800fe5:	09 ee                	or     %ebp,%esi
  800fe7:	89 d9                	mov    %ebx,%ecx
  800fe9:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800fed:	89 d5                	mov    %edx,%ebp
  800fef:	8b 44 24 08          	mov    0x8(%esp),%eax
  800ff3:	d3 ed                	shr    %cl,%ebp
  800ff5:	89 f9                	mov    %edi,%ecx
  800ff7:	d3 e2                	shl    %cl,%edx
  800ff9:	89 d9                	mov    %ebx,%ecx
  800ffb:	d3 e8                	shr    %cl,%eax
  800ffd:	09 c2                	or     %eax,%edx
  800fff:	89 d0                	mov    %edx,%eax
  801001:	89 ea                	mov    %ebp,%edx
  801003:	f7 f6                	div    %esi
  801005:	89 d5                	mov    %edx,%ebp
  801007:	89 c3                	mov    %eax,%ebx
  801009:	f7 64 24 0c          	mull   0xc(%esp)
  80100d:	39 d5                	cmp    %edx,%ebp
  80100f:	72 10                	jb     801021 <__udivdi3+0xc1>
  801011:	8b 74 24 08          	mov    0x8(%esp),%esi
  801015:	89 f9                	mov    %edi,%ecx
  801017:	d3 e6                	shl    %cl,%esi
  801019:	39 c6                	cmp    %eax,%esi
  80101b:	73 07                	jae    801024 <__udivdi3+0xc4>
  80101d:	39 d5                	cmp    %edx,%ebp
  80101f:	75 03                	jne    801024 <__udivdi3+0xc4>
  801021:	83 eb 01             	sub    $0x1,%ebx
  801024:	31 ff                	xor    %edi,%edi
  801026:	89 d8                	mov    %ebx,%eax
  801028:	89 fa                	mov    %edi,%edx
  80102a:	83 c4 1c             	add    $0x1c,%esp
  80102d:	5b                   	pop    %ebx
  80102e:	5e                   	pop    %esi
  80102f:	5f                   	pop    %edi
  801030:	5d                   	pop    %ebp
  801031:	c3                   	ret    
  801032:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801038:	31 ff                	xor    %edi,%edi
  80103a:	31 db                	xor    %ebx,%ebx
  80103c:	89 d8                	mov    %ebx,%eax
  80103e:	89 fa                	mov    %edi,%edx
  801040:	83 c4 1c             	add    $0x1c,%esp
  801043:	5b                   	pop    %ebx
  801044:	5e                   	pop    %esi
  801045:	5f                   	pop    %edi
  801046:	5d                   	pop    %ebp
  801047:	c3                   	ret    
  801048:	90                   	nop
  801049:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801050:	89 d8                	mov    %ebx,%eax
  801052:	f7 f7                	div    %edi
  801054:	31 ff                	xor    %edi,%edi
  801056:	89 c3                	mov    %eax,%ebx
  801058:	89 d8                	mov    %ebx,%eax
  80105a:	89 fa                	mov    %edi,%edx
  80105c:	83 c4 1c             	add    $0x1c,%esp
  80105f:	5b                   	pop    %ebx
  801060:	5e                   	pop    %esi
  801061:	5f                   	pop    %edi
  801062:	5d                   	pop    %ebp
  801063:	c3                   	ret    
  801064:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801068:	39 ce                	cmp    %ecx,%esi
  80106a:	72 0c                	jb     801078 <__udivdi3+0x118>
  80106c:	31 db                	xor    %ebx,%ebx
  80106e:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801072:	0f 87 34 ff ff ff    	ja     800fac <__udivdi3+0x4c>
  801078:	bb 01 00 00 00       	mov    $0x1,%ebx
  80107d:	e9 2a ff ff ff       	jmp    800fac <__udivdi3+0x4c>
  801082:	66 90                	xchg   %ax,%ax
  801084:	66 90                	xchg   %ax,%ax
  801086:	66 90                	xchg   %ax,%ax
  801088:	66 90                	xchg   %ax,%ax
  80108a:	66 90                	xchg   %ax,%ax
  80108c:	66 90                	xchg   %ax,%ax
  80108e:	66 90                	xchg   %ax,%ax

00801090 <__umoddi3>:
  801090:	55                   	push   %ebp
  801091:	57                   	push   %edi
  801092:	56                   	push   %esi
  801093:	53                   	push   %ebx
  801094:	83 ec 1c             	sub    $0x1c,%esp
  801097:	8b 54 24 3c          	mov    0x3c(%esp),%edx
  80109b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  80109f:	8b 74 24 34          	mov    0x34(%esp),%esi
  8010a3:	8b 7c 24 38          	mov    0x38(%esp),%edi
  8010a7:	85 d2                	test   %edx,%edx
  8010a9:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  8010ad:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8010b1:	89 f3                	mov    %esi,%ebx
  8010b3:	89 3c 24             	mov    %edi,(%esp)
  8010b6:	89 74 24 04          	mov    %esi,0x4(%esp)
  8010ba:	75 1c                	jne    8010d8 <__umoddi3+0x48>
  8010bc:	39 f7                	cmp    %esi,%edi
  8010be:	76 50                	jbe    801110 <__umoddi3+0x80>
  8010c0:	89 c8                	mov    %ecx,%eax
  8010c2:	89 f2                	mov    %esi,%edx
  8010c4:	f7 f7                	div    %edi
  8010c6:	89 d0                	mov    %edx,%eax
  8010c8:	31 d2                	xor    %edx,%edx
  8010ca:	83 c4 1c             	add    $0x1c,%esp
  8010cd:	5b                   	pop    %ebx
  8010ce:	5e                   	pop    %esi
  8010cf:	5f                   	pop    %edi
  8010d0:	5d                   	pop    %ebp
  8010d1:	c3                   	ret    
  8010d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  8010d8:	39 f2                	cmp    %esi,%edx
  8010da:	89 d0                	mov    %edx,%eax
  8010dc:	77 52                	ja     801130 <__umoddi3+0xa0>
  8010de:	0f bd ea             	bsr    %edx,%ebp
  8010e1:	83 f5 1f             	xor    $0x1f,%ebp
  8010e4:	75 5a                	jne    801140 <__umoddi3+0xb0>
  8010e6:	3b 54 24 04          	cmp    0x4(%esp),%edx
  8010ea:	0f 82 e0 00 00 00    	jb     8011d0 <__umoddi3+0x140>
  8010f0:	39 0c 24             	cmp    %ecx,(%esp)
  8010f3:	0f 86 d7 00 00 00    	jbe    8011d0 <__umoddi3+0x140>
  8010f9:	8b 44 24 08          	mov    0x8(%esp),%eax
  8010fd:	8b 54 24 04          	mov    0x4(%esp),%edx
  801101:	83 c4 1c             	add    $0x1c,%esp
  801104:	5b                   	pop    %ebx
  801105:	5e                   	pop    %esi
  801106:	5f                   	pop    %edi
  801107:	5d                   	pop    %ebp
  801108:	c3                   	ret    
  801109:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801110:	85 ff                	test   %edi,%edi
  801112:	89 fd                	mov    %edi,%ebp
  801114:	75 0b                	jne    801121 <__umoddi3+0x91>
  801116:	b8 01 00 00 00       	mov    $0x1,%eax
  80111b:	31 d2                	xor    %edx,%edx
  80111d:	f7 f7                	div    %edi
  80111f:	89 c5                	mov    %eax,%ebp
  801121:	89 f0                	mov    %esi,%eax
  801123:	31 d2                	xor    %edx,%edx
  801125:	f7 f5                	div    %ebp
  801127:	89 c8                	mov    %ecx,%eax
  801129:	f7 f5                	div    %ebp
  80112b:	89 d0                	mov    %edx,%eax
  80112d:	eb 99                	jmp    8010c8 <__umoddi3+0x38>
  80112f:	90                   	nop
  801130:	89 c8                	mov    %ecx,%eax
  801132:	89 f2                	mov    %esi,%edx
  801134:	83 c4 1c             	add    $0x1c,%esp
  801137:	5b                   	pop    %ebx
  801138:	5e                   	pop    %esi
  801139:	5f                   	pop    %edi
  80113a:	5d                   	pop    %ebp
  80113b:	c3                   	ret    
  80113c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801140:	8b 34 24             	mov    (%esp),%esi
  801143:	bf 20 00 00 00       	mov    $0x20,%edi
  801148:	89 e9                	mov    %ebp,%ecx
  80114a:	29 ef                	sub    %ebp,%edi
  80114c:	d3 e0                	shl    %cl,%eax
  80114e:	89 f9                	mov    %edi,%ecx
  801150:	89 f2                	mov    %esi,%edx
  801152:	d3 ea                	shr    %cl,%edx
  801154:	89 e9                	mov    %ebp,%ecx
  801156:	09 c2                	or     %eax,%edx
  801158:	89 d8                	mov    %ebx,%eax
  80115a:	89 14 24             	mov    %edx,(%esp)
  80115d:	89 f2                	mov    %esi,%edx
  80115f:	d3 e2                	shl    %cl,%edx
  801161:	89 f9                	mov    %edi,%ecx
  801163:	89 54 24 04          	mov    %edx,0x4(%esp)
  801167:	8b 54 24 0c          	mov    0xc(%esp),%edx
  80116b:	d3 e8                	shr    %cl,%eax
  80116d:	89 e9                	mov    %ebp,%ecx
  80116f:	89 c6                	mov    %eax,%esi
  801171:	d3 e3                	shl    %cl,%ebx
  801173:	89 f9                	mov    %edi,%ecx
  801175:	89 d0                	mov    %edx,%eax
  801177:	d3 e8                	shr    %cl,%eax
  801179:	89 e9                	mov    %ebp,%ecx
  80117b:	09 d8                	or     %ebx,%eax
  80117d:	89 d3                	mov    %edx,%ebx
  80117f:	89 f2                	mov    %esi,%edx
  801181:	f7 34 24             	divl   (%esp)
  801184:	89 d6                	mov    %edx,%esi
  801186:	d3 e3                	shl    %cl,%ebx
  801188:	f7 64 24 04          	mull   0x4(%esp)
  80118c:	39 d6                	cmp    %edx,%esi
  80118e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801192:	89 d1                	mov    %edx,%ecx
  801194:	89 c3                	mov    %eax,%ebx
  801196:	72 08                	jb     8011a0 <__umoddi3+0x110>
  801198:	75 11                	jne    8011ab <__umoddi3+0x11b>
  80119a:	39 44 24 08          	cmp    %eax,0x8(%esp)
  80119e:	73 0b                	jae    8011ab <__umoddi3+0x11b>
  8011a0:	2b 44 24 04          	sub    0x4(%esp),%eax
  8011a4:	1b 14 24             	sbb    (%esp),%edx
  8011a7:	89 d1                	mov    %edx,%ecx
  8011a9:	89 c3                	mov    %eax,%ebx
  8011ab:	8b 54 24 08          	mov    0x8(%esp),%edx
  8011af:	29 da                	sub    %ebx,%edx
  8011b1:	19 ce                	sbb    %ecx,%esi
  8011b3:	89 f9                	mov    %edi,%ecx
  8011b5:	89 f0                	mov    %esi,%eax
  8011b7:	d3 e0                	shl    %cl,%eax
  8011b9:	89 e9                	mov    %ebp,%ecx
  8011bb:	d3 ea                	shr    %cl,%edx
  8011bd:	89 e9                	mov    %ebp,%ecx
  8011bf:	d3 ee                	shr    %cl,%esi
  8011c1:	09 d0                	or     %edx,%eax
  8011c3:	89 f2                	mov    %esi,%edx
  8011c5:	83 c4 1c             	add    $0x1c,%esp
  8011c8:	5b                   	pop    %ebx
  8011c9:	5e                   	pop    %esi
  8011ca:	5f                   	pop    %edi
  8011cb:	5d                   	pop    %ebp
  8011cc:	c3                   	ret    
  8011cd:	8d 76 00             	lea    0x0(%esi),%esi
  8011d0:	29 f9                	sub    %edi,%ecx
  8011d2:	19 d6                	sbb    %edx,%esi
  8011d4:	89 74 24 04          	mov    %esi,0x4(%esp)
  8011d8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  8011dc:	e9 18 ff ff ff       	jmp    8010f9 <__umoddi3+0x69>
