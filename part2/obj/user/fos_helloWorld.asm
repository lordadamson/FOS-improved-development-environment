
obj/user/fos_helloWorld:     file format elf32-i386


Disassembly of section .text:

00800020 <_start>:
// starts us running when we are initially loaded into a new environment.
.text
.globl _start
_start:
	// See if we were started with arguments on the stack
	mov $0, %eax
  800020:	b8 00 00 00 00       	mov    $0x0,%eax
	cmpl $USTACKTOP, %esp
  800025:	81 fc 00 e0 bf ee    	cmp    $0xeebfe000,%esp
	jne args_exist
  80002b:	75 04                	jne    800031 <args_exist>

	// If not, push dummy argc/argv arguments.
	// This happens when we are loaded by the kernel,
	// because the kernel does not know about passing arguments.
	pushl $0
  80002d:	6a 00                	push   $0x0
	pushl $0
  80002f:	6a 00                	push   $0x0

00800031 <args_exist>:

args_exist:
	call libmain
  800031:	e8 1b 00 00 00       	call   800051 <libmain>
1:      jmp 1b
  800036:	eb fe                	jmp    800036 <args_exist+0x5>

00800038 <_main>:
// hello, world
#include <inc/lib.h>

void
_main(void)
{	
  800038:	55                   	push   %ebp
  800039:	89 e5                	mov    %esp,%ebp
  80003b:	83 ec 08             	sub    $0x8,%esp
	cprintf("HELLO WORLD , FOS IS SAYING HI :D:D:D\n");	
  80003e:	83 ec 0c             	sub    $0xc,%esp
  800041:	68 a0 11 80 00       	push   $0x8011a0
  800046:	e8 1e 01 00 00       	call   800169 <cprintf>
  80004b:	83 c4 10             	add    $0x10,%esp
}
  80004e:	90                   	nop
  80004f:	c9                   	leave  
  800050:	c3                   	ret    

00800051 <libmain>:
volatile struct Env *env;
char *binaryname = "(PROGRAM NAME UNKNOWN)";

void
libmain(int argc, char **argv)
{
  800051:	55                   	push   %ebp
  800052:	89 e5                	mov    %esp,%ebp
  800054:	83 ec 08             	sub    $0x8,%esp
	// set env to point at our env structure in envs[].
	// LAB 3: Your code here.
	env = envs;
  800057:	c7 05 04 20 80 00 00 	movl   $0xeec00000,0x802004
  80005e:	00 c0 ee 

	// save the name of the program so that panic() can use it
	if (argc > 0)
  800061:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800065:	7e 0a                	jle    800071 <libmain+0x20>
		binaryname = argv[0];
  800067:	8b 45 0c             	mov    0xc(%ebp),%eax
  80006a:	8b 00                	mov    (%eax),%eax
  80006c:	a3 00 20 80 00       	mov    %eax,0x802000

	// call user main routine
	_main(argc, argv);
  800071:	83 ec 08             	sub    $0x8,%esp
  800074:	ff 75 0c             	pushl  0xc(%ebp)
  800077:	ff 75 08             	pushl  0x8(%ebp)
  80007a:	e8 b9 ff ff ff       	call   800038 <_main>
  80007f:	83 c4 10             	add    $0x10,%esp

	// exit gracefully
	//exit();
	sleep();
  800082:	e8 19 00 00 00       	call   8000a0 <sleep>
}
  800087:	90                   	nop
  800088:	c9                   	leave  
  800089:	c3                   	ret    

0080008a <exit>:

#include <inc/lib.h>

void
exit(void)
{
  80008a:	55                   	push   %ebp
  80008b:	89 e5                	mov    %esp,%ebp
  80008d:	83 ec 08             	sub    $0x8,%esp
	sys_env_destroy(0);	
  800090:	83 ec 0c             	sub    $0xc,%esp
  800093:	6a 00                	push   $0x0
  800095:	e8 56 0d 00 00       	call   800df0 <sys_env_destroy>
  80009a:	83 c4 10             	add    $0x10,%esp
}
  80009d:	90                   	nop
  80009e:	c9                   	leave  
  80009f:	c3                   	ret    

008000a0 <sleep>:

void
sleep(void)
{	
  8000a0:	55                   	push   %ebp
  8000a1:	89 e5                	mov    %esp,%ebp
  8000a3:	83 ec 08             	sub    $0x8,%esp
	sys_env_sleep();
  8000a6:	e8 79 0d 00 00       	call   800e24 <sys_env_sleep>
}
  8000ab:	90                   	nop
  8000ac:	c9                   	leave  
  8000ad:	c3                   	ret    

008000ae <putch>:
};


static void
putch(int ch, struct printbuf *b)
{
  8000ae:	55                   	push   %ebp
  8000af:	89 e5                	mov    %esp,%ebp
  8000b1:	83 ec 08             	sub    $0x8,%esp
	b->buf[b->idx++] = ch;
  8000b4:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000b7:	8b 00                	mov    (%eax),%eax
  8000b9:	8d 48 01             	lea    0x1(%eax),%ecx
  8000bc:	8b 55 0c             	mov    0xc(%ebp),%edx
  8000bf:	89 0a                	mov    %ecx,(%edx)
  8000c1:	8b 55 08             	mov    0x8(%ebp),%edx
  8000c4:	89 d1                	mov    %edx,%ecx
  8000c6:	8b 55 0c             	mov    0xc(%ebp),%edx
  8000c9:	88 4c 02 08          	mov    %cl,0x8(%edx,%eax,1)
	if (b->idx == 256-1) {
  8000cd:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000d0:	8b 00                	mov    (%eax),%eax
  8000d2:	3d ff 00 00 00       	cmp    $0xff,%eax
  8000d7:	75 23                	jne    8000fc <putch+0x4e>
		sys_cputs(b->buf, b->idx);
  8000d9:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000dc:	8b 00                	mov    (%eax),%eax
  8000de:	89 c2                	mov    %eax,%edx
  8000e0:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000e3:	83 c0 08             	add    $0x8,%eax
  8000e6:	83 ec 08             	sub    $0x8,%esp
  8000e9:	52                   	push   %edx
  8000ea:	50                   	push   %eax
  8000eb:	e8 ca 0c 00 00       	call   800dba <sys_cputs>
  8000f0:	83 c4 10             	add    $0x10,%esp
		b->idx = 0;
  8000f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000f6:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	}
	b->cnt++;
  8000fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  8000ff:	8b 40 04             	mov    0x4(%eax),%eax
  800102:	8d 50 01             	lea    0x1(%eax),%edx
  800105:	8b 45 0c             	mov    0xc(%ebp),%eax
  800108:	89 50 04             	mov    %edx,0x4(%eax)
}
  80010b:	90                   	nop
  80010c:	c9                   	leave  
  80010d:	c3                   	ret    

0080010e <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
  80010e:	55                   	push   %ebp
  80010f:	89 e5                	mov    %esp,%ebp
  800111:	81 ec 18 01 00 00    	sub    $0x118,%esp
	struct printbuf b;

	b.idx = 0;
  800117:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
  80011e:	00 00 00 
	b.cnt = 0;
  800121:	c7 85 f4 fe ff ff 00 	movl   $0x0,-0x10c(%ebp)
  800128:	00 00 00 
	vprintfmt((void*)putch, &b, fmt, ap);
  80012b:	ff 75 0c             	pushl  0xc(%ebp)
  80012e:	ff 75 08             	pushl  0x8(%ebp)
  800131:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800137:	50                   	push   %eax
  800138:	68 ae 00 80 00       	push   $0x8000ae
  80013d:	e8 cc 01 00 00       	call   80030e <vprintfmt>
  800142:	83 c4 10             	add    $0x10,%esp
	sys_cputs(b.buf, b.idx);
  800145:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
  80014b:	83 ec 08             	sub    $0x8,%esp
  80014e:	50                   	push   %eax
  80014f:	8d 85 f0 fe ff ff    	lea    -0x110(%ebp),%eax
  800155:	83 c0 08             	add    $0x8,%eax
  800158:	50                   	push   %eax
  800159:	e8 5c 0c 00 00       	call   800dba <sys_cputs>
  80015e:	83 c4 10             	add    $0x10,%esp

	return b.cnt;
  800161:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
}
  800167:	c9                   	leave  
  800168:	c3                   	ret    

00800169 <cprintf>:

int
cprintf(const char *fmt, ...)
{
  800169:	55                   	push   %ebp
  80016a:	89 e5                	mov    %esp,%ebp
  80016c:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
  80016f:	8d 45 0c             	lea    0xc(%ebp),%eax
  800172:	89 45 f4             	mov    %eax,-0xc(%ebp)
	cnt = vcprintf(fmt, ap);
  800175:	8b 45 08             	mov    0x8(%ebp),%eax
  800178:	83 ec 08             	sub    $0x8,%esp
  80017b:	ff 75 f4             	pushl  -0xc(%ebp)
  80017e:	50                   	push   %eax
  80017f:	e8 8a ff ff ff       	call   80010e <vcprintf>
  800184:	83 c4 10             	add    $0x10,%esp
  800187:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return cnt;
  80018a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  80018d:	c9                   	leave  
  80018e:	c3                   	ret    

0080018f <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
  80018f:	55                   	push   %ebp
  800190:	89 e5                	mov    %esp,%ebp
  800192:	53                   	push   %ebx
  800193:	83 ec 14             	sub    $0x14,%esp
  800196:	8b 45 10             	mov    0x10(%ebp),%eax
  800199:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80019c:	8b 45 14             	mov    0x14(%ebp),%eax
  80019f:	89 45 f4             	mov    %eax,-0xc(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
  8001a2:	8b 45 18             	mov    0x18(%ebp),%eax
  8001a5:	ba 00 00 00 00       	mov    $0x0,%edx
  8001aa:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001ad:	77 55                	ja     800204 <printnum+0x75>
  8001af:	3b 55 f4             	cmp    -0xc(%ebp),%edx
  8001b2:	72 05                	jb     8001b9 <printnum+0x2a>
  8001b4:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  8001b7:	77 4b                	ja     800204 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
  8001b9:	8b 45 1c             	mov    0x1c(%ebp),%eax
  8001bc:	8d 58 ff             	lea    -0x1(%eax),%ebx
  8001bf:	8b 45 18             	mov    0x18(%ebp),%eax
  8001c2:	ba 00 00 00 00       	mov    $0x0,%edx
  8001c7:	52                   	push   %edx
  8001c8:	50                   	push   %eax
  8001c9:	ff 75 f4             	pushl  -0xc(%ebp)
  8001cc:	ff 75 f0             	pushl  -0x10(%ebp)
  8001cf:	e8 3c 0d 00 00       	call   800f10 <__udivdi3>
  8001d4:	83 c4 10             	add    $0x10,%esp
  8001d7:	83 ec 04             	sub    $0x4,%esp
  8001da:	ff 75 20             	pushl  0x20(%ebp)
  8001dd:	53                   	push   %ebx
  8001de:	ff 75 18             	pushl  0x18(%ebp)
  8001e1:	52                   	push   %edx
  8001e2:	50                   	push   %eax
  8001e3:	ff 75 0c             	pushl  0xc(%ebp)
  8001e6:	ff 75 08             	pushl  0x8(%ebp)
  8001e9:	e8 a1 ff ff ff       	call   80018f <printnum>
  8001ee:	83 c4 20             	add    $0x20,%esp
  8001f1:	eb 1b                	jmp    80020e <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
  8001f3:	83 ec 08             	sub    $0x8,%esp
  8001f6:	ff 75 0c             	pushl  0xc(%ebp)
  8001f9:	ff 75 20             	pushl  0x20(%ebp)
  8001fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8001ff:	ff d0                	call   *%eax
  800201:	83 c4 10             	add    $0x10,%esp
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
  800204:	83 6d 1c 01          	subl   $0x1,0x1c(%ebp)
  800208:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  80020c:	7f e5                	jg     8001f3 <printnum+0x64>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
  80020e:	8b 4d 18             	mov    0x18(%ebp),%ecx
  800211:	bb 00 00 00 00       	mov    $0x0,%ebx
  800216:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800219:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80021c:	53                   	push   %ebx
  80021d:	51                   	push   %ecx
  80021e:	52                   	push   %edx
  80021f:	50                   	push   %eax
  800220:	e8 1b 0e 00 00       	call   801040 <__umoddi3>
  800225:	83 c4 10             	add    $0x10,%esp
  800228:	05 80 12 80 00       	add    $0x801280,%eax
  80022d:	0f b6 00             	movzbl (%eax),%eax
  800230:	0f be c0             	movsbl %al,%eax
  800233:	83 ec 08             	sub    $0x8,%esp
  800236:	ff 75 0c             	pushl  0xc(%ebp)
  800239:	50                   	push   %eax
  80023a:	8b 45 08             	mov    0x8(%ebp),%eax
  80023d:	ff d0                	call   *%eax
  80023f:	83 c4 10             	add    $0x10,%esp
}
  800242:	90                   	nop
  800243:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  800246:	c9                   	leave  
  800247:	c3                   	ret    

00800248 <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
  800248:	55                   	push   %ebp
  800249:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  80024b:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  80024f:	7e 1c                	jle    80026d <getuint+0x25>
		return va_arg(*ap, unsigned long long);
  800251:	8b 45 08             	mov    0x8(%ebp),%eax
  800254:	8b 00                	mov    (%eax),%eax
  800256:	8d 50 08             	lea    0x8(%eax),%edx
  800259:	8b 45 08             	mov    0x8(%ebp),%eax
  80025c:	89 10                	mov    %edx,(%eax)
  80025e:	8b 45 08             	mov    0x8(%ebp),%eax
  800261:	8b 00                	mov    (%eax),%eax
  800263:	83 e8 08             	sub    $0x8,%eax
  800266:	8b 50 04             	mov    0x4(%eax),%edx
  800269:	8b 00                	mov    (%eax),%eax
  80026b:	eb 40                	jmp    8002ad <getuint+0x65>
	else if (lflag)
  80026d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800271:	74 1e                	je     800291 <getuint+0x49>
		return va_arg(*ap, unsigned long);
  800273:	8b 45 08             	mov    0x8(%ebp),%eax
  800276:	8b 00                	mov    (%eax),%eax
  800278:	8d 50 04             	lea    0x4(%eax),%edx
  80027b:	8b 45 08             	mov    0x8(%ebp),%eax
  80027e:	89 10                	mov    %edx,(%eax)
  800280:	8b 45 08             	mov    0x8(%ebp),%eax
  800283:	8b 00                	mov    (%eax),%eax
  800285:	83 e8 04             	sub    $0x4,%eax
  800288:	8b 00                	mov    (%eax),%eax
  80028a:	ba 00 00 00 00       	mov    $0x0,%edx
  80028f:	eb 1c                	jmp    8002ad <getuint+0x65>
	else
		return va_arg(*ap, unsigned int);
  800291:	8b 45 08             	mov    0x8(%ebp),%eax
  800294:	8b 00                	mov    (%eax),%eax
  800296:	8d 50 04             	lea    0x4(%eax),%edx
  800299:	8b 45 08             	mov    0x8(%ebp),%eax
  80029c:	89 10                	mov    %edx,(%eax)
  80029e:	8b 45 08             	mov    0x8(%ebp),%eax
  8002a1:	8b 00                	mov    (%eax),%eax
  8002a3:	83 e8 04             	sub    $0x4,%eax
  8002a6:	8b 00                	mov    (%eax),%eax
  8002a8:	ba 00 00 00 00       	mov    $0x0,%edx
}
  8002ad:	5d                   	pop    %ebp
  8002ae:	c3                   	ret    

008002af <getint>:

// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
  8002af:	55                   	push   %ebp
  8002b0:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
  8002b2:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  8002b6:	7e 1c                	jle    8002d4 <getint+0x25>
		return va_arg(*ap, long long);
  8002b8:	8b 45 08             	mov    0x8(%ebp),%eax
  8002bb:	8b 00                	mov    (%eax),%eax
  8002bd:	8d 50 08             	lea    0x8(%eax),%edx
  8002c0:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c3:	89 10                	mov    %edx,(%eax)
  8002c5:	8b 45 08             	mov    0x8(%ebp),%eax
  8002c8:	8b 00                	mov    (%eax),%eax
  8002ca:	83 e8 08             	sub    $0x8,%eax
  8002cd:	8b 50 04             	mov    0x4(%eax),%edx
  8002d0:	8b 00                	mov    (%eax),%eax
  8002d2:	eb 38                	jmp    80030c <getint+0x5d>
	else if (lflag)
  8002d4:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  8002d8:	74 1a                	je     8002f4 <getint+0x45>
		return va_arg(*ap, long);
  8002da:	8b 45 08             	mov    0x8(%ebp),%eax
  8002dd:	8b 00                	mov    (%eax),%eax
  8002df:	8d 50 04             	lea    0x4(%eax),%edx
  8002e2:	8b 45 08             	mov    0x8(%ebp),%eax
  8002e5:	89 10                	mov    %edx,(%eax)
  8002e7:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ea:	8b 00                	mov    (%eax),%eax
  8002ec:	83 e8 04             	sub    $0x4,%eax
  8002ef:	8b 00                	mov    (%eax),%eax
  8002f1:	99                   	cltd   
  8002f2:	eb 18                	jmp    80030c <getint+0x5d>
	else
		return va_arg(*ap, int);
  8002f4:	8b 45 08             	mov    0x8(%ebp),%eax
  8002f7:	8b 00                	mov    (%eax),%eax
  8002f9:	8d 50 04             	lea    0x4(%eax),%edx
  8002fc:	8b 45 08             	mov    0x8(%ebp),%eax
  8002ff:	89 10                	mov    %edx,(%eax)
  800301:	8b 45 08             	mov    0x8(%ebp),%eax
  800304:	8b 00                	mov    (%eax),%eax
  800306:	83 e8 04             	sub    $0x4,%eax
  800309:	8b 00                	mov    (%eax),%eax
  80030b:	99                   	cltd   
}
  80030c:	5d                   	pop    %ebp
  80030d:	c3                   	ret    

0080030e <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
  80030e:	55                   	push   %ebp
  80030f:	89 e5                	mov    %esp,%ebp
  800311:	56                   	push   %esi
  800312:	53                   	push   %ebx
  800313:	83 ec 20             	sub    $0x20,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  800316:	eb 17                	jmp    80032f <vprintfmt+0x21>
			if (ch == '\0')
  800318:	85 db                	test   %ebx,%ebx
  80031a:	0f 84 be 03 00 00    	je     8006de <vprintfmt+0x3d0>
				return;
			putch(ch, putdat);
  800320:	83 ec 08             	sub    $0x8,%esp
  800323:	ff 75 0c             	pushl  0xc(%ebp)
  800326:	53                   	push   %ebx
  800327:	8b 45 08             	mov    0x8(%ebp),%eax
  80032a:	ff d0                	call   *%eax
  80032c:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
  80032f:	8b 45 10             	mov    0x10(%ebp),%eax
  800332:	8d 50 01             	lea    0x1(%eax),%edx
  800335:	89 55 10             	mov    %edx,0x10(%ebp)
  800338:	0f b6 00             	movzbl (%eax),%eax
  80033b:	0f b6 d8             	movzbl %al,%ebx
  80033e:	83 fb 25             	cmp    $0x25,%ebx
  800341:	75 d5                	jne    800318 <vprintfmt+0xa>
				return;
			putch(ch, putdat);
		}

		// Process a %-escape sequence
		padc = ' ';
  800343:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
		width = -1;
  800347:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
		precision = -1;
  80034e:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
		lflag = 0;
  800355:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
		altflag = 0;
  80035c:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
  800363:	8b 45 10             	mov    0x10(%ebp),%eax
  800366:	8d 50 01             	lea    0x1(%eax),%edx
  800369:	89 55 10             	mov    %edx,0x10(%ebp)
  80036c:	0f b6 00             	movzbl (%eax),%eax
  80036f:	0f b6 d8             	movzbl %al,%ebx
  800372:	8d 43 dd             	lea    -0x23(%ebx),%eax
  800375:	83 f8 55             	cmp    $0x55,%eax
  800378:	0f 87 33 03 00 00    	ja     8006b1 <vprintfmt+0x3a3>
  80037e:	8b 04 85 a4 12 80 00 	mov    0x8012a4(,%eax,4),%eax
  800385:	ff e0                	jmp    *%eax

		// flag to pad on the right
		case '-':
			padc = '-';
  800387:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
			goto reswitch;
  80038b:	eb d6                	jmp    800363 <vprintfmt+0x55>
			
		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
  80038d:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
			goto reswitch;
  800391:	eb d0                	jmp    800363 <vprintfmt+0x55>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  800393:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
				precision = precision * 10 + ch - '0';
  80039a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  80039d:	89 d0                	mov    %edx,%eax
  80039f:	c1 e0 02             	shl    $0x2,%eax
  8003a2:	01 d0                	add    %edx,%eax
  8003a4:	01 c0                	add    %eax,%eax
  8003a6:	01 d8                	add    %ebx,%eax
  8003a8:	83 e8 30             	sub    $0x30,%eax
  8003ab:	89 45 e0             	mov    %eax,-0x20(%ebp)
				ch = *fmt;
  8003ae:	8b 45 10             	mov    0x10(%ebp),%eax
  8003b1:	0f b6 00             	movzbl (%eax),%eax
  8003b4:	0f be d8             	movsbl %al,%ebx
				if (ch < '0' || ch > '9')
  8003b7:	83 fb 2f             	cmp    $0x2f,%ebx
  8003ba:	7e 3f                	jle    8003fb <vprintfmt+0xed>
  8003bc:	83 fb 39             	cmp    $0x39,%ebx
  8003bf:	7f 3a                	jg     8003fb <vprintfmt+0xed>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
  8003c1:	83 45 10 01          	addl   $0x1,0x10(%ebp)
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
  8003c5:	eb d3                	jmp    80039a <vprintfmt+0x8c>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
  8003c7:	8b 45 14             	mov    0x14(%ebp),%eax
  8003ca:	83 c0 04             	add    $0x4,%eax
  8003cd:	89 45 14             	mov    %eax,0x14(%ebp)
  8003d0:	8b 45 14             	mov    0x14(%ebp),%eax
  8003d3:	83 e8 04             	sub    $0x4,%eax
  8003d6:	8b 00                	mov    (%eax),%eax
  8003d8:	89 45 e0             	mov    %eax,-0x20(%ebp)
			goto process_precision;
  8003db:	eb 1f                	jmp    8003fc <vprintfmt+0xee>

		case '.':
			if (width < 0)
  8003dd:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8003e1:	79 80                	jns    800363 <vprintfmt+0x55>
				width = 0;
  8003e3:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
			goto reswitch;
  8003ea:	e9 74 ff ff ff       	jmp    800363 <vprintfmt+0x55>

		case '#':
			altflag = 1;
  8003ef:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
			goto reswitch;
  8003f6:	e9 68 ff ff ff       	jmp    800363 <vprintfmt+0x55>
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
			goto process_precision;
  8003fb:	90                   	nop
		case '#':
			altflag = 1;
			goto reswitch;

		process_precision:
			if (width < 0)
  8003fc:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800400:	0f 89 5d ff ff ff    	jns    800363 <vprintfmt+0x55>
				width = precision, precision = -1;
  800406:	8b 45 e0             	mov    -0x20(%ebp),%eax
  800409:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  80040c:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
			goto reswitch;
  800413:	e9 4b ff ff ff       	jmp    800363 <vprintfmt+0x55>

		// long flag (doubled for long long)
		case 'l':
			lflag++;
  800418:	83 45 e8 01          	addl   $0x1,-0x18(%ebp)
			goto reswitch;
  80041c:	e9 42 ff ff ff       	jmp    800363 <vprintfmt+0x55>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
  800421:	8b 45 14             	mov    0x14(%ebp),%eax
  800424:	83 c0 04             	add    $0x4,%eax
  800427:	89 45 14             	mov    %eax,0x14(%ebp)
  80042a:	8b 45 14             	mov    0x14(%ebp),%eax
  80042d:	83 e8 04             	sub    $0x4,%eax
  800430:	8b 00                	mov    (%eax),%eax
  800432:	83 ec 08             	sub    $0x8,%esp
  800435:	ff 75 0c             	pushl  0xc(%ebp)
  800438:	50                   	push   %eax
  800439:	8b 45 08             	mov    0x8(%ebp),%eax
  80043c:	ff d0                	call   *%eax
  80043e:	83 c4 10             	add    $0x10,%esp
			break;
  800441:	e9 93 02 00 00       	jmp    8006d9 <vprintfmt+0x3cb>

		// error message
		case 'e':
			err = va_arg(ap, int);
  800446:	8b 45 14             	mov    0x14(%ebp),%eax
  800449:	83 c0 04             	add    $0x4,%eax
  80044c:	89 45 14             	mov    %eax,0x14(%ebp)
  80044f:	8b 45 14             	mov    0x14(%ebp),%eax
  800452:	83 e8 04             	sub    $0x4,%eax
  800455:	8b 18                	mov    (%eax),%ebx
			if (err < 0)
  800457:	85 db                	test   %ebx,%ebx
  800459:	79 02                	jns    80045d <vprintfmt+0x14f>
				err = -err;
  80045b:	f7 db                	neg    %ebx
			if (err > MAXERROR || (p = error_string[err]) == NULL)
  80045d:	83 fb 07             	cmp    $0x7,%ebx
  800460:	7f 0b                	jg     80046d <vprintfmt+0x15f>
  800462:	8b 34 9d 60 12 80 00 	mov    0x801260(,%ebx,4),%esi
  800469:	85 f6                	test   %esi,%esi
  80046b:	75 19                	jne    800486 <vprintfmt+0x178>
				printfmt(putch, putdat, "error %d", err);
  80046d:	53                   	push   %ebx
  80046e:	68 91 12 80 00       	push   $0x801291
  800473:	ff 75 0c             	pushl  0xc(%ebp)
  800476:	ff 75 08             	pushl  0x8(%ebp)
  800479:	e8 68 02 00 00       	call   8006e6 <printfmt>
  80047e:	83 c4 10             	add    $0x10,%esp
			else
				printfmt(putch, putdat, "%s", p);
			break;
  800481:	e9 53 02 00 00       	jmp    8006d9 <vprintfmt+0x3cb>
			if (err < 0)
				err = -err;
			if (err > MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
			else
				printfmt(putch, putdat, "%s", p);
  800486:	56                   	push   %esi
  800487:	68 9a 12 80 00       	push   $0x80129a
  80048c:	ff 75 0c             	pushl  0xc(%ebp)
  80048f:	ff 75 08             	pushl  0x8(%ebp)
  800492:	e8 4f 02 00 00       	call   8006e6 <printfmt>
  800497:	83 c4 10             	add    $0x10,%esp
			break;
  80049a:	e9 3a 02 00 00       	jmp    8006d9 <vprintfmt+0x3cb>

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
  80049f:	8b 45 14             	mov    0x14(%ebp),%eax
  8004a2:	83 c0 04             	add    $0x4,%eax
  8004a5:	89 45 14             	mov    %eax,0x14(%ebp)
  8004a8:	8b 45 14             	mov    0x14(%ebp),%eax
  8004ab:	83 e8 04             	sub    $0x4,%eax
  8004ae:	8b 30                	mov    (%eax),%esi
  8004b0:	85 f6                	test   %esi,%esi
  8004b2:	75 05                	jne    8004b9 <vprintfmt+0x1ab>
				p = "(null)";
  8004b4:	be 9d 12 80 00       	mov    $0x80129d,%esi
			if (width > 0 && padc != '-')
  8004b9:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004bd:	7e 6f                	jle    80052e <vprintfmt+0x220>
  8004bf:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  8004c3:	74 69                	je     80052e <vprintfmt+0x220>
				for (width -= strnlen(p, precision); width > 0; width--)
  8004c5:	8b 45 e0             	mov    -0x20(%ebp),%eax
  8004c8:	83 ec 08             	sub    $0x8,%esp
  8004cb:	50                   	push   %eax
  8004cc:	56                   	push   %esi
  8004cd:	e8 19 03 00 00       	call   8007eb <strnlen>
  8004d2:	83 c4 10             	add    $0x10,%esp
  8004d5:	29 45 e4             	sub    %eax,-0x1c(%ebp)
  8004d8:	eb 17                	jmp    8004f1 <vprintfmt+0x1e3>
					putch(padc, putdat);
  8004da:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  8004de:	83 ec 08             	sub    $0x8,%esp
  8004e1:	ff 75 0c             	pushl  0xc(%ebp)
  8004e4:	50                   	push   %eax
  8004e5:	8b 45 08             	mov    0x8(%ebp),%eax
  8004e8:	ff d0                	call   *%eax
  8004ea:	83 c4 10             	add    $0x10,%esp
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
  8004ed:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  8004f1:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  8004f5:	7f e3                	jg     8004da <vprintfmt+0x1cc>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  8004f7:	eb 35                	jmp    80052e <vprintfmt+0x220>
				if (altflag && (ch < ' ' || ch > '~'))
  8004f9:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  8004fd:	74 1c                	je     80051b <vprintfmt+0x20d>
  8004ff:	83 fb 1f             	cmp    $0x1f,%ebx
  800502:	7e 05                	jle    800509 <vprintfmt+0x1fb>
  800504:	83 fb 7e             	cmp    $0x7e,%ebx
  800507:	7e 12                	jle    80051b <vprintfmt+0x20d>
					putch('?', putdat);
  800509:	83 ec 08             	sub    $0x8,%esp
  80050c:	ff 75 0c             	pushl  0xc(%ebp)
  80050f:	6a 3f                	push   $0x3f
  800511:	8b 45 08             	mov    0x8(%ebp),%eax
  800514:	ff d0                	call   *%eax
  800516:	83 c4 10             	add    $0x10,%esp
  800519:	eb 0f                	jmp    80052a <vprintfmt+0x21c>
				else
					putch(ch, putdat);
  80051b:	83 ec 08             	sub    $0x8,%esp
  80051e:	ff 75 0c             	pushl  0xc(%ebp)
  800521:	53                   	push   %ebx
  800522:	8b 45 08             	mov    0x8(%ebp),%eax
  800525:	ff d0                	call   *%eax
  800527:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
  80052a:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  80052e:	89 f0                	mov    %esi,%eax
  800530:	8d 70 01             	lea    0x1(%eax),%esi
  800533:	0f b6 00             	movzbl (%eax),%eax
  800536:	0f be d8             	movsbl %al,%ebx
  800539:	85 db                	test   %ebx,%ebx
  80053b:	74 26                	je     800563 <vprintfmt+0x255>
  80053d:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  800541:	78 b6                	js     8004f9 <vprintfmt+0x1eb>
  800543:	83 6d e0 01          	subl   $0x1,-0x20(%ebp)
  800547:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  80054b:	79 ac                	jns    8004f9 <vprintfmt+0x1eb>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80054d:	eb 14                	jmp    800563 <vprintfmt+0x255>
				putch(' ', putdat);
  80054f:	83 ec 08             	sub    $0x8,%esp
  800552:	ff 75 0c             	pushl  0xc(%ebp)
  800555:	6a 20                	push   $0x20
  800557:	8b 45 08             	mov    0x8(%ebp),%eax
  80055a:	ff d0                	call   *%eax
  80055c:	83 c4 10             	add    $0x10,%esp
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
  80055f:	83 6d e4 01          	subl   $0x1,-0x1c(%ebp)
  800563:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  800567:	7f e6                	jg     80054f <vprintfmt+0x241>
				putch(' ', putdat);
			break;
  800569:	e9 6b 01 00 00       	jmp    8006d9 <vprintfmt+0x3cb>

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
  80056e:	83 ec 08             	sub    $0x8,%esp
  800571:	ff 75 e8             	pushl  -0x18(%ebp)
  800574:	8d 45 14             	lea    0x14(%ebp),%eax
  800577:	50                   	push   %eax
  800578:	e8 32 fd ff ff       	call   8002af <getint>
  80057d:	83 c4 10             	add    $0x10,%esp
  800580:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800583:	89 55 f4             	mov    %edx,-0xc(%ebp)
			if ((long long) num < 0) {
  800586:	8b 45 f0             	mov    -0x10(%ebp),%eax
  800589:	8b 55 f4             	mov    -0xc(%ebp),%edx
  80058c:	85 d2                	test   %edx,%edx
  80058e:	79 23                	jns    8005b3 <vprintfmt+0x2a5>
				putch('-', putdat);
  800590:	83 ec 08             	sub    $0x8,%esp
  800593:	ff 75 0c             	pushl  0xc(%ebp)
  800596:	6a 2d                	push   $0x2d
  800598:	8b 45 08             	mov    0x8(%ebp),%eax
  80059b:	ff d0                	call   *%eax
  80059d:	83 c4 10             	add    $0x10,%esp
				num = -(long long) num;
  8005a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  8005a3:	8b 55 f4             	mov    -0xc(%ebp),%edx
  8005a6:	f7 d8                	neg    %eax
  8005a8:	83 d2 00             	adc    $0x0,%edx
  8005ab:	f7 da                	neg    %edx
  8005ad:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005b0:	89 55 f4             	mov    %edx,-0xc(%ebp)
			}
			base = 10;
  8005b3:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8005ba:	e9 bc 00 00 00       	jmp    80067b <vprintfmt+0x36d>

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
  8005bf:	83 ec 08             	sub    $0x8,%esp
  8005c2:	ff 75 e8             	pushl  -0x18(%ebp)
  8005c5:	8d 45 14             	lea    0x14(%ebp),%eax
  8005c8:	50                   	push   %eax
  8005c9:	e8 7a fc ff ff       	call   800248 <getuint>
  8005ce:	83 c4 10             	add    $0x10,%esp
  8005d1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  8005d4:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 10;
  8005d7:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
			goto number;
  8005de:	e9 98 00 00 00       	jmp    80067b <vprintfmt+0x36d>

		// (unsigned) octal
		case 'o':
			// Replace this with your code.
			putch('X', putdat);
  8005e3:	83 ec 08             	sub    $0x8,%esp
  8005e6:	ff 75 0c             	pushl  0xc(%ebp)
  8005e9:	6a 58                	push   $0x58
  8005eb:	8b 45 08             	mov    0x8(%ebp),%eax
  8005ee:	ff d0                	call   *%eax
  8005f0:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  8005f3:	83 ec 08             	sub    $0x8,%esp
  8005f6:	ff 75 0c             	pushl  0xc(%ebp)
  8005f9:	6a 58                	push   $0x58
  8005fb:	8b 45 08             	mov    0x8(%ebp),%eax
  8005fe:	ff d0                	call   *%eax
  800600:	83 c4 10             	add    $0x10,%esp
			putch('X', putdat);
  800603:	83 ec 08             	sub    $0x8,%esp
  800606:	ff 75 0c             	pushl  0xc(%ebp)
  800609:	6a 58                	push   $0x58
  80060b:	8b 45 08             	mov    0x8(%ebp),%eax
  80060e:	ff d0                	call   *%eax
  800610:	83 c4 10             	add    $0x10,%esp
			break;
  800613:	e9 c1 00 00 00       	jmp    8006d9 <vprintfmt+0x3cb>

		// pointer
		case 'p':
			putch('0', putdat);
  800618:	83 ec 08             	sub    $0x8,%esp
  80061b:	ff 75 0c             	pushl  0xc(%ebp)
  80061e:	6a 30                	push   $0x30
  800620:	8b 45 08             	mov    0x8(%ebp),%eax
  800623:	ff d0                	call   *%eax
  800625:	83 c4 10             	add    $0x10,%esp
			putch('x', putdat);
  800628:	83 ec 08             	sub    $0x8,%esp
  80062b:	ff 75 0c             	pushl  0xc(%ebp)
  80062e:	6a 78                	push   $0x78
  800630:	8b 45 08             	mov    0x8(%ebp),%eax
  800633:	ff d0                	call   *%eax
  800635:	83 c4 10             	add    $0x10,%esp
			num = (unsigned long long)
				(uint32) va_arg(ap, void *);
  800638:	8b 45 14             	mov    0x14(%ebp),%eax
  80063b:	83 c0 04             	add    $0x4,%eax
  80063e:	89 45 14             	mov    %eax,0x14(%ebp)
  800641:	8b 45 14             	mov    0x14(%ebp),%eax
  800644:	83 e8 04             	sub    $0x4,%eax
  800647:	8b 00                	mov    (%eax),%eax

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
  800649:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80064c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
				(uint32) va_arg(ap, void *);
			base = 16;
  800653:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
			goto number;
  80065a:	eb 1f                	jmp    80067b <vprintfmt+0x36d>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
  80065c:	83 ec 08             	sub    $0x8,%esp
  80065f:	ff 75 e8             	pushl  -0x18(%ebp)
  800662:	8d 45 14             	lea    0x14(%ebp),%eax
  800665:	50                   	push   %eax
  800666:	e8 dd fb ff ff       	call   800248 <getuint>
  80066b:	83 c4 10             	add    $0x10,%esp
  80066e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  800671:	89 55 f4             	mov    %edx,-0xc(%ebp)
			base = 16;
  800674:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
		number:
			printnum(putch, putdat, num, base, width, padc);
  80067b:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  80067f:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800682:	83 ec 04             	sub    $0x4,%esp
  800685:	52                   	push   %edx
  800686:	ff 75 e4             	pushl  -0x1c(%ebp)
  800689:	50                   	push   %eax
  80068a:	ff 75 f4             	pushl  -0xc(%ebp)
  80068d:	ff 75 f0             	pushl  -0x10(%ebp)
  800690:	ff 75 0c             	pushl  0xc(%ebp)
  800693:	ff 75 08             	pushl  0x8(%ebp)
  800696:	e8 f4 fa ff ff       	call   80018f <printnum>
  80069b:	83 c4 20             	add    $0x20,%esp
			break;
  80069e:	eb 39                	jmp    8006d9 <vprintfmt+0x3cb>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
  8006a0:	83 ec 08             	sub    $0x8,%esp
  8006a3:	ff 75 0c             	pushl  0xc(%ebp)
  8006a6:	53                   	push   %ebx
  8006a7:	8b 45 08             	mov    0x8(%ebp),%eax
  8006aa:	ff d0                	call   *%eax
  8006ac:	83 c4 10             	add    $0x10,%esp
			break;
  8006af:	eb 28                	jmp    8006d9 <vprintfmt+0x3cb>
			
		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
  8006b1:	83 ec 08             	sub    $0x8,%esp
  8006b4:	ff 75 0c             	pushl  0xc(%ebp)
  8006b7:	6a 25                	push   $0x25
  8006b9:	8b 45 08             	mov    0x8(%ebp),%eax
  8006bc:	ff d0                	call   *%eax
  8006be:	83 c4 10             	add    $0x10,%esp
			for (fmt--; fmt[-1] != '%'; fmt--)
  8006c1:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8006c5:	eb 04                	jmp    8006cb <vprintfmt+0x3bd>
  8006c7:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8006cb:	8b 45 10             	mov    0x10(%ebp),%eax
  8006ce:	83 e8 01             	sub    $0x1,%eax
  8006d1:	0f b6 00             	movzbl (%eax),%eax
  8006d4:	3c 25                	cmp    $0x25,%al
  8006d6:	75 ef                	jne    8006c7 <vprintfmt+0x3b9>
				/* do nothing */;
			break;
  8006d8:	90                   	nop
		}
	}
  8006d9:	e9 38 fc ff ff       	jmp    800316 <vprintfmt+0x8>
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
				return;
  8006de:	90                   	nop
			for (fmt--; fmt[-1] != '%'; fmt--)
				/* do nothing */;
			break;
		}
	}
}
  8006df:	8d 65 f8             	lea    -0x8(%ebp),%esp
  8006e2:	5b                   	pop    %ebx
  8006e3:	5e                   	pop    %esi
  8006e4:	5d                   	pop    %ebp
  8006e5:	c3                   	ret    

008006e6 <printfmt>:

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
  8006e6:	55                   	push   %ebp
  8006e7:	89 e5                	mov    %esp,%ebp
  8006e9:	83 ec 18             	sub    $0x18,%esp
	va_list ap;

	va_start(ap, fmt);
  8006ec:	8d 45 10             	lea    0x10(%ebp),%eax
  8006ef:	83 c0 04             	add    $0x4,%eax
  8006f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
	vprintfmt(putch, putdat, fmt, ap);
  8006f5:	8b 45 10             	mov    0x10(%ebp),%eax
  8006f8:	ff 75 f4             	pushl  -0xc(%ebp)
  8006fb:	50                   	push   %eax
  8006fc:	ff 75 0c             	pushl  0xc(%ebp)
  8006ff:	ff 75 08             	pushl  0x8(%ebp)
  800702:	e8 07 fc ff ff       	call   80030e <vprintfmt>
  800707:	83 c4 10             	add    $0x10,%esp
	va_end(ap);
}
  80070a:	90                   	nop
  80070b:	c9                   	leave  
  80070c:	c3                   	ret    

0080070d <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
  80070d:	55                   	push   %ebp
  80070e:	89 e5                	mov    %esp,%ebp
	b->cnt++;
  800710:	8b 45 0c             	mov    0xc(%ebp),%eax
  800713:	8b 40 08             	mov    0x8(%eax),%eax
  800716:	8d 50 01             	lea    0x1(%eax),%edx
  800719:	8b 45 0c             	mov    0xc(%ebp),%eax
  80071c:	89 50 08             	mov    %edx,0x8(%eax)
	if (b->buf < b->ebuf)
  80071f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800722:	8b 10                	mov    (%eax),%edx
  800724:	8b 45 0c             	mov    0xc(%ebp),%eax
  800727:	8b 40 04             	mov    0x4(%eax),%eax
  80072a:	39 c2                	cmp    %eax,%edx
  80072c:	73 12                	jae    800740 <sprintputch+0x33>
		*b->buf++ = ch;
  80072e:	8b 45 0c             	mov    0xc(%ebp),%eax
  800731:	8b 00                	mov    (%eax),%eax
  800733:	8d 48 01             	lea    0x1(%eax),%ecx
  800736:	8b 55 0c             	mov    0xc(%ebp),%edx
  800739:	89 0a                	mov    %ecx,(%edx)
  80073b:	8b 55 08             	mov    0x8(%ebp),%edx
  80073e:	88 10                	mov    %dl,(%eax)
}
  800740:	90                   	nop
  800741:	5d                   	pop    %ebp
  800742:	c3                   	ret    

00800743 <vsnprintf>:

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
  800743:	55                   	push   %ebp
  800744:	89 e5                	mov    %esp,%ebp
  800746:	83 ec 18             	sub    $0x18,%esp
	struct sprintbuf b = {buf, buf+n-1, 0};
  800749:	8b 45 08             	mov    0x8(%ebp),%eax
  80074c:	89 45 ec             	mov    %eax,-0x14(%ebp)
  80074f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800752:	8d 50 ff             	lea    -0x1(%eax),%edx
  800755:	8b 45 08             	mov    0x8(%ebp),%eax
  800758:	01 d0                	add    %edx,%eax
  80075a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  80075d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
  800764:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  800768:	74 06                	je     800770 <vsnprintf+0x2d>
  80076a:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80076e:	7f 07                	jg     800777 <vsnprintf+0x34>
		return -E_INVAL;
  800770:	b8 03 00 00 00       	mov    $0x3,%eax
  800775:	eb 20                	jmp    800797 <vsnprintf+0x54>

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
  800777:	ff 75 14             	pushl  0x14(%ebp)
  80077a:	ff 75 10             	pushl  0x10(%ebp)
  80077d:	8d 45 ec             	lea    -0x14(%ebp),%eax
  800780:	50                   	push   %eax
  800781:	68 0d 07 80 00       	push   $0x80070d
  800786:	e8 83 fb ff ff       	call   80030e <vprintfmt>
  80078b:	83 c4 10             	add    $0x10,%esp

	// null terminate the buffer
	*b.buf = '\0';
  80078e:	8b 45 ec             	mov    -0x14(%ebp),%eax
  800791:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
  800794:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  800797:	c9                   	leave  
  800798:	c3                   	ret    

00800799 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
  800799:	55                   	push   %ebp
  80079a:	89 e5                	mov    %esp,%ebp
  80079c:	83 ec 18             	sub    $0x18,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
  80079f:	8d 45 10             	lea    0x10(%ebp),%eax
  8007a2:	83 c0 04             	add    $0x4,%eax
  8007a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
	rc = vsnprintf(buf, n, fmt, ap);
  8007a8:	8b 45 10             	mov    0x10(%ebp),%eax
  8007ab:	ff 75 f4             	pushl  -0xc(%ebp)
  8007ae:	50                   	push   %eax
  8007af:	ff 75 0c             	pushl  0xc(%ebp)
  8007b2:	ff 75 08             	pushl  0x8(%ebp)
  8007b5:	e8 89 ff ff ff       	call   800743 <vsnprintf>
  8007ba:	83 c4 10             	add    $0x10,%esp
  8007bd:	89 45 f0             	mov    %eax,-0x10(%ebp)
	va_end(ap);

	return rc;
  8007c0:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  8007c3:	c9                   	leave  
  8007c4:	c3                   	ret    

008007c5 <strlen>:

#include <inc/string.h>

int
strlen(const char *s)
{
  8007c5:	55                   	push   %ebp
  8007c6:	89 e5                	mov    %esp,%ebp
  8007c8:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; *s != '\0'; s++)
  8007cb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8007d2:	eb 08                	jmp    8007dc <strlen+0x17>
		n++;
  8007d4:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
  8007d8:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8007dc:	8b 45 08             	mov    0x8(%ebp),%eax
  8007df:	0f b6 00             	movzbl (%eax),%eax
  8007e2:	84 c0                	test   %al,%al
  8007e4:	75 ee                	jne    8007d4 <strlen+0xf>
		n++;
	return n;
  8007e6:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  8007e9:	c9                   	leave  
  8007ea:	c3                   	ret    

008007eb <strnlen>:

int
strnlen(const char *s, uint32 size)
{
  8007eb:	55                   	push   %ebp
  8007ec:	89 e5                	mov    %esp,%ebp
  8007ee:	83 ec 10             	sub    $0x10,%esp
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007f1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  8007f8:	eb 0c                	jmp    800806 <strnlen+0x1b>
		n++;
  8007fa:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
int
strnlen(const char *s, uint32 size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
  8007fe:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800802:	83 6d 0c 01          	subl   $0x1,0xc(%ebp)
  800806:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  80080a:	74 0a                	je     800816 <strnlen+0x2b>
  80080c:	8b 45 08             	mov    0x8(%ebp),%eax
  80080f:	0f b6 00             	movzbl (%eax),%eax
  800812:	84 c0                	test   %al,%al
  800814:	75 e4                	jne    8007fa <strnlen+0xf>
		n++;
	return n;
  800816:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800819:	c9                   	leave  
  80081a:	c3                   	ret    

0080081b <strcpy>:

char *
strcpy(char *dst, const char *src)
{
  80081b:	55                   	push   %ebp
  80081c:	89 e5                	mov    %esp,%ebp
  80081e:	83 ec 10             	sub    $0x10,%esp
	char *ret;

	ret = dst;
  800821:	8b 45 08             	mov    0x8(%ebp),%eax
  800824:	89 45 fc             	mov    %eax,-0x4(%ebp)
	while ((*dst++ = *src++) != '\0')
  800827:	90                   	nop
  800828:	8b 45 08             	mov    0x8(%ebp),%eax
  80082b:	8d 50 01             	lea    0x1(%eax),%edx
  80082e:	89 55 08             	mov    %edx,0x8(%ebp)
  800831:	8b 55 0c             	mov    0xc(%ebp),%edx
  800834:	8d 4a 01             	lea    0x1(%edx),%ecx
  800837:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  80083a:	0f b6 12             	movzbl (%edx),%edx
  80083d:	88 10                	mov    %dl,(%eax)
  80083f:	0f b6 00             	movzbl (%eax),%eax
  800842:	84 c0                	test   %al,%al
  800844:	75 e2                	jne    800828 <strcpy+0xd>
		/* do nothing */;
	return ret;
  800846:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  800849:	c9                   	leave  
  80084a:	c3                   	ret    

0080084b <strncpy>:

char *
strncpy(char *dst, const char *src, uint32 size) {
  80084b:	55                   	push   %ebp
  80084c:	89 e5                	mov    %esp,%ebp
  80084e:	83 ec 10             	sub    $0x10,%esp
	uint32 i;
	char *ret;

	ret = dst;
  800851:	8b 45 08             	mov    0x8(%ebp),%eax
  800854:	89 45 f8             	mov    %eax,-0x8(%ebp)
	for (i = 0; i < size; i++) {
  800857:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  80085e:	eb 23                	jmp    800883 <strncpy+0x38>
		*dst++ = *src;
  800860:	8b 45 08             	mov    0x8(%ebp),%eax
  800863:	8d 50 01             	lea    0x1(%eax),%edx
  800866:	89 55 08             	mov    %edx,0x8(%ebp)
  800869:	8b 55 0c             	mov    0xc(%ebp),%edx
  80086c:	0f b6 12             	movzbl (%edx),%edx
  80086f:	88 10                	mov    %dl,(%eax)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
  800871:	8b 45 0c             	mov    0xc(%ebp),%eax
  800874:	0f b6 00             	movzbl (%eax),%eax
  800877:	84 c0                	test   %al,%al
  800879:	74 04                	je     80087f <strncpy+0x34>
			src++;
  80087b:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
strncpy(char *dst, const char *src, uint32 size) {
	uint32 i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
  80087f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800883:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800886:	3b 45 10             	cmp    0x10(%ebp),%eax
  800889:	72 d5                	jb     800860 <strncpy+0x15>
		*dst++ = *src;
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
  80088b:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  80088e:	c9                   	leave  
  80088f:	c3                   	ret    

00800890 <strlcpy>:

uint32
strlcpy(char *dst, const char *src, uint32 size)
{
  800890:	55                   	push   %ebp
  800891:	89 e5                	mov    %esp,%ebp
  800893:	83 ec 10             	sub    $0x10,%esp
	char *dst_in;

	dst_in = dst;
  800896:	8b 45 08             	mov    0x8(%ebp),%eax
  800899:	89 45 fc             	mov    %eax,-0x4(%ebp)
	if (size > 0) {
  80089c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8008a0:	74 33                	je     8008d5 <strlcpy+0x45>
		while (--size > 0 && *src != '\0')
  8008a2:	eb 17                	jmp    8008bb <strlcpy+0x2b>
			*dst++ = *src++;
  8008a4:	8b 45 08             	mov    0x8(%ebp),%eax
  8008a7:	8d 50 01             	lea    0x1(%eax),%edx
  8008aa:	89 55 08             	mov    %edx,0x8(%ebp)
  8008ad:	8b 55 0c             	mov    0xc(%ebp),%edx
  8008b0:	8d 4a 01             	lea    0x1(%edx),%ecx
  8008b3:	89 4d 0c             	mov    %ecx,0xc(%ebp)
  8008b6:	0f b6 12             	movzbl (%edx),%edx
  8008b9:	88 10                	mov    %dl,(%eax)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
  8008bb:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  8008bf:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  8008c3:	74 0a                	je     8008cf <strlcpy+0x3f>
  8008c5:	8b 45 0c             	mov    0xc(%ebp),%eax
  8008c8:	0f b6 00             	movzbl (%eax),%eax
  8008cb:	84 c0                	test   %al,%al
  8008cd:	75 d5                	jne    8008a4 <strlcpy+0x14>
			*dst++ = *src++;
		*dst = '\0';
  8008cf:	8b 45 08             	mov    0x8(%ebp),%eax
  8008d2:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
  8008d5:	8b 55 08             	mov    0x8(%ebp),%edx
  8008d8:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8008db:	29 c2                	sub    %eax,%edx
  8008dd:	89 d0                	mov    %edx,%eax
}
  8008df:	c9                   	leave  
  8008e0:	c3                   	ret    

008008e1 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8008e1:	55                   	push   %ebp
  8008e2:	89 e5                	mov    %esp,%ebp
	while (*p && *p == *q)
  8008e4:	eb 08                	jmp    8008ee <strcmp+0xd>
		p++, q++;
  8008e6:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8008ea:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
  8008ee:	8b 45 08             	mov    0x8(%ebp),%eax
  8008f1:	0f b6 00             	movzbl (%eax),%eax
  8008f4:	84 c0                	test   %al,%al
  8008f6:	74 10                	je     800908 <strcmp+0x27>
  8008f8:	8b 45 08             	mov    0x8(%ebp),%eax
  8008fb:	0f b6 10             	movzbl (%eax),%edx
  8008fe:	8b 45 0c             	mov    0xc(%ebp),%eax
  800901:	0f b6 00             	movzbl (%eax),%eax
  800904:	38 c2                	cmp    %al,%dl
  800906:	74 de                	je     8008e6 <strcmp+0x5>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
  800908:	8b 45 08             	mov    0x8(%ebp),%eax
  80090b:	0f b6 00             	movzbl (%eax),%eax
  80090e:	0f b6 d0             	movzbl %al,%edx
  800911:	8b 45 0c             	mov    0xc(%ebp),%eax
  800914:	0f b6 00             	movzbl (%eax),%eax
  800917:	0f b6 c0             	movzbl %al,%eax
  80091a:	29 c2                	sub    %eax,%edx
  80091c:	89 d0                	mov    %edx,%eax
}
  80091e:	5d                   	pop    %ebp
  80091f:	c3                   	ret    

00800920 <strncmp>:

int
strncmp(const char *p, const char *q, uint32 n)
{
  800920:	55                   	push   %ebp
  800921:	89 e5                	mov    %esp,%ebp
	while (n > 0 && *p && *p == *q)
  800923:	eb 0c                	jmp    800931 <strncmp+0x11>
		n--, p++, q++;
  800925:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
  800929:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  80092d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint32 n)
{
	while (n > 0 && *p && *p == *q)
  800931:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800935:	74 1a                	je     800951 <strncmp+0x31>
  800937:	8b 45 08             	mov    0x8(%ebp),%eax
  80093a:	0f b6 00             	movzbl (%eax),%eax
  80093d:	84 c0                	test   %al,%al
  80093f:	74 10                	je     800951 <strncmp+0x31>
  800941:	8b 45 08             	mov    0x8(%ebp),%eax
  800944:	0f b6 10             	movzbl (%eax),%edx
  800947:	8b 45 0c             	mov    0xc(%ebp),%eax
  80094a:	0f b6 00             	movzbl (%eax),%eax
  80094d:	38 c2                	cmp    %al,%dl
  80094f:	74 d4                	je     800925 <strncmp+0x5>
		n--, p++, q++;
	if (n == 0)
  800951:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800955:	75 07                	jne    80095e <strncmp+0x3e>
		return 0;
  800957:	b8 00 00 00 00       	mov    $0x0,%eax
  80095c:	eb 16                	jmp    800974 <strncmp+0x54>
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
  80095e:	8b 45 08             	mov    0x8(%ebp),%eax
  800961:	0f b6 00             	movzbl (%eax),%eax
  800964:	0f b6 d0             	movzbl %al,%edx
  800967:	8b 45 0c             	mov    0xc(%ebp),%eax
  80096a:	0f b6 00             	movzbl (%eax),%eax
  80096d:	0f b6 c0             	movzbl %al,%eax
  800970:	29 c2                	sub    %eax,%edx
  800972:	89 d0                	mov    %edx,%eax
}
  800974:	5d                   	pop    %ebp
  800975:	c3                   	ret    

00800976 <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
  800976:	55                   	push   %ebp
  800977:	89 e5                	mov    %esp,%ebp
  800979:	83 ec 04             	sub    $0x4,%esp
  80097c:	8b 45 0c             	mov    0xc(%ebp),%eax
  80097f:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  800982:	eb 14                	jmp    800998 <strchr+0x22>
		if (*s == c)
  800984:	8b 45 08             	mov    0x8(%ebp),%eax
  800987:	0f b6 00             	movzbl (%eax),%eax
  80098a:	3a 45 fc             	cmp    -0x4(%ebp),%al
  80098d:	75 05                	jne    800994 <strchr+0x1e>
			return (char *) s;
  80098f:	8b 45 08             	mov    0x8(%ebp),%eax
  800992:	eb 13                	jmp    8009a7 <strchr+0x31>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
  800994:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800998:	8b 45 08             	mov    0x8(%ebp),%eax
  80099b:	0f b6 00             	movzbl (%eax),%eax
  80099e:	84 c0                	test   %al,%al
  8009a0:	75 e2                	jne    800984 <strchr+0xe>
		if (*s == c)
			return (char *) s;
	return 0;
  8009a2:	b8 00 00 00 00       	mov    $0x0,%eax
}
  8009a7:	c9                   	leave  
  8009a8:	c3                   	ret    

008009a9 <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
  8009a9:	55                   	push   %ebp
  8009aa:	89 e5                	mov    %esp,%ebp
  8009ac:	83 ec 04             	sub    $0x4,%esp
  8009af:	8b 45 0c             	mov    0xc(%ebp),%eax
  8009b2:	88 45 fc             	mov    %al,-0x4(%ebp)
	for (; *s; s++)
  8009b5:	eb 0f                	jmp    8009c6 <strfind+0x1d>
		if (*s == c)
  8009b7:	8b 45 08             	mov    0x8(%ebp),%eax
  8009ba:	0f b6 00             	movzbl (%eax),%eax
  8009bd:	3a 45 fc             	cmp    -0x4(%ebp),%al
  8009c0:	74 10                	je     8009d2 <strfind+0x29>
// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
	for (; *s; s++)
  8009c2:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  8009c6:	8b 45 08             	mov    0x8(%ebp),%eax
  8009c9:	0f b6 00             	movzbl (%eax),%eax
  8009cc:	84 c0                	test   %al,%al
  8009ce:	75 e7                	jne    8009b7 <strfind+0xe>
  8009d0:	eb 01                	jmp    8009d3 <strfind+0x2a>
		if (*s == c)
			break;
  8009d2:	90                   	nop
	return (char *) s;
  8009d3:	8b 45 08             	mov    0x8(%ebp),%eax
}
  8009d6:	c9                   	leave  
  8009d7:	c3                   	ret    

008009d8 <memset>:


void *
memset(void *v, int c, uint32 n)
{
  8009d8:	55                   	push   %ebp
  8009d9:	89 e5                	mov    %esp,%ebp
  8009db:	83 ec 10             	sub    $0x10,%esp
	char *p;
	int m;

	p = v;
  8009de:	8b 45 08             	mov    0x8(%ebp),%eax
  8009e1:	89 45 fc             	mov    %eax,-0x4(%ebp)
	m = n;
  8009e4:	8b 45 10             	mov    0x10(%ebp),%eax
  8009e7:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (--m >= 0)
  8009ea:	eb 0e                	jmp    8009fa <memset+0x22>
		*p++ = c;
  8009ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
  8009ef:	8d 50 01             	lea    0x1(%eax),%edx
  8009f2:	89 55 fc             	mov    %edx,-0x4(%ebp)
  8009f5:	8b 55 0c             	mov    0xc(%ebp),%edx
  8009f8:	88 10                	mov    %dl,(%eax)
	char *p;
	int m;

	p = v;
	m = n;
	while (--m >= 0)
  8009fa:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  8009fe:	83 7d f8 00          	cmpl   $0x0,-0x8(%ebp)
  800a02:	79 e8                	jns    8009ec <memset+0x14>
		*p++ = c;

	return v;
  800a04:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a07:	c9                   	leave  
  800a08:	c3                   	ret    

00800a09 <memcpy>:

void *
memcpy(void *dst, const void *src, uint32 n)
{
  800a09:	55                   	push   %ebp
  800a0a:	89 e5                	mov    %esp,%ebp
  800a0c:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;

	s = src;
  800a0f:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a12:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a15:	8b 45 08             	mov    0x8(%ebp),%eax
  800a18:	89 45 f8             	mov    %eax,-0x8(%ebp)
	while (n-- > 0)
  800a1b:	eb 17                	jmp    800a34 <memcpy+0x2b>
		*d++ = *s++;
  800a1d:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800a20:	8d 50 01             	lea    0x1(%eax),%edx
  800a23:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800a26:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800a29:	8d 4a 01             	lea    0x1(%edx),%ecx
  800a2c:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800a2f:	0f b6 12             	movzbl (%edx),%edx
  800a32:	88 10                	mov    %dl,(%eax)
	const char *s;
	char *d;

	s = src;
	d = dst;
	while (n-- > 0)
  800a34:	8b 45 10             	mov    0x10(%ebp),%eax
  800a37:	8d 50 ff             	lea    -0x1(%eax),%edx
  800a3a:	89 55 10             	mov    %edx,0x10(%ebp)
  800a3d:	85 c0                	test   %eax,%eax
  800a3f:	75 dc                	jne    800a1d <memcpy+0x14>
		*d++ = *s++;

	return dst;
  800a41:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800a44:	c9                   	leave  
  800a45:	c3                   	ret    

00800a46 <memmove>:

void *
memmove(void *dst, const void *src, uint32 n)
{
  800a46:	55                   	push   %ebp
  800a47:	89 e5                	mov    %esp,%ebp
  800a49:	83 ec 10             	sub    $0x10,%esp
	const char *s;
	char *d;
	
	s = src;
  800a4c:	8b 45 0c             	mov    0xc(%ebp),%eax
  800a4f:	89 45 fc             	mov    %eax,-0x4(%ebp)
	d = dst;
  800a52:	8b 45 08             	mov    0x8(%ebp),%eax
  800a55:	89 45 f8             	mov    %eax,-0x8(%ebp)
	if (s < d && s + n > d) {
  800a58:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a5b:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800a5e:	73 54                	jae    800ab4 <memmove+0x6e>
  800a60:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800a63:	8b 45 10             	mov    0x10(%ebp),%eax
  800a66:	01 d0                	add    %edx,%eax
  800a68:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  800a6b:	76 47                	jbe    800ab4 <memmove+0x6e>
		s += n;
  800a6d:	8b 45 10             	mov    0x10(%ebp),%eax
  800a70:	01 45 fc             	add    %eax,-0x4(%ebp)
		d += n;
  800a73:	8b 45 10             	mov    0x10(%ebp),%eax
  800a76:	01 45 f8             	add    %eax,-0x8(%ebp)
		while (n-- > 0)
  800a79:	eb 13                	jmp    800a8e <memmove+0x48>
			*--d = *--s;
  800a7b:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
  800a7f:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
  800a83:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800a86:	0f b6 10             	movzbl (%eax),%edx
  800a89:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800a8c:	88 10                	mov    %dl,(%eax)
	s = src;
	d = dst;
	if (s < d && s + n > d) {
		s += n;
		d += n;
		while (n-- > 0)
  800a8e:	8b 45 10             	mov    0x10(%ebp),%eax
  800a91:	8d 50 ff             	lea    -0x1(%eax),%edx
  800a94:	89 55 10             	mov    %edx,0x10(%ebp)
  800a97:	85 c0                	test   %eax,%eax
  800a99:	75 e0                	jne    800a7b <memmove+0x35>
	const char *s;
	char *d;
	
	s = src;
	d = dst;
	if (s < d && s + n > d) {
  800a9b:	eb 24                	jmp    800ac1 <memmove+0x7b>
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
			*d++ = *s++;
  800a9d:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800aa0:	8d 50 01             	lea    0x1(%eax),%edx
  800aa3:	89 55 f8             	mov    %edx,-0x8(%ebp)
  800aa6:	8b 55 fc             	mov    -0x4(%ebp),%edx
  800aa9:	8d 4a 01             	lea    0x1(%edx),%ecx
  800aac:	89 4d fc             	mov    %ecx,-0x4(%ebp)
  800aaf:	0f b6 12             	movzbl (%edx),%edx
  800ab2:	88 10                	mov    %dl,(%eax)
		s += n;
		d += n;
		while (n-- > 0)
			*--d = *--s;
	} else
		while (n-- > 0)
  800ab4:	8b 45 10             	mov    0x10(%ebp),%eax
  800ab7:	8d 50 ff             	lea    -0x1(%eax),%edx
  800aba:	89 55 10             	mov    %edx,0x10(%ebp)
  800abd:	85 c0                	test   %eax,%eax
  800abf:	75 dc                	jne    800a9d <memmove+0x57>
			*d++ = *s++;

	return dst;
  800ac1:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800ac4:	c9                   	leave  
  800ac5:	c3                   	ret    

00800ac6 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint32 n)
{
  800ac6:	55                   	push   %ebp
  800ac7:	89 e5                	mov    %esp,%ebp
  800ac9:	83 ec 10             	sub    $0x10,%esp
	const uint8 *s1 = (const uint8 *) v1;
  800acc:	8b 45 08             	mov    0x8(%ebp),%eax
  800acf:	89 45 fc             	mov    %eax,-0x4(%ebp)
	const uint8 *s2 = (const uint8 *) v2;
  800ad2:	8b 45 0c             	mov    0xc(%ebp),%eax
  800ad5:	89 45 f8             	mov    %eax,-0x8(%ebp)

	while (n-- > 0) {
  800ad8:	eb 30                	jmp    800b0a <memcmp+0x44>
		if (*s1 != *s2)
  800ada:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800add:	0f b6 10             	movzbl (%eax),%edx
  800ae0:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800ae3:	0f b6 00             	movzbl (%eax),%eax
  800ae6:	38 c2                	cmp    %al,%dl
  800ae8:	74 18                	je     800b02 <memcmp+0x3c>
			return (int) *s1 - (int) *s2;
  800aea:	8b 45 fc             	mov    -0x4(%ebp),%eax
  800aed:	0f b6 00             	movzbl (%eax),%eax
  800af0:	0f b6 d0             	movzbl %al,%edx
  800af3:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800af6:	0f b6 00             	movzbl (%eax),%eax
  800af9:	0f b6 c0             	movzbl %al,%eax
  800afc:	29 c2                	sub    %eax,%edx
  800afe:	89 d0                	mov    %edx,%eax
  800b00:	eb 1a                	jmp    800b1c <memcmp+0x56>
		s1++, s2++;
  800b02:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
  800b06:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
memcmp(const void *v1, const void *v2, uint32 n)
{
	const uint8 *s1 = (const uint8 *) v1;
	const uint8 *s2 = (const uint8 *) v2;

	while (n-- > 0) {
  800b0a:	8b 45 10             	mov    0x10(%ebp),%eax
  800b0d:	8d 50 ff             	lea    -0x1(%eax),%edx
  800b10:	89 55 10             	mov    %edx,0x10(%ebp)
  800b13:	85 c0                	test   %eax,%eax
  800b15:	75 c3                	jne    800ada <memcmp+0x14>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
  800b17:	b8 00 00 00 00       	mov    $0x0,%eax
}
  800b1c:	c9                   	leave  
  800b1d:	c3                   	ret    

00800b1e <memfind>:

void *
memfind(const void *s, int c, uint32 n)
{
  800b1e:	55                   	push   %ebp
  800b1f:	89 e5                	mov    %esp,%ebp
  800b21:	83 ec 10             	sub    $0x10,%esp
	const void *ends = (const char *) s + n;
  800b24:	8b 55 08             	mov    0x8(%ebp),%edx
  800b27:	8b 45 10             	mov    0x10(%ebp),%eax
  800b2a:	01 d0                	add    %edx,%eax
  800b2c:	89 45 fc             	mov    %eax,-0x4(%ebp)
	for (; s < ends; s++)
  800b2f:	eb 17                	jmp    800b48 <memfind+0x2a>
		if (*(const unsigned char *) s == (unsigned char) c)
  800b31:	8b 45 08             	mov    0x8(%ebp),%eax
  800b34:	0f b6 00             	movzbl (%eax),%eax
  800b37:	0f b6 d0             	movzbl %al,%edx
  800b3a:	8b 45 0c             	mov    0xc(%ebp),%eax
  800b3d:	0f b6 c0             	movzbl %al,%eax
  800b40:	39 c2                	cmp    %eax,%edx
  800b42:	74 0e                	je     800b52 <memfind+0x34>

void *
memfind(const void *s, int c, uint32 n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
  800b44:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b48:	8b 45 08             	mov    0x8(%ebp),%eax
  800b4b:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  800b4e:	72 e1                	jb     800b31 <memfind+0x13>
  800b50:	eb 01                	jmp    800b53 <memfind+0x35>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
  800b52:	90                   	nop
	return (void *) s;
  800b53:	8b 45 08             	mov    0x8(%ebp),%eax
}
  800b56:	c9                   	leave  
  800b57:	c3                   	ret    

00800b58 <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
  800b58:	55                   	push   %ebp
  800b59:	89 e5                	mov    %esp,%ebp
  800b5b:	83 ec 10             	sub    $0x10,%esp
	int neg = 0;
  800b5e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
	long val = 0;
  800b65:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b6c:	eb 04                	jmp    800b72 <strtol+0x1a>
		s++;
  800b6e:	83 45 08 01          	addl   $0x1,0x8(%ebp)
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
  800b72:	8b 45 08             	mov    0x8(%ebp),%eax
  800b75:	0f b6 00             	movzbl (%eax),%eax
  800b78:	3c 20                	cmp    $0x20,%al
  800b7a:	74 f2                	je     800b6e <strtol+0x16>
  800b7c:	8b 45 08             	mov    0x8(%ebp),%eax
  800b7f:	0f b6 00             	movzbl (%eax),%eax
  800b82:	3c 09                	cmp    $0x9,%al
  800b84:	74 e8                	je     800b6e <strtol+0x16>
		s++;

	// plus/minus sign
	if (*s == '+')
  800b86:	8b 45 08             	mov    0x8(%ebp),%eax
  800b89:	0f b6 00             	movzbl (%eax),%eax
  800b8c:	3c 2b                	cmp    $0x2b,%al
  800b8e:	75 06                	jne    800b96 <strtol+0x3e>
		s++;
  800b90:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800b94:	eb 15                	jmp    800bab <strtol+0x53>
	else if (*s == '-')
  800b96:	8b 45 08             	mov    0x8(%ebp),%eax
  800b99:	0f b6 00             	movzbl (%eax),%eax
  800b9c:	3c 2d                	cmp    $0x2d,%al
  800b9e:	75 0b                	jne    800bab <strtol+0x53>
		s++, neg = 1;
  800ba0:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800ba4:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
  800bab:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800baf:	74 06                	je     800bb7 <strtol+0x5f>
  800bb1:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  800bb5:	75 24                	jne    800bdb <strtol+0x83>
  800bb7:	8b 45 08             	mov    0x8(%ebp),%eax
  800bba:	0f b6 00             	movzbl (%eax),%eax
  800bbd:	3c 30                	cmp    $0x30,%al
  800bbf:	75 1a                	jne    800bdb <strtol+0x83>
  800bc1:	8b 45 08             	mov    0x8(%ebp),%eax
  800bc4:	83 c0 01             	add    $0x1,%eax
  800bc7:	0f b6 00             	movzbl (%eax),%eax
  800bca:	3c 78                	cmp    $0x78,%al
  800bcc:	75 0d                	jne    800bdb <strtol+0x83>
		s += 2, base = 16;
  800bce:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  800bd2:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  800bd9:	eb 2a                	jmp    800c05 <strtol+0xad>
	else if (base == 0 && s[0] == '0')
  800bdb:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bdf:	75 17                	jne    800bf8 <strtol+0xa0>
  800be1:	8b 45 08             	mov    0x8(%ebp),%eax
  800be4:	0f b6 00             	movzbl (%eax),%eax
  800be7:	3c 30                	cmp    $0x30,%al
  800be9:	75 0d                	jne    800bf8 <strtol+0xa0>
		s++, base = 8;
  800beb:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800bef:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  800bf6:	eb 0d                	jmp    800c05 <strtol+0xad>
	else if (base == 0)
  800bf8:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  800bfc:	75 07                	jne    800c05 <strtol+0xad>
		base = 10;
  800bfe:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
  800c05:	8b 45 08             	mov    0x8(%ebp),%eax
  800c08:	0f b6 00             	movzbl (%eax),%eax
  800c0b:	3c 2f                	cmp    $0x2f,%al
  800c0d:	7e 1b                	jle    800c2a <strtol+0xd2>
  800c0f:	8b 45 08             	mov    0x8(%ebp),%eax
  800c12:	0f b6 00             	movzbl (%eax),%eax
  800c15:	3c 39                	cmp    $0x39,%al
  800c17:	7f 11                	jg     800c2a <strtol+0xd2>
			dig = *s - '0';
  800c19:	8b 45 08             	mov    0x8(%ebp),%eax
  800c1c:	0f b6 00             	movzbl (%eax),%eax
  800c1f:	0f be c0             	movsbl %al,%eax
  800c22:	83 e8 30             	sub    $0x30,%eax
  800c25:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800c28:	eb 48                	jmp    800c72 <strtol+0x11a>
		else if (*s >= 'a' && *s <= 'z')
  800c2a:	8b 45 08             	mov    0x8(%ebp),%eax
  800c2d:	0f b6 00             	movzbl (%eax),%eax
  800c30:	3c 60                	cmp    $0x60,%al
  800c32:	7e 1b                	jle    800c4f <strtol+0xf7>
  800c34:	8b 45 08             	mov    0x8(%ebp),%eax
  800c37:	0f b6 00             	movzbl (%eax),%eax
  800c3a:	3c 7a                	cmp    $0x7a,%al
  800c3c:	7f 11                	jg     800c4f <strtol+0xf7>
			dig = *s - 'a' + 10;
  800c3e:	8b 45 08             	mov    0x8(%ebp),%eax
  800c41:	0f b6 00             	movzbl (%eax),%eax
  800c44:	0f be c0             	movsbl %al,%eax
  800c47:	83 e8 57             	sub    $0x57,%eax
  800c4a:	89 45 f4             	mov    %eax,-0xc(%ebp)
  800c4d:	eb 23                	jmp    800c72 <strtol+0x11a>
		else if (*s >= 'A' && *s <= 'Z')
  800c4f:	8b 45 08             	mov    0x8(%ebp),%eax
  800c52:	0f b6 00             	movzbl (%eax),%eax
  800c55:	3c 40                	cmp    $0x40,%al
  800c57:	7e 3c                	jle    800c95 <strtol+0x13d>
  800c59:	8b 45 08             	mov    0x8(%ebp),%eax
  800c5c:	0f b6 00             	movzbl (%eax),%eax
  800c5f:	3c 5a                	cmp    $0x5a,%al
  800c61:	7f 32                	jg     800c95 <strtol+0x13d>
			dig = *s - 'A' + 10;
  800c63:	8b 45 08             	mov    0x8(%ebp),%eax
  800c66:	0f b6 00             	movzbl (%eax),%eax
  800c69:	0f be c0             	movsbl %al,%eax
  800c6c:	83 e8 37             	sub    $0x37,%eax
  800c6f:	89 45 f4             	mov    %eax,-0xc(%ebp)
		else
			break;
		if (dig >= base)
  800c72:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800c75:	3b 45 10             	cmp    0x10(%ebp),%eax
  800c78:	7d 1a                	jge    800c94 <strtol+0x13c>
			break;
		s++, val = (val * base) + dig;
  800c7a:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  800c7e:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800c81:	0f af 45 10          	imul   0x10(%ebp),%eax
  800c85:	89 c2                	mov    %eax,%edx
  800c87:	8b 45 f4             	mov    -0xc(%ebp),%eax
  800c8a:	01 d0                	add    %edx,%eax
  800c8c:	89 45 f8             	mov    %eax,-0x8(%ebp)
		// we don't properly detect overflow!
	}
  800c8f:	e9 71 ff ff ff       	jmp    800c05 <strtol+0xad>
		else if (*s >= 'A' && *s <= 'Z')
			dig = *s - 'A' + 10;
		else
			break;
		if (dig >= base)
			break;
  800c94:	90                   	nop
		s++, val = (val * base) + dig;
		// we don't properly detect overflow!
	}

	if (endptr)
  800c95:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  800c99:	74 08                	je     800ca3 <strtol+0x14b>
		*endptr = (char *) s;
  800c9b:	8b 45 0c             	mov    0xc(%ebp),%eax
  800c9e:	8b 55 08             	mov    0x8(%ebp),%edx
  800ca1:	89 10                	mov    %edx,(%eax)
	return (neg ? -val : val);
  800ca3:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  800ca7:	74 07                	je     800cb0 <strtol+0x158>
  800ca9:	8b 45 f8             	mov    -0x8(%ebp),%eax
  800cac:	f7 d8                	neg    %eax
  800cae:	eb 03                	jmp    800cb3 <strtol+0x15b>
  800cb0:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  800cb3:	c9                   	leave  
  800cb4:	c3                   	ret    

00800cb5 <strsplit>:

int strsplit(char *string, char *SPLIT_CHARS, char **argv, int * argc)
{
  800cb5:	55                   	push   %ebp
  800cb6:	89 e5                	mov    %esp,%ebp
	// Parse the command string into splitchars-separated arguments
	*argc = 0;
  800cb8:	8b 45 14             	mov    0x14(%ebp),%eax
  800cbb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	(argv)[*argc] = 0;
  800cc1:	8b 45 14             	mov    0x14(%ebp),%eax
  800cc4:	8b 00                	mov    (%eax),%eax
  800cc6:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800ccd:	8b 45 10             	mov    0x10(%ebp),%eax
  800cd0:	01 d0                	add    %edx,%eax
  800cd2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800cd8:	eb 0c                	jmp    800ce6 <strsplit+0x31>
			*string++ = 0;
  800cda:	8b 45 08             	mov    0x8(%ebp),%eax
  800cdd:	8d 50 01             	lea    0x1(%eax),%edx
  800ce0:	89 55 08             	mov    %edx,0x8(%ebp)
  800ce3:	c6 00 00             	movb   $0x0,(%eax)
	*argc = 0;
	(argv)[*argc] = 0;
	while (1) 
	{
		// trim splitchars
		while (*string && strchr(SPLIT_CHARS, *string))
  800ce6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ce9:	0f b6 00             	movzbl (%eax),%eax
  800cec:	84 c0                	test   %al,%al
  800cee:	74 19                	je     800d09 <strsplit+0x54>
  800cf0:	8b 45 08             	mov    0x8(%ebp),%eax
  800cf3:	0f b6 00             	movzbl (%eax),%eax
  800cf6:	0f be c0             	movsbl %al,%eax
  800cf9:	50                   	push   %eax
  800cfa:	ff 75 0c             	pushl  0xc(%ebp)
  800cfd:	e8 74 fc ff ff       	call   800976 <strchr>
  800d02:	83 c4 08             	add    $0x8,%esp
  800d05:	85 c0                	test   %eax,%eax
  800d07:	75 d1                	jne    800cda <strsplit+0x25>
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
  800d09:	8b 45 08             	mov    0x8(%ebp),%eax
  800d0c:	0f b6 00             	movzbl (%eax),%eax
  800d0f:	84 c0                	test   %al,%al
  800d11:	74 5d                	je     800d70 <strsplit+0xbb>
			break;

		//check current number of arguments
		if (*argc == MAX_ARGUMENTS-1) 
  800d13:	8b 45 14             	mov    0x14(%ebp),%eax
  800d16:	8b 00                	mov    (%eax),%eax
  800d18:	83 f8 0f             	cmp    $0xf,%eax
  800d1b:	75 07                	jne    800d24 <strsplit+0x6f>
		{
			return 0;
  800d1d:	b8 00 00 00 00       	mov    $0x0,%eax
  800d22:	eb 69                	jmp    800d8d <strsplit+0xd8>
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
  800d24:	8b 45 14             	mov    0x14(%ebp),%eax
  800d27:	8b 00                	mov    (%eax),%eax
  800d29:	8d 48 01             	lea    0x1(%eax),%ecx
  800d2c:	8b 55 14             	mov    0x14(%ebp),%edx
  800d2f:	89 0a                	mov    %ecx,(%edx)
  800d31:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d38:	8b 45 10             	mov    0x10(%ebp),%eax
  800d3b:	01 c2                	add    %eax,%edx
  800d3d:	8b 45 08             	mov    0x8(%ebp),%eax
  800d40:	89 02                	mov    %eax,(%edx)
		while (*string && !strchr(SPLIT_CHARS, *string))
  800d42:	eb 04                	jmp    800d48 <strsplit+0x93>
			string++;
  800d44:	83 45 08 01          	addl   $0x1,0x8(%ebp)
			return 0;
		}
		
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
  800d48:	8b 45 08             	mov    0x8(%ebp),%eax
  800d4b:	0f b6 00             	movzbl (%eax),%eax
  800d4e:	84 c0                	test   %al,%al
  800d50:	74 86                	je     800cd8 <strsplit+0x23>
  800d52:	8b 45 08             	mov    0x8(%ebp),%eax
  800d55:	0f b6 00             	movzbl (%eax),%eax
  800d58:	0f be c0             	movsbl %al,%eax
  800d5b:	50                   	push   %eax
  800d5c:	ff 75 0c             	pushl  0xc(%ebp)
  800d5f:	e8 12 fc ff ff       	call   800976 <strchr>
  800d64:	83 c4 08             	add    $0x8,%esp
  800d67:	85 c0                	test   %eax,%eax
  800d69:	74 d9                	je     800d44 <strsplit+0x8f>
			string++;
	}
  800d6b:	e9 68 ff ff ff       	jmp    800cd8 <strsplit+0x23>
		while (*string && strchr(SPLIT_CHARS, *string))
			*string++ = 0;
		
		//if the command string is finished, then break the loop
		if (*string == 0)
			break;
  800d70:	90                   	nop
		// save the previous argument and scan past next arg
		(argv)[(*argc)++] = string;
		while (*string && !strchr(SPLIT_CHARS, *string))
			string++;
	}
	(argv)[*argc] = 0;
  800d71:	8b 45 14             	mov    0x14(%ebp),%eax
  800d74:	8b 00                	mov    (%eax),%eax
  800d76:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  800d7d:	8b 45 10             	mov    0x10(%ebp),%eax
  800d80:	01 d0                	add    %edx,%eax
  800d82:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	return 1 ;
  800d88:	b8 01 00 00 00       	mov    $0x1,%eax
}
  800d8d:	c9                   	leave  
  800d8e:	c3                   	ret    

00800d8f <syscall>:
#include <inc/syscall.h>
#include <inc/lib.h>

static inline uint32
syscall(int num, uint32 a1, uint32 a2, uint32 a3, uint32 a4, uint32 a5)
{
  800d8f:	55                   	push   %ebp
  800d90:	89 e5                	mov    %esp,%ebp
  800d92:	57                   	push   %edi
  800d93:	56                   	push   %esi
  800d94:	53                   	push   %ebx
  800d95:	83 ec 10             	sub    $0x10,%esp
	// 
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
  800d98:	8b 45 08             	mov    0x8(%ebp),%eax
  800d9b:	8b 55 0c             	mov    0xc(%ebp),%edx
  800d9e:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800da1:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800da4:	8b 7d 18             	mov    0x18(%ebp),%edi
  800da7:	8b 75 1c             	mov    0x1c(%ebp),%esi
  800daa:	cd 30                	int    $0x30
  800dac:	89 45 f0             	mov    %eax,-0x10(%ebp)
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");
	
	return ret;
  800daf:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  800db2:	83 c4 10             	add    $0x10,%esp
  800db5:	5b                   	pop    %ebx
  800db6:	5e                   	pop    %esi
  800db7:	5f                   	pop    %edi
  800db8:	5d                   	pop    %ebp
  800db9:	c3                   	ret    

00800dba <sys_cputs>:

void
sys_cputs(const char *s, uint32 len)
{
  800dba:	55                   	push   %ebp
  800dbb:	89 e5                	mov    %esp,%ebp
	syscall(SYS_cputs, (uint32) s, len, 0, 0, 0);
  800dbd:	8b 45 08             	mov    0x8(%ebp),%eax
  800dc0:	6a 00                	push   $0x0
  800dc2:	6a 00                	push   $0x0
  800dc4:	6a 00                	push   $0x0
  800dc6:	ff 75 0c             	pushl  0xc(%ebp)
  800dc9:	50                   	push   %eax
  800dca:	6a 00                	push   $0x0
  800dcc:	e8 be ff ff ff       	call   800d8f <syscall>
  800dd1:	83 c4 18             	add    $0x18,%esp
}
  800dd4:	90                   	nop
  800dd5:	c9                   	leave  
  800dd6:	c3                   	ret    

00800dd7 <sys_cgetc>:

int
sys_cgetc(void)
{
  800dd7:	55                   	push   %ebp
  800dd8:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_cgetc, 0, 0, 0, 0, 0);
  800dda:	6a 00                	push   $0x0
  800ddc:	6a 00                	push   $0x0
  800dde:	6a 00                	push   $0x0
  800de0:	6a 00                	push   $0x0
  800de2:	6a 00                	push   $0x0
  800de4:	6a 01                	push   $0x1
  800de6:	e8 a4 ff ff ff       	call   800d8f <syscall>
  800deb:	83 c4 18             	add    $0x18,%esp
}
  800dee:	c9                   	leave  
  800def:	c3                   	ret    

00800df0 <sys_env_destroy>:

int	sys_env_destroy(int32  envid)
{
  800df0:	55                   	push   %ebp
  800df1:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_env_destroy, envid, 0, 0, 0, 0);
  800df3:	8b 45 08             	mov    0x8(%ebp),%eax
  800df6:	6a 00                	push   $0x0
  800df8:	6a 00                	push   $0x0
  800dfa:	6a 00                	push   $0x0
  800dfc:	6a 00                	push   $0x0
  800dfe:	50                   	push   %eax
  800dff:	6a 03                	push   $0x3
  800e01:	e8 89 ff ff ff       	call   800d8f <syscall>
  800e06:	83 c4 18             	add    $0x18,%esp
}
  800e09:	c9                   	leave  
  800e0a:	c3                   	ret    

00800e0b <sys_getenvid>:

int32 sys_getenvid(void)
{
  800e0b:	55                   	push   %ebp
  800e0c:	89 e5                	mov    %esp,%ebp
	 return syscall(SYS_getenvid, 0, 0, 0, 0, 0);
  800e0e:	6a 00                	push   $0x0
  800e10:	6a 00                	push   $0x0
  800e12:	6a 00                	push   $0x0
  800e14:	6a 00                	push   $0x0
  800e16:	6a 00                	push   $0x0
  800e18:	6a 02                	push   $0x2
  800e1a:	e8 70 ff ff ff       	call   800d8f <syscall>
  800e1f:	83 c4 18             	add    $0x18,%esp
}
  800e22:	c9                   	leave  
  800e23:	c3                   	ret    

00800e24 <sys_env_sleep>:

void sys_env_sleep(void)
{
  800e24:	55                   	push   %ebp
  800e25:	89 e5                	mov    %esp,%ebp
	syscall(SYS_env_sleep, 0, 0, 0, 0, 0);
  800e27:	6a 00                	push   $0x0
  800e29:	6a 00                	push   $0x0
  800e2b:	6a 00                	push   $0x0
  800e2d:	6a 00                	push   $0x0
  800e2f:	6a 00                	push   $0x0
  800e31:	6a 04                	push   $0x4
  800e33:	e8 57 ff ff ff       	call   800d8f <syscall>
  800e38:	83 c4 18             	add    $0x18,%esp
}
  800e3b:	90                   	nop
  800e3c:	c9                   	leave  
  800e3d:	c3                   	ret    

00800e3e <sys_allocate_page>:


int sys_allocate_page(void *va, int perm)
{
  800e3e:	55                   	push   %ebp
  800e3f:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_allocate_page, (uint32) va, perm, 0 , 0, 0);
  800e41:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e44:	8b 45 08             	mov    0x8(%ebp),%eax
  800e47:	6a 00                	push   $0x0
  800e49:	6a 00                	push   $0x0
  800e4b:	6a 00                	push   $0x0
  800e4d:	52                   	push   %edx
  800e4e:	50                   	push   %eax
  800e4f:	6a 05                	push   $0x5
  800e51:	e8 39 ff ff ff       	call   800d8f <syscall>
  800e56:	83 c4 18             	add    $0x18,%esp
}
  800e59:	c9                   	leave  
  800e5a:	c3                   	ret    

00800e5b <sys_get_page>:

int sys_get_page(void *va, int perm)
{
  800e5b:	55                   	push   %ebp
  800e5c:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_get_page, (uint32) va, perm, 0 , 0, 0);
  800e5e:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e61:	8b 45 08             	mov    0x8(%ebp),%eax
  800e64:	6a 00                	push   $0x0
  800e66:	6a 00                	push   $0x0
  800e68:	6a 00                	push   $0x0
  800e6a:	52                   	push   %edx
  800e6b:	50                   	push   %eax
  800e6c:	6a 06                	push   $0x6
  800e6e:	e8 1c ff ff ff       	call   800d8f <syscall>
  800e73:	83 c4 18             	add    $0x18,%esp
}
  800e76:	c9                   	leave  
  800e77:	c3                   	ret    

00800e78 <sys_map_frame>:
		
int sys_map_frame(int32 srcenv, void *srcva, int32 dstenv, void *dstva, int perm)
{
  800e78:	55                   	push   %ebp
  800e79:	89 e5                	mov    %esp,%ebp
  800e7b:	56                   	push   %esi
  800e7c:	53                   	push   %ebx
	return syscall(SYS_map_frame, srcenv, (uint32) srcva, dstenv, (uint32) dstva, perm);
  800e7d:	8b 75 18             	mov    0x18(%ebp),%esi
  800e80:	8b 5d 14             	mov    0x14(%ebp),%ebx
  800e83:	8b 4d 10             	mov    0x10(%ebp),%ecx
  800e86:	8b 55 0c             	mov    0xc(%ebp),%edx
  800e89:	8b 45 08             	mov    0x8(%ebp),%eax
  800e8c:	56                   	push   %esi
  800e8d:	53                   	push   %ebx
  800e8e:	51                   	push   %ecx
  800e8f:	52                   	push   %edx
  800e90:	50                   	push   %eax
  800e91:	6a 07                	push   $0x7
  800e93:	e8 f7 fe ff ff       	call   800d8f <syscall>
  800e98:	83 c4 18             	add    $0x18,%esp
}
  800e9b:	8d 65 f8             	lea    -0x8(%ebp),%esp
  800e9e:	5b                   	pop    %ebx
  800e9f:	5e                   	pop    %esi
  800ea0:	5d                   	pop    %ebp
  800ea1:	c3                   	ret    

00800ea2 <sys_unmap_frame>:

int sys_unmap_frame(int32 envid, void *va)
{
  800ea2:	55                   	push   %ebp
  800ea3:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_unmap_frame, envid, (uint32) va, 0, 0, 0);
  800ea5:	8b 55 0c             	mov    0xc(%ebp),%edx
  800ea8:	8b 45 08             	mov    0x8(%ebp),%eax
  800eab:	6a 00                	push   $0x0
  800ead:	6a 00                	push   $0x0
  800eaf:	6a 00                	push   $0x0
  800eb1:	52                   	push   %edx
  800eb2:	50                   	push   %eax
  800eb3:	6a 08                	push   $0x8
  800eb5:	e8 d5 fe ff ff       	call   800d8f <syscall>
  800eba:	83 c4 18             	add    $0x18,%esp
}
  800ebd:	c9                   	leave  
  800ebe:	c3                   	ret    

00800ebf <sys_calculate_required_frames>:

uint32 sys_calculate_required_frames(uint32 start_virtual_address, uint32 size)
{
  800ebf:	55                   	push   %ebp
  800ec0:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_req_frames, start_virtual_address, (uint32) size, 0, 0, 0);
  800ec2:	6a 00                	push   $0x0
  800ec4:	6a 00                	push   $0x0
  800ec6:	6a 00                	push   $0x0
  800ec8:	ff 75 0c             	pushl  0xc(%ebp)
  800ecb:	ff 75 08             	pushl  0x8(%ebp)
  800ece:	6a 09                	push   $0x9
  800ed0:	e8 ba fe ff ff       	call   800d8f <syscall>
  800ed5:	83 c4 18             	add    $0x18,%esp
}
  800ed8:	c9                   	leave  
  800ed9:	c3                   	ret    

00800eda <sys_calculate_free_frames>:

uint32 sys_calculate_free_frames()
{
  800eda:	55                   	push   %ebp
  800edb:	89 e5                	mov    %esp,%ebp
	return syscall(SYS_calc_free_frames, 0, 0, 0, 0, 0);
  800edd:	6a 00                	push   $0x0
  800edf:	6a 00                	push   $0x0
  800ee1:	6a 00                	push   $0x0
  800ee3:	6a 00                	push   $0x0
  800ee5:	6a 00                	push   $0x0
  800ee7:	6a 0a                	push   $0xa
  800ee9:	e8 a1 fe ff ff       	call   800d8f <syscall>
  800eee:	83 c4 18             	add    $0x18,%esp
}
  800ef1:	c9                   	leave  
  800ef2:	c3                   	ret    

00800ef3 <sys_freeMem>:

void sys_freeMem(void* start_virtual_address, uint32 size)
{
  800ef3:	55                   	push   %ebp
  800ef4:	89 e5                	mov    %esp,%ebp
	syscall(SYS_freeMem, (uint32) start_virtual_address, size, 0, 0, 0);
  800ef6:	8b 45 08             	mov    0x8(%ebp),%eax
  800ef9:	6a 00                	push   $0x0
  800efb:	6a 00                	push   $0x0
  800efd:	6a 00                	push   $0x0
  800eff:	ff 75 0c             	pushl  0xc(%ebp)
  800f02:	50                   	push   %eax
  800f03:	6a 0b                	push   $0xb
  800f05:	e8 85 fe ff ff       	call   800d8f <syscall>
  800f0a:	83 c4 18             	add    $0x18,%esp
	return;
  800f0d:	90                   	nop
}
  800f0e:	c9                   	leave  
  800f0f:	c3                   	ret    

00800f10 <__udivdi3>:
  800f10:	55                   	push   %ebp
  800f11:	57                   	push   %edi
  800f12:	56                   	push   %esi
  800f13:	53                   	push   %ebx
  800f14:	83 ec 1c             	sub    $0x1c,%esp
  800f17:	8b 74 24 3c          	mov    0x3c(%esp),%esi
  800f1b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
  800f1f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
  800f23:	8b 7c 24 38          	mov    0x38(%esp),%edi
  800f27:	85 f6                	test   %esi,%esi
  800f29:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  800f2d:	89 ca                	mov    %ecx,%edx
  800f2f:	89 f8                	mov    %edi,%eax
  800f31:	75 3d                	jne    800f70 <__udivdi3+0x60>
  800f33:	39 cf                	cmp    %ecx,%edi
  800f35:	0f 87 c5 00 00 00    	ja     801000 <__udivdi3+0xf0>
  800f3b:	85 ff                	test   %edi,%edi
  800f3d:	89 fd                	mov    %edi,%ebp
  800f3f:	75 0b                	jne    800f4c <__udivdi3+0x3c>
  800f41:	b8 01 00 00 00       	mov    $0x1,%eax
  800f46:	31 d2                	xor    %edx,%edx
  800f48:	f7 f7                	div    %edi
  800f4a:	89 c5                	mov    %eax,%ebp
  800f4c:	89 c8                	mov    %ecx,%eax
  800f4e:	31 d2                	xor    %edx,%edx
  800f50:	f7 f5                	div    %ebp
  800f52:	89 c1                	mov    %eax,%ecx
  800f54:	89 d8                	mov    %ebx,%eax
  800f56:	89 cf                	mov    %ecx,%edi
  800f58:	f7 f5                	div    %ebp
  800f5a:	89 c3                	mov    %eax,%ebx
  800f5c:	89 d8                	mov    %ebx,%eax
  800f5e:	89 fa                	mov    %edi,%edx
  800f60:	83 c4 1c             	add    $0x1c,%esp
  800f63:	5b                   	pop    %ebx
  800f64:	5e                   	pop    %esi
  800f65:	5f                   	pop    %edi
  800f66:	5d                   	pop    %ebp
  800f67:	c3                   	ret    
  800f68:	90                   	nop
  800f69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  800f70:	39 ce                	cmp    %ecx,%esi
  800f72:	77 74                	ja     800fe8 <__udivdi3+0xd8>
  800f74:	0f bd fe             	bsr    %esi,%edi
  800f77:	83 f7 1f             	xor    $0x1f,%edi
  800f7a:	0f 84 98 00 00 00    	je     801018 <__udivdi3+0x108>
  800f80:	bb 20 00 00 00       	mov    $0x20,%ebx
  800f85:	89 f9                	mov    %edi,%ecx
  800f87:	89 c5                	mov    %eax,%ebp
  800f89:	29 fb                	sub    %edi,%ebx
  800f8b:	d3 e6                	shl    %cl,%esi
  800f8d:	89 d9                	mov    %ebx,%ecx
  800f8f:	d3 ed                	shr    %cl,%ebp
  800f91:	89 f9                	mov    %edi,%ecx
  800f93:	d3 e0                	shl    %cl,%eax
  800f95:	09 ee                	or     %ebp,%esi
  800f97:	89 d9                	mov    %ebx,%ecx
  800f99:	89 44 24 0c          	mov    %eax,0xc(%esp)
  800f9d:	89 d5                	mov    %edx,%ebp
  800f9f:	8b 44 24 08          	mov    0x8(%esp),%eax
  800fa3:	d3 ed                	shr    %cl,%ebp
  800fa5:	89 f9                	mov    %edi,%ecx
  800fa7:	d3 e2                	shl    %cl,%edx
  800fa9:	89 d9                	mov    %ebx,%ecx
  800fab:	d3 e8                	shr    %cl,%eax
  800fad:	09 c2                	or     %eax,%edx
  800faf:	89 d0                	mov    %edx,%eax
  800fb1:	89 ea                	mov    %ebp,%edx
  800fb3:	f7 f6                	div    %esi
  800fb5:	89 d5                	mov    %edx,%ebp
  800fb7:	89 c3                	mov    %eax,%ebx
  800fb9:	f7 64 24 0c          	mull   0xc(%esp)
  800fbd:	39 d5                	cmp    %edx,%ebp
  800fbf:	72 10                	jb     800fd1 <__udivdi3+0xc1>
  800fc1:	8b 74 24 08          	mov    0x8(%esp),%esi
  800fc5:	89 f9                	mov    %edi,%ecx
  800fc7:	d3 e6                	shl    %cl,%esi
  800fc9:	39 c6                	cmp    %eax,%esi
  800fcb:	73 07                	jae    800fd4 <__udivdi3+0xc4>
  800fcd:	39 d5                	cmp    %edx,%ebp
  800fcf:	75 03                	jne    800fd4 <__udivdi3+0xc4>
  800fd1:	83 eb 01             	sub    $0x1,%ebx
  800fd4:	31 ff                	xor    %edi,%edi
  800fd6:	89 d8                	mov    %ebx,%eax
  800fd8:	89 fa                	mov    %edi,%edx
  800fda:	83 c4 1c             	add    $0x1c,%esp
  800fdd:	5b                   	pop    %ebx
  800fde:	5e                   	pop    %esi
  800fdf:	5f                   	pop    %edi
  800fe0:	5d                   	pop    %ebp
  800fe1:	c3                   	ret    
  800fe2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  800fe8:	31 ff                	xor    %edi,%edi
  800fea:	31 db                	xor    %ebx,%ebx
  800fec:	89 d8                	mov    %ebx,%eax
  800fee:	89 fa                	mov    %edi,%edx
  800ff0:	83 c4 1c             	add    $0x1c,%esp
  800ff3:	5b                   	pop    %ebx
  800ff4:	5e                   	pop    %esi
  800ff5:	5f                   	pop    %edi
  800ff6:	5d                   	pop    %ebp
  800ff7:	c3                   	ret    
  800ff8:	90                   	nop
  800ff9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  801000:	89 d8                	mov    %ebx,%eax
  801002:	f7 f7                	div    %edi
  801004:	31 ff                	xor    %edi,%edi
  801006:	89 c3                	mov    %eax,%ebx
  801008:	89 d8                	mov    %ebx,%eax
  80100a:	89 fa                	mov    %edi,%edx
  80100c:	83 c4 1c             	add    $0x1c,%esp
  80100f:	5b                   	pop    %ebx
  801010:	5e                   	pop    %esi
  801011:	5f                   	pop    %edi
  801012:	5d                   	pop    %ebp
  801013:	c3                   	ret    
  801014:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  801018:	39 ce                	cmp    %ecx,%esi
  80101a:	72 0c                	jb     801028 <__udivdi3+0x118>
  80101c:	31 db                	xor    %ebx,%ebx
  80101e:	3b 44 24 08          	cmp    0x8(%esp),%eax
  801022:	0f 87 34 ff ff ff    	ja     800f5c <__udivdi3+0x4c>
  801028:	bb 01 00 00 00       	mov    $0x1,%ebx
  80102d:	e9 2a ff ff ff       	jmp    800f5c <__udivdi3+0x4c>
  801032:	66 90                	xchg   %ax,%ax
  801034:	66 90                	xchg   %ax,%ax
  801036:	66 90                	xchg   %ax,%ax
  801038:	66 90                	xchg   %ax,%ax
  80103a:	66 90                	xchg   %ax,%ax
  80103c:	66 90                	xchg   %ax,%ax
  80103e:	66 90                	xchg   %ax,%ax

00801040 <__umoddi3>:
  801040:	55                   	push   %ebp
  801041:	57                   	push   %edi
  801042:	56                   	push   %esi
  801043:	53                   	push   %ebx
  801044:	83 ec 1c             	sub    $0x1c,%esp
  801047:	8b 54 24 3c          	mov    0x3c(%esp),%edx
  80104b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
  80104f:	8b 74 24 34          	mov    0x34(%esp),%esi
  801053:	8b 7c 24 38          	mov    0x38(%esp),%edi
  801057:	85 d2                	test   %edx,%edx
  801059:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  80105d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  801061:	89 f3                	mov    %esi,%ebx
  801063:	89 3c 24             	mov    %edi,(%esp)
  801066:	89 74 24 04          	mov    %esi,0x4(%esp)
  80106a:	75 1c                	jne    801088 <__umoddi3+0x48>
  80106c:	39 f7                	cmp    %esi,%edi
  80106e:	76 50                	jbe    8010c0 <__umoddi3+0x80>
  801070:	89 c8                	mov    %ecx,%eax
  801072:	89 f2                	mov    %esi,%edx
  801074:	f7 f7                	div    %edi
  801076:	89 d0                	mov    %edx,%eax
  801078:	31 d2                	xor    %edx,%edx
  80107a:	83 c4 1c             	add    $0x1c,%esp
  80107d:	5b                   	pop    %ebx
  80107e:	5e                   	pop    %esi
  80107f:	5f                   	pop    %edi
  801080:	5d                   	pop    %ebp
  801081:	c3                   	ret    
  801082:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  801088:	39 f2                	cmp    %esi,%edx
  80108a:	89 d0                	mov    %edx,%eax
  80108c:	77 52                	ja     8010e0 <__umoddi3+0xa0>
  80108e:	0f bd ea             	bsr    %edx,%ebp
  801091:	83 f5 1f             	xor    $0x1f,%ebp
  801094:	75 5a                	jne    8010f0 <__umoddi3+0xb0>
  801096:	3b 54 24 04          	cmp    0x4(%esp),%edx
  80109a:	0f 82 e0 00 00 00    	jb     801180 <__umoddi3+0x140>
  8010a0:	39 0c 24             	cmp    %ecx,(%esp)
  8010a3:	0f 86 d7 00 00 00    	jbe    801180 <__umoddi3+0x140>
  8010a9:	8b 44 24 08          	mov    0x8(%esp),%eax
  8010ad:	8b 54 24 04          	mov    0x4(%esp),%edx
  8010b1:	83 c4 1c             	add    $0x1c,%esp
  8010b4:	5b                   	pop    %ebx
  8010b5:	5e                   	pop    %esi
  8010b6:	5f                   	pop    %edi
  8010b7:	5d                   	pop    %ebp
  8010b8:	c3                   	ret    
  8010b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  8010c0:	85 ff                	test   %edi,%edi
  8010c2:	89 fd                	mov    %edi,%ebp
  8010c4:	75 0b                	jne    8010d1 <__umoddi3+0x91>
  8010c6:	b8 01 00 00 00       	mov    $0x1,%eax
  8010cb:	31 d2                	xor    %edx,%edx
  8010cd:	f7 f7                	div    %edi
  8010cf:	89 c5                	mov    %eax,%ebp
  8010d1:	89 f0                	mov    %esi,%eax
  8010d3:	31 d2                	xor    %edx,%edx
  8010d5:	f7 f5                	div    %ebp
  8010d7:	89 c8                	mov    %ecx,%eax
  8010d9:	f7 f5                	div    %ebp
  8010db:	89 d0                	mov    %edx,%eax
  8010dd:	eb 99                	jmp    801078 <__umoddi3+0x38>
  8010df:	90                   	nop
  8010e0:	89 c8                	mov    %ecx,%eax
  8010e2:	89 f2                	mov    %esi,%edx
  8010e4:	83 c4 1c             	add    $0x1c,%esp
  8010e7:	5b                   	pop    %ebx
  8010e8:	5e                   	pop    %esi
  8010e9:	5f                   	pop    %edi
  8010ea:	5d                   	pop    %ebp
  8010eb:	c3                   	ret    
  8010ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  8010f0:	8b 34 24             	mov    (%esp),%esi
  8010f3:	bf 20 00 00 00       	mov    $0x20,%edi
  8010f8:	89 e9                	mov    %ebp,%ecx
  8010fa:	29 ef                	sub    %ebp,%edi
  8010fc:	d3 e0                	shl    %cl,%eax
  8010fe:	89 f9                	mov    %edi,%ecx
  801100:	89 f2                	mov    %esi,%edx
  801102:	d3 ea                	shr    %cl,%edx
  801104:	89 e9                	mov    %ebp,%ecx
  801106:	09 c2                	or     %eax,%edx
  801108:	89 d8                	mov    %ebx,%eax
  80110a:	89 14 24             	mov    %edx,(%esp)
  80110d:	89 f2                	mov    %esi,%edx
  80110f:	d3 e2                	shl    %cl,%edx
  801111:	89 f9                	mov    %edi,%ecx
  801113:	89 54 24 04          	mov    %edx,0x4(%esp)
  801117:	8b 54 24 0c          	mov    0xc(%esp),%edx
  80111b:	d3 e8                	shr    %cl,%eax
  80111d:	89 e9                	mov    %ebp,%ecx
  80111f:	89 c6                	mov    %eax,%esi
  801121:	d3 e3                	shl    %cl,%ebx
  801123:	89 f9                	mov    %edi,%ecx
  801125:	89 d0                	mov    %edx,%eax
  801127:	d3 e8                	shr    %cl,%eax
  801129:	89 e9                	mov    %ebp,%ecx
  80112b:	09 d8                	or     %ebx,%eax
  80112d:	89 d3                	mov    %edx,%ebx
  80112f:	89 f2                	mov    %esi,%edx
  801131:	f7 34 24             	divl   (%esp)
  801134:	89 d6                	mov    %edx,%esi
  801136:	d3 e3                	shl    %cl,%ebx
  801138:	f7 64 24 04          	mull   0x4(%esp)
  80113c:	39 d6                	cmp    %edx,%esi
  80113e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  801142:	89 d1                	mov    %edx,%ecx
  801144:	89 c3                	mov    %eax,%ebx
  801146:	72 08                	jb     801150 <__umoddi3+0x110>
  801148:	75 11                	jne    80115b <__umoddi3+0x11b>
  80114a:	39 44 24 08          	cmp    %eax,0x8(%esp)
  80114e:	73 0b                	jae    80115b <__umoddi3+0x11b>
  801150:	2b 44 24 04          	sub    0x4(%esp),%eax
  801154:	1b 14 24             	sbb    (%esp),%edx
  801157:	89 d1                	mov    %edx,%ecx
  801159:	89 c3                	mov    %eax,%ebx
  80115b:	8b 54 24 08          	mov    0x8(%esp),%edx
  80115f:	29 da                	sub    %ebx,%edx
  801161:	19 ce                	sbb    %ecx,%esi
  801163:	89 f9                	mov    %edi,%ecx
  801165:	89 f0                	mov    %esi,%eax
  801167:	d3 e0                	shl    %cl,%eax
  801169:	89 e9                	mov    %ebp,%ecx
  80116b:	d3 ea                	shr    %cl,%edx
  80116d:	89 e9                	mov    %ebp,%ecx
  80116f:	d3 ee                	shr    %cl,%esi
  801171:	09 d0                	or     %edx,%eax
  801173:	89 f2                	mov    %esi,%edx
  801175:	83 c4 1c             	add    $0x1c,%esp
  801178:	5b                   	pop    %ebx
  801179:	5e                   	pop    %esi
  80117a:	5f                   	pop    %edi
  80117b:	5d                   	pop    %ebp
  80117c:	c3                   	ret    
  80117d:	8d 76 00             	lea    0x0(%esi),%esi
  801180:	29 f9                	sub    %edi,%ecx
  801182:	19 d6                	sbb    %edx,%esi
  801184:	89 74 24 04          	mov    %esi,0x4(%esp)
  801188:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  80118c:	e9 18 ff ff ff       	jmp    8010a9 <__umoddi3+0x69>
